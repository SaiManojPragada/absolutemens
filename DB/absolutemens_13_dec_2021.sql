-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 13, 2021 at 01:03 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.29-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `absolutemens`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE `aboutus` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `short_description` text,
  `description` text,
  `video_url` varchar(100) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aboutus`
--

INSERT INTO `aboutus` (`id`, `title`, `short_description`, `description`, `video_url`, `created_at`, `updated_at`) VALUES
(1, 'ABOUT US', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure nostrum numquam, distinctio corrupti nam optio fuga, molestiae natus facilis culpa accusantium molestias, excepturi! Atque libero accusamus inventore eaque. Neque, eum? Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure nostrum numquam, distinctio corrupti nam optio fuga, molestiae natus facilis culpa accusantium molestias, excepturi! Atque libero accusamus inventore eaque. Neque, eum?</p>\r\n', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n', 'https://www.youtube.com/embed/r96qIXOd2os', '1632730977', '1632894859');

-- --------------------------------------------------------

--
-- Table structure for table `access_rights`
--

CREATE TABLE `access_rights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `privileges_code` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `updated_at` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_addresses`
--

CREATE TABLE `admin_addresses` (
  `id` int(11) NOT NULL,
  `address_title` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_addresses`
--

INSERT INTO `admin_addresses` (`id`, `address_title`, `address`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Admin Office One', '<p>This is the Main Address in the area</p>\r\n', '1637570281', '1637570336', 1),
(2, 'Admin Office One', '<p>This is the Second Address in the Area</p>\r\n', '1637570297', '1638779643', 1),
(3, 'Main Branch', '<p>5th lane Dwarka Nagar, near degree college, Bharat Towers.</p>\r\n', '1638779618', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_dashboard_main_menu`
--

CREATE TABLE `admin_dashboard_main_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `menu_icon` varchar(100) DEFAULT NULL,
  `display_order` int(11) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `menu_url` varchar(100) NOT NULL DEFAULT '#',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_dashboard_main_menu`
--

INSERT INTO `admin_dashboard_main_menu` (`id`, `menu_name`, `menu_icon`, `display_order`, `created_at`, `updated_at`, `menu_url`, `status`) VALUES
(1, 'Admin Control Panel', 'fa fa-th-large', 0, '1623047703', '', '#', 1),
(13, 'Enquiries', 'fa fa-comments-o', 101, '1623119550', '', 'enquiries', 1),
(14, 'CMS Pages', 'fa fa-book', 11, '1623125694', '1632735366', 'Cms_pages', 1),
(15, 'Admin Login Logs', 'fa fa-user', 115, '1623218389', '1633065000', 'login_logs', 1),
(17, 'Home Page', 'fa fa-home', 2, '1632722526', '1633070947', '#', 1),
(18, 'Packages', 'fa fa-money', 10, '1632726397', '', 'packages', 1),
(19, 'About Us', 'fa fa-info-circle', 2, '1632728373', '', 'aboutus', 1),
(20, 'Manage Footer', 'fa fa-download', 12, '1632734603', '', '#', 1),
(21, 'Blogs', 'fa fa-rss', 11, '1632895652', '1632895683', 'blogs', 1),
(22, 'Email Subscriptions', 'fa fa-envelope', 102, '1632918445', '1632918464', 'email_subscriptions', 1),
(23, 'Manage Master', 'fa fa-tachometer', 1, '1632978926', '1632979058', '#', 1),
(24, 'Customers', 'fa fa-users', 103, '1632979864', '', '#', 1),
(25, 'Manage Locations', 'fa fa-map-marker', 1, '1632993050', '1632993065', '#', 1),
(26, 'Manage Vendors', 'fa fa-shopping-cart', 104, '1633064674', '1633064741', '#', 1),
(27, 'Customers Reports', 'fa fa-flag', 102, '1633095796', '', 'customers_reports', 1),
(28, 'We Shop For You Info', 'fa fa-shopping-cart', 2, '1633945500', '1633945521', 'we_shop_for_you_info', 1),
(29, 'Subscriptions List', 'fa fa-paste', 15, '1634705377', '1637061079', 'orders_list', 0),
(30, 'Manage Fashion Designers & Analysts', 'fa fa-users', 105, '1634969131', '1634969266', '#', 1),
(31, 'Manage Orders', 'fa fa-paste	', 6, '1636370772', '1637061095', 'orders', 1),
(32, 'Vendor Withdraw Requests', 'fa fa-wallet', 10, '', '', 'vendor_withdraw_requests', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_dashboard_sub_menu`
--

CREATE TABLE `admin_dashboard_sub_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `menu_url` varchar(100) NOT NULL,
  `main_menu_id` int(10) UNSIGNED NOT NULL,
  `display_order` int(10) UNSIGNED NOT NULL DEFAULT '10',
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_dashboard_sub_menu`
--

INSERT INTO `admin_dashboard_sub_menu` (`id`, `menu_name`, `menu_url`, `main_menu_id`, `display_order`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Database Backup', 'dbbackup', 1, 1, '1623050682', '', 1),
(2, 'Social Media Links', 'Social_media_links', 1, 2, '1623050707', '', 1),
(3, 'Site Settings', 'site_settings', 1, 3, '1623050729', '', 1),
(4, 'Change Password	', 'change_password', 1, 4, '1623050771', '', 1),
(5, 'Sliders', 'sliders', 17, 1, '1623051600', '1632725963', 1),
(38, 'Banners', 'banners', 17, 2, '1632727062', '', 1),
(39, 'How it Works Images', 'how_it_works_images', 17, 3, '1632727532', '', 1),
(40, 'How it Works Points', 'how_it_works_points', 17, 4, '1632727557', '', 1),
(41, 'Testimonials', 'testimonials', 17, 5, '1632732523', '', 1),
(42, 'Blog Section Info', 'blog_info', 17, 6, '1632733918', '', 1),
(43, 'Footer Info', 'footer_info', 20, 1, '1632734625', '1632734631', 1),
(44, 'Styles', 'Styles', 23, 4, '1632979102', '', 1),
(45, 'Users List', 'customers', 24, 1, '1632979889', '', 1),
(46, 'User Login Logs', 'user_login_logs', 24, 2, '1632979913', '', 1),
(47, 'States', 'states', 25, 1, '1632993379', '', 1),
(48, 'Districts', 'districts', 25, 2, '1632993396', '', 1),
(49, 'Cities', 'cities', 25, 3, '1632993413', '', 1),
(50, 'Vendors List', 'Vendors', 26, 1, '1633064767', '', 1),
(68, 'SEO', 'seo', 1, 100, '1639205876', '', 1),
(67, 'Orders Feedbacks', 'orders_feedbacks', 31, 3, '1638782336', '', 1),
(66, 'Sizes', 'Sizes', 23, 3, '', '', 1),
(54, 'Manage Heights', 'add_manage_heights', 23, 5, '1633936997', '', 1),
(55, 'Manage Weights', 'add_manage_weights', 23, 6, '1633937026', '', 1),
(56, 'Manage Packages', 'packages', 18, 1, '1634014746', '', 1),
(57, 'Package Page Banners', 'package_page_banners', 18, 2, '1634014776', '', 1),
(58, 'Coupons', 'coupons', 23, 7, '1634710592', '', 1),
(59, 'Designers & Analysts', 'designers_analysts', 30, 1, '1634969308', '', 1),
(60, 'Categories', 'categories', 23, 1, '1635484336', '1635484390', 1),
(61, 'Customer Orders', 'orders', 31, 1, '1637062204', '', 1),
(62, 'Product Orders', 'product_orders', 31, 2, '1637062290', '1637062497', 1),
(63, 'List', 'withdraw_history', 32, 10, '', '', 1),
(64, 'Admin Addresses', 'admin_addresses', 1, 50, '1637569999', '', 1),
(65, 'Subcategories', 'Sub_categories', 23, 2, '', '', 1),
(69, 'Terms & Conditions', 'terms_and_conditions', 1, 101, '1639206608', '1639207475', 0),
(70, 'Privacy Policy', 'Privacy_policy', 1, 102, '1639206729', '1639207469', 0),
(71, 'Return Policies', 'return_policy', 1, 103, '1639206957', '1639207463', 0);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image`, `created_at`, `updated_at`, `status`, `link`) VALUES
(1, '24b6b27f552a237c853815be5895526f.gif', '1632727167', '1638780529', 1, 'https://www.google.com'),
(2, '5dc52ea8f7c0a6885ab2cacf9c126f3e.jpg', '1632727403', NULL, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `image` varchar(100) NOT NULL COMMENT '<font><b>Note : </b></font> Image Size Width: 560px; Height: 429px',
  `short_description` text,
  `description` longtext,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `slug`, `image`, `short_description`, `description`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Aypi Non Habent Claritatem Insitam', 'aypi-non-habent-claritatem-insitam', '83e0378f46440e3cc574ce1a6d8d1872.jpg', '<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>\r\n', '<h3>Aypi Non Habent Claritatem Insitam.</h3>\r\n\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example,</p>\r\n\r\n<p>which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<blockquote>No one rejects, dislikes, or avoids pleasure itself, because it pleasure, but because those who do not know how to pursue pleasure ratinally encounter consequences that are.</blockquote>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>\r\n', '1632896251', '1637385233', 1),
(2, 'Bypi non habent claritatem  habent claritate insitam', 'bypi-non-habent-claritatem-habent-claritate-insitam', 'b4f8fe2e32011258a9e0a16449f67a36.jpg', '<p>which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</p>\r\n', '<h3>Bypi non habent claritatem &nbsp;habent claritate insitam.</h3>\r\n\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example,</p>\r\n\r\n<p>which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<blockquote>No one rejects, dislikes, or avoids pleasure itself, because it pleasure, but because those who do not know how to pursue pleasure ratinally encounter consequences that are.</blockquote>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>\r\n', '1632896297', '1637385237', 1),
(3, 'Cypi non habent claritatem habent clari insitam', 'cypi-non-habent-claritatem-habent-clari-insitam', '4b07124c42451670d49a6e933842432b.jpg', '<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>\r\n', '<h3>Cypi non habent claritatem habent clari insitam.</h3>\r\n\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example,</p>\r\n\r\n<p>which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&rdquo;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<blockquote>No one rejects, dislikes, or avoids pleasure itself, because it pleasure, but because those who do not know how to pursue pleasure ratinally encounter consequences that are.</blockquote>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.</p>\r\n', '1632896396', '1637385241', 1);

-- --------------------------------------------------------

--
-- Table structure for table `blog_info`
--

CREATE TABLE `blog_info` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `sub_title` text,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog_info`
--

INSERT INTO `blog_info` (`id`, `title`, `sub_title`, `created_at`, `updated_at`, `status`) VALUES
(1, 'From Our Blog', '<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram anteposuerit litterarum formas</p>\r\n', '1632733956', '1638781076', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `subscription_transaction_id` bigint(20) NOT NULL,
  `designers_and_analysts_id` bigint(20) NOT NULL,
  `vendors_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_inventory_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `total_amount` float NOT NULL,
  `cart_status` enum('active','in_active') NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `data_table_prefix` varchar(100) NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `data_table_prefix`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Shirts', 'shirts', '1635484529', '1638779689', 1),
(2, 'T-Shirts', 'shirts', '1635484578', '1635490266', 1),
(3, 'Pants', 'pants', '1635484645', '1635490271', 1),
(4, 'Shoes', 'shoes', '1635484657', '1635490275', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `districts_id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `districts_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Vizag', '1632995116', NULL),
(2, 2, 'Vizianagaram', '1632995144', NULL),
(3, 3, 'Hyderabad', '1638338344', NULL),
(4, 4, 'Warangal', '1638780317', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `seo_title` varchar(100) DEFAULT NULL,
  `seo_keywords` text,
  `seo_description` text,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `name`, `title`, `description`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Terms and Conditions', 'Terms and Conditions', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n', 'Terms and Conditions', 'Terms and Conditions', 'Terms and Conditions', '1632736580', '1639207736', 1),
(2, 'Privacy Policy', 'Privacy Policy', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n', 'Privacy Policy', 'Privacy Policy', 'Privacy Policy', '1632736612', '1639207744', 1),
(3, 'Return Policy', 'Return Policy', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n', 'Return Policy	', 'Return Policy	', 'Return Policy	', '1632736623', '1639207761', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact_enquiries`
--

CREATE TABLE `contact_enquiries` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `request_type` varchar(150) NOT NULL,
  `message` text NOT NULL,
  `created_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_enquiries`
--

INSERT INTO `contact_enquiries` (`id`, `name`, `email`, `phone`, `request_type`, `message`, `created_at`) VALUES
(1, 'Grace King', 'manumanoj199700@gmail.com', '09000700588', 'Partner with us', 'dasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsad', '1633009715'),
(2, 'Grace King', 'manumanoj199700@gmail.com', '09000700588', 'Partner with us', 'dasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsad', '1633009791'),
(3, 'Grace King', 'manumanoj199700@gmail.com', '09000700588', 'Partner with us', 'dasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsad', '1633009871'),
(4, 'Grace King', 'manumanoj199700@gmail.com', '09000700588', 'Partner with us', 'dasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsaddasdsad', '1633010002'),
(5, 'Grace King', 'manumanoj199700@gmail.com', '09000700588', 'Partner with us', 'dasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdas', '1633010072'),
(6, 'Grace King', 'manumanoj199700@gmail.com', '09000700588', 'Partner with us', 'Test Message cahcua aijsdsandosa', '1638256024'),
(7, 'Manoj', 'manumanoj199700@gmail.com', '9000700588', 'Work with us', 'Manoj is this a Test message, Yes it is good.', '1638256767'),
(8, 'Manoj', 'manumanoj199700@gmail.com', '9000700588', 'Partner with us', 'This is a Message that is Given and Takenb', '1638257857'),
(9, 'pavanmadhur kolukula', 'pavanmadhurjune30@gmail.com', '9390412309', 'Other', 'I want to work with your company', '1638259399'),
(10, 'Grace King', 'manumanoj199700@gmail.com', '9000700588', 'Partner with us', 'asdsad ad asd sad sad ad asd adada da dsad a', '1638260655');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `coupon_code` varchar(100) NOT NULL,
  `discount_type` enum('Flat','Percentage') NOT NULL,
  `discount` int(11) NOT NULL,
  `discount_max_amount` int(11) NOT NULL DEFAULT '0' COMMENT '<font color="red"><b>Note : </b></font>Leave it 0 ''Zero'' for no max limit',
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `coupon_code`, `discount_type`, `discount`, `discount_max_amount`, `created_at`, `updated_at`, `status`) VALUES
(1, 'GET10', 'Percentage', 10, 1000, '1634710964', '1638946339', 1),
(2, 'GET200', 'Flat', 200, 500, '1634710990', '1638965159', 1),
(5, 'FIRSTUSER', 'Flat', 50, 5000, '1638785689', '1638785703', 1),
(6, 'NEWCODE', 'Percentage', 50, 5000, '1638946086', '1638965221', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(100) DEFAULT NULL,
  `address` text,
  `states_id` bigint(20) DEFAULT NULL,
  `districts_id` bigint(20) DEFAULT NULL,
  `cities_id` bigint(20) DEFAULT NULL,
  `landmark` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `pincode` varchar(25) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `forgot_password_token` text,
  `forgot_password_timeout` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `preferences` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `mobile`, `password`, `salt`, `address`, `states_id`, `districts_id`, `cities_id`, `landmark`, `street_name`, `pincode`, `created_at`, `updated_at`, `forgot_password_token`, `forgot_password_timeout`, `status`, `preferences`) VALUES
(3, 'Grace King', 'manumanoj199700@gmail.com', '9000700588', '9d7891f0eaa99346ad9ba9d89b09a0a8', 'Klc7RY', 'D.no: 5-96, vivekanandha nagar, street no: 4, Old Dairy farm', 1, 2, 2, 'Landmark is this', 'OLD DAIRY FARM', '530040', '1633068585', '1638962016', '1638260574t2EP4Igtt3', '1638267720', 1, '{\"1\": \"2\", \"2\": \"8\", \"3\": \"12\", \"4\": \"21\"}'),
(4, 'Test Account', 'test@test.com', '9900990088', 'd23527f64628e2e84d5b4e0f81a90157', 'EOzllZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1633068692', NULL, NULL, NULL, 1, 'null'),
(6, 'Siripuram Shiva Teja', 'shivateja2207@gmail.com', '9390791945', '7c510fc0b76f9e02692300b9e847093d', 'slKTBP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1637406248', NULL, NULL, NULL, 1, '{\"1\": \"4\", \"2\": \"10\", \"3\": \"17\", \"4\": \"23\"}'),
(9, 'Manoj', 'pragadasaimanoj@gmail.com', '9977889988', '0a4483b89692dc536831c4c11668325a', 'i7uQds', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1638252384', NULL, NULL, NULL, 1, 'null'),
(11, 'akhil', 'dvakhil@gmail.com', '9059594813', 'aa7c802f77453da0306a6030ab84e88e', 'uBGFfj', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1638422791', NULL, NULL, NULL, 1, 'null'),
(12, 'Manikanta Reddy', 'manikantareddy166328@gmail.com', '8790361953', 'd67ead9fcf5d4030152cc18a1c472f1f', 'qFbx0g', 'allipuram, ammavari temple street', 1, 1, 1, 'Near Royal Enfield showroom', 'allipuram, ammavari temple street', '123456', '1638506899', '1638780698', NULL, NULL, 1, '{\"1\": \"3\", \"2\": \"9\", \"3\": \"15\", \"4\": \"22\"}'),
(13, 'Vijay', 'vgatla65@gmail.com', '9989891918', '82a5874aaba4dd69df3d5c090fd6b275', 'OzCPe0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1638778765', NULL, NULL, NULL, 1, '{\"1\": \"2\", \"2\": \"7\", \"3\": \"13\", \"4\": \"20\"}');

-- --------------------------------------------------------

--
-- Table structure for table `customers_login_logs`
--

CREATE TABLE `customers_login_logs` (
  `id` int(11) NOT NULL,
  `customers_id` bigint(20) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `ip_address` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customers_login_logs`
--

INSERT INTO `customers_login_logs` (`id`, `customers_id`, `created_at`, `ip_address`) VALUES
(10, 3, '1633072004', '202.53.69.163'),
(11, 3, '1633072013', '202.53.69.163'),
(12, 3, '1633072064', '202.53.69.163'),
(13, 3, '1633072073', '202.53.69.163'),
(14, 3, '1633091884', '202.53.69.163'),
(15, 3, '1633329217', '49.37.148.93'),
(16, 3, '1633935275', '49.37.148.116'),
(17, 3, '1633943809', '49.37.148.116'),
(18, 3, '1633954561', '49.37.148.116'),
(19, 3, '1634014554', '202.53.69.163'),
(20, 3, '1634023676', '202.53.69.163'),
(21, 3, '1634029982', '49.37.146.127'),
(22, 3, '1634041683', '49.37.146.127'),
(23, 3, '1634100980', '49.37.146.127'),
(24, 3, '1634101612', '49.37.146.127'),
(25, 3, '1634115853', '49.37.146.127'),
(26, 3, '1634124373', '49.37.146.127'),
(27, 3, '1634126635', '49.37.146.127'),
(28, 3, '1634137763', '49.37.146.127'),
(29, 3, '1634190499', '49.37.146.127'),
(30, 3, '1634192443', '49.37.146.127'),
(31, 3, '1634195395', '49.37.146.127'),
(32, 3, '1634217261', '49.37.146.127'),
(33, 3, '1634360036', '49.37.149.133'),
(34, 3, '1634639810', '49.37.149.67'),
(35, 3, '1634726144', '49.37.149.67'),
(36, 3, '1634800697', '49.37.150.118'),
(37, 3, '1634883135', '202.53.69.163'),
(38, 3, '1636964568', '49.37.148.219'),
(39, 3, '1636964902', '49.37.148.219'),
(40, 5, '1637059388', '49.37.175.13'),
(41, 3, '1637059655', '49.37.175.13'),
(42, 3, '1637383033', '49.37.148.219'),
(43, 3, '1637383059', '49.37.148.219'),
(44, 3, '1637385685', '49.37.148.219'),
(45, 3, '1637385885', '49.37.148.219'),
(46, 3, '1637388985', '49.37.169.185'),
(47, 3, '1637573781', '49.37.148.219'),
(48, 3, '1637575688', '49.37.148.219'),
(49, 3, '1637575767', '49.37.148.219'),
(50, 3, '1637576544', '49.37.148.219'),
(51, 3, '1637576558', '49.37.148.219'),
(52, 3, '1637576654', '49.37.148.219'),
(53, 3, '1637576673', '49.37.148.219'),
(54, 3, '1637582591', '49.37.148.219'),
(55, 3, '1637583247', '49.37.148.219'),
(56, 3, '1637649960', '49.37.148.219'),
(57, 3, '1637730265', '49.37.148.219'),
(58, 3, '1637732180', '49.37.175.73'),
(59, 3, '1637738104', '49.37.148.219'),
(60, 3, '1637738146', '49.37.148.219'),
(61, 3, '1637738816', '49.37.148.219'),
(62, 3, '1637738931', '49.37.148.219'),
(63, 3, '1637738947', '49.37.148.219'),
(64, 3, '1637739111', '49.37.148.219'),
(65, 3, '1637739139', '49.37.148.219'),
(66, 3, '1637825140', '49.37.148.219'),
(67, 5, '1637920999', '49.37.173.151'),
(68, 3, '1637941271', '124.123.175.209'),
(69, 3, '1637988823', '49.37.151.25'),
(70, 3, '1637989190', '49.37.151.25'),
(71, 3, '1638014153', '49.206.46.41'),
(72, 3, '1638168360', '49.37.151.25'),
(73, 3, '1638183176', '49.37.151.25'),
(74, 3, '1638252773', '49.37.151.25'),
(75, 10, '1638260957', '49.206.48.177'),
(76, 10, '1638265299', '49.206.48.177'),
(77, 11, '1638422813', '103.5.134.176'),
(78, 3, '1638423842', '49.37.151.25'),
(79, 3, '1638425674', '49.37.151.25'),
(80, 3, '1638429083', '49.37.151.25'),
(81, 3, '1638429294', '49.37.151.25'),
(82, 3, '1638430072', '49.37.151.25'),
(83, 3, '1638451276', '49.37.151.25'),
(84, 12, '1638598550', '202.53.69.163'),
(85, 12, '1638600158', '202.53.69.163'),
(86, 12, '1638601363', '49.37.151.25'),
(87, 12, '1638767080', '202.53.69.163'),
(88, 3, '1638770221', '49.37.151.25'),
(89, 13, '1638778782', '49.37.171.3'),
(90, 13, '1638779993', '49.37.171.3'),
(91, 12, '1638780670', '202.53.69.163'),
(92, 12, '1638781547', '202.53.69.163'),
(93, 3, '1638781769', '49.37.151.25'),
(94, 13, '1638782058', '49.37.171.3'),
(95, 3, '1638784207', '49.37.151.25'),
(96, 3, '1638854383', '49.37.151.25'),
(97, 12, '1638879604', '202.53.69.163'),
(98, 12, '1638879615', '202.53.69.163'),
(99, 12, '1638944706', '202.53.69.163'),
(100, 3, '1638954242', '49.37.148.243'),
(101, 3, '1639138594', '49.37.148.243'),
(102, 3, '1639197510', '49.37.148.243'),
(103, 3, '1639201188', '49.37.148.243'),
(104, 3, '1639204846', '49.37.148.243'),
(105, 3, '1639204869', '49.37.148.243'),
(106, 3, '1639204881', '49.37.148.243'),
(107, 3, '1639204888', '49.37.148.243'),
(108, 3, '1639204908', '49.37.148.243'),
(109, 3, '1639205433', '49.37.148.243'),
(110, 12, '1639209292', '202.53.69.163'),
(111, 6, '1639299498', '103.5.134.179'),
(112, 6, '1639303264', '103.5.134.179'),
(113, 6, '1639308431', '103.5.134.179'),
(114, 6, '1639308809', '103.5.134.179'),
(115, 6, '1639309273', '103.5.134.179'),
(116, 6, '1639309467', '103.5.134.179');

-- --------------------------------------------------------

--
-- Table structure for table `customer_reports`
--

CREATE TABLE `customer_reports` (
  `id` int(11) NOT NULL,
  `customers_id` bigint(20) NOT NULL,
  `order_id` varchar(100) DEFAULT NULL,
  `issue_type` varchar(100) DEFAULT NULL,
  `message` text,
  `created_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_reports`
--

INSERT INTO `customer_reports` (`id`, `customers_id`, `order_id`, `issue_type`, `message`, `created_at`, `status`) VALUES
(1, 3, '', 'Others', 'report_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_error', '1633095672', 1),
(2, 3, '', 'Others', 'report_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_errorreport_error', '1633095695', 1),
(3, 3, 'ABM9819345', 'Submit Feedback', 'Good Nice Thankyou for your service.', '1637385723', 1);

-- --------------------------------------------------------

--
-- Table structure for table `designers_and_analysts`
--

CREATE TABLE `designers_and_analysts` (
  `id` int(11) NOT NULL,
  `ref_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(22) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(22) NOT NULL,
  `designation` enum('Fashion Designer','Fashion Analyst') NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designers_and_analysts`
--

INSERT INTO `designers_and_analysts` (`id`, `ref_id`, `name`, `email`, `phone_number`, `password`, `salt`, `designation`, `created_at`, `updated_at`, `status`) VALUES
(7, 'AMFDTy1nD5', 'Ram', 'ram@gmail.com', '9900998877', '87a415bee399db85b4d7a00f95654408', 'wvM3WfrBQ6', 'Fashion Designer', '1634977376', '1638787363', 1),
(8, 'AMFDQGwZ5p', 'Shyam', 'shyam@gmail.com', '8877667788', '9c40564c14271ab9b24c7c0f565c49eb', 'wAFBj2', 'Fashion Analyst', '1634977390', NULL, 1),
(9, 'AMFDQVB8DY', 'VIjay', 'vijay@thecolourmoon.com', '9989891918', '28efa580be532e9da35be796655dfe31', 'D197d6', 'Fashion Analyst', '1637055411', NULL, 1),
(12, 'AMFDnowAm5', 'Manoj', 'pragadasaimanoj@gmail.com', '9988770088', 'c2a31c0865157675f8d40b5a140c2362', '1jpVPH', 'Fashion Designer', '1638252941', NULL, 1),
(14, 'AMFDocAf3W', 'Manikanta Reddy Analyst', 'neelapumanikanta3@gmail.com', '8790361953', '6ca7b067ba424b5b5074e5cff0624b00', 'GmNJzv', 'Fashion Analyst', '1638787565', '1638788670', 1),
(15, 'AMFDInFEyG', 'Manikanta Reddy', 'mani.colourmoon@gmail.com', '8790361953', 'fcd73b8bceed20db8436bf26fa3d5e24', 'KLx4YO', 'Fashion Designer', '1638787656', '1638788678', 1);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(11) NOT NULL,
  `states_id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `states_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'Visakhapatnam', '1632995051', NULL),
(2, 1, 'Vizianagaram', '1632995066', '1632995074'),
(3, 2, 'Ranga reddy', '1638338328', NULL),
(4, 2, 'Warangal District', '1638780235', NULL),
(5, 2, 'nalgonda', '1639308783', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_subscriptions`
--

CREATE TABLE `email_subscriptions` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `email_subscriptions`
--

INSERT INTO `email_subscriptions` (`id`, `email`, `created_at`) VALUES
(1, 'test@test.com', '1632920739'),
(3, 'sdfghjk', '1633067481'),
(4, 'sdfghjk@fghjfk.com', '1633067486'),
(5, 'manoj@test.com', '1633067829'),
(6, 'manoj@manoj.com', '1633067861'),
(7, 'dasdasds@sadada.fv', '1633067910'),
(8, 'dsadsad@edasda.cadsa', '1633067965'),
(9, 'vijay@gmail.com', '1637818739'),
(10, 'mrudhula15mrudhu@gmail.com', '1638258263');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `message` longtext NOT NULL,
  `created_at` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fashion_desg_login_logs`
--

CREATE TABLE `fashion_desg_login_logs` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(30) NOT NULL,
  `designers_and_analysts_id` bigint(20) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fashion_desg_login_logs`
--

INSERT INTO `fashion_desg_login_logs` (`id`, `ip_address`, `designers_and_analysts_id`, `created_at`) VALUES
(1, '49.37.150.118', 2, '1634798991'),
(2, '202.53.69.163', 2, '1634806806'),
(3, '49.37.150.118', 2, '1634878959'),
(4, '202.53.69.163', 2, '1635483920'),
(5, '202.53.69.163', 7, '1635572905'),
(6, '202.53.69.163', 7, '1635572933'),
(7, '202.53.69.163', 7, '1635573054'),
(8, '202.53.69.163', 7, '1635573086'),
(9, '202.53.69.163', 7, '1635573812'),
(10, '202.53.69.163', 7, '1635573837'),
(11, '202.53.69.163', 7, '1635573839'),
(12, '202.53.69.163', 7, '1635573898'),
(13, '202.53.69.163', 7, '1635574024'),
(14, '202.53.69.163', 7, '1635574828'),
(15, '202.53.69.163', 7, '1635574972'),
(16, '49.37.144.165', 7, '1635756701'),
(17, '49.37.144.165', 7, '1635761528'),
(18, '49.37.144.165', 7, '1635829446'),
(19, '49.37.144.165', 7, '1635841798'),
(20, '49.37.146.52', 7, '1636348204'),
(21, '49.37.146.52', 7, '1636355234'),
(22, '49.37.146.52', 7, '1636370098'),
(23, '49.37.146.52', 7, '1636440598'),
(24, '49.37.146.52', 7, '1636461071'),
(25, '49.37.171.31', 7, '1636461094'),
(26, '49.37.146.52', 7, '1636462349'),
(27, '49.37.168.7', 7, '1636525883'),
(28, '49.37.168.7', 7, '1636620639'),
(29, '49.37.168.7', 7, '1636692790'),
(30, '202.53.69.163', 7, '1636697465'),
(31, '202.53.69.163', 7, '1636698842'),
(32, '49.37.168.7', 7, '1636703426'),
(33, '49.37.172.23', 7, '1636776632'),
(34, '49.37.172.23', 8, '1636781332'),
(35, '49.37.172.23', 8, '1636781758'),
(36, '49.37.169.121', 8, '1636891484'),
(37, '49.37.169.121', 7, '1636892980'),
(38, '49.37.169.121', 8, '1636895610'),
(39, '49.37.169.121', 7, '1636897089'),
(40, '49.37.148.219', 7, '1636951514'),
(41, '49.37.168.7', 7, '1636951676'),
(42, '49.37.168.7', 8, '1636958399'),
(43, '49.37.168.7', 7, '1636958547'),
(44, '49.37.168.7', 8, '1636972481'),
(45, '49.37.168.7', 7, '1636972659'),
(46, '49.37.168.7', 8, '1636980321'),
(47, '49.37.168.7', 7, '1636980839'),
(48, '49.37.175.13', 7, '1637042505'),
(49, '49.37.175.13', 8, '1637045206'),
(50, '49.37.175.13', 7, '1637047233'),
(51, '49.37.175.13', 7, '1637050567'),
(52, '49.37.175.13', 8, '1637050868'),
(53, '49.37.175.13', 7, '1637051301'),
(54, '49.37.148.219', 7, '1637054611'),
(55, '49.37.175.13', 8, '1637056947'),
(56, '49.37.175.13', 9, '1637056966'),
(57, '49.37.175.13', 7, '1637057712'),
(58, '49.37.175.13', 9, '1637057938'),
(59, '49.37.175.13', 8, '1637058055'),
(60, '49.37.175.13', 7, '1637058146'),
(61, '49.37.175.13', 9, '1637060025'),
(62, '49.37.175.13', 7, '1637060115'),
(63, '49.37.175.13', 9, '1637060180'),
(64, '49.37.175.13', 7, '1637060216'),
(65, '49.37.175.13', 7, '1637062130'),
(66, '49.37.175.13', 9, '1637062855'),
(67, '49.37.175.13', 7, '1637063076'),
(68, '49.37.148.219', 9, '1637125301'),
(69, '49.37.148.219', 7, '1637125374'),
(70, '49.37.148.219', 7, '1637386061'),
(71, '49.37.148.219', 8, '1637386089'),
(72, '49.37.148.219', 7, '1637386203'),
(73, '49.37.169.185', 8, '1637389106'),
(74, '49.37.169.185', 7, '1637389174'),
(75, '49.37.148.219', 8, '1637391262'),
(76, '49.37.148.219', 7, '1637391313'),
(77, '49.37.148.219', 8, '1637392422'),
(78, '49.37.148.219', 7, '1637392525'),
(79, '49.37.148.219', 8, '1637395574'),
(80, '49.37.148.219', 7, '1637395659'),
(81, '49.37.148.219', 8, '1637396491'),
(82, '49.37.148.219', 8, '1637571751'),
(83, '49.37.175.73', 8, '1637650515'),
(84, '49.37.175.73', 7, '1637664330'),
(85, '49.37.175.73', 8, '1637664403'),
(86, '49.37.175.73', 8, '1637729811'),
(87, '49.37.175.73', 9, '1637735103'),
(88, '49.37.175.73', 7, '1637738143'),
(89, '49.37.175.73', 8, '1637745349'),
(90, '49.37.175.73', 9, '1637745367'),
(91, '49.37.175.73', 9, '1637753025'),
(92, '49.37.175.209', 9, '1637821541'),
(93, '49.37.173.15', 9, '1637831819'),
(94, '124.123.175.209', 8, '1637935502'),
(95, '49.206.46.41', 8, '1638014530'),
(96, '49.37.175.113', 9, '1638161515'),
(97, '49.37.175.113', 7, '1638166139'),
(98, '49.37.151.25', 7, '1638247044'),
(99, '49.37.151.25', 7, '1638247081'),
(100, '49.37.151.25', 7, '1638248200'),
(101, '49.37.151.25', 7, '1638250291'),
(102, '49.37.151.25', 7, '1638250943'),
(103, '49.37.151.25', 7, '1638250954'),
(104, '49.37.151.25', 7, '1638251015'),
(105, '49.37.151.25', 8, '1638266368'),
(106, '49.37.151.25', 12, '1638266471'),
(107, '49.37.151.25', 7, '1638266525'),
(108, '49.206.48.177', 7, '1638277367'),
(109, '49.206.48.177', 7, '1638277551'),
(110, '49.206.48.177', 7, '1638277652'),
(111, '49.206.48.177', 7, '1638277810'),
(112, '49.206.48.177', 7, '1638280196'),
(113, '49.206.48.177', 7, '1638280449'),
(114, '49.206.48.177', 8, '1638280672'),
(115, '103.5.134.176', 8, '1638423483'),
(116, '103.5.134.176', 7, '1638424342'),
(117, '49.37.151.25', 12, '1638428727'),
(118, '49.37.151.25', 8, '1638429418'),
(119, '49.37.151.25', 12, '1638429554'),
(120, '202.53.69.163', 7, '1638599166'),
(121, '202.53.69.163', 9, '1638599195'),
(122, '202.53.69.163', 7, '1638599389'),
(123, '202.53.69.163', 8, '1638767628'),
(124, '49.37.151.25', 12, '1638767993'),
(125, '202.53.69.163', 12, '1638768028'),
(126, '49.37.171.3', 7, '1638779966'),
(127, '49.37.171.3', 9, '1638780139'),
(128, '202.53.69.163', 14, '1638788525'),
(129, '202.53.69.163', 15, '1638788556'),
(130, '202.53.69.163', 14, '1638788572'),
(131, '202.53.69.163', 14, '1638788685'),
(132, '202.53.69.163', 14, '1638789000'),
(133, '202.53.69.163', 15, '1638790238'),
(134, '202.53.69.163', 14, '1638791227'),
(135, '202.53.69.163', 15, '1638791598'),
(136, '202.53.69.163', 15, '1638791913'),
(137, '202.53.69.163', 14, '1638791962'),
(138, '202.53.69.163', 15, '1638792335'),
(139, '49.37.151.25', 12, '1638792542'),
(140, '202.53.69.163', 15, '1638795114'),
(141, '202.53.69.163', 14, '1638795124'),
(142, '202.53.69.163', 15, '1638795198'),
(143, '202.53.69.163', 14, '1638795598'),
(144, '202.53.69.163', 15, '1638795642'),
(145, '202.53.69.163', 14, '1638797034'),
(146, '202.53.69.163', 15, '1638797063'),
(147, '49.37.151.25', 12, '1638797102'),
(148, '49.37.151.25', 8, '1638797115'),
(149, '202.53.69.163', 14, '1638797647'),
(150, '202.53.69.163', 15, '1638797684'),
(151, '202.53.69.163', 14, '1638798201'),
(152, '202.53.69.163', 14, '1638798536'),
(153, '202.53.69.163', 15, '1638798542'),
(154, '202.53.69.163', 15, '1638879444'),
(155, '202.53.69.163', 14, '1638879454'),
(156, '202.53.69.163', 14, '1638942509'),
(157, '49.37.148.243', 8, '1638947719'),
(158, '49.37.148.243', 7, '1638948025'),
(159, '49.37.148.243', 8, '1638948052'),
(160, '49.37.148.243', 7, '1638948101'),
(161, '49.37.148.243', 12, '1638948118'),
(162, '202.53.69.163', 15, '1638954240'),
(163, '202.53.69.163', 14, '1638954928'),
(164, '202.53.69.163', 14, '1638964826'),
(165, '202.53.69.163', 14, '1639026002'),
(166, '103.5.134.179', 7, '1639302569'),
(167, '103.5.134.179', 7, '1639308119');

-- --------------------------------------------------------

--
-- Table structure for table `fashion_designer_cart`
--

CREATE TABLE `fashion_designer_cart` (
  `id` int(11) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_inventory_id` bigint(20) NOT NULL,
  `designers_and_analysts_id` bigint(20) NOT NULL,
  `subscription_transaction_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `cart_status` enum('active','in_active') NOT NULL,
  `comment` text NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fashion_designer_cart`
--

INSERT INTO `fashion_designer_cart` (`id`, `product_id`, `product_inventory_id`, `designers_and_analysts_id`, `subscription_transaction_id`, `quantity`, `unit_price`, `subtotal`, `cart_status`, `comment`, `created_at`, `updated_at`, `status`) VALUES
(20, 12, 11, 14, 73, 1, 900, 900, 'in_active', 'rg', '1638798363', '', 1),
(21, 12, 10, 8, 51, 1, 850, 850, 'in_active', 'Exchange', '1638948079', '', 1),
(22, 12, 11, 14, 83, 1, 900, 900, 'active', 'good', '1638952573', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `footer_info`
--

CREATE TABLE `footer_info` (
  `id` int(11) NOT NULL,
  `background_image` varchar(100) NOT NULL COMMENT '<font color="red">Note:</font> Upload Width: 340px; Height: 340px. This Image Will Repeat Itself',
  `subscribe_news_letter_title` varchar(100) NOT NULL,
  `subscribe_news_letter_description` text,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `footer_info`
--

INSERT INTO `footer_info` (`id`, `background_image`, `subscribe_news_letter_title`, `subscribe_news_letter_description`, `created_at`, `updated_at`) VALUES
(1, '37062b6e9416cd4a371ecf9acbc6837d.jpg', 'Special Offers For Subscribers', '<p>Subscribe to our newsletters now and stay up to date with new collections, the latest lookbooks and exclusive packages.</p>\r\n', '1632734977', '1632897042');

-- --------------------------------------------------------

--
-- Table structure for table `heights`
--

CREATE TABLE `heights` (
  `id` int(11) NOT NULL,
  `feet` int(11) NOT NULL,
  `inches` int(11) NOT NULL DEFAULT '0',
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `heights`
--

INSERT INTO `heights` (`id`, `feet`, `inches`, `created_at`, `updated_at`, `status`) VALUES
(1, 5, 0, '1633937332', NULL, 1),
(2, 5, 1, '1633937338', NULL, 1),
(3, 5, 2, '1633937344', NULL, 1),
(4, 5, 3, '1633937350', NULL, 1),
(5, 5, 4, '1633937357', NULL, 1),
(6, 5, 5, '1633937361', NULL, 1),
(7, 5, 6, '1633937367', NULL, 1),
(8, 5, 7, '1633937373', NULL, 1),
(9, 5, 8, '1633937378', NULL, 1),
(10, 5, 9, '1633937384', NULL, 1),
(11, 5, 10, '1633937392', '1633937399', 1),
(12, 5, 11, '1633937405', NULL, 1),
(13, 6, 0, '1633937410', NULL, 1),
(14, 6, 1, '1633937415', NULL, 1),
(15, 6, 2, '1633937420', NULL, 1),
(16, 6, 3, '1633937428', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `homepage_sliders`
--

CREATE TABLE `homepage_sliders` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text,
  `button_title` varchar(100) DEFAULT NULL,
  `button_link` varchar(255) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `homepage_sliders`
--

INSERT INTO `homepage_sliders` (`id`, `title`, `image`, `description`, `button_title`, `button_link`, `created_at`, `updated_at`, `status`) VALUES
(1, 'New Arrivals\r\nSpring Collection\r\n', '58b572e8c0f71d186657db485649aba6.jpg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit.\r\nQuas quis l\r\n', 'About Us', 'aboutus', '1632726156', '1633944560', 1),
(2, 'Simplify\r\nEverything', '803de3ef445c62129ad59f00eb8509a3.jpg', 'Typi non habent claritatem insitam est usus legentis in iis qui facit eorum<br />\r\nclaritatem..', 'Shop now', 'aboutus', '1632726226', '1633944565', 1),
(3, 'Best Choice\r\nCollection\r\n', 'a36f5f3716d3000ea9f45a593a03ba99.jpg', 'Discover the collection as styled by fashion icon Caroline<br />\r\nIssa in our new season campaign.', 'Read More', 'https://colormoon.in/absolutemens/packages', '1632726286', '1637817316', 1),
(4, 'test slider-1', 'dde56d929c8b53c0737ad62dab416283.jpg', '1st pic', 'test', 'https://mail.yahoo.com/d/folders/1/messages/230?guce_referrer=aHR0cHM6Ly9tYWlsLnZlcml6b25zbWFsbGJ1c2luZXNzZXNzZW50aWFscy5jb20v&guce_referrer_sig=AQAAAGkNSrMsMiW72Y2QVjVD548p0Y-WIVc6lk2Yj5di1Yey5mqiHTbRC8_87FcuNC-dPSBnYkISqJfpnbgd8EdpXWUZ0fNAfqARPHWDhAQkEy', '1638780404', '1638780572', 1);

-- --------------------------------------------------------

--
-- Table structure for table `how_it_works_images`
--

CREATE TABLE `how_it_works_images` (
  `id` int(11) NOT NULL,
  `title` text,
  `bg_image` varchar(100) DEFAULT NULL COMMENT '<font color="red">Note:</font> Upload Width: 340px; Height: 340px. This Image Will Repeat Itself',
  `image` varchar(100) DEFAULT NULL COMMENT '<font color="red">Note:</font> Upload Width: 761px; Height: 761px',
  `video_url` varchar(100) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `how_it_works_images`
--

INSERT INTO `how_it_works_images` (`id`, `title`, `bg_image`, `image`, `video_url`, `created_at`, `updated_at`) VALUES
(1, '<h2><strong>HOW&nbsp;IT</strong> <span style=\"font-family:arial,helvetica,sans-serif\">WORKS?</span></h2>\r\n', 'f31f42942d5d3e73260d8d39dd40060a.jpg', 'b8917feebc2ef1c64eba8b0b9816c459.png', 'https://www.youtube.com/embed/r96qIXOd2os', '1632727860', '1632893225');

-- --------------------------------------------------------

--
-- Table structure for table `how_it_works_points`
--

CREATE TABLE `how_it_works_points` (
  `id` int(11) NOT NULL,
  `font_awesome_icon` varchar(70) NOT NULL,
  `title` text NOT NULL,
  `image` varchar(100) NOT NULL COMMENT '<font color="red"><b>Note</b></font>Width: 761; Height: 641',
  `description` text,
  `display_order` int(11) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `how_it_works_points`
--

INSERT INTO `how_it_works_points` (`id`, `font_awesome_icon`, `title`, `image`, `description`, `display_order`, `created_at`, `updated_at`, `status`) VALUES
(1, 'fal fa-user', '<h5><strong>01</strong> &nbsp;Create your profile</h5>\r\n', '72be3ee0845f610b55ee109cd514805e.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.</p>\r\n', 1, '1632728130', '1632922606', 1),
(2, 'fal fa-gift', '<h5><strong>02</strong>&nbsp;Select Your Package</h5>\r\n', '42d32ded5b31089c572e5405c9e84749.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.</p>\r\n', 2, '1632728205', '1632921943', 1),
(3, 'fal fa-list', '<h5><strong>03</strong>&nbsp;Fill The Details</h5>\r\n', '52739a1e0b37443c92d923616355bec4.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.</p>\r\n', 3, '1632728268', '1632921951', 1),
(4, 'fal fa-thumbs-up', '<p><strong>04</strong>&nbsp;Lets Enjoy!!</p>\r\n', 'ae587dfbae61f9b85c44df67c4f348e2.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.</p>\r\n', 4, '1632728289', '1632921959', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_logs`
--

CREATE TABLE `login_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `created_at` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_logs`
--

INSERT INTO `login_logs` (`id`, `name`, `ip_address`, `created_at`) VALUES
(1, 'admin@swarnaandhra.com', '::1', '1623317523'),
(2, 'admin@absolutemens.com', '::1', '1632720663'),
(3, 'admin@absolutemens.com', '::1', '1632891061'),
(4, 'admin@absolutemens.com', '::1', '1632978747'),
(5, 'admin@absolutemens.com', '::1', '1633005591'),
(6, 'admin@absolutemens.com', '49.37.147.129', '1633063367'),
(7, 'admin@absolutemens.com', '49.37.147.129', '1633064523'),
(8, 'admin@absolutemens.com', '157.48.200.8', '1633066561'),
(9, 'admin@absolutemens.com', '202.53.69.163', '1633066901'),
(10, 'admin@absolutemens.com', '202.53.69.163', '1633070131'),
(11, 'admin@absolutemens.com', '202.53.69.163', '1633095709'),
(12, 'admin@absolutemens.com', '49.37.148.116', '1633936935'),
(13, 'admin@absolutemens.com', '49.37.148.116', '1633944527'),
(14, 'admin@absolutemens.com', '49.37.148.116', '1633953616'),
(15, 'admin@absolutemens.com', '49.37.148.116', '1633957286'),
(16, 'admin@absolutemens.com', '202.53.69.163', '1634014544'),
(17, 'admin@absolutemens.com', '202.53.69.163', '1634014711'),
(18, 'admin@absolutemens.com', '49.37.146.127', '1634037038'),
(19, 'admin@absolutemens.com', '49.37.146.127', '1634100523'),
(20, 'admin@absolutemens.com', '49.37.146.127', '1634101368'),
(21, 'admin@absolutemens.com', '49.37.146.127', '1634117825'),
(22, 'admin@absolutemens.com', '49.37.146.127', '1634126379'),
(23, 'admin@absolutemens.com', '49.37.146.127', '1634192424'),
(24, 'admin@absolutemens.com', '49.37.146.127', '1634193341'),
(25, 'admin@absolutemens.com', '49.37.149.67', '1634630630'),
(26, 'admin@absolutemens.com', '49.37.149.67', '1634639661'),
(27, 'admin@absolutemens.com', '49.37.149.67', '1634731830'),
(28, 'admin@absolutemens.com', '49.37.150.118', '1634797522'),
(29, 'admin@absolutemens.com', '202.53.69.163', '1635483871'),
(30, 'admin@absolutemens.com', '202.53.69.163', '1635486563'),
(31, 'admin@absolutemens.com', '202.53.69.163', '1635573965'),
(32, 'admin@absolutemens.com', '49.37.144.165', '1635756634'),
(33, 'admin@absolutemens.com', '49.37.144.165', '1635841779'),
(34, 'admin@absolutemens.com', '49.37.145.94', '1636090792'),
(35, 'admin@absolutemens.com', '49.37.146.178', '1636172963'),
(36, 'admin@absolutemens.com', '49.37.146.52', '1636347211'),
(37, 'admin@absolutemens.com', '49.37.146.52', '1636369558'),
(38, 'admin@absolutemens.com', '49.37.146.52', '1636433338'),
(39, 'admin@absolutemens.com', '202.53.69.163', '1636699240'),
(40, 'admin@absolutemens.com', '49.37.148.219', '1636951322'),
(41, 'admin@absolutemens.com', '49.37.148.219', '1636964779'),
(42, 'admin@absolutemens.com', '49.37.175.13', '1637047861'),
(43, 'admin@absolutemens.com', '49.37.148.219', '1637054595'),
(44, 'admin@absolutemens.com', '49.37.175.13', '1637055381'),
(45, 'admin@absolutemens.com', '49.37.175.13', '1637059985'),
(46, 'admin@absolutemens.com', '49.37.148.219', '1637124237'),
(47, 'admin@absolutemens.com', '49.37.148.219', '1637125416'),
(48, 'admin@absolutemens.com', '49.37.148.219', '1637141606'),
(49, 'admin@absolutemens.com', '49.37.148.219', '1637231387'),
(50, 'admin@absolutemens.com', '49.37.148.219', '1637235994'),
(51, 'admin@absolutemens.com', '49.37.148.219', '1637297746'),
(52, 'admin@absolutemens.com', '49.37.169.185', '1637301892'),
(53, 'admin@absolutemens.com', '49.37.169.185', '1637303427'),
(54, 'admin@absolutemens.com', '49.37.169.185', '1637318033'),
(55, 'admin@absolutemens.com', '49.37.148.219', '1637382763'),
(56, 'admin@absolutemens.com', '49.37.148.219', '1637383707'),
(57, 'admin@absolutemens.com', '49.37.169.185', '1637385348'),
(58, 'admin@absolutemens.com', '49.37.148.219', '1637385735'),
(59, 'admin@absolutemens.com', '49.37.148.219', '1637385996'),
(60, 'admin@absolutemens.com', '49.37.148.219', '1637386240'),
(61, 'admin@absolutemens.com', '49.37.148.219', '1637391337'),
(62, 'admin@absolutemens.com', '49.37.148.219', '1637392539'),
(63, 'admin@absolutemens.com', '49.37.148.219', '1637395696'),
(64, 'admin@absolutemens.com', '103.5.134.176', '1637405989'),
(65, 'admin@absolutemens.com', '49.37.148.219', '1637569961'),
(66, 'admin@absolutemens.com', '49.37.148.219', '1637580496'),
(67, 'admin@absolutemens.com', '49.37.148.219', '1637583218'),
(68, 'admin@absolutemens.com', '49.37.148.219', '1637585443'),
(69, 'admin@absolutemens.com', '103.5.134.176', '1637590085'),
(70, 'admin@absolutemens.com', '49.37.148.219', '1637645957'),
(71, 'admin@absolutemens.com', '49.37.148.219', '1637733183'),
(72, 'admin@absolutemens.com', '49.37.175.73', '1637734967'),
(73, 'admin@absolutemens.com', '49.37.148.219', '1637738557'),
(74, 'admin@absolutemens.com', '49.37.148.219', '1637738896'),
(75, 'admin@absolutemens.com', '49.37.148.219', '1637739053'),
(76, 'admin@absolutemens.com', '49.37.148.219', '1637739091'),
(77, 'admin@absolutemens.com', '49.37.148.219', '1637817262'),
(78, 'admin@absolutemens.com', '49.37.175.209', '1637822182'),
(79, 'admin@absolutemens.com', '49.37.173.151', '1637903330'),
(80, 'admin@absolutemens.com', '49.37.173.151', '1637928618'),
(81, 'admin@absolutemens.com', '124.123.175.209', '1637935692'),
(82, 'admin@absolutemens.com', '49.206.46.41', '1638014387'),
(83, 'admin@absolutemens.com', '49.37.175.113', '1638161558'),
(84, 'admin@absolutemens.com', '49.37.151.25', '1638166864'),
(85, 'admin@absolutemens.com', '49.37.151.25', '1638196931'),
(86, 'admin@absolutemens.com', '49.37.151.25', '1638252636'),
(87, 'admin@absolutemens.com', '49.37.151.25', '1638252843'),
(88, 'admin@absolutemens.com', '49.37.151.25', '1638266453'),
(89, 'admin@absolutemens.com', '49.206.48.177', '1638268419'),
(90, 'admin@absolutemens.com', '49.206.48.177', '1638270341'),
(91, 'admin@absolutemens.com', '49.37.151.25', '1638333928'),
(92, 'admin@absolutemens.com', '124.123.189.253', '1638337958'),
(93, 'admin@absolutemens.com', '103.5.134.176', '1638423245'),
(94, 'admin@absolutemens.com', '49.37.151.25', '1638423866'),
(95, 'admin@absolutemens.com', '202.53.69.163', '1638508115'),
(96, 'admin@absolutemens.com', '202.53.69.163', '1638599033'),
(97, 'admin@absolutemens.com', '202.53.69.163', '1638603763'),
(98, 'admin@absolutemens.com', '202.53.69.163', '1638604714'),
(99, 'admin@absolutemens.com', '202.53.69.163', '1638605945'),
(100, 'admin@absolutemens.com', '202.53.69.163', '1638767007'),
(101, 'admin@absolutemens.com', '202.53.69.163', '1638775171'),
(102, 'admin@absolutemens.com', '49.37.151.25', '1638780007'),
(103, 'admin@absolutemens.com', '49.37.171.3', '1638780112'),
(104, 'admin@absolutemens.com', '49.37.151.25', '1638792668'),
(105, 'admin@absolutemens.com', '49.37.171.3', '1638796222'),
(106, 'admin@absolutemens.com', '49.37.151.25', '1638797143'),
(107, 'admin@absolutemens.com', '202.53.69.163', '1638879415'),
(108, 'admin@absolutemens.com', '49.37.148.243', '1638939289'),
(109, 'admin@absolutemens.com', '202.53.69.163', '1638942477'),
(110, 'admin@absolutemens.com', '49.37.169.159', '1638943416'),
(111, 'admin@absolutemens.com', '202.53.69.163', '1638967216'),
(112, 'admin@absolutemens.com', '202.53.69.163', '1639025977'),
(113, 'admin@absolutemens.com', '49.37.175.83', '1639052956'),
(114, 'admin@absolutemens.com', '49.37.148.243', '1639200982'),
(115, 'admin@absolutemens.com', '49.37.148.243', '1639201198'),
(116, 'admin@absolutemens.com', '103.5.134.179', '1639301632'),
(117, 'admin@absolutemens.com', '103.5.134.179', '1639303051'),
(118, 'admin@absolutemens.com', '103.5.134.179', '1639303292'),
(119, 'admin@absolutemens.com', '103.5.134.179', '1639308576');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `subscription_transaction_id` bigint(20) NOT NULL,
  `fashion_manager_id` bigint(20) NOT NULL,
  `fashion_analyst_id` bigint(20) NOT NULL,
  `order_date` date NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `total` float NOT NULL,
  `final_total` float NOT NULL,
  `order_status` enum('suggested_by_fashion_analyst','approved_by_fashion_manager','rejected_by_fashion_manager','order_placed_by_admin','order_dispatched_by_vendor','order_delivered','order_cancelled','order_completed') NOT NULL,
  `reject_reason` varchar(500) DEFAULT NULL,
  `is_exchanged` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders_feedbacks`
--

CREATE TABLE `orders_feedbacks` (
  `id` int(11) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `order_unq_id` varchar(100) NOT NULL,
  `customers_id` bigint(20) NOT NULL,
  `rating` varchar(20) DEFAULT NULL,
  `feedback_message` text NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_feedbacks`
--

INSERT INTO `orders_feedbacks` (`id`, `order_id`, `order_unq_id`, `customers_id`, `rating`, `feedback_message`, `created_at`, `status`) VALUES
(7, 51, 'ABM8421934', 3, '4', 'This is a Feedback', '1638784192', 1),
(8, 47, 'ABM7015458', 3, '4', 'This is a test Feedback', '1638784622', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders_log`
--

CREATE TABLE `orders_log` (
  `id` int(11) NOT NULL,
  `order_real_id` bigint(20) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `log` text NOT NULL,
  `action_user_id` bigint(20) DEFAULT NULL,
  `order_status` varchar(100) NOT NULL DEFAULT 'admin_assigned_to_fashion',
  `table_name` varchar(100) DEFAULT NULL,
  `from_table` enum('subscription_transactions','order_products') NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `action_date_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_log`
--

INSERT INTO `orders_log` (`id`, `order_real_id`, `order_id`, `log`, `action_user_id`, `order_status`, `table_name`, `from_table`, `created_at`, `action_date_time`) VALUES
(1, 40, 'ABM4551305', 'Admin Has Assigned / Updated the Ram (AMFDTy1nD5) Fashion Manger and VIjay (AMFDQVB8DY) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638162513', '2021-11-29 10:38:33'),
(2, 40, 'ABM64639643', 'Fashion analyst-VIjay (9989891918) has forwared the fashion items containing Order id - ABM64639643 to Fashion manager-Ram (9900998877).Fashion manager-Ram(9900998877) yet to be processed to forward to the admin', 9, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638165700', '2021-11-29 11:31:40'),
(3, 40, 'ABM64639643', 'Fashion manager-Ram (9900998877) has accepted the suggestions placed by  the Fashion analyst - VIjay (9989891918) and forwarded to Administrator for further processing', 7, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638166526', '2021-11-29 11:45:26'),
(34, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638179629', '2021-11-29 15:23:49'),
(35, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638179801', '2021-11-29 15:26:41'),
(36, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638180876', '2021-11-29 15:44:36'),
(37, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638181047', '2021-11-29 15:47:27'),
(38, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638181415', '2021-11-29 15:53:35'),
(39, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638183193', '2021-11-29 16:23:13'),
(40, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638183341', '2021-11-29 16:25:41'),
(41, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638183396', '2021-11-29 16:26:36'),
(42, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638183448', '2021-11-29 16:27:28'),
(43, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638183555', '2021-11-29 16:29:15'),
(44, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638184725', '2021-11-29 16:48:45'),
(45, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638184788', '2021-11-29 16:49:48'),
(46, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638184863', '2021-11-29 16:51:03'),
(47, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638188875', '2021-11-29 17:57:55'),
(48, 44, 'ABM8942818', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638188932', '2021-11-29 17:58:52'),
(49, 45, 'ABM1372693', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638192302', '2021-11-29 18:55:02'),
(50, 45, 'ABM1372693', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638192306', '2021-11-29 18:55:06'),
(51, 46, 'ABM8989389', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638192465', '2021-11-29 18:57:45'),
(52, 47, 'ABM7015458', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638195990', '2021-11-29 19:56:30'),
(53, 47, 'ABM7015458', 'Admin Has Assigned / Updated the Ram (AMFDTy1nD5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638196969', '2021-11-29 20:12:49'),
(54, 46, 'ABM8989389', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638254666', '2021-11-30 12:14:26'),
(55, 46, 'ABM8989389', 'Admin Has Assigned / Updated the Ram (AMFDTy1nD5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638254709', '2021-11-30 12:15:09'),
(56, 46, 'ABM8989389', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638254715', '2021-11-30 12:15:15'),
(57, 47, 'ABM37265068', 'Fashion analyst-Shyam (8877667788) has forwared the fashion items containing Order id - ABM37265068 to Fashion manager-Ram (9900998877).Fashion manager-Ram(9900998877) yet to be processed to forward to the admin', 8, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638266425', '2021-11-30 15:30:25'),
(58, 47, 'ABM37265068', 'Fashion manager-Ram (9900998877) has accepted the suggestions placed by  the Fashion analyst - Shyam (8877667788) and forwarded to Administrator for further processing', 7, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638266535', '2021-11-30 15:32:15'),
(59, 4, 'ABM37265068', 'Create Order and Send it to the Vendor Action Performed By the Admin', 1, 'order_placed_by_admin', 'users', 'subscription_transactions', '1638266555', '2021-11-30 15:32:35'),
(60, 4, 'ABM37265068', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638266612', '2021-11-30 15:33:32'),
(61, 4, 'ABM37265068', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638266615', '2021-11-30 15:33:35'),
(62, 4, 'ABM37265068', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638266617', '2021-11-30 15:33:37'),
(63, 4, 'ABM37265068', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638266619', '2021-11-30 15:33:39'),
(64, 4, 'ABM37265068', 'Order \'order_placed_by_admin\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_placed_by_admin', 'vendors', 'subscription_transactions', '1638336836', '2021-12-01 11:03:56'),
(65, 50, 'ABM8714337', 'Order Has Been Created By the User and Payment is Successful', 11, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638423203', '2021-12-02 11:03:23'),
(66, 50, 'ABM8714337', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638423363', '2021-12-02 11:06:03'),
(67, 4, 'ABM37265068', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638423908', '2021-12-02 11:15:08'),
(68, 4, 'ABM37265068', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638423914', '2021-12-02 11:15:14'),
(69, 50, 'ABM84892350', 'Fashion analyst-Shyam (8877667788) has forwared the fashion items containing Order id - ABM84892350 to Fashion manager-Manoj (9988770088).Fashion manager-Manoj(9988770088) yet to be processed to forward to the admin', 8, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638424229', '2021-12-02 11:20:29'),
(70, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638424704', '2021-12-02 11:28:24'),
(71, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638424750', '2021-12-02 11:29:10'),
(72, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638424840', '2021-12-02 11:30:40'),
(73, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638424880', '2021-12-02 11:31:20'),
(74, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638425411', '2021-12-02 11:40:11'),
(75, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638425419', '2021-12-02 11:40:19'),
(76, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638425428', '2021-12-02 11:40:28'),
(77, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638425437', '2021-12-02 11:40:37'),
(78, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638425439', '2021-12-02 11:40:39'),
(79, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638425514', '2021-12-02 11:41:54'),
(80, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638425523', '2021-12-02 11:42:03'),
(81, 4, 'ABM37265068', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638425548', '2021-12-02 11:42:28'),
(82, 50, 'ABM84892350', 'Fashion manager-Manoj (9988770088) has accepted the suggestions placed by  the Fashion analyst - Shyam (8877667788) and forwarded to Administrator for further processing', 12, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638428745', '2021-12-02 12:35:45'),
(83, 50, 'ABM84892350', 'Fashion manager-Manoj (9988770088) has accepted the suggestions placed by  the Fashion analyst - Shyam (8877667788) and forwarded to Administrator for further processing', 12, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638428792', '2021-12-02 12:36:32'),
(84, 5, 'ABM84892350', 'Create Order and Send it to the Vendor Action Performed By the Admin', 1, 'order_placed_by_admin', 'users', 'subscription_transactions', '1638428801', '2021-12-02 12:36:41'),
(85, 5, 'ABM84892350', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638428821', '2021-12-02 12:37:01'),
(86, 5, 'ABM84892350', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638428823', '2021-12-02 12:37:03'),
(87, 5, 'ABM84892350', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638428825', '2021-12-02 12:37:05'),
(88, 5, 'ABM84892350', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638428828', '2021-12-02 12:37:08'),
(89, 51, 'ABM8421934', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638429142', '2021-12-02 12:42:22'),
(90, 51, 'ABM8421934', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638429151', '2021-12-02 12:42:31'),
(91, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638429354', '2021-12-02 12:45:54'),
(92, 51, 'ABM44494354', 'Fashion analyst-Shyam (8877667788) has forwared the fashion items containing Order id - ABM44494354 to Fashion manager-Manoj (9988770088).Fashion manager-Manoj(9988770088) yet to be processed to forward to the admin', 8, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638429540', '2021-12-02 12:49:00'),
(93, 51, 'ABM44494354', 'Fashion manager-Manoj (9988770088) has accepted the suggestions placed by  the Fashion analyst - Shyam (8877667788) and forwarded to Administrator for further processing', 12, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638429693', '2021-12-02 12:51:33'),
(94, 6, 'ABM44494354', 'Create Order and Send it to the Vendor Action Performed By the Admin', 1, 'order_placed_by_admin', 'users', 'subscription_transactions', '1638429888', '2021-12-02 12:54:48'),
(95, 6, 'ABM44494354', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638429952', '2021-12-02 12:55:52'),
(96, 6, 'ABM44494354', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638429954', '2021-12-02 12:55:54'),
(97, 6, 'ABM44494354', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638429957', '2021-12-02 12:55:57'),
(98, 6, 'ABM44494354', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638429959', '2021-12-02 12:55:59'),
(99, 6, 'ABM44494354', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638429986', '2021-12-02 12:56:26'),
(100, 52, 'ABM5449150', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638598990', '2021-12-04 11:53:10'),
(101, 52, 'ABM5449150', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and VIjay (AMFDQVB8DY) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638599066', '2021-12-04 11:54:26'),
(102, 52, 'ABM99711648', 'Fashion analyst-VIjay (9989891918) has forwared the fashion items containing Order id - ABM99711648 to Fashion manager-Manoj (9988770088).Fashion manager-Manoj(9988770088) yet to be processed to forward to the admin', 9, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638599328', '2021-12-04 11:58:48'),
(103, 52, 'ABM5449150', 'Admin Has Assigned / Updated the Ram (AMFDTy1nD5) Fashion Manger and VIjay (AMFDQVB8DY) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638599424', '2021-12-04 12:00:24'),
(104, 52, 'ABM99711648', 'Fashion manager-Ram (9900998877) has accepted the suggestions placed by  the Fashion analyst - VIjay (9989891918) and forwarded to Administrator for further processing', 7, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638599625', '2021-12-04 12:03:45'),
(105, 7, 'ABM99711648', 'Create Order and Send it to the Vendor Action Performed By the Admin', 1, 'order_placed_by_admin', 'users', 'subscription_transactions', '1638599666', '2021-12-04 12:04:26'),
(106, 7, 'ABM99711648', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638599793', '2021-12-04 12:06:33'),
(107, 7, 'ABM99711648', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638599888', '2021-12-04 12:08:08'),
(108, 7, 'ABM99711648', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638599922', '2021-12-04 12:08:42'),
(109, 54, 'ABM9836255', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638767411', '2021-12-06 10:40:11'),
(110, 54, 'ABM9836255', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638767563', '2021-12-06 10:42:43'),
(111, 54, 'ABM56128832', 'Fashion analyst-Shyam (8877667788) has forwared the fashion items containing Order id - ABM56128832 to Fashion manager-Manoj (9988770088).Fashion manager-Manoj(9988770088) yet to be processed to forward to the admin', 8, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638767843', '2021-12-06 10:47:23'),
(112, 54, 'ABM56128832', 'Fashion manager-Manoj (9988770088) has accepted the suggestions placed by  the Fashion analyst - Shyam (8877667788) and forwarded to Administrator for further processing', 12, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638768105', '2021-12-06 10:51:45'),
(113, 8, 'ABM56128832', 'Create Order and Send it to the Vendor Action Performed By the Admin', 1, 'order_placed_by_admin', 'users', 'subscription_transactions', '1638768325', '2021-12-06 10:55:25'),
(114, 8, 'ABM56128832', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638768543', '2021-12-06 10:59:03'),
(115, 8, 'ABM56128832', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638768546', '2021-12-06 10:59:06'),
(116, 8, 'ABM56128832', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638768549', '2021-12-06 10:59:09'),
(117, 8, 'ABM56128832', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638768552', '2021-12-06 10:59:12'),
(118, 8, 'ABM56128832', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638768565', '2021-12-06 10:59:25'),
(119, 8, 'ABM56128832', 'Order \'order_placed_by_admin\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_placed_by_admin', 'vendors', 'subscription_transactions', '1638768567', '2021-12-06 10:59:27'),
(120, 8, 'ABM56128832', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638768571', '2021-12-06 10:59:31'),
(121, 8, 'ABM56128832', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638768576', '2021-12-06 10:59:36'),
(122, 8, 'ABM56128832', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638768639', '2021-12-06 11:00:39'),
(123, 56, 'ABM4310202', 'Order Has Been Created By the User and Payment is Successful', 13, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638780084', '2021-12-06 14:11:24'),
(124, 56, 'ABM4310202', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and VIjay (AMFDQVB8DY) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638780129', '2021-12-06 14:12:09'),
(125, 67, 'ABM4477547', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638786034', '2021-12-06 15:50:34'),
(126, 67, 'ABM4477547', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and Manikanta Reddy Analyst (AMFDocAf3W) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638788711', '2021-12-06 16:35:11'),
(128, 51, 'ABM8421934', 'Exchange Order Has Been placed By the User', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638790404', '2021-12-06 17:03:24'),
(129, 67, 'ABM4477547', 'Fashion manager-Manikanta Reddy (8790361953) has reassigned this order to Fashion analyst - Manikanta Reddy Analyst (8790361953)', 15, 'reassigned_to_fashion_analyst', 'designers_and_analysts', 'subscription_transactions', '1638791205', '2021-12-06 17:16:45'),
(130, 68, 'ABM660184', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638791767', '2021-12-06 17:26:07'),
(131, 68, 'ABM660184', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and Manikanta Reddy Analyst (AMFDocAf3W) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638791901', '2021-12-06 17:28:21'),
(132, 68, 'ABM18915530', 'Fashion analyst-Manikanta Reddy Analyst (8790361953) has forwared the fashion items containing Order id - ABM18915530 to Fashion manager-Manikanta Reddy (8790361953).Fashion manager-Manikanta Reddy(8790361953) yet to be processed to forward to the admin', 14, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638792326', '2021-12-06 17:35:26'),
(133, 69, 'ABM3582457', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638793001', '2021-12-06 17:46:41'),
(134, 51, 'ABM8421934', 'Exchange Order Has Been placed By the User', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638793876', '2021-12-06 18:01:16'),
(135, 51, 'ABM8421934', 'Exchange Order Has Been placed By the User', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638793995', '2021-12-06 18:03:15'),
(136, 51, 'ABM8421934', 'Exchange Order Has Been placed By the User', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638794086', '2021-12-06 18:04:46'),
(137, 68, 'ABM18915530', 'Fashion manager-Manikanta Reddy (8790361953) has accepted the suggestions placed by  the Fashion analyst - Manikanta Reddy Analyst (8790361953) and forwarded to Administrator for further processing', 15, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638795302', '2021-12-06 18:25:02'),
(138, 51, 'ABM8421934', 'Exchange Order Has Been placed By the User', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638795472', '2021-12-06 18:27:52'),
(139, 70, 'ABM6919693', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638795551', '2021-12-06 18:29:11'),
(140, 70, 'ABM6919693', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and Manikanta Reddy Analyst (AMFDocAf3W) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638795585', '2021-12-06 18:29:45'),
(141, 67, 'ABM42526767', 'Fashion analyst-Manikanta Reddy Analyst (8790361953) has forwared the fashion items containing Order id - ABM42526767 to Fashion manager-Manikanta Reddy (8790361953).Fashion manager-Manikanta Reddy(8790361953) yet to be processed to forward to the admin', 14, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638795626', '2021-12-06 18:30:26'),
(142, 67, 'ABM42526767', 'Fashion manager-Manikanta Reddy (8790361953) has accepted the suggestions placed by  the Fashion analyst - Manikanta Reddy Analyst (8790361953) and forwarded to Administrator for further processing', 15, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638795655', '2021-12-06 18:30:55'),
(143, 10, 'ABM42526767', 'Create Order and Send it to the Vendor Action Performed By the Admin', 1, 'order_placed_by_admin', 'users', 'subscription_transactions', '1638795938', '2021-12-06 18:35:38'),
(144, 10, 'ABM42526767', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638796121', '2021-12-06 18:38:41'),
(145, 10, 'ABM42526767', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638796126', '2021-12-06 18:38:46'),
(146, 10, 'ABM42526767', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638796129', '2021-12-06 18:38:49'),
(147, 10, 'ABM42526767', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638796131', '2021-12-06 18:38:51'),
(148, 10, 'ABM42526767', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638796187', '2021-12-06 18:39:47'),
(149, 71, 'ABM8216337', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638796709', '2021-12-06 18:48:29'),
(150, 71, 'ABM8216337', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and Manikanta Reddy Analyst (AMFDocAf3W) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638796745', '2021-12-06 18:49:05'),
(151, 70, 'ABM20504284', 'Fashion analyst-Manikanta Reddy Analyst (8790361953) has forwared the fashion items containing Order id - ABM20504284 to Fashion manager-Manikanta Reddy (8790361953).Fashion manager-Manikanta Reddy(8790361953) yet to be processed to forward to the admin', 14, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638797058', '2021-12-06 18:54:18'),
(152, 70, 'ABM20504284', 'Fashion manager-Manikanta Reddy (8790361953) has accepted the suggestions placed by  the Fashion analyst - Manikanta Reddy Analyst (8790361953) and forwarded to Administrator for further processing', 15, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638797194', '2021-12-06 18:56:34'),
(153, 11, 'ABM20504284', 'Create Order and Send it to the Vendor Action Performed By the Admin', 1, 'order_placed_by_admin', 'users', 'subscription_transactions', '1638797260', '2021-12-06 18:57:40'),
(154, 72, 'ABM2381539', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638797584', '2021-12-06 19:03:04'),
(155, 72, 'ABM2381539', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and Manikanta Reddy Analyst (AMFDocAf3W) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638797638', '2021-12-06 19:03:58'),
(156, 71, 'ABM65587105', 'Fashion analyst-Manikanta Reddy Analyst (8790361953) has forwared the fashion items containing Order id - ABM65587105 to Fashion manager-Manikanta Reddy (8790361953).Fashion manager-Manikanta Reddy(8790361953) yet to be processed to forward to the admin', 14, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638797679', '2021-12-06 19:04:39'),
(157, 71, 'ABM65587105', 'Fashion manager-Manikanta Reddy (8790361953) has accepted the suggestions placed by  the Fashion analyst - Manikanta Reddy Analyst (8790361953) and forwarded to Administrator for further processing', 15, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638797743', '2021-12-06 19:05:43'),
(158, 12, 'ABM65587105', 'Create Order and Send it to the Vendor Action Performed By the Admin', 1, 'order_placed_by_admin', 'users', 'subscription_transactions', '1638797807', '2021-12-06 19:06:47'),
(159, 12, 'ABM65587105', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638797838', '2021-12-06 19:07:18'),
(160, 12, 'ABM65587105', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638797841', '2021-12-06 19:07:21'),
(161, 12, 'ABM65587105', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638797844', '2021-12-06 19:07:24'),
(162, 12, 'ABM65587105', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638797846', '2021-12-06 19:07:26'),
(163, 12, 'ABM65587105', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638797861', '2021-12-06 19:07:41'),
(164, 73, 'ABM9725284', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638798155', '2021-12-06 19:12:35'),
(165, 73, 'ABM9725284', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and Manikanta Reddy Analyst (AMFDocAf3W) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638798187', '2021-12-06 19:13:07'),
(166, 73, 'ABM9725284', 'Fashion analyst-Manikanta Reddy Analyst (8790361953) has forwared the fashion items containing Order id - ABM9725284 to Fashion manager-Manikanta Reddy (8790361953).Fashion manager-Manikanta Reddy(8790361953) yet to be processed to forward to the admin', 14, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638798454', '2021-12-06 19:17:34'),
(167, 73, 'ABM9725284', 'Fashion manager-Manikanta Reddy (8790361953) has accepted the suggestions placed by  the Fashion analyst - Manikanta Reddy Analyst (8790361953) and forwarded to Administrator for further processing', 15, 'approved_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638798557', '2021-12-06 19:19:17'),
(168, 13, 'ABM9725284', 'Create Order and Send it to the Vendor Action Performed By the Admin', 1, 'order_placed_by_admin', 'users', 'subscription_transactions', '1638798571', '2021-12-06 19:19:31'),
(169, 13, 'ABM9725284', 'Order \'order_dispatched_by_vendor\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_dispatched_by_vendor', 'vendors', 'subscription_transactions', '1638798599', '2021-12-06 19:19:59'),
(170, 13, 'ABM9725284', 'Order \'order_delivered\' action performed by Vendor JACK(SHP764664), oneshop@gmail.com, 9988776655', 2, 'order_delivered', 'vendors', 'subscription_transactions', '1638798615', '2021-12-06 19:20:15'),
(171, 13, 'ABM9725284', 'Order Completed Action Performed By the Admin', 1, 'order_completed', 'users', 'subscription_transactions', '1638798623', '2021-12-06 19:20:23'),
(172, 51, 'ABM8421934', 'Exchange Order Has Been placed By the User', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638855056', '2021-12-07 11:00:56'),
(173, 76, 'ABM755164', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638881416', '2021-12-07 18:20:16'),
(174, 77, 'ABM7639289', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638881501', '2021-12-07 18:21:41'),
(175, 72, 'ABM2381539', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and Manikanta Reddy Analyst (AMFDocAf3W) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638945100', '2021-12-08 12:01:40'),
(176, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638945763', '2021-12-08 12:12:43'),
(177, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638946154', '2021-12-08 12:19:14'),
(178, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638946291', '2021-12-08 12:21:31'),
(180, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638946373', '2021-12-08 12:22:53'),
(182, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638946375', '2021-12-08 12:22:55'),
(184, 81, 'ABM6140209', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638946419', '2021-12-08 12:23:39'),
(185, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638946541', '2021-12-08 12:25:41'),
(187, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638946550', '2021-12-08 12:25:50'),
(189, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638946630', '2021-12-08 12:27:10'),
(190, 81, 'ABM6140209', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and Manikanta Reddy Analyst (AMFDocAf3W) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638946823', '2021-12-08 12:30:23'),
(191, 51, 'ABM8421934', 'Admin Has Assigned / Updated the Manoj (AMFDnowAm5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638947440', '2021-12-08 12:40:40'),
(192, 51, 'ABM44494354', 'Fashion manager-Manoj (9988770088) has rejected the suggestions placed by  the Fashion analyst - Shyam (8877667788) and the order has been reverted back to Fashion analyst - Shyam (8877667788)', 12, 'rejected_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638947440', '2021-12-08 12:40:40'),
(193, 51, 'ABM8421934', 'Fashion analyst-Shyam (8877667788) has forwared the fashion items containing Order id - ABM8421934 to Fashion manager-Manoj (9988770088).Fashion manager-Manoj(9988770088) yet to be processed to forward to the admin', 8, 'suggested_from_fashion_designer', 'designers_and_analysts', 'order_products', '1638948083', '2021-12-08 12:51:23'),
(194, 83, 'ABM5478461', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638952221', '2021-12-08 14:00:21'),
(195, 83, 'ABM5478461', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and Manikanta Reddy Analyst (AMFDocAf3W) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638952250', '2021-12-08 14:00:50'),
(196, 84, 'ABM9332070', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638953274', '2021-12-08 14:17:54'),
(197, 47, 'ABM7015458', 'Exchange Order Has Been placed By the User', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638954386', '2021-12-08 14:36:26'),
(198, 47, 'ABM7015458', 'Admin Has Assigned / Updated the Ram (AMFDTy1nD5) Fashion Manger and Shyam (AMFDQGwZ5p) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1638954531', '2021-12-08 14:38:51'),
(199, 47, 'ABM37265068', 'Fashion manager-Ram (9900998877) has rejected the suggestions placed by  the Fashion analyst - Shyam (8877667788) and the order has been reverted back to Fashion analyst - Shyam (8877667788)', 7, 'rejected_by_fashion_manager', 'designers_and_analysts', 'order_products', '1638954532', '2021-12-08 14:38:52'),
(200, 98, 'ABM466526', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962418', '2021-12-08 16:50:18'),
(201, 99, 'ABM1058298', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962511', '2021-12-08 16:51:51'),
(202, 88, 'ABM7712047', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962644', '2021-12-08 16:54:04'),
(203, 88, 'ABM7712047', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962658', '2021-12-08 16:54:18'),
(204, 88, 'ABM7712047', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962661', '2021-12-08 16:54:21'),
(205, 88, 'ABM7712047', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962662', '2021-12-08 16:54:22'),
(206, 88, 'ABM7712047', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962701', '2021-12-08 16:55:01'),
(207, 88, 'ABM7712047', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962703', '2021-12-08 16:55:03'),
(208, 100, 'ABM3824635', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962708', '2021-12-08 16:55:08'),
(209, 101, 'ABM4019533', 'Order Has Been Created By the User and Payment is Successful', 12, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638962828', '2021-12-08 16:57:08'),
(210, 102, 'ABM7109328', 'Order Has Been Created By the User and Payment is Successful', 3, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1638965265', '2021-12-08 17:37:45'),
(211, 102, 'ABM7109328', 'Admin Has marked this order ABM7109328 as Cancelled', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1639201787', '2021-12-11 11:19:47'),
(212, 104, 'ABM3056267', 'Order Has Been Created By the User and Payment is Successful', 6, 'admin_assigned_to_fashion', 'customers', 'subscription_transactions', '1639300671', '2021-12-12 14:47:51'),
(213, 104, 'ABM3056267', 'Admin Has Assigned / Updated the Manikanta Reddy (AMFDInFEyG) Fashion Manger and VIjay (AMFDQVB8DY) Fashion Analyst to the Order', 1, 'admin_assigned_to_fashion', 'users', 'subscription_transactions', '1639301749', '2021-12-12 15:05:49');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_inventory_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` float NOT NULL,
  `subtotal` float NOT NULL,
  `comment` text NOT NULL,
  `approved_by_fashion_manager` enum('no','yes') NOT NULL,
  `order_status` enum('order_placed_by_admin','order_dispatched_by_vendor','order_delivered','order_cancelled_by_vendor') NOT NULL DEFAULT 'order_placed_by_admin',
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `title` text NOT NULL,
  `no_of_items` int(11) DEFAULT NULL,
  `min_styles` int(11) DEFAULT NULL,
  `max_styles` int(11) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `image` varchar(100) NOT NULL COMMENT '<font color="red"><b>Note : </b></font>Width: 600px; Height: 800px',
  `duration` int(11) NOT NULL,
  `duration_in_words` varchar(100) NOT NULL,
  `price` float(10,2) NOT NULL,
  `discount_amount` float(10,2) NOT NULL DEFAULT '0.00',
  `delivery_charges` float(10,2) NOT NULL DEFAULT '0.00',
  `subscription_features` text,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `slug`, `title`, `no_of_items`, `min_styles`, `max_styles`, `short_description`, `description`, `image`, `duration`, `duration_in_words`, `price`, `discount_amount`, `delivery_charges`, `subscription_features`, `created_at`, `updated_at`, `status`) VALUES
(1, 'package-one-collection', '<h2><strong>Package One</strong></h2>\r\n\r\n<h2><span style=\"font-family:arial,helvetica,sans-serif\">Collection</span></h2>\r\n', 4, 2, 3, '<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt.</p>\r\n', '<h5>Title Here</h5>\r\n\r\n<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n\r\n<ul>\r\n	<li>Rent 4 items at a time</li>\r\n	<li>10,000+ styles from 400 + desginers</li>\r\n	<li>Wear items with retail value with up to 500</li>\r\n	<li>Monthly shipments worth upto 2000</li>\r\n</ul>\r\n', 'f4df402307eec2f76066ce97da91f990.jpg', 30, 'Month', 5000.00, 0.00, 0.00, '<ul>\r\n	<li>Rent 4 items at a time</li>\r\n	<li>10,000+ styles from 400 + desginers</li>\r\n	<li>Wear items with retail value with up to 500</li>\r\n	<li>Monthly shipments worth upto 2000</li>\r\n</ul>\r\n', '1632726633', '1639303250', 1),
(2, 'package-two-collection', '<h2><strong>Package Two</strong></h2>\r\n\r\n<h2><span style=\"font-family:arial,helvetica,sans-serif\">Collection</span></h2>\r\n', 4, 1, 3, '<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt.</p>\r\n', '<h5>Title Here</h5>\r\n\r\n<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n\r\n<ul>\r\n	<li>Rent 4 items at a time</li>\r\n	<li>10,000+ styles from 400 + desginers</li>\r\n	<li>Wear items with retail value with up to 500</li>\r\n	<li>Monthly shipments worth upto 2000</li>\r\n</ul>\r\n', '7c4f69d441928670a646564489b1ab3f.jpg', 30, 'Month', 8000.00, 0.00, 0.00, '<ul>\r\n	<li>Rent 4 items at a time</li>\r\n	<li>10,000+ styles from 400 + desginers</li>\r\n	<li>Wear items with retail value with up to 500</li>\r\n	<li>Monthly shipments worth upto 2000</li>\r\n</ul>\r\n', '1632726669', '1634194936', 1),
(3, 'package-three-collection', '<h2><strong>Package Three</strong></h2>\r\n\r\n<h2><span style=\"font-family:arial,helvetica,sans-serif\">Collection</span></h2>\r\n', 4, 1, 3, '<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt.</p>\r\n', '<h5>Title Here</h5>\r\n\r\n<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n\r\n<ul>\r\n	<li>Rent 4 items at a time</li>\r\n	<li>10,000+ styles from 400 + desginers</li>\r\n	<li>Wear items with retail value with up to 500</li>\r\n	<li>Monthly shipments worth upto 2000</li>\r\n</ul>\r\n', 'dc29bc351bf57424791f8d2ded982801.jpg', 30, 'Month', 10000.00, 0.00, 0.00, '<ul>\r\n	<li>Rent 4 items at a time</li>\r\n	<li>10,000+ styles from 400 + desginers</li>\r\n	<li>Wear items with retail value with up to 500</li>\r\n	<li>Monthly shipments worth upto 2000</li>\r\n</ul>\r\n', '1632726710', '1634194939', 1),
(4, 'package-four-collection', '<p><span style=\"font-size:28px\"><strong>Package Four</strong></span><br />\r\n<span style=\"font-size:24px\">Collection</span></p>\r\n', 4, 1, 4, '<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt.</p>\r\n', '<p>Title Here<br />\r\nLorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n\r\n<p>Rent 4 items at a time<br />\r\n10,000+ styles from 400 + desginers<br />\r\nWear items with retail value with up to 500<br />\r\nMonthly shipments worth upto 2000</p>\r\n', 'bac1877bdef1222ffd339719f1e1950c.jpg', 1, 'one', 5000.00, 1000.00, 250.00, '<p>Rent 4 items at a time<br />\r\n10,000+ styles from 400 + desginers<br />\r\nWear items with retail value with up to 500<br />\r\nMonthly shipments worth upto 2000</p>\r\n', '1638781832', '1638782106', 1),
(6, 'package-five-collection', '<p><strong><span style=\"font-size:28px\">Package Five</span></strong><br />\r\n<span style=\"font-size:24px\">Collection</span></p>\r\n', 5, 1, 5, '<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt.</p>\r\n', '<p>Title Here<br />\r\nLorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n\r\n<p>Rent 4 items at a time<br />\r\n10,000+ styles from 400 + desginers<br />\r\nWear items with retail value with up to 500<br />\r\nMonthly shipments worth upto 2000</p>\r\n', '018a2c71b3a97b94943ec5b2f806ee78.jpg', 2, 'TWO', 6000.00, 1000.00, 250.00, '<p>Rent 4 items at a time<br />\r\n10,000+ styles from 400 + desginers<br />\r\nWear items with retail value with up to 500<br />\r\nMonthly shipments worth upto 2000</p>\r\n', '1638781982', '1638781995', 1);

-- --------------------------------------------------------

--
-- Table structure for table `package_page_banners`
--

CREATE TABLE `package_page_banners` (
  `id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL COMMENT '<font color="red"><b>Note : </b></font> IRecomended Image Size: 1140 x 331',
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package_page_banners`
--

INSERT INTO `package_page_banners` (`id`, `image`, `created_at`, `updated_at`, `status`) VALUES
(2, '8ecf085347d3d853737028929e66bf8d.jpg', '1634015135', NULL, 1),
(3, '9e86f9dcb1a6d7db5c0405ae14b8792a.jpg', '1634015141', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pants_sizes`
--

CREATE TABLE `pants_sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(10) NOT NULL,
  `type` enum('Casual Pants','Formal Pants','Jeans Pants') NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pants_sizes`
--

INSERT INTO `pants_sizes` (`id`, `size`, `type`, `created_at`, `updated_at`, `status`) VALUES
(1, '28', 'Casual Pants', '1633071353', NULL, 1),
(2, '30', 'Casual Pants', '1633071360', NULL, 1),
(3, '32', 'Casual Pants', '1633071365', NULL, 1),
(4, '34', 'Casual Pants', '1633071370', NULL, 1),
(5, '28', 'Formal Pants', '1633071379', NULL, 1),
(6, '30', 'Formal Pants', '1633071384', NULL, 1),
(7, '32', 'Formal Pants', '1633071389', NULL, 1),
(8, '34', 'Formal Pants', '1633071394', NULL, 1),
(9, '28', 'Jeans Pants', '1633071402', NULL, 1),
(10, '30', 'Jeans Pants', '1633071406', NULL, 1),
(11, '32', 'Jeans Pants', '1633071410', NULL, 1),
(12, '34', 'Jeans Pants', '1633071418', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policy`
--

CREATE TABLE `privacy_policy` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacy_policy`
--

INSERT INTO `privacy_policy` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Privacy Policy', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas</p>\r\n', '1639206854', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `vendors_id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text,
  `categories_id` bigint(20) NOT NULL,
  `sub_categories_id` int(11) NOT NULL,
  `sizes_id` bigint(20) DEFAULT NULL,
  `quantity_available` bigint(20) DEFAULT NULL,
  `product_id` varchar(30) DEFAULT NULL,
  `brand` varchar(100) NOT NULL,
  `brand_code` varchar(30) NOT NULL COMMENT 'Ex: LP for  Louis Philip, Used for Generating Product Code',
  `in_stock` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `vendors_id`, `title`, `description`, `categories_id`, `sub_categories_id`, `sizes_id`, `quantity_available`, `product_id`, `brand`, `brand_code`, `in_stock`, `created_at`, `updated_at`, `status`) VALUES
(11, 2, 'Men Regular Fit Checkered Casual Shirt', 'Men Regular Fit Checkered Casual Shirt', 1, 1, 0, 0, 'RD3267089', 'Roadster', 'RD', 1, '1637904576', '1637904913', 1),
(12, 2, 'Color Block Men Round Neck Blue T Shirt', 'Color Block Men Round Neck Blue T-Shirt', 2, 6, 0, 0, 'LVI3834481', 'Levi', 'LVI', 1, '1637910401', '1639119867', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_images`
--

CREATE TABLE `products_images` (
  `id` int(11) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_images`
--

INSERT INTO `products_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`, `status`) VALUES
(14, 12, 'uploads/products/products-716ef531e99296fc8a060b73a5494b4647a43111.jpeg', '1637910550', '1637910550', 1),
(15, 12, 'uploads/products/products-5c24e6c4324df94715f6f2bc3c288684827787ee.jpeg', '1637910557', '1637910557', 1),
(16, 12, 'uploads/products/products-f789cf0fd299fde6be23357714eef5687a3a5251.jpeg', '1637910563', '1637910563', 1),
(17, 11, 'uploads/products/products-b98e6332900e2a550a638323e9eb3263afb5f94a.jpeg', '1637910581', '1637910581', 1),
(18, 11, 'uploads/products/products-05b212b8e4000d041887c78e34301848a35fe3bd.jpeg', '1637910589', '1637910589', 1),
(19, 11, 'uploads/products/products-d6753411acf63b24d2dd7483b5ff2d5b6032f6fb.jpeg', '1637910595', '1637910595', 1),
(20, 14, 'uploads/products/products-37a8873f17d5b8dd738ae515e485b00d38ecd0ff.jpeg', '1638956052', '1638956052', 1),
(21, 14, 'uploads/products/products-acbe61af442ab9a9012ab0c146d03c459e3e2700.jpeg', '1638956063', '1638956063', 1),
(22, 13, 'uploads/products/products-03d378354b6efa946444d9999f958eee07f2e29f.jpg', '1638956117', '1638956117', 1),
(23, 13, 'uploads/products/products-9cdd7621b96ea5ee9aa1883705713064fc75e40a.jpg', '1638956123', '1638956123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_inventory`
--

CREATE TABLE `products_inventory` (
  `id` int(11) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `sizes_id` bigint(20) NOT NULL,
  `size` varchar(10) NOT NULL,
  `quantity` int(11) NOT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_inventory`
--

CREATE TABLE `product_inventory` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `price` float NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL,
  `size_id` int(11) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_inventory`
--

INSERT INTO `product_inventory` (`id`, `vendor_id`, `product_id`, `price`, `quantity`, `size_id`, `created_at`, `updated_at`, `status`) VALUES
(2, 2, 11, 600, 2, 1, '', '', 1),
(5, 2, 11, 500, 6, 2, '', '', 1),
(6, 2, 11, 600, 0, 3, '', '', 1),
(7, 2, 11, 650, 5, 4, '', '', 1),
(8, 2, 11, 450, 6, 5, '', '', 1),
(9, 2, 12, 800, 27, 7, '', '', 1),
(10, 2, 12, 850, 8, 8, '', '', 1),
(11, 2, 12, 900, 0, 9, '', '', 1),
(12, 2, 12, 950, 3, 10, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `return_policy`
--

CREATE TABLE `return_policy` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `return_policy`
--

INSERT INTO `return_policy` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Return Policy', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas</p>\r\n', '1639206986', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `page_name` varchar(100) NOT NULL,
  `seo_title` varchar(100) NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_description` text NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `page_name`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `status`) VALUES
(1, 'how_it_works', 'How It Works?', 'How It Works?', 'How It Works?', '1639206221', '1639207930', 1),
(2, 'contact_us', 'Contact Us', 'Contact Us', 'Contact Us', '1639206203', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shirts_sizes`
--

CREATE TABLE `shirts_sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(20) NOT NULL,
  `type` enum('Shirt','T-Shirt') NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shirts_sizes`
--

INSERT INTO `shirts_sizes` (`id`, `size`, `type`, `created_at`, `updated_at`, `status`) VALUES
(1, 'S', 'Shirt', '1633070998', '1633071019', 1),
(2, 'M', 'Shirt', '1633071006', '1633071023', 1),
(3, 'L', 'Shirt', '1633071014', NULL, 1),
(4, 'XL', 'Shirt', '1633071033', NULL, 1),
(5, 'XS', 'T-Shirt', '1633071048', '1633071136', 1),
(6, 'S', 'T-Shirt', '1633071116', '1633071131', 1),
(7, 'M', 'T-Shirt', '1633071125', NULL, 1),
(8, 'L', 'T-Shirt', '1633071143', NULL, 1),
(9, 'XL', 'T-Shirt', '1633071150', NULL, 1),
(10, 'XXL', 'T-Shirt', '1633071156', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shoes_sizes`
--

CREATE TABLE `shoes_sizes` (
  `id` int(11) NOT NULL,
  `size` varchar(23) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shoes_sizes`
--

INSERT INTO `shoes_sizes` (`id`, `size`, `created_at`, `updated_at`, `status`) VALUES
(1, '8', '1633071513', NULL, 1),
(2, '9', '1633071517', NULL, 1),
(3, '10', '1633071522', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(100) DEFAULT NULL,
  `logo` varchar(100) NOT NULL COMMENT '<font color="red">Note:</font> Upload Width: 117px; Height: 60px',
  `favicon` varchar(100) NOT NULL COMMENT '<font color="red">Note:</font> Upload Width: 100px; Height: 115px',
  `contact_number` varchar(100) NOT NULL,
  `secondary_contact_number` varchar(100) DEFAULT NULL,
  `phone_number` varchar(20) NOT NULL,
  `secondary_phone_numer` varchar(20) DEFAULT NULL,
  `contact_email` varchar(60) DEFAULT NULL,
  `secondary_email` varchar(60) DEFAULT NULL,
  `address` text,
  `about_site` text NOT NULL,
  `google_maps` text,
  `seo_title` text,
  `seo_keywords` text,
  `seo_description` text,
  `smtp_host` varchar(100) DEFAULT NULL,
  `smtp_port` varchar(100) DEFAULT NULL,
  `smtp_user_name` varchar(100) DEFAULT NULL,
  `smtp_password` varchar(100) DEFAULT NULL,
  `smtp_type` varchar(100) DEFAULT NULL,
  `razorpay_key` varchar(100) DEFAULT NULL,
  `razorpay_secret` varchar(100) DEFAULT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_name`, `logo`, `favicon`, `contact_number`, `secondary_contact_number`, `phone_number`, `secondary_phone_numer`, `contact_email`, `secondary_email`, `address`, `about_site`, `google_maps`, `seo_title`, `seo_keywords`, `seo_description`, `smtp_host`, `smtp_port`, `smtp_user_name`, `smtp_password`, `smtp_type`, `razorpay_key`, `razorpay_secret`, `created_at`, `updated_at`) VALUES
(1, 'Absolutemens', 'bb73daa4583eadd0c58e91a8656e3f6b.svg', '63704fb734ea171b47896d2e6364f453.png', '(+91) 123 456 7890', '', '(+91) 123 456 7890', '', 'info@absolutemens.com', '', '<p>5th Lane, Near Tilak Showroom, Dwarkanagar, Visakhapatnam</p>\r\n', '<p>We denounce with righteous indi gnation and dislike men who are so beguiled and demoralized by the charms of pleasure of your moment, so blinded by desire those who fail weakness.</p>\r\n', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12613.47608654031!2d-122.50864139156641!3d37.781390759825626!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808587b42ebc4ef9%3A0x5e73c1ad9c8a015a!2sSutro%20Heights!5e0!3m2!1sen!2sus!4v1623317758682!5m2!1sen!2sus\" width=\"600\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>', 'Absolute Mens Fashion', 'Absolute Mens Fashion', 'Absolute Mens Fashion', 'smtp-relay.sendinblue.com', '465', 'ksriraviteja@gmail.com', 'zcL1CATF4780vbK2', 'ssl', 'rzp_test_fM6YiRRPkO02s2', '2LfiWWVUoMLQF9PfTeAZQhZ0', '', '1639053367');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `category_id`, `title`, `created_at`, `updated_at`, `status`) VALUES
(2, 1, 'M', '1637903694', '', 1),
(3, 1, 'XL', '1637903701', '', 1),
(4, 1, 'XXL', '1637903708', '', 1),
(5, 1, 'S', '1637903716', '', 1),
(7, 2, 'M', '1637903756', '', 1),
(8, 2, 'L', '1637903767', '', 1),
(9, 2, 'XL', '1637903775', '', 1),
(10, 2, 'XXL', '1637903780', '', 1),
(11, 3, '28', '1637903788', '', 1),
(12, 3, '30', '1637903795', '', 1),
(13, 3, '32', '1637903800', '', 1),
(14, 3, '34', '1637903805', '', 1),
(15, 3, '36', '1637903809', '', 1),
(16, 3, '38', '1637903814', '', 1),
(17, 3, '40', '1637903820', '', 1),
(18, 4, '6', '1637903828', '', 1),
(19, 4, '7', '1637903832', '', 1),
(20, 4, '8', '1637903837', '', 1),
(21, 4, '9', '1637903841', '', 1),
(22, 4, '10', '1637903845', '', 1),
(23, 4, '12', '1637903850', '', 1),
(24, 1, 'L', '1638780010', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id` int(11) NOT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `google` varchar(100) DEFAULT NULL,
  `youtube` varchar(100) DEFAULT NULL,
  `linkedin` varchar(100) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `pinterest` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`id`, `facebook`, `twitter`, `google`, `youtube`, `linkedin`, `instagram`, `pinterest`) VALUES
(1, 'https://www.facebook.com', 'https://www.twitter.com', 'dasdasdasdas', 'dsadasdas', 'dsadasdsad', 'https://www.instagram.com', 'https://www.pinintrest.com');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Andhra Pradesh', '1632994941', NULL),
(2, 'Telanagana', '1638338310', '1638780197'),
(3, 'Chattisgarh', '1638423285', NULL),
(4, 'Tamil Nadu', '1638780177', NULL),
(6, 'Kerala', '1638780209', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_inventory_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `type` enum('','credit','debit') NOT NULL,
  `comment` varchar(355) NOT NULL,
  `date` date NOT NULL,
  `balance` int(11) NOT NULL,
  `action_for` varchar(100) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `product_id`, `product_inventory_id`, `quantity`, `type`, `comment`, `date`, `balance`, `action_for`, `created_at`, `updated_at`, `status`) VALUES
(37, 11, 2, 5, 'credit', '5 stock credited on 26-11-2021', '2021-11-26', 5, 'initial_stock', '1637909235', '', 1),
(38, 11, 3, 33, 'credit', '33 stock credited on 26-11-2021', '2021-11-26', 33, 'initial_stock', '1637909805', '', 1),
(39, 11, 5, 6, 'credit', '6 stock credited on 26-11-2021', '2021-11-26', 6, 'initial_stock', '1637909896', '', 1),
(40, 11, 6, 5, 'credit', '5 stock credited on 26-11-2021', '2021-11-26', 5, 'initial_stock', '1637909907', '', 1),
(41, 11, 7, 5, 'credit', '5 stock credited on 26-11-2021', '2021-11-26', 5, 'initial_stock', '1637910301', '', 1),
(42, 11, 8, 6, 'credit', '6 stock credited on 26-11-2021', '2021-11-26', 6, 'initial_stock', '1637910312', '', 1),
(43, 12, 9, 5, 'credit', '5 stock credited on 26-11-2021', '2021-11-26', 5, 'initial_stock', '1637910429', '', 1),
(44, 12, 10, 6, 'credit', '6 stock credited on 26-11-2021', '2021-11-26', 6, 'initial_stock', '1637910437', '', 1),
(45, 12, 11, 7, 'credit', '7 stock credited on 26-11-2021', '2021-11-26', 7, 'initial_stock', '1637910445', '', 1),
(46, 12, 12, 3, 'credit', '3 stock credited on 26-11-2021', '2021-11-26', 3, 'initial_stock', '1637910452', '', 1),
(47, 12, 9, 10, 'credit', '10 stock credited on 26-11-2021', '2021-11-26', 15, 'credit_from_vendor', '1637919803', '', 1),
(48, 12, 9, 5, 'debit', '5 stock debited on 26-11-2021', '2021-11-26', 10, 'debit_from_vendor', '1637919966', '', 1),
(49, 12, 9, 2, 'debit', '2 stock debited on 26-11-2021', '2021-11-26', 8, 'debit_from_vendor', '1637919980', '', 1),
(50, 12, 10, 5, 'credit', '5 stock credited on 26-11-2021', '2021-11-26', 11, 'credit_from_vendor', '1637920368', '', 1),
(51, 11, 2, 1, 'debit', '1 stock debited on 29-11-2021', '2021-11-29', 4, 'debit_from_increasing_quantity_in_cart', '1638165413', '', 1),
(52, 11, 2, 1, 'debit', '1 stock debited on 29-11-2021', '2021-11-29', 3, 'debit_from_increasing_quantity_in_cart', '1638165515', '', 1),
(53, 11, 2, 1, 'credit', '1 stock credited on 29-11-2021', '2021-11-29', 4, 'credit_from_reducing_quantity_in_cart', '1638165518', '', 1),
(54, 11, 2, 1, 'debit', '1 stock debited on 29-11-2021', '2021-11-29', 3, 'debit_from_increasing_quantity_in_cart', '1638165524', '', 1),
(55, 11, 2, 1, 'credit', '1 stock credited on 29-11-2021', '2021-11-29', 4, 'credit_from_reducing_quantity_in_cart', '1638165526', '', 1),
(56, 11, 2, 1, 'debit', '1 stock debited on 30-11-2021', '2021-11-30', 3, 'debit_from_increasing_quantity_in_cart', '1638266401', '', 1),
(57, 12, 10, 1, 'debit', '1 stock debited on 30-11-2021', '2021-11-30', 10, 'debit_from_increasing_quantity_in_cart', '1638266409', '', 1),
(58, 11, 2, 1, 'debit', '1 stock debited on 30-11-2021', '2021-11-30', 2, 'debit_from_increasing_quantity_in_cart', '1638266416', '', 1),
(59, 11, 2, 1, 'credit', '1 stock credited on 30-11-2021', '2021-11-30', 3, 'credit_from_reducing_quantity_in_cart', '1638266419', '', 1),
(60, 12, 9, 2, 'credit', '2 stock credited on 01-12-2021', '2021-12-01', 10, 'credit_from_vendor', '1638337261', '', 1),
(61, 12, 9, 10, 'credit', '10 stock credited on 01-12-2021', '2021-12-01', 20, 'credit_from_vendor', '1638337462', '', 1),
(62, 11, 2, 1, 'debit', '1 stock debited on 01-12-2021', '2021-12-01', 2, 'debit_from_vendor', '1638337516', '', 1),
(63, 11, 2, 1, 'debit', '1 stock debited on 02-12-2021', '2021-12-02', 1, 'debit_from_increasing_quantity_in_cart', '1638423612', '', 1),
(64, 12, 10, 1, 'debit', '1 stock debited on 02-12-2021', '2021-12-02', 9, 'debit_from_increasing_quantity_in_cart', '1638424176', '', 1),
(65, 12, 9, 2, 'debit', '2 stock debited on 02-12-2021', '2021-12-02', 18, 'debit_from_vendor', '1638428996', '', 1),
(66, 12, 9, 10, 'credit', '10 stock credited on 02-12-2021', '2021-12-02', 28, 'credit_from_vendor', '1638429048', '', 1),
(67, 11, 2, 1, 'debit', '1 stock debited on 02-12-2021', '2021-12-02', 0, 'debit_from_increasing_quantity_in_cart', '1638429525', '', 1),
(68, 12, 10, 1, 'debit', '1 stock debited on 02-12-2021', '2021-12-02', 8, 'debit_from_increasing_quantity_in_cart', '1638429532', '', 1),
(69, 12, 9, 1, 'debit', '1 stock debited on 04-12-2021', '2021-12-04', 27, 'debit_from_increasing_quantity_in_cart', '1638599286', '', 1),
(70, 12, 9, 1, 'debit', '1 stock debited on 04-12-2021', '2021-12-04', 26, 'debit_from_increasing_quantity_in_cart', '1638599315', '', 1),
(71, 12, 9, 1, 'credit', '1 stock credited on 04-12-2021', '2021-12-04', 27, 'credit_from_reducing_quantity_in_cart', '1638599323', '', 1),
(72, 11, 6, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 4, 'debit_from_increasing_quantity_in_cart', '1638767763', '', 1),
(73, 12, 11, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 6, 'debit_from_increasing_quantity_in_cart', '1638767775', '', 1),
(74, 11, 6, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 3, 'debit_from_increasing_quantity_in_cart', '1638767836', '', 1),
(75, 11, 6, 1, 'credit', '1 stock credited on 06-12-2021', '2021-12-06', 4, 'credit_from_reducing_quantity_in_cart', '1638767837', '', 1),
(76, 11, 6, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 3, 'debit_from_increasing_quantity_in_cart', '1638789837', '', 1),
(77, 12, 11, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 5, 'debit_from_increasing_quantity_in_cart', '1638789990', '', 1),
(78, 11, 6, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 2, 'debit_from_increasing_quantity_in_cart', '1638791979', '', 1),
(79, 12, 11, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 4, 'debit_from_increasing_quantity_in_cart', '1638791984', '', 1),
(80, 11, 6, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 1, 'debit_from_increasing_quantity_in_cart', '1638795157', '', 1),
(81, 12, 11, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 3, 'debit_from_increasing_quantity_in_cart', '1638795621', '', 1),
(82, 11, 6, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 0, 'debit_from_increasing_quantity_in_cart', '1638797050', '', 1),
(83, 12, 11, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 2, 'debit_from_increasing_quantity_in_cart', '1638797673', '', 1),
(84, 12, 10, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 7, 'debit_from_increasing_quantity_in_cart', '1638797705', '', 1),
(85, 12, 11, 1, 'debit', '1 stock debited on 06-12-2021', '2021-12-06', 1, 'debit_from_increasing_quantity_in_cart', '1638798363', '', 1),
(86, 11, 2, 1, 'credit', '1 stock credited on 08-12-2021', '2021-12-08', 1, 'credit_for_order_rejection_by_fashion_manager', '1638947440', '', 1),
(87, 12, 10, 1, 'credit', '1 stock credited on 08-12-2021', '2021-12-08', 8, 'credit_for_order_rejection_by_fashion_manager', '1638947440', '', 1),
(88, 12, 10, 1, 'debit', '1 stock debited on 08-12-2021', '2021-12-08', 7, 'debit_from_increasing_quantity_in_cart', '1638948079', '', 1),
(89, 12, 11, 1, 'debit', '1 stock debited on 08-12-2021', '2021-12-08', 0, 'debit_from_increasing_quantity_in_cart', '1638952573', '', 1),
(90, 11, 2, 1, 'credit', '1 stock credited on 08-12-2021', '2021-12-08', 2, 'credit_for_order_rejection_by_fashion_manager', '1638954532', '', 1),
(91, 12, 10, 1, 'credit', '1 stock credited on 08-12-2021', '2021-12-08', 8, 'credit_for_order_rejection_by_fashion_manager', '1638954532', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE `styles` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `name`, `created_at`, `updated_at`, `status`) VALUES
(1, 'College', '1633958005', NULL, 1),
(2, 'Trendy', '1633958027', NULL, 1),
(3, 'Office', '1633958035', NULL, 1),
(4, 'Casual', '1633958042', NULL, 1),
(5, 'Vintage', '1633958074', '1633958092', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions_transactions`
--

CREATE TABLE `subscriptions_transactions` (
  `id` int(11) NOT NULL,
  `unq_id` varchar(100) NOT NULL,
  `packages_id` bigint(20) NOT NULL,
  `customers_id` bigint(20) NOT NULL,
  `fashion_manager_id` bigint(20) DEFAULT NULL,
  `fashion_analyst_id` bigint(20) DEFAULT NULL,
  `selected_styles` varchar(100) DEFAULT NULL,
  `selected_sizes` json NOT NULL,
  `height` varchar(11) DEFAULT NULL,
  `weight` varchar(11) DEFAULT NULL,
  `preferences_likes` text,
  `preferences_dislikes` text,
  `address_name` varchar(100) DEFAULT NULL,
  `address_email` varchar(100) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `alternative_phone_number` varchar(20) DEFAULT NULL,
  `address` text,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `district` varchar(100) DEFAULT NULL,
  `landmark` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `pincode` varchar(11) DEFAULT NULL,
  `is_billing_address_checked` tinyint(1) NOT NULL DEFAULT '0',
  `is_terms_and_conditions_checked` tinyint(1) NOT NULL DEFAULT '0',
  `total_price` float(10,2) DEFAULT NULL,
  `discount` float(10,2) DEFAULT NULL,
  `coupon_code` varchar(11) DEFAULT NULL,
  `coupon_discount` float(10,2) DEFAULT NULL,
  `grand_total` float(10,2) DEFAULT NULL,
  `delivery_charges` float(10,2) DEFAULT NULL,
  `payment_type` varchar(100) DEFAULT NULL,
  `razorpay_payment_id` varchar(150) DEFAULT NULL,
  `razorpay_order_id` varchar(150) DEFAULT NULL,
  `razorpay_signature` varchar(150) DEFAULT NULL,
  `amount_paid` float(10,2) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL,
  `expiry_date` varchar(20) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `order_status` enum('Order Placed','Assigned to Fashion Analyst','Selected Items Under Review','Selections Confirmed','Dispatched','Shipped','Out For Delivery','Delivered','Returned','Exchange Requested','Cancellation Requested','Cancelled') NOT NULL DEFAULT 'Order Placed',
  `cancellation_reason` text,
  `dispatched_date` varchar(22) DEFAULT NULL,
  `shipped_date` varchar(22) DEFAULT NULL,
  `estimated_delivery_date` varchar(22) DEFAULT NULL,
  `delivered_date` varchar(22) DEFAULT NULL,
  `cancellation_requested_date` varchar(22) DEFAULT NULL,
  `cancelled_date` varchar(22) DEFAULT NULL,
  `exchange_request_date` varchar(22) DEFAULT NULL,
  `exchange_remarks` text,
  `is_exchanged` tinyint(1) NOT NULL DEFAULT '0',
  `no_of_items` int(11) NOT NULL,
  `package_title` text NOT NULL,
  `order_budget` int(11) DEFAULT NULL,
  `admin_address_id` bigint(20) DEFAULT NULL,
  `admin_address` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions_transactions`
--

INSERT INTO `subscriptions_transactions` (`id`, `unq_id`, `packages_id`, `customers_id`, `fashion_manager_id`, `fashion_analyst_id`, `selected_styles`, `selected_sizes`, `height`, `weight`, `preferences_likes`, `preferences_dislikes`, `address_name`, `address_email`, `phone_number`, `alternative_phone_number`, `address`, `city`, `state`, `district`, `landmark`, `street_name`, `pincode`, `is_billing_address_checked`, `is_terms_and_conditions_checked`, `total_price`, `discount`, `coupon_code`, `coupon_discount`, `grand_total`, `delivery_charges`, `payment_type`, `razorpay_payment_id`, `razorpay_order_id`, `razorpay_signature`, `amount_paid`, `transaction_id`, `created_at`, `updated_at`, `expiry_date`, `status`, `order_status`, `cancellation_reason`, `dispatched_date`, `shipped_date`, `estimated_delivery_date`, `delivered_date`, `cancellation_requested_date`, `cancelled_date`, `exchange_request_date`, `exchange_remarks`, `is_exchanged`, `no_of_items`, `package_title`, `order_budget`, `admin_address_id`, `admin_address`) VALUES
(102, 'ABM7109328', 6, 3, NULL, NULL, '4,3,', '{\"1\": \"2\", \"2\": \"8\", \"3\": \"12\", \"4\": \"21\"}', '5.10', '62', 'Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.', 'Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.', 'Grace King', 'manumanoj199700@gmail.com', '9000700588', '', 'D.no: 5-96, vivekanandha nagar, street no: 4, Old Dairy farm', 'Vizianagaram', 'Andhra Pradesh', 'Vizianagaram', 'Landmark is this', 'OLD DAIRY FARM', '530040', 0, 1, 6000.00, 1000.00, 'NEWCODE', 3000.00, 2250.00, 250.00, NULL, 'pay_IUyVj0bA9gLVOV', 'order_IUyVeUjaj4SDPE', '0a201bdb081fd6da3b98b75fa58d3a58d5aaaa6563e3e595fdcfb63300913b21', 2250.00, 'ABM8622', '1638964711', '1638965255', '1639138064', 1, 'Cancelled', 'This is Good Reason to cancel the Order..', NULL, NULL, '1639829264', NULL, '1639200597', '1639201786', NULL, NULL, 0, 5, 'Package Five\r\nCollection\r\n', NULL, NULL, NULL),
(103, 'ABM4089031', 6, 3, NULL, NULL, '2,1,', '{\"1\": \"2\", \"2\": \"8\", \"3\": \"12\", \"4\": \"21\"}', '5.10', '62', 'Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.', 'Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.', 'Grace King', 'manumanoj199700@gmail.com', '9000700588', '', 'D.no: 5-96, vivekanandha nagar, street no: 4, Old Dairy farm', 'Vizianagaram', 'Andhra Pradesh', 'Vizianagaram', 'Landmark is this', 'OLD DAIRY FARM', '530040', 0, 1, 6000.00, 1000.00, '', 0.00, 5250.00, 250.00, NULL, NULL, NULL, NULL, NULL, NULL, '1638965417', NULL, '1639141889', 0, 'Order Placed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5, 'Package Five\r\nCollection\r\n', NULL, NULL, NULL),
(104, 'ABM3056267', 6, 6, 15, 9, '4,3,2,1,5,', '{\"1\": \"3\", \"2\": \"8\", \"3\": \"13\", \"4\": \"20\"}', '5.5', '62', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', 'ShivaTeja Siripuram', 'shivateja2207@gmail.com', '09390791945', '', 'H.no 8-2-289/A/1,Old Venkateswara Nagar, Banjarahills', 'Hyderabad', 'Telanagana', 'Ranga reddy', '3rd building, 9th cross, maruthi layout', 'New Balaji Pg , Nagavara Jn.', '560045', 1, 1, 6000.00, 1000.00, NULL, NULL, 5250.00, 250.00, NULL, 'pay_IWVkiiFqgSlP20', 'order_IWVkTPGEhVLHPC', '15bef775f1d0fb084ad3f71ec05677d1573fbcd156287a3e2a21e4a28c270d00', 5250.00, 'ABM1294195', '1639299983', '1639300651', '1639473469', 1, 'Assigned to Fashion Analyst', NULL, NULL, NULL, '1640164669', NULL, NULL, NULL, NULL, NULL, 0, 5, 'Package Five\r\nCollection\r\n', 5000, 3, '<p>5th lane Dwarka Nagar, near degree college, Bharat Towers.</p>\r\n'),
(105, 'ABM9370935', 6, 1, NULL, NULL, '4,3,2,1,5,', '{\"1\": \"4\", \"2\": \"10\", \"3\": \"12\", \"4\": \"20\"}', '6.0', '65', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 6000.00, 1000.00, NULL, NULL, 5250.00, 250.00, NULL, NULL, NULL, NULL, NULL, NULL, '1639308181', NULL, '1639481166', 0, 'Order Placed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5, 'Package Five\r\nCollection\r\n', NULL, NULL, NULL),
(106, 'ABM4781138', 6, 6, NULL, NULL, '5,3,2,1,4,', '{\"1\": \"4\", \"2\": \"10\", \"3\": \"17\", \"4\": \"23\"}', '5.7', '60', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 6000.00, 1000.00, NULL, NULL, 5250.00, 250.00, NULL, NULL, NULL, NULL, NULL, NULL, '1639308453', NULL, '1639481321', 0, 'Order Placed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5, 'Package Five\r\nCollection\r\n', NULL, NULL, NULL),
(107, 'ABM7372594', 4, 6, NULL, NULL, '5,4,3,1,', '{\"1\": \"4\", \"2\": \"10\", \"3\": \"17\", \"4\": \"23\"}', '5.5', '57', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 5000.00, 1000.00, NULL, NULL, 4250.00, 250.00, NULL, NULL, NULL, NULL, NULL, NULL, '1639308819', NULL, '1639395258', 0, 'Order Placed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 4, 'Package Four\r\nCollection\r\n', NULL, NULL, NULL),
(108, 'ABM7634168', 4, 6, NULL, NULL, '5,4,3,2,', '{\"1\": \"4\", \"2\": \"10\", \"3\": \"17\", \"4\": \"23\"}', '5.3', '59', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', 'Dhoby Ghaut MRT station is an underground Mass Rapid Transit (MRT) interchange station on the North South, North East and Circle lines in Singapore. Located beneath the eastern end of Orchard Road shopping belt in Dhoby Ghaut, Museum Planning Area, the station is integrated with the commercial development The Atrium@Orchard. The station is near landmarks such as The Istana, the MacDonald House, Plaza Singapura and Dhoby Ghaut Green.\r\n\r\nDhoby Ghaut station was part of the early plans for the original MRT network since 1982. It was constructed as part of Phase I of the MRT network which was completed in 1987. Following the network\'s operational split, the station has been served by the North South line since 1989. To construct the North East line platforms, which were completed in 2003, the Stamford Canal had to be diverted while excavating through part of Mount Sophia. The Circle line platforms opened in 2010 along with Stages 1 and 2 of the line.\r\n\r\nThe only triple-line MRT interchange station in Singapore, Dhoby Ghaut station is one of the deepest and largest stations with five basement levels. Its deepest point is at 28 metres (92 ft) below ground. The station features many forms of artworks, three of them under the Art-in-Transit scheme in the North East line and Circle line stations, a pair of Art Seats at the Circle line platforms and an art piece above the North South line platforms.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 5000.00, 1000.00, NULL, NULL, 4250.00, 250.00, NULL, NULL, NULL, NULL, NULL, NULL, '1639309480', NULL, '1639395913', 0, 'Order Placed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 4, 'Package Four\r\nCollection\r\n', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscription_images`
--

CREATE TABLE `subscription_images` (
  `id` int(11) NOT NULL,
  `subscriptions_transactions_id` varchar(150) NOT NULL,
  `customers_id` bigint(20) NOT NULL,
  `image` varchar(100) NOT NULL,
  `type` enum('Full','Half') DEFAULT NULL,
  `created_at` varchar(22) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription_images`
--

INSERT INTO `subscription_images` (`id`, `subscriptions_transactions_id`, `customers_id`, `image`, `type`, `created_at`, `status`) VALUES
(55, 'ABM44826', 3, 'uploads/user/3edca34f147fc0a7d37812612774af7f.jpg', 'Full', '1634212074', 1),
(56, 'ABM44826', 3, 'uploads/user/2378d9ebbbaf9bc94c4887b2a2657d8f.jpg', 'Full', '1634212074', 1),
(57, 'ABM44826', 3, 'uploads/user/4b4d4b0416b3452be09e1116a6157f2d.jpg', 'Full', '1634212074', 1),
(58, 'ABM44826', 3, 'uploads/user/9ef67dcc002cb5fe678834d78fffba07.jpg', 'Half', '1634212084', 1),
(59, 'ABM44826', 3, 'uploads/user/df1474a12bbdedd978ac8005e228f055.jpg', 'Half', '1634212084', 1),
(60, 'ABM44826', 3, 'uploads/user/4c2814087430c1e6b377d5c72c91d8d3.jpg', 'Half', '1634212084', 1),
(62, 'ABM80483', 3, 'uploads/user/f14f27742a45d386f734adfd9b0bc246.PNG', 'Full', '1634640090', 1),
(63, 'ABM80483', 3, 'uploads/user/1d3754a02e38dd2813f52d675c773e63.jpg', 'Half', '1634640118', 1),
(64, 'ABM80483', 3, 'uploads/user/0413be9eed941c602c43492baa097913.jpg', 'Full', '1634640138', 1),
(65, 'ABM536116', 3, 'uploads/user/7b893647ea1ffe469c175116f94b064f.jpg', 'Full', '1636964608', 1),
(68, 'ABM536116', 3, 'uploads/user/647aa15322495800331b6dda24180aba.jpg', 'Full', '1636964637', 1),
(69, 'ABM536116', 3, 'uploads/user/9f14bcc7de9022dd0f83d06673c4bfd8.jpg', 'Half', '1636964654', 1),
(70, 'ABM536116', 3, 'uploads/user/228a7d64fc48a13b4c845e1f861735f8.jpg', 'Half', '1636964654', 1),
(71, 'ABM9819345', 3, 'uploads/user/30188de4be9303925475924093aa4936.jpg', 'Full', '1637059914', 1),
(72, 'ABM9819345', 3, 'uploads/user/5d55cab5702afce5bc8ba0b1ea877d53.jpg', 'Half', '1637059922', 1),
(73, 'ABM4970065', 3, 'uploads/user/ddffbf4e03027dd77c84f56fd30615db.jpg', 'Full', '1637385912', 1),
(76, 'ABM4970065', 3, 'uploads/user/cacc440c145aabcd60709ead8a908b7f.jpg', 'Half', '1637385935', 1),
(78, 'ABM4970065', 3, 'uploads/user/0160a1c3b3da6e462ce64ac7a192ab9e.jpg', 'Half', '1637385942', 1),
(79, 'ABM4970065', 3, 'uploads/user/eeed163e3a371db0fb1527fcba85ec13.jpg', 'Full', '1637385962', 1),
(80, 'ABM3542765', 3, 'uploads/user/53c57e87bf0424aec1713655a8e331b9.jpg', 'Full', '1637389002', 1),
(81, 'ABM3542765', 3, 'uploads/user/56a02e7d69555afd8da4630cba79beba.jpg', 'Half', '1637389007', 1),
(82, 'ABM2356811', 3, 'uploads/user/9aa75b3a22ac8b1223ab0dc9c8c4d6b3.jpg', 'Full', '1637391196', 1),
(83, 'ABM2356811', 3, 'uploads/user/345d20e958c56b82e23cd7409fb74577.jpg', 'Half', '1637391200', 1),
(84, 'ABM1772162', 3, 'uploads/user/72f95fdb6195461f93c4c4aa21b29e80.jpg', 'Full', '1637391883', 1),
(85, 'ABM1772162', 3, 'uploads/user/6279502e384ea7e7334f962513e9aeb3.jpg', 'Half', '1637391885', 1),
(86, 'ABM8610281', 3, 'uploads/user/3288a92c5e779ddbe890f40855c8d1aa.jpg', 'Full', '1637392382', 1),
(87, 'ABM8610281', 3, 'uploads/user/7bda9b3ca5a34d73f566d98a8d133d99.jpg', 'Half', '1637392384', 1),
(88, 'ABM6814990', 3, 'uploads/user/cee9141c2f255f4a3a59c110e3f225c1.jpg', 'Full', '1637395486', 1),
(89, 'ABM6814990', 3, 'uploads/user/aa3cf7f613969e1bba3eae03d611e2c1.jpg', 'Half', '1637395488', 1),
(90, 'ABM4303112', 3, 'uploads/user/bed6f64f4c38e56f4fc1813975cb182a.jpg', 'Full', '1637396447', 1),
(91, 'ABM4303112', 3, 'uploads/user/baf014fe65471ec97bc89cdbd4c526bf.jpg', 'Half', '1637396449', 1),
(92, 'ABM2710038', 3, 'uploads/user/554e7b300946d514ce6e49fcc7ed12a7.jpg', 'Full', '1637583194', 1),
(93, 'ABM2710038', 3, 'uploads/user/980c727ace4bec383393832ad034066a.jpg', 'Half', '1637583197', 1),
(94, 'ABM9370145', 3, 'uploads/user/a2a8f76a8a5762948e66ff5df1e28a4d.jpg', 'Full', '1637732210', 1),
(95, 'ABM9370145', 3, 'uploads/user/2ec9f5ca704ec6356f33d9d7bafaa292.jpg', 'Half', '1637732214', 1),
(96, 'ABM4174782', 3, 'uploads/user/67a824696f2abdf4e2ccca1a2ee94618.jpg', 'Full', '1637733038', 1),
(97, 'ABM4174782', 3, 'uploads/user/635ad5e94e7206d0366969a2d6f0b887.jpg', 'Half', '1637733040', 1),
(98, 'ABM5116018', 3, 'uploads/user/bf79d862b094c4c617f41e59e42fafdb.jpg', 'Full', '1637733068', 1),
(99, 'ABM5116018', 3, 'uploads/user/3818faf423228dc8400f82fcef12d555.jpg', 'Half', '1637733070', 1),
(100, 'ABM4439222', 3, 'uploads/user/26ec3b35fc388da4a450fa4d0c2ad8c4.jpg', 'Full', '1637734926', 1),
(101, 'ABM4439222', 3, 'uploads/user/088b42f1e76a5e4647b106e22d3e74a2.jpg', 'Half', '1637734928', 1),
(102, 'ABM4551305', 5, 'uploads/user/m-pk19sh09b-surhi-original-imaft8w4k6xheux6.jpeg', 'Full', '1637928522', 1),
(103, 'ABM4551305', 5, 'uploads/user/939b58d5870e1230fe65d09031f23c76.jpg', 'Full', '1637928525', 1),
(104, 'ABM4551305', 5, 'uploads/user/5dcc52c2f74b2dd4fce3d5ccdedde49c.jpg', 'Half', '1637928529', 1),
(105, 'ABM78483', 3, 'uploads/user/3966e05899f0c4a0123cd9bed75b8f03.jpg', 'Full', '1638168396', 1),
(106, 'ABM78483', 3, 'uploads/user/23c01cc34f8b685ab2dd429a7c8c5590.jpg', 'Full', '1638168396', 1),
(107, 'ABM78483', 3, 'uploads/user/7265968ea408f50dfbb15cfa5d6adffb.jpg', 'Half', '1638168402', 1),
(108, 'ABM78483', 3, 'uploads/user/2ab3c9375fc201c4d944b75fb1cb9900.jpg', 'Half', '1638168402', 1),
(109, 'ABM5730776', 3, 'uploads/user/4525c69d613a7c38c14bced1e2d24eb7.jpg', 'Full', '1638168946', 1),
(110, 'ABM5730776', 3, 'uploads/user/5c967c7b24960c2a257bc7d45a5abecf.jpg', 'Half', '1638168950', 1),
(111, 'ABM1914549', 3, 'uploads/user/ac5fee68983ed7a12bdb5b18738aefa9.jpg', 'Full', '1638169525', 1),
(112, 'ABM1914549', 3, 'uploads/user/e72c22258f90af61f34983684a9e2f50.jpg', 'Half', '1638169528', 1),
(113, 'ABM8942818', 3, 'uploads/user/a17cb4e599d04cc22a45d4212181fca1.jpg', 'Full', '1638178988', 1),
(114, 'ABM8942818', 3, 'uploads/user/2bfb0380089437be919a143df626a9c3.jpg', 'Half', '1638178991', 1),
(115, 'ABM1372693', 3, 'uploads/user/dad7531d55c1196763df324b532a0ae0.png', 'Full', '1638192196', 1),
(116, 'ABM1372693', 3, 'uploads/user/50de7e1f629ddf2efcf0ffe57bfa3596.jpg', 'Half', '1638192198', 1),
(117, 'ABM8989389', 3, 'uploads/user/f90ea9bcb38a6733b64a27c331d7f1e5.jpg', 'Full', '1638192444', 1),
(118, 'ABM8989389', 3, 'uploads/user/051ef5092c3395f0e8a50d838d973a21.jpg', 'Half', '1638192446', 1),
(119, 'ABM7015458', 3, 'uploads/user/88caab2d9558b5479fc59c13eff1d3a4.jpg', 'Full', '1638195949', 1),
(120, 'ABM7015458', 3, 'uploads/user/308667f163c9e6e2d5024b1595c07de9.jpg', 'Half', '1638195954', 1),
(121, 'ABM3689824', 10, 'uploads/user/c7e093731928c09245821e8de4799ed7.png', 'Full', '1638262194', 1),
(122, 'ABM3689824', 10, 'uploads/user/1b90505ffc667db786c23dc8650b24c1.png', 'Half', '1638262198', 1),
(123, 'ABM8714337', 11, 'uploads/user/18d74a03f4bd5131d928566c7349c854.jpg', 'Full', '1638422960', 1),
(124, 'ABM8714337', 11, 'uploads/user/117ed228508585d79352295cc52b216b.png', 'Half', '1638422973', 1),
(125, 'ABM8421934', 3, 'uploads/user/1f3cfaca771c43b2f711de9273e84beb.jpg', 'Full', '1638429114', 1),
(126, 'ABM8421934', 3, 'uploads/user/790b6fdb4f288fed03a2c501616d3748.jpg', 'Half', '1638429117', 1),
(127, 'ABM5449150', 12, 'uploads/user/66820d14540c5119698a1f104f0165da.PNG', 'Full', '1638598849', 1),
(128, 'ABM5449150', 12, 'uploads/user/c60ac903c1f068c1a9c78f2eeb07a6c1.jpg', 'Half', '1638598857', 1),
(131, 'ABM4359662', 12, 'uploads/user/22f8fd9f21ecc06e1c89dd443be3a6e8.jpg', 'Full', '1638767257', 1),
(132, 'ABM4359662', 12, 'uploads/user/931ad07025b3f0dd2b7747c459073635.png', 'Half', '1638767266', 1),
(133, 'ABM9836255', 12, 'uploads/user/41c8b0f262911d9e9ef97d126aecdc91.jpg', 'Full', '1638767356', 1),
(134, 'ABM9836255', 12, 'uploads/user/9f077c9be4f2e0a9cb9f1ecdbdc1c332.jpg', 'Half', '1638767358', 1),
(135, 'ABM4310202', 13, 'uploads/user/c78d2ccde5290da91040265230c20240.jpg', 'Full', '1638780016', 1),
(136, 'ABM4310202', 13, 'uploads/user/1896b5d76e77f4b990a1dc923ff86e5e.jpg', 'Half', '1638780020', 1),
(137, 'ABM5790478', 3, 'uploads/user/a8e951dfd55b3f46db75f9279597ab0d.jpg', 'Full', '1638781884', 1),
(138, 'ABM5790478', 3, 'uploads/user/d5a04bef6d87723dcb72f66858e03eaf.jpg', 'Half', '1638781886', 1),
(139, 'ABM4477547', 12, 'uploads/user/a519900aa1e351c04edbb7890625a045.jpg', 'Full', '1638783670', 1),
(140, 'ABM4477547', 12, 'uploads/user/8b03791e10be86eff77575fb023f7586.jpg', 'Half', '1638783672', 1),
(141, 'ABM660184', 12, 'uploads/user/ddfe104d416bfe8cbdf93bfdd33a9fdf.jpg', 'Full', '1638791739', 1),
(142, 'ABM660184', 12, 'uploads/user/01083c074ddce9188d346b9b62058a61.jpg', 'Half', '1638791741', 1),
(143, 'ABM3582457', 12, 'uploads/user/d8897ffa24e7f4b54106cfbfa90c01e7.jpg', 'Full', '1638792969', 1),
(144, 'ABM3582457', 12, 'uploads/user/da054af26ff3aea97e4e86c0d86f2038.jpg', 'Half', '1638792972', 1),
(145, 'ABM6919693', 12, 'uploads/user/5e5e928ed60011c2106da61c148e57a2.jpg', 'Full', '1638795514', 1),
(146, 'ABM6919693', 12, 'uploads/user/667a3c1c60b3d134258e86f0ad772904.jpg', 'Half', '1638795516', 1),
(147, 'ABM8216337', 12, 'uploads/user/b8eca3f0caaa145a7eda8f7cfb8fa93d.jpg', 'Full', '1638796683', 1),
(148, 'ABM8216337', 12, 'uploads/user/0064178b4e81475e08357df7a76eb603.jpg', 'Half', '1638796684', 1),
(149, 'ABM2381539', 12, 'uploads/user/bde4c53a560fbd74a069a44b6d901a9c.jpg', 'Full', '1638797554', 1),
(150, 'ABM2381539', 12, 'uploads/user/0adbfcd81cf25d9f5cf0062588224cae.jpg', 'Half', '1638797556', 1),
(151, 'ABM9725284', 12, 'uploads/user/ab4d3bab47f9e4a07ef5579eb2c00d1c.jpg', 'Full', '1638798127', 1),
(152, 'ABM9725284', 12, 'uploads/user/b8776c95e5bfd74ccd939bb5ebacb147.jpg', 'Half', '1638798129', 1),
(153, 'ABM5028751', 12, 'uploads/user/9cf4e77313e8f89e34963daf6c6f6b02.jpg', 'Full', '1638880462', 1),
(154, 'ABM5028751', 12, 'uploads/user/52d36b3a4634f6ea83e563864541b632.jpg', 'Half', '1638880463', 1),
(155, 'ABM5028751', 12, 'uploads/user/51885c1db62ea005a2aef66999bf4e93.jpg', 'Full', '1638880823', 1),
(156, 'ABM5028751', 12, 'uploads/user/44822261ae29007696e7c2614996c4c5.jpg', 'Half', '1638880823', 1),
(157, 'ABM5028751', 12, 'uploads/user/965e7d1df099d12aecaa883b2b3011e0.jpg', 'Full', '1638880858', 1),
(158, 'ABM5028751', 12, 'uploads/user/6cf36b55e7aa1c1de2541e396980150c.jpg', 'Half', '1638880858', 1),
(159, 'ABM755164', 12, 'uploads/user/c6e8338e84f17e8cefbed092888a8acb.jpg', 'Full', '1638881362', 1),
(160, 'ABM755164', 12, 'uploads/user/530088f95f799e1c88a276f2feb175e8.jpg', 'Half', '1638881365', 1),
(161, 'ABM7639289', 12, 'uploads/user/b315c3eed0ee3eef4becd8c6d23c0b81.jpg', 'Full', '1638881466', 1),
(162, 'ABM7639289', 12, 'uploads/user/2bb7e57e9ce47ac1fdc0451edd113d30.jpg', 'Half', '1638881467', 1),
(163, 'ABM9562669', 12, 'uploads/user/5db6c6adfdf24cdf0553cc7dadef5753.jpg', 'Full', '1638945430', 1),
(164, 'ABM9562669', 12, 'uploads/user/0da9da7ced67961e303f7d8d1fc0316d.jpg', 'Half', '1638945432', 1),
(166, 'ABM9562669', 12, 'uploads/user/cf7ec1ea9a55db0a4b0e2fb78ef31182.jpg', 'Full', '1638945468', 1),
(167, 'ABM9562669', 12, 'uploads/user/8e02e1509abfd498b2e2812ccd24ea65.jpg', 'Half', '1638945468', 1),
(168, 'ABM9562669', 12, 'uploads/user/ceb2b7079d5ea08ecc70d4500d60e0f3.jpg', 'Half', '1638945474', 1),
(169, 'ABM9562669', 12, 'uploads/user/5ccdc6eefccb10235d0faa7b91f1a849.jpg', 'Full', '1638945474', 1),
(174, 'ABM7524781', 12, 'uploads/user/7dfeb0b6ab6cdfad0d5f6848eeac9300.jpg', 'Full', '1638945930', 1),
(175, 'ABM7524781', 12, 'uploads/user/57ec4e5406dad400a44868f1420062f2.jpg', 'Half', '1638945932', 1),
(176, 'ABM7524781', 12, 'uploads/user/d1bc83bb4326b1788511331d61070094.jpg', 'Full', '1638945935', 1),
(177, 'ABM7524781', 12, 'uploads/user/502f3f8d51f9830385da7ef93b4b62ac.jpg', 'Half', '1638945935', 1),
(178, 'ABM7524781', 12, 'uploads/user/7a9016e1c1cb2f264e085e3c49921606.jpg', 'Full', '1638945938', 1),
(179, 'ABM7524781', 12, 'uploads/user/e897a38f68adddfbcb68e80a211d6179.jpg', 'Half', '1638945938', 1),
(180, 'ABM7524781', 12, 'uploads/user/03aef8f0d4ba16c3b881d2b52fd3ef3e.jpg', 'Full', '1638945941', 1),
(181, 'ABM7524781', 12, 'uploads/user/58478f82eb8789cef466e00825f716f8.jpg', 'Half', '1638945941', 1),
(182, 'ABM7524781', 12, 'uploads/user/83fc16a8604659d91df423930edd5201.jpg', 'Full', '1638945944', 1),
(183, 'ABM7524781', 12, 'uploads/user/cd24e6da26e6eb9d86387c77d61a21a0.jpg', 'Half', '1638945944', 1),
(184, 'ABM6140209', 12, 'uploads/user/2e89321d7af1af09a45e01ca6287e5bb.jpg', 'Full', '1638946121', 1),
(186, 'ABM6140209', 12, 'uploads/user/a4594371c1c546a40e6f0f268edba80b.jpg', 'Half', '1638946126', 1),
(187, 'ABM6140209', 12, 'uploads/user/1d2df9935a9f0152dfbb0955317ca25f.jpg', 'Full', '1638946133', 1),
(188, 'ABM6140209', 12, 'uploads/user/da042e5375f292f4d6baec932c773f46.jpg', 'Half', '1638946133', 1),
(189, 'ABM3660695', 12, 'uploads/user/1a2fab84b51838a89a080ab9568115e8.jpg', 'Full', '1638948378', 1),
(190, 'ABM3660695', 12, 'uploads/user/9bca69f7908719c75bf32724c941b5dc.jpg', 'Half', '1638948379', 1),
(191, 'ABM5478461', 12, 'uploads/user/bfcf4619f94e64e9a84e4ba3ffd5fbf7.jpg', 'Full', '1638948439', 1),
(192, 'ABM5478461', 12, 'uploads/user/b1d3d1898eae23998729fcc27b07390a.jpg', 'Half', '1638948441', 1),
(193, 'ABM9332070', 12, 'uploads/user/929ed1244b7fc0bd14b1c812a742cbaa.jpg', 'Full', '1638953220', 1),
(194, 'ABM9332070', 12, 'uploads/user/fa16588f4bce12eda21503ca4c61ee9f.jpg', 'Half', '1638953222', 1),
(195, 'ABM1073265', 12, 'uploads/user/6a1369aad337cf0d87a4d8627f15699a.jpg', 'Full', '1638953606', 1),
(196, 'ABM1073265', 12, 'uploads/user/00fc8109814a871b4beca791666b2569.jpg', 'Half', '1638953608', 1),
(197, 'ABM1073265', 12, 'uploads/user/8bdac8e1364059ba3957d2ec852ca313.jpg', 'Full', '1638953621', 1),
(198, 'ABM1073265', 12, 'uploads/user/3a298005d58cbe1ef6edae27051312f0.jpg', 'Half', '1638953621', 1),
(199, 'ABM1073265', 12, 'uploads/user/3c5eafe40fc871d0925f800f590d4820.jpg', 'Half', '1638953627', 1),
(200, 'ABM1073265', 12, 'uploads/user/707cdd7e387c5d6681637af801e2e7d6.jpg', 'Full', '1638953627', 1),
(201, 'ABM1073265', 12, 'uploads/user/12d0940b7b6275955caf9255529cfe8b.jpg', 'Full', '1638953630', 1),
(202, 'ABM1073265', 12, 'uploads/user/3184c1ff6a3b8a0607df56a93fdcaab2.jpg', 'Half', '1638953630', 1),
(203, 'ABM1073265', 12, 'uploads/user/b0055fc3fecd956585afceaea3dc7116.jpg', 'Full', '1638953634', 1),
(204, 'ABM1073265', 12, 'uploads/user/f0370dde5dc529a086f280c9f8cd9ea5.jpg', 'Half', '1638953634', 1),
(205, 'ABM1073265', 12, 'uploads/user/3b124fc352ba755effd0af38e8c8f483.jpg', 'Full', '1638953638', 1),
(206, 'ABM1073265', 12, 'uploads/user/ff44edd160c3f1b69b5ac5d308e143d9.jpg', 'Half', '1638953638', 1),
(207, 'ABM8449063', 3, 'uploads/user/576f8b08b8d94a2a6011164852020434.jpg', 'Full', '1638954671', 1),
(208, 'ABM8449063', 3, 'uploads/user/fbdf0f3a959f50c25b602b443f4ad3d2.jpg', 'Half', '1638954674', 1),
(209, 'ABM7712047', 3, 'uploads/user/dacae1d7e8260109f3e8b075f441b2d0.jpg', 'Full', '1638955621', 1),
(210, 'ABM7712047', 3, 'uploads/user/f82f948a0f3229d2ed4d097b0e03f854.jpg', 'Half', '1638955624', 1),
(211, 'ABM4013140', 12, 'uploads/user/26d539c1f7e41618a8290d73920aba8e.jpg', 'Full', '1638957602', 1),
(212, 'ABM4013140', 12, 'uploads/user/40eb5fc67775c509831dfdf1e82620ab.jpg', 'Half', '1638957604', 1),
(213, 'ABM8831858', 12, 'uploads/user/e9808115cfe20f0274f20ec279d8021f.jpg', 'Full', '1638957741', 1),
(214, 'ABM8831858', 12, 'uploads/user/b985ab4fe5792b5e3af714271503a2ef.jpg', 'Half', '1638957743', 1),
(215, 'ABM9031742', 3, 'uploads/user/2a15d944978c3fabd5f063e1d5e2acfe.jpg', 'Full', '1638959496', 1),
(216, 'ABM9031742', 3, 'uploads/user/9211a63c0f6a5bcaf96a89de700ed61d.jpg', 'Half', '1638959498', 1),
(217, 'ABM6106820', 12, 'uploads/user/45684d449c6260fc018c1c001b4c36f0.jpg', 'Full', '1638960703', 1),
(218, 'ABM6106820', 12, 'uploads/user/b2686cf02631797192bec9ea3d176e80.jpg', 'Half', '1638960705', 1),
(219, 'ABM1850134', 12, 'uploads/user/5235572be3b555b4255335bd3ca278b7.jpg', 'Full', '1638960894', 1),
(220, 'ABM1850134', 12, 'uploads/user/9390a21d121cb606e5a8f1ed3c8d86fb.jpg', 'Half', '1638960896', 1),
(221, 'ABM1850134', 12, 'uploads/user/70eeb20338ecc80ea0e509dd3a296cc1.jpg', 'Full', '1638960905', 1),
(222, 'ABM1850134', 12, 'uploads/user/42f7fdea0704f527cd2e8cc02b63a545.jpg', 'Half', '1638960905', 1),
(223, 'ABM1850134', 12, 'uploads/user/29e50dd83dfebed1bd00fff58477e2d9.jpg', 'Full', '1638960908', 1),
(224, 'ABM1850134', 12, 'uploads/user/e2a5ddde5a8483d0ddccc44a2416dcce.jpg', 'Half', '1638960908', 1),
(225, 'ABM1850134', 12, 'uploads/user/d29df9ad07cb8341dd36b84d96667948.jpg', 'Full', '1638961032', 1),
(226, 'ABM1850134', 12, 'uploads/user/315a639908a7791f238443cb6555e379.jpg', 'Half', '1638961032', 1),
(227, 'ABM5291945', 12, 'uploads/user/ecc4404a3b61a1064da4a8c817ceb356.jpg', 'Full', '1638961100', 1),
(228, 'ABM5291945', 12, 'uploads/user/674e1e653e94aacfc41b4021f19c4f3e.jpg', 'Half', '1638961102', 1),
(229, 'ABM8186941', 12, 'uploads/user/db366894a764934cfa7a3f3f46887cd8.jpg', 'Full', '1638961134', 1),
(230, 'ABM8186941', 12, 'uploads/user/9ddaa3eab816f04832de0e53e56d61e6.png', 'Half', '1638961135', 1),
(231, 'ABM6589914', 12, 'uploads/user/35375c6f1425226372d79edd41954a5c.jpg', 'Full', '1638961547', 1),
(232, 'ABM6589914', 12, 'uploads/user/c5aa7fb834fd08cf47ee57fde04d92fb.jpg', 'Half', '1638961550', 1),
(233, 'ABM5909521', 3, 'uploads/user/bc9540851f670197a1d65de66fa9e266.jpg', 'Half', '1638961920', 1),
(234, 'ABM466526', 12, 'uploads/user/4c0b4d3254fdc2154776601bf04ac86c.jpg', 'Full', '1638962377', 1),
(235, 'ABM466526', 12, 'uploads/user/bd3fcf84514cbeb9b83766a45412c430.jpg', 'Half', '1638962378', 1),
(242, 'ABM1058298', 12, 'uploads/user/e9edf2cee4d19c12feb36501e035f3de.jpg', 'Full', '1638962477', 1),
(243, 'ABM1058298', 12, 'uploads/user/32417aa4fec598f0248dcab78b616407.jpg', 'Half', '1638962485', 1),
(244, 'ABM3824635', 12, 'uploads/user/b9d473f05c25b01c8b44c81455f586cc.jpg', 'Full', '1638962535', 1),
(245, 'ABM3824635', 12, 'uploads/user/9881ee743bd43192e2c916c10edd01cc.jpg', 'Half', '1638962536', 1),
(246, 'ABM4019533', 12, 'uploads/user/dcd20ba677313a82d66ecf91bb94d8dc.jpg', 'Full', '1638962768', 1),
(247, 'ABM4019533', 12, 'uploads/user/ad578985c907de9b07f1792e6c23c809.jpg', 'Half', '1638962769', 1),
(248, 'ABM7109328', 3, 'uploads/user/ee5bb6987b0332294c25229b39b2033e.jpg', 'Full', '1638964789', 1),
(249, 'ABM7109328', 3, 'uploads/user/2dedd57e228d25bed35182ecf3997150.jpg', 'Half', '1638964792', 1),
(257, 'ABM4089031', 3, 'uploads/user/ada0411558ff1a191f1674f8872ef2c0.PNG', 'Half', '1638965475', 1),
(271, 'ABM4089031', 3, 'uploads/user/c565a8219e69199451ddc225a47b8239.jpg', 'Full', '1638966268', 1),
(272, 'ABM3056267', 6, 'uploads/user/WhatsApp Image 2021-11-17 at 8_58_55 PM (1).jpeg', 'Full', '1639300367', 1),
(273, 'ABM3056267', 6, 'uploads/user/fb83c8cc21e00dc93ea6c7462924c0f4.jpg', 'Half', '1639300389', 1),
(274, 'ABM3056267', 6, 'uploads/user/515f148055b571db3e86a137c5c6391f.jpg', 'Full', '1639300398', 1),
(276, 'ABM9370935', 1, 'uploads/user/816584539fee478e51d6cab92ee100d2.jpg', 'Full', '1639308342', 1),
(277, 'ABM9370935', 1, 'uploads/user/89f3fff4a40efad953b46f51cc113e2f.jpg', 'Half', '1639308355', 1),
(278, 'ABM4781138', 6, 'uploads/user/4007990b885fc62311cb3d60ba538da0.jpg', 'Full', '1639308467', 1),
(279, 'ABM4781138', 6, 'uploads/user/d7ddaa71f94265704531cc01f697a0a3.jpg', 'Half', '1639308509', 1),
(280, 'ABM7372594', 6, 'uploads/user/fd789b6d420f7a011e86d2198e9dbd5a.jpg', 'Full', '1639308834', 1),
(281, 'ABM7372594', 6, 'uploads/user/744812f754006adc70a496a563eb815e.jpg', 'Half', '1639308845', 1),
(282, 'ABM7634168', 6, 'uploads/user/4718941aa988ddf45ae366e9550add92.jpg', 'Full', '1639309493', 1),
(283, 'ABM7634168', 6, 'uploads/user/cf25a991e6ed969f6041741a1665a5db.jpg', 'Half', '1639309503', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(155) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `title`, `created_at`, `updated_at`, `status`) VALUES
(3, 1, 'Casual shirts', '1637822844', '', 1),
(4, 1, 'Printed shirts', '1637822858', '', 1),
(5, 2, 'V neck Tshirts', '1637822894', '', 1),
(6, 2, 'Full sleeve', '1637822909', '', 1),
(7, 2, 'Half sleeve', '1637822923', '', 1),
(8, 3, 'Formal pants', '1637822939', '', 1),
(9, 3, 'Jean pants', '1637822950', '', 1),
(10, 3, 'Trousers', '1637823191', '', 1),
(11, 4, 'Formal Shoes', '1637823219', '', 1),
(12, 4, 'Sports shoes', '1637823234', '', 1),
(13, 4, 'Casual Shoes', '1637823252', '', 1),
(14, 1, 'Formal Shirts', '1638779980', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `terms_conditions`
--

CREATE TABLE `terms_conditions` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms_conditions`
--

INSERT INTO `terms_conditions` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Terms and Conditions', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n\r\n<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Atque hic temporibus et numquam id tempore sequi doloremque minima pariatur optio error, provident recusandae saepe nihil possimus expedita quibusdam architecto voluptates. Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repellat laborum ex, inventore at, quo nulla! Sint, suscipit nam eum rem unde modi molestias quos iure non repellat quibusdam eligendi.</p>\r\n', '1639206649', '1639206709');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL COMMENT '<font color="red">Note:</font> Upload Width: 100px; Height: 100px',
  `description` text NOT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Justin', '44c5cbb3d0a0febf84f2241392713e94.jpg', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure nostrum numquam, distinctio corrupti nam optio fuga, molestiae natus facilis culpa accusantium molestias, excepturi!</p>\r\n', '1632733142', NULL, 1),
(2, 'Sneha', '8c47e1e2ef1af427d48d3237ba3f7f51.jpg', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure nostrum numquam, distinctio corrupti nam optio fuga, molestiae natus facilis culpa accusantium molestias, excepturi!</p>\r\n', '1632733151', NULL, 1),
(3, 'Mathews', '9ae16469ea607224d4ebc95104fb19c7.jpg', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure nostrum numquam, distinctio corrupti nam optio fuga, molestiae natus facilis culpa accusantium molestias, excepturi!</p>\r\n', '1632733205', NULL, 1),
(4, 'test testimonail-1', '8df4afe2ac9982a18de3d9b3a39b0243.jpg', '<p>HELLO</p>\r\n', '1638781398', '1638781466', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(60) DEFAULT NULL COMMENT 'This will be username, email is username',
  `mobile` varchar(20) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `salt` varchar(25) DEFAULT NULL,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `username`, `mobile`, `password`, `salt`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 'Admin', 'admin@absolutemens.com', '', 'ba0c5814630044fad909591f2b6209b8', 'QXP9oL6aMF215', '1479899025', '1638775726', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_preferences`
--

CREATE TABLE `user_preferences` (
  `id` int(11) NOT NULL,
  `customers_id` int(11) NOT NULL,
  `shirt_size_id` int(11) DEFAULT NULL,
  `t_shirt_size_id` int(11) DEFAULT NULL,
  `casual_pant_size_id` int(11) DEFAULT NULL,
  `formal_pant_size_id` int(11) DEFAULT NULL,
  `jeans_pant_size_id` int(11) DEFAULT NULL,
  `shoe_size_id` int(11) DEFAULT NULL,
  `budget` varchar(120) DEFAULT NULL,
  `occassion` text,
  `description` text,
  `liked_preferences` text,
  `disliked_preferences` text,
  `special_preferences` text,
  `special_measurements` text,
  `height` bigint(20) DEFAULT NULL,
  `weight` bigint(20) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_preferences`
--

INSERT INTO `user_preferences` (`id`, `customers_id`, `shirt_size_id`, `t_shirt_size_id`, `casual_pant_size_id`, `formal_pant_size_id`, `jeans_pant_size_id`, `shoe_size_id`, `budget`, `occassion`, `description`, `liked_preferences`, `disliked_preferences`, `special_preferences`, `special_measurements`, `height`, `weight`, `created_at`, `updated_at`, `status`) VALUES
(1, 3, 3, 7, 2, 6, 10, 2, '6000', 'All Ocsasions', 'Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.', 'Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.', 'Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.', 'Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.', 'Lorem Ipsum jest tekstem stosowanym jako przyk?adowy wype?niacz w przemy?le poligraficznym.', 11, 8, NULL, NULL, 1),
(2, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(3, 5, 3, 8, 3, 7, 11, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `ref_code` varchar(100) DEFAULT NULL,
  `business_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `land_mark` varchar(255) NOT NULL,
  `owner_name` varchar(120) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `gst_number` varchar(100) DEFAULT NULL,
  `states_id` bigint(20) NOT NULL,
  `districts_id` bigint(20) NOT NULL,
  `cities_id` bigint(20) NOT NULL,
  `address` text NOT NULL,
  `contact_number` varchar(30) NOT NULL,
  `alternate_contact_number` varchar(30) DEFAULT NULL,
  `pincode` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` varchar(100) NOT NULL,
  `display_image` varchar(100) NOT NULL,
  `onhold_wallet_amount` int(11) NOT NULL DEFAULT '0',
  `is_online` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `ref_code`, `business_name`, `description`, `land_mark`, `owner_name`, `contact_email`, `gst_number`, `states_id`, `districts_id`, `cities_id`, `address`, `contact_number`, `alternate_contact_number`, `pincode`, `password`, `salt`, `display_image`, `onhold_wallet_amount`, `is_online`, `created_at`, `updated_at`, `status`) VALUES
(2, 'SHP764664', 'One Shop Stop', '<p>This is the Description........</p>', 'Landmark', 'JACK', 'oneshop@gmail.com', 'GST123456789012', 1, 1, 1, '<p>Test Address, Test Street Name. Test Area</p>', '9988776655', '9900990088', '530040', '7e85ab7a0f73ab1163a55535ce15fa99', 'bg3jaj', 'b5a506af9f44752ab71635bf098d717a.jpg', 694, 1, '1633065930', '1638795314', 1),
(11, 'SHP1278745', 'Second Shop', '<p>Second Shop</p>', 'Second Shop', 'Second Shop', 'pragadasaimanoj@gmail.com', 'GST123456789012', 1, 1, 1, '<p>Second Shop</p>', '9900990088', '', '530090', '35c2490516a7f231840009739e5c2257', 'BbD15h', '8048622136a9233542acbc81475df7b2.jpg', 0, 0, '1638254223', '', 1),
(12, 'SHP536678', 'REDDY VENDOR', '<p>TOP 10 BRAND COLLECTIONS WILL BE AVAILABLE HERE like</p><p>1) US&nbsp;polo</p><p>2)&nbsp;Gucci</p><p>3)&nbsp;Giorgio Armani</p><p>4) Balenciaga</p><p>5) The North Face</p><p>6) H&amp;M</p><p>7) Versace</p><p>8) Prada</p><p>9) Mufti</p><p>10) Indian Terrian</p>', 'Near Royal Enfield showroom', 'Manikanta Reddy vendor', 'maninr87@gmail.com', '128191911128812', 1, 1, 1, '<p>Dwarka nagar 5 th lane, near Degree college. Bharat towers.</p>', '1234567890', '9741548415', '852451', 'ee54be4cdfd09236710915b96126e962', 'XwduyW', '8792477229789a55314f42ce7cd56683.jpeg', 0, 0, '1638604570', '1638958819', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendors_gallery`
--

CREATE TABLE `vendors_gallery` (
  `id` int(11) NOT NULL,
  `vendors_id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors_gallery`
--

INSERT INTO `vendors_gallery` (`id`, `vendors_id`, `title`, `image`, `created_at`, `updated_at`, `status`) VALUES
(7, 2, 'Image Title', 'e9f7dba53d4d6aac4da36aa7910a38c0.jpg', '1634883087', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_bank_details`
--

CREATE TABLE `vendor_bank_details` (
  `id` int(11) NOT NULL,
  `vendors_id` bigint(20) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `branch` varchar(100) DEFAULT NULL,
  `ifsc_code` varchar(100) NOT NULL,
  `contact_id` varchar(100) DEFAULT NULL,
  `created_at` varchar(22) DEFAULT NULL,
  `updated_at` varchar(22) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_bank_details`
--

INSERT INTO `vendor_bank_details` (`id`, `vendors_id`, `account_name`, `account_number`, `bank_name`, `branch`, `ifsc_code`, `contact_id`, `created_at`, `updated_at`, `status`) VALUES
(1, 2, 'Savings', '13222233334444432', 'Union Bank of India', 'ADARSHNAGAR', 'UBIN0813966', NULL, NULL, '1639121354', 1),
(2, 12, 'MANIKANTA', '20344696223', 'State Bank of India', 'PAYAKARAOPETA', 'SBIN0002778', NULL, '1638794277', '1638963961', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_login_logs`
--

CREATE TABLE `vendor_login_logs` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(30) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `created_at` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_login_logs`
--

INSERT INTO `vendor_login_logs` (`id`, `ip_address`, `vendor_id`, `created_at`) VALUES
(1, '49.37.150.118', 2, '1634798991'),
(2, '202.53.69.163', 2, '1634806806'),
(3, '49.37.150.118', 2, '1634878959'),
(4, '202.53.69.163', 2, '1635483920'),
(5, '49.37.144.165', 2, '1635829681'),
(6, '49.37.144.165', 2, '1635841821'),
(7, '202.53.69.163', 2, '1635911565'),
(8, '202.53.69.163', 2, '1635911565'),
(9, '49.37.146.52', 2, '1636347653'),
(10, '49.37.146.52', 2, '1636369633'),
(11, '49.37.146.52', 2, '1636440983'),
(12, '49.37.146.52', 2, '1636461247'),
(13, '49.37.171.31', 2, '1636461249'),
(14, '49.37.168.7', 2, '1636533360'),
(15, '202.53.69.163', 2, '1636697972'),
(16, '49.37.148.219', 2, '1637131989'),
(17, '49.37.148.219', 2, '1637140576'),
(18, '49.37.148.219', 2, '1637229558'),
(19, '49.37.148.219', 2, '1637233991'),
(20, '49.37.148.219', 2, '1637234843'),
(21, '49.37.171.21', 2, '1637234846'),
(22, '49.37.169.185', 2, '1637297699'),
(23, '49.37.148.219', 2, '1637306818'),
(24, '49.37.169.185', 2, '1637318332'),
(25, '49.37.169.185', 2, '1637328550'),
(26, '49.37.148.219', 2, '1637382973'),
(27, '49.37.169.185', 2, '1637384206'),
(28, '49.37.148.219', 2, '1637386293'),
(29, '49.37.169.185', 2, '1637389315'),
(30, '49.37.148.219', 2, '1637391368'),
(31, '49.37.148.219', 2, '1637392517'),
(32, '49.37.148.219', 2, '1637392575'),
(33, '49.37.148.219', 2, '1637395754'),
(34, '49.37.148.219', 2, '1637571768'),
(35, '49.37.175.73', 2, '1637753437'),
(36, '49.37.175.209', 2, '1637821858'),
(37, '49.37.173.15', 2, '1637824331'),
(38, '49.37.173.15', 2, '1637838800'),
(39, '49.37.173.151', 2, '1637902508'),
(40, '49.37.151.25', 2, '1638247203'),
(41, '49.37.151.25', 2, '1638247437'),
(42, '49.37.151.25', 2, '1638247469'),
(43, '49.37.151.25', 2, '1638247493'),
(44, '49.37.151.25', 2, '1638247501'),
(45, '49.37.151.25', 2, '1638266572'),
(46, '49.37.151.25', 2, '1638273051'),
(47, '49.37.151.25', 2, '1638333938'),
(48, '124.123.189.253', 2, '1638336309'),
(49, '49.37.151.25', 2, '1638423892'),
(50, '49.37.151.25', 2, '1638428663'),
(51, '49.37.151.25', 2, '1638428813'),
(52, '49.37.151.25', 2, '1638429917'),
(53, '202.53.69.163', 2, '1638599751'),
(54, '202.53.69.163', 12, '1638604605'),
(55, '202.53.69.163', 12, '1638604662'),
(56, '202.53.69.163', 2, '1638768389'),
(57, '202.53.69.163', 12, '1638793151'),
(58, '202.53.69.163', 2, '1638796080'),
(59, '49.37.151.25', 2, '1638796882'),
(60, '202.53.69.163', 12, '1638796947'),
(61, '202.53.69.163', 2, '1638797431'),
(62, '202.53.69.163', 2, '1638797824'),
(63, '202.53.69.163', 2, '1638798589'),
(64, '202.53.69.163', 2, '1638879486'),
(65, '202.53.69.163', 2, '1638942534'),
(66, '202.53.69.163', 12, '1638954787'),
(67, '202.53.69.163', 2, '1638955849'),
(68, '202.53.69.163', 12, '1638955937'),
(69, '202.53.69.163', 2, '1638958370'),
(70, '202.53.69.163', 12, '1638958774'),
(71, '202.53.69.163', 13, '1638964134'),
(72, '202.53.69.163', 13, '1638964191'),
(73, '202.53.69.163', 12, '1638964692'),
(74, '202.53.69.163', 2, '1638964718'),
(75, '202.53.69.163', 12, '1639026043'),
(76, '49.37.148.243', 2, '1639119623');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_settlements`
--

CREATE TABLE `vendor_settlements` (
  `id` bigint(20) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `amount` float NOT NULL,
  `date` date NOT NULL,
  `withdraw_request_id` bigint(20) NOT NULL,
  `settled_status` enum('pending','completed') NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_settlements`
--

INSERT INTO `vendor_settlements` (`id`, `vendor_id`, `amount`, `date`, `withdraw_request_id`, `settled_status`, `created_at`, `updated_at`, `status`) VALUES
(1, 2, 500, '2021-12-04', 1, 'completed', '1638600024', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_wallet`
--

CREATE TABLE `vendor_wallet` (
  `id` bigint(20) NOT NULL,
  `vendor_settlement_id` bigint(20) DEFAULT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `action` enum('','credit','debit') NOT NULL,
  `credit_type` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `title` varchar(355) NOT NULL,
  `comment` varchar(555) NOT NULL,
  `balance` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_wallet`
--

INSERT INTO `vendor_wallet` (`id`, `vendor_settlement_id`, `vendor_id`, `action`, `credit_type`, `amount`, `title`, `comment`, `balance`, `date`, `created_at`, `updated_at`, `status`) VALUES
(1, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 600, '2021-12-02', '1638424704', '', 1),
(2, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 1450, '2021-12-02', '1638424704', '', 1),
(3, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 2050, '2021-12-02', '1638424750', '', 1),
(4, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 2900, '2021-12-02', '1638424750', '', 1),
(5, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 3500, '2021-12-02', '1638424840', '', 1),
(6, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 4350, '2021-12-02', '1638424840', '', 1),
(7, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 4950, '2021-12-02', '1638424880', '', 1),
(8, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 5800, '2021-12-02', '1638424880', '', 1),
(9, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 6400, '2021-12-02', '1638425401', '', 1),
(10, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 7250, '2021-12-02', '1638425401', '', 1),
(11, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 7850, '2021-12-02', '1638425411', '', 1),
(12, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 8700, '2021-12-02', '1638425411', '', 1),
(13, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 9300, '2021-12-02', '1638425419', '', 1),
(14, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 10150, '2021-12-02', '1638425419', '', 1),
(15, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 10750, '2021-12-02', '1638425428', '', 1),
(16, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 11600, '2021-12-02', '1638425428', '', 1),
(17, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 12200, '2021-12-02', '1638425437', '', 1),
(18, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 13050, '2021-12-02', '1638425437', '', 1),
(19, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 13650, '2021-12-02', '1638425439', '', 1),
(20, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 14500, '2021-12-02', '1638425439', '', 1),
(21, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 15100, '2021-12-02', '1638425514', '', 1),
(22, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 15950, '2021-12-02', '1638425514', '', 1),
(23, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 16550, '2021-12-02', '1638425523', '', 1),
(24, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 17400, '2021-12-02', '1638425523', '', 1),
(25, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM37265068 ', 18000, '2021-12-02', '1638425548', '', 1),
(26, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM37265068 ', 18850, '2021-12-02', '1638425548', '', 1),
(27, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM44494354 ', 19450, '2021-12-02', '1638429986', '', 1),
(28, NULL, 2, 'credit', 'from order delivery', 850, 'Amount credited', '850 amount credited in wallet for completing the order with ref id - ABM44494354 ', 20300, '2021-12-02', '1638429986', '', 1),
(29, NULL, 2, 'credit', 'from order delivery', 800, 'Amount credited', '800 amount credited in wallet for completing the order with ref id - ABM99711648 ', 21100, '2021-12-04', '1638599922', '', 1),
(30, NULL, 2, 'debit', 'from withdraw request', 500, 'Amount Debited', '500 amount debited for withdraw request id - WD00001 ', 20600, '2021-12-04', '1638600024', '', 1),
(31, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM56128832 ', 21200, '2021-12-06', '1638768639', '', 1),
(32, NULL, 2, 'credit', 'from order delivery', 900, 'Amount credited', '900 amount credited in wallet for completing the order with ref id - ABM56128832 ', 22100, '2021-12-06', '1638768639', '', 1),
(33, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM42526767 ', 22700, '2021-12-06', '1638796187', '', 1),
(34, NULL, 2, 'credit', 'from order delivery', 900, 'Amount credited', '900 amount credited in wallet for completing the order with ref id - ABM42526767 ', 23600, '2021-12-06', '1638796187', '', 1),
(35, NULL, 2, 'credit', 'from order delivery', 600, 'Amount credited', '600 amount credited in wallet for completing the order with ref id - ABM65587105 ', 24200, '2021-12-06', '1638797861', '', 1),
(36, NULL, 2, 'credit', 'from order delivery', 900, 'Amount credited', '900 amount credited in wallet for completing the order with ref id - ABM65587105 ', 25100, '2021-12-06', '1638797861', '', 1),
(37, NULL, 2, 'credit', 'from order delivery', 900, 'Amount credited', '900 amount credited in wallet for completing the order with ref id - ABM9725284 ', 26000, '2021-12-06', '1638798623', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_withdraw_requests`
--

CREATE TABLE `vendor_withdraw_requests` (
  `id` bigint(20) NOT NULL,
  `increment` bigint(20) NOT NULL,
  `vendor_id` bigint(20) NOT NULL,
  `request_id` varchar(355) NOT NULL,
  `comment` varchar(400) NOT NULL,
  `amount` int(11) NOT NULL,
  `withdraw_status` enum('submitted','released','rejected') NOT NULL,
  `reject_reason` varchar(600) DEFAULT NULL,
  `approve_comment` varchar(355) DEFAULT NULL,
  `date_time` date NOT NULL,
  `released_at` datetime NOT NULL,
  `rejected_at` datetime NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_withdraw_requests`
--

INSERT INTO `vendor_withdraw_requests` (`id`, `increment`, `vendor_id`, `request_id`, `comment`, `amount`, `withdraw_status`, `reject_reason`, `approve_comment`, `date_time`, `released_at`, `rejected_at`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, 2, 'WD00001', 'Withdraw amount of 500 on 04-12-21 12:09:52', 500, 'released', NULL, 'txn id - dwewdewdewwd', '2021-12-04', '2021-12-04 12:10:24', '0000-00-00 00:00:00', '1638599992', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `weights`
--

CREATE TABLE `weights` (
  `id` int(11) NOT NULL,
  `weight` varchar(30) NOT NULL,
  `created_at` varchar(22) NOT NULL,
  `updated_at` varchar(22) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weights`
--

INSERT INTO `weights` (`id`, `weight`, `created_at`, `updated_at`, `status`) VALUES
(1, '55', '1633937566', '', 1),
(2, '56', '1633937570', '', 1),
(3, '57', '1633937579', '', 1),
(4, '58', '1633937585', '', 1),
(5, '59', '1633937590', '', 1),
(6, '60', '1633937596', '', 1),
(7, '61', '1633937600', '', 1),
(8, '62', '1633937605', '', 1),
(9, '63', '1633937612', '', 1),
(10, '64', '1633937617', '', 1),
(11, '65', '1633937621', '', 1),
(12, '66', '1633937626', '', 1),
(13, '67', '1633937631', '', 1),
(14, '68', '1633937637', '', 1),
(15, '69', '1633937645', '', 1),
(17, '70', '1638780055', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `we_shop_for_you_info`
--

CREATE TABLE `we_shop_for_you_info` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `we_shop_for_you_info`
--

INSERT INTO `we_shop_for_you_info` (`id`, `title`, `description`, `created_at`, `updated_at`, `status`) VALUES
(1, 'WE SHOP FOR YOU DETAILS', '<h5>Title Here</h5>\r\n\r\n<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n\r\n<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n\r\n<h5>Title Here</h5>\r\n\r\n<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n\r\n<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n\r\n<h5>Title Here</h5>\r\n\r\n<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Fuga inventore at suscipit, nisi eius obcaecati accusamus provident eum exercitationem praesentium, velit blanditiis sequi incidunt nam, accusantium, porro autem deserunt. Voluptas? Lorem ipsum dolor sit, amet, consectetur adipisicing elit. Quia, omnis repellendus quod laudantium perferendis accusamus sequi obcaecati dolor dolorum deleniti nihil mollitia modi tenetur rem aut, debitis sunt porro. Autem.</p>\r\n', '1633945732', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `withdraw_settings`
--

CREATE TABLE `withdraw_settings` (
  `id` bigint(20) NOT NULL,
  `minimum_withdraw_amount` int(11) NOT NULL,
  `maximum_withdraw_amount` int(11) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `withdraw_settings`
--

INSERT INTO `withdraw_settings` (`id`, `minimum_withdraw_amount`, `maximum_withdraw_amount`, `updated_at`, `status`) VALUES
(1, 100, 5000, '1637238329', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `access_rights`
--
ALTER TABLE `access_rights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `admin_addresses`
--
ALTER TABLE `admin_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_dashboard_main_menu`
--
ALTER TABLE `admin_dashboard_main_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_dashboard_sub_menu`
--
ALTER TABLE `admin_dashboard_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_info`
--
ALTER TABLE `blog_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_enquiries`
--
ALTER TABLE `contact_enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers_login_logs`
--
ALTER TABLE `customers_login_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_reports`
--
ALTER TABLE `customer_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designers_and_analysts`
--
ALTER TABLE `designers_and_analysts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_subscriptions`
--
ALTER TABLE `email_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fashion_desg_login_logs`
--
ALTER TABLE `fashion_desg_login_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fashion_designer_cart`
--
ALTER TABLE `fashion_designer_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_info`
--
ALTER TABLE `footer_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `heights`
--
ALTER TABLE `heights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage_sliders`
--
ALTER TABLE `homepage_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `how_it_works_images`
--
ALTER TABLE `how_it_works_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `how_it_works_points`
--
ALTER TABLE `how_it_works_points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_logs`
--
ALTER TABLE `login_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_feedbacks`
--
ALTER TABLE `orders_feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_log`
--
ALTER TABLE `orders_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_page_banners`
--
ALTER TABLE `package_page_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pants_sizes`
--
ALTER TABLE `pants_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_inventory`
--
ALTER TABLE `products_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_inventory`
--
ALTER TABLE `product_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `return_policy`
--
ALTER TABLE `return_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shirts_sizes`
--
ALTER TABLE `shirts_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoes_sizes`
--
ALTER TABLE `shoes_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `styles`
--
ALTER TABLE `styles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions_transactions`
--
ALTER TABLE `subscriptions_transactions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unq_id` (`unq_id`);

--
-- Indexes for table `subscription_images`
--
ALTER TABLE `subscription_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_conditions`
--
ALTER TABLE `terms_conditions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_preferences`
--
ALTER TABLE `user_preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors_gallery`
--
ALTER TABLE `vendors_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_bank_details`
--
ALTER TABLE `vendor_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_login_logs`
--
ALTER TABLE `vendor_login_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_settlements`
--
ALTER TABLE `vendor_settlements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_wallet`
--
ALTER TABLE `vendor_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_withdraw_requests`
--
ALTER TABLE `vendor_withdraw_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `weights`
--
ALTER TABLE `weights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `we_shop_for_you_info`
--
ALTER TABLE `we_shop_for_you_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdraw_settings`
--
ALTER TABLE `withdraw_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutus`
--
ALTER TABLE `aboutus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_addresses`
--
ALTER TABLE `admin_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin_dashboard_main_menu`
--
ALTER TABLE `admin_dashboard_main_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `admin_dashboard_sub_menu`
--
ALTER TABLE `admin_dashboard_sub_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `blog_info`
--
ALTER TABLE `blog_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contact_enquiries`
--
ALTER TABLE `contact_enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `customers_login_logs`
--
ALTER TABLE `customers_login_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `customer_reports`
--
ALTER TABLE `customer_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `designers_and_analysts`
--
ALTER TABLE `designers_and_analysts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `email_subscriptions`
--
ALTER TABLE `email_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fashion_desg_login_logs`
--
ALTER TABLE `fashion_desg_login_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `fashion_designer_cart`
--
ALTER TABLE `fashion_designer_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `footer_info`
--
ALTER TABLE `footer_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `heights`
--
ALTER TABLE `heights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `homepage_sliders`
--
ALTER TABLE `homepage_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `how_it_works_images`
--
ALTER TABLE `how_it_works_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `how_it_works_points`
--
ALTER TABLE `how_it_works_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `login_logs`
--
ALTER TABLE `login_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders_feedbacks`
--
ALTER TABLE `orders_feedbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders_log`
--
ALTER TABLE `orders_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `package_page_banners`
--
ALTER TABLE `package_page_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pants_sizes`
--
ALTER TABLE `pants_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products_images`
--
ALTER TABLE `products_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `products_inventory`
--
ALTER TABLE `products_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_inventory`
--
ALTER TABLE `product_inventory`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `return_policy`
--
ALTER TABLE `return_policy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `shirts_sizes`
--
ALTER TABLE `shirts_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `shoes_sizes`
--
ALTER TABLE `shoes_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `styles`
--
ALTER TABLE `styles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `subscriptions_transactions`
--
ALTER TABLE `subscriptions_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `subscription_images`
--
ALTER TABLE `subscription_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `terms_conditions`
--
ALTER TABLE `terms_conditions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_preferences`
--
ALTER TABLE `user_preferences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `vendors_gallery`
--
ALTER TABLE `vendors_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vendor_bank_details`
--
ALTER TABLE `vendor_bank_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vendor_login_logs`
--
ALTER TABLE `vendor_login_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `vendor_settlements`
--
ALTER TABLE `vendor_settlements`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendor_wallet`
--
ALTER TABLE `vendor_wallet`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `vendor_withdraw_requests`
--
ALTER TABLE `vendor_withdraw_requests`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `weights`
--
ALTER TABLE `weights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `we_shop_for_you_info`
--
ALTER TABLE `we_shop_for_you_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `withdraw_settings`
--
ALTER TABLE `withdraw_settings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
