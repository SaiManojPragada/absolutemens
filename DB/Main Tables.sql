#
# TABLE STRUCTURE FOR: access_rights
#

DROP TABLE IF EXISTS `access_rights`;

CREATE TABLE `access_rights` (
  `id` bigint(20) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `privileges_code` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `updated_at` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: admin_dashboard_main_menu
#

DROP TABLE IF EXISTS `admin_dashboard_main_menu`;

CREATE TABLE `admin_dashboard_main_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) NOT NULL,
  `menu_icon` varchar(100) DEFAULT NULL,
  `display_order` int(11) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `menu_url` varchar(100) NOT NULL DEFAULT '#',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO `admin_dashboard_main_menu` (`id`, `menu_name`, `menu_icon`, `display_order`, `created_at`, `updated_at`, `menu_url`, `status`) VALUES ('1', 'Admin Control Panel', 'fa fa-th-large', 0, '1623047703', '', '#', 1);
INSERT INTO `admin_dashboard_main_menu` (`id`, `menu_name`, `menu_icon`, `display_order`, `created_at`, `updated_at`, `menu_url`, `status`) VALUES ('13', 'Enquiries', 'fa fa-comments-o', 101, '1623119550', '', 'enquiries', 1);
INSERT INTO `admin_dashboard_main_menu` (`id`, `menu_name`, `menu_icon`, `display_order`, `created_at`, `updated_at`, `menu_url`, `status`) VALUES ('14', 'CMS Pages', 'fa fa-book', 11, '1623125694', '', 'Cms_pages', 1);
INSERT INTO `admin_dashboard_main_menu` (`id`, `menu_name`, `menu_icon`, `display_order`, `created_at`, `updated_at`, `menu_url`, `status`) VALUES ('15', 'Admin Login Logs', 'fa fa-user', 104, '1623218389', '', 'login_logs', 1);


#
# TABLE STRUCTURE FOR: admin_dashboard_sub_menu
#

DROP TABLE IF EXISTS `admin_dashboard_sub_menu`;

CREATE TABLE `admin_dashboard_sub_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) NOT NULL,
  `menu_url` varchar(100) NOT NULL,
  `main_menu_id` int(10) unsigned NOT NULL,
  `display_order` int(10) unsigned NOT NULL DEFAULT 10,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

INSERT INTO `admin_dashboard_sub_menu` (`id`, `menu_name`, `menu_url`, `main_menu_id`, `display_order`, `created_at`, `updated_at`, `status`) VALUES ('1', 'Database Backup', 'dbbackup', 1, 1, '1623050682', '', 1);
INSERT INTO `admin_dashboard_sub_menu` (`id`, `menu_name`, `menu_url`, `main_menu_id`, `display_order`, `created_at`, `updated_at`, `status`) VALUES ('2', 'Social Media Links', 'Social_media_links', 1, 2, '1623050707', '', 1);
INSERT INTO `admin_dashboard_sub_menu` (`id`, `menu_name`, `menu_url`, `main_menu_id`, `display_order`, `created_at`, `updated_at`, `status`) VALUES ('3', 'Site Settings', 'site_settings', 1, 3, '1623050729', '', 1);
INSERT INTO `admin_dashboard_sub_menu` (`id`, `menu_name`, `menu_url`, `main_menu_id`, `display_order`, `created_at`, `updated_at`, `status`) VALUES ('4', 'Change Password	', 'change_password', 1, 4, '1623050771', '', 1);
INSERT INTO `admin_dashboard_sub_menu` (`id`, `menu_name`, `menu_url`, `main_menu_id`, `display_order`, `created_at`, `updated_at`, `status`) VALUES ('5', 'Sliders', 'sliders', 2, 1, '1623051600', '', 1);


#
# TABLE STRUCTURE FOR: cms_pages
#

DROP TABLE IF EXISTS `cms_pages`;

CREATE TABLE `cms_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# TABLE STRUCTURE FOR: enquiries
#

DROP TABLE IF EXISTS `enquiries`;

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `message` longtext NOT NULL,
  `created_at` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# TABLE STRUCTURE FOR: login_logs
#

DROP TABLE IF EXISTS `login_logs`;

CREATE TABLE `login_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `login_logs` (`id`, `name`, `ip_address`, `created_at`) VALUES ('1', 'admin@swarnaandhra.com', '::1', '1623317523');


#
# TABLE STRUCTURE FOR: site_settings
#

DROP TABLE IF EXISTS `site_settings`;

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(100) DEFAULT NULL,
  `logo` varchar(100) NOT NULL COMMENT '<font color="red">Note:</font> Upload Height: 160px; Width: 184px',
  `favicon` varchar(100) NOT NULL COMMENT '<font color="red">Note:</font> Upload Height: 160px; Width: 184px',
  `contact_number` varchar(100) NOT NULL,
  `secondary_contact_number` varchar(100) DEFAULT NULL,
  `phone_number` varchar(20) NOT NULL,
  `secondary_phone_numer` varchar(20) DEFAULT NULL,
  `contact_email` varchar(60) DEFAULT NULL,
  `secondary_email` varchar(60) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `about_site` text NOT NULL,
  `google_maps` text DEFAULT NULL,
  `seo_title` text DEFAULT NULL,
  `seo_keywords` text DEFAULT NULL,
  `seo_description` text DEFAULT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `site_settings` (`id`, `site_name`, `logo`, `favicon`, `contact_number`, `secondary_contact_number`, `phone_number`, `secondary_phone_numer`, `contact_email`, `secondary_email`, `address`, `about_site`, `google_maps`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES (1, 'Swarnandhra', '5bda80392a9a4182fb2327be9092a0e5.png', '17e3bd0556f4b81f561470ba244ac04a.png', '(+91) 123 456 7890', '', '(+91) 123 456 7890', '', 'support@swarnandhra.com', '', '<p>Address&nbsp;Address&nbsp;AddressAddressAddress&nbsp;Address&nbsp;Address&nbsp;Address</p>\r\n\r\n<p>Address&nbsp;AddressAddressv&nbsp;</p>\r\n', '<p>We denounce with righteous indi gnation and dislike men who are so beguiled and demoralized by the charms of pleasure of your moment, so blinded by desire those who fail weakness.</p>\r\n', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12613.47608654031!2d-122.50864139156641!3d37.781390759825626!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808587b42ebc4ef9%3A0x5e73c1ad9c8a015a!2sSutro%20Heights!5e0!3m2!1sen!2sus!4v1623317758682!5m2!1sen!2sus\" width=\"600\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\"></iframe>', 'Swarnandhra', 'Swarnandhra', 'Swarnandhra', '', '1623317770');


#
# TABLE STRUCTURE FOR: social_media
#

DROP TABLE IF EXISTS `social_media`;

CREATE TABLE `social_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `google` varchar(100) DEFAULT NULL,
  `youtube` varchar(100) DEFAULT NULL,
  `linkedin` varchar(100) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `pinterest` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# TABLE STRUCTURE FOR: users
#

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(60) DEFAULT NULL COMMENT 'This will be username, email is username',
  `mobile` varchar(20) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `salt` varchar(25) DEFAULT NULL,
  `created_at` varchar(30) NOT NULL,
  `updated_at` varchar(30) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `role_id`, `name`, `username`, `mobile`, `password`, `salt`, `created_at`, `updated_at`, `status`) VALUES ('1', '1', 'Admin', 'admin@swarnaandhra.com', NULL, '6058f20919b53ca150f513921f18d89e', 's45GLbESn9403', '1479899025', NULL, 1);


