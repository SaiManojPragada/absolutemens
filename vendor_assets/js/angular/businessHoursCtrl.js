$(function () {
    $("#restaurant_category_form").validate({
        rules: {
            week_name: {
                required: true,
            },
            from_time: {
                required: true,
            },
            is_recommened: {
                required: true,
            }
        },
        messages: {
            week_name: {
                required: "Enter Week Name",
            },
            from_time: {
                required: "Enter From Time",
            },
            is_recommened: {
                required: "Select is Recommended",
            },

        }
    });
});
app.controller("businessHoursCtrl", function ($scope, dataService, vendorService, $timeout) {
    $scope.dataList = {};
    $scope.type = 0;


    $scope.getBusinessTimings = function () {
        vendorService.getData("business_hours", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                $(".error_place_holder").html(response.data.message);
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                toastr.success(response.data.message);
                $scope.dataList = response.data.data;
            }
        });
    }

    $scope.saveTimings = function () {

        if ($("#restaurant_business_hours_form").valid()) {
            dataService.getLoading();
            vendorService.postData("business_hours/add", {
                business_hours: $scope.dataList
            }).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    // location.reload();
                }
            });
        }
    };

    $scope.getBusinessTimings();


});