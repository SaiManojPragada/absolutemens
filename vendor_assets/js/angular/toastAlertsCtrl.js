app.controller("toastAlertsCtrl", function ($scope, $sce) {
    function showToast() {
        angular.element("#snackbar").addClass("show");
        document.getElementById("notificationSound").play();
        hideToast();
    }
    function showNewOrderToast() {
        angular.element("#snackbar").addClass("show");
        document.getElementById("newOrderReceivedSound").play();
        hideToast();
    }

    function hideToast() {
        setTimeout(function () {
            angular.element("#snackbar").removeClass("show");
        }, 7000);
    }

    angular.element("#snackbar").on("mouseover", function () {
        showToast();
    });


    socket.on("STORE NEW SERVICE ORDER", function (response) {
        console.log(response);
        $scope.toastMsg = "New Service Order #" + response.ref_id + "<br/>Delivery Address : " + response.address;
        $scope.toastMsg = $sce.trustAsHtml($scope.toastMsg);
        $scope.$apply();
        showNewOrderToast();
    });
});