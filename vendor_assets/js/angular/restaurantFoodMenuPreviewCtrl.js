$(function () {
    $("#restaurant_category_form").validate({
        rules: {
            name: {
                required: true,
            },
            is_recommened: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Enter Category Name",
            },
            is_recommened: {
                required: "Select is Recommended",
            },

        }
    });
});
app.controller("restaurantFoodMenuPreviewCtrl", function ($scope, dataService, vendorService, $timeout) {
    $scope.foodMenu = [];
    $scope.foodItem = {};

    getFoodMenuPreview();



    function getFoodMenuPreview() {
        dataService.getLoading();
        vendorService.postData("food_items/preview", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.foodMenu = response.data.data;
                if ($scope.foodMenu.length > 0) {
                    $scope.showFoodItem($scope.foodMenu[0]);
                }
            }
        });
    }
    ;

    $scope.showFoodItem = function (foodItem) {
        $scope.foodItem = angular.copy(foodItem);
    };

    $scope.UpdateRestaurantCategoryStatus = function (obj) {
        obj.available = !obj.available;
        if (obj.available == true) {
            available = false;
        } else {
            available = true;
        }
        vendorService.postData("food_items/mark_category_as_sold_out_or_available", {'category_id': obj.id, 'available': available}).then(function (response) {
            getFoodMenuPreview();
        });
    };

    $scope.UpdateRestaurantFoodItemStatus = function (obj) {
        obj.available = !obj.available;
        if (obj.available == true) {
            available = false;
        } else {
            available = true;
        }
        vendorService.postData("food_items/mark_food_item_as_sold_out_or_available", {'id': obj.id, 'available': available}).then(function (response) {
            getFoodMenuPreview();
        });
    };

});