app.controller("ordersCtrl", function ($scope, dataService, vendorService, $filter, $timeout, $sce) {

    $scope.orders = {};

    $scope.getOrders = function (param) {
        dataService.getLoading();
        vendorService.postData("my_orders", {param: param}).then(function (response) {
            dataService.closeLoading();
            console.log(response.data);
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.orders = response.data.data;
            }
        });
    };

    $scope.changeStatus = function (id, order_id, val, param) {
        if (confirm("Are you sure Want to Update this Order Status ?")) {
            dataService.getLoading();
            vendorService.postData("my_orders/update_order_status", {id: id, order_id: order_id, order_status: val}).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $scope.getOrders(param);
                }
            });
        } else {
            $scope.getOrders(param);
        }
    };

    $scope.changeProductStatus = function (id, real_id, order_id, val, param) {
        if (confirm("Are you sure Want to Update this Order Status ?")) {
            dataService.getLoading();
            vendorService.postData("my_orders/update_order_product_status", {id: id, real_id: real_id, order_id: order_id, order_status: val}).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $scope.getOrders(param);
                }
            });
        } else {
            $scope.getOrders(param);
        }
    };
    $scope.log_order_id = "";
    $scope.logs = {};
    $scope.showLogs = function (id) {
        $scope.log_order_id = "";
        console.log(id);
        dataService.getLoading();
        $('#logsModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        vendorService.postData("my_orders/get_logs", {order_real_id: id}).then(function (response) {
            console.log(response.data);
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
                $('#logsModal').modal('toggle');
            }
            if (response.data.err_code === "valid") {
                $scope.logs = response.data.data.logs;
                $scope.logs_order_id = response.data.data.order_id;
            }
        });
        dataService.closeLoading();
    };


});