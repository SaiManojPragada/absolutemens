$.fn.modal.Constructor.prototype.enforceFocus = function () {};
$(function () {
    $("#update_profile_form").validate({
        rules: {
            business_name: {
                required: true,
                maxlength: 32,
                minlength: 3
            },
            contact_email: {
                required: true,
                maxlength: 32,
                minlength: 10
            },
            contact_number: {
                required: true,
                maxlength: 10,
                minlength: 10
            },
            alternate_contact_number: {
                required: true,
                maxlength: 10,
                minlength: 10
            },
            cities_id: {
                required: true
            },
            address: {
                required: true,
                minlength: 10
            },
            pincode: {
                required: true,
                maxlength: 6,
                minlength: 6
            },
            gst_number: {
                required: true,
                maxlength: 15,
                minlength: 15
            }

        },
        messages: {
            business_name: {
                required: "Enter Vendor name ",
                minlength: "Minimum 3 Characters required",
                maxlength: "Maximum 32 Characters only"
            }
        }
    });
});

app.controller("updateProfileCtrl", function ($scope, dataService, vendorService) {
    $scope.profile = {};
    $scope.cities = {};
    $scope.updateProfile = function () {
        if ($("#update_profile_form").valid()) {
            dataService.getLoading();
            vendorService.postData("update_profile", $scope.profile).then(function (response) {
                dataService.closeLoading();
                console.log(response);
                $("#update_profile_form").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                }
                ;
            });
        }
        ;
    };

    $scope.getProfileInfo = function () {
        vendorService.getData("profile", {}).then(function (response) {
            dataService.closeLoading();
            $scope.getCities();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.profile = response.data.vendor_details;
            }
        });
    };

    $scope.getCities = function () {
        vendorService.getCData("districts_cities/get_cities").then(function (response) {
            $scope.cities = response.data.data;
        });
    };



});