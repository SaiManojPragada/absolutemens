app.controller("allOrdersCtrl", function ($scope, dataService, vendorService, $timeout, $sce) {

    $scope.filter = {
        q: "",
        type: "", //service status
        from_date: "",
        to_date: "",
        page: 1
    };

    $scope.getOrders = function () {
        $scope.ordersList = [];
        dataService.getLoading();
        vendorService.getAllOrdersList($scope.filter).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code == "valid") {

                $scope.ordersList = response.data.data.results;
                $scope.ordersPagination = response.data.pagination;
                console.log($scope.ordersPagination);
                if ($scope.ordersPagination.pagination.length > 0) {
                    $scope.ordersPagination.pagination = $sce.trustAsHtml($scope.ordersPagination.pagination);
                }
            }
        });
    };
    $scope.getOrders();

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getOrders();
    });

    $scope.ShowOrderPopup = function (obj) {
        $scope.order = obj;
        $('#orderDetailsModal').modal("show");
    };

});