$(function () {
  $("#galleryFrom").validate({
    rules: {
      title: {
        required: true,
      }
    },
    messages: {
      title: {
        required: "Please enter title",
      }
    }
  });
});
app.controller("restaurantDocumentsCtrl", function ($scope, dataService, vendorService, $timeout) {
  $scope.documents = {};

  getDocuments();

  function getDocuments() {
    dataService.getLoading();
    vendorService.getData("business_documents", {}).then(function (response) {
      dataService.closeLoading();
      if (response.data.err_code === "invalid") {
        // toastr.warning(response.data.message);
      }
      if (response.data.err_code === "valid") {
        // toastr.success(response.data.message);
        $scope.documents = response.data.data;
      }
    });
  };
});