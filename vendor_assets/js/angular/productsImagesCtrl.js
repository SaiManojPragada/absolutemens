$(function () {
    $("#products_images_form").validate({
        rules: {
            image: {
                required: true
            },
            status: {
                required: true
            }
        },
        messages: {
            image: {
                required: "Select an Image"
            },
            status: {
                required: "Select Image Status"
            }
        }
    });
});
app.controller("productsImagesCtrl", function ($scope, dataService, vendorService, $filter, $timeout, $sce) {


    $scope.images = {};
    $scope.block = {};

    function initializeObj() {
        $scope.block = {};
    }

    initializeObj();
    var id_m = 0;
    $scope.getProductImages = function (id) {
        id_m = id;
        dataService.getLoading();
        console.log(id);
        vendorService.postData("products_images", {id: id}).then(function (response) {
            dataService.closeLoading();
            console.log(response.data);
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.images = response.data.data;
            }
        });
    };

    $scope.addUpdateProductImage = function () {
        if ($("#products_images_form").valid()) {
            dataService.getLoading();
            console.log($scope.block);

            vendorService.postData('products_images/post_image', $scope.block).then(function (response) {
                dataService.closeLoading();
                console.log(response.data);
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $scope.getProductImages($scope.block.product_id);
                    initializeObj();
                    $("#productImageModal").modal("hide");
                }
            });
        }
    };




    $scope.addProductImage = function () {
        initializeObj();
        $scope.block.product_id = id_m;
        $('#productImageModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    };


    $scope.deleteImage = function (item) {
        var isDelete = confirm('Do you want to delete Image ?');
        if (isDelete) {
            dataService.getLoading();
            vendorService.postData("products_images/delete", {
                id: item.id, image: item.image
            }).then(function (response) {
                console.log(response);
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $("#productImageModal").modal("hide");
                    $scope.images = {};
                    $scope.getProductImages(item.product_id);
                }
            });
        }
    };

});