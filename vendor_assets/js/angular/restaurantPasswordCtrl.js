$.fn.modal.Constructor.prototype.enforceFocus = function () {};
$(function () {
    $("#restaurant_password_form").validate({
        rules: {
            current_password: {
                required: true,
                // minlength: 6,
                maxlength: 32
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            },
            password_cnf: {
                required: true,
                minlength: 6,
                maxlength: 32,
                equalTo: "#restaurantPassId"
            },

        },
        messages: {
            current_password: {
                required: "Enter Current Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only"
            },
            password: {
                required: "Enter New Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only"
            },
            password_cnf: {
                required: "Enter Confirm Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only",
                equalTo: "Password and Confirm Password should be Same"
            }
        }
    });
});
app.controller("restaurantPasswordCtrl", function ($scope, dataService, vendorService, $timeout) {
    $scope.changePass = {};

    $scope.updateRestaturantPassword = function () {
        if ($("#restaurant_password_form").valid()) {
            dataService.getLoading();
            vendorService.postData("update_password", $scope.changePass).then(function (response) {
                dataService.closeLoading();
                console.log(response);
                $("#restaurant_password_form").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $scope.changePass = {};
                }
                ;
            });
        }
        ;
    };



});