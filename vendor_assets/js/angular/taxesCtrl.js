$(function () {
  $("#restaurant_tax_form").validate({
    rules: {
      tax_name: {
        required: true,
      },
      tax_type: {
        required: true,
      },
	  tax_amount:{
		  required: true,
	  },
	  tax_apply_above:{
		  required: true,
	  },
	  tax_status:{
		  required: true,
	  }
    },
    messages: {
      tax_name: {
        required: "Enter Tax Name",
      },
      tax_type: {
        required: "Choose Tax Calculation type",
      },
	  tax_amount: {
        required: "Enter Tax amount",
      },
	  tax_apply_above: {
        required: "Enter what is the minimum to apply tax",
      }
    }
  });
});
app.controller("taxesCtrl", function ($scope, dataService, vendorService, $timeout) {
  $scope.restaurant = {};
  function initializeObj(){
	$scope.tax = {
			tax_name: "",
			tax_type: "Percentage",
			tax_amount: 0,
			tax_apply_above: 0,
			tax_status: "Active"
	  }  
  }
  initializeObj();
  
  $scope.type = 0;

  getTaxes();

  $scope.addUpdateTax = function () {
    if ($("#restaurant_tax_form").valid()) {
      dataService.getLoading();
      vendorService.postData("taxes/add", $scope.tax).then(function (response) {
        dataService.closeLoading();
        if (response.data.err_code === "invalid") {
          $(".error_place_holder").html(response.data.message);
          toastr.warning(response.data.message);
        }
        if (response.data.err_code === "valid") {
          toastr.success(response.data.message);
		  getTaxes();
			$('#taxModal').modal("hide");
        }
      });
    }
  };

  $scope.deleteTax = function (tax) {
    var isDelete = confirm('Do you want to delete Tax ?');
    if (isDelete) {
      dataService.getLoading();
      vendorService.postData("taxes/delete", {
        id: tax.id
      }).then(function (response) {
        dataService.closeLoading();
        if (response.data.err_code === "invalid") {
          toastr.warning(response.data.message);
        }
        if (response.data.err_code === "valid") {
          toastr.success(response.data.message);
		  getTaxes();
        }
      });
    }
  };

  function getTaxes() {
    dataService.getLoading();
    vendorService.postData("taxes", {}).then(function (response) {
      dataService.closeLoading();
      if (response.data.err_code === "invalid") {
        toastr.warning(response.data.message);
      }
      if (response.data.err_code === "valid") {
        $scope.taxes = response.data.data;
      }
    });
  };

  $scope.addTaxModal = function(){
	  initializeObj();
	$('#taxModal').modal({
		backdrop: 'static',
		keyboard: false
	});
  };

  $scope.editTaxModal = function(obj){
	  
	  $scope.tax = angular.copy(obj);
	$('#taxModal').modal({
		backdrop: 'static',
		keyboard: false
	});
  };
});