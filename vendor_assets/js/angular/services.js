angular.module("application.services", [])
        .service("authService", function ($cookies) {
            return {
                isLogged: function () {
                    if ($cookies.get("token")) {
                        return true;
                    } else {
                        location.href = websiteUrl + "login";
                    }
                },
                checkLoginStatus: function () {
                    if ($cookies.get("token")) {
                        return true;
                    } else {
                        return false;
                    }
                },
                checkCustomerLoginStatus: function () {
                    return this.getCustomerToken();
                },
                getCustomerToken: function () {
                    if ($cookies.get("token") && $cookies.get("user_type") == 2) {
                        return $cookies.get("token");
                    } else {
                        return false;
                    }
                },
                getLoginToken: function () {
                    if ($cookies.get("token")) {
                        return $cookies.get("token");
                    } else {
                        return false;
                    }
                }
            };
        })
        .service("dataService", function ($cookies, $http, authService) {
            return {
                getData: function (url, data) {
                    return $http.get(apiBaseUrl + url + "?" + $.param(data));
                },
                postData: function (url, data) {
                    var postData = $.param(data);
                    var post_url = apiBaseUrl + url;
                    return $http.post(post_url, postData, config);
                },
                getLoading: function () {
                    $.LoadingOverlay("show", {
                        zIndex: 99999999999999
                    });
                },
                closeLoading: function () {
                    $.LoadingOverlay("hide");
                },
                getStates: function () {
                    return $http.get(apiBaseUrl + "locations");
                },
                getdistricts: function (state_id) {
                    return $http.get(apiBaseUrl + "locations/districts?states_id=" + state_id);
                },
                getCities: function (state_id, district_id) {
                    return $http.get(apiBaseUrl + "locations/cities?states_id=" + state_id + '&districts_id=' + district_id);
                },
                getTermsAndConditions: function () {
                    var url = apiBaseUrl + "cms/terms_and_conditions";
                    return $http.get(url);
                },
                getUserIdentityQuerSting: function () {
                    return "token=" + this.getUserToken();
                },
                getUserToken: function () {
                    return localStorage.getItem("token");
                },
                doRegister: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "register";
                    return $http.post(url, postData, config);
                },
                sendOTP: function (mobile) {
                    return $http.get(apiBaseUrl + "send_otp?mobile=" + mobile);
                },
                verifyOtp: function (mobile, otp) {
                    return $http.get(apiBaseUrl + "verify_otp?mobile=" + mobile + "&otp=" + otp);
                },
                doLogin: function (user) {
                    return $http.get(apiBaseUrl + "login?mobile=" + user.mobile);
                },
                getSubscriptionPlans: function () {
                    return $http.get(apiBaseUrl + "subscription_plans");
                },
                addToLikedVideos: function (homepage_video_id) {
                    var url = apiBaseUrl + "like/add?" + this.getUserIdentityQuerSting();
                    url += "&homepage_video_id=" + homepage_video_id;
                    return $http.get(url);
                },
                validateReferralCode: function (referral_code) {
                    var url = apiBaseUrl + "referral_code/validate?" + this.getUserIdentityQuerSting();
                    url += "&referral_code=" + referral_code;
                    return $http.get(url);
                },
                doSubmitEnquiryToProfessional: function (data) {
                    var url = apiBaseUrl + "submit_inquiry_to_professional/submit?" + this.getUserIdentityQuerSting()
                    return $http.post(url, data, fileConfig);
                },
                leftPad: function (number) {
                    /* add zero to numbers less than 10,Eg: 2 -> 02Â */
                    return ((number < 10 && number >= 0) ? '0' : '') + number;
                },
                getFaqs: function () {
                    var url = apiBaseUrl + "cms/faqs";
                    return $http.get(url);
                },
                doSubmitContactUsRequest: function (data) {
                    var url = apiBaseUrl + "submit_contact_us_form";
                    var postData = $.param(data);
                    return $http.post(url, postData, config);
                },
                doSubmitFeedbackRequest: function (data) {
                    var url = apiBaseUrl + "submit_feedback_form";
                    var postData = $.param(data);
                    return $http.post(url, postData, config);
                },
                getContactUsDetailsData: function () {
                    var url = apiBaseUrl + "contact_us_details";
                    return $http.get(url);
                },
                doSubmitSaleInquiryRequest: function (data) {
                    var url = apiBaseUrl + "submit_sales_inquiry_feedback_form";
                    var postData = $.param(data);
                    return $http.post(url, postData, config);
                },
                getServiceInfo: function (sub_category_id) {
                    var url = apiBaseUrl + "service_view?sub_category_id=" + sub_category_id;
                    return $http.get(url);
                },
                sendInquiry: function (data) {
                    var url = apiBaseUrl + "submit_contact_us_form";
                    var postData = $.param(data);
                    return $http.post(url, postData, config);
                },
                getPincodeLocations: function (pincode) {
                    var url = apiBaseUrl + "locations_by_pincode?";
                    url += "pincode=" + pincode;
                    return $http.get(url);
                },
                getApepdclBillDetails: function (bill_number) {
                    var url = apiBaseUrl + "bill_details/apepdcl?";
                    url += "bill_number=" + bill_number;
                    return $http.get(url);
                }
            };
        })
        .service("customerService", function ($http, dataService) {
            return {
                doUpdateProfile: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/update_profile?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getProfile: function () {
                    var url = apiBaseUrl + "customer/profile?" + dataService.getUserIdentityQuerSting();
                    return $http.get(url);
                },
                updateProfilePicture: function (data) {
                    var url = apiBaseUrl + "customer/update_profile/picture?" + dataService.getUserIdentityQuerSting()
                    return $http.post(url, data, fileConfig);
                },
                getPrivacyPolicy: function () {
                    var url = apiBaseUrl + "customer/cms/privacy_policy";
                    return $http.get(url);
                },
                createBooking: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/submit_service_request?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyServices: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/my_services?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyServiceInfo: function (service_id) {
                    var url = apiBaseUrl + "customer/my_services/view?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.get(url);
                },
                getMyOrders: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/my_orders?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyOrderInfo: function (order_id) {
                    var url = apiBaseUrl + "customer/my_orders/view?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + order_id;
                    return $http.get(url);
                },
                setUpdatedStatus: function (service_id, data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/my_services/update_status?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.post(url, postData, config);
                },
                setOrderUpdatedStatus: function (service_id, data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/my_orders/update_status?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.post(url, postData, config);
                },
                setFeedback: function (service_id, data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/my_services/update_feedback?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.post(url, postData, config);
                },
                setOrderFeedback: function (service_id, data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/my_orders/update_feedback?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.post(url, postData, config);
                },
                addToCart: function (product_id) {
                    var url = apiBaseUrl + "cart/add?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + product_id;
                    return $http.get(url);
                },
                updateQty: function (id, qty) {
                    var url = apiBaseUrl + "cart/update_qty?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + id;
                    url += "&qty=" + qty;
                    return $http.get(url);
                },
                getCart: function () {
                    var url = apiBaseUrl + "cart/get?" + dataService.getUserIdentityQuerSting();
                    return $http.get(url);
                },
                removeCartItem: function (id) {
                    var url = apiBaseUrl + "cart/remove?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + id;
                    return $http.get(url);
                },
                createOrder: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/submit_product_request?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyTransactions: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "customer/transactions?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                setWithDrawRequest: function (amount) {
                    var url = apiBaseUrl + "customer/submit_withdraw_money_request?" + dataService.getUserIdentityQuerSting();
                    url += "&amount=" + amount;
                    url += "&action_type=" + 'WITHDRAW';
                    return $http.get(url);
                },
                addMoneyRequest: function (amount) {
                    var url = apiBaseUrl + "customer/add_payment_request?" + dataService.getUserIdentityQuerSting();
                    url += "&amount=" + amount;
                    url += "&source=Website";
                    return $http.get(url);
                },
                createSubscriptionRequest: function (plan_id) {
                    var url = apiBaseUrl + "customer/add_subscription_request?" + dataService.getUserIdentityQuerSting();
                    url += "&plan_id=" + plan_id;
                    return $http.get(url);
                }
            };
        })
        .service("vendorService", function ($http, dataService) {
            return {
                getData: function (url, data) {
                    return $http.get(apiBaseUrl + 'vendor/' + url + '?' + $.param(data));
                },

                getCData: function (url) {
                    return $http.get(apiBaseUrl + 'vendor/' + url);
                },
                postData: function (url, data) {
                    var postData = $.param(data);
                    var post_url = apiBaseUrl + "vendor/" + url;
                    return $http.post(post_url, postData, config);
                },
                postFormData: function (url, data) {
                    var post_url = apiBaseUrl + "vendor/" + url;
                    return $http.post(post_url, data, fileConfig);
                },
                getProfile: function () {
                    var url = apiBaseUrl + "vendor/profile?" + dataService.getUserIdentityQuerSting();
                    return $http.get(url);
                },
                doUpdateProfile: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "vendor/update_profile?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyServices: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "vendor/my_services?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyServiceInfo: function (service_id) {
                    var url = apiBaseUrl + "vendor/my_services/view?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.get(url);
                },
                setMarkAsAccepted: function (service_id) {
                    var url = apiBaseUrl + "vendor/my_services/mark_as_accepted?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.get(url);
                },
                setMarkAsRejected: function (service_id) {
                    var url = apiBaseUrl + "vendor/my_services/mark_as_rejected?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.get(url);
                },
                setUpdatedStatus: function (service_id, data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "vendor/my_services/update_status?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.post(url, postData, config);
                },
                getMyOrders: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "vendor/my_orders?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyOrderInfo: function (order_id) {
                    var url = apiBaseUrl + "vendor/my_orders/view?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + order_id;
                    return $http.get(url);
                },
                setOrderMarkAsAccepted: function (service_id) {
                    var url = apiBaseUrl + "vendor/my_orders/mark_as_accepted?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.get(url);
                },
                setOrderMarkAsRejected: function (service_id) {
                    var url = apiBaseUrl + "vendor/my_orders/mark_as_rejected?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.get(url);
                },
                setOrderUpdatedStatus: function (service_id, data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "vendor/my_orders/update_status?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.post(url, postData, config);
                },
                setWithDrawRequest: function (amount) {
                    var url = apiBaseUrl + "vendor/submit_withdraw_money_request?" + dataService.getUserIdentityQuerSting();
                    url += "&amount=" + amount;
                    return $http.get(url);
                },
                getMyTransactions: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "vendor/transactions?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getAllOrdersList: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "vendor/all_orders?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                }
            };
        })
        .service("franchiseService", function ($http, dataService) {
            return {
                getProfile: function () {
                    var url = apiBaseUrl + "franchise/profile?" + dataService.getUserIdentityQuerSting();
                    return $http.get(url);
                },
                doUpdateProfile: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "franchise/update_profile?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyServices: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "franchise/my_services?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyServiceInfo: function (service_id) {
                    var url = apiBaseUrl + "franchise/my_services/view?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + service_id;
                    return $http.get(url);
                },
                getMyOrders: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "franchise/my_orders?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyOrderInfo: function (order_id) {
                    var url = apiBaseUrl + "franchise/my_orders/view?" + dataService.getUserIdentityQuerSting();
                    url += "&id=" + order_id;
                    return $http.get(url);
                },
                getMyTransactions: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "franchise/transactions?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyCustomers: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "franchise/my_customers?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getNewCustomers: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "franchise/new_customers?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                getMyVendors: function (data) {
                    var postData = $.param(data);
                    var url = apiBaseUrl + "franchise/my_vendors?" + dataService.getUserIdentityQuerSting();
                    return $http.post(url, postData, config);
                },
                setMarkAsAdded: function (customer_id) {
                    var url = apiBaseUrl + "franchise/my_customers/add?" + dataService.getUserIdentityQuerSting();
                    url += "&customer_id=" + customer_id;
                    return $http.get(url);
                },
                setMarkAsRemoved: function (customer_id) {
                    var url = apiBaseUrl + "franchise/my_customers/remove?" + dataService.getUserIdentityQuerSting();
                    url += "&customer_id=" + customer_id;
                    return $http.get(url);
                },
                setUpCreditLimit: function (customer_id, credit_limit) {
                    var url = apiBaseUrl + "franchise/my_customers/credit_limit?" + dataService.getUserIdentityQuerSting();
                    url += "&customer_id=" + customer_id;
                    url += "&credit_limit=" + credit_limit;
                    return $http.get(url);
                },
                setUpWalletAmount: function (data) {
                    var postData = $.param(data);
                    console.log(postData);
                    var url = apiBaseUrl + "franchise/my_customers/update_wallet?" + dataService.getUserIdentityQuerSting() + '&' + postData;
                    return $http.get(url);
                },
                setCustomerStatus: function (customer_id, new_status) {
                    var url = apiBaseUrl + "franchise/my_customers/block_unblock?" + dataService.getUserIdentityQuerSting();
                    url += "&customer_id=" + customer_id;
                    url += "&new_status=" + new_status;
                    return $http.get(url);
                }
            };
        });