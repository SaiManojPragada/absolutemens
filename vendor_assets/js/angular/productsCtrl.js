

$(function () {
    $("#stock_form").validate({
        rules: {
            type: {
                required: true
            },
            quantity: {
                required: true,
                digits: true
            }
        },
        messages: {
            type: {
                required: "Type is required"
            },
            quantity: {
                required: "Quantity is required",
                digits: "Enter only digits"
            }
        }
    });

    $("#quantity_form").validate({
        rules: {
            sizes_id: {
                required: true
            },
            price: {
                required: true,
                digits: true
            },
            quantity: {
                required: true,
                digits: true
            }
        },
        messages: {
            type: {
                required: "Select a Size"
            },
            price: {
                required: "Price is required",
                digits: "Enter only digits"
            },
            quantity: {
                required: "Quantity is required",
                digits: "Enter only digits"
            }
        }
    });


    $("#products_form").validate({
        rules: {
            categories_id: {
                required: true
            },
            sub_categories_id: {
                required: true
            },
            title: {
                required: true,
                minlength: 3
            },
            sizes_id: {
                required: true
            },
            quantity_available: {
                required: true
            },
            product_id: {
                required: true
            },
            brand: {
                required: true,
                minlength: 3
            },
            brand_code: {
                required: true,
                minlength: 3
            },
            description: {
                required: true,
                minlength: 30
            },
            status: {
                required: true
            }
        },
        messages: {
            categories_id: {
                required: "Select Category"
            },
            sub_categories_id: {
                required: "Select Sub Category"
            },
            title: {
                required: "Enter Item Name",
                minlength: "Product Title is too short"
            },
            sizes_id: {
                required: "Select Size"
            },
            quantity_available: {
                required: "Enter Available Quantity"
            },
            brand: {
                required: "Enter Brand Name",
                minlength: "Brand Name is Too Short"
            },
            brand_code: {
                required: "Enter Brand Code"
            },
            description: {
                required: "Enter Product Desctription",
                minlength: "Product description is too short"
            },
            status: {
                required: "Select Product Status"
            }

        }
    });


});



app.controller("productsCtrl", function ($scope, dataService, vendorService, $filter, $timeout, $sce) {
    function initializeObj() {
        $scope.item = {
            categories_id: '',
            title: '',
            sizes_id: '',
            quantity_available: '',
            brand: '',
            brand_code: '',
            price: ''
        };
        $scope.filter = {
            page: 1,
            product_inventory_id
        };
        $scope.stockObj = {
            quantity: '',
            type: '',
            product_inventory_id
        }
    }

    initializeObj();
    $scope.type = 0;
    $scope.newinv = {};

    getCategories();

    $scope.addUpdateProduct = function () {
        if ($("#products_form").valid()) {
            var dataFD = new FormData();


            $.each($scope.item, function (key, value) {
                dataFD.append(key, value);
            });
            var regex = /^[A-Za-z0-9 ]+$/;
            if (!regex.test($scope.item.title)) {
                return false;
            }
            var regex = /^[A-Za-z0-9 ]+$/;
            if (!regex.test($scope.item.brand)) {
                return false;
            }
            var regex = /^[a-zA-Z0-9]{3,7}$/;
            if (!regex.test($scope.item.brand_code)) {
                return false;
            }
            var url = "products/add";
            dataService.getLoading();
            vendorService.postFormData(url, dataFD).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    $("#products_form").trigger("reset");
                    toastr.success(response.data.message);
                    initializeObj();
                    $("#productModal").modal("hide");
                    $scope.getProducts();
                }
            });
        }
    };

    $scope.getStockList = function () {
        dataService.getLoading();
        dataService.postData("vendor/products/stock_list?page=" + $scope.filter.page, $scope.filter).then(function (response) {
            dataService.closeLoading();
            $scope.stockList = response.data.data.data;
            $scope.product_details = response.data.data.product_details;
            $scope.stockListPagination = response.data.pagination;
            if ($scope.stockListPagination.pagination.length > 0) {
                $scope.stockListPagination.pagination = $sce.trustAsHtml($scope.stockListPagination.pagination);
            }
        });
    }

    $scope.deleteProduct = function (item) {
        var isDelete = confirm('Do you want to delete Product ?');
        if (isDelete) {
            dataService.getLoading();
            vendorService.postData("products/delete", {
                id: item.id
            }).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $("#productsModal").modal("hide");
                    $scope.getProducts();
                }
            });
        }
    };

    function getCategories() {
        dataService.getLoading();
        vendorService.postData("Categories", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.categories = response.data.data;
            }
        });
    }
    ;

    $scope.getSizes = function (val) {
        dataService.getLoading();
        vendorService.postData("Sizes", {cat: val}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.sizes = response.data.data;
            }
        });
    };
    getRandomNumber();
    function getRandomNumber() {
        vendorService.postData("Random_generator", {column: 'product_id', table: 'products'}).then(function (response) {
            $scope.random_number = response.data.data;
        });
    }

    $scope.generateProductCode = function () {
        $scope.item.product_id = $scope.item.brand_code + $scope.random_number;
    };

    $scope.getProducts = function () {
        dataService.getLoading();
        vendorService.postData("products", {}).then(function (response) {

            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.products = response.data.data.results;

            }

            dataService.closeLoading();
        });
    };

    $scope.product = {};
    $scope.filterObj = {};
    $scope.searchItems = function () {
        dataService.getLoading();
        vendorService.postData("products/get_products", $scope.filterObj).then(function (response) {
            $scope.products = {};
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.products = response.data.data.results;
            }
        });
    };


    $scope.getSubcategories = function (category_id) {
        dataService.getLoading();
        vendorService.postData("products/get_sub_categories", {category_id: category_id}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
                $scope.sub_categories = [];
            }
            if (response.data.err_code === "valid") {
                $scope.sub_categories = response.data.data;
            }
        });
    };

    $scope.getSingleProduct = function (id) {
        dataService.getLoading();
        vendorService.postData("products/single/" + id + '?inventory_id=' + product_inventory_id, {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.product = response.data.data;
            }
        });
    };


    $scope.getProductsWithImages = function () {
        dataService.getLoading();
        vendorService.postData("products/images", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.products = response.data.data.results;
                console.log($scope.products);
            }
        });
    };

    $scope.getProductsWithImagesById = function (id) {
        dataService.getLoading();
        vendorService.postData("products/images", {id: id}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.products = response.data.data.results;
                console.log($scope.products);
            }
        });
    };


    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getStockList();
    });
    //$scope.getFoodItems();


    $scope.addProduct = function () {
        initializeObj();
        $('#productModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    };

    $scope.manageQuantity = function (obj) {
        $scope.getSizes(obj.categories_id);
        $scope.item = angular.copy(obj);
        $scope.getInventory(obj.id, obj.categories_id);
        $scope.getSizes(obj.categories_id);
        $('#quantityModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    };

    $scope.ManageStock = function () {
        $('#stockModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    }

    $scope.change_product_status = function (id) {
        dataService.getLoading();
        vendorService.postData("products/availability/" + id, {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
                $scope.getProducts();
            }
            if (response.data.err_code === "valid") {
                $scope.getProducts();
            }
        });
    }

    $scope.inventory = {};
    $scope.getInventory = function (id, category_id) {
        $scope.newinv.product_id = id;
        $scope.newinv.category_id = category_id;
        dataService.getLoading();
        vendorService.postData("products/inventory/" + id, {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                $scope.inventory = {};
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                toastr.success(response.data.message);
                $scope.inventory = response.data.data;
                console.log($scope.inventory);
            }
        });
    };


    $scope.addProductQuantity = function () {
        if ($("#quantity_form").valid()) {
            dataService.getLoading();
            vendorService.postData('products/add_to_inventory', $scope.newinv).then(function (response) {
                var pid = $scope.newinv.product_id;
                var cid = $scope.newinv.category_id;
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    $scope.getInventory(pid, cid);
                    console.log(response.data);
                }
                $scope.newinv = {};
                $scope.newinv.product_id = pid;
                $scope.newinv.category_id = cid;
                dataService.closeLoading();
            });
        }
    };


    $scope.editProductInventory = function (obj) {
        dataService.getLoading();
        $scope.newinv = angular.copy(obj)
        dataService.closeLoading();
    };

    $scope.deleteProductInventory = function (obj) {
        var isDelete = confirm('Do you want to delete Entry from this product Inventory ?');
        if (isDelete) {
            dataService.getLoading();
            vendorService.postData('products/delete_from_inventory', {id: obj.id}).then(function (response) {
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.warning(response.data.message);
                    $scope.getInventory(obj.product_id, obj.category_id);
                }
                $scope.newinv = {};
                dataService.closeLoading();
            });
        }
    }

    $scope.EditProduct = function (obj) {
        initializeObj();
        $scope.getSizes(obj.categories_id);
        $scope.item = angular.copy(obj);
        $scope.getSubcategories($scope.item.categories_id);
        $('#productModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    };

    $scope.AddOrRemoveStock = function () {
        if ($('#stock_form').valid()) {
            if ($scope.stockObj.type == "credit") {
                $scope.stockObj.action_for = "credit_from_vendor";
            } else {
                $scope.stockObj.action_for = "debit_from_vendor";
            }
            vendorService.postData('stock_management/add_or_update_stock', $scope.stockObj).then(function (response) {
                if (response.data.status === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.status === "valid") {
                    toastr.success(response.data.message);
                    $scope.getStockList();
                }
                $('#stockModal').modal('hide')
                dataService.closeLoading();
            });
        }
    }




});