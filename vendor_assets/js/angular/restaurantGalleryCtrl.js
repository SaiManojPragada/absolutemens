$(function () {
    $("#galleryFrom").validate({
        rules: {
            title: {
                required: true,
            },
            image: {
                required: true,
            }
        },
        messages: {
            title: {
                required: "Please enter title",
            },
            image: {
                required: "Please Select Image",
            }

        }
    });
});
app.controller("restaurantGalleryCtrl", function ($scope, dataService, vendorService, $timeout) {
    $scope.gallery = [];
    $scope.galleryItem = {};

    getGallery();

    $scope.addUpdateGallery = function () {
        if ($("#galleryFrom").valid()) {
            var galleryItemFD = new FormData();
            $.each($scope.galleryItem, function (key, value) {
                galleryItemFD.append(key, value);
                console.log(key, value);
            });
            var url = "Gallery/add";

            dataService.getLoading();
            vendorService.postFormData(url, galleryItemFD).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $("#galleryModal").modal("hide");
                    getGallery();

                }
            });
        }
    };

    $scope.deleteImage = function (galleryItem) {
        console.log("called");
        var isDelete = confirm('Do you want to delete Gallery Image ?');
        if (isDelete) {
            dataService.getLoading();
            vendorService.postData("gallery/delete", {
                id: galleryItem.id
            }).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    getGallery();
                }
            });
        }
    };

    function getGallery() {
        dataService.getLoading();
        vendorService.getData("gallery", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                // toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.gallery = response.data.data;
            }
        });
    }
    ;

    $scope.addGalleryItem = function () {
        $scope.galleryItem = {};
        $('#galleryModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    };
});