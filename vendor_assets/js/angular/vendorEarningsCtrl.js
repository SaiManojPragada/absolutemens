
app.controller("vendorEarningsCtrl", function ($scope, dataService, $sce) {

    function initializeObj() {

        $scope.filter = {
            page: 1,

        };

        $scope.earningsList = [];
    }
    initializeObj();

    $scope.getVendorEarnings = function () {
        dataService.getLoading();
        dataService.postData("vendor/earnings", $scope.filter).then(function (response) {
            dataService.closeLoading();
            $scope.earningsList = response.data.data.data;
            $scope.total_amount = response.data.data.total_amount;
            $scope.earningsPagination = response.data.pagination;
            if ($scope.earningsPagination.pagination.length > 0) {
                $scope.earningsPagination.pagination = $sce.trustAsHtml($scope.earningsPagination.pagination);
            }
        });
    };

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getWalletHistory();
    });

});