$(function () {
    $("#withdrawRequestForm").validate({
        rules: {
            amount: {
                required: true,
                digits: true
            }
        },
        messages: {
            amount: {
                required: "Amount cannot be empty",
                digits: "Enter only digits"
            }
        }
    });
});
app.controller("withdrawCtrl", function ($scope, dataService, $sce) {

    function initializeObj() {
        $scope.wallet_details = {
            current_wallet_balance: '',
            onhold_wallet_amount: ''
        };

        $scope.filter = {
            page: 1,
            action: ''
        };

        $scope.withdrawObj = {
            amount: ''
        };
        $scope.withdrawHistory = [];
    }
    initializeObj();
    $scope.getWalletBalance = function () {
        dataService.getLoading();
        dataService.postData("vendor/withdraw_requests/wallet_balance", {type: 'wallet_balance'}).then(function (response) {
            dataService.closeLoading();
            $scope.wallet_details = response.data.data;
        });

    };

    $scope.submitWithdrawlRequest = function () {
        if ($('#withdrawRequestForm').valid()) {
            dataService.getLoading();
            dataService.postData("vendor/withdraw_requests", $scope.withdrawObj).then(function (response) {
                dataService.closeLoading();
                $scope.withdrawObj = {
                    amount: ''
                };
                if (response.data.status == "valid") {
                    toastr.success(response.data.message);
                    $scope.getWalletBalance();
                } else {
                    toastr.warning(response.data.message);
                }
            }
            );
        }
    };

    $scope.getWithDrawHistory = function () {
        dataService.getLoading();
        dataService.postData("vendor/withdraw_requests/list?page=" + $scope.filter.page, $scope.filter).then(function (response) {
            dataService.closeLoading();
            $scope.withdrawHistory = response.data.data;
            $scope.withdrawPagination = response.data.pagination;
            if ($scope.withdrawPagination.pagination.length > 0) {
                $scope.withdrawPagination.pagination = $sce.trustAsHtml($scope.withdrawPagination.pagination);
            }
        });
    };

    $scope.getWalletHistory = function () {
        dataService.getLoading();
        dataService.postData("vendor/withdraw_requests/wallet_list?page=" + $scope.filter.page, $scope.filter).then(function (response) {
            dataService.closeLoading();
            $scope.walletHistory = response.data.data;
            $scope.walletPagination = response.data.pagination;
            if ($scope.walletPagination.pagination.length > 0) {
                $scope.walletPagination.pagination = $sce.trustAsHtml($scope.walletPagination.pagination);
            }
        }
        );
    };

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getWalletHistory();
    });

});