app.controller("dashboardCtrl", function ($scope, dataService, vendorService, $timeout) {

  $scope.dashboard = {};


  $scope.getDashboardData = function () {
    dataService.getLoading();
    vendorService.postData("dashboard", {}).then(function (response) {
      dataService.closeLoading();
      if (response.data.err_code === "invalid") {
        $(".error_place_holder").html(response.data.message);
        toastr.warning(response.data.message);
      }
      if (response.data.err_code === "valid") {
        $scope.dashboard = response.data.data;
      }
    });
  };






});