$(function () {
    $("#login_form").validate({
        rules: {
            username: {
                required: true,
                //digits: true,
                // minlength: 10,
                // maxlength: 10
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            }
        },
        messages: {
            mobile: {
                required: "Enter registered mobile number",
                minlength: "Please enter valid mobile number",
                maxlength: "Plase enter valid mobile number"
            },
            password: {
                required: "Enter Password ",
                // minlength: "Minimum 6 Characters required",
                //maxlength: "Maximum 32 Characters only"
            }
        }
    });
});
app.controller("loginCtrl", function ($scope, dataService, vendorService, $timeout) {
    $scope.user = {
        username: ""
    };
    $scope.doLogin = function () {
        if ($("#login_form").valid()) {
            dataService.getLoading();
            vendorService.postData("login", $scope.user).then(function (response) {
                dataService.closeLoading();
                $("#login_form").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    location.reload();
                    // localStorage.setItem("token", response.data.user.token);
                    $.LoadingOverlay("show");
                    setTimeout(function () {
                        $.LoadingOverlay("hide");
                        location.reload();
                    }, 1000);
                }
            });
        }
    };

});