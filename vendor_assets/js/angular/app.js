$other_modules = ['naif.base64', 'datatables'];
$available_modules = ['application.services', 'application.directives', 'ngCookies', 'naif.base64'];
//This loop is for if new
for (var m = 0; m < $other_modules.length; m++) {
    try {
        if (angular.module($other_modules[m])) {
            $available_modules.push($other_modules[m]);
        }
    } catch (err) {
    }
}

var app = angular.module("vendorApp", $available_modules);



var config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
};
var fileConfig = {
    transformRequest: angular.identity,
    headers: {
        //'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
        'Content-Type': undefined,
        'X-Requested-With': 'XMLHttpRequest',
        'Process-Data': false
    }
};

app.controller("noficationsCtrl", function ($scope, dataService, vendorService, $timeout) {
    $scope.getNotifications = function () {
        vendorService.getData("notifications", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                $scope.notificationsList = response.data.data;
            }
        });
    };
    $scope.getNotifications();
});

app.controller("aliveController", function ($scope, dataService, vendorService, $rootScope, $timeout) {

    $rootScope.is_online = false;
    $scope.is_online = false;
    $scope.GetAliveStatus = function () {
        vendorService.getData("online_or_offline_status", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                $scope.is_online = response.data.data;
                $rootScope.is_online = response.data.data;
            }
        });
    };

    $scope.GetAliveStatus();

    $scope.UpdateRestaurantAliveStatus = function () {
        $scope.is_online = !$scope.is_online;
        if ($scope.is_online == true) {
            is_online = false;
        } else {
            is_online = true;
        }
        console.log(!$scope.is_online);
        vendorService.postData("online_or_offline_status/update", {'is_online': is_online}).then(function (response) {
            $scope.GetAliveStatus();
        });
    };
});


var dateFormate = 'Y-m-d';
var timeFormat = 'hh:mm A';
var checkin_date_format = 'd-m-Y h:00 A';

app.directive('datepickerFrom', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                format: dateFormate,
                maxDate: new Date(),
                validateOnBlur: false,
                timepicker: false
            });
        }
    };
});

app.directive('datepickerTo', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                format: dateFormate,
                maxDate: new Date(),
                validateOnBlur: false,
                timepicker: false
            });
        }
    };
});

app.directive('timepickerNg', function ($parse) {
    return {
        require: '?ngModel',
        restrict: 'C',
        link: function (scope, element, attrs, controller) {
            element.datetimepicker({
                format: 'H:00:00',
                validateOnBlur: false,
                timepicker: true,
                datepicker: false
            });
        }
    };
});

let unique_id;
let product_inventory_id;