$(function () {
    $("#restaurant_profile_form").validate({
        rules: {
            business_name: {
                required: true,
                minlength: 3
            },
            description: {
                required: true,
                minlength: 30
            },
            states_id: {
                required: true
            },
            districts_id: {
                required: true
            },
            cities_id: {
                required: true
            },
            land_mark: {
                required: true,
                minlength: 3
            },
            address: {
                required: true,
                minlength: 30
            },
            contact_email: {
                required: true
            },
            contact_number: {
                required: true,
                maxlength: 10,
                minlength: 10
            },
            owner_name: {
                required: true,
                minlength: 3
            }
        },
        messages: {
            business_name: {
                required: "Enter Valid Shop Name",
                minlength: 3
            },
            description: {
                required: "Enter Bussiness Description",
                minlength: "Description Should be atleasr 30 Characters"
            },
            states_id: {
                required: "Select a State"
            },
            districts_id: {
                required: "Select a District"
            },
            cities_id: {
                required: "Select a City"
            },
            land_mark: {
                required: "Enter Valid Landmark",
                minlength: "Minimum length is 3 Charecters"
            },
            address: {
                required: "Enter Valid Address",
                minlength: "Address should be atleast 30 Charecters"
            },
            contact_email: {
                required: "Enter Valid Email",
                email: "Enter Valid Email"
            },
            contact_number: {
                required: "Enter Valid phone number",
                maxlength: "Invalid Phone Number",
                minlength: "Invalid Phone Number"
            },
            owner_name: {
                required: "Enter Valid Name",
                minlength: "Name should be atleast 3 letters"
            }
        }
    });
});
app.controller("restaurantProfileCtrl", function ($scope, dataService, vendorService, $timeout) {


    getStates();
    getRestaurantProfile();
    $scope.restaurant = {};
    $scope.updateRestaturantProfile = function () {
        if ($("#restaurant_profile_form").valid()) {
            var restaurantFD = new FormData();
            $.each($scope.restaurant, function (key, value) {
                restaurantFD.append(key, value);
            });
            var regex = /^[A-Za-z0-9 ]+$/;
            if (!regex.test($scope.restaurant.business_name)) {
                console.log($scope.restaurant.business_name);
                return false;
            }

            var regex = /^[A-Za-z0-9 ]+$/;
            if (!regex.test($scope.restaurant.land_mark)) {
                console.log($scope.restaurant.land_mark);
                return false;
            }

            var regex = /^[A-Za-z0-9 ]+$/;
            if (!regex.test($scope.restaurant.owner_name)) {
                console.log($scope.restaurant.owner_name);
                return false;
            }
            dataService.getLoading();
            vendorService.postFormData("profile/update", restaurantFD).then(function (response) {
                dataService.closeLoading();
                $("#restaurant_profile_form").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    // $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    // location.reload();
                    // $.LoadingOverlay("show");
                    // setTimeout(function () {
                    //   $.LoadingOverlay("hide");
                    //   location.reload();
                    // }, 1000);
                }
            });
        }
    };

    function getRestaurantProfile() {
        dataService.getLoading();
        vendorService.postData("profile", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.restaurant = response.data.vendor_details;
                getDistricts($scope.restaurant.states_id);
                getCities($scope.restaurant.districts_id);
            }
        });
    }
    ;

    $scope.changeState = function (states_id) {
        if (states_id) {
            $scope.restaurant.cities_id = '0';
            $scope.restaurant.districts_id = '0';
            $scope.cities = [];
            $scope.districts = [];
            getDistricts(states_id);
        }
    }

    $scope.changeDistrict = function (districts_id) {
        if (districts_id) {
            $scope.restaurant.locations_id = '0';
            $scope.locations = [];
            getCities(districts_id);
        }
    }

    function getStates() {
        dataService.getLoading();
        dataService.getData("locations", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                // toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.states = response.data.data;
            }
        });
    }
    ;

    function getDistricts(statesId) {
        if (statesId == 0) {
            return;
        }
        dataService.getLoading();
        dataService.getData("locations/districts", {
            states_id: statesId
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                // toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.districts = response.data.data;
            }
        });
    }
    ;

    function getCities(districtId) {
        if (districtId == 0) {
            return;
        }
        dataService.getLoading();
        dataService.getData("locations/cities", {
            districts_id: districtId
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                // toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.cities = response.data.data;
            }
        });
    }
    ;

});