$(function () {
    $("#change_pass").validate({
        rules: {
            current_password: {
                required: true,
                minlength: 6,
                maxlength: 32
            },
            new_password: {
                required: true,
                minlength: 6,
                maxlength: 32
            },
            re_password: {
                required: true,
                minlength: 6,
                maxlength: 32,
                equalTo: "#new_password"
            }
        },
        messages: {
            current_password: {
                required: "Enter current password",
                minlength: "Please enter valid password",
                maxlength: "Please enter valid password"
            },
            new_password: {
                required: "Enter new password",
                minlength: "Please enter valid password",
                maxlength: "Please enter valid password"
            },
            re_password: {
                required: "Re-Enter new password",
                minlength: "Please enter valid password",
                maxlength: "Please enter valid password",
                equalTo: "Passwords does not match"
            }
        }
    });
});
app.controller("changePasswordCtrl", function ($scope, dataService) {
    $scope.myObj = {};
    $scope.updatePassword = function () {
        dataService.getLoading();
        dataService.postData("fashion_designer/update_password", $scope.myObj).then(function (response) {
            dataService.closeLoading();
            console.log(response.data);
            if (response.data.err_code === "valid") {
                $scope.myObj = {};
                swal('Success', response.data.message, 'success');
            } else {
                swal('Attention', response.data.message, 'warning');
            }
        });
    }
});