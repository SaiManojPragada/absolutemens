$(function () {
    $("#rejectReasonForm").validate({
        rules: {
            reject_reason: {
                required: true
            }
        },
        messages: {
            reject_reason: {
                required: "Reject reason is required",

            }
        }
    });
});
app.controller("myOrdersCtrl", function ($scope, dataService, vendorService, $filter, $timeout, $sce) {

    function initializeObj() {
        $scope.toBeApprovedOrders = [];
        $scope.disableVerifyButton = false;
        $scope.rejectObj = {
            order_id: '',
            reject_reason: ''
        };
    }
    initializeObj();
    $scope.getToBeApprovedOrders = function () {
        dataService.getLoading();
        dataService.postData("fashion_designer/orders/to_be_approved", {type: 'to_be_approved'}).then(function (response) {
            dataService.closeLoading();
            if (response.data.status === "valid") {
                $scope.toBeApprovedOrders = response.data.data;
            }
        });
    };


    $scope.getForwardedToAdminOrders = function () {
        dataService.getLoading();
        dataService.postData("fashion_designer/orders/forwarded_to_admin", {type: 'to_be_approved'}).then(function (response) {
            dataService.closeLoading();
            if (response.data.status === "valid") {
                $scope.forwardedToAdminOrders = response.data.data;
            }
        });
    };



    $scope.showOrderDetailsModalPopup = function (id) {
        dataService.getLoading();
        dataService.postData("fashion_designer/orders/details", {order_id: id}).then(function (response) {
            dataService.closeLoading();
            if (response.data.status == "valid") {
                $scope.orderViewObj = response.data.data;
                angular.element("#order-details-modal-popup").modal({backdrop: 'static', keyboard: false});
            } else {
                swal('Attention', response.data.message, 'warning');
            }
        });
    };

    $scope.showLogsModalPopup = function (unique_order_id) {
        dataService.getLoading();
        dataService.postData("fashion_designer/orders/orders_log", {unique_order_id}).then(function (response) {
            dataService.closeLoading();
            if (response.data.status == "valid") {
                $scope.orderLogsList = response.data.data;
                angular.element("#order-logs-modal-popup").modal({backdrop: 'static', keyboard: false});
            } else {
                swal('Attention', response.data.message, 'warning');
            }
        });
    };

    $scope.checkSubscriptionValidation = function (subscription_transaction_id, order_id, order_product_id) {
        dataService.getLoading();
        $scope.checkSubscriptionPostObj = {
            subscription_transaction_id,
            order_id
        };
        dataService.postData("fashion_designer/orders/check_subscription_validation", $scope.checkSubscriptionPostObj).then(function (response) {
            dataService.closeLoading();
            if (response.data.status == "valid") {
                $scope.disableVerifyButton = false;
                $scope.updateFashionManagerSelectedStatus(order_product_id, order_id);
            } else {
                $scope.disableVerifyButton = true;
                swal('Attention', response.data.message, 'warning');
            }
        });
    };

    $scope.updateFashionManagerSelectedStatus = function (order_product_id, order_id) {
        dataService.getLoading();
        $scope.updateFashionManagerSelectedStatusPostObj = {
            order_product_id
        };
        dataService.postData("fashion_designer/orders/update_fashion_manager_selected_status", $scope.updateFashionManagerSelectedStatusPostObj).then(function (response) {
            if (response.data.status == "valid") {
                $scope.showOrderDetailsModalPopup(order_id);
            } else {
                swal('Attention', response.data.message, 'warning');
            }
        });
    };

    $scope.acceptAndForwardToAdmin = function (order_id) {
        $scope.acceptAndForwardPostObj = {
            order_id
        };
        dataService.getLoading();
        dataService.postData("fashion_designer/orders/accept_and_forward_to_admin", $scope.acceptAndForwardPostObj).then(function (response) {
            dataService.closeLoading();
            if (response.data.status == "valid") {
                angular.element("#order-details-modal-popup").modal("hide");
                swal('Success', response.data.message, 'success');
                $timeout(() => {
                    document.location = 'forwarded_to_admin';
                }, 1200);
            } else {
                swal('Attention', response.data.message, 'warning');
            }
        });
    };

    $scope.openRejectModalPopup = function (order_id) {
        swal({
            title: "Are you sure?",
            text: "Once rejected, you will not be able to revert back!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $scope.rejectObj.order_id = order_id;
                angular.element("#reject-reason-modal-popup").modal({backdrop: 'static', keyboard: false});
            } else {
                swal("User cancelled action");
            }
        });
    };


    $scope.rejectOrder = function () {
        if ($("#rejectReasonForm").valid()) {
            dataService.getLoading();
            dataService.postData("fashion_designer/orders/reject_order", $scope.rejectObj).then(function (response) {
                dataService.closeLoading();
                if (response.data.status == "valid") {
                    angular.element("#order-details-modal-popup").modal("hide");
                    swal('Success', response.data.message, 'success');
                    angular.element("#reject-reason-modal-popup").modal("hide");
                    $scope.getToBeApprovedOrders();
                } else {
                    swal('Attention', response.data.message, 'warning');
                }
            });
        }

    };
});

