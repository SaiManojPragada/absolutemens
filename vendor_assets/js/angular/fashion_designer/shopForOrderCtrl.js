app.controller("shopForOrderCtrl", function ($scope, dataService, $sce, $timeout) {

    function initializeObj() {
        $scope.shopForOrderPostObj = {
            page: 1,
            unique_order_id: unique_id,
            type: ''
        };
        $scope.shopForObj = {
            order_details: {},
            related_products: [],
            categoriesList: []
        };

    }
    initializeObj();


    $scope.getOrdersDetailsWithProducts = function (type) {
        $scope.shopForOrderPostObj.type = type;
        console.log($scope.shopForOrderPostObj)
        dataService.getLoading();
        dataService.postData("fashion_designer/shop_for_order?page=" + $scope.shopForOrderPostObj.page, $scope.shopForOrderPostObj).then(function (response) {
            dataService.closeLoading();
            if (type == "initial") {
                $scope.shopForObj = response.data.data;
            } else if (type == "pagination") {
                $scope.shopForObj.related_products = response.data.data.related_products;
            }
            $scope.productsPagination = response.data.pagination;
            if ($scope.productsPagination.pagination.length > 0) {
                $scope.productsPagination.pagination = $sce.trustAsHtml($scope.productsPagination.pagination);
            }
        });
    };


    $scope.getProductDetails = function (id, inventory_id) {
        dataService.getLoading();
        $scope.productDetailsPostObj = {
            product_id: id,
            inventory_id
        };
        dataService.postData("fashion_designer/shop_for_order/product_details", $scope.productDetailsPostObj).then(function (response) {
            dataService.closeLoading();
            $scope.product = response.data.data;
        });
    };


    $scope.forwardToFashionManager = function () {
        $scope.checkAlreadyForwardedToFashionManager();
        if (!$scope.pageObj.is_forwarded_to_fashion_manager) {
            dataService.getLoading();
            dataService.postData("fashion_designer/shop_for_order/forward_to_fashion_manager", {type: 'forward_to_fashion_manager'}).then(function (response) {
                dataService.closeLoading();
                if (response.data.status == "valid") {
                    swal('Good job', response.data.message, 'success');
                    $scope.checkAlreadyForwardedToFashionManager();
                } else {
                    swal('Attention', response.data.message, 'warning');
                    // $scope.checkAlreadyForwardedToFashionManager();
                }
            });
        }

    };

    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.shopForOrderPostObj.page = $(this).attr("data-ci-pagination-page");
        $scope.getOrdersDetailsWithProducts('pagination');
    });

});