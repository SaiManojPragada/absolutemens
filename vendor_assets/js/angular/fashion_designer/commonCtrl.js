$(function () {
    $("#suggestionForm").validate({
        rules: {
            comment: {
                required: true
            }
        },
        messages: {
            comment: {
                required: "Suggestion is required",

            }
        }
    });
});

app.controller("commonCtrl", function ($scope, dataService) {

    function initializeObj() {
        $scope.cartObj = {
            product_inventory_id: '',
            product_id: '',
            quantity: '',
            unique_id
        };
        $scope.cartList = [];
        $scope.pageObj = {
            is_forwarded_to_fashion_manager: true
        };

    }

    initializeObj();

    $scope.addToCart = function () {
        if ($scope.cartObj.product_inventory_id) {
            if ($("#suggestionForm").valid()) {
                dataService.getLoading();
                dataService.postData("fashion_designer/cart/add_or_update_cart", $scope.cartObj).then(function (response) {
                    dataService.closeLoading();
                    angular.element("#cart-comment-modal-popup").modal("hide");
                    if (response.data.status == "valid") {
                        swal(response.data.message);
                        $scope.getCartList();
                        $scope.cartObj = {
                            product_inventory_id: '',
                            product_id: '',
                            quantity: '',
                            unique_id
                        };
                    } else if (response.data.status == "invalid") {
                        swal(response.data.message);
                    }
                });
            }
        } else {
            swal('Please select product size')
            return false;
        }
    };

    $scope.getCartList = function () {
        dataService.getLoading();
        dataService.postData("fashion_designer/cart/cart_list", {type: 'cart_list'}).then(function (response) {
            dataService.closeLoading();
            $scope.cartList = response.data.data.result;
            $scope.sub_total = response.data.data.sub_total;
            $scope.final_amount_including_taxes = response.data.data.final_amount_including_taxes;
        });
    };

    $scope.deleteCartItem = function (id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover",
            icon: "warning",
            buttons: true,
            dangerMode: true
        }).then((willDelete) => {
            if (willDelete) {
                dataService.getLoading();
                $scope.deleteCartItemPostObj = {
                    id
                };
                dataService.postData("fashion_designer/cart/delete_cart_item", $scope.deleteCartItemPostObj).then(function (response) {
                    dataService.closeLoading();
                    if (response.data.status == "valid") {
                        swal(response.data.message);
                        let index = $scope.cartList.indexOf(id);
                        let tempArr = $scope.cartList;
                        tempArr.splice(index, 1);
                        $scope.cartList = tempArr;
                    } else if (response.data.status == "invalid") {
                        swal(response.data.message);
                    }
                    $scope.getCartList();
                });
            } else {
                swal("User cancelled action");
            }
        });
    };

    $scope.updateQuantity = function (quantity, cart_id, product_inventory_id, type) {
        $scope.disableButton = true;
        dataService.getLoading();
        $scope.updateQuantityObj = {
            quantity,
            cart_id,
            product_inventory_id,
            type
        };
        dataService.postData("fashion_designer/cart/update_cart_quantity", $scope.updateQuantityObj).then(function (response) {
            $scope.disableButton = false;
            dataService.closeLoading();
            if (response.data.status == "valid") {
                swal(response.data.message);
                $scope.getCartList();
            } else if (response.data.status == "invalid") {
                swal(response.data.message);
            }
        });
    };

    $scope.checkAlreadyForwardedToFashionManager = function () {
        dataService.postData("fashion_designer/shop_for_order/check_order_already_forwarded_to_fashion_manager", {unique_id}).then(function (response) {
            if (response.data.status == "valid") {
                $scope.pageObj.is_forwarded_to_fashion_manager = false;
                $scope.orderLogsList = response.data.data;
            } else {
                $scope.pageObj.is_forwarded_to_fashion_manager = true;
                //swal('Attention', response.data.message, 'warning');
                $scope.orderLogsList = response.data.data;
            }
        });
    };

    $scope.openCartCommentModalPopup = function (product_id, qty, inventory_id) {
        if (!product_id) {
            swal('Product id is required');
            return false;
        }
        if (!qty) {
            swal('Quantity  is required');
            return false;
        }
        $scope.cartObj.product_id = product_id;
        $scope.cartObj.quantity = qty;
        $scope.cartObj.product_inventory_id = inventory_id;
        $scope.getCartComment(product_id, inventory_id);
        if ($scope.cartObj.product_inventory_id) {
            angular.element("#cart-comment-modal-popup").modal({backdrop: 'static', keyboard: false});
        } else {

            swal('Please select product size');
            return false;
        }
    };

    $scope.getCartComment = function (product_id, product_inventory_id) {
        $scope.cartCommentPostObj = {
            product_id,
            product_inventory_id,
            unique_id
        };
        if ($scope.cartCommentPostObj.product_id && $scope.cartCommentPostObj.product_inventory_id) {
            dataService.postData("fashion_designer/cart/get_cart_comment", $scope.cartCommentPostObj).then(function (response) {
                dataService.closeLoading();
                if (response.data.status == "valid") {
                    $scope.cartObj.comment = response.data.data;
                } else {
                    $scope.cartObj.comment = "";
                }
            });
        }

    };

});