$(function () {
    $("#reAssignForm").validate({
        rules: {
            fashion_analyst_id: {
                required: true
            }
        },
        messages: {
            fashion_analyst_id: {
                required: "Select a fashion analyst",

            }
        }
    });
});

app.controller("ordersCtrl", function ($scope, dataService, vendorService, $filter, $timeout, $sce) {
    $scope.orders = {};
    function initializeObj() {
        $scope.fashionAnalystsList = [];
        $scope.assignPostObj = {
            subscription_transaction_id: '',
            fashion_analyst_id: ''
        };
    }

    initializeObj();
    $scope.getOrders = function () {
        dataService.getLoading();
        dataService.postData("fashion_designer/my_orders", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                toastr.success(response.data.message);
                $scope.orders = response.data.data;
                $scope.disbled = response.data.disable_details
                console.log($scope.disbled);
            }
        });
    };
    $scope.singleOrder = {};
    $scope.getOrderDetails = function (id) {
        dataService.getLoading();
        dataService.postData("fashion_designer/my_orders/get_single_order", {id: id}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                toastr.success(response.data.message);
                $scope.singleOrder = response.data.data;
                $scope.thisCanBeusedInsideNgBindHtml = $sce.trustAsHtml($scope.singleOrder.package_details.description);
                console.log($scope.singleOrder);
            }
        });
    };
    $scope.openReassignModalPopup = function (id) {
        $scope.assignPostObj.subscription_transaction_id = id;
        swal({
            title: "Are you sure?",
            text: "Once re-assigned, you will not be able to revert back!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $scope.getFashionAnalysts();
            } else {
                swal("User cancelled action");
            }
        });
    };
    $scope.getFashionAnalysts = function () {
        dataService.getLoading();
        dataService.postData("fashion_designer/orders/get_fashion_analysts", {type: 'get_fashion_analysts'}).then(function (response) {
            dataService.closeLoading();
            $scope.fashionAnalystsList = response.data.data;
            angular.element("#reassign-modal-popup").modal({backdrop: 'static', keyboard: false});
        });
    };
    $scope.reAssignFashionAnalyst = function () {
        if ($("#reAssignForm").valid()) {
            dataService.getLoading();
            dataService.postData("fashion_designer/orders/reassign_fashion_analyst", $scope.assignPostObj).then(function (response) {
                if (response.data.status == "valid") {
                    dataService.closeLoading();
                    swal('Success', response.data.message, 'success');
                    angular.element("#reassign-modal-popup").modal('hide');
                } else {
                    swal('Attention', response.data.message, 'warning');
                }
            });
        }

    };
});