$(function () {
    $("#login_form").validate({
        rules: {
            username: {
                required: true,
                //digits: true,
                // minlength: 10,
                // maxlength: 10
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            }
        },
        messages: {
            mobile: {
                required: "Enter registered mobile number",
                minlength: "Please enter valid mobile number",
                maxlength: "Plase enter valid mobile number"
            },
            password: {
                required: "Enter Password ",
                // minlength: "Minimum 6 Characters required",
                //maxlength: "Maximum 32 Characters only"
            }
        }
    });
});
app.controller("loginCtrl", function ($scope, dataService, customerService, $timeout) {
    $scope.social = {};
    $scope.user = {};

    $scope.doLogin = function () {
        if ($("#loginForm").valid()) {
            dataService.getLoading();
            $mobile = $scope.user.mobile;


            console.log($scope.user);
            dataService.postData("fashion_login", $scope.user).then(function (response) {
                dataService.closeLoading();
                console.log(response);
                $("#loginForm").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    if (response.data.error_type === "mobile_verification_required") {
                        $mobile = response.data.mobile;
                        toastr.success(response.data.title);
                        $scope.showVerficationCodeBox(response)
                    } else {
                        toastr.success(response.data.message);
                        $.LoadingOverlay("show");
                        setTimeout(function () {
                            $.LoadingOverlay("hide");
                            location.reload();
                        }, 1000);
                    }

                }
            });
        }
    };

});