$(function () {
    $("#restaurant_category_form").validate({
        rules: {
            name: {
                required: true,
            },
            is_recommened: {
                required: true,
            },
            admin_commission_percentage: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "Enter Category Name",
            },
            is_recommened: {
                required: "Select is Recommended",
            },
            admin_commission_percentage: {
                required: "Enter Commission Percentage",
            }
        }
    });
});
app.controller("restaurantCategoriesCtrl", function ($scope, dataService, vendorService, $timeout) {
    $scope.restaurant = {};
    $scope.category = {
        name: 0,
        priority: 0,
        is_recommened: 0,
        admin_commission_percentage: 0
    }
    $scope.type = 0;



    $scope.addUpdateCategory = function () {
        if ($("#restaurant_category_form").valid()) {

            var categoryFd = new FormData();
            $.each($scope.category, function (key, value) {
                categoryFd.append(key, value);
            });


            dataService.getLoading();
            vendorService.postFormData("categories/add", categoryFd).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    location.reload();
                }
            });
        }
    };

    $scope.deleteCategory = function (category) {
        var isDelete = confirm('Do you want to delete Category ?');
        if (isDelete) {
            dataService.getLoading();
            vendorService.postData("categories/delete", {
                id: category.id
            }).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    location.reload();
                }
            });
        }
    };

    $scope.filterObj = {
        search_key: ''
    };

    $scope.getCategories = function () {
        console.log($scope.filterObj);
        dataService.getLoading();

        vendorService.postData("categories", $scope.filterObj).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.categories = response.data.data;
            }
        });
    };




    $scope.showModal = function (type, category) {
        $scope.type = type;
        $scope.title = type == 1 ? 'Update' : 'Add';
        $scope.category = type == 1 ? angular.copy(category) : {};

        $('#categoryModal').modal({
            show: true
        });
    };


    $scope.manageTimings = function (categoryItem) {
        $scope.categoryItem = angular.copy(categoryItem);
        $scope.categoryItem.restaurant_categories_id = $scope.categoryItem.id;
        $scope.getCategoryTimings($scope.categoryItem.restaurant_categories_id);
        //$scope.getImages(foodItem.id);
        $('#categoryTimingsModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    };

    $scope.getCategoryTimings = function (restaurants_category_id) {
        var url = "category_timings";
        dataService.getLoading();
        var foodItemFD = new FormData();
        foodItemFD.append("restaurant_categories_id", restaurants_category_id);
        vendorService.postFormData(url, foodItemFD).then(function (response) {
            dataService.closeLoading();
            $scope.categoryItem.timings = response.data.data;
        });
    }

    $scope.addCategoryTimings = function () {
        if ($("#category_timings_form").valid()) {
            var url = "category_timings/add";
            dataService.getLoading();
            vendorService.postData(url, $scope.categoryItem).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $scope.getCategoryTimings($scope.categoryItem.restaurant_categories_id);
                }
            });
        }
    };

    $scope.deleteCategoryTimings = function (item) {
        var isDelete = confirm('Do you want to delete?');
        if (isDelete) {
            dataService.getLoading();
            vendorService.postData("category_timings/delete", {
                id: item.id
            }).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $scope.getCategoryTimings($scope.categoryItem.restaurant_categories_id);
                }
            });
        }
    };
});