$(function () {
    $("#bankForm").validate({
        rules: {
            account_name: {
                required: true,
            },
            account_number: {
                required: true,
            },
            bank_name: {
                required: true,
            },
            branch: {
                required: true,
            },
            ifsc_code: {
                required: true,
            }
        },
        messages: {
            account_name: {
                required: "Account Name is Required"
            },
            account_number: {
                required: "Account Number is Required"
            },
            bank_name: {
                required: "Bank Name is Require",
            },
            branch: {
                required: "Branch name is Required",
            },
            ifsc_code: {
                required: "IFSC code is Required",
            }
        }
    });
});
app.controller("restaurantBankAccountCtrl", function ($scope, dataService, vendorService, $timeout) {
    var rb = this;
    rb.focus = function () {
        document.getElementById('account_name').focus();
    };

    $scope.addUpdateBankDetails = function () {
        if ($("#bankForm").valid()) {
            dataService.getLoading();
            vendorService.postData("bank_details/add", $scope.bank).then(function (response) {
                dataService.closeLoading();
                console.log(response.data);
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $scope.getBankDetails();
                }
            });
        }
    };

    $scope.getIfscDetails = function () {
        dataService.getLoading();
        vendorService.postData("bank_details/ifsc_details", {"ifsc_code": $scope.bank.ifsc_code}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                $scope.bank.branch = response.data.data.ifsc_detais.BRANCH;
                $scope.bank.bank_name = response.data.data.ifsc_detais.BANK;
            } else {
                toastr.warning(response.data.message);
                $scope.bank.branch = "";
                $scope.bank.bank_name = "";
            }
        });

    };

    $scope.getBankDetails = function () {
        dataService.getLoading();
        vendorService.postData("bank_details", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.bank = response.data.data[0];
            }
        });
    };
    $scope.getBankDetails();

});