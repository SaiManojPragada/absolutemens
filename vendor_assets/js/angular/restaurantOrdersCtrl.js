$(function () {
    $("#order_reject_form").validate({
        rules: {
            rejected_reason: {
                required: true,
            },

        },
        messages: {
            rejected_reason: {
                required: "Enter Reason for  Rejection",
            },
        }
    });
});
$(function () {
    $("#order_delivery_form").validate({
        rules: {
            delivery_person_name: {
                required: true,
            },
            delivery_person_contact_number: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
            }

        },
        messages: {
            delivery_person_name: {
                required: "Enter Delivery Person Name",
            },
            delivery_person_contact_number: {
                required: "Enter Delivery Person Contact Number",
                minlength: 'Enter Valid 10 Digit Mobile Number',
                maxlength: 'Enter Valid 10 Digit Mobile Number',
            },
        }
    });
});
app.controller("restaurantOrdersCtrl", function ($scope, dataService, vendorService, $timeout) {

    $scope.type = 0;
    $scope.order = {};
    $scope.reject = {};
    $scope.outForDelivery = {};

    $scope.newOrders = [];
    $scope.preparingOrders = [];
    $scope.onGoingOrders = [];
    $scope.deliveredOrders = [];

    setInterval(function () {
        getNewOrders(true);
    }, 1000 * 60);

    getAllOrders();

    socket.on("NEW SERVICE ORDER", function (response) {
        getAllOrders();
    });


    function getAllOrders() {
        getNewOrders();
        getPreparingOrders();
        getOnGoingOrders();
        getDeliveredOrders();

        for ($i = 0; $i < $scope.preparingOrders.length; $i++) {
            if ($scope.preparingOrders[$i].ref_id == $scope.order.ref_id) {
                showCurrentOrder($scope.preparingOrders[$i], 'Pending');
            }
        }

    }

    $scope.acceptOrder = function (refId) {
        dataService.getLoading();
        vendorService.postData("update_order_status/accepted", {
            ref_id: refId
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                $(".error_place_holder").html(response.data.message);
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                toastr.success(response.data.message);
                $('#orderDetailsModal').modal("hide");
                getAllOrders();
            }
        });
    };

    $scope.verifyVendorOTP = function () {
        dataService.getLoading();
        vendorService.postData("validate_otp", {
            ref_id: $scope.order.ref_id,
            otp: $scope.accept.otp
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                $(".error_place_holder").html(response.data.message);
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                toastr.success(response.data.message);
                $('#orderOTPModel').modal("hide");
                getAllOrders();


            }
        });
    };

    $scope.rejectOrder = function () {
        $scope.reject.ref_id = $scope.order.ref_id;
        if ($("#order_reject_form").valid()) {
            dataService.getLoading();
            vendorService.postData("update_order_status/rejected", $scope.reject).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $scope.reject = {};
                    $('#orderRejectModal').on('hide.bs.modal', function (e) {
                        $('#orderDetailsModal').modal("hide");
                    });
                    $('#orderRejectModal').modal("hide");
                    getAllOrders();
                }
            });
        }
        ;
    };

    $scope.outForDeliveryOrder = function () {
        $scope.outForDelivery.ref_id = $scope.order.ref_id;
        if ($("#order_delivery_form").valid()) {
            dataService.getLoading();
            vendorService.postData("update_order_status/out_for_delivery", $scope.outForDelivery).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    $scope.outForDelivery = {};
                    $('#orderDeliverModal').on('hide.bs.modal', function (e) {
                        $('#orderDetailsModal').modal("hide");
                    });
                    $('#orderDeliverModal').modal("hide");
                    getAllOrders();
                }
            });
        }
        ;
    };

    $scope.markAsDelivered = function (refId) {
        dataService.getLoading();
        vendorService.postData("update_order_status/mark_as_delivered", {
            ref_id: refId
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                $(".error_place_holder").html(response.data.message);
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                toastr.success(response.data.message);
                $('#orderDetailsModal').modal("hide");
                getAllOrders();
            }
        });
    };

    $scope.deleteCategory = function (category) {
        var isDelete = confirm('Do you want to delete Category ?');
        if (isDelete) {
            dataService.getLoading();
            vendorService.postData("categories/delete", {
                id: category.id
            }).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                }
            });
        }
    };

    function getNewOrders(type) {
        if (!type) {
            dataService.getLoading();
        }
        vendorService.postData("orders", {
            type: 'Pending'
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.newOrders = response.data.data;
            }
        });
    }
    ;

    function getPreparingOrders() {
        dataService.getLoading();
        vendorService.postData("orders", {
            type: 'Preparing'
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.preparingOrders = response.data.data;
            }
        });
    }
    ;

    function getOnGoingOrders() {
        dataService.getLoading();
        vendorService.postData("orders", {
            type: 'On Going'
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.onGoingOrders = response.data.data;
            }
        });
    }
    ;

    function getDeliveredOrders() {
        dataService.getLoading();
        vendorService.postData("orders", {
            type: 'Completed'
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                $scope.deliveredOrders = response.data.data;
            }
        });
    }
    ;

    $scope.showCurrentOrder = function (order, type) {
        if (order) {
            $scope.order = order;
            $scope.type = type;
        }
    };


});