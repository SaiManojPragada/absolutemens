$other_modules = ['naif.base64', 'datatables'];
$available_modules = ['application.services', 'application.directives', 'ngCookies'];
//This loop is for if new
for (var m = 0; m < $other_modules.length; m++) {
    try {
        if (angular.module($other_modules[m])) {
            $available_modules.push($other_modules[m]);
        }
    } catch (err) {
    }
}

var app = angular.module("customerApp", $available_modules);

var config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
};
var fileConfig = {
    transformRequest: angular.identity,
    headers: {
        //'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;',
        'Content-Type': undefined,
        'X-Requested-With': 'XMLHttpRequest',
        'Process-Data': false
    }
};

