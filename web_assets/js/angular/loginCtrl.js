$(function () {
    $("#loginForm").validate({
        rules: {
            username: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            }
        },
        messages: {
            mobile: {
                required: "Enter registered mobile number",
                minlength: "Please enter valid mobile number",
                maxlength: "Plase enter valid mobile number"
            },
            password: {
                required: "Enter Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only"
            }
        }
    });
});
app.controller("loginCtrl", function ($scope, dataService, customerService, $timeout) {
    $scope.social = {};
    $scope.user = {};
    var facebook_account_id = "";
    var google_account_id = "";
    // get social logins enable function 
    $scope.getSocialButtons = function() {
        dataService.getLoading();
        dataService.getSocialButtons().then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                $scope.social = response.data.data;
                console.log($scope.social, $scope.social.google_login_enabled);
            } 
        });
    };
    $scope.doLogin = function () {
        if ($("#loginForm").valid()) {
            dataService.getLoading();
            $mobile = $scope.user.mobile;
            
            if (localStorage.getItem("facebook_account_id") != null) {
                $scope.user.facebook_account_id = localStorage.getItem("facebook_account_id");
            }
            
            if (localStorage.getItem("google_account_id") != null) {
                $scope.user.google_account_id = localStorage.getItem("google_account_id");
            }
            
            
            console.log($scope.user);
            dataService.postData("login", $scope.user).then(function (response) {
                dataService.closeLoading();
                $("#loginForm").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    $(".error_place_holder").html(response.data.message);
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    if (response.data.error_type === "mobile_verification_required") {
                        $mobile = response.data.mobile;
                        toastr.success(response.data.title);
                        $scope.showVerficationCodeBox(response)
                    } else {
                        toastr.success(response.data.message);
                        $.LoadingOverlay("show");
                        setTimeout(function () {
                            $.LoadingOverlay("hide");
                            location.reload();
                        }, 1000);
                    }
                    
                }
            });
        }
    };
    
    $scope.sendMobileOtp = function (mobile) {
        angular.element("#modal-login").modal("hide");
        dataService.getLoading();
        dataService.sendOTP(mobile).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                toastr.success(response.data.title);
                $scope.showVerficationCodeBox(response);
            } else if (response.data.err_code === "invalid") {
                if (response.data.error_type === "mobile_verification_required") {
                    // $scope.showVerficationCodeBox(response);
                } else {
                    toastr.warning(response.data.message);
                }
            }
        });
    };
    
    $scope.showVerficationCodeBox = function (response) {
        angular.element("#modal-login").modal("hide");
        swal({
            title: response.data.title,
            text: response.data.message,
            inputAttributes: {
                autocapitalize: 'off'
            },
            input: 'text',
            showCancelButton: true,
            cancelButtonText: 'Resend OTP',
            confirmButtonText: 'Confirm',
            animation: "slide-from-top",
            inputPlaceholder: "Enter Verification Code",
        }).then((result) => {
            console.log(result);
            if (result.dismiss == "cancel") {
                $scope.sendMobileOtp($mobile);
                return false;
            } else if (result.value === "") {
                toastr.warning("Verfication code is required!");
                $scope.showVerficationCodeBox(response);
                return false;
            } else if (result.value) {
                swal.close();
                dataService.getLoading();
                dataService.verifyOtp($mobile, result.value).then(function (response) {
                    dataService.closeLoading();
                    if (response.data.err_code === "valid") {
                        toastr.success(response.data.message);
                        $.LoadingOverlay("show");
                        setTimeout(function () {
                            dataService.closeLoading();
                            location.reload();
                        }, 1000);
                    } else if (response.data.err_code === "invalid") {
                        if (response.data.error_type === "invalid_otp") {
                            $scope.showVerficationCodeBox(response);
                        } else {
                            toastr.warning(response.data.message);
                        }
                    }
                });
            }
        });
    };
    
    $scope.getSocialButtons();

    $scope.showForgotPassword = function () {
        angular.element("#modal-login").modal("hide");
        swal({
            title: "Forgot Password",
            text: "Enter Registered Mobile Number/Email Id",
            inputAttributes: {
                autocapitalize: 'off'
            },
            input: 'text',
            showCancelButton: true,
            cancelButtonText: 'Cancel',
            confirmButtonText: 'Submit',
            animation: "slide-from-top",
            inputPlaceholder: "Enter Registered Mobile Number/Email Id",
        }).then((result) => {
            console.log(result);
            if (result.dismiss == "cancel") {
                return false;
            } else if (result.value === "") {
                toastr.warning("Registered Mobile Number/Email Id is required!");
                $scope.showForgotPassword();
                return false;
            } else if (result.value) {
                swal.close();
                dataService.getLoading();
                dataService.postData('forgot_password', {
                    username: result.value
                }).then(function (response) {
                    dataService.closeLoading();
                    if (response.data.err_code === "valid") {
                        toastr.success(response.data.message);
                    } else if (response.data.err_code === "invalid") {
                        toastr.warning(response.data.message);
                    }
                });
            }
            ;
        });
    };



    /*****-----FACEBOOK LOGIN---------********/



    $scope.doSocialLogin = function (data) {
        console.log(data);
        //return;
        dataService.getLoading();
        dataService.postData('social_login', data).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                console.log(response.data);
                if (response.data.type === "details_not_found") {
                    toastr.warning(response.data.message);
                } else {
                    location.reload();
                    toastr.success(response.data.message);
                }
            } else if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
        });
    };

    // Load the JavaScript SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));



    window.fbAsyncInit = function () {
        // FB JavaScript SDK configuration and setup
        FB.init({
            appId: '241907267070517', // FB App ID
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true, // parse social plugins on this page
            version: 'v3.2' // use graph api version 2.8
        });

        // Check whether the user already logged in
        FB.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                //display user data
                //getFbUserData();
            }
        });
    };



// Facebook login with JavaScript SDK
    $scope.fbLogin = function () {
        FB.login(function (response) {
            if (response.authResponse) {
                // Get and display the user profile data
                $scope.getFbUserData();
            } else {
                //document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
            }
        }, {scope: 'email'});
    }

// Fetch the user profile data from facebook
    $scope.getFbUserData = function () {
        FB.api('/me', {
            locale: 'en_US',
            fields: 'id,first_name,last_name,email,link,gender'},
                function (response) {
                    console.log(response);
                    if (document.getElementById('fbLink')) {
                        document.getElementById('fbLink').setAttribute("onclick", "fbLogout()");
                    }
                    var data = {
                        firstname: response.first_name,
                        lastname: response.last_name,
                        email: response.email,
                        facebook_account_id: response.id
                    };
                    facebook_account_id = response.id;
                    localStorage.setItem("facebook_account_id", facebook_account_id);
                    $scope.doSocialLogin(data);
                    //document.getElementById('userData').innerHTML = '<p><b>FB ID:</b> ' + response.id + '</p><p><b>Name:</b> ' + response.first_name + ' ' + response.last_name + '</p><p><b>Email:</b> ' + response.email + '</p><p><b>Gender:</b> ' + response.gender + '</p><p><b>Locale:</b> ' + response.locale + '</p><p><b>Picture:</b> <img src="' + response.picture.data.url + '"/></p><p><b>FB Profile:</b> <a target="_blank" href="' + response.link + '">click to view profile</a></p>';
                });
    }
// Logout from facebook
    $scope.fbLogout = function () {
        FB.logout(function () {
            if (document.getElementById('fbLink')) {
                document.getElementById('fbLink').setAttribute("onclick", "fbLogin()");
            }
            document.getElementById('fbLink').innerHTML = '<img src="fblogin.png"/>';
            document.getElementById('userData').innerHTML = '';
            document.getElementById('status').innerHTML = 'You have successfully logout from Facebook.';
        });
    }
    /*****-----FACEBOOK LOGIN ENDS---------********/


    /*****-----GOOGLE LOGIN STARTS---------********/

    $scope.google_signin = function (googleUser) {
        var profile = googleUser.getBasicProfile();
        var data = {
            firstname: profile.getName(),
            lastname: '',
            email: profile.getEmail(),
            google_account_id: profile.getId(),
            profile_pic: profile.getImageUrl()
        };
        google_account_id = profile.getId();
        localStorage.setItem("google_account_id", google_account_id);
        console.log("Google Account ID : " + google_account_id);
        $scope.doSocialLogin(data);
    };

    /*****-----GOOGLE LOGIN ENDS---------********/
});


var googleUser = {};
window.addEventListener('load', function () {
    gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
            client_id: '649178196715-orrcs60qhtdck188nioh59nf4i229hmr.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
        });

        auth2.attachClickHandler(document.querySelector("#googleLoginBtn"), {}, function (googleUser) {
            var scope = angular.element($('#social_login_div')).scope();
            scope.$apply(function () {
                scope.google_signin(googleUser);
            });
        });
    }
    );
});