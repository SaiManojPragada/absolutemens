app.controller("favouritesCtrl", function ($scope, dataService, customerService, $timeout) {
  $scope.restaurants = [];
  $scope.filter = {
    favourites: true
  };

  getRestaurants();




  function getRestaurants(hideLoader) {
    if (!hideLoader) {
      dataService.getLoading();
    }
    dataService.postData("restaurants", $scope.filter).then(function (response) {
      dataService.closeLoading();
      if (response.data.err_code === "invalid") {
        $(".error_place_holder").html(response.data.message);
        // toastr.warning(response.data.message);
      }
      if (response.data.err_code === "valid") {
        // toastr.success(response.data.message);
        $scope.restaurants = response.data.data;

      }
    });
  }



});