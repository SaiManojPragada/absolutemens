$.fn.modal.Constructor.prototype.enforceFocus = function () {};
$(function () {
    $("#newStoreRequest").validate({
        /*errorPlacement: function (error, element) {
         //error.appendTo(element.closest(".form-group"));
         },*/
        rules: {
            business_name: {
                required: true,
            },
            address: {
                required: true,
            },
            city: {
                required: true,
            },
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            mobile_number: {
                required: true,
                number: true
            },
            email: {
                email: true,
                required: true,
            },
            no_of_stores: {
                required: true,
            },
            message: {
                required: true,
                minlength: 25
            },
            captcha: {
                required: true,
                minlength: 5,
                maxlength: 5,
            }
        },
        messages: {
            business_name: {
                required: "Enter Business name",
            },
            address: {
                required: "Enter Address",
            },
            email: {
                required: "Enter your email",
                email: "Please provide valid email",
            },
            city: {
                required: "Enter your city",
            },
            mobile_number: {
                required: "Enter mobile number",
                nowhitespace: "Please enter valid mobile number",
                number: "Enter valid mobile number"
            },
            subject: {
                required: "Enter your query",
            },
            message: {
                required: "Enter message",
                minlenght: "Few words pelase"
            },
            captcha: {
                required: "Required",
                minlength: "",
                maxlength: ""
            }
        }
    });


    $("#newBrandRequest").validate({
        /*errorPlacement: function (error, element) {
         //error.appendTo(element.closest(".form-group"));
         },*/
        rules: {
            business_name: {
                required: true,
            },

            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            mobile_number: {
                required: true,
                number: true
            },
            email: {
                email: true,
                required: true,
            },

            message: {
                required: true,
                minlength: 25
            },
            captcha: {
                required: true,
                minlength: 5,
                maxlength: 5,
            }
        },
        messages: {
            business_name: {
                required: "Enter Business name",
            },
            email: {
                required: "Enter your email",
                email: "Please provide valid email",
            },
            mobile_number: {
                required: "Enter mobile number",
                nowhitespace: "Please enter valid mobile number",
                number: "Enter valid mobile number"
            },
            captcha: {
                required: "Required",
                minlength: "",
                maxlength: ""
            }
        }
    });

});
app.controller("contactUsFormCtrl", function ($scope, dataService, $timeout) {

    $scope.nsObj = {
        business_name: "",
        address: "",
        state: "",
        city: "",
        first_name: "",
        last_name: "",
        mobile_number: "",
        email: "",
        no_of_stores: "",
        message: "",
        captcha: "",
    };

    $scope.get_captcha = function () {
        $scope.captcha_image = "";
        $scope.captcha_image = apiBaseUrl + "send_add_store_request/captcha?rand=" + Math.random().toString().replace('0.', '');
    };

    $scope.get_captcha();

    $scope.submitNewStoreFormRequest = function () {
        if ($("#newStoreRequest").valid()) {
            $scope.get_captcha();
            dataService.getLoading();
            dataService.postData("send_add_store_request", $scope.nsObj).then(function (response) {
                dataService.closeLoading();
                $("#newStoreRequest").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                } else if (response.data.err_code === "invalid_from") {
                    toastr.warning(response.data.message);
                }

                if (response.data.err_code === "valid") {
                    $("#newStoreRequest").trigger("reset");
                    toastr.success(response.data.message);
                    setTimeout(function () {
                        location.reload();
                    }, 10000)
                }
            });
        }
    };


    $scope.submitNewBrandFormRequest = function () {
        if ($("#newBrandRequest").valid()) {
            $scope.get_captcha();
            dataService.getLoading();
            dataService.postData("send_add_brand_request", $scope.nsObj).then(function (response) {
                dataService.closeLoading();
                $("#newBrandRequest").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                } else if (response.data.err_code === "invalid_from") {
                    toastr.warning(response.data.message);
                }

                if (response.data.err_code === "valid") {
                    $("#newBrandRequest").trigger("reset");
                    toastr.success(response.data.message);
                    setTimeout(function () {
                        location.reload();
                    }, 10000)
                }
            });
        }
    };
});