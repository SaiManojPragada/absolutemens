app.controller("checkOutCtrl", function ($scope, dataService, $rootScope, customerService, $timeout) {
    $scope.createOrder = function () {
        $(".customer_address_list").css("border", "0px");
        $(".payment_mode_div").css("border", "0px");
        console.log($scope.cartObj.delivery_address);
        if ($scope.cartObj.delivery_address == "") {
            $(".customer_address_list").css("border", "1px solid red");
            alert("Please choose delivery location");
            startWatching();
            return;
        }
        if ($scope.cartObj.payment_mode == "") {
            alert("Please choose payment method");
            $(".payment_mode_div").css("border", "1px solid red");
            return;
        }
        dataService.getLoading();
        $scope.cartObj.creation_source = "website";
        dataService.postJSONData("create_order", $scope.cartObj).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                toastr.success(response.data.title);
                location.href = baseurl + "order_gateway?ref_id=" + response.data.ref_id;
            } else {
                toastr.error(response.data.message);
            }
        })
        console.log(JSON.stringify($scope.cartObj));
    };
    function startWatching() {
        $scope.$watch('cartObj.delivery_address', function (newValue, oldValue, scope) {
            if (newValue == null || newValue == undefined || newValue == "") {
                $(".customer_address_list").css("border", "1px solid red");
            } else {
                $(".customer_address_list").css("border", "0px");
            }
        }, true);

        $scope.$watch('cartObj.payment_mode', function (newValue, oldValue, scope) {
            if (newValue == "") {
                $(".payment_mode_div").css("border", "1px solid red");
            } else {
                $(".payment_mode_div").css("border", "0px");
            }
        }, true);
    }

    $scope.$watch('cartObj.delivery_address', function (newValue, oldValue, scope) {
        if (newValue == null || newValue == undefined || newValue == "") {
        } else {
            dataService.postData("cart/update_delivery_location", {'customers_addresses_id': newValue.id}).then(function (response) {
                dataService.closeLoading();
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.title);
                    $rootScope.getCart();
                } else {
                    toastr.error(response.data.message);
                    $scope.cartObj.delivery_address = '';
                }
            })
        }
    }, true);

    $scope.ShowAvailableCoupons = function () {
        dataService.getLoading();
        dataService.getData("available_coupons", {'restaurants_id': $scope.cartObj.restaurants_id}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                $('#couponsModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $scope.available_coupons = response.data.data;

                //toastr.success(response.data.title);
            } else {
                toastr.error(response.data.message);
            }
        })
    };

    $scope.ApplyCoupon = function (item) {
        $scope.ValidateCoupon(item.coupon_code);
    };

    $scope.ValidateCoupon = function (coupon_code) {
        $scope.cartObj.creation_source = "website";
        var obj = {
            restaurants_id: $scope.cartObj.restaurants_id,
            coupon_code: coupon_code,
            sess_cart_id: $scope.cartObj.sess_cart_id,
            creation_source: $scope.cartObj.creation_source
        }
        dataService.getLoading();
        dataService.postData("validate_coupon_code", obj).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                toastr.success(response.data.title);
                $rootScope.getCart();
                $('#couponsModal').modal("hide");
            } else {
                toastr.error(response.data.message);
            }
        })
    };

    $scope.RemoveAppliedCoupon = function () {
        $scope.cartObj.creation_source = "website";
        var obj = {
            restaurants_id: $scope.cartObj.restaurants_id,
            sess_cart_id: $scope.cartObj.sess_cart_id,
            creation_source: $scope.cartObj.creation_source
        }
        dataService.getLoading();
        dataService.postData("cart/remove_coupon_code", obj).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                toastr.success(response.data.title);
                $rootScope.getCart();
                $('#couponsModal').modal("hide");
            } else {
                toastr.error(response.data.message);
            }
        })
    };



});