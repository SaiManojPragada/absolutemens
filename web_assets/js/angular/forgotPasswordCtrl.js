$(function () {
  $("#reset_password_form").validate({
    rules: {
      forgot_password_verfication_code: {
        required: true
      },
      password: {
        required: true,
        minlength: 6,
        maxlength: 32
      },
	  cnf_password: {
        required: true,
        minlength: 6,
        maxlength: 32,
		equalTo : '#password'
      }
    },
    messages: {
      password: {
        required: "Enter Password ",
        minlength: "Minimum 6 Characters required",
        maxlength: "Maximum 32 Characters only"
      }
    }
  });
});
app.controller("resetPasswordCtrl", function ($scope, dataService, customerService, $timeout) {
	
});