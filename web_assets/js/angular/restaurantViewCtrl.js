$(function () {
    $("#loginForm").validate({
        rules: {
            username: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            }
        },
        messages: {
            mobile: {
                required: "Enter registered mobile number",
                minlength: "Please enter valid mobile number",
                maxlength: "Plase enter valid mobile number"
            },
            password: {
                required: "Enter Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only"
            }
        }
    });
});
app.controller("restaurantViewCtrl", function ($scope, dataService, customerService, $timeout, $sce, $rootScope) {
    $scope.restaurant = {};

    $scope.addToFavourite = function () {
        if (!restaurant) {
            toastr.warning("Invalid Restaurant");
            return;
        }
        dataService.getLoading();
        customerService.getData("update_to_favourite", {
            restaurants_id: restaurant.id
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                toastr.success(response.data.message);
            }
        });
    }

    $scope.getRestaurantView = function (url) {
        dataService.getLoading();
        dataService.getData("restaurant_data", {
            seo_url: url
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                $(".error_place_holder").html(response.data.message);
                // toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.restaurant = response.data.data;
                $scope.restaurant.description = $sce.trustAsHtml($scope.restaurant.description);
                $scope.restaurant.map_link = $sce.trustAsResourceUrl($scope.restaurant.map_link);
                $rootScope.restaurant = $scope.restaurant;
            }
        });
    };

    $scope.ProceedToCheckOut = function () {
        if ($scope.cartObj.sub_total < $scope.restaurant.minimum_order_amount) {
            $scope.minimum_order_error = "Minimum order Subtotal should be Rs." + $scope.restaurant.minimum_order_amount + "/-";

            $timeout(function () {
                $scope.minimum_order_error = "";
            }, 4000);
            //toastr.warning("Minimum order should be Rs."+$scope.restaurant.minimum_order_amount+"/-");
        } else {
            location.href = baseurl + "checkout";
        }
    }

    $scope.review_message = "";
    $scope.reviewsList = [];
    $scope.GetReviews = function () {
        $scope.review_message = "";
        dataService.getData("restaurant_data/reviews", {
            seo_url: $scope.restaurant.seo_url,
            page: 1
        }).then(function (response) {
            if (response.data.err_code === "invalid") {
                console.log(response.data.data);
                $scope.review_message = response.data.message;
            }
            if (response.data.err_code === "valid") {
                $scope.reviewsList = response.data.data.results;
            }
        });
    };
});