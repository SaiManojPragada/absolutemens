app.controller("orderViewCtrl", function ($scope, dataService, customerService, $timeout) {
  $scope.order = [];
  console.log("order view ctrl");

  $scope.updateProfile = function () {
    if ($("#profileForm").valid()) {
      $mobile = $scope.user.mobile;
      dataService.getLoading();
      dataService.postData("profile/update", $scope.user).then(function (response) {
        dataService.closeLoading();
        $("#profileForm").LoadingOverlay("hide");
        if (response.data.err_code === "invalid") {
          toastr.warning(response.data.message);
        }
        if (response.data.err_code === "valid") {
          toastr.success(response.data.message);
          location.reload();
        };
      });
    };
  };

  $scope.getOrderView = function (ref_id) {
    dataService.getLoading();
    customerService.postData("service_order_view", {
      ref_id: ref_id
    }).then(function (response) {
      dataService.closeLoading();
      if (response.data.err_code === "invalid") {
        toastr.warning(response.data.message);
      }
      if (response.data.err_code === "valid") {
        // toastr.success(response.data.message);
        $scope.order = response.data.data;
      }
    });
  };


});