app.controller("offersCtrl", function ($scope, dataService, $rootScope, customerService, $cookies) {

    $scope.getAvailableOffers = function () {
        dataService.getLoading();
        dataService.getData("available_coupons", {'restaurants_id': $scope.cartObj.restaurants_id,
            "latitude": $cookies.get("latitude"), "longitude": $cookies.get("longitude")}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {

                $scope.available_coupons = response.data.data;

                //toastr.success(response.data.title);
            } else {
                toastr.error(response.data.message);
            }
        })
    };




});