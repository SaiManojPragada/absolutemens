$.fn.modal.Constructor.prototype.enforceFocus = function () {};
$(function () {
  $("#profileForm").validate({
    rules: {
      customer_name: {
        required: true,
      },
      email: {
        required: true,
        email: true,
      },
      mobile: {
        required: true,
        digits: true,
        minlength: 10,
        maxlength: 10
      },
      alternate_mobile: {
        digits: true,
        minlength: 10,
        maxlength: 10
      }

    },
    messages: {
      customer_name: {
        required: "Enter Full Name",
      },
      email: {
        required: "Enter Email Id",
        email: "Enter Valid Email Id"
      },
      mobile: {
        required: "Enter  mobile number",
        minlength: "Please enter valid mobile number",
        maxlength: "Plase enter valid mobile number"
      },
      alternate_mobile: {
        minlength: "Please enter valid mobile number",
        maxlength: "Plase enter valid mobile number"
      },
    }
  });
});
app.controller("profileCtrl", function ($scope, dataService, customerService, $timeout) {
  $scope.user = {};
  getProfile();

  $scope.updateProfile = function () {
    if ($("#profileForm").valid()) {
      $mobile = $scope.user.mobile;
      dataService.getLoading();
      dataService.postData("customer/profile/update", $scope.user).then(function (response) {
        dataService.closeLoading();
        $("#profileForm").LoadingOverlay("hide");
        if (response.data.err_code === "invalid") {
          toastr.warning(response.data.message);
        }
        if (response.data.err_code === "valid") {
          toastr.success(response.data.message);
          location.reload();
        };
      });
    };
  };

  function getProfile() {
    dataService.getLoading();
    dataService.postData("customer/profile", {}).then(function (response) {
      dataService.closeLoading();
      if (response.data.err_code === "invalid") {
        toastr.warning(response.data.message);
      }
      if (response.data.err_code === "valid") {
        // toastr.success(response.data.message);
        $scope.user = response.data.customer_details;
      }
    });
  };


});