$.fn.modal.Constructor.prototype.enforceFocus = function () {};
$(function () {
  $("#changePassForm").validate({
    rules: {
      current_password: {
        required: true,
        // minlength: 6,
        maxlength: 32
      },
      password: {
        required: true,
        minlength: 6,
        maxlength: 32
      },
      password_cnf: {
        required: true,
        minlength: 6,
        maxlength: 32,
        equalTo: "#changePassId"
      },

    },
    messages: {
      current_password: {
        required: "Enter Current Password ",
        minlength: "Minimum 6 Characters required",
        maxlength: "Maximum 32 Characters only"
      },
      password: {
        required: "Enter New Password ",
        minlength: "Minimum 6 Characters required",
        maxlength: "Maximum 32 Characters only"
      },
      password_cnf: {
        required: "Enter Confirm Password ",
        minlength: "Minimum 6 Characters required",
        maxlength: "Maximum 32 Characters only",
        equalTo: "Password and Confirm Password should be Same"
      }
    }
  });
});
app.controller("changePasswordCtrl", function ($scope, dataService, customerService, $timeout) {
  $scope.changePass = {};

  $scope.updatePassword = function () {
    if ($("#changePassForm").valid()) {
      dataService.getLoading();
      customerService.postData("update_password", $scope.changePass).then(function (response) {
        dataService.closeLoading();
        $("#changePassForm").LoadingOverlay("hide");
        if (response.data.err_code === "invalid") {
          toastr.warning(response.data.message);
        }
        if (response.data.err_code === "valid") {
          toastr.success(response.data.message);
          location.reload();
        };
      });
    };
  };



});