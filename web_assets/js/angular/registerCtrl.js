$.fn.modal.Constructor.prototype.enforceFocus = function () {};
$(function () {
    $("#registerForm").validate({
        rules: {
            customer_name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            mobile: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            },
            password_cnf: {
                required: true,
                minlength: 6,
                maxlength: 32,
                equalTo: "#password"
            }
        },
        messages: {
            customer_name: {
                required: "Enter Full Name",
            },
            email: {
                required: "Enter Email Id",
                email: "Enter Valid Email Id"
            },
            mobile: {
                required: "Enter  mobile number",
                minlength: "Please enter valid mobile number",
                maxlength: "Plase enter valid mobile number"
            },
            password: {
                required: "Enter Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only"
            },
            password_cnf: {
                required: "Enter Confirm Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only",
                equalTo: "Password and Confirm Password should be Same"
            }

        }
    });
});
app.controller("registerCtrl", function ($scope, dataService, customerService, $timeout) {
    $scope.user = {};

    $scope.doRegister = function () {
        if ($("#registerForm").valid()) {
            $mobile = $scope.user.mobile;
            dataService.getLoading();

            if (localStorage.getItem("facebook_account_id") != null) {
                $scope.user.facebook_account_id = localStorage.getItem("facebook_account_id");
            }

            if (localStorage.getItem("google_account_id") != null) {
                $scope.user.google_account_id = localStorage.getItem("google_account_id");
            }

            dataService.postData("register", $scope.user).then(function (response) {
                dataService.closeLoading();
                $("#registerForm").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    $("#registerForm").trigger("reset");
                    $scope.showVerficationCodeBox(response);
                }
                ;
            });
        }
        ;
    };

    $scope.sendMobileOtp = function (mobile) {
        angular.element("#modal-login").modal("hide");
        dataService.getLoading();
        dataService.sendOTP(mobile).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "valid") {
                toastr.success(response.data.title);
                $scope.showVerficationCodeBox(response);
            } else if (response.data.err_code === "invalid") {
                if (response.data.error_type === "mobile_verification_required") {
                    // $scope.showVerficationCodeBox(response);
                } else {
                    toastr.warning(response.data.message);
                }
            }
        });
    };

    $scope.showVerficationCodeBox = function (response) {
        angular.element("#modal-login").modal("hide");
        swal({
            title: response.data.title,
            text: response.data.message,
            inputAttributes: {
                autocapitalize: 'off'
            },
            input: 'text',
            showCancelButton: true,
            cancelButtonText: 'Resend OTP',
            confirmButtonText: 'Confirm',
            animation: "slide-from-top",
            inputPlaceholder: "Enter Verification Code",
        }).then((result) => {
            console.log(result);
            if (result.dismiss == "cancel") {
                $scope.sendMobileOtp($mobile);
                return false;
            } else if (result.value === "") {
                toastr.warning("Verfication code is required!");
                $scope.showVerficationCodeBox(response);
                return false;
            } else if (result.value) {
                swal.close();
                dataService.getLoading();
                dataService.verifyOtp($mobile, result.value).then(function (response) {
                    dataService.closeLoading();
                    if (response.data.err_code === "valid") {
                        toastr.success(response.data.message);
                        $.LoadingOverlay("show");
                        setTimeout(function () {
                            dataService.closeLoading();
                            location.reload();
                        }, 1000);
                    } else if (response.data.err_code === "invalid") {
                        if (response.data.error_type === "invalid_otp") {
                            $scope.showVerficationCodeBox(response);
                        } else {
                            toastr.warning(response.data.message);
                        }
                    }
                });
            }
        });
    };


});