app.controller("ordersCtrl", function ($scope, dataService, customerService, $timeout, $sce) {
  $scope.orders = [];
  $scope.data = {};
  
	$scope.filter = {
		q:"",
		service_status:"",
		from_date:"",
		to_date:"",
		page:1
	};

  getOrders();

  $scope.updateProfile = function () {
    if ($("#profileForm").valid()) {
      $mobile = $scope.user.mobile;
      dataService.getLoading();
      dataService.postData("profile/update", $scope.user).then(function (response) {
        dataService.closeLoading();
        $("#profileForm").LoadingOverlay("hide");
        if (response.data.err_code === "invalid") {
          toastr.warning(response.data.message);
        }
        if (response.data.err_code === "valid") {
          toastr.success(response.data.message);
          location.reload();
        };
      });
    };
  };

  function getOrders() {
    dataService.getLoading();
    customerService.postData("service_orders", $scope.filter).then(function (response) {
      dataService.closeLoading();
      if (response.data.err_code === "invalid") {
		//toastr.warning(response.data.message);
      }
      if (response.data.err_code === "valid") {
        // toastr.success(response.data.message);
        $scope.orders = response.data.data;
        $scope.data = response.data;
		$scope.ordersPagination = response.data.pagination;
		if ($scope.ordersPagination.pagination.length > 0) {
			$scope.ordersPagination.pagination = $sce.trustAsHtml($scope.ordersPagination.pagination);
		}
      }
    });
  };
  
  $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
		getOrders();
    });


});