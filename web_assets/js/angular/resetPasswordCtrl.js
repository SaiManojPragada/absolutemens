$.fn.modal.Constructor.prototype.enforceFocus = function () {};
$(function () {
  $("#reset_password_form").validate({
    rules: {
      password: {
        required: true,
        minlength: 6,
        maxlength: 32
      },
      cnf_password: {
        required: true,
        minlength: 6,
        maxlength: 32,
        equalTo: "#restPasswordId"
      },

    },
    messages: {
      password: {
        required: "Enter New Password ",
        minlength: "Minimum 6 Characters required",
        maxlength: "Maximum 32 Characters only"
      },
      cnf_password: {
        required: "Enter Confirm Password ",
        minlength: "Minimum 6 Characters required",
        maxlength: "Maximum 32 Characters only",
        equalTo: "Password and Confirm Password should be Same"
      }
    }
  });
});
app.controller("resetPasswordCtrl", function ($scope, dataService, customerService, $timeout) {
  $scope.myObj = {};

  $scope.doResetPasswordWithCode = function (verificationCode) {
    $scope.myObj.forgot_password_verfication_code = verificationCode;
    if ($("#reset_password_form").valid()) {
      dataService.getLoading();
      dataService.postData("reset_password_with_code", $scope.myObj).then(function (response) {
        dataService.closeLoading();
        $("#reset_password_form").LoadingOverlay("hide");
        if (response.data.err_code === "invalid") {
          toastr.warning(response.data.message);
        }
        if (response.data.err_code === "valid") {
          toastr.success(response.data.message);
          location.href = baseurl;
        };
      });
    };
  };



});