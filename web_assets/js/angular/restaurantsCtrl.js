$(function () {
    $("#loginForm").validate({
        rules: {
            username: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            }
        },
        messages: {
            mobile: {
                required: "Enter registered mobile number",
                minlength: "Please enter valid mobile number",
                maxlength: "Plase enter valid mobile number"
            },
            password: {
                required: "Enter Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only"
            }
        }
    });
});
app.controller("restaurantsCtrl", function ($scope, dataService, customerService, $timeout, $sce) {
    $scope.restaurants = [];
    $scope.cusineLimit = 5;
    $scope.cusines = [];
    $scope.filter = {
        name_sort: "name_asc",
        sort_by: "name_asc", //rating,min_order,fast_delivery
        cusines: [],
        quick: [],
        search_key: '',
    };

    $scope.filter = {};

    $scope.getRestaurants = function (hideLoader) {
        $scope.filter.latitude = $scope.customerAddress.latitude;
        $scope.filter.longitude = $scope.customerAddress.longitude;
        if (!hideLoader) {
            dataService.getLoading();
        }
        dataService.getData("restaurants", $scope.filter).then(function (response) {
            dataService.closeLoading();
            $scope.restaurants = [];
            if (response.data.err_code === "invalid") {
                $(".error_place_holder").html(response.data.message);
                // toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.restaurants = response.data.data;
                $scope.restaurantsPagination = response.data.pagination;
                if ($scope.restaurantsPagination.pagination.length > 0) {
                    $scope.restaurantsPagination.pagination = $sce.trustAsHtml($scope.restaurantsPagination.pagination);
                }
            }
        });
    }

    $scope.$watch('filter', function (newValue, oldValue, scope) {
        if (newValue != oldValue) {
            //console.log($scope.filter);
            $scope.getRestaurants(true);
        }
    }, true);

    $scope.updateSortBy = function (value) {
        if (value) {
            if (value == 'name_asc' || value == 'name_dsc') {
                $scope.filter.name_sort = value;
            }
            $scope.filter.sort_by = value;
        }
    }

    $scope.updateCusine = function () {
        var cuisines = $scope.restaurants.display_filters.cuisine_types;
        var selected = [];
        cuisines.forEach((c) => {
            if (c.is_checked) {
                selected.push(c.id);
            }
        });
        $scope.filter.cusines = selected;
    }

    $scope.removeCusine = function (cusine) {
        if (cusine) {
            cusine.is_checked = !cusine.is_checked;
        }
        $scope.updateCusine();
    }


    $scope.toggleCusineLimit = function () {
        var length = $scope.restaurants.display_filters.cuisine_types.length;
        if ($scope.cusineLimit == length) {
            $scope.cusineLimit = 5;
        } else {
            $scope.cusineLimit = length;
        }
    }





    $(document).on("click", "a[data-ci-pagination-page]", function (e) {
        e.preventDefault();
        $scope.filter.page = $(this).attr("data-ci-pagination-page");
        $scope.getRestaurants();
    });


});