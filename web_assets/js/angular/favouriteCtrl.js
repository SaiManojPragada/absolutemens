$(function () {
  $("#loginForm").validate({
    rules: {
      username: {
        required: true,
        digits: true,
        minlength: 10,
        maxlength: 10
      },
      password: {
        required: true,
        minlength: 6,
        maxlength: 32
      }
    },
    messages: {
      mobile: {
        required: "Enter registered mobile number",
        minlength: "Please enter valid mobile number",
        maxlength: "Plase enter valid mobile number"
      },
      password: {
        required: "Enter Password ",
        minlength: "Minimum 6 Characters required",
        maxlength: "Maximum 32 Characters only"
      }
    }
  });
});
app.controller("favouriteCtrl", function ($scope, dataService, customerService, $timeout) {
  $scope.search = {};
  $scope.restaurants = [];

  getFavourites();

  $scope.addToFavourite = function (restaurant) {
    if (!restaurant) {
      toastr.warning("Invalid Restaurant");
      return;
    }
    dataService.getLoading();
    customerService.getData("update_to_favourite", {
      restaurants_id: restaurant.id
    }).then(function (response) {
      dataService.closeLoading();
      if (response.data.err_code === "invalid") {
        toastr.warning(response.data.message);
      }
      if (response.data.err_code === "valid") {
        toastr.success(response.data.message);
      }
    });
  }


  function getRestaurants() {
    dataService.getLoading();
    dataService.getData("restaurants", {}).then(function (response) {
      dataService.closeLoading();
      if (response.data.err_code === "invalid") {
        $(".error_place_holder").html(response.data.message);
        // toastr.warning(response.data.message);
      }
      if (response.data.err_code === "valid") {
        // toastr.success(response.data.message);
        $scope.restaurants = response.data.data;

      }
    });
  }



});