$(function () {
    $("#loginForm").validate({
        rules: {
            username: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 32
            }
        },
        messages: {
            mobile: {
                required: "Enter registered mobile number",
                minlength: "Please enter valid mobile number",
                maxlength: "Plase enter valid mobile number"
            },
            password: {
                required: "Enter Password ",
                minlength: "Minimum 6 Characters required",
                maxlength: "Maximum 32 Characters only"
            }
        }
    });
});
app.controller("homeCtrl", function ($scope, dataService, customerService, $timeout) {
    $scope.search = {};
    // $scope.restaurants = [];
    // $scope.searchRestaurants = [];

    // getSearchRestaurants();
    // getRestaurants();
    getSearchResult();



    function getRestaurants() {
        dataService.getLoading();
        dataService.getData("restaurants", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                $(".error_place_holder").html(response.data.message);
                // toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.restaurants = response.data.data;
            }
        });
    }


    function getSearchRestaurants() {
        dataService.getLoading();
        dataService.getData("restaurant_names", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                $(".error_place_holder").html(response.data.message);
                // toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.searchRestaurants = response.data.data;

            }
        });
    }

    function getSearchResult() {
        $.search = function () {
            console.log($scope.cartObj.latitude);
            console.log($scope.cartObj.longitude);
            $(".search").each(function () {
                var $search = $(this);
                var $searchInput = $search.find(".search-input");
                var $searchResult = $search.find(".search-result");
                var $searchItem = $searchResult.find(".search-item");

                $searchInput.on("keyup", function () {
                    if ($(this).val().length > 0) {
                        var cA = JSON.parse(localStorage.getItem("customerAddress"));

                        var searhLat = searhLng = "";
                        if (localStorage.getItem("customerAddress")) {
                            searhLat = cA.latitude;
                            searhLng = cA.longitude;
                        }

                        $.ajax({
                            url: apiBaseUrl + "restaurant_names?keyword=" + $(this).val() + "&latitude=" + searhLat + "&longitude=" + searhLng,
                            type: "GET",
                            success: function (resp) {
                                var html = '';
                                html += '<ul>';
                                $.each(resp.data, function (i, item) {
                                    html += '<li>';
                                    html += '<a href="' + item.seo_url + '" onclick="location.href=this.href" class="search-item"><svg class="icon"><use xlink:href="#food" /></svg>';
                                    html += item.restaurant_name;
                                    html += '</a>';
                                    html += '</li>';
                                });
                                html += '</ul>';
                                $searchResult.html(html).show();
                            }
                        });
                        $searchResult.show();
                    } else {
                        $searchResult.hide();
                    }
                });
                $(document).on("click", ".search-item", function (e) {
                    e.preventDefault();
                    $searchInput.val($(this).text());
                    $searchResult.hide();
                });
            });
        }
        $.search();
    }

});

