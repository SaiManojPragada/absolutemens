app.controller("commonCtrl", function ($scope, dataService, customerService, $rootScope, $sce, $timeout, $cookies) {
    $scope.location = {};
    $scope.cartObj = {};

    dataService.getLoading();
    $rootScope.getCart = function () {
        dataService.getData("cart", {}).then(function (response) {
            dataService.closeLoading();
            console.log("Cart Session id : " + response.data.sess_cart_id);

            if (response.data.err_code === "invalid") {
                $scope.cartObj = {
                    latitude: '',
                    longitude: ''
                };
            } else {
                $scope.cartObj = response.data.data;
                if ($scope.cartObj.length == 0) {
                    $scope.cartObj.cart_items = [];
                }
            }
        });
    };
    $rootScope.getCart();

    $scope.getLocation = function () {
        var latLng = "";
        navigator.geolocation.getCurrentPosition(function (position) {
            console.log(position);

            if (!angular.isDefined($scope.cartObj)) {
                $scope.cartObj = {
                    latitude: '',
                    longitude: ''
                };
            }

            console.log($scope.cartObj);

            var coords = position.coords;
            $scope.cartObj.latitude = coords.latitude;
            $scope.cartObj.longitude = coords.longitude;

            $cookies.put("latitude", $scope.cartObj.latitude);
            $cookies.put("longitude", $scope.cartObj.longitude);

            if ($scope.cartObj.latitude === undefined || $scope.cartObj.longitude === undefined) {
                latLng = new google.maps.LatLng(coords.latitude, coords.longitude);
            } else {
                latLng = new google.maps.LatLng($scope.cartObj.latitude, $scope.cartObj.longitude);
            }
            var geocoder = new google.maps.Geocoder;
            geocodeLatLng(geocoder, latLng);
        }, function (error) {
            toastr.error(error.message);
        });
    };
    //$scope.getLocation();


    $scope.addToCart = function (item) {
        if (item.options.length > 0 || item.add_ons.length > 0) {
            $scope.optionObj = angular.copy(item);
            $('#customizeItemModel').modal({
                backdrop: 'static',
                keyboard: false
            });
            return;
        }
        dataService.postData("cart/add", {
            restaurants_id: item.restaurants_id,
            restaurant_food_items_id: item.id
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                if (angular.isDefined(response.data.err_type)) {
                    if (response.data.err_type == "ORDER_DIFF_RESTAURANT") {
                        swal({
                            title: response.data.title,
                            text: response.data.message,
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, start fresh'
                        }).then((result) => {
                            if (result.value) {
                                swal.close();
                                dataService.getLoading();
                                dataService.postData("cart/delete", {}).then(function (response) {
                                    dataService.closeLoading();
                                    $("#addressForm").LoadingOverlay("hide");
                                    if (response.data.err_code === "invalid") {
                                        toastr.warning(response.data.message);
                                    }
                                    if (response.data.err_code === "valid") {
                                        toastr.success(response.data.message);
                                        $scope.getCart();
                                    }
                                    ;
                                });
                            }
                        });
                    }
                } else {
                    toastr.warning(response.data.message);
                }
            } else {
                if (response.data.title !== 'Cart Updated successfully') {
                    toastr.warning(response.data.message);
                }
            }
            if (response.data.err_code === "valid") {
                $scope.getCart();
            }
        });
    };

    $scope.addCustomizedToCart = function (obj, selected_food_option_id) {
        console.log(obj.add_ons);
        console.log(selected_food_option_id);
        if (obj.options.length > 0) {
            if (!(selected_food_option_id > 0)) {
                swal("Please select Qty");
                return;
            }
        }

        var selected_addons = "";
        if (obj.add_ons.length > 0) {
            selected_addons = [];
            for (var i = 0; i < obj.add_ons.length; i++) {
                //console.log(obj.add_ons[i].is_mandatory + " ---- " + obj.add_ons[i].is_selected);
                if (obj.add_ons[i].is_mandatory == 1 && (obj.add_ons[i].is_selected == false || obj.add_ons[i].is_selected == undefined)) {
                    swal("Please select mandatory addon choices");
                    return false;
                }
                if (obj.add_ons[i].is_selected === true) {
                    selected_addons.push(obj.add_ons[i].id);
                }
            }
            selected_addons = selected_addons.join();
        }
        console.log(selected_addons);

        if (obj.options.length === 0 && obj.add_ons.length > 0) {
            if (selected_addons === "") {
                swal("Please select atleast one addon");
                return;
            }
        }

        console.log({
            restaurants_id: obj.restaurants_id,
            restaurant_food_items_id: obj.id,
            restaurant_food_item_options_id: selected_food_option_id,
            restaurant_food_item_addons_id: selected_addons
        });

        dataService.postData("cart/add", {
            restaurants_id: obj.restaurants_id,
            restaurant_food_items_id: obj.id,
            restaurant_food_item_options_id: selected_food_option_id,
            restaurant_food_item_addons_id: selected_addons
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                if (angular.isDefined(response.data.err_type)) {
                    if (response.data.err_type == "ORDER_DIFF_RESTAURANT") {
                        $('#customizeItemModel').modal("hide");
                        swal({
                            title: response.data.title,
                            text: response.data.message,
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, start fresh'
                        }).then((result) => {
                            if (result.value) {
                                swal.close();
                                dataService.getLoading();
                                dataService.postData("cart/delete", {}).then(function (response) {
                                    dataService.closeLoading();
                                    $("#addressForm").LoadingOverlay("hide");
                                    if (response.data.err_code === "invalid") {
                                        toastr.warning(response.data.message);
                                    }
                                    if (response.data.err_code === "valid") {
                                        toastr.success(response.data.message);
                                        $scope.getCart();
                                    }
                                    ;
                                });
                            }
                        });
                    }
                } else {
                    toastr.warning(response.data.title);
                }

            }
            if (response.data.err_code === "valid") {
                $scope.optionObj = {};
                $('#customizeItemModel').modal("hide");
                $scope.getCart();
            }
        });
    };

    $scope.removeFromCart = function (item) {
        dataService.getLoading();
        dataService.postData("cart/remove_item", {
            id: item.id
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.title);
            }
            if (response.data.err_code === "valid") {
                $scope.getCart();
            }
        });
    }

    $scope.updateQty = function (item, type) {
        if (type == "increase") {
            item.qty++;
        } else if (type == "decrease") {
            item.qty--;
        }
        dataService.getLoading();
        dataService.postData("cart/update_qty", item).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.title);
            }
            if (response.data.err_code === "valid") {
                $scope.getCart();
            }
        });
    }

    var customerAddress = angular.fromJson(localStorage.getItem("customerAddress"));
    if (customerAddress) {
        $scope.customerAddress = customerAddress;
    } else {
        $scope.customerAddress = {};
    }

    $scope.$watch('customerAddress', function (newValue, oldValue, scope) {
        if (newValue != oldValue) {
            localStorage.setItem("customerAddress", angular.toJson($scope.customerAddress));
        }
    }, true);


    $scope.searchLocations = function () {
        var geocoder = new google.maps.Geocoder;
        //Create the search box and link it to the UI element.
        var input = document.getElementsByClassName('pac-input')[0];
        var searchBox = new google.maps.places.SearchBox(input);
        if (!angular.isDefined($scope.customerAddress.latitude)) {
            navigator.geolocation.getCurrentPosition(function (position) {
                console.log(position);
                var coords = position.coords;
                $scope.cartObj.latitude = coords.latitude;
                $scope.cartObj.longitude = coords.longitude;
                var latLng = new google.maps.LatLng($scope.cartObj.latitude, $scope.cartObj.longitude);
                var geocoder = new google.maps.Geocoder;
                geocodeLatLng(geocoder, latLng);
            }, function (error) {
                toastr.error(error.message);
            });
        } else {
            var latLng = new google.maps.LatLng($scope.customerAddress.latitude, $scope.customerAddress.longitude);
            //geocodeLatLng(geocoder, latLng);
        }

        $scope.cartObj.latitude = $scope.customerAddress.latitude;
        $scope.cartObj.longitude = $scope.customerAddress.longitude;

        $cookies.put("latitude", $scope.cartObj.latitude);
        $cookies.put("longitude", $scope.cartObj.longitude);


        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {

            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            // For each place, get the icon, name and location.
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                $scope.customerAddress.address = place.formatted_address;
                var latLng = place.geometry.location;
                $scope.customerAddress.latitude = latLng.lat();
                $scope.customerAddress.longitude = latLng.lng();
                $scope.customerAddress.name = place.name;
                $scope.customerAddress.location = place.name;
                $scope.$apply();
                removeSideBar();
                getRestaurants();

                //$scope.getLocationStoresCount();
            });
        });
    }

    $scope.searchLocations();
    getAddresses();

    $scope.getRestaurants = function () {
        getRestaurants();
    }

    function getRestaurants() {
        $scope.restaurants = [];
        dataService.getLoading();
        dataService.postData("restaurants", {
            latitude: $scope.customerAddress.latitude,
            longitude: $scope.customerAddress.longitude
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                $(".error_place_holder").html(response.data.message);
                // toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.restaurants = response.data.data;

            }
        });
    }


    $scope.getLocationStoresCount = function () {
        dataService.getData("locations/stores_count", {
            latitude: $scope.customerAddress.latitude,
            longitude: $scope.customerAddress.longitude
        }).then(function (response) {
            dataService.closeLoading();
            $scope.locationStores = [];
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.locationStores = response.data.data;
            }
        });
    }


    $scope.getCuisineCategories = function () {
        dataService.getData("cuisines", {
            latitude: $scope.customerAddress.latitude,
            longitude: $scope.customerAddress.longitude
        }).then(function (response) {
            dataService.closeLoading();
            $scope.cuisineCategories = [];
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.cuisineCategories = response.data.data;
            }
        });
    };


    function removeSideBar() {
        $(".sidebar").removeClass("sidebar-in");
        $(".sidebar-overlay").removeClass("sidebar-in");
        $("body").removeClass("sidebar-in");
    }


    $scope.selectAddress = function (address) {
        if (address) {
            $scope.customerAddress.address = address.address;
            $scope.customerAddress.latitude = address.latitude;
            $scope.customerAddress.longitude = address.longitude;
            $scope.customerAddress.name = address.address_type;
            removeSideBar();
            getRestaurants();
        }
    };

    function getAddresses() {
        dataService.getLoading();
        customerService.postData("addresses", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.homeAddresses = response.data.data;
            }
        });
    }


    function geocodeLatLng(geocoder, latLng) {
        console.trace();
        console.log(latLng);
        geocoder.geocode({
            'location': latLng
        }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    $scope.customerAddress.address = results[0].formatted_address;
                    $scope.customerAddress.latitude = latLng.lat();
                    $scope.customerAddress.longitude = latLng.lng();
                    $scope.customerAddress.name = results[0].address_components[1].long_name;
                    $scope.customerAddress.location = results[0].address_components[1].long_name;
                    $scope.$apply();
                    removeSideBar();
                    getRestaurants();
                } else {
                    toastr.warning('No results found');
                }
            } else {
                toastr.error('Geocoder failed due to: ' + status);
            }
        });
    }

    $scope.addToFavourite = function (restaurant) {
        if (!restaurant) {
            toastr.warning("Invalid Restaurant");
            return;
        }

        dataService.getLoading();
        customerService.getData("update_to_favourite", {
            restaurants_id: restaurant.id
        }).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                toastr.success(response.data.message);
                console.log(restaurant.is_favourite);
                restaurant.is_favourite = !restaurant.is_favourite;
                console.log(restaurant.is_favourite);

            }
        });
    }

    $scope.getCms = function (path) {
        dataService.getLoading();
        dataService.postData("cms/" + path, {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.cms = response.data.page;
                $scope.cms.description = $sce.trustAsHtml($scope.cms.description);
            }
        });
    };



    $scope.getCmsAbout = function () {
        dataService.getLoading();
        dataService.postData("cms/about_us", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                // $scope.cms = response.data.page;
                console.log('descriptionAbt', response.data.page)
                $scope.cms.descriptionAbt = $sce.trustAsHtml(response.data.page.description);
                var dt = $scope.cms.descriptionAbt;
                dt = String(dt).replace(/<[^>]+>/gm, '');
                $scope.cms.descriptionAbt = dt.substr(0, 50);
                console.log('change description: ', $scope.cms.descriptionAbt);
            }
        });
    };

    $scope.ProceedToCheckOut = function () {
        if ($scope.cartObj.sub_total < $scope.cartObj.restaurant_info.minimum_order_amount) {
            $scope.minimum_order_error = "Minimum order Subtotal should be Rs." + $scope.cartObj.restaurant_info.minimum_order_amount + "/-";

            $timeout(function () {
                $scope.minimum_order_error = "";
            }, 4000);
            //toastr.warning("Minimum order should be Rs."+$scope.restaurant.minimum_order_amount+"/-");
        } else {
            location.href = baseurl + "checkout";
        }
    };




});