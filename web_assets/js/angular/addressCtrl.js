$.fn.modal.Constructor.prototype.enforceFocus = function () {};
$(function () {
    $("#addressForm").validate({
        rules: {
            address: {
                required: true,
            },
            door_no: {
                required: true,
            },
            landmark: {
                required: true,
            },
            address_type: {
                required: true,
            }

        },
        messages: {
            door_no: {
                required: "Enter Door / Flat No",
            },
            address: {
                required: "Select Address from Map",
            },
            landmark: {
                required: "Enter  Landmark",
            },
            address_type: {
                required: "Select Address Type",
            },
        }
    });
});
app.controller("addressCtrl", function ($scope, dataService, customerService, $timeout) {
    $scope.address = {};

    var latLng = new google.maps.LatLng($scope.customerAddress.latitude, $scope.customerAddress.longitude);
    var map = new google.maps.Map(document.getElementById('map_canvas'), {
        zoom: 12,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var bounds = new google.maps.LatLngBounds();
    var geocoder = new google.maps.Geocoder;
    // Create the search box and link it to the UI element.
    var input = document.getElementsByClassName('pac-input')[1];
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    // var map = new google.maps.Map(document.getElementById('map_canvas'), {
    //   zoom: 12,
    //   center: new google.maps.LatLng($scope.cartObj.latitude, $scope.cartObj.longitude),
    //   mapTypeId: google.maps.MapTypeId.ROADMAP
    // });

    $('#addressModal').on('shown.bs.modal', function () {
        if ($scope.address.id) {
            latLng = new google.maps.LatLng($scope.address.latitude, $scope.address.longitude);
            initializeMap();
        } else {
            latLng = new google.maps.LatLng($scope.customerAddress.latitude, $scope.customerAddress.longitude);
            initializeMap();
        }
    });

    $scope.useCurrentLocation = function () {
        latLng = new google.maps.LatLng($scope.customerAddress.latitude, $scope.customerAddress.longitude);
        initializeMap();
    }



    $scope.showAddressModal = function () {
        $scope.address = {};
        $('#addressModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    }

    $scope.editAddress = function (address) {
        $scope.address = angular.copy(address);
        $('#addressModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    }


    getAddresses();

    $scope.addUpdateAddress = function () {
        if ($("#addressForm").valid()) {
            dataService.getLoading();
            customerService.postData("addresses/add", $scope.address).then(function (response) {
                dataService.closeLoading();
                $("#addressForm").LoadingOverlay("hide");
                if (response.data.err_code === "invalid") {
                    toastr.warning(response.data.message);
                }
                if (response.data.err_code === "valid") {
                    toastr.success(response.data.message);
                    location.reload();
                }
                ;
            });
        }
        ;
    };

    $scope.deleteAddress = function (address) {
        swal({
            title: 'Delete Address',
            text: "Are you sure?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                dataService.getLoading();
                customerService.postData("addresses/delete", {
                    id: address.id
                }).then(function (response) {
                    dataService.closeLoading();
                    $("#addressForm").LoadingOverlay("hide");
                    if (response.data.err_code === "invalid") {
                        toastr.warning(response.data.message);
                    }
                    if (response.data.err_code === "valid") {
                        toastr.success(response.data.message);
                        location.reload();
                    }
                    ;
                });
            }
        });

    };

    function getAddresses() {
        dataService.getLoading();
        customerService.postData("addresses", {}).then(function (response) {
            dataService.closeLoading();
            if (response.data.err_code === "invalid") {
                toastr.warning(response.data.message);
            }
            if (response.data.err_code === "valid") {
                // toastr.success(response.data.message);
                $scope.addresses = response.data.data;
            }
        });
    }
    ;

    function initializeMap() {
        geocodeLatLng(latLng);
        var markers = [];
        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];
        if ($scope.address.id) {
            markers.push(new google.maps.Marker({
                position: latLng,
                draggable: true
            }));
            map.setCenter(markers[0].position);
            markers[0].setMap(map);
            google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                console.log(evt.latLng);
                geocodeLatLng(evt.latLng);
            });
            google.maps.event.addListener(markers[0], 'dragstart', function (evt) {
                console.log(evt.latLng);
                geocodeLatLng(evt.latLng);
            });
        } else {
            var position = new google.maps.LatLng($scope.customerAddress.latitude, $scope.customerAddress.longitude);
            bounds.extend(position);
            markers.push(new google.maps.Marker({
                position: position,
                draggable: true
            }));
            map.setCenter(markers[0].position);
            markers[0].setMap(map);
            google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                console.log(evt.latLng);
                geocodeLatLng(evt.latLng);
            });
            google.maps.event.addListener(markers[0], 'dragstart', function (evt) {
                console.log(evt.latLng);
                geocodeLatLng(evt.latLng);
            });
        }

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {

            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }

            // For each place, get the icon, name and location.

            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                /*markers.push(new google.maps.Marker({
                 map: map,
                 icon: icon,
                 title: place.name,
                 position: place.geometry.location
                 }));
                 */
                markers.push(new google.maps.Marker({
                    position: place.geometry.location,
                    draggable: true
                }));

                geocodeLatLng(place.geometry.location);

                google.maps.event.addListener(markers[0], 'dragend', function (evt) {
                    console.log(evt.latLng);
                    geocodeLatLng(evt.latLng);
                });

                google.maps.event.addListener(markers[0], 'dragstart', function (evt) {
                    console.log(evt.latLng);
                    geocodeLatLng(evt.latLng);
                });
                map.setCenter(markers[0].position);
                markers[0].setMap(map);

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }
    ;

    function geocodeLatLng(latLng) {
        geocoder.geocode({
            'location': latLng
        }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    $scope.address.address = results[0].formatted_address;
                    $scope.$apply();
                    $scope.address.latitude = latLng.lat();
                    $scope.address.longitude = latLng.lng();
                } else {
                    toastr.warning('No results found');
                }
            } else {
                toastr.error('Geocoder failed due to: ' + status);
            }
        });
    }


});