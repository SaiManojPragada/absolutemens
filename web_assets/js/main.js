$(".interface_sidebar_toggle").bind("click", function () {
    if (!$(".interface_sidebar").hasClass("active")) {
        $(".interface_sidebar").addClass("active");
        $(".interface_sidebar_toggle").addClass("active");
    } else {
        $(".interface_sidebar").removeClass("active");
        $(".interface_sidebar_toggle").removeClass("active");
    }

});
$('.topaddscroll').slick({
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
        {
            breakpoint: 993,
            settings: {
                slidesToShow: 1
            }
        },
        {
            breakpoint: 870,
            settings: {
                slidesToShow: 1
            }
        },
        {
            breakpoint: 650,
            settings: {
                slidesToShow: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                //centerMode: true
            }
        }
    ]
});
$('.dealaddscroll').slick({
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive: [
        {
            breakpoint: 993,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 870,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 650,
            settings: {
                slidesToShow: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                //centerMode: true
            }
        }
    ]
});
$('.restaurent-slider').slick({
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
});


$('#chksidebar').stickySidebar({
    topSpacing: 60,
    bottomSpacing: 60,
});
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 150) {
        $(".header").addClass("shadowheader");
    } else {
        $(".header").removeClass("shadowheader");
    }
});

$('.count').prop('disabled', true);
    $(document).on('click', '.plus', function () {
        $('.count').val(parseInt($('.count').val()) + 1);
    });
    $(document).on('click', '.minus', function () {
        $('.count').val(parseInt($('.count').val()) - 1);
        if ($('.count').val() == 0) {
            $('.count').val(1);
        }
    });
    
    
$('#catgbox').change(function() {
    $('.prdetails').hide();
    $('#' + $(this).val()).show();
 });