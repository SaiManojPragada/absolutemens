var u = location.port != "" ? ':' + location.port : "";
var baseurl = location.protocol + "//" + document.domain + u + '/';
var apiBaseUrl = location.protocol + "//" + document.domain + u + '/api/';

if (document.domain === "mumstores.co.in" || document.domain === "www.mumstores.co.in") {
    var baseurl = location.protocol + "//" + document.domain + u + '/';
    var apiBaseUrl = location.protocol + "//" + document.domain + u + '/api/';
} else if (document.domain === "admin.mumstores.co.in") {
    var baseurl = location.protocol + "//" + document.domain + u + '/';
    var apiBaseUrl = location.protocol + "//" + document.domain + u + '/api/';
} else if (document.domain === "test.mumstores.co.in") {
    var baseurl = location.protocol + "//" + document.domain + u + '/';
    var apiBaseUrl = location.protocol + "//" + document.domain + u + '/api/';
} else {
    var baseurl = "https://colormoon.in/absolutemens/";
    var apiBaseUrl = "https://colormoon.in/absolutemens" + '/api/';
}