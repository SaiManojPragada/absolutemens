<html>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>uploads/<?= $site_property->favicon ?>">
</html>
<div class="pkgsteps">
    <?= $this->load->view("includes/header", $main_data); ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <?php $this->load->view('menu-dashboard'); ?>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="whitebox contentdashboard">
                    <h3>Change Password</h3>	
                    <br>
                    <?php if (!empty($this->session->userdata('change_password_s'))) { ?><p style="color: green"><?= $this->session->userdata('change_password_s') ?></p><?php } ?>
                    <?php if (!empty($this->session->userdata('change_password_e'))) { ?><p style="color: tomato"><?= $this->session->userdata('change_password_e') ?></p><?php } ?>
                    <?php
                    $this->session->unset_userdata('change_password_s');
                    $this->session->unset_userdata('change_password_e');
                    ?>
                    <br>
                    <center>
                        <div style="padding: 30px 30px">
                            <form method="post" action="<?= base_url('change_password/change') ?>" id="changePassForm">
                                <div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fal fa-key"></i></span>
                                        </div>
                                        <input type="password" name="old_password" id="old_password" class="form-control" placeholder="Enter Old Password *">
                                    </div><br>

                                    <div style="width: 100%"><span id="new_pass_err"></span></div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fal fa-key"></i></span>
                                        </div>
                                        <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter New Password *">
                                    </div><br>
                                    <div style="width: 100%"><span id="re_new_pass_err"></span></div>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1"><i class="fal fa-sync"></i></span>
                                        </div>
                                        <input type="password" name="re_new_password" id="re_new_password" class="form-control" placeholder="Re Enter New Password *">
                                    </div><br>
                                    <div class="form-group">
                                        <button type="submit" name="submit" value="submit" class="btn btn-primary" ><i class="fal fa-paper-plane"></i> Change Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </center>
                    <style>
                        #new_pass_err, #re_new_pass_err {
                            color: tomato;
                            text-align: left;
                            float: left
                        }
                    </style>
                    <script>
                        $("#changePassForm").submit(function (e) {
                            var pass = document.getElementById("new_password").value;
                            var re_pass = document.getElementById("re_new_password").value;
                            var pass_reg = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)\S{6,50}$/);
                            if (pass.length < 6) {
                                $("#new_pass_err").html("* Password must be atleast 6 Characters");

                                e.preventDefault();
                                return false;
                            } else {
                                $("#new_pass_err").html("");
                            }

                            if (!pass_reg.test(pass)) {
                                $("#new_pass_err").html("* Password should contain atleast one Capital letter,Small letter and minimum of 6 characters with no spaces");
                                e.preventDefault();
                                return false;
                            } else {
                                $("#new_pass_err").html("");
                            }

                            if (pass !== re_pass) {
                                $("#re_new_pass_err").html("* Passwords does not Match");

                                e.preventDefault();
                                return false;
                            } else {
                                $("#re_new_pass_err").html("");
                            }
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>