<div class="pkgsteps">
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <div class="container" style="margin-top: 80px">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12">
                <div class="whitebox">
                    <form class="new-added-form" method="post" id="form_step_one" action="javascript: void(0);">
                        <section>
                            <p><strong>How would you describe your style?</strong></p>
                            <div class="row justify-content-center">
                                <div class="col-lg-7 col-md-7">
                                    <img src="<?= base_url('assets') ?>/images/pkgbanners.gif" alt="" class="img-fluid">
                                </div>

                                <center class="col-lg-12 col-md-12"><span id="styles_err" style="color: tomato"></span></center>
                                <div class="col-lg-10 col-md-10 text-center selfashion">
                                    <div class="input-group">
                                        <?php foreach ($styles as $item) { ?>
                                            <span class="button input-group-btn">
                                                <button type="button" class="btn btn-outline-primary <?php
                                                if ($selected_styles && str_contains($selected_styles, $item->id . ',')) {
                                                    echo "btn-primary active";
                                                }
                                                ?>" style="margin: 10px" data-color="primary" onclick="checkThis(this, '<?= $item->id ?>')"><?= $item->name ?></button>
                                                <input type="checkbox" value="<?= $item->id ?>" class="hidden" id="checkstyle<?= $item->id ?>"/>
                                            </span>
                                        <?php } ?>
                                    </div>
                                </div>
                                <input id="selected_styles" type="hidden" name="selected_styles" value="<?= $selected_styles ?>">
                                <input id="unq_id" type="hidden" name="unq_id" value="<?= $random_unq_key ?>">
                                <div class="col-lg-12 col-md-12">
                                    <p class="stytxt">" Please select the styles Minimum of <?= ($package_details) ? $package_details->min_styles : 1 ?> and maximum of <?= ($package_details) ? $package_details->max_styles : sizeof($styles) ?>"</p>
                                </div>
                                <div class="col-lg-12 col-md-12 mt-5">
                                    <div class="d-flex justify-content-end">
                                        <button type="submit" class="btn-next" onclick="checkSubmit()">NEXT</button>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </form>
                </div>
            </div>
            <?php if ($current_package !== "we-shop-for-you") { ?>
                <div class="col-lg-4 col-md-8">
                    <div class="d-flex justify-content-between mb-2 align-items-center">
                        Selected Package
                        <a href="<?= base_url('packages') ?>" class="btn btn-sm btn-primary"><i class="fal fa-sync"></i> Change Package</a>
                    </div>
                    <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                        <div class="banner-img">
                            <a href="#"><img src="<?= base_url() ?>uploads/<?= $package_details->image ?>" alt="banner" /></a>
                        </div>
                        <div class="banner-content-3">
                            <?= $package_details->title ?>
                            <h5><?= $package_details->no_of_items ?> items / <?= $package_details->duration_in_words ?></h5>
                            <p>Subscription Start from <?= (int) $package_details->price ?>/month</p>
                            <?= $package_details->subscription_features ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="best-sell-active owl-carousel owl-theme">
                    <?php foreach ($package_banners as $banner) { ?>
                        <div class="item"><a href="#"><img src="<?= base_url() ?>uploads/<?= $banner->image ?>" alt=""></a></div>
                            <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var this_check = "<?= $selected_styles ?>";
    var action_url = "<?= base_url('package_steps/submit_form/' . $current_package) ?>";
    var max_styles = <?= ($package_details) ? $package_details->max_styles : sizeof($styles) ?>;
    var min_styles = <?= ($package_details) ? $package_details->min_styles : 1 ?>;
    function checkThis(eve, id) {
        var temp = id + ',';

        if (eve.classList.contains('btn-primary')) {
            eve.classList.remove("btn-primary");
            eve.classList.remove("active");
        }

        if (this_check.includes(id)) {
            this_check = this_check.replace(temp, '');
        } else {
            if (this_check.length <= (max_styles - 1) * 2) {
                this_check += temp;
                eve.classList.add('btn-primary', 'active');
            } else {
                document.getElementById("styles_err").innerHTML = "You can Only select Maximum of " + max_styles + " Styles";
            }
        }

        document.getElementById("selected_styles").value = this_check;
    }

    function checkSubmit() {
        if (this_check.length >= min_styles * 2 && this_check.length <= max_styles * 2) {
            document.getElementById("styles_err").innerHTML = "";
            document.getElementById("form_step_one").setAttribute("action", action_url);
        } else {
            document.getElementById("styles_err").innerHTML = "Please Select Atlest " + min_styles + " Styles";
        }
    }
</script>