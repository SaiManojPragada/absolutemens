<div class="dashboard-menu d-none d-lg-block" id="sidebar">
    <h4>DASHBOARD MENU</h4>
    <ul>
        <li <?php if ($user_page == 'dashboard') { ?>class="active"<?php } ?>><a href="<?= base_url() ?>dashboard"><i class="fal fa-user"></i> My Profile</a></li>
        <li <?php if ($user_page == 'sizes') { ?>class="active"<?php } ?>><a href="<?= base_url() ?>dashboard/sizes"><i class="fal fa-ruler-horizontal"></i> Sizes </a></li>
        <!--<li <?php if ($user_page == 'preferances') { ?>class="active"<?php } ?>><a href="<?= base_url() ?>dashboard/subscription-preferances"><i class="fal fa-envelope-open"></i> Subscription & Preferances</a></li>-->
        <li <?php if ($user_page == 'dash-we-shop-for-you') { ?>class="active"<?php } ?>><a href="<?= base_url() ?>dashboard/we-shop-for-you"><i class="fal fa-shopping-bag"></i> We Shop For You</a></li>
        <li <?php if ($user_page == 'myorders') { ?>class="active"<?php } ?>><a href="<?= base_url() ?>dashboard/my-orders"><i class="fal fa-list"></i> My Subscriptions</a></li>
        <li <?php if ($user_page == 'report') { ?>class="active"<?php } ?>><a href="<?= base_url() ?>dashboard/report"><i class="fal fa-comment"></i> Report Issue</a></li>
        <li <?php if ($user_page == 'changepassword') { ?>class="active"<?php } ?>><a href="<?= base_url() ?>dashboard/change-password"><i class="fal fa-lock-alt"></i> Change Password</a></li>
        <li><a href="<?= base_url('authentication/logout') ?>"><i class="fal fa-power-off"></i> Logout</a></li>
    </ul>
</div>