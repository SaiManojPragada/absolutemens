<div class="wrapper wrapper-content animated fadeInLeft">
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="col-md-3">
            <h3>Welcome to <?= SITE_TITLE ?></h3>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">

        <div class="col-lg-3" onclick="location.href = '<?= base_url() ?>admin/customers'" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Registered Users </h3><br>
                        <h2 class="font-bold"><?= $registered_users ?></h2>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-lg-3" onclick="location.href = '<?= base_url() ?>admin/designers_analysts'" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Fashion Managers </h3><br>
                        <h2 class="font-bold"><?= $fashion_managers ?></h2>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-lg-3" onclick="location.href = '<?= base_url() ?>admin/designers_analysts'" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Fashion Analysts </h3><br>
                        <h2 class="font-bold"><?= $fashion_analysts ?></h2>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-lg-3" onclick="location.href = '<?= base_url() ?>admin/vendors'" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Vendors </h3><br>
                        <h2 class="font-bold"><?= $vendors ?></h2>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-lg-12">
            <hr>
            <h2>Amounts Info</h2>
        </div>
        <div class="col-lg-6" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-money fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Completed Orders Amount </h3><br>
                        <h2 class="font-bold"><?= !empty($completed_orders_amount->amount_paid) ? $completed_orders_amount->amount_paid : "0" ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-money fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Pending Orders Amount </h3><br>
                        <h2 class="font-bold"><?= !empty($pending_orders_amount->amount_paid) ? $pending_orders_amount->amount_paid : "0" ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <hr>
            <h2>Orders Info</h2>
        </div>
        <div class="col-lg-3" onclick="location.href = '<?= base_url() ?>admin/orders'" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Total Orders </h3><br>
                        <h2 class="font-bold"><?= $total_orders ?></h2>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-lg-3" onclick="location.href = '<?= base_url() ?>admin/orders'" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-check fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Pending Orders </h3><br>
                        <h2 class="font-bold"><?= $pending_orders ?></h2>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-lg-3" onclick="location.href = '<?= base_url() ?>admin/orders?type=delivered'" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-check fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Completed Orders </h3><br>
                        <h2 class="font-bold"><?= $completed_orders ?></h2>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-lg-3" onclick="location.href = '<?= base_url() ?>admin/orders?type=cancelled'" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-times fa-5x"></i>
                    </div>
                    <div class="col-xs-8 text-right">
                        <h3> Cancelled Orders </h3><br>
                        <h2 class="font-bold"><?= $cancelled_orders ?></h2>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-lg-12" onclick="location.href = '<?= base_url() ?>admin/orders'" style="cursor:pointer">
            <div class="widget style1 navy-bg">
                <div class="row">
                    <div class="col-xs-2">
                        <i class="fa fa-clock-o fa-5x"></i>
                    </div>
                    <div class="col-xs-8">
                        <center>
                            <h3> Today's Orders </h3><br>
                            <h2 class="font-bold"><?= sizeof($todays_orders) ?></h2>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <?= $completed_orders_amount->amount_paid ?>
            <hr>
            <h2>Today's Orders</h2>
            <table class="table table-striped table-bordered dataTables-example dataTable">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Order Id</th>
                        <th>Customer Details</th>
                        <th>Package Details</th>
                        <th>Amount Paid</th>
                        <th>Status</th>
                        <th>Ordered On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($todays_orders as $index => $order) { ?>
                        <tr>
                            <td><?= $index + 1 ?></td>
                            <td><?= $order->unq_id ?></td>
                            <td><p><?= $order->customer_details->name ?></p>
                                <p><b>Ph : </b><?= $order->phone_number ?></p>
                                <p><b>Email : </b><?= $order->customer_details->email ?></p>
                            </td>
                            <td><p><?= strip_tags($order->package_details->title) ?></p>
                                <p><b>Package Expiry Days : </b><?= strip_tags($order->package_details->duration) ?> days</p>
                            </td>
                            <td>Rs. <?= $order->amount_paid ?></td>
                            <td><?= $order->order_status ?></td>
                            <td><?= date('d M Y, h:i A', $order->updated_at) ?></td>
                        </tr>
                    <?php } ?>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>Order Id</th>
                        <th>Customer Name</th>
                        <th>Package Details</th>
                        <th>Amount Paid</th>
                        <th>Status</th>
                        <th>Ordered On</th>
                    </tr>
                </tfoot>
            </table>
        </div>

    </div>
    <div class="row">
    </div>
</div>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('Monitor every thing...', 'Welcome to <?= SITE_TITLE ?>');

        }, 1300);
    });
</script>
<script>
    $(document).ready(function () {
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'excel', title: 'Today Orders'},
                {extend: 'pdf', title: 'Today Orders'},

                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]

        });
    });
</script>