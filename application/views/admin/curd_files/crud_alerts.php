<?php
if (isset($_GET['msg'])) {
    if ($_GET['msg'] == 'update') {
        ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            Updated Successfully..!
        </div>
    <?php } ?>	
    <?php if ($_GET['msg'] == 'add') { ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            Added Successfully..!
        </div>
    <?php } ?>
    <?php if ($_GET['msg'] == 'delete') { ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            Deleted Successfully..!
        </div>
    <?php } ?>
    <?php if ($_GET['msg'] == 'error') { ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            Unable to Update
        </div>
    <?php } ?>
    <?php if (!empty($this->session->userdata('s_error'))) { ?>
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?=
            $this->session->userdata('s_error');
            $this->session->unset_userdata('s_error');
            ?>
        </div>
    <?php } ?>
    <?php if (!empty($this->session->userdata('s_scss'))) { ?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?=
            $this->session->userdata('s_scss');
            $this->session->unset_userdata('s_scss');
            ?>
        </div>
    <?php } ?>
<?php } ?>