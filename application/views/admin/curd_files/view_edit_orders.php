<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= singular($subject) ?> <?= $this->input->get_post('display_text') ?> Details</h5>
                    <div class="ibox-tools">
                        <?php
                        if (isset($action_buttons["back_action"])) {
                            if ($action_buttons["back_action"] == true) {
                                ?>
                                <?php /* <a class="btn btn-primary btn-xs" href="<?= $current_page_link ?>"> */ ?>
                                <button class="btn btn-primary btn-xs" onclick="window.history.go(-1)">
                                    <i class="fa fa-chevron-left" aria-hidden="true"></i> Back
                                </button>
                                <?php
                            } else {
                                
                            }
                        } else {
                            ?>
                            <button class="btn btn-primary btn-xs" onclick="window.history.go(-1)">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i> Back
                            </button>
                        <?php } ?>
                    </div>
                </div>
                <div class="ibox-content row" style="margin: 0px 0px">
                    <div class="col-md-12">
                        <p style="text-align: right; font-size: 18px;"><b>Order Status : </b><?= $edit_item_row->order_status ?></p>
                        <p style="position: absolute; top: 0; font-size: 18px;"><b>Order ID : </b>#<?= $edit_item_row->unq_id ?> <?php if ($edit_item_row->is_we_shop_for_you) { ?><span style="color: green; font-weight: bold">(We Shop For You)</span><?php } ?></p>
                    </div>
                    <div class="col-md-12"><hr></div>

                    <div class="col-md-6">
                        <h3>User Details and Address</h3>
                        <hr>
                        <p><b class="col-md-4">Name : </b><?= strip_tags($edit_item_row->address_name) ?></p>
                        <p><b class="col-md-4">E-mail : </b><?= strip_tags($edit_item_row->address_email) ?></p>
                        <p><b class="col-md-4">Phone Number : </b><?= strip_tags($edit_item_row->phone_number) ?></p>
                        <p><b class="col-md-4">Alternative Ph Number : </b><?= !empty($edit_item_row->alternative_phone_number) ? strip_tags($edit_item_row->alternative_phone_number) : 'N/A' ?></p>
                        <p><b class="col-md-4">Address : </b><?= strip_tags($edit_item_row->address) ?></p>
                    </div>
                    <div class="col-md-6">
                        <h3>Package & Payment Details</h3>
                        <hr>
                        <p><b class="col-md-4">Package title : </b><?= (!$edit_item_row->is_we_shop_for_you) ? $edit_item_row->package_title : "We Shop For You" ?></p>
                        <p><b class="col-md-4">Grand Total : </b><?= $edit_item_row->grand_total ?></p>
                        <p><b class="col-md-4">Discount : </b><?= $edit_item_row->discount ?></p>
                        <p><b class="col-md-4">Coupon : </b><?= !empty($edit_item_row->coupon_code) ? $edit_item_row->coupon_code : "N/A" ?></p>
                        <p><b class="col-md-4">Coupon Discount : </b><?= !empty($edit_item_row->coupon_discount) ? $edit_item_row->coupon_discount : "N/A" ?></p>
                        <p><b class="col-md-4">Delivery Charges : </b><?= $edit_item_row->delivery_charges ?></p>
                        <p><b class="col-md-4">Razorpay Order Id : </b><?= $edit_item_row->razorpay_order_id ?></p>
                        <p><b class="col-md-4">Razorpay Payment Id : </b><?= $edit_item_row->razorpay_payment_id ?></p>
                        <p><b class="col-md-4">Amount Paid : </b><?= $edit_item_row->amount_paid ?></p>
                        <p><b class="col-md-4">Order Status : </b><?= $edit_item_row->order_status ?></p>
                    </div>
                    <div class="col-md-12"><hr></div>


                    <div class="col-md-6">
                        <h3>Order Dates</h3>
                        <hr>
                        <?php if ($edit_item_row->created_at) { ?>
                            <p><b>Order Created On : </b><?= date('d M Y, h:i A', $edit_item_row->created_at) ?></p>
                        <?php } ?>
                        <?php if ($edit_item_row->dispatched_date) { ?>
                            <p><b>Order Dispatched On : </b><?= date('d M Y, h:i A', $edit_item_row->dispatched_date) ?></p>
                        <?php } ?>
                        <?php if ($edit_item_row->shipped_date) { ?>
                            <p><b>Order Shipped On : </b><?= date('d M Y, h:i A', $edit_item_row->shipped_date) ?></p>
                        <?php } ?>
                        <?php if ($edit_item_row->delivered_date) { ?>
                            <p><b>Order Delivered On : </b><?= date('d M Y, h:i A', $edit_item_row->delivered_date) ?></p>
                        <?php } ?>
                        <?php if ($edit_item_row->exchange_request_date) { ?>
                            <p><b>Order Exchange Requested On : </b><?= date('d M Y, h:i A', $edit_item_row->exchange_request_date) ?></p>
                        <?php } ?>
                        <?php if ($edit_item_row->cancellation_requested_date) { ?>
                            <p><b>Order Cancellation Requested On : </b><?= date('d M Y, h:i A', $edit_item_row->cancellation_requested_date) ?></p>
                        <?php } ?>
                        <?php if ($edit_item_row->cancelled_date) { ?>
                            <p><b>Order Cancelled On : </b><?= date('d M Y, h:i A', $edit_item_row->cancelled_date) ?></p>
                        <?php } ?>
                        <?php if ($edit_item_row->exchange_remarks) { ?>
                            <p><b>Order Exchange Remarks : </b><?= $edit_item_row->exchange_remarks ?></p>
                        <?php } ?>
                        <?php if ($edit_item_row->cancellation_reason) { ?>
                            <p><b>Order Cancellation Reason : </b><?= $edit_item_row->cancellation_reason ?></p>
                        <?php } ?>
                    </div>

                    <div class="col-md-6" style="display: none">
                        <h3>User Sizes Details</h3>
                        <hr>
                        <?php foreach ($selected_sizes as $s) { ?>
                            <p><b class="col-md-4"><?= $s->category ?> Size : </b><?= $s->size ?></p>
                        <?php } ?>

                        <p><b class="col-md-4">Height : </b><?= $edit_item_row->height ?></p>
                        <p><b class="col-md-4">Weight : </b><?= $edit_item_row->weight ?></p>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
</div>