
<?php include_once('crud_alerts.php') ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $subject ?></h5>
                </div>
                <div class="ibox-content">
                    <ul class="nav nav-tabs">
                        <li class="<?= ($_GET['type'] == 'all' || !$_GET['type']) ? 'active' : '' ?>"><a href="<?= base_url('admin/product_orders') ?>">All Orders</a></li>
                        <li class="<?= ($_GET['type'] == 'inprogress') ? 'active' : '' ?>"><a href="<?= base_url('admin/product_orders?type=inprogress') ?>">In Progress</a></li>
                        <li class="<?= ($_GET['type'] == 'completed') ? 'active' : '' ?>"><a href="<?= base_url('admin/product_orders?type=completed') ?>">Completed</a></li>
                        <li class="<?= ($_GET['type'] == 'cancelled') ? 'active' : '' ?>"><a href="<?= base_url('admin/product_orders?type=cancelled') ?>">Cancelled</a></li>
                    </ul>
                    <br><br>
                    <?php if ($this->input->get('type') == 'inprogress') { ?>
                        <div>
                            <form method="get" class="row" action="">
                                <input type="hidden" name="type" value="<?= $this->input->get('type') ?>">
                                <div class="form-group col-md-2">
                                    <select class="form-control" name="inprogress">
                                        <option value="">All</option>
                                        <option value="tobeordered" <?= ($this->input->get('inprogress') == 'tobeordered') ? 'selected' : '' ?>>To Be Ordered</option>
                                        <option value="ordered" <?= ($this->input->get('inprogress') == 'ordered') ? 'selected' : '' ?>>Ordered</option>
                                        <option value="delivered" <?= ($this->input->get('inprogress') == 'delivered') ? 'selected' : '' ?>>Delivered</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <button class="btn btn-success" style="width: 20%">Submit</button>
                                </div>
                            </form>
                        </div>
                    <?php } ?>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Order Id</th>
                                    <th>Order Details</th>
                                    <th>Order Date</th>
                                    <th>Order Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $count = 1; ?>
                                <?php foreach ($list_items as $item) { ?>
                                    <tr>
                                        <td><?= $count++ ?></td>
                                        <td><?= $item->order_id ?></td>
                                        <td>
                                            <p><b>No Of Items Selected : </b><?= sizeof($item->order_products) ?></p>
                                            <table class="table table-striped table-bordered table-hover">
                                                <tbody>
                                                    <tr>
                                                        <th>Fashion Manager</th>
                                                        <td>
                                                            <p><b>Name : </b><?= $item->fashion_manager->name ?></p>
                                                            <p><b>Email : </b><a href="mailto:<?= $item->fashion_manager->email ?>"><?= $item->fashion_manager->email ?></a>,
                                                                <b>Ph : </b><a href="tel:<?= $item->fashion_manager->phone_number ?>"><?= $item->fashion_manager->phone_number ?></a></p>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fashion Analyst</th>
                                                        <td>
                                                            <p><b>Name : </b><?= $item->fashion_analyst->name ?></p>
                                                            <p><b>Email : </b><a href="mailto:<?= $item->fashion_analyst->email ?>"><?= $item->fashion_analyst->email ?></a>,
                                                                <b>Ph : </b><a href="tel:<?= $item->fashion_analyst->phone_number ?>"><?= $item->fashion_analyst->phone_number ?></a></p>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td><?= date('d-M-Y', strtotime($item->order_date)) ?></td>
                                        <td>
                                            <p><b>Pending Products : <?= $item->order_pending ?></b></p>
                                            <p><b>Products Dispatched: <?= $item->order_dispatched_by_vendor ?></b></p>
                                            <p><b>Delivered : <?= $item->order_delivered ?></b></p>
                                            <p><b>Cancelled : <?= $item->order_cancelled_by_vendor ?></b></p>
                                            <?php if ($item->order_status == "order_completed") { ?>
                                                <p><span style="background-color: green; padding: 3px 10px; color: white; border-radius: 10px">Order Completed</span></p>
                                            <?php } else if ($item->order_status == "order_cancelled") { ?>
                                                <p><span style="background-color: red; padding: 3px 10px; color: white; border-radius: 10px">Order Cancelled</span></p>
                                            <?php } else { ?>
                                                <p><span style="background-color: orange; padding: 3px 10px; color: white; border-radius: 10px">In Progress</span></p>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#view_order_items" onclick="getOrderItems('<?= $item->id ?>', '<?= $item->order_id ?>')">View Selected Prducts</button>
                                            <?php if ($item->order_status == "approved_by_fashion_manager") { ?>
                                                <button class="btn btn-primary btn-xs" onclick="placeOrder('<?= $item->id ?>', '<?= $item->order_id ?>')">Place Order to vendor</button>
                                            <?php } ?>
                                            <?php if ($item->order_status == "rejected_by_fashion_manager") { ?>
                                                <button class="btn btn-danger btn-xs"  data-toggle="modal" data-target="#rejection_remark" onclick="showRemark(`<?= $item->reject_reason ?>`, '<?= $item->order_id ?>')">View Rejection Remarks</button>
                                            <?php } ?>
                                            <?php if ($item->order_pending == 0 && $item->order_dispatched_by_vendor == 0 && $item->order_status !== "order_completed" && $item->order_status !== "order_cancelled" && $item->order_status !== "rejected_by_fashion_manager" && $item->order_status != 'suggested_by_fashion_analyst' && !empty($item->order_status)) { ?>
                                                <button class="btn btn-primary btn-xs" onclick="completeOrder('<?= $item->id ?>', '<?= $item->order_id ?>')">Complete and Close Order</button>
                                            <?php } ?>
                                            <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#view_logs" onclick="viewLogs('<?= $item->order_id ?>', '<?= $item->order_id ?>', '<?= $item->subscription_transaction_id ?>')">View Logs</button>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>S.No</th>
                                    <th>Order Id</th>
                                    <th>Order Details</th>
                                    <th>Order Status</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="view_logs" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 75%">
            <div class="modal-content ">
                <div class="modal-header">
                    <h4><span id="logs_order_id"></span> - LOGS</h4>
                    <button data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-bordered table-hover dataTable dtr-inline">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Log</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody id="log_body">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="view_order_items" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 75%">
            <div class="modal-content" style="width: 100%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 id="order_id_head"></h3>
                    <h5>Selected Products</h5>
                </div>
                <div class="modal-body" id="order_view_body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="rejection_remark" class="modal fade" role="dialog">
        <div class="modal-dialog" style="width: 50%">
            <div class="modal-content" style="width: 100%">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 id="order_id_head_rej"></h3>
                    <h5>Rejection Reason</h5>
                </div>
                <div class="modal-body" id="rejection_view">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
//    function viewLogs(id, order_id, sid) {
//
//        $("#logs_order_id").html("#" + id);
//        var html = '<table class="table table-striped table-bordered table-hover">\n\
//<thead>\n\
//\n\<tr>\n\
//\n\<th>S.No</th><th>Action Performed</th><th>Date</th>\n\
//</tr>\n\
//</thead><tbody>';
//        $.ajax({
//            url: "<?= base_url() ?>admin/order_logs/get_logs",
//            type: "post",
//            data: {order_real_id: id, subs_id: sid},
//            success: function (resp) {
//                resp = JSON.parse(resp);
//                resp = resp.data.logs;
//                if (resp) {
//                    console.log(resp);
//                    for (var i = 0; i < resp.length; i++) {
//                        html += '<tr><td>' + (i + 1) + '</td><td>' + resp[i].log + '</td><td>' + resp[i].action_date_time + '</td></tr>';
//                    }
//                    html += '</tbody></table>';
//                    $("#log_body").html(html);
//                } else {
//                    $("#log_body").html("<center>No Logs Found</center>");
//                }
//            }
//        });
//    }
    function cancelOrder(id, order_id) {
        if (confirm("Are You Sure Want to Cancel this Order ?")) {
            location.href = "<?= base_url() ?>admin/product_orders/cancel_order/" + id + '/' + order_id;
        }
    }
    function completeOrder(id, order_id) {
        if (confirm("Are You Sure Want to Complete this Order ?")) {
            location.href = "<?= base_url() ?>admin/product_orders/complete_order/" + id + '/' + order_id;
        }
    }
    function placeOrder(id, order_id) {
        if (confirm("Are You Sure Want to Place Order and Forward this Order to Vendor ?")) {
            location.href = "<?= base_url() ?>admin/product_orders/place_order/" + id + '/' + order_id;
        }
    }
    function showRemark(remark, order_id) {
        $("#order_id_head_rej").html(order_id);
        $("#rejection_view").html(remark);
    }
    function getOrderItems(id, order_id) {
        $("#order_id_head").html(order_id);
        var html = '<table class="table table-striped table-bordered table-hover">\n\
<thead>\n\
\n\<tr>\n\
\n\<th>S.No</th><th>Vendor Details</th><th>Product Id</th><th>Product Name</th><th>Product Image</th><th>Brand</th><th>Size</th><th>Quantity</th><th>Price (₹ Rs)</th><th>Product Status</th>\n\
</tr>\n\
</thead><tbody>';
        $.ajax({
            url: "<?= base_url() ?>admin/product_orders/get_order_items",
            type: "post",
            data: {id: id},
            success: function (resp) {
                if (resp) {
                    resp = JSON.parse(resp);
//                    console.log(resp);
                    for (var i = 0; i < resp.length; i++) {
                        html += "<tr><td>" + (i + 1) + "</td><td><p><b>Ref Id : </b>" + resp[i].vendor_details.ref_code + "</p>\n\
        <p><b>Shop Name : </b>" + resp[i].vendor_details.business_name + "</p>\n\
        <p><b>Owner Name : </b>" + resp[i].vendor_details.owner_name + "</p>\n\
<p><b>E-mail : </b><a href='mailto:" + resp[i].vendor_details.contact_email + "'>" + resp[i].vendor_details.contact_email + "</a>, \n\
\n\<b>Mobile : </b><a href='mailto:" + resp[i].vendor_details.contact_number + "'>" + resp[i].vendor_details.contact_number + "</a>, \n\
\n\<b>Alternate Mobile : </b><a href='mailto:" + resp[i].vendor_details.alternate_contact_number + "'>" + resp[i].vendor_details.alternate_contact_number + "</a></p>\n\
</td>\n\
        <td>" + resp[i].product_details.product_id + "\
        </td><td>" + resp[i].product_details.title + "</td><td><img src='" + resp[i].product_image + "' style='width: 150px'/></td><td>" + resp[i].product_details.brand + "\
</td><td>" + resp[i].size.title + "</td><td>" + resp[i].quantity + "</td><td>" + resp[i].subtotal + "</td><td>" + resp[i].dsp_status + "</td></tr>";
                    }
                    html += '</tbody></table>';
                    $("#order_view_body").html(html);
                } else {
                    $("#order_view_body").html("<center>No Products Found</center>");
                }
            }
        });
    }
</script>
<script>
    $(document).ready(function () {
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
<?php /* ?> {extend: 'csv'},<?php */ ?>
                {extend: 'excel', title: ' <?= plural($subject) ?>'},
                {extend: 'pdf', title: ' <?= plural($subject) ?>'
                }
                ,
                {extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ]
        });
    });
</script>