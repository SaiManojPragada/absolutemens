<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Manage Vendor Withdraw Requests</h5>
                </div>
                <div class="ibox-content">
                    <hr/>
                    <form action="" method="get">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Vendor name</label>
                                    <input type ="text" name="owner_name" class="form-control"  value="<?= $this->input->get('owner_name') ?>"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Vendor Email</label>
                                    <input type ="text" name="owner_email" class="form-control" value="<?= $this->input->get('owner_email') ?>" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Vendor Phone number</label>
                                    <input type ="text" name="owner_contact_number" class="form-control" value="<?= $this->input->get('owner_contact_number') ?>" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Withdraw Request id</label>
                                    <input type ="text" name="request_id" class="form-control" value="<?= $this->input->get('request_id') ?>"  />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Withdraw status</label>
                                    <select type ="text" name="withdraw_status" class="form-control">
                                        <option value="">All</option>
                                        <option value="submitted" <?= $this->input->get('withdraw_status') == "submitted" ? 'selected' : '' ?>>Submitted</option>
                                        <option value="released" <?= $this->input->get('withdraw_status') == "released" ? 'selected' : '' ?>>Released</option>
                                        <option value="rejected"  <?= $this->input->get('withdraw_status') == "rejected" ? 'selected' : '' ?>>Rejected</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>From date</label>
                                    <input type ="date" name="from_date" class="form-control" value="<?= $this->input->get('from_date') ?>" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>To date</label>
                                    <input type ="date" name="to_date" class="form-control" value="<?= $this->input->get('to_date') ?>"  />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary" style="margin-top: 25px">Search</button>
                            </div>
                        </div>
                    </form>
                    <hr/>
                    <div class="table-responsive">
                        <?php if ($data[0]->id != "") { ?>
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Request Id</th>
                                        <th>Vendor details</th>
                                        <th>Bank details</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Comment</th>
                                        <th>Created at</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($data as $item) {
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?= $i ?></td><td> #<?= $item->request_id ?></td>
                                            <td>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td>Business name</td>
                                                        <td><?= $item->business_name ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Name</td>
                                                        <td><?= $item->owner_name ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Email</td>
                                                        <td><?= $item->contact_email ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Mobile</td>
                                                        <td><?= $item->contact_number ?></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td>Account name</td>
                                                        <td><?= $item->account_name ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Account number</td>
                                                        <td><?= $item->account_number ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bank name</td>
                                                        <td><?= $item->bank_name ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Branch</td>
                                                        <td><?= $item->branch ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>IFSC code</td>
                                                        <td><?= $item->ifsc_code ?></td>
                                                    </tr>
                                                </table>

                                            </td>
                                            <td><i class="fa fa-inr"></i> <?= $item->amount ?></td>
                                            <td>
                                                <h6>
                                                    <?php if ($item->withdraw_status == "submitted") { ?>
                                                        <span class="badge badge-warning-light"><?= $item->withdraw_status ?></span>
                                                    <?php } ?>
                                                    <?php if ($item->withdraw_status == "released") { ?>
                                                        <span class="badge badge-primary"><?= $item->withdraw_status ?></span> <p style="font-size: 12px;margin-top: 9px;color:#000"><?= $item->approved_at ?></p><br>

                                                    <?php } ?>
                                                    <?php if ($item->withdraw_status == "rejected") { ?>
                                                        <span class="badge badge-danger"><?= $item->withdraw_status ?></span> <p style="font-size: 12px;margin-top: 9px;color:#000"><?= $item->rejected_at ?></p><br>

                                                    <?php } ?>
                                                </h6>
                                            </td>
                                            <td>
                                                <?php if ($item->withdraw_status == "released") { ?>
                                                    <p style="font-size: 11px;margin-top: 9px;color:#000"><?= $item->approve_comment ?></p>
                                                <?php } ?>
                                                <?php if ($item->withdraw_status == "rejected") { ?>
                                                    <p style="font-size: 11px;margin-top: 9px;color:#000"><?= $item->reject_reason ?></p>
                                                <?php } ?>
                                            </td>
                                            <td><?= $item->created_at ?></td>
                                            <td>
                                                <?php if ($item->withdraw_status == "submitted") { ?>
                                                    <a id="approveRequest" href="javascript:void(0)" onclick="approveRequest('<?= $item->id ?>')"  data-id="<?= $item->id ?>"   class="btn btn-success btn-xs">
                                                        Approve
                                                    </a>
                                                    <br>
                                                    <a href="<?= base_url() ?>">
                                                        <a id="rejectRequest" href="javascript:void(0)" onclick="rejectRequest('<?= $item->id ?>')"  data-id="<?= $item->id ?>"   class="btn btn-danger btn-xs">
                                                            Reject
                                                        </a>
                                                    </a>
                                                <?php } else { ?>
                                                    <p>N/A</p>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <div class="alert alert-danger">
                                No data found
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="approveModalPopup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Approve withdraw request</h4>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() ?>admin/withdraw_history/approve" method="post">
                    <div class="form-group">
                        <label>Comment/Txn id/Proof</label>
                        <textarea type="text" class="form-control" name="approve_comment" id="approve_comment" rows="5" required></textarea>
                    </div>
                    <input type="hidden" name="withdraw_request_id" id="request_id" />
                    <button type="submit" class="btn btn-primary">Approve Withdraw Request</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="rejectModalPopup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reject withdraw request</h4>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() ?>admin/withdraw_history/reject" method="post">
                    <div class="form-group">
                        <label>Reject Reason</label>
                        <textarea type="text" class="form-control" name="reject_reason" id="reject_reason" rows="5" required></textarea>
                    </div>
                    <input type="hidden" name="withdraw_request_id" id="request_id1" />
                    <button type="submit" class="btn btn-primary">Reject Withdraw Request</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    function approveRequest(id) {
        if (confirm('Are you sure you want to approve this request')) {
            $('#request_id').val(id);
            $('#approveModalPopup').modal("show");
        }
    }

    function rejectRequest(id) {
        if (confirm('Are you sure you want to approve this request')) {
            $('#request_id1').val(id);
            $('#rejectModalPopup').modal("show");
        }
    }
//    $(document).ready(function () {
//
//        $('#approveRequest').on('click', function () {
//            var request_id = $('#approveRequest').data('id');
//            if (confirm('Are you sure you want to approve this request')) {
//                $('#request_id').val(request_id);
//                $('#approveModalPopup').modal("show");
//            }
//        });
//
//        $('#rejectRequest').on('click', function () {
//            var request_id = $('#rejectRequest').data('id');
//            if (confirm('Are you sure you want to approve this request')) {
//                $('#request_id1').val(request_id);
//                $('#rejectModalPopup').modal("show");
//            }
//        });
//
//    });

</script>