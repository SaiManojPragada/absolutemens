<div>
    <div class="ibox-title">
        <strong>Products Preview</strong>
    </div>
    <div class="ibox-content">
        <div>
            <div> 


                <div class="p-0">
                    <?php if (empty($products)) { ?>
                        <div class="container-fluid">
                            <h4 class="text-center ">No Images found</h4>
                        </div>
                    <?php } ?>

                    <div class="wrapper wrapper-content">
                        <div class="row" style="padding-left: 20px; padding-right: 20px">
                            <?php
                            $i = 0;
                            foreach ($products as $item) {
                                ?>
                                <div class="col-md-3" style="margin: 10px 10px;">
                                    <div class="card box-prod" style="padding: 10px; width: 100%">
                                        <div id="carouselExampleIndicators<?= $i ?>" class="carousel slide" data-ride="carousel">

                                            <div class="carousel-inner" style="background-color: #343A40">

                                                <div id="my-pics<?= $i ?>" class="carousel slide" data-ride="carousel" style="width:300px; height: 400px;margin:auto;">



                                                    <!-- Content -->
                                                    <div class="carousel-inner" role="listbox">
                                                        <?php
                                                        $count = 0;
                                                        if (!$item->images) {
                                                            ?>
                                                            <div class="item <?= ($count === 0) ? 'active' : '' ?>">
                                                                <center  style="width: 100%; height: 400px;padding-top: 50%">
                                                                    <span style="color: white; font-size: 18px"> No Images</span>
                                                                </center>
                                                            </div>
                                                            <?php
                                                        }
                                                        foreach ($item->images as $img) {
                                                            ?>
                                                            <div class="item <?= ($count === 0) ? 'active' : '' ?>">
                                                                <img src="<?= base_url($img->image) ?>" style="width: 100%; height: 400px; object-fit: cover">
                                                            </div>



                                                            <?php
                                                            $count++;
                                                        }
                                                        ?>

                                                    </div>
                                                    <!-- Previous/Next controls -->
                                                    <a class="left carousel-control" href="#my-pics<?= $i ?>" role="button" data-slide="prev">
                                                        <span class="icon-prev" aria-hidden="true"></span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control" href="#my-pics<?= $i ?>" role="button" data-slide="next">
                                                        <span class="icon-next" aria-hidden="true"></span>
                                                        <span class="sr-only">Next</span>
                                                    </a>

                                                </div>
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleIndicators<?= $i ?>" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#carouselExampleIndicators<?= $i ?>" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                        <br>
                                        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
                                        <div class="price-div"><i class="fa fa-inr"></i> <?= $item->price ?></div>
                                        <div style="padding-left: 20px; padding-right: 20px">
                                            <h5 style="width: 100%"> <?= $item->product_id ?> <span> (<?= $item->brand ?>)</span></h5>
                                            <p> <?= $item->title ?></p>
                                        </div>

                                    </div>
                                </div>
                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .price-div{
        position: absolute;
        z-index: 1000;
        bottom: 150px;
        right: 0px;
        padding: 2.5% 8% 2.5% 5%;
        background-color: #343A40;
        margin-right: 10px;
        color: white;
    }
    .box-prod:hover{
        transition: 0.3s;
        box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
        transform: scale(1.01);
    }
</style>