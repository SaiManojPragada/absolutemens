</div>
</div>

<!-- View Feedback Modal ends Starts Here -->
<div id="viewFeedbackModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4><span id="feedback_order_id"></span> - Feedback</h4>
            </div>
            <div class="modal-body">
                <label> Rating </label><br>
                <span id="ratings" style="font-size: 35px; color: #FFD700"></span><br><br>
                <label>Feedback</label><br>
                <p id="feedback_message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="approve_cancellation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>#<span id="cancellation_order_id"></span> - Cancellation</h4>
            </div>
            <form method="post" action="<?= base_url('admin/orders/approve_cancellation') ?>" id="approve_cancellation_form">
                <div class="modal-body">
                    <label class="control-label">Cancellation Message to User</label>
                    <input type="hidden" value="" id="id_cancel" name="order_id"/>
                    <textarea class="form-control" rows="4" name="cancellation_approved_message" required></textarea>
                    <hr>
                    <p><strong style="color: tomato">Note : </strong> By Proceeding you are implying that the Refund has been Already initiated from your Side.</p>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" id="approve_cancellation_btn" class="btn btn-success">Approve Cancellation</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="content_out">
    <div id="content_pdf" style="color: black !important;">

    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.3.1/jspdf.umd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.0/html2canvas.min.js" integrity="sha512-UcDEnmFoMh0dYHu0wGsf5SKB7z7i5j3GuXHCnb3i4s44hfctoLihr896bxM0zL7jGkcHQXXrJsFIL62ehtd6yQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    function generateInvoice(id, unq_id) {
        console.log(id + ', ' + unq_id);
        var out = "";
        $.ajax({
            url: "<?= base_url('admin/ajax_pdf/generate_pdf') ?>",
            type: "post",
            data: {id: id},
            success: function (resp) {
                $("#content_pdf").html(resp);
                make_pdf(document.querySelector('#content_pdf'), unq_id);
                setTimeout(function () {
                    $("#content_pdf").html("");
                }, 1500);
            }

        });
    }
    function make_pdf(source, unq_id) {
        const {jsPDF} = window.jspdf;
        var pdf = new jsPDF('l', 'mm', [1097, 1010]);
        pdf.html(source, {
            callback: function (doc) {
                doc.save(unq_id + ".pdf");
            },
            x: 10,
            y: 10
        });
    }
</script>

<script>
    $("#approve_cancellation_btn").click(function (e) {
        if (confirm("Are you sure want to Approve Cancellation to this Order ?")) {
            if ($("#approve_cancellation_form").validate()) {
                $("#approve_cancellation_form").submit();
            }
        }
    });
    function approveCancellation(id, unq_id) {
        $("#cancellation_order_id").html(unq_id);
        $("#id_cancel").val(id);
    }
</script>
<div id="logsModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 75%">
        <div class="modal-content ">
            <div class="modal-header">
                <h4><span id="logs_order_id"></span> - LOGS</h4>
                <button data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered table-hover dataTable dtr-inline">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Log</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody id="log_body">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    function viewLogs(unq_id) {
        $('#logs_order_id').html(unq_id);
        $.ajax({
            url: '<?= base_url('admin/logs') ?>',
            type: 'post',
            data: {id: unq_id},
            success: function (resp) {
                $('#log_body').html(resp);
            }
        });
    }
</script>
<!-- Assigning Fashion Designer And Manager Modal ends Starts Here (EXCHANGE MODEL) -->

<div id="reassignExchangeOrder" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form action="<?= base_url('admin/orders/assign_exchange') ?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign Fashion Manager and Fashion Analyst <b>( Exchange Order )</b></h4>
                </div>
                <div class="modal-body">
                    <h4><b>Order Id : </b><span id="exc_order_dsp_id"></span></h4><br>
                    <input id="exc_order_real_id" value="" type="hidden" name="order_real_id">
                    <input id="exc_order_id" value="" type="hidden" name="order_id">
                    <div class="form-group">
                        <label class="control-label">Select Admin Address for Product Delivery</label>
                        <select class="form-control" name="admin_address_id" id="exc_admin_address_id" required>
                            <option value="">Select Address</option>
                            <?php foreach ($admin_addresses as $address) { ?>
                                <option value="<?= $address->id ?>"><?= $address->address_title . ', ' . $address->address ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Budget Amount</label>
                        <input class="form-control" name="order_budget"  id="exc_order_budget" type="number" min="0" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Select Fashion Manager</label>
                        <select class="form-control" name="fashion_manger_id" id="exc_fashion_manger_id" required>
                            <option value="">Select Fashion Manager</option>
                            <?php foreach ($fashion_managers as $manager) { ?>
                                <option value="<?= $manager->id ?>"><?= $manager->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Select Fashion Analyst</label>
                        <select class="form-control" name="fashion_analyst_id" id="exc_fashion_analyst_id" required>
                            <option value="">Select Fashion Analyst</option>
                            <?php foreach ($fashion_analysts as $analyst) { ?>
                                <option value="<?= $analyst->id ?>"><?= $analyst->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Estimated Delivery date</label>
                        <input class="form-control" name="estimated_delivery_date"  id="exc_estimated_delivery_date" type="date" min="0" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Assigning Fashion Designer And Manager Modal ends Starts Here -->

<div id="assignFashionManagerModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <form action="<?= base_url('admin/orders/assign') ?>" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Assign Fashion Manager and Fashion Analyst</h4>
                </div>
                <div class="modal-body">
                    <h4><b>Order Id : </b><span id="order_dsp_id"></span></h4><br>
                    <input id="order_real_id" value="" type="hidden" name="order_real_id">
                    <input id="order_id" value="" type="hidden" name="order_id">
                    <div class="form-group">
                        <label class="control-label">Select Admin Address for Product Delivery</label>
                        <select class="form-control" name="admin_address_id" id="admin_address_id" required>
                            <option value="">Select Address</option>
                            <?php foreach ($admin_addresses as $address) { ?>
                                <option value="<?= $address->id ?>"><?= $address->address_title . ', ' . $address->address ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Budget Amount</label>
                        <input class="form-control" name="order_budget"  id="order_budget" type="number" min="0" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Select Fashion Manager</label>
                        <select class="form-control" name="fashion_manger_id" id="fashion_manger_id" required>
                            <option value="">Select Fashion Manager</option>
                            <?php foreach ($fashion_managers as $manager) { ?>
                                <option value="<?= $manager->id ?>"><?= $manager->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Select Fashion Analyst</label>
                        <select class="form-control" name="fashion_analyst_id" id="fashion_analyst_id" required>
                            <option value="">Select Fashion Analyst</option>
                            <?php foreach ($fashion_analysts as $analyst) { ?>
                                <option value="<?= $analyst->id ?>"><?= $analyst->name ?></option>
                            <?php } ?>
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>

    </div>
</div>

<script>

    function showOrderFeedback(rating, message, order_id) {
        var act_rating = parseInt(rating);
        var rate_html = "";
        for (var i = 1; i < 6; i++) {
            if (i <= act_rating) {
                rate_html += '<i class="fa fa-star"></i>';
            } else {
                rate_html += '<i class="fa fa-star-o"></i>';
            }
        }
        $("#feedback_order_id").html('#' + order_id);
        $("#ratings").html(rate_html);
        $("#feedback_message").html(message);
    }

    function assignOrderId(order_id, order_real_id, fashion_manger_id, fashion_analyst_id, budget, address_id) {
        document.getElementById("order_id").value = order_id;
        document.getElementById("order_dsp_id").innerHTML = order_real_id;
        document.getElementById("order_real_id").value = order_real_id;
        document.getElementById("fashion_manger_id").value = fashion_manger_id;
        document.getElementById("fashion_analyst_id").value = fashion_analyst_id;
        document.getElementById("admin_address_id").value = address_id;
        document.getElementById("order_budget").value = parseInt(budget);
    }

    function exchangeRequest(order_id, order_real_id, fashion_manger_id, fashion_analyst_id, budget, address_id) {
        document.getElementById("exc_order_id").value = order_id;
        document.getElementById("exc_order_dsp_id").innerHTML = order_real_id;
        document.getElementById("exc_order_real_id").value = order_real_id;
        document.getElementById("exc_fashion_manger_id").value = fashion_manger_id;
        document.getElementById("exc_fashion_analyst_id").value = fashion_analyst_id;
        document.getElementById("exc_admin_address_id").value = parseInt(address_id);
        document.getElementById("exc_order_budget").value = parseInt(budget);
    }

    function orderMarkAsShipped(id) {
        if (confirm("Are You Sure Want to Mark this Order as Shipped ?")) {
            location.href = '<?= base_url() . 'admin/orders/mark_as_shipped/' ?>' + id;
        } else {
            return false;
        }
    }

    function orderMarkAsDelivered(id) {
        if (confirm("Are You Sure Want to Mark this Order as Delivered ?")) {
            location.href = '<?= base_url() . 'admin/orders/mark_as_delivered/' ?>' + id;
        } else {
            return false;
        }
    }

</script>

<!-- Assigning Fashion Designer And Manager Modal ends Here -->

</div>
<!-- Mainly scripts -->

<script src="<?= base_url() ?>admin_assets/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>admin_assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?= base_url() ?>admin_assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Flot -->
<script src="<?= base_url() ?>admin_assets/js/plugins/flot/jquery.flot.js"></script>
<script src="<?= base_url() ?>admin_assets/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?= base_url() ?>admin_assets/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?= base_url() ?>admin_assets/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?= base_url() ?>admin_assets/js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="<?= base_url() ?>admin_assets/js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?= base_url() ?>admin_assets/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?= base_url() ?>admin_assets/js/inspinia.js"></script>
<script src="<?= base_url() ?>admin_assets/js/plugins/pace/pace.min.js"></script>


<!--For Dashboard Page Only because charts is there-->
<!-- GITTER -->
<script src="<?= base_url() ?>admin_assets/js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="<?= base_url() ?>admin_assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="<?= base_url() ?>admin_assets/js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="<?= base_url() ?>admin_assets/js/plugins/chartJs/Chart.min.js"></script>
<!--For Dashboard Page only because charts is there end-->


<script src="<?= base_url() ?>admin_assets/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- jQuery UI -->
<script src="<?= base_url() ?>admin_assets/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- Toastr -->
<script src="<?= base_url() ?>admin_assets/js/plugins/toastr/toastr.min.js"></script>

<!--Loading Overlay-->
<script src="<?= base_url() ?>admin_assets/js/loadingoverlay_progress.min.js"></script>
<script src="<?= base_url() ?>admin_assets/js/loadingoverlay.min.js"></script>

<!--Choosen Select-->
<script src="<?= base_url() ?>admin_assets/js/chosen.jquery.min.js"></script>
<script>$(function () {
        $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
    });</script>

<script src="<?= base_url() ?>admin_assets/js/jquery.fancybox.js"></script>
<script src="<?= base_url() ?>admin_assets/js/jquery.fancybox-media.js"></script>
<script src="<?= base_url() ?>admin_assets/js/jquery.datetimepicker.js"></script>
<script src="<?= base_url() ?>admin_assets/js/jquery.validate.min.js"></script>

<script src="<?= base_url() ?>admin_assets/js/plugins/dataTables/datatables.min.js"></script>
<script src="<?= base_url() ?>admin_assets/js/additional-methods.js"></script>
<script>
//    document.body.style.zoom = "80%";
</script>
<script>
    $(document).ready(function () {
        $(".fancybox").fancybox({
            openEffect: 'elastic',
            closeEffect: 'elastic',
            helpers: {
                overlay: {
                    speedIn: 0,
                    speedOut: 0,
                    opacity: 0.5
                }
            }
        });
        $('.fancybox-media').fancybox({
            openEffect: 'elastic',
            closeEffect: 'elastic',
            helpers: {
                media: {}
            }
        });
        $('.datepicker').datetimepicker({
            timepicker: false,
            format: 'm-d-Y',
            scrollInput: false
        });
        $(document).on('mousewheel', '.datepicker', function () {
            return false;
        });
    });
</script>

<?php
if (isset($editor_type)) {
    if ($editor_type == "basic") {
        ?>
        <script src="<?= base_url() ?>admin_assets/ckeditor_basic/ckeditor.js"></script>
    <?php } else if ($editor_type == "standard") { ?>
        <script src="<?= base_url() ?>admin_assets/ckeditor_standard/ckeditor.js"></script>
    <?php } else if ($editor_type == "full") { ?>
        <script src="<?= base_url() ?>admin_assets/ckeditor_full/ckeditor.js"></script>
        <?php
    }
} else {
    ?><script src="<?= base_url() ?>admin_assets/ckeditor_basic/ckeditor.js"></script>
<?php } ?>

<!-- Tags Input -->
<script src="<?= base_url() ?>admin_assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script>
    $(document).ready(function () {
        $('.tagsinput').tagsinput({
            tagClass: 'label label-primary'
        });
    });
</script>

<script>
    $(document).on("click", ".autoCopy", function () {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(this).attr("data-to-copy")).select();
        document.execCommand("copy");
        toastr.success('Path copied to clipboard');
        $temp.remove();
    });
</script>
<div class="container-fluid col-12" style="background-color: #2f4050; padding: 10px 30px">
    <span style="color: #ffffff; float: right; letter-spacing: 0.8px"><?php
        $this->benchmark->mark('code_end');
        echo 'This page took <b>' . $this->benchmark->elapsed_time('code_start', 'code_end') . '</b> Seconds and <b>' .
        $this->benchmark->memory_usage('code_start', 'code_end') . '</b> of Memory';
        ?></span>
</div>
</body>
</html>

<?php if ($this->session->flashdata('sucess') != "") { ?>

    <script>
        swal("Good job!", "<?= $this->session->flashdata('sucess') ?>", "success");

    </script>

<?php } ?>

<?php if ($this->session->flashdata('error') != "") { ?>

    <script>
        swal("Attention !", "<?= $this->session->flashdata('error') ?>", "error");

    </script>

<?php } ?>
 