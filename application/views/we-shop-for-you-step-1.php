<div class="pkgsteps">
	<?php include 'includes/header.php' ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12 col-md-12">
				<div class="whitebox">
					<form class="new-added-form" action="we-shop-for-you-step-2.php">
						<h4></h4>
						<section>
							<h6>We Shop For You</h6>
							<div class="row">
								<div class="col-lg-6 col-md-6 clearfix">
									<div class="input-group mb-2">
									  <div class="input-group-prepend">
									    <span class="input-group-text" id="basic-addon1"><i class="fal fa-rupee-sign"></i></span>
									  </div>
									  <input type="text" class="form-control" placeholder="What is your Budget">
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<p class="text-danger"><small>Note: We shop for you orders set the minimum budget to greater than <i class="fal fa-rupee-sign"></i> 3500/-</small></p>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-lg-6 col-md-6">
									<div class="form-group mb-3">
										<label for="">Occasion</label>
										<textarea rows="3" class="form-control" placeholder="Describe your occassion in max workds 150"></textarea>
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="form-group mb-3">
										<label for="">Description</label>
									<textarea rows="3" class="form-control" placeholder="Describe your occassion in max words from 40 to 500"></textarea>
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="form-group mb-3">
										<label for="">Likes</label>
									<textarea rows="3" class="form-control" placeholder="Describe your likes in max workds 150"></textarea>
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="form-group mb-3">
										<label for="">Dislikes</label>
									    <textarea rows="3" class="form-control" placeholder="Describe your dislikes in max words from 40 to 500"></textarea>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 mt-5">
									<div class="d-flex justify-content-end">
										<button type="submit" class="btn-next">NEXT</button>
									</div>
								</div>
							</div>
						</section>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="best-sell-active owl-carousel owl-theme">
					<div class="item"><a href="#"><img src="images/bannerhow-1.jpg" alt=""></a></div>
					<div class="item"><a href="#"><img src="images/bannerhow-2.jpg" alt=""></a></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php' ?>