<div class="subpagebg">
</div>
<div class="greybg">
    <br>    <br>
    <br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12">
                <div class="whitebox">
                    <form class="new-added-form" method="POST" action="<?= base_url('package_steps/submit_form/' . $current_package . '/step-three') ?>">
                        <section>
                            <input id="unq_id" type="hidden" name="unq_id" value="<?= $random_unq_key ?>">
                            <p><strong>What are Your Sizes ?</strong></p>
                            <div class="row">
                                <?php foreach ($categories as $c) { ?>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-group mb-3">
                                            <label><?= $c->name ?></label>
                                            <select class="form-control" name="<?= $c->id ?>" required>
                                                <option value="">--select <?= $c->name ?> size--</option>
                                                <?php foreach ($c->sizes as $item) { ?>
                                                    <option value="<?= $item->id ?>" <?php
                                                    if ($item->is_selected == 'yes') {
                                                        echo "selected";
                                                    }
                                                    ?>><?= $item->title ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div> 
                                    </div>
                                <?php } ?>
                                <div class="col-lg-12 col-md-12">
                                    <a href="#viewstyleModal" data-toggle="modal"><i class="fal fa-ruler-horizontal"></i> View Style Guide</a>
                                </div>
                                <div class="col-lg-12 col-md-12 mt-5">
                                    <div class="d-flex justify-content-between">
                                        <button type="button" class="btn-previous" onclick="location.href = '<?= base_url() ?>package-steps/step-one/<?= $current_package ?>/<?= $random_unq_key ?>'">PREVIOUS</button>
                                        <button type="submit" class="btn-next">NEXT</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
            <?php if ($current_package !== "we-shop-for-you") { ?>
                <div class="col-lg-4 col-md-8">
                    <div class="d-flex justify-content-between mb-2 align-items-center">
                        Selected Package
                        <a href="<?= base_url('packages') ?>" class="btn btn-sm btn-primary"><i class="fal fa-sync"></i> Change Package</a>
                    </div>
                    <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                        <div class="banner-img">
                            <a href="#"><img src="<?= base_url() ?>uploads/<?= $package_details->image ?>" alt="banner" /></a>
                        </div>
                        <div class="banner-content-3">
                            <?= $package_details->title ?>
                            <h5><?= $package_details->no_of_items ?> items / <?= $package_details->duration_in_words ?></h5>
                            <p>Subscription Start from <?= (int) $package_details->price ?>/month</p>
                            <?= $package_details->subscription_features ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="best-sell-active owl-carousel owl-theme">
                    <?php foreach ($package_banners as $banner) { ?>
                        <div class="item"><a href="#"><img src="<?= base_url() ?>uploads/<?= $banner->image ?>" alt=""></a></div>
                            <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--view style guide--->