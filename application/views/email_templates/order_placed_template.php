<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    <center>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
                <tr>
                    <td bgcolor="#ffffff" style="font-size:0px"></td>
                    <td bgcolor="#ffffff" width="660" align="center">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" dir="ltr">
                            <tbody>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <th style="padding: 50px">
                                                        <img src="<?= base_url() ?>uploads/ACTLOGO.png" width="200">
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" dir="ltr">
                            <tbody>
                                <tr>
                                    <td align="left" valign="top" style="min-width:10px">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>

                                                </tr>
                                                <tr>

                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="600" valign="top" align="center"><br>
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding:0px 20px 30px 20px;word-break:break-word">
                                            <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <p style="font-size:32px;line-height:40px;color:#2c2e2f;margin:0" dir="ltr"><span>You`ve made a payment of ₹ <?= $order_details->amount_paid ?></span></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:0px 20px 20px 20px">
                                                        <p style="font-size:16px;line-height:24px;color:#2c2e2f;margin:0;word-break:break-word" dir="ltr"><span>It may take a few moments for this transaction to appear in your account.</span></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:0px 10px 20px 10px">
                                                        <table id="" cellspacing="0" cellpadding="0" border="0" width="100%" dir="ltr" style="font-size:16px">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding:0px 10px;text-align:left;border-top:0px;width:50%;vertical-align:top">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="padding:0px 0px 20px 0px">
                                                                                        <p style="font-size:16px;line-height:25px;color:#2c2e2f;margin:0;word-break:break-word" dir="ltr"><span><strong>Transaction ID</strong></span><br>
                                                                                            #<?= $order_details->transaction_id ?>
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                    <td style="padding:0px 10px;text-align:left;border-top:0px;width:50%;vertical-align:top">
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="padding:0px 0px 20px 0px">
                                                                                        <p style="font-size:16px;line-height:25px;color:#2c2e2f;margin:0;word-break:break-word" dir="ltr"><span><strong>Transaction date</strong></span><br><span><?= date('d M Y', $order_details->updated_at) ?></span></p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:0px 10px 20px 10px">
                                                        <table id="" cellspacing="0" cellpadding="0" border="0" width="100%" dir="ltr" style="font-size:16px">
                                                            <tbody>
                                                                <tr>
                                                                    <th style="border-bottom:1px solid #b7bcbf;padding:15px 10px;text-align:left;border-top:1px solid #b7bcbf;background-color:#f5f7fa"><span>Description</span></th>
                                                                    <th style="border-bottom:1px solid #b7bcbf;padding:15px 10px;text-align:right;border-top:1px solid #b7bcbf;background-color:#f5f7fa;width:135px"><span>Amount</span></th>
                                                                </tr>
                                                                <tr>
                                                                    <td style="border-bottom:1px solid #cbd2d6;padding:10px 10px;text-align:left;border-top:0px;line-height:22px"><span style="font-size:16px;color:#2c2e2f"><span><?= strip_tags($package_details->title) ?></span></span></td>
                                                                    <td style="border-bottom:1px solid #cbd2d6;padding:10px 10px;text-align:right;border-top:0px"><span>₹ <?= strip_tags($package_details->price) ?></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:0px 10px 20px 10px">
                                                        <table id="" cellspacing="0" cellpadding="0" border="0" width="100%" dir="ltr" style="font-size:16px">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;vertical-align:top" colspan="1"><span><strong>Subtotal</strong></span></td>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;width:135px"><span>₹ <?= $order_details->total_price ?></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;vertical-align:top" colspan="1"><span><strong>Discount</strong></span></td>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;width:135px"><span>₹ <?= $order_details->discount ?></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;vertical-align:top" colspan="1"><span><strong>Coupon Discount</strong></span></td>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;width:135px"><span>₹ <?= $order_details->coupon_discount ?></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;vertical-align:top" colspan="1"><span><strong>Total</strong></span></td>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;width:135px"><span>₹ <?= $order_details->grand_total ?></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;vertical-align:top;padding-top:15px;padding-bottom:10px" colspan="1"><span><strong>Payment</strong></span></td>
                                                                    <td style="text-align:right;padding:5px 10px 0px 0px;width:135px;padding-top:15px;padding-bottom:10px"><span>₹ <?= $order_details->amount_paid ?></span></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:0px 20px 20px 20px">
                                                        <p>Thankyou for Shopping with us.</p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:0px 20px 20px 20px">
                                                        <p style="font-size:16px;line-height:24px;color:#2c2e2f;margin:0;word-break:break-word" dir="ltr"><span><strong>Issues with this transaction?</strong></span>
                                                        </p><table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="padding:5px 0px">
                                                                        <?php $site_property = $this->site_model->get_site_properties(); ?>
                                                                        <p style="font-size:16px;line-height:24px;color:#2c2e2f;margin:0;word-break:break-word" dir="ltr"><span>Contact Us on <a href="mailto:<?= $site_property->contact_email ?>"><?= $site_property->contact_email ?></a> or call us On <a href="tel:<?= $site_property->contact_number ?>"><?= $site_property->contact_number ?></a>.</span></p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <p></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td valign="top" align="left" style="min-width:10px">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td align="center" width="600">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" dir="ltr">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>

                                                                    <td width="120" align="center" valign="top">
                                                                        <img src="<?= base_url() ?>uploads/ACTLOGO.png" width="120" style="display:block" border="0" alt="Absolutemens" class="CToWUd"></td>

                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td bgcolor="#ffffff" style="font-size:0px"></td>
                </tr>
            </tbody>

        </table>
    </center>
</body>
</html>