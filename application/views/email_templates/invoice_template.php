<?php $site_property = $this->site_model->get_site_properties(); ?>
<div style="width: 45%">
    <table width="100%" cellspacing="0" cellpadding="0" style=" font-family: sans-serif; font-size: 13px; color: #676767;">
        <tr>
            <td align="center">
                <img src="<?= base_url('uploads/' . $site_property->favicon) ?>" style="width:70px; height:auto">
        <center><h2><?= $site_property->site_name ?></h2></center>
        </td>
        </tr>
    </table>

    <table  style="width: 100%; margin-bottom: 10px; margin-top: 10px;" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2" style="padding:10px 20px; background: #eee; vertical-align: top;border: 1px solid #ccc; font-size: 16px; font-weight: bold; text-align: center;">Order ID : #<?= $order_data->unq_id ?></td>
        </tr>
        <tr>
            <td width="50%" style="padding:20px;vertical-align: middle;border: 1px solid #ccc; ">
                <h4 style="font-weight: bold; font-size: 16px; margin: 0px; margin-bottom: 10px; letter-spacing: -0.0400em;">Customer Details</h4>
                <p> <h5>Name : <?= $customer_details->name ?></h5></p>
                <p> <h5>Mobile : <?= $order_data->phone_number ?> </h5></p>
                <p> <h5>E-Mail : <?= $order_data->address_email ?></h5> </p>
                <p> <h5>Address : <?= $order_data->address ?>, <?= $order_data->city ?>, <?= $order_data->district ?>, <?= $order_data->state ?></h5></p>
                <p> <h5>Landmark : <?= $customer_details->landmark ?></h5></p>
                <p> <h5>Street : <?= $customer_details->street_name ?></h5></p>
                <p> <h5>Pincode : <?= $customer_details->pincode ?></h5></p>
            </td>
            <td width="50%" style="padding:20px; vertical-align: top;border: 1px solid #ccc;">
                <h4 style="font-weight: bold; font-size: 16px; margin: 0px; margin-bottom: 10px; letter-spacing: -0.0400em;">Package Details</h4>

                <?php if (!$order_data->is_we_shop_for_you) { ?>
                    <p> <h5>Package Title : <?= strip_tags($package_details->title) ?></h5></p>
                    <p> <h5>No of Items : <?= $package_details->no_of_items ?></h5></p>
                    <p> <h5>Price : <?= $order_data->package_amount ?></h5> </p>
                    <p> <h5>Duration : <?= $order_data->package_duration ?> days </h5></p>
                <?php } else { ?>
                    <p> <h5>Package Title : We Shop For You Order</h5></p>
                    <p> <h5>No of Items : <?= $order_data->no_of_items ?></h5></p>
                <?php } ?>
            </td>
        </tr>
    </table>

    <h2>
        <center>Payment Details</center>
    </h2>
    <table  style="width: 100%; margin-bottom: 10px;" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td style="width: 50%; border: 1px solid #ccc; padding: 20px; vertical-align: top;">
                    <p><h5>Payment Id : <?= $order_data->razorpay_payment_id ?></h5></p>
                    <p><h5>Payment Order Id : <?= $order_data->razorpay_order_id ?></h5></p>
                    <p><h5>Ordered On : <?= date('d M Y, h:i A', $order_data->updated_at) ?></h5></p>
                    <p><h5>Expires On : <?= date('d M Y, h:i A', $order_data->expiry_date) ?></h5></p>
                    <?php if (!empty($order_data->dispatched_date)) { ?><p><h5>Dispatched On : <?= date('d M Y, h:i A', $order_data->dispatched_date) ?></h5></p><?php } ?>
                    <?php if (!empty($order_data->delivered_date)) { ?><p><h5>Shipped On : <?= date('d M Y, h:i A', $order_data->shipped_date) ?></h5></p><?php } ?>
                    <?php if (!empty($order_data->delivered_date)) { ?><p><h5>Delivered On : <?= date('d M Y, h:i A', $order_data->delivered_date) ?></h5></p><?php } ?>
                    <?php if (!empty($order_data->cancelled_date)) { ?><p><h5>Cancelled On : <?= date('d M Y, h:i A', $order_data->cancelled_date) ?></h5> </p><?php } ?>
                    <?php if (!empty($order_data->cancellation_reason)) { ?><p><h5>Cancellation Reason : <?= $order_data->cancellation_reason ?></h5></p><?php } ?>
                </td>
                <td style="width: 25%; border: 1px solid #ccc; padding: 20px; vertical-align: top;">
                    <p> <h5>Total Price :</h5></p>
                    <p> <h5>Discount :</h5></p>
                    <p> <h5>Coupon Discount :</h5></p>
                    <p> <h5>Delivery Charges :</h5></p>
                    <p> <h5>Grand Total :</h5></p>
                    <p> <h5>Amount Paid :</h5></p>
                </td>
                <td style="width: 25%; border: 1px solid #ccc; padding: 20px; vertical-align: top;">
                    <p> <h5><?= $order_data->total_price ?></h5></p>
                    <p> <h5><?= ($order_data->discount) ? $order_data->discount : 0 ?></h5></p>
                    <p> <h5><?= ($order_data->coupon_discount) ? $order_data->coupon_discount : 0 ?></h5></p>
                    <p> <h5><?= ($order_data->delivery_charges) ? $order_data->delivery_charges : 0 ?></h5></p>
                    <p> <h5><?= ($order_data->grand_total) ? $order_data->grand_total : 0 ?></h5></p>
                    <p style="font-size: 30px"> <h4><?= ($order_data->amount_paid) ? $order_data->amount_paid : 0 ?></h4></p>
                </td>
            </tr>
        </tbody>
    </table>
</div>