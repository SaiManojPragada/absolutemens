<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    <center style='width: 100%'>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody>
                <tr>
                    <td bgcolor="#ffffff" style="font-size:0px"></td>
                    <td bgcolor="#ffffff" width="660" align="center">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" dir="ltr">
                            <tbody>
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <th style="padding: 50px">
                                                        <img src="<?= base_url() ?>uploads/ACTLOGO.png" width="200">
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" dir="ltr">
                            <tbody>
                                <tr>
                                    <td align="left" valign="top" style="min-width:10px">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>

                                                </tr>
                                                <tr>
                                                    <td><?= $message ?></td>

                                                </tr>
                                                <tr>

                                                </tr>
                                                <tr>

                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>
                                                            Thanks & Regards
                                                        </b>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="padding:0px 20px 20px 20px">
                                        </p><table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="padding:5px 0px">

                                                        <?php $site_property = $this->site_model->get_site_properties(); ?>
                                                        <p style="font-size:16px;line-height:24px;color:#2c2e2f;margin:0;word-break:break-word" dir="ltr"><span>Contact Us on <a href="mailto:<?= $site_property->contact_email ?>"><?= $site_property->contact_email ?></a> or call us On <a href="tel:<?= $site_property->contact_number ?>"><?= $site_property->contact_number ?></a>.</span></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td valign="top" align="left" style="min-width:10px">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td align="center" width="600">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" dir="ltr">
                            <tbody>
                                <tr>
                                    <td>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>

                                                    <td width="120" align="center" valign="top">
                                                        <img src="<?= base_url() ?>uploads/ACTLOGO.png" width="120" style="display:block" border="0" alt="Absolutemens" class="CToWUd"></td>

                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </center>
</body>
</html>