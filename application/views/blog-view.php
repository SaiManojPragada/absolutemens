<div class="cms-subpage">
    <h4><?= $blog->title ?></h4>
</div>
<!-- shop-main-area-start -->
<div class="shop-main-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-12 order-lg-0 order-md-1">
                <!-- blog-details-area-start -->
                <div class="blog-details-area mb-40-2 pt-5 pb-5">
                    <div class="blog-details-img">
                        <a><img src="<?= base_url() ?>uploads/<?= $blog->image ?>" alt="banner" /></a>
                    </div>
                    <div class="blog-info">
                        <?= $blog->description ?>
                    </div>								
                </div>
                <!-- blog-details-area-end -->
            </div>
            <div class="col-lg-4 col-md-12 col-12 order-lg-1 order-md-0">
                <!-- blog-right-area-start -->
                <div class="blog-right-area pt-5">
                    <!-- blog-right-start -->
                    <div class="blog-right mb-50 mb-3">
                        <h3>Recent Posts</h3>
                        <div class="sidebar-post">
                            <!-- single-post-start -->
                            <?php foreach ($similar_blogs as $item) { ?>
                                <div class="single-post mb-20">
                                    <div class="post-img">
                                        <a href="<?= base_url() ?>blog-view/<?= $item->slug ?>"><img src="<?= base_url() ?>uploads/<?= $item->image ?>" alt="post" /></a>
                                    </div>
                                    <div class="post-text">
                                        <h4><a href="<?= base_url() ?>blog-view/<?= $item->slug ?>"><?= $item->title ?></a></h4>
                                        <span><?= date("F d, Y", $item->created_at) ?></span>
                                    </div>
                                </div>
                            <?php } ?>
                            <!--                            <div class="single-post mb-20">
                                                            <div class="post-img">
                                                                <a href="blog-view.php"><img src="images/blog-1.jpg" alt="post" /></a>
                                                            </div>
                                                            <div class="post-text">
                                                                <h4><a href="blog-view.php">Aypi non habent claritatem  insitam.</a></h4>
                                                                <span>August 10, 2020</span>
                                                            </div>
                                                        </div>
                                                         single-post-end 
                                                         single-post-start 
                                                        <div class="single-post mb-20">
                                                            <div class="post-img">
                                                                <a href="blog-view.php"><img src="images/blog-1.jpg" alt="post" /></a>
                                                            </div>
                                                            <div class="post-text">
                                                                <h4><a href="blog-view.php">Bypi non habent claritatem  insitam.</a></h4>
                                                                <span>August 10, 2020</span>
                                                            </div>
                                                        </div>
                                                         single-post-end 
                                                         single-post-start 
                                                        <div class="single-post mb-20">
                                                            <div class="post-img">
                                                                <a href="blog-view.php"><img src="images/blog-1.jpg" alt="post" /></a>
                                                            </div>
                                                            <div class="post-text">
                                                                <h4><a href="blog-view.php">Cypi non habent claritatem  insitam.</a></h4>
                                                                <span>August 10, 2020</span>
                                                            </div>
                                                        </div>
                                                         single-post-end 
                                                         single-post-start 
                                                        <div class="single-post">
                                                            <div class="post-img">
                                                                <a href="blog-view.php"><img src="images/blog-1.jpg" alt="post" /></a>
                                                            </div>
                                                            <div class="post-text">
                                                                <h4><a href="blog-view.php">Aypi non habent claritatem  insitam.</a></h4>
                                                                <span>August 10, 2020</span>
                                                            </div>
                                                        </div>-->
                            <!-- single-post-end -->
                        </div>
                    </div>
                </div>
                <!-- blog-right-area-end -->
            </div>
        </div>
    </div>
</div>
<!-- shop-main-area-end -->