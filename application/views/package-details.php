<div class="cms-subpage">
    <h4><?= strip_tags($pack_details->title) ?></h4>
</div>
<div class="howitbox ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <?= $pack_details->description ?>
            </div>
        </div>
    </div>
</div>
<style>
    .howitbox ul{
        margin-left: 30px;
        list-style: circle;
    }
</style>