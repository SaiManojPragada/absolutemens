<div class="cms-subpage">
    <h4>CONTACT US</h4>
</div>
<div class="container-fluid">
    <?php if (!empty($this->session->userdata("contact_success"))) { ?>
        <div id="succes-div">
            <p><?= $this->session->userdata("contact_success"); ?><button class="pull-right btn" onclick="close_message('succes-div')"><b>&times;</b></button></p>
        </div>
    <?php }$this->session->unset_userdata("contact_success"); ?>


    <?php if (!empty($this->session->userdata("contact_error"))) { ?>
        <div id="error-div">
            <p><?= $this->session->userdata("contact_error"); ?><button class="pull-right btn" onclick="close_message('error-div')"><b>&times;</b></button></p>
        </div>
    </div>
<?php }$this->session->unset_userdata("contact_error"); ?>

</div>
<div class="container pt-5">
    <div class="row cont-details">
        <div class="col-lg-4 col-md-12">
            <div class="card shadow-sm">
                <div class="card-body">
                    <span><i class="fal fa-map-marker-alt"></i></span>
                    <?= $site_property->address ?><br>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card shadow-sm">
                <div class="card-body">
                    <span><i class="fal fa-phone"></i></span>
                    <?= $site_property->contact_number ?>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="card shadow-sm">
                <div class="card-body">
                    <span><i class="fal fa-envelope-open"></i></span>
                    <?= $site_property->contact_email ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <img src="<?= base_url() ?>assets/images/contactbg.jpg" alt="" class="img-fluid">
        </div>
        <div class="col-lg-6 col-md-6 align-self-center">
            <div class="contact-form">
                <h3><i class="fa fa-envelope-o"></i> Enquiry</h3>
                <form action="" class="contact-form" id="contact-form" method="post" onsubmit="validateCaptcha()" autocomplete="off">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group mb-3">
                                <input type="text" class="form-control" placeholder="Name*" name="name" required minlength="3" data-parsley-pattern="^[a-zA-Z\s]*$" data-parsley-pattern-message="Please Enter Valid Name" data-parsley-required-message="Please Enter Valid Name">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group mb-3">
                                <input type="email" class="form-control" placeholder="Email*" name="email" required data-parsley-type-message="Please Enter Valid Email" data-parsley-required-message="Please Enter Valid Email" data-parsley-trigger="change" >
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="form-group mb-3">
                                <input type="number" class="form-control" placeholder="Phone*" name="phone" required min='1' data-parsley-minlength="10"  data-parsley-maxlength="10" data-parsley-required-message="Please Enter Phone Number" data-parsley-minlength-message="Enter Valid Phone Number *" data-parsley-maxlength-message="Enter Valid Phone Number">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group mb-3">
                                <select class="form-control" name="request_type" required data-parsley-required-message="Please Select a Request Type"> 
                                    <option value="">Select Nature of Request*</option>
                                    <option value="Partner with us">Partner with us</option>
                                    <option value="Work with us">Work with us</option>
                                    <option value="Know delivery status">Know delivery status</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group mb-3">
                                <textarea class="form-control" rows="5" minlength="30" placeholder="Message*" name="message" required></textarea>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group mb-3">
                                <label for="">Enter Captcha</label> <br>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div id="captcha">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <a href="javascript:void(0)" class="unstyled-button" onclick="createCaptcha()"><i class="fa fa-sync"></i></a>
                                    </div>
                                </div>

                                <input type="text" class="form-control" name="cpatchaTextBox" placeholder="Enter Captcha code" id="cpatchaTextBox" required="" data-parsley-required-message="Please Enter Captcha"/>
                                <span id="captchaErr" style="color: red"></span>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group mb-3">
                                <button type="submit" class="btn-submit" name="submit" value="submit"><i class="fal fa-paper-plane"></i> SUBMIT</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    #succes-div, #error-div{
        margin-top: 5px;
        padding: 15px 30px 7px 30px;
        border-radius: 12px;
    }
    #succes-div p, #error-div p{
        width: 100%;
    }
    #succes-div{
        background-color: #39824d65;
        color: #39824d;
        border: 1px solid #39824d;
    }
    #error-div{
        background-color: #82393965;
        color: #823939;
        border: 1px solid #823939;
    }
    #succes-div button{
        float: right;
        color : #39824d;
    }
    #error-div button{
        float: right;
        color: #823939;
    }


</style>
<style>
    .parsley-errors-list{
        list-style:none;
    }
    .parsley-required , .parsley-errors-list li{
        color:red;
        list-style:none;
    }
</style>
<script>

    function close_message(div) {
        document.getElementById(div).style.display = "none";
    }


    var code;
    createCaptcha();
    $(document).ready(function () {
        $(window).keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });
    function createCaptcha() {
        //clear the contents of captcha div first 
        document.getElementById('captcha').innerHTML = "";
        var charsArray =
                "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var lengthOtp = 6;
        var captcha = [];
        for (var i = 0; i < lengthOtp; i++) {
            //below code will not allow Repetition of Characters
            var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
            if (captcha.indexOf(charsArray[index]) == -1)
                captcha.push(charsArray[index]);
            else
                i--;
        }
        var canv = document.createElement("canvas");
        canv.id = "captcha";
        canv.width = 150;
        canv.height = 50;
        var ctx = canv.getContext("2d");
        ctx.font = "25px Georgia";
        ctx.strokeText(captcha.join(""), 0, 30);
        //storing captcha so that can validate you can save it somewhere else according to your specific requirements
        code = captcha.join("");
        document.getElementById("captcha").appendChild(canv); // adds the canvas to the body element
    }
    $("form").submit(function (e) {
        if (document.getElementById("cpatchaTextBox").value == code) {
            $("form").submit();
        } else {
            e.preventDefault();
            createCaptcha();
            return false;
        }
    });
    function validateCaptcha() {
        if (document.getElementById("cpatchaTextBox").value == code) {
            return true;

        } else {
            if ((document.getElementById("cpatchaTextBox").value).length > 0) {
                $("#captchaErr").html('Invalid Captcha! Try Again');
            }

            createCaptcha();
            return false;
        }
    }
    createCaptcha();

</script>