<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?= $site_property->site_name ?></title>
        <meta name="title" content="<?= $site_property->seo_title ?>">
        <meta name="description" content="<?= $site_property->seo_keywords ?>">
        <meta name="keywords" content="<?= $site_property->seo_description ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>uploads/<?= $site_property->favicon ?>">

        <!-- all css here -->
        <!-- bootstrap v3.3.6 css -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/meanmenu.min.css">
        <!-- owl.carousel css -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css">
        <!-- ionicons.min css -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/ionicons.min.css">
        <!-- nivo-slider.css -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/nivo-slider.css">
        <!-- style css -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.steps1.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/newcustom.css">
        <!-- responsive css -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css">
        <!-- modernizr css -->
        <script src="<?= base_url() ?>assets/js/modernizr-2.8.3.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
    </head>
    <body class="home-2">
        <?php $page = basename($_SERVER['SCRIPT_NAME']); ?>
        <?php if ($this->session->userdata('login_success')) { ?>
            <div class="alert alert-success" role="alert" id="my_login_alert">
                Hi, <b><?= $this->session->userdata('user_name') ?></b>. Welcome Back
                <span class="btn" style="cursor: pointer" onclick="fadeOutMyMessage(0)">&times;</span>
            </div>
        <?php } $this->session->unset_userdata('login_success'); ?>
        <?php if ($this->session->userdata('logout_success')) { ?>
            <div class="alert alert-success" role="alert" id="my_login_alert">
                Successfully Logged Out
                <span class="btn" style="cursor: pointer" onclick="fadeOutMyMessage(0)">&times;</span>
            </div>
        <?php } $this->session->unset_userdata('logout_success'); ?>
        <?php if ($this->session->userdata('logout_success_admin')) { ?>
            <div class="alert alert-danger" role="alert" id="my_login_alert">
                Please Login Again to Continue
                <span class="btn" style="cursor: pointer" onclick="fadeOutMyMessage(0)">&times;</span>
            </div>
        <?php } $this->session->unset_userdata('logout_success_admin'); ?>
        <?php if ($this->session->userdata('custom_message')) { ?>
            <div class="alert alert-danger" role="alert" id="my_login_alert">
                <?= $this->session->userdata('custom_message') ?>
                <span class="btn" style="cursor: pointer" onclick="fadeOutMyMessage(0)">&times;</span>
            </div>
        <?php } $this->session->unset_userdata('custom_message'); ?>

        <?php if ($this->session->userdata('feedback_submission_success')) { ?>
            <div class="alert alert-success" role="alert" id="my_login_alert">
                <?= $this->session->userdata('feedback_submission_success') ?>
                <span class="btn" style="cursor: pointer" onclick="fadeOutMyMessage(0)">&times;</span>
            </div>
        <?php } $this->session->unset_userdata('feedback_submission_success'); ?>

        <?php if ($this->session->userdata('feedback_submission_failed')) { ?>
            <div class="alert alert-danger" role="alert" id="my_login_alert" style="padding-left: 30px; padding-right: 30px ">
                <?= $this->session->userdata('feedback_submission_failed') ?>
                <span class="btn" style="cursor: pointer" onclick="fadeOutMyMessage(0)">&times;</span>
            </div>
        <?php } $this->session->unset_userdata('feedback_submission_failed'); ?>

        <style>
            #my_login_alert{
                position: fixed;
                right: 50px;
                top: 100px;
                z-index: 100000;
                opacity: 0.8;
                transition: visibility 0s 2s, opacity 2s linear;
            }
        </style>
        <script>
            setTimeout(function () {
                var ele = document.getElementById("my_login_alert");
                if (ele) {
                    fadeOutMyMessage(2500);
                }
            }, 2500);
            function fadeOutMyMessage(time) {
                $("#my_login_alert").fadeOut(time);
            }
        </script>
        <div id="page-wraper-2">
            <!-- header-area-start -->
            <header>
                <!-- header-top-area-start -->
                <div class="header-top-area" id="sticky-header">
                    <div class="container-fulied">
                        <div class="row justify-content-between">
                            <div class="col-xl-5 col-lg-5 col-md-5 d-none d-lg-block d-md-block">
                                <!-- menu-area-start -->
                                <div class="menu-area">
                                    <nav>
                                        <ul>
                                            <li><a href="<?= base_url() ?>">Home</a></li>
                                            <li><a href="<?= base_url() ?>how-it-works">How It Works?</a></li>
                                            <li><a href="<?= base_url() ?>contact-us">Contact Us</a></li>
                                        </ul>
                                    </nav>
                                </div>
                                <!-- menu-area-end -->
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-3 col-5">
                                <!-- logo-area-start -->
                                <div class="logo-area">
                                    <a href="<?= base_url() ?>"><img src="<?= base_url() ?>uploads/<?= $site_property->logo ?>" alt="logo" /></a>
                                </div>
                                <!-- logo-area-end -->
                            </div>
                            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-3 col-6">
                                <!-- header-right-area-start -->
                                <div class="header-right-area">
                                    <ul>

                                        <li>
                                            <button class="btn theme-toggle sidebar-toggle mob-tog-btn" data-sidebar-target="02">
                                                <i class="icon ion-drag"></i>
                                            </button>
                                            <a href="#" id="show-cart"><i class="icon ion-drag"></i></a>
                                            <div class="shapping-area" id="hide-cart">
                                                <div class="single-shapping">
                                                    <span>My Account</span>
                                                    <?php if (empty($this->session->userdata('user_unq_id'))) { ?>
                                                        <ul>
                                                            <li><a href="#registerModal" data-toggle="modal"><i class="fal fa-user"></i> Register</a></li>
                                                            <li><a href="#loginModal" data-toggle="modal"><i class="fal fa-lock"></i> Login</a></li>
                                                        </ul>
                                                    <?php } else { ?>
                                                        <ul>
                                                            <li><a href="<?= base_url() ?>dashboard"><i class="fal fa-user"></i> Profile</a></li>
                                                            <li><a href="<?= base_url('authentication/logout') ?>"><i class="fal fa-sign-out"></i> Logout</a></li>
                                                        </ul>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- header-right-area-end -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- header-top-area-end -->
            </header>
            <!-- header-area-end -->
            <div class="sidebar sidebar-left" id="02">
                <div class="d-flex flex-column">
                    <div class="flex-grow-0 border-bottom py-2 px-2 px-md-5 bg-light">
                        <div class="row align-items-center">
                            <div class="col">
                                <h5 class="m-0">Main Menu</h5>
                            </div>
                            <div class="col-auto">
                                <button class="btn bg-transparent shadow-none btn-sm" data-sidebar-dismiss="02"><i class="fal fa-times h4 m-0 text-black-50"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="flex-grow-1" style="overflow-y: auto;">
                        <div class="list-group list-group-flush">
                            <a href="index.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-home"></i> Home</a>
                            <a href="how-it-works.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-question-circle"></i> How It Works?</a>
                            <a href="contact-us.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-handshake-alt"></i> Contact Us</a>
                            <a href="dashboard.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-user"></i> My Profile</a>
                            <a href="sizes.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-ruler-horizontal"></i> Sizes </a>
                            <a href="subscription-preferances.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-envelope-open"></i> Subscription & Preferances</a>
                            <a href="dash-we-shop-for-you.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-shopping-bag"></i> We Shop For You</a>
                            <a href="myorders.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-list"></i> My Orders</a>
                            <a href="report-issue.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-comment"></i> Report Issue</a>
                            <a href="index.php" class="list-group-item list-group-item-action px-3 px-md-3"><i class="fal fa-power-off"></i> Logout</a>
                        </div>
                    </div>
                    <div class="flex-grow-0"></div>
                </div>
            </div>