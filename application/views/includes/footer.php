<!-- newslatter-area-start -->
<div class="modal fade" id="viewstyleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                </button>
                <img src="<?= base_url() ?>assets/images/sizechart.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</div>
<div class="newslatter-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-12">
                <div class="bt-top ptb-80">
                    <div class="newlatter-content text-center">
                        <h3><?= $footer_info->subscribe_news_letter_title ?></h3>
                        <?= $footer_info->subscribe_news_letter_description ?>
                        <form action="javascript: 0;" >
                            <input type="email" id="subs_email" placeholder="Enter your email address here..." required style="color: gray"/>
                            <button onclick="post_subs_email();"><i class="fal fa-envelope-open fa-lg"></i> Subscribe</button>
                        </form>
                        <span id="subs_email_message" style="color: tomato; margin-top: 15px;"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-3 align-self-center">
                <div class="shdbox">
                    <h4 class="cntdetails">Quick <span>Links</span></h4>
                    <ul class="quicklinks">
                        <?php foreach ($policies as $item) { ?>
                            <li><a href="<?= base_url() ?>policies/<?= urlencode($item->name) ?>"><?= $item->name ?></a></li>
                        <?php } ?>
                        <!--                        <li><a href="terms-and-conditions">Terms and Conditions</a></li>
                        <li><a href="privacy-policy.php">Privacy Policy</a></li>
                        <li><a href="return-policy.php">Return Policy</a></li>-->
                    </ul>
                </div>
            </div>
            <div class="col-md-12 col-lg-4 align-self-center">
                <div class="shdbox">
                    <h4 class="cntdetails">Contact <span>Details</span></h4>
                    <ul class="addressdetails">
                        <li><i class="fal fa-phone"></i><a href="tel:<?= $site_property->contact_number ?>"><?= $site_property->contact_number ?></a></li>
                        <li><i class="fal fa-envelope"></i> <a href="mailto:<?= $site_property->contact_email ?>"><?= $site_property->contact_email ?></a></li>
                        <li><i class="fal fa-map-marker-alt"></i><?= $site_property->address ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    function post_subs_email() {
        var email = document.getElementById("subs_email").value;
        if (email.length > 6 && validateEmail(email)) {
            $.ajax({
                url: "<?= base_url() ?>contact_us/subscribe_email",
                type: "POST",
                data: {email: email},
                success: function (resp) {
                    document.getElementById("subs_email_message").innerHTML = resp;
                }
            });
        }
    }


</script>
<!-- newslatter-area-end -->
<!-- footer-area-start -->
<footer>
    <div class="footer-area ptb-10">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <!-- footer-logo-start -->
                    <div class="footer-logo mb-3">
                        <a href="<?= base_url() ?>"><img src="<?= base_url() ?>uploads/<?= $site_property->logo ?>" alt="logo" /></a>
                    </div>
                    <!-- footer-logo-end -->
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-12 align-self-center">
                    <!-- copy-right-area-start -->
                    <div class="copy-right-area mb-3 text-center">
                        <p> &copy; 2021 copyright <?= $site_property->site_name ?> All Rights Reserved <br> Design & Developed by <a href="https://thecolourmoon.com/" target="_blank"> Colourmoon</a></p>
                    </div>
                    <!-- copy-right-area-end -->
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 align-self-center">
                    <!-- footer-social-icon-start -->
                    <div class="footer-social-icon">
                        <ul>
                            <?php if ($social_media_link->facebook) { ?>
                                <li><a href="<?= $social_media_link->facebook ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <?php } ?>
                            <?php if ($social_media_link->twitter) { ?>
                                <li><a href="<?= $social_media_link->twitter ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <?php } ?>
                            <?php if ($social_media_link->pinterest) { ?>
                                <li><a href="<?= $social_media_link->pinterest ?>" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                            <?php } ?>
                            <?php if ($social_media_link->instagram) { ?>
                                <li><a href="<?= $social_media_link->instagram ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- footer-social-icon-end -->
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer-area-end -->
</div>
<!-- page-wraper-start -->

<div class="login-box">
    <a href="#registerModal" data-toggle="modal"><i class="fal fa-user"></i> Register</a>
    <a href="#loginModal" data-toggle="modal"><i class="fal fa-lock"></i> Login</a>
</div>


<!--login-modal-->
<div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center py-3 mb-3">
                    <i class="fal fa-lock-alt fa-4x"></i>
                    <h3 class="pt-3">Login</h3>
                    <?php if (!empty($this->session->userdata("login_err"))) { ?>
                        <p style="color: red"><?= $this->session->userdata("login_err") ?></p>
                    <?php } ?>
                    <form class="form" method="post" action="<?= base_url() ?>authentication/login" id="loginForm">
                        <span class="" id="login_email_err"></span>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fal fa-envelope-open"></i></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Enter Email / Mobile" name="email_or_phone" required>
                        </div>
                        <span  id="password_err1"></span>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fal fa-key"></i></span>
                            </div>
                            <input type="password"  id="" class="form-control password" placeholder="Password" name="password" required >
                        </div>
                        <div class="form-group">
                            <p class="text-right"><a href="#forgotModal" data-toggle="modal" data-dismiss="modal"><i class="fal fa-info-circle"></i> Forgot Password?</a></p>
                        </div>
                        <div class="form-group">
                            <button type="submit"  id="login_btn"  class="btn-login"><i class="fal fa-paper-plane"></i> LOGIN</button>
                        </div>
                    </form>
                    <p class="text-center">New to <?= $site_property->site_name ?> fashion <a href="#registerModal" data-toggle="modal" data-dismiss="modal">Create Account?</a></p>
                </div>
            </div>
            <div class="backdrop-logo"><img src="<?= base_url() ?>assets/images/backdrop-logo.png" alt=""></div>
        </div>
    </div>
</div>
<!--register-modal-->
<div class="modal fade" id="registerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center py-3 mb-3">
                    <i class="fal fa-user fa-4x"></i>
                    <form class="form" method="post" action="<?= base_url() ?>authentication/register" id="registerForm" autocomplete="off">
                        <h3 class="pt-3">Register</h3>
                        <?php if (!empty($this->session->userdata("register_err"))) { ?>
                            <p style="color: red"><?= $this->session->userdata("register_err") ?></p>
                        <?php } ?>
                        <?php if (!empty($this->session->userdata("register_success"))) { ?>
                            <p style="color: green"><?= $this->session->userdata("register_success") ?></p>
                        <?php } ?>
                        <span class="" id="name_err"></span>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fal fa-user"></i></span>
                            </div>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Name *" required="" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode == 32)">
                        </div>
                        <span class="" id="email_err" style="text-align: left"></span>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fal fa-envelope-open"></i></span>
                            </div>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email *" required>
                        </div>
                        <span class="" id="mobile_err" style="text-align: left"></span>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fal fa-mobile"></i></span>
                            </div>
                            <input type="text" name="mobile" id="phone_number" class="form-control" placeholder="Mobile *" required>
                        </div>
                        <span id="password_err" style="text-align: left" ></span>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fal fa-key"></i></span>
                            </div>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password *" required>
                        </div>
                        <span class="" id="confirm_password_err" style="text-align: left"></span>

                        <div class="form-group">
                            <button type="submit" id="register_submit" class="btn btn-primary"><i class="fal fa-paper-plane"></i> Register</button>
                        </div>
                    </form>
                    <p>Already have an account? <a href="#loginModal" data-toggle="modal" data-dismiss="modal">Login</a></p>
                </div>
            </div>
            <div class="backdrop-logo"><img src="<?= base_url() ?>assets/images/backdrop-logo.png" alt=""></div>
        </div>
    </div>
</div>

<!--forgotmodal-->
<!--login-modal-->
<div class="modal fade" id="forgotModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center py-3 mb-3">
                    <i class="fal fa-key fa-4x"></i>
                    <h3 class="pt-3">Forgot Password?</h3>
                    <P>Please enter your email address to request a password reset.</P>

                    <div style="width: 100%">
                        <p id="forgotEmailErr" style="float: left; text-align: left; font-size: 14px"></p>
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fal fa-envelope-open"></i></span>
                        </div>
                        <input type="email" id="forgot_email" class="form-control" placeholder="Enter Email Address *">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn-login" onclick="forgotPassowrd()"><i class="fal fa-paper-plane"></i> SUBMIT</button>
                    </div>
                </div>
            </div>
            <div class="backdrop-logo"><img src="<?= base_url() ?>assets/images/backdrop-logo.png" alt=""></div>
        </div>
    </div>
</div>

<script>

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    function forgotPassowrd() {
        var err = document.getElementById("forgotEmailErr");
        var email = document.getElementById("forgot_email").value;
        if (!validateEmail(email)) {
            err.innerHTML = "Enter Valid Email";
            err.style.color = "red";
            return false;
        } else {
            err.innerHTML = "";
            $.ajax({
                url: '<?= base_url() ?>authentication/forgot_password',
                type: 'post',
                data: {email: email},
                success: function (resp) {
                    resp = JSON.parse(resp);
                    console.log(resp);
                    err.innerHTML = resp.message;
                    if (resp.status === "invalid") {
                        err.style.color = "red";
                    } else {
                        err.style.color = "green";
                    }
                }
            });
        }
    }
</script>
<!--feedback-modal-->
<div class="modal fade feedbackModal" id="feedbackModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center py-3 mb-3">
                    <h3 class="pt-3">Feedback</h3>
                    <form action="<?= base_url('my_orders/submit_feedback') ?>" id="feedback_form" method="post">
                        <input type="hidden" name="order_id" id="order_id">
                        <input type="hidden" name="order_unq_id" id="order_unq_id">
                        <div class="form-group text-left">
                            <label for="">Rating</label> <br>
                            <fieldset class="rating">
                                <input type="radio" id="star5" name="rating" class="stars" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4" name="rating" class="stars" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3" name="rating" class="stars" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2" name="rating" class="stars" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1" name="rating" class="stars" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                            </fieldset><br> <br>
                            <label id="rating_err" for="" style="color: tomato;"></label>
                        </div>
                        <div class="form-group">
                            <textarea rows="5" class="form-control" name="feedback_message" id="feedback_message" placeholder="Message"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" id="submitfeedback" class="btn-login btn-block">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade feedbackModal" id="returnOrderModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Request For Exchange</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                </button>
            </div>
            <hr>
            <form action="<?= base_url('my_orders/request_exchange') ?>" method="post" id="exchangeForm">
                <div class="modal-body">
                    <input type="hidden" name="exchange_subscription_transaction_id" id="exchange_subscription_transaction_id">
                    <input type="hidden" name="exchange_subscription_transaction_unq_id" id="exchange_subscription_transaction_unq_id">
                    <textarea name="exchange_reason" id="exchange_reason" class="form-control" placeholder="Enter Request Reason" required minlength="30"></textarea>
                    <p id="reason_err" style="color: tomato"></p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary btn-sm" type="submit" id="exchange_submit">Proceed to Exchange</button>
                </div>
            </form>
            <div id="next-step-exchange" style="display: none">
                <div class="modal-body">
                    <label>Order Id : </label>&nbsp;&nbsp;<span id="exchange_order_id"></span><br><br>
                    <label>Exchange Request Remark</label>
                    <br>
                    <p id="request_exchange_remark"></p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary btn-sm" id="exchange_go_back">Go Back</button>
                    <button class="btn btn-primary btn-sm" id="place_exchange">Place Exchange</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#place_exchange').click(function (e) {
        $('#exchangeForm').submit();
    });
    $("#exchange_submit").click(function (e) {
        e.preventDefault();
        var exchange_reason = $("#exchange_reason").val();
        if (exchange_reason.length > 30) {
            $('#reason_err').html('');
            $("#request_exchange_remark").html(exchange_reason);
            document.getElementById("exchangeForm").style.display = "none";
            document.getElementById("next-step-exchange").style.display = "block";
        } else {
            $('#reason_err').html("<br> Please Enter the reason of exchange in atleast 30 words");
        }
    });
    $("#exchange_go_back").click(function () {
        document.getElementById("exchangeForm").style.display = "block";
        document.getElementById("next-step-exchange").style.display = "none";
    });
    function setOrderIdForExchange(order_id, unq_id = '') {
        $("#exchange_order_id").html(unq_id);
        $("#exchange_subscription_transaction_unq_id").val(unq_id);
        $("#exchange_subscription_transaction_id").val(order_id);
    }
    $("#submitfeedback").click(function (e) {
        e.preventDefault();
        var chec = document.querySelector('input[name="rating"]:checked');
        if (!chec) {
            $("#rating_err").html("please select the stars");
            return false;
        } else {
            $("#rating_err").html("");
            $("#feedback_form").submit();
        }
    });

    function feedbackModal(order_id, order_real_id, rating = '', message = '') {
        $("#order_id").val(order_id);
        $('#order_unq_id').val(order_real_id);
        var ratings = document.getElementsByClassName("stars");
        document.getElementById("feedback_message").value = "";
        for (var i = 0; i < ratings.length; i++) {
            ratings[i].checked = false;
        }
        if (rating !== '') {
            document.getElementById("star" + rating).checked = true;
        }
        if (message !== '') {
            $('#feedback_message').val(message);
    }
    }
</script>
<!-- all js here -->
<!-- jquery latest version -->
<script src="<?= base_url() ?>assets/js/jquery-v3.4.1.min.js"></script>
<!-- popper js -->
<script src="<?= base_url() ?>assets/js/popper.js"></script>
<!-- bootstrap js -->
<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
<!-- owl.carousel js -->
<script src="<?= base_url() ?>assets/js/owl.carousel.min.js"></script>
<!-- magnific popup js -->
<script src="<?= base_url() ?>assets/js/magnific-popup.js"></script>
<!-- meanmenu js -->
<script src="<?= base_url() ?>assets/js/jquery.meanmenu.js"></script>
<!-- jquery-ui js -->
<script src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
<!-- wow js -->
<script src="<?= base_url() ?>assets/js/wow.min.js"></script>
<!-- jquery.nivo.slider.js -->
<script src="<?= base_url() ?>assets/js/jquery.nivo.slider.js"></script>
<!-- jquery.elevateZoom-3.0.8.min.js -->
<script src="<?= base_url() ?>assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
<!-- jquery.parallax-1.1.3.js -->
<script src="<?= base_url() ?>assets/js/jquery.parallax-1.1.3.js"></script>
<!-- jquery.counterup.min.js -->
<script src="<?= base_url() ?>assets/js/jquery.counterup.min.js"></script>
<!-- waypoints.min.js -->
<script src="<?= base_url() ?>assets/js/waypoints.min.js"></script>
<!-- plugins js -->
<script src="<?= base_url() ?>assets/js/plugins.js"></script>
<!-- main js -->
<script src="<?= base_url() ?>assets/js/main.js"></script>
<script src="<?= base_url() ?>assets/js/jquery.steps.min.js"></script>
<script src="<?= base_url() ?>assets/js/steps.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/sidebar.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/sticky-sidebar/3.3.1/sticky-sidebar.min.js"></script>
<script>
    $(document).ready(function () {
        $('#sidebar').stickySidebar({
            topSpacing: 100,
            bottomSpacing: 100
        });
    })
</script>

<style>
    #registerModal span, #loginModal span{
        float: left;
        color: indianred;
        font-size: 14px;
    }

</style>
<?php
if (!empty($this->session->userdata("register_err")) || !empty($this->session->userdata("register_success"))) {
    echo "<script>$('#registerModal').modal('show');</script>";
}
if (!empty($this->session->userdata("login_err"))) {
    echo "<script>$('#loginModal').modal('show');</script>";
}
$this->session->unset_userdata("register_err");
$this->session->unset_userdata("register_success");
$this->session->unset_userdata("login_err");
?>
<script>
    $(document).ready(function () {
        $(window).keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
    });
    $("#registerForm").submit(function (e) {
        var valid = true;
        if ($('#name').val().length < 3) {
            $('#name_err').html("* Name must be atleast 3 Charecters");
            valid = false;
            return valid;
        } else {
            $('#name_err').html("");
        }
        if ($('#email').val().length < 8 && !validateEmail($('#email').val())) {
            $('#email_err').html("* Please enter a valid Email");
            valid = false;
            return valid;
        } else {
            $('#email_err').html("");
        }
        if ($('#mobile').val().length !== 10) {
            $('#mobile_err').html("* Please enter a valid Mobile Number");
            valid = false;
            return valid;
        } else {
            $('#mobile_err').html("");
        }
        if ($('#password').val().length < 8) {
            $('#password_err').html("* Password must be atleast 8 Charecters");
            valid = false;
            return valid;
        } else {
            $('#password_err').html("");
        }
        if ($('#password').val() !== $('#confirm_password').val()) {
            $('#confirm_password_err').html("* Passwords does not Match");
            valid = false;
            return valid;
        } else {
            $('#confirm_password_err_err').html("");
        }

        if (valid) {
            $("#registerForm").submit();
        } else {
            e.preventDefault();
            return false;
        }
    });
    function validateRegisterForm(eve) {
        eve.preventDefault();
    }

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    $(function () {
        $('.button-checkbox').each(function () {

            // Settings
            var $widget = $(this),
                    $button = $widget.find('button'),
                    $checkbox = $widget.find('input:checkbox'),
                    color = $button.data('color'),
                    settings = {
                        on: {
                            icon: ''//'glyphicon glyphicon-check'
                        },
                        off: {
                            icon: ''//'glyphicon glyphicon-unchecked'
                        }
                    };
            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });
            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');
                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");
                // Set the button's icon
                $button.find('.state-icon')
                        .removeClass()
                        .addClass('state-icon ' + settings[$button.data('state')].icon);
                // Update the button's color
                if (isChecked) {
                    $button
                            .removeClass('btn-default')
                            .addClass('btn-' + color + ' active');
                } else {
                    $button
                            .removeClass('btn-' + color + ' active')
                            .addClass('btn-default');
                }
            }

            // Initialization
            function init() {

                updateDisplay();
                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });</script>
<style>
    /* width */
    ::-webkit-scrollbar {
        width: 6px;
        background-color: white
    }

    /* Track */
    ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 5px grey;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #23527c;
        border-radius: 10px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: #0c1a2e;
    }
</style>
<script>
    $(document).ready(function () {
        const validateEmail = (email) => {
            return email.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        };
        const validate = () => {
            const $email_err = $('#email_err');
            const email = $('#email').val();
            $email_err.text('');
            $('#register_submit').prop('disabled', false);
            if (validateEmail(email)) {
                $('#register_submit').prop('disabled', false);
                $email_err.text('');
            } else {
                $('#register_submit').prop('disabled', true);
                $email_err.text("Enter valid email address");
                $email_err.css('color', 'red');
            }
            return false;
        };
        $('#email').on('input', validate);





        const validateMoble = (mobile) => {
            return mobile.match(/^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/);
        };


        const validatePhoneNumber = () => {
            const $mobile_err = $('#mobile_err');
            const mobile = $('#phone_number').val();
            $mobile_err.text('');
            $('#register_submit').prop('disabled', false);
            if (validateMoble(mobile)) {
                $('#register_submit').prop('disabled', false);
                $mobile_err.text('');
            } else {
                $('#register_submit').prop('disabled', true);
                $mobile_err.text("Enter valid 10 digit mobile number");
                $mobile_err.css('color', 'red');
            }
            return false;
        };

        $('#phone_number').on('input', validatePhoneNumber);


        const validatePass = (password) => {
            return password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)\S{6,50}$/);
        };


        const validatePassword = () => {
            const $password_err = $('#password_err');
            const password = $('#password').val();
            $password_err.text('');
            $('#register_submit').prop('disabled', false);
            $('#login_btn').prop('disabled', false);
            if (validatePass(password)) {
                $('#register_submit').prop('disabled', false);

                $password_err.text('');
            } else {
                $('#register_submit').prop('disabled', true);

                $password_err.text("Password should contain atleast one Capital letter,Small letter and minimum of 6 characters with no spaces");
                $password_err.css('color', 'red');
            }
            return false;
        };

        $('#password').on('input', validatePassword);




        const validateLoginPass = (password) => {
            return password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)\S{6,50}$/);
        };


        const validateLoginPassword = () => {
            const $password_err1 = $('#password_err1');
            const password = $('#lpassword').val();
            $password_err1.text('');
            $('#register_submit').prop('disabled', false);
            $('#login_btn').prop('disabled', false);
            if (validateLoginPass(password)) {
                $('#login_btn').prop('disabled', false);
                $password_err1.text('');
            } else {
                $('#login_btn').prop('disabled', true);

                $password_err1.text("Password should contain atleast one Capital letter,Small letter and minimum of 6 characters with no spaces");
                $password_err1.css('color', 'red');
            }
            return false;
        };

        $('#lpassword').on('input', validateLoginPassword);




    });

</script>

<script type="text/javascript" src="<?php echo base_url(); ?>web_assets/js/plugins/parsleyjs/dist/parsley.min.js"></script> 
<script type="text/javascript">
    $(document).ready(function () {
        $('#contact-form').parsley();
    });
</script>
</body>
</html>
