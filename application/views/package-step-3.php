<div class="pkgsteps">
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12">
                <div class="whitebox">
                    <form class="new-added-form" id="step_three_form" method="POST" enctype="multipart/form-data" action="<?= base_url('package_steps/submit_form/' . $current_package . '/step-four') ?>">
                        <section>
                            <input id="unq_id" type="hidden" name="unq_id" value="<?= $random_unq_key ?>">
                            <p><strong>Your Preferences</strong></p>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group mb-3">
                                        <select class="form-control" name="height" required>
                                            <option value="">-- select Height --</option>
                                            <?php foreach ($heights as $hh) { ?>
                                                <option value="<?= $het = $hh->feet . '.' . $hh->inches; ?>" <?php
                                                if ($user_preferences->height === $het) {
                                                    echo "selected";
                                                } else if ($hh->id === $user_preferences->height) {
                                                    echo "selected";
                                                }
                                                ?>><?= $het ?>"</option>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group mb-3">
                                        <select class="form-control" name="weight" required>
                                            <option value="">-- select Weight --</option>
                                            <?php foreach ($weights as $hh) { ?>
                                                <option value="<?= $hh->weight ?>" <?php
                                                if ($user_preferences->weight === "" . $hh->weight . "") {
                                                    echo "selected";
                                                } else if ($hh->id === $user_preferences->weight) {
                                                    echo "selected";
                                                }
                                                ?>><?= $hh->weight ?></option>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group mb-0">
                                        <input type="file" id="file-2" class="inputfile inputfile-1 full_sized_images" name="full_sized_images" multiple accept="image/*"  <?php
                                        if (!$previous_full_images) {
                                            //echo 'required';
                                        }
                                        ?>/>
                                        <label for="file-2"> <span>Upload your full sized photos</span></label>
                                    </div>
                                    <div class="full_gallery row">
                                        <?php
                                        foreach ($previous_full_images as $img) {
                                            if ($img->type == "Full") {
                                                ?>
                                                <div class="col-3">
                                                    <a onclick="deleteImage('<?= $img->id ?>', '<?= $img->type ?>', '<?= $img->subscriptions_transactions_id ?>')">
                                                        <center>
                                                            <i class="fal fa-trash-alt"></i>
                                                        </center>
                                                    </a>
                                                    <img src="<?= base_url($img->image) ?>" alt="alt"/>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group mb-0">
                                        <input type="file" id="file-1" class="inputfile inputfile-1 half_sized_images" name="half_sized_images" multiple accept="images/*" <?php
                                        if (!$previous_half_images) {
                                            //echo 'required';
                                        }
                                        ?> />
                                        <label for = "file-1"> <span>Upload your half sized photos</span></label>
                                    </div>
                                    <div class = "half_gallery row">
                                        <?php
                                        foreach ($previous_half_images as $img) {
                                            if ($img->type == "Half") {
                                                ?>
                                                <div class="col-3">
                                                    <a onclick="deleteImage('<?= $img->id ?>', '<?= $img->type ?>', '<?= $img->subscriptions_transactions_id ?>')">
                                                        <center>
                                                            <i class="fal fa-trash-alt"></i>
                                                        </center>
                                                    </a>
                                                    <img src="<?= base_url($img->image) ?>" alt="alt"/>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <p class="text-danger"><small><b>Note:</b> Add Half and Full-Size photos (By providing Half and Full size images it will be helpful to choose better suitable Clothing.)</small></p>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <p><strong>Preferences</strong></p>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group mb-3">
                                        <label for="">Likes</label>
                                        <textarea rows="3" required class="form-control" minlength="30" maxlength="100" name="preferences_likes" placeholder=""><?= ($user_preferences->preferences_likes) ? $user_preferences->preferences_likes : $user_preferences->liked_preferences ?></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group mb-3">
                                        <label for="">Dislikes</label>
                                        <textarea rows="3" required class="form-control" minlength="30" maxlength="100"  name="preferences_dislikes" placeholder=""><?= ($user_preferences->preferences_dislikes) ? $user_preferences->preferences_dislikes : $user_preferences->disliked_preferences ?></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 mt-5">
                                    <div class="d-flex justify-content-between">
                                        <button type="button" class="btn-previous" onclick="location.href = '<?= base_url() ?>package-steps/step-two/<?= $current_package ?>/<?= $random_unq_key ?>'">PREVIOUS</button>
                                        <button type="submit" class="btn-next" id="next-button">NEXT</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
            <?php if ($current_package !== "we-shop-for-you") { ?>
                <div class="col-lg-4 col-md-8">
                    <div class="d-flex justify-content-between mb-2 align-items-center">
                        Selected Package
                        <a href="<?= base_url('packages') ?>" class="btn btn-sm btn-primary"><i class="fal fa-sync"></i> Change Package</a>
                    </div>
                    <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                        <div class="banner-img">
                            <a href="#"><img src="<?= base_url() ?>uploads/<?= $package_details->image ?>" alt="banner" /></a>
                        </div>
                        <div class="banner-content-3">
                            <?= $package_details->title ?>
                            <h5><?= $package_details->no_of_items ?> items / <?= $package_details->duration_in_words ?></h5>
                            <p>Subscription Start from <?= (int) $package_details->price ?>/month</p>
                            <?= $package_details->subscription_features ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="best-sell-active owl-carousel owl-theme">
                    <?php foreach ($package_banners as $banner) { ?>
                        <div class="item"><a href="#"><img src="<?= base_url() ?>uploads/<?= $banner->image ?>" alt=""></a></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var j = false;
    k = false;
    $("#step_three_form").submit(function () {
        if (j && k) {
            $("#step_three_form")[0].reset();
        }
    });
    $('.full_sized_images').on('change', function () {
        var fd = new FormData();
        var ins = this.files.length;
        for (var x = 0; x < ins; x++) {
            fd.append("files[]", this.files[x]);
        }
        $.ajax({
            url: '<?= base_url() ?>package_steps/upload_submitted_images/<?= $random_unq_key ?>/Full',
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (response) {
                $("#file-2").removeAttr("required");
                $(".full_gallery").html(response);
                j = true;
            }
        });
    });

    $('.half_sized_images').on('change', function () {
        var fd = new FormData();
        var ins = this.files.length;
        for (var x = 0; x < ins; x++) {
            fd.append("files[]", this.files[x]);
        }
        $.ajax({
            url: '<?= base_url() ?>package_steps/upload_submitted_images/<?= $random_unq_key ?>/Half',
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (response) {
                $(".half_gallery").html(response);
                $("#file-1").removeAttr("required");
                k = true;
            }
        });
    });

    function deleteImage(id, type, unq_id) {
        $.ajax({
            url: '<?= base_url() ?>package_steps/delete_image/<?= $random_unq_key ?>',
                        data: {id: id, type: type, unq_id: unq_id},
                        cache: false,
                        type: 'POST',
                        success: function (response) {
                            if (type == "Half") {
                                $(".half_gallery").html(response);
                            }
                            if (type == "Full") {
                                $(".full_gallery").html(response);
                            }
                            location.href = "";
                        }
                    });
                }
</script>
<style>
    .col-3{
        padding: 0px;
    }
    .full_gallery, .half_gallery {
        margin-left: 10px;
    }
    .full_gallery img, .half_gallery img{
        width: 80px;
        height: 80px;
    }
    .full_gallery a, .half_gallery a{
        cursor: pointer;
        position: absolute;
        width: 100%;
        opacity: 0;
        height: 80px;
        top: 0;
        right:0;
        background-color: #eaeaea;
    }
    .full_gallery a center, .half_gallery a center{
        height: 80px;
    }
    .full_gallery a center i, .half_gallery a center i{
        margin-top: 40%;
        color: tomato;
    }
    .full_gallery a:hover{
        opacity: 1;
    }

    .half_gallery a:hover{
        opacity: 1;
    }

</style>