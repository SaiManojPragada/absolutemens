<div class="pkgsteps">
	<?php include 'includes/header.php' ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12 col-md-12">
				<div class="whitebox">
					<form class="new-added-form" action="we-shop-for-you-step-3.php">
						<section>
							<h6>We Shop For You</h6>
							<p><strong>What is Your Size ?</strong></p>
							<div class="row">
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<select class="form-control">
										<option value="">Shirt size?</option>
										<option value="">XL</option>
										<option value="">L</option>
										<option value="">M</option>
									</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<select class="form-control">
										<option value="">T-Shirt size?</option>
										<option value="">XL</option>
										<option value="">L</option>
										<option value="">M</option>
									</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<select class="form-control">
										<option value="">Casual pants size?</option>
										<option value="">42</option>
										<option value="">40</option>
										<option value="">38</option>
									</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<select class="form-control">
										<option value="">Formals size?</option>
										<option value="">42</option>
										<option value="">40</option>
										<option value="">38</option>
									</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<select class="form-control">
										<option value="">Jeans size?</option>
										<option value="">42</option>
										<option value="">40</option>
										<option value="">38</option>
									</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<select class="form-control">
										<option value="">Shoes size?</option>
										<option value="">42</option>
										<option value="">40</option>
										<option value="">38</option>
									</select>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 pb-3">
									<a href="#"><i class="fal fa-ruler-horizontal"></i> View Style Guide</a>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="form-group mb-0">
										<input type="file" id="file-1" class="inputfile inputfile-1" multiple />
										<label for="file-1"> <span>Upload your full sized photos</span></label>
									</div>
								</div>
								<div class="col-lg-6 col-md-6">
									<div class="form-group mb-0">
										<input type="file" id="file-1" class="inputfile inputfile-1" multiple />
										<label for="file-1"> <span>Upload your half sized photos</span></label>
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<p class="text-danger"><small>*Note: Adding Half and Full-Size photos</small></p>
								</div>
								<div class="col-lg-6 col-md-6">
									<label for="">Special Instructions</label>
									<textarea rows="3" class="form-control" placeholder="Describe your occassion in max words 200"></textarea>
								</div>
								<div class="col-lg-6 col-md-6">
									<label for="">Special Measurements</label>
									<textarea rows="3" class="form-control" placeholder="Describe your occassion in max words 200"></textarea>
								</div>
								<div class="col-lg-12 col-md-12 mt-5">
									<div class="d-flex justify-content-between">
										<button type="button" class="btn-previous" onclick="location.href='we-shop-for-you-step-1.php'">PREVIOUS</button>
										<button type="submit" class="btn-next">NEXT</button>
									</div>
								</div>
							</div>
						</section>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="best-sell-active owl-carousel owl-theme">
					<div class="item"><a href="#"><img src="images/bannerhow-1.jpg" alt=""></a></div>
					<div class="item"><a href="#"><img src="images/bannerhow-2.jpg" alt=""></a></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php' ?>