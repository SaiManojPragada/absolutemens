<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css"
              integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js"
        integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container-fluid" style="padding: 0px">
            <div class="container-fluid" style="min-height: 16%; background-color: #0857c040">
                <img src="<?= base_url() ?>uploads/ACTLOGO.png" alt="alt" style="width: 20%; position: absolute;
                     margin-left: 40%"/>
            </div>
            <div class="container" style="margin-top: 5%; border: 1px solid #00000050; border-radius: 10px; box-shadow: #00000035 0px 0px 50px;
                 padding: 20px 40px; color: #00000085">
                <h3>Hi There,</h3><span >&nbsp;<?= $title ?></span><br><br>
                <div class="wrapper-content">
                    <?= $message ?>
                </div>
            </div>
        </div>
    </body>
</html>