<div class="pkgsteps">
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="whitebox orderconfirm">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="text-center text-black-50">Package Order Confirm</h3>
                        </div>
                    </div>

                    <?php if ($current_package !== "we-shop-for-you") { ?>
                        <div class="row pkgitems">
                            <div class="col-lg-3 col-md-4">
                                <a href="#"><img src="<?= base_url() ?>uploads/<?= $package_details->image ?>" alt="banner" /></a>
                            </div>
                            <div class="col-lg-8 col-md-8 align-self-center">
                                <?= $package_details->title ?>
                                <?= $package_details->description ?>
                            </div>
                        </div>

                    <?php } else { ?>
                        <div class="row pkgitems">
                            <div class="col-lg-12">
                                <h3>We Shop For You Order</h3>
                            </div>
                        </div>
                    <?php } ?>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <a href="<?= base_url() ?>package-steps/step-four/<?= $current_package ?>/<?= $random_unq_key ?>" class="btn-edit"><i class="fal fa-edit"></i> Edit</a>
                        </div>
                        <div class="col-lg-12">
                            <p><strong>Shipping Address</strong></p>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <ul>
                                        <li><span>Name</span>: <?= $existing->address_name ?></li>
                                        <li><span>Address</span>: <?= $existing->address ?></li>
                                        <li><span>City</span>: <?= $existing->city ?></li>
                                        <li><span>State</span>: <?= $existing->state ?></li>
                                        <li><span>District</span>: <?= $existing->district ?></li>
                                    </ul>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <ul>
                                        <li><span>Landmark</span>: <?= $existing->landmark ?></li>
                                        <li><span>Street Name</span>: <?= $existing->street_name ?></li>
                                        <li><span>Pincode</span>: <?= $existing->pincode ?></li>
                                        <li><span>Email</span>: <?= $existing->address_email ?> </li>
                                        <li><span>Phone No.</span>: <?= $existing->phone_number ?></li>
                                        <li><span>Phone No.(Optional)</span>: <?= $existing->alternative_phone_number ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <a href="<?= base_url() ?>package-steps/step-one/<?= $current_package ?>/<?= $random_unq_key ?>" class="btn-edit"><i class="fal fa-edit"></i> Edit</a>
                            <p><strong>Style Selected</strong></p>
                            <p><?php
                                $out = "";
                                foreach ($selected_styles as $style) {
                                    $out .= $style->name . ', ';
                                }
                                echo rtrim($out, ', ');
                                ?></p>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="<?= base_url() ?>package-steps/step-two/<?= $current_package ?>/<?= $random_unq_key ?>" class="btn-edit"><i class="fal fa-edit"></i> Edit</a>
                            <p><strong>Your Size Details</strong></p>
                            <ul>
                                <?php foreach ($categories as $c) { ?>
                                    <?php foreach ($c->sizes as $item) { ?>
                                        <?php if ($item->is_selected == 'yes') { ?>
                                            <li><span><?= $c->name ?> Size</span>:  <?= $item->title ?></li>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>


<!--                                <li><span>Shirt Size</span>:  <?= $existing->shirt_size ?></li>
                                <li><span>T-Shirt Size</span>:  <?= $existing->t_shirt_size ?></li>
                                <li><span>Casual pants size</span>:  <?= $existing->casual_pant_size ?></li>
                                <li><span>Formals size</span>:  <?= $existing->formal_pant_size ?></li>
                                <li><span>Jeans size</span>:  <?= $existing->jeans_size ?></li>
                                <li><span>Shoes size</span>:  <?= $existing->shoes_size ?></li>-->
                            </ul>
                        </div>
                        <!-- <div class="col-lg-4 col-md-6">
                                <p><strong>Your Preferences</strong></p>
                                <ul>
                                        <li><span>Height</span>: 6.0"</li>
                                        <li><span>Weight</span>: 70"</li>
                                </ul>
                        </div> -->
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="<?= base_url() ?>package-steps/step-three/<?= $current_package ?>/<?= $random_unq_key ?>" class="btn-edit"><i class="fal fa-edit"></i> Edit</a>
                            <p><strong>Full Sized Photos</strong></p>	
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 updphsize">
                            <?php
                            foreach ($selected_images as $img) {
                                if ($img->type == "Full") {
                                    ?>
                                    <img src="<?= base_url() . $img->image ?>" alt="" style="object-fit: cover">
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="pt-3"><strong>Half Sized Photos</strong></p>	
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 updphhfsize">
                            <?php
                            foreach ($selected_images as $img) {
                                if ($img->type == "Half") {
                                    ?>
                                    <img src="<?= base_url() . $img->image ?>" alt="" style="object-fit: cover">
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="row pt-3">
                        <div class="col-lg-12">
                            <p><strong>Preferances</strong></p>	
                            <p><strong>Likes</strong></p>
                            <p><?= $existing->preferences_likes ?></p>
                            <p><strong>Disikes</strong></p>
                            <p><?= $existing->preferences_dislikes ?></p>
                        </div>

                    </div><hr>
                    <form action="javascript: void(0);">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" required>
                                <label class="form-check-label" for="defaultCheck1">
                                    I Accepet the <a href="<?= base_url('policies/Terms+and+Conditions') ?>" target="_blank">Terms and conditions</a>
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" class="btn-submit" onclick="checkAndProceed()">Order Confirm</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    function checkAndProceed() {
        var check = document.getElementById("defaultCheck1").checked;
        if (!check) {
            return false;
        }
        $.ajax({
            url: "<?= base_url() ?>package_steps/do_update",
            type: "post",
            data: {is_terms_and_conditions_checked: '1', unq_id: '<?= $random_unq_key ?>'},
            success: function (resp) {
                if (resp) {
                    location.href = "<?= base_url('place-order/' . $random_unq_key) ?>";
                } else {
                    alert("failed");
                }
            }
        });
    }
</script>