<html>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>uploads/<?= $site_property->favicon ?>">
</html>
<div class="pkgsteps">
    <?= $this->load->view("includes/header", $main_data); ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <?php include 'menu-dashboard.php' ?>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="whitebox contentdashboard">
                    <h3>Report Issue</h3>	
                    <?=
                    $this->session->userdata("report_success");
                    $this->session->unset_userdata("report_success");
                    ?>
                    <?=
                    $this->session->userdata("report_error");
                    $this->session->unset_userdata("report_error");
                    ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="alert alert-danger">Note: If you place an order, before 48 hours only We will do change  after 48 Hours we will not do any changes</div>
                        </div>
                    </div>
                    <form method="post" action="<?= base_url('report/post_report') ?>">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Order ID <small>( Leave Empty if not related to order )</small></label>
                                    <input type="text" name="order_id" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Raising an issue</label>
                                    <select class="form-control" required name="issue_type">
                                        <option value="">Choose the issue type</option>
                                        <option value="Submit Feedback">Submit Feedback</option>
                                        <option value="Delivery Status">Delivery Status</option>
                                        <option value="Request for Exchange">Request for Exchange</option>
                                        <option value="Others">Others</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group mb-3">
                                    <label for="">Message</label>
                                    <textarea rows="3" class="form-control" name="message" required minlength="30"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn-submit"><i class="fal fa-paper-plane"></i> SEND</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>