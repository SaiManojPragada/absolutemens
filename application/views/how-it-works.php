<div class="cms-subpage">
    <h4><?= strip_tags($how_it_works_images->title) ?></h4>
</div>
<div class="howitbox ptb-80">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="videobox">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="<?= $how_it_works_images->video_url ?>" allowfullscreen></iframe>
                    </div>							
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <img src="<?= base_url() . 'uploads/' . $how_it_works_points[3]->image ?>" alt="">
            </div>
            <div class="col-lg-5 col-md-6 align-self-center">
                <h2><?= str_replace("<h5>", "", str_replace("<strong>", "<span>", str_replace("</strong>", "</span>", $how_it_works_points[3]->title))) ?></h2>
                <?= $how_it_works_points[3]->description ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 offset-lg-1 align-self-center order-lg-0 order-md-0 order-1">
                <h2><?= str_replace("<h5>", "", str_replace("<strong>", "<span>", str_replace("</strong>", "</span>", $how_it_works_points[2]->title))) ?></h2>
                <?= $how_it_works_points[2]->description ?>
            </div>
            <div class="col-lg-5 col-md-6 order-lg-1 order-md-1 order-0">
                <img src="<?= base_url() . 'uploads/' . $how_it_works_points[2]->image ?>" alt="">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-md-6">
                <img src="<?= base_url() . 'uploads/' . $how_it_works_points[1]->image ?>" alt="">
            </div>
            <div class="col-lg-5 col-md-6 align-self-center">
                <h2><?= str_replace("<h5>", "", str_replace("<strong>", "<span>", str_replace("</strong>", "</span>", $how_it_works_points[1]->title))) ?></h2>
                <?= $how_it_works_points[1]->description ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 offset-lg-1 align-self-center order-lg-0 order-md-0 order-1">
                <h2><?= str_replace("<h5>", "", str_replace("<strong>", "<span>", str_replace("</strong>", "</span>", $how_it_works_points[0]->title))) ?></h2>
                <?= $how_it_works_points[0]->description ?>
            </div>
            <div class="col-lg-5 col-md-6 order-lg-1 order-md-1 order-0">
                <img src="<?= base_url() . 'uploads/' . $how_it_works_points[0]->image ?>" alt="">
            </div>
        </div>
    </div>
</div>