
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <?php include 'menu-dashboard.php' ?> 
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="whitebox contentdashboard">
                    <h3>Subscription & Preferances</h3>
                    <div class="row">
                        <div class="col-lg-5 col-md-6 align-self-center">
                            <p><strong>Selected style</strong></p>
                            <div class="selfashion">
                                <div class="input-group ml-0">
                                    <span class="button-checkbox input-group-btn">
                                        <button type="button" class="btn btn-outline-primary" data-color="primary">College</button>
                                        <input type="checkbox" class="hidden"  checked/>
                                    </span>
                                    <span class="button-checkbox input-group-btn">
                                        <button type="button" class="btn btn-outline-primary" data-color="primary">Trendy</button>
                                        <input type="checkbox" class="hidden" checked/>
                                    </span>
                                    <span class="button-checkbox input-group-btn">
                                        <input type="checkbox" class="hidden"  checked/>
                                        <button type="button" class="btn btn-outline-primary" data-color="primary">Office</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="d-lg-flex d-md-flex justify-content-between mb-4 subscrpre">
                                <a href="change-package.php" class="btn btn-sm btn-primary"><i class="fal fa-sync"></i> Change Package</a>
                                <a href="un-subscription-package.php" class="btn btn-sm btn-danger cancelbtn"><i class="fal fa-times-circle"></i> Cancel Subscription</a>
                            </div>
                            <div class="d-flex justify-content-between mb-2 align-items-center">
                                Selected Package
                            </div>
                            <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                                <div class="banner-img">
                                    <a href="#"><img src="images/banner-1.jpg" alt="banner" class="htfix" /></a>
                                </div>
                                <div class="banner-content-3">
                                    <h2>Style Box</h2>
                                    <h5>4 items / Month</h5>
                                    <p>Subscription Start from 5000/month</p>
                                    <ul>
                                        <li>Rent 4 items at a time </li>
                                        <li>10,000+ styles from 400 + desginers </li>
                                        <li>Wear items with retail value with up to 500 </li>
                                        <li>Monthly shipments worth upto 2000</li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div> <hr>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <label for="">Preferances (Likes)</label>
                            <textarea rows="5" class="form-control mb-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure dignissimos corporis quam facilis quo asperiores laudantium vitae id aliquid nesciunt, reprehenderit, odit nulla recusandae, cupiditate. Obcaecati optio, minus eaque eum! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Debitis est quae fugit adipisci modi, odio ea repellat perspiciatis, rem, repudiandae nihil nam blanditiis doloribus corrupti eos iusto laudantium illo et.
                            </textarea>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <label for="">Preferances (Disikes)</label>
                            <textarea rows="5" class="form-control mb-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure dignissimos corporis quam facilis quo asperiores laudantium vitae id aliquid nesciunt, reprehenderit, odit nulla recusandae, cupiditate. Obcaecati optio, minus eaque eum! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Debitis est quae fugit adipisci modi, odio ea repellat perspiciatis, rem, repudiandae nihil nam blanditiis doloribus corrupti eos iusto laudantium illo et.
                            </textarea>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group mb-3">
                                <label for="">Height</label>
                                <select class="form-control">
                                    <option value="" selected>6.0"</option>
                                    <option value="">5.5"</option>
                                    <option value="">5.1"</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group mb-3">
                                <label for="">Weight</label>
                                <select class="form-control">
                                    <option value="">70</option>
                                    <option value="" selected>60"</option>
                                    <option value="">50</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 mt-2">
                            <div class="form-group">
                                <button type="submit" class="btn-submit"><i class="fal fa-save"></i> UPDATE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>