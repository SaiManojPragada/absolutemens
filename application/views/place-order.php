<div class="pkgsteps">
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
                <div class="whitebox orderconfirm" style="min-height: 430px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="text-center text-black-50">Place Order</h3>
                        </div>
                        <div class="col-lg-12">
                            <p><strong>PRICE DETAILS <small>(<?= $package_details->no_of_items ?> items)</small></strong></p>
                            <div class="d-flex justify-content-between mb-2">
                                <span>Total MRP</span>
                                <span><i class="fal fa-rupee-sign"></i> <?= $order_details->total_price ?></span>
                            </div>
                            <div class="d-flex justify-content-between mb-2">
                                <span>Discount on MRP</span>
                                <span class="text-success">- <i class="fal fa-rupee-sign"></i> <?= $order_details->discount ?></span>
                            </div>
                            <div class="d-flex justify-content-between mb-2">
                                <span>Coupon Discount</span>
                                <span class="text-danger"><a href="#" class="text-danger couponapply" id="couponAmount">
                                        <?php
                                        if (empty($order_details->coupon_code)) {
                                            echo 'Apply Coupon';
                                        } else {
                                            echo '- <i class="fal fa-rupee-sign"></i> ' . $order_details->coupon_discount;
                                        }
                                        ?></a></span>
                            </div>
                            <div class="d-flex mb-2 couponbox">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="coupon_code" placeholder="Enter Coupon Code" autocomplete="off" <?= empty($order_details->coupon_code) ? '' : 'disabled' ?> value="<?= $order_details->coupon_code ?>">
                                    <img src="<?= base_url('assets/images/linear-loader.gif') ?>" alt="alt" id="loader-div"/>
                                    <div class="input-group-append">

                                        <button class="btn btn-primary apply_coupon_btn" type="button" id="button-addon2" style="<?php
                                        if ($order_details->coupon_discount > 0) {
                                            echo "display: none;";
                                        }
                                        ?>" onclick="checkCoupon()">Apply</button>
                                        <button class="btn btn-danger remove_coupon_btn" type="button" id="button-addon2" style="<?php
                                        if ($order_details->coupon_discount < 1) {
                                            echo "display: none;";
                                        }
                                        ?>" onclick="removeCoupon()"><i class="fa fa-trash-alt"></i></button>
                                    </div>
                                </div>
                            </div>
                            <p><span id="couponErr" style="color: tomato"></span>
                                <span id="couponScc" style="color: green"></span></p>
                            <div class="d-flex justify-content-between mb-2">
                                <span>Convenience Fee </span>
                                <?php if ($package_details->delivery_charges == 0 && $order_details->delivery_charges == 0) { ?>
                                    <span><del><i class="fal fa-rupee-sign"></i> 99</del> <b class="text-success">FREE</b></span>
                                <?php } else { ?>
                                    <span><i class="fal fa-rupee-sign"></i> <?= $order_details->delivery_charges ?></span>
                                <?php } ?>
                            </div>
                            <hr>
                            <div class="d-flex justify-content-between mb-4">
                                <span><strong>Total Amount</strong></span>
                                <span><strong><i class="fal fa-rupee-sign"></i> <span id="grand_total"><?= $order_details->grand_total ?></span></strong></span>
                            </div>
                            <?php if ($package_details->delivery_charges == 0 && $order_details->delivery_charges == 0) { ?>
                                <div class="alert alert-success">
                                    <i class="fal fa-truck"></i> Yay! <span class="text-success">No Convenience Fee <del><i class="fal fa-rupee-sign"></i> 99</del> </span>on this order
                                </div>
                            <?php } ?>
                            <button type="submit" class="btn btn-primary btn-block" onclick="location.href = '<?= base_url() ?>payment/<?= $order_details->unq_id ?>'">PLACE ORDER</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<style>
    #loader-div{
        position: absolute;
        right: 90px;
        top: 10px;
        width: 50px;
        display: none;
    }
</style>
<script>

    function removeCoupon() {
        document.getElementById("loader-div").style.display = "block";
        console.log('<?= $order_details->unq_id ?>');
        $.ajax({
            url: '<?= base_url() ?>confirm_payment/remove_coupon',
            type: 'post',
            data: {unq_id: '<?= $order_details->unq_id ?>'},
            success: function (response) {
                response = jQuery.parseJSON(response);
                if (response['err_code'] === "invalid") {
                    $("#couponScc").html("");
                    $("#couponErr").html(response['message']);
                }
                if (response['err_code'] === "valid") {
                    console.log(response['data']);
                    $("#couponErr").html("");
                    $("#coupon_code").val("");
                    $("#couponScc").html(response['message']);
                    $("#grand_total").html(response['data']['grand_total']);
                    $("#couponAmount").html('');
                    $(".remove_coupon_btn").hide();
                    $(".apply_coupon_btn").show();
                    $("#coupon_code").prop('disabled', false);
                }

                document.getElementById("loader-div").style = "none";
            }
        });
    }


    function checkCoupon() {
        document.getElementById("loader-div").style.display = "block";
        var code = document.getElementById("coupon_code").value;
        if (code.length < 3) {
            $("#couponErr").html('Please Enter a Valid Coupon Code');
            document.getElementById("loader-div").style = "none";
            return true;
        }
        $.ajax({
            url: '<?= base_url() ?>confirm_payment/apply_coupon',
            type: 'post',
            data: {coupon_code: code, unq_id: '<?= $order_details->unq_id ?>'},
            success: function (response) {
                response = jQuery.parseJSON(response);
                if (response['err_code'] === "invalid") {
                    $("#couponScc").html("");
                    $("#couponErr").html(response['message']);
                }
                if (response['err_code'] === "valid") {
                    console.log(response['data']);
                    $("#couponErr").html("");
                    $("#coupon_code").prop('disabled', true);
                    $("#couponScc").html(response['message']);
                    $("#grand_total").html(response['data']['grand_total']);
                    $("#couponAmount").html('- <i class="fal fa-rupee-sign"></i> ' + response['data']['coupon_discount']);
                    $(".remove_coupon_btn").show();
                    $(".apply_coupon_btn").hide();
                }

                document.getElementById("loader-div").style = "none";
            }
        });

    }
</script>