<html>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>uploads/<?= $site_property->favicon ?>">
</html>
<div class="pkgsteps">
    <?= $this->load->view("includes/header", $main_data); ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <?php include 'menu-dashboard.php' ?> 
            </div>
            <div class="col-lg-9 col-md-12">
                <form method="post" action="<?= base_url() ?>sizes/update_sizes">
                    <div class="whitebox contentdashboard">
                        <h3>Sizes</h3>
                        <?=
                        $this->session->userdata("sizes_update_success");
                        $this->session->unset_userdata("sizes_update_success");
                        ?>
                        <?=
                        $this->session->userdata("sizes_update_error");
                        $this->session->unset_userdata("sizes_update_error");
                        ?>
                        <div class="row mt-4">

                            <?php
                            foreach ($categories as $c) {
                                ?>

                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group mb-3">
                                        <label for=""><?= $c->name ?> size</label>
                                        <select class="form-control" name="<?= $c->id ?>">
                                            <option value="">--select <?= $c->name ?> size--</option>
                                            <?php foreach ($c->sizes as $item) { ?>
                                                <option <?= $item->is_selected == 'yes' ? 'selected' : '' ?> value="<?= $item->id ?>" ><?= $item->title ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            <?php } ?>

                            <div class="col-lg-12 col-md-12">
                                <a href="#viewstyleModal" data-toggle="modal"><i class="fal fa-ruler-horizontal"></i> View Style Guide</a>
                            </div>
                            <div class="col-lg-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn-submit"><i class="fal fa-save"></i> UPDATE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>