<div class="pkgsteps">
	<?php include 'includes/header.php' ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-md-12">
				<div class="whitebox contentdashboard unsubscr">
					<h3>UnSubscribe</h3>
					 <hr>
					<form action="">
						<p><strong>I have chose to un-subscribe myself from the following:</strong></p>
					<div class="custom-control custom-radio mb-2">
					  <input type="radio" id="proemail" name="emailprom" class="custom-control-input">
					  <label class="custom-control-label" for="proemail">Promotional E-mails</label>
					</div>
					<p>We are sorry to find out that you have chosen not to receive any future promotional e-mails from HDFC Bank. To help us improve our services, we would appreciate if you would spare a few minutes and give us a reason for unsubscription.</p>
					<div class="custom-control custom-radio mb-1">
					  <input type="radio" id="r1" name="reasons" class="custom-control-input">
					  <label class="custom-control-label" for="r1">Irrelevant Products</label>
					</div>
					<div class="custom-control custom-radio mb-1">
					  <input type="radio" id="r2" name="reasons" class="custom-control-input">
					  <label class="custom-control-label" for="r2">Lot of mails being received from HDFC Bank</label>
					</div>
					<div class="custom-control custom-radio mb-1">
					  <input type="radio" id="r3" name="reasons" class="custom-control-input">
					  <label class="custom-control-label" for="r3">Lengthy e-mails</label>
					</div>
					<div class="custom-control custom-radio mb-1">
					  <input type="radio" id="r4" name="reasons" class="custom-control-input">
					  <label class="custom-control-label" for="r4">Content in the emails is not interesting</label>
					</div>
					<div class="custom-control custom-radio mb-1">
					  <input type="radio" id="r5" name="reasons" class="custom-control-input">
					  <label class="custom-control-label" for="r5">This is not my e-mail ID</label>
					</div>
					<div class="custom-control custom-radio mb-1">
					  <input type="radio" id="r6" name="reasons" class="custom-control-input">
					  <label class="custom-control-label" for="r6">Change of e-mail ID for HDFC Bank to contact me</label>
					</div>
					<div class="custom-control custom-radio mb-1">
					  <input type="radio" id="r7" name="reasons" class="custom-control-input">
					  <label class="custom-control-label" for="r7">Donot understand what is being said in the email</label>
					</div>
					<div class="custom-control custom-radio mb-1">
					  <input type="radio" id="r8" name="reasons" class="custom-control-input">
					  <label class="custom-control-label" for="r8">Other reasons</label>
					</div>
					<p>Please enter your Email address:</p>
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Please Enter Your email address">
							</div>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<button type="reset" class="btn btn-primary">Reset</button>
					</div>
					</form>
					<div class="alert alert-danger">
						Note : If you are an existing customer, you will continue to receive your account statements and other important advices and information relatina to transactions on your account.
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<?php include 'includes/footer.php' ?>