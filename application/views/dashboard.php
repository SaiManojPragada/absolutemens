<html>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>uploads/<?= $site_property->favicon ?>">
</html>
<div class="pkgsteps">
    <?= $this->load->view("includes/header", $main_data); ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <div class="container">

        <br><br>
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <?php include 'menu-dashboard.php' ?>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="whitebox contentdashboard">
                    <h3>My Profile</h3>	
                    <span class="success_message">
                        <?=
                        $this->session->userdata('update_profile_success');
                        $this->session->unset_userdata('update_profile_success')
                        ?>
                    </span>
                    <span class="failure_message">
                        <?=
                        $this->session->userdata('update_profile_error');
                        $this->session->userdata('update_profile_error')
                        ?>
                    </span>
                    <form method="POST" action="<?= base_url('dashboard/update_profile') ?>">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group mb-3">
                                    <label for="">Name*</label>
                                    <input type="text" name="name" class="form-control" value="<?= $user_data->name ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Email Address*</label>
                                    <input type="email" disabled name="email" class="form-control" value="<?= $user_data->email ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Phone Number*</label>
                                    <input type="text" name="mobile" class="form-control" value="<?= $user_data->mobile ?>" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group mb-3">
                                    <label for="">Address*</label>
                                    <input type="text" name="address" class="form-control" value="<?= $user_data->address ?>" placeholder="Enter your Address.." required>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group mb-3">
                                    <label for="">State*</label>
                                    <select class="form-control" name="states_id" id="states_list" onchange="getDistricts(this)" required>
                                        <option value="">--select state--</option>
                                        <?php foreach ($states as $item) { ?>
                                            <option value="<?= $item->id ?>" <?php
                                            if ($user_data->states_id == $item->id) {
                                                echo"selected";
                                            }
                                            ?>><?= $item->name ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group mb-3">
                                    <label for="">District*</label>
                                    <select class="form-control" name="districts_id" id="districts_list" onchange="getCities(this)" required>
                                        <option value="">--select district--</option>
                                        <?php foreach ($districts as $item) { ?>
                                            <option value="<?= $item->id ?>" <?php
                                            if ($user_data->districts_id == $item->id) {
                                                echo"selected";
                                            }
                                            ?>><?= $item->name ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group mb-3">
                                    <label for="">City*</label>
                                    <select class="form-control" name="cities_id" id="cities_list" required>
                                        <option value="">--select city--</option>
                                        <?php foreach ($cities as $item) { ?>
                                            <option value="<?= $item->id ?>" <?php
                                            if ($user_data->cities_id == $item->id) {
                                                echo"selected";
                                            }
                                            ?>><?= $item->name ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group mb-3">
                                    <label for="">Landmark</label>
                                    <input type="text" name="landmark" class="form-control" value="<?= $user_data->landmark ?>" placeholder="Enter Landmark">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group mb-3">
                                    <label for="">Street Name</label>
                                    <input type="text" name="street_name" class="form-control" value="<?= $user_data->street_name ?>" placeholder="Enter Street Name">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group mb-3">
                                    <label for="">Pincode*</label>
                                    <input type="text" name="pincode" class="form-control" value="<?= $user_data->pincode ?>" placeholder="Enter Pincode" required>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 mt-2">
                                <div class="form-group">
                                    <button type="submit" class="btn-submit" name="update"><i class="fal fa-save"></i> UPDATE</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<style>
    .success_message, .failure_message{
        margin-left: 10px;
        margin-bottom: 10px;
    }
    .success_message{
        color: green;
    }
    .failure_message{
        color: tomato;
    }
</style>
<script>
    function getDistricts(eve) {
        $.ajax({
            url: "<?= base_url('dashboard/get_districts') ?>",
            type: "POST",
            data: {state_id: eve.value},
            success: function (resp) {
                $("#districts_list").html(resp);
                $("#cities_list").html("");
            }
        });
    }
    function getCities(eve) {
        $.ajax({
            url: "<?= base_url('dashboard/get_cities') ?>",
            type: "POST",
            data: {districts_id: eve.value},
            success: function (resp) {
                $("#cities_list").html(resp);
            }
        });
    }
</script>