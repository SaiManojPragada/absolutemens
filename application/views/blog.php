<div class="cms-subpage">
    <h4>BLOGS</h4>
</div>
<div class="blog-area ptb-80">
    <div class="container">
        <div class="row">
            <?php foreach ($blogs as $item) { ?>
                <div class="col-lg-4 col-md-4 mb-3">
                    <div class="single-blog">
                        <div class="blog-img">
                            <a href="blog-view/<?= $item->slug ?>"><img src="<?= base_url() ?>uploads/<?= $item->image ?>" alt="blog" /></a>
                            <div class="date">
                                <?= date('M', $item->created_at) ?> <span><?= date('d', $item->created_at) ?></span>
                            </div>
                        </div>
                        <div class="blog-content pt-20">
                            <h3><a href="blog-view/<?= $item->slug ?>"><?= $item->title ?></a></h3>
                            <?= $item->short_description ?>
                            <a href="blog-view/<?= $item->slug ?>">Read more ...</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!--            <div class="col-lg-4 col-md-4 mb-3">
                            <div class="single-blog">
                                <div class="blog-img">
                                    <a href="#"><img src="images/blog-1.jpg" alt="blog" /></a>
                                    <div class="date">
                                        Aug <span>09</span>
                                    </div>
                                </div>
                                <div class="blog-content pt-20">
                                    <h3><a href="blog-view.php">Aypi non habent claritatem habent claritatem  insitam.</a></h3>
                                    <p>Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.</p>
                                    <a href="blog-view.php">Read more ...</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-3">
                            <div class="single-blog">
                                <div class="blog-img">
                                    <a href="blog-view.php"><img src="images/blog-2.jpg" alt="blog" /></a>
                                    <div class="date">
                                        Aug <span>09</span>
                                    </div>
                                </div>
                                <div class="blog-content pt-20">
                                    <h3><a href="blog-view.php">Bypi non habent claritatem  habent claritate insitam.</a></h3>
                                    <p>Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.</p>
                                    <a href="blog-view.php">Read more ...</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-3">
                            <div class="single-blog">
                                <div class="blog-img">
                                    <a href="blog-view.php"><img src="images/blog-3.jpg" alt="blog" /></a>
                                    <div class="date">
                                        Aug <span>09</span>
                                    </div>
                                </div>
                                <div class="blog-content pt-20">
                                    <h3><a href="blog-view.php">Cypi non habent claritatem habent clari insitam.</a></h3>
                                    <p>Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.</p>
                                    <a href="blog-view.php">Read more ...</a>
                                </div>
                            </div>
                        </div>
            
                        <div class="col-lg-4 col-md-4 mb-3">
                            <div class="single-blog">
                                <div class="blog-img">
                                    <a href="#"><img src="images/blog-1.jpg" alt="blog" /></a>
                                    <div class="date">
                                        Aug <span>09</span>
                                    </div>
                                </div>
                                <div class="blog-content pt-20">
                                    <h3><a href="blog-view.php">Aypi non habent claritatem habent claritatem  insitam.</a></h3>
                                    <p>Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.</p>
                                    <a href="#">Read more ...</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-3">
                            <div class="single-blog">
                                <div class="blog-img">
                                    <a href="blog-view.php"><img src="images/blog-2.jpg" alt="blog" /></a>
                                    <div class="date">
                                        Aug <span>09</span>
                                    </div>
                                </div>
                                <div class="blog-content pt-20">
                                    <h3><a href="#">Bypi non habent claritatem  habent claritate insitam.</a></h3>
                                    <p>Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.</p>
                                    <a href="blog-view.php">Read more ...</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 mb-3">
                            <div class="single-blog">
                                <div class="blog-img">
                                    <a href="blog-view.php"><img src="images/blog-3.jpg" alt="blog" /></a>
                                    <div class="date">
                                        Aug <span>09</span>
                                    </div>
                                </div>
                                <div class="blog-content pt-20">
                                    <h3><a href="blog-view.php">Cypi non habent claritatem habent clari insitam.</a></h3>
                                    <p>Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.</p>
                                    <a href="blog-view.php">Read more ...</a>
                                </div>
                            </div>
                        </div>-->

        </div>
    </div>
</div>