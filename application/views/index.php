<!-- slider-area-start -->
<div class="slider-area">
    <div id="slider">
        <?php
        $count = 1;
        foreach ($sliders as $slide) {
            ?>
            <img src="<?= base_url() ?>uploads/<?= $slide->image ?>" alt="slider-img" title="#caption<?= $count++ ?>" />
        <?php } ?>
<!--        <img src="assets/images/slider-1.jpg" alt="slider-img" title="#caption1" />
<img src="assets/images/slider-2.jpg" alt="slider-img" title="#caption2" />
<img src="assets/images/slider-3.jpg" alt="slider-img" title="#caption3" />-->
    </div>
    <?php
    $count = 1;
    foreach ($sliders as $slide) {
        ?>
        <div class="nivo-html-caption" id="caption<?= $count++ ?>" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider-text">
                            <h3 class="wow fadeInLeft" data-wow-delay=".3s"><?= $slide->title ?></h3>
                            <p class="wow fadeInLeft" data-wow-delay="1.3s"><?= $slide->description ?></p>
                            <a href="<?= $slide->button_link ?>" class=" wow bounceInRight" data-wow-delay="1.5s"><?= $slide->button_title ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!--    <div class="nivo-html-caption" id="caption1" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider-text">
                            <h3 class="wow fadeInLeft" data-wow-delay=".3s">New Arrivals</h3>
                            <h3 class="wow fadeInLeft" data-wow-delay=".5s">Spring Collection</h3>
                            <p class="wow fadeInLeft" data-wow-delay="1.3s">Lorem ipsum dolor sit amet consectetur adipisicing elit. <br>Quas quis l</p>
                            <a href="about-us.php" class=" wow bounceInRight" data-wow-delay="1.5s">About Us</a>
                        </div>
                    </div> col 
                </div> row 
            </div> container 
        </div>
        <div class="nivo-html-caption" id="caption2" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider-text">
                            <h3 class="wow fadeInLeft" data-wow-delay=".3s">Simplify</h3>
                            <h3 class="wow fadeInLeft" data-wow-delay=".5s">Everything</h3>
                            <p class="wow fadeInLeft" data-wow-delay="1.3s">Typi non habent claritatem insitam est usus legentis in iis qui facit eorum <br /> claritatem..</p>
                            <a href="weshop-details.php" class=" wow bounceInRight" data-wow-delay="1.5s">Shop Now</a>
                        </div>
                    </div> col 
                </div> row 
            </div> container 
        </div>
        <div class="nivo-html-caption" id="caption3" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider-text">
                            <h3 class="wow fadeInLeft" data-wow-delay=".3s">Best Choice</h3>
                            <h3 class="wow fadeInLeft" data-wow-delay=".5s">Collection</h3>
                            <p class="wow fadeInLeft" data-wow-delay="1.3s">Discover the collection as styled by fashion icon Caroline <br>Issa in our new season  campaign.</p>
                            <a href="package-details.php" class=" wow bounceInRight" data-wow-delay="1.5s">read more</a>
                        </div>
                    </div> col 
                </div> row 
            </div> container 
        </div>-->
</div>
<!-- slider-area-end -->
<!-- banner-area-start -->
<!-- banner-area-start -->
<div class="banner-area ptb-50">
    <div class="container">
        <div class="col-12">
            <a class="btn btn-primary mb-3" href="<?= base_url('packages') ?>" style="float: right;">View More <i class="fa fa-angle-right"></i></a>
        </div>
        <div class="row justify-content-center col-12">
            <?php foreach ($packages as $item) { ?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-10">
                    <!-- single-banner-start -->
                    <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                        <div class="banner-img">
                            <a href="#"><img src="<?= base_url() ?>uploads/<?= $item->image ?>" alt="banner" /></a>
                        </div>
                        <div class="banner-content-3">
                            <?= $item->title ?>
                            <p><?= $item->short_description ?></p>
                            <?php if ($this->session->userdata("user_unq_id")) { ?>
                                <a href="package-steps/step-one/<?= $item->slug ?>"><i class="fal fa-chevron-circle-right"></i> Subscribe Now</a>
                            <?php } else { ?>
                                <a href="#loginModal" data-toggle="modal"><i class="fal fa-chevron-circle-right"></i> Subscribe Now</a>
                            <?php } ?>
                            <a href="package-details/<?= $item->slug ?>" class="btn-blue"><i class="fal fa-chevron-circle-right"></i> Know More</a>
                        </div>
                    </div>
                    <!-- single-banner-end -->
                </div>
            <?php } ?>


            <!--            <div class="col-xl-4 col-lg-4 col-md-6 col-10">
                             single-banner-start 
                            <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                                <div class="banner-img">
                                    <a href="package-step-1.php"><img src="<?= base_url() ?>assets/images/banner-1.jpg" alt="banner" /></a>
                                </div>
                                <div class="banner-content-3">
                                    <h2>Package One</h2>
                                    <h2><span>Collection</span></h2>
                                    <p>Lorem ipsum dolor, sit amet consectetur dolor, sit amet  adipisicing elit. Neque adipisicing sit ipsum odio magnam</p>
                                    <a href="package-step-1.php"><i class="fal fa-chevron-circle-right"></i> Subscribe Now</a>
                                    <a href="package-details.php" class="btn-blue"><i class="fal fa-chevron-circle-right"></i> Know More</a>
                                </div>
                            </div>
                             single-banner-end 
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-10">
                             single-banner-start 
                            <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                                <div class="banner-img">
                                    <a href="package-step-1.php"><img src="<?= base_url() ?>assets/images/banner-2.jpg" alt="banner" /></a>
                                </div>
                                <div class="banner-content-3">
                                    <h2>Package Two</h2>
                                    <h2><span>Collection</span></h2>
                                    <p>Lorem ipsum dolor, sit amet consectetur dolor, sit amet  adipisicing elit. Neque adipisicing sit ipsum odio magnam</p>
                                    <a href="package-step-1.php"><i class="fal fa-chevron-circle-right"></i> Subscribe Now</a>
                                    <a href="package-details.php" class="btn-blue"><i class="fal fa-chevron-circle-right"></i> Know More</a>
                                </div>
                            </div>
                             single-banner-end 
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-6 col-10">
                             single-banner-start 
                            <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                                <div class="banner-img">
                                    <a href="package-step-1.php"><img src="<?= base_url() ?>assets/images/banner-3.jpg" alt="banner" /></a>
                                </div>
                                <div class="banner-content-3">
                                    <h2>Package Three</h2>
                                    <h2><span>Collection</span></h2>
                                    <p>Lorem ipsum dolor, sit amet consectetur dolor, sit amet  adipisicing elit. Neque adipisicing sit ipsum odio magnam</p>
                                    <a href="package-step-1.php"><i class="fal fa-chevron-circle-right"></i> Subscribe Now</a>
                                    <a href="package-details.php" class="btn-blue"><i class="fal fa-chevron-circle-right"></i> Know More</a>
                                </div>
                            </div>
                             single-banner-end 
                        </div>-->


            <?php
            $count = 0;
            foreach ($banners as $item) {
                if ($count == 0) {
                    ?>
                    <div class="col-lg-12 col-md-12 mt-40 mt-sm-20 text-center">
                        <a href="<?= $item->link ?>"><img src="<?= base_url() ?>uploads/<?= $item->image ?>" srcset="<?= base_url() ?>uploads/<?= $item->image ?> , <?= base_url() ?>uploads/<?= $item->image ?> 768w" alt="" class="img-fluid"></a>
                        <div class="text-center">
                            <a href="weshop-details" class="btn btn-primary mt-3 px-5">
                                <i class="fal fa-arrow-circle-right"></i>
                                <strong>We Shop For You</strong>
                                <br> Know More </a>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="col-lg-12 col-md-12 mt-4">
                        <a href="<?= $item->link ?>"><img src="<?= base_url() ?>uploads/<?= $item->image ?>" alt="" class="img-fluid"></a>
                    </div>
                    <?php
                }$count++;
            }
            ?>




            <!--            <div class="col-lg-12 col-md-12 mt-40 mt-sm-20 text-center">
                            <a href="we-shop-for-you-step-1.php"><img src="<?= base_url() ?>assets/images/weshop.gif" srcset="<?= base_url() ?>assets/images/weshop.gif , <?= base_url() ?>assets/images/weshop-mobile.gif 768w" alt="" class="img-fluid"></a>
                            <div class="text-center">
                                <a href="weshop-details.php" class="btn btn-primary mt-3 px-5">
                                    <i class="fal fa-arrow-circle-right"></i>
                                    <strong>We Shop For You</strong>
                                    <br> Know More </a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 mt-4">
                            <a href="#"><img src="<?= base_url() ?>assets/images/comingsoon.jpg" alt="" class="img-fluid"></a>
                        </div>-->
        </div>

    </div>
</div>
<!-- banner-area-end -->
<div class="how-it-works" style="background-image: url(<?= base_url() ?>uploads/<?= $how_it_works_images->bg_image ?>)">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-12 align-self-center order-lg-0 order-md-1">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <?= $how_it_works_images->title ?>
                    </div>
                </div>
                <div class="drk-blue">
                    <div class="row">
                        <?php
                        $count = 1;
                        foreach ($how_it_works_points as $point) {
                            ?>
                            <div class="col-lg-6 col-md-6">
                                <div class="media">
                                    <span class="fal fa-gift"></span>
                                    <div class="media-body">
                                        <?= $point->title ?>
                                        <?= $point->description ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <!--                        <div class="col-lg-6 col-md-6">
                                                    <div class="media">
                                                        <span class="fal fa-user"></span>
                                                        <div class="media-body">
                                                            <h5 class="mt-0"><b>01</b>  Create your profile</h5>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="media">
                                                        <span class="fal fa-gift"></span>
                                                        <div class="media-body">
                                                            <h5 class="mt-0"><b>02</b>  Select your package</h5>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="media">
                                                        <span class="fal fa-list"></span>
                                                        <div class="media-body">
                                                            <h5 class="mt-0"><b>03</b>  Fill the details</h5>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="media">
                                                        <span class="fal fa-thumbs-up"></span>
                                                        <div class="media-body">
                                                            <h5 class="mt-0"><b>04</b> Lets enjoy!!</h5>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio error cumque, quidem harum iure aperiam itaque ipsa provident.</p>
                                                        </div>
                                                    </div>
                                                </div>-->
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 order-lg-1 order-md-0"><img src="<?= base_url() ?>uploads/<?= $how_it_works_images->image ?>" alt="" class="img-fluid"></div>
        </div>
    </div>
</div>

<section class="pt-5 pb-3">
    <div class="container">
        <div class="row">
            <div class="<?php
            if (!empty($about_us->video_url)) {
                echo 'col-lg-5';
            } else {
                echo 'col-lg-12';
            }
            ?> col-md-12 align-self-center">
                <h2 class="text-center mb-3"><?= $about_us->title ?></h2>
<!--                <p class="pb-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure nostrum numquam, distinctio corrupti nam optio fuga, molestiae natus facilis culpa accusantium molestias, excepturi! Atque libero accusamus inventore eaque. Neque, eum? Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure nostrum numquam, distinctio corrupti nam optio fuga, molestiae natus facilis culpa accusantium molestias, excepturi! Atque libero accusamus inventore eaque. Neque, eum?</p>-->
                <?= $about_us->short_description ?>
            </div>
            <?php if (!empty($about_us->video_url)) { ?>
                <div class="col-lg-7 col-md-12">
                    <div class="videobox">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="<?= $about_us->video_url ?>" allowfullscreen></iframe>
                        </div>							
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<!-- testimonial-area-start -->
<div class="testimonial-area bg ptb-80">
    <div class="container">
        <div class="testimonial-active owl-carousel">
            <?php foreach ($testimonials as $item) { ?>
                <div class="single-testimonial text-center">
                    <div class="testimonial-img">
                        <a href="#"><img src="<?= base_url() ?>uploads/<?= $item->image ?>" alt="" class="rounded-circle" /></a>
                    </div>
                    <div class="testimonial-content">
                        <?= $item->description ?>
                        <i class="fa fa-quote-right"></i>
                        <h4><?= $item->name ?></h4>
                    </div>
                </div>
            <?php } ?>
            <!--            <div class="single-testimonial text-center">
                            <div class="testimonial-img">
                                <a href="#"><img src="<?= base_url() ?>assets/images/testi-1.jpg" alt="" class="rounded-circle" /></a>
                            </div>
                            <div class="testimonial-content">
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ullam nam laboriosam atque qui molestiae nisi voluptatibus consequuntur iusto, labore voluptate fugit praesentium repudiandae voluptates esse dolorum fuga neque earum ab!</p>
                                <i class="fa fa-quote-right"></i>
                                <h4>Title Here</h4>
                            </div>
                        </div>
                        <div class="single-testimonial text-center">
                            <div class="testimonial-img">
                                <a href="#"><img src="<?= base_url() ?>assets/images/testi-1.jpg" alt="" class="rounded-circle"/></a>
                            </div>
                            <div class="testimonial-content">
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ullam nam laboriosam atque qui molestiae nisi voluptatibus consequuntur iusto, labore voluptate fugit praesentium repudiandae voluptates esse dolorum fuga neque earum ab!</p>
                                <i class="fa fa-quote-right"></i>
                                <h4>Title Here</h4>
                            </div>
                        </div>
                        <div class="single-testimonial text-center">
                            <div class="testimonial-img">
                                <a href="#"><img src="<?= base_url() ?>assets/images/testi-1.jpg" alt="" class="rounded-circle"/></a>
                            </div>
                            <div class="testimonial-content">
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ullam nam laboriosam atque qui molestiae nisi voluptatibus consequuntur iusto, labore voluptate fugit praesentium repudiandae voluptates esse dolorum fuga neque earum ab!</p>
                                <i class="fa fa-quote-right"></i>
                                <h4>Title Here</h4>
                            </div>
                        </div>
                        <div class="single-testimonial text-center">
                            <div class="testimonial-img">
                                <a href="#"><img src="<?= base_url() ?>assets/images/testi-1.jpg" alt="" class="rounded-circle"/></a>
                            </div>
                            <div class="testimonial-content">
                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ullam nam laboriosam atque qui molestiae nisi voluptatibus consequuntur iusto, labore voluptate fugit praesentium repudiandae voluptates esse dolorum fuga neque earum ab!</p>
                                <i class="fa fa-quote-right"></i>
                                <h4>Title Here</h4>
                            </div>
                        </div>-->
        </div>
    </div>
</div>
<!-- testimonial-area-end -->
<!-- blog-area-start -->
<div class="blog-area pt-50 pb-40">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title mb-30 text-center">
                    <h2><?= $blog_info->title ?></h2>
                    <?= $blog_info->sub_title ?>
                </div>
            </div>
        </div>
        <div class="blog-active owl-carousel">
            <!-- single-blog-start -->
            <?php foreach ($blogs as $item) { ?>
                <div class="single-blog">
                    <div class="blog-img">
                        <a href="blog-view/<?= $item->slug ?>"><img src="<?= base_url() ?>uploads/<?= $item->image ?>" alt="blog" /></a>
                        <div class="date">
                            <?= date('M', $item->created_at) ?> <span><?= date('d', $item->created_at) ?></span>
                        </div>
                    </div>
                    <div class="blog-content pt-20">
                        <h3><a href="blog-view/<?= $item->slug ?>"><?= $item->title ?></a></h3>
                        <?= $item->short_description ?>
                        <a href="blog-view/<?= $item->slug ?>">Read more ...</a>
                    </div>
                </div>
            <?php } ?>
            <!--            <div class="single-blog">
                            <div class="blog-img">
                                <a href="blog-view.php"><img src="<?= base_url() ?>assets/images/blog-1.jpg" alt="blog" /></a>
                                <div class="date">
                                    Aug <span>09</span>
                                </div>
                            </div>
                            <div class="blog-content pt-20">
                                <h3><a href="blog-view.php">Aypi non habent claritatem habent claritatem  insitam.</a></h3>
                                <p>Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.</p>
                                <a href="blog-view.php">Read more ...</a>
                            </div>
                        </div>
                         single-blog-end 
                         single-blog-start 
                        <div class="single-blog">
                            <div class="blog-img">
                                <a href="blog-view.php"><img src="<?= base_url() ?>assets/images/blog-2.jpg" alt="blog" /></a>
                                <div class="date">
                                    Aug <span>09</span>
                                </div>
                            </div>
                            <div class="blog-content pt-20">
                                <h3><a href="blog-view.php">Bypi non habent claritatem  habent claritate insitam.</a></h3>
                                <p>Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.</p>
                                <a href="blog-view.php">Read more ...</a>
                            </div>
                        </div>
                         single-blog-end 
                         single-blog-start 
                        <div class="single-blog">
                            <div class="blog-img">
                                <a href="blog-view.php"><img src="<?= base_url() ?>assets/images/blog-3.jpg" alt="blog" /></a>
                            <div class="date">
                                Aug <span>09</span>
                            </div>
                        </div>
                        <div class="blog-content pt-20">
                            <h3><a href="blog-view.php">Cypi non habent claritatem habent clari insitam.</a></h3>
                            <p>Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.Aypi non habent claritatem  insitam. Aypi non habent claritatem  insitam.</p>
                            <a href="blog-view.php">Read more ...</a>
                        </div>
                    </div>-->
            <!-- single-blog-end -->
        </div>
        <div class="row my-5">
            <div class="col-lg-12 col-md-12 text-center">
                <a href="blogs" class="btn-blog">View Blogs</a>
            </div>
        </div>
    </div>
</div>
<!-- blog-area-end -->