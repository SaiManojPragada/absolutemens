<div class="cms-subpage">
    <h4><?= $data->title ?></h4>
</div>
<div class="container pt-5">
    <div class="row justify-content-center cmsblock">
        <div class="col-lg-12">
            <?= $data->description ?>
        </div>
    </div>
</div>