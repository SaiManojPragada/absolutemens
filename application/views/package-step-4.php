<div class="pkgsteps">
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12">
                <div class="whitebox">
                    <form class="new-added-form" method="POST" enctype="multipart/form-data" action="<?= base_url('package_steps/submit_form/' . $current_package . '/checkout') ?>">
                        <section>
                            <input id="unq_id" type="hidden" name="unq_id" value="<?= $random_unq_key ?>">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <p><strong>Shipping Address</strong></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="Name*" name="address_name" required value="<?= $user_data->name ?>">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="Email Address*" name="address_email" required value="<?= $user_data->email ?>">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group mb-3">
                                        <input type="number" class="form-control" min="0" placeholder="Phone Number*" name="phone_number" required value="<?= $user_data->mobile ?>">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group mb-3">
                                        <input type="number" class="form-control" min="0" placeholder="Alternate Phone Number (Optional)" name="alternative_phone_number" value="<?= $user_data->alternative_phone_number ?>">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="Address" name="address" required value="<?= $user_data->address ?>">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group mb-3">
                                        <select class="form-control" name="state" id="states_list" onchange="getDistricts(this)" required>
                                            <option value="">--Select State--</option>
                                            <?php foreach ($states as $item) { ?>
                                                <option value="<?= $item->id ?>" <?php
                                                if ($user_data->states_id === $item->id) {
                                                    echo"selected";
                                                }
                                                ?>><?= $item->name ?></option>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group mb-3">
                                        <select class="form-control" name="district" id="districts_list" onchange="getCities(this)" required>
                                            <?php foreach ($districts as $item) { ?>
                                                <option value="<?= $item->id ?>" <?php
                                                if ($user_data->districts_id === $item->id) {
                                                    echo"selected";
                                                }
                                                ?>><?= $item->name ?></option>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group mb-3">
                                        <select class="form-control" name="city" id="cities_list" required>
                                            <option value="">--select city--</option>
                                            <?php foreach ($cities as $item) { ?>
                                                <option value="<?= $item->id ?>" <?php
                                                if ($user_data->cities_id === $item->id) {
                                                    echo"selected";
                                                }
                                                ?>><?= $item->name ?></option>
                                                    <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="Landmark" name="landmark" required value="<?= $user_data->landmark ?>">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group mb-3">
                                        <input type="text" class="form-control" placeholder="Street Name" name="street_name" required value="<?= $user_data->street_name ?>">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group mb-3">
                                        <input type="number" class="form-control" min="0" placeholder="Pincode" name="pincode" required value="<?= $user_data->pincode ?>">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" <?php
                                        if ($user_data->is_billing_address_checked) {
                                            echo "checked";
                                        }
                                        ?> name="is_billing_address_checked" id="defaultCheck1">
                                        <label class="form-check-label" for="defaultCheck1">
                                            Make this as billing address
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 mt-5">
                                    <div class="d-flex justify-content-between">
                                        <button type="button" class="btn-previous" onclick="location.href = '<?= base_url() ?>package-steps/step-three/<?= $current_package ?>/<?= $random_unq_key ?>'">PREVIOUS</button>
                                        <button type="submit" class="btn-next">CONTINUE TO CHECKOUT</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
            <?php if ($current_package !== "we-shop-for-you") { ?>
                <div class="col-lg-4 col-md-8">
                    <div class="d-flex justify-content-between mb-2 align-items-center">
                        Selected Package
                        <a href="<?= base_url('packages') ?>" class="btn btn-sm btn-primary"><i class="fal fa-sync"></i> Change Package</a>
                    </div>
                    <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                        <div class="banner-img">
                            <a href="#"><img src="<?= base_url() ?>uploads/<?= $package_details->image ?>" alt="banner" /></a>
                        </div>
                        <div class="banner-content-3">
                            <?= $package_details->title ?>
                            <h5><?= $package_details->no_of_items ?> items / <?= $package_details->duration_in_words ?></h5>
                            <p>Subscription Start from <?= (int) $package_details->price ?>/month</p>
                            <?= $package_details->subscription_features ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="best-sell-active owl-carousel owl-theme">
                    <?php foreach ($package_banners as $banner) { ?>
                        <div class="item"><a href="#"><img src="<?= base_url() ?>uploads/<?= $banner->image ?>" alt=""></a></div>
                            <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        function getDistricts(eve) {
            $.ajax({
                url: "<?= base_url('dashboard/get_districts') ?>",
                type: "POST",
                data: {state_id: eve.value},
                success: function (resp) {
                    console.log(resp);
                    $("#districts_list").html(resp);
                    $("#cities_list").html("");
                }
            });
        }
        function getCities(eve) {
            $.ajax({
                url: "<?= base_url('dashboard/get_cities') ?>",
                type: "POST",
                data: {districts_id: eve.value},
                success: function (resp) {
                    $("#cities_list").html(resp);
                }
            });
        }
    </script>