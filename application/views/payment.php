</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="whitebox orderconfirm">

                    <h2>Do Not Refresh or Close this Page</h2>
                    <br>
                    <p>Please Wait While we Redirect You....</p>


                </div>
            </div>
        </div>

    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
    var options = {
        "key": "<?= RAZORPAY_KEY ?>", // Enter the Key ID generated from the Dashboard
        "amount": '<?= $order_details->grand_total ?>', // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
        "currency": "INR",
        "name": "<?= SITE_TITLE ?>",
        "description": 'Package',
        "image": "<?= base_url() ?>uploads/<?= SITE_LOGO ?>",
                "order_id": "<?= $order->id ?>", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                "handler": function (response) {

                    var r_p_id = response.razorpay_payment_id;
                    var r_o_id = response.razorpay_order_id;
                    var r_signature = response.razorpay_signature;
                    var ref_id = "<?= $refId ?>";
                    $.ajax({
                        url: "<?= base_url() ?>confirm_payment/success_up",
                        type: "POST",
                        data: {razorpay_payment_id: r_p_id, razorpay_order_id: r_o_id, razorpay_signature: r_signature, ref_id: ref_id, amount_paid: <?= $order_details->grand_total ?>, status: 1},
                        success: function (resp) {
                            console.log(resp);
                            if (resp) {
                                location.href = "<?= base_url() ?>payment/okPayment/" + ref_id;
                            } else {
                                location.href = "<?= base_url() ?>payment/noPayment/" + ref_id;
                            }
                        }
                    });
                },
                "modal": {
                    ondismiss: function () {
                        location.href = "<?= base_url() ?>payment/noPayment/<?= $refId ?>";
                                    }
                                },
                                "prefill": {
                                    "name": "<?= $user_data->name ?>",
                                    "email": "<?= $user_data->email ?>",
                                    "contact": "<?= $user_data->mobile ?>"
                                },
                                "notes": {
                                    "address": "Paying to <?= SITE_TITLE ?>"
                                },
                                "theme": {
                                    "color": "#3399cc"
                                }
                            };
                            var rzp1 = new Razorpay(options);
                            rzp1.on('payment.failed', function (response) {
                                location.href = "<?= base_url() ?>/payment/noPayment/" + ref_id;
                            });

                            $(document).ready(function () {
                                rzp1.open();
                            });
</script>