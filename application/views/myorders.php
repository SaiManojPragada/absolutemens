<html>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>uploads/<?= $site_property->favicon ?>">
</html>
<div class="pkgsteps">
    <?= $this->load->view("includes/header", $main_data); ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <?php $this->load->view('menu-dashboard'); ?>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="whitebox contentdashboard">
                    <h3>My Orders</h3>
                    <?php
                    $count = 0;
                    foreach ($orders as $item) {
                        ?>
                        <div class="row mt-4">
                            <?php if (!$item->is_we_shop_for_you) { ?>
                                <div class="col-lg-3 col-md-4">
                                    <img src="<?= base_url('uploads/' . $item->package->image) ?>" alt="" class="product-img mb-3">
                                </div>
                            <?php } ?>
                            <div class="<?= (!$item->is_we_shop_for_you) ? 'col-lg-9 col-md-8' : 'col-12' ?> order-box">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4"><h5><?= (!$item->is_we_shop_for_you) ? strip_tags($item->package->title) : 'We Shop For You Order' ?></h5></div>

                                    <div class="col-lg-2 col-md-2"><?php if ($item->is_exchanged) { ?><span class="badge bg-warning">Exchange</span><?php } ?></div>

                                    <div class="col-lg-6 col-md-6"><p class="fz-12">Order ID : <?= $item->unq_id ?><br><a href="<?= base_url('dashboard/my-orders/' . $item->unq_id) ?>" style="font-size:15px">View Order <i class="fa fa-angle-right"></i></a></p></div>
                                </div>
                                <div class="row justify-content-between">
                                    <div class="col-lg-4 col-md-4"><p class="price"><i class="fal fa-rupee-sign"></i> <?= $item->grand_total ?>/-</p></div>


                                    <?php if ($item->order_status == "Delivered") { ?>
                                        <div class="col-lg-3 col-md-4">
                                            <a href="#feedbackModal" onclick="feedbackModal('<?= $item->id ?>', '<?= $item->unq_id ?>', '<?= $item->feedback->rating ?>', '<?= $item->feedback->feedback_message ?>')" data-toggle="modal" class="btn btn-primary btn-sm">Feedback</a>

                                        </div>
                                        <div class="col-lg-12 col-md-12">
                                            <p><span>Delivered on : </span><?= date('d ', $item->delivered_date) . date('M Y', $item->delivered_date) ?></p>
                                        </div>
                                        <?php if ($item->expiry_date > time()) { ?>
                                            <div class="col-12" style="padding-right: 30px">
                                                <a href="#returnOrderModal"  data-toggle="modal" onclick="setOrderIdForExchange('<?= $item->id ?>', '<?= $item->unq_id ?>')" class="btn btn-primary btn-sm" style="float: right">Order Exchange</a>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php if ($item->is_exchanged && $item->order_status != 'Delivered') { ?>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12">
                                            <ul class="step d-flex flex-nowrap">
                                                <li class="step-item <?php
                                                if ($item->order_status == "Exchange Requested" || $item->order_status == "Order Placed" || $item->order_status == "Assigned to Fashion Analyst" || $item->order_status == "Selected Items Under Review" || $item->order_status == "Selections Confirmed" || $item->order_status == "Rejected") {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a href="javascript: void(0);" class="">Exchange Ordered <br><?= date('d/m/Y', $item->created_at) ?></a>
                                                </li>
                                                <li class="step-item <?php
                                                if ($item->order_status == "Dispatched") {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a href="javascript: void(0);" class="">Dispatched <br><?php
                                                        if (!empty($item->dispatched_date)) {
                                                            echo date('d/m/Y', $item->dispatched_date);
                                                        }
                                                        ?></a>
                                                </li>
                                                <li class="step-item <?php
                                                if ($item->order_status == "Shipped") {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a href="javascript: void(0);" class="">Shipped <br><?php
                                                        if (!empty($item->shipped_date)) {
                                                            echo date('d/m/Y', $item->shipped_date);
                                                        }
                                                        ?></a>
                                                </li>
                                                <li class="step-item <?php
                                                if ($item->order_status == "Out For Delivery") {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a href="javascript: void(0);" class="">Estimated Delivery <br><?php
                                                        if (!empty($item->estimated_delivery_date)) {
                                                            echo date('d/m/Y', $item->estimated_delivery_date);
                                                        } else {
                                                            echo date('d/m/Y', strtotime(date('Y-m-d H:i:s', $item->created_at) . '+ 10 days'));
                                                        }
                                                        ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <?php } else if ($item->order_status != "Delivered" && $item->order_status != "Returned" && $item->order_status != "Cancelled" && $item->order_status != "Cancellation Requested") { ?>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12">
                                            <ul class="step d-flex flex-nowrap">
                                                <li class="step-item <?php
                                                if ($item->order_status == "Order Placed" || $item->order_status == "Assigned to Fashion Analyst" || $item->order_status == "Selected Items Under Review" || $item->order_status == "Selections Confirmed" || $item->order_status == "Rejected") {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a href="javascript: void(0);" class="">Ordered <br><?= date('d/m/Y', $item->created_at) ?></a>
                                                </li>
                                                <li class="step-item <?php
                                                if ($item->order_status == "Dispatched") {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a href="javascript: void(0);" class="">Dispatched <br><?php
                                                        if (!empty($item->dispatched_date)) {
                                                            echo date('d/m/Y', $item->dispatched_date);
                                                        }
                                                        ?></a>
                                                </li>
                                                <li class="step-item <?php
                                                if ($item->order_status == "Shipped") {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a href="javascript: void(0);" class="">Shipped <br><?php
                                                        if (!empty($item->shipped_date)) {
                                                            echo date('d/m/Y', $item->shipped_date);
                                                        }
                                                        ?></a>
                                                </li>
                                                <li class="step-item <?php
                                                if ($item->order_status == "Out For Delivery") {
                                                    echo "active";
                                                }
                                                ?>">
                                                    <a href="javascript: void(0);" class="">Estimated Delivery <br><?php
                                                        if (!empty($item->estimated_delivery_date)) {
                                                            echo date('d/m/Y', $item->estimated_delivery_date);
                                                        } else {
                                                            echo date('d/m/Y', strtotime(date('Y-m-d H:i:s', $item->created_at) . '+ 10 days'));
                                                        }
                                                        ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <?php if ($item->order_status != "Delivered" || $item->order_status != "Exchange Requested" || $item->order_status != "Cancellation Requested" || $item->order_status != "Cancelled") { ?>
                                            <div class="col-12">
                                                <br>
                                                <button class="btn btn-primary btn-xs" style="float: right" data-target="#cancel_order_modal" onclick="assignCancelOrder('<?= $item->unq_id ?>', '<?= $item->id ?>')" data-toggle="modal">Cancel Subscription</button>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } else if ($item->order_status == 'Cancellation Requested') { ?>
                                    <div class="col-lg-12 col-md-12">
                                        <span class="badge badge-danger text-white">Status : Cancellation Requested</span>

                                        <p><span>Cancellation Requested on : </span><?= date('d ', $item->cancellation_requested_date) . date('M Y', $item->cancellation_requested_date) ?></p>
                                    </div>
                                <?php } else if ($item->order_status == 'Cancelled') { ?>
                                    <div class="col-lg-12 col-md-12">
                                        <p><span>Cancelled on : </span><?= date('d ', $item->cancelled_date) . date('M Y', $item->cancelled_date) ?></p>
                                        <p><strong>Cancellation Details : </strong><?= $item->cancellation_approved_message ?></p>
                                    </div>
                                <?php } else if ($item->order_status == "Delivered") { ?>
                                    <?php if (!empty($item->feedback)) { ?>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 rating">
                                                <p><span>Your Rating</span></p>
                                                <?php for ($i = 1; $i < 6; $i++) { ?>
                                                    <?php if ($i <= $item->feedback->rating) { ?>
                                                        <i class="fas fa-star"></i>
                                                    <?php } else { ?>
                                                        <i class="far fa-star"></i>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <div class="col-12" >
                                    <br>
                                    <p style="text-align: right"><strong>Ordered Date : </strong><?= date('d/m/Y h:i A', $item->updated_at) ?></p>
                                    <p style="text-align: right"><strong>Expiry Date : </strong><?= date('d/m/Y h:i A', $item->expiry_date) ?></p>
                                </div>
                            </div>
                        </div>
                        <hr>
                    <?php } if (!$orders) { ?>
                        <br></br></br>
                        <center><h4>No Orders Found</h4></center>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="cancel_order_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="padding: 20px">
                <h3>Cancel Order <span id="assignCancelOrderId">#ABM321431</span></h3>
                <button class="btn pull-right" data-dismiss="modal">&times;</button>
            </div>

            <form action="javascript:void(0);" id="cancellation_form">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Cancellation Reason</label>
                        <textarea class="form-control" rows="5" placeholder="Tell us the reason for cancellation..." id="cancellation_reason"></textarea>
                        <br><span id="cancellation_message_err" style="color: tomato"></span>
                    </div>
                    <img src="<?= base_url('assets/images/another_spinner.gif') ?>" alt="alt" style="width: 30px; float: right; display: none" id="process_spinner"/>
                </div>
            </form>
            <div class="modal-body" id="cancellation_next" style="display: none;">
                <center>
                    <p id="cancellation_resp_message"></p>
                </center>
            </div>

            <div class="modal-footer">
                <button class="btn btn-default pull-right" data-dismiss="modal" onclick="location.reload();">Close</button>
                <button class="btn btn-primary pull-right" id="order_cancel">Cancel Order</button>
            </div>
        </div>
    </div>
</div>

<script>
    var g_id = "";
    function assignCancelOrder(unq_id, id) {
        g_id = id;
        $('#assignCancelOrderId').html('#' + unq_id);
    }

    $("#order_cancel").click(function (e) {
        var reason = $("#cancellation_reason").val();
        if (reason.length < 10) {
            $("#cancellation_message_err").html('Please provide atleast 10 characters');
            return true;
        } else {
            $("#cancellation_message_err").html('');
        }
        $("#process_spinner").show();
        $.ajax({
            url: '<?= base_url() . 'my_orders/cancel_order' ?>',
            type: 'post',
            data: {id: g_id, reason: reason},
            success: function (resp) {
                $("#process_spinner").hide();
                $("#cancellation_resp_message").html(resp);
                $('#cancellation_form').hide();
                $('#cancellation_next').show();
                $('#order_cancel').hide();
                $('#cancellation_reason').val();
            }
        });

    });
</script>