<html>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>uploads/<?= $site_property->favicon ?>">
</html>
<div class="pkgsteps">
    <?= $this->load->view("includes/header", $main_data); ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg" style="padding: 80px 0px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <?php $this->load->view('menu-dashboard'); ?>
            </div>

            <div class="col-lg-9 col-md-12">
                <div class="whitebox contentdashboard">
                    <h3>Sizes</h3>
                    <?=
                    $this->session->userdata("prefs_update_success");
                    $this->session->unset_userdata("prefs_update_success");
                    ?>
                    <?=
                    $this->session->userdata("prefs_update_error");
                    $this->session->unset_userdata("prefs_update_error");
                    ?>
                    <form method="POST" action="<?= base_url() ?>we_shop_for_you/update_preferences">
                        <h3>We Shop For You</h3>
                        <div class="row justify-content-between">
                            <div class="col-lg-5 col-md-5 clearfix">
                                <label for="">Budget</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="fal fa-rupee-sign"></i></span>
                                    </div>
                                    <input type="number" min="0" class="form-control" name="budget" value="<?= $user_preferences->budget ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Occasion</label>
                                    <textarea rows="5" class="form-control" name="occassion"><?= $user_preferences->occassion ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Description</label>
                                    <textarea rows="5" class="form-control" name="description"><?= $user_preferences->description ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Preferences (Likes)</label>
                                    <textarea rows="5" class="form-control" maxlength="100"  name="liked_preferences"><?= $user_preferences->liked_preferences ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Preferences (Dislikes)</label>
                                    <textarea rows="5" class="form-control" maxlength="100"  name="disliked_preferences"><?= $user_preferences->disliked_preferences ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Special Preferences </label>
                                    <textarea rows="5" class="form-control" name="special_preferences"><?= $user_preferences->special_preferences ?></textarea>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group mb-3">
                                    <label for="">Special Measurements</label>
                                    <textarea rows="5" class="form-control" name="special_measurements"><?= $user_preferences->special_measurements ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-3 col-md-4">
                                <div class="form-group mb-3">
                                    <label for="">Height</label>
                                    <select class="form-control" name="height">
                                        <option value="">-- select Height --</option>
                                        <?php foreach ($heights as $hh) { ?>
                                            <option value="<?= $hh->id ?>" <?php
                                            if ($hh->id == $user_preferences->height) {
                                                echo "selected";
                                            }
                                            ?>><?= $hh->feet . '.' . $hh->inches ?>"</option>
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4">
                                <div class="form-group mb-3">
                                    <label for="">Weight <small>(Kgs)</small></label>
                                    <select class="form-control" name="weight">
                                        <option value="">-- select Weight --</option>
                                        <?php foreach ($weights as $hh) { ?>
                                            <option value="<?= $hh->id ?>" <?php
                                            if ($hh->id == $user_preferences->weight) {
                                                echo "selected";
                                            }
                                            ?>><?= $hh->weight ?></option>
                                                <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            foreach ($categories as $c) {
                                ?>
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group mb-3">
                                        <label for=""><?= $c->name ?> size</label>
                                        <select class="form-control" name="<?= $c->id ?>">
                                            <option value="">--select <?= $c->name ?> size--</option>
                                            <?php foreach ($c->sizes as $item) { ?>
                                                <option <?= $item->is_selected == 'yes' ? 'selected' : '' ?> value="<?= $item->id ?>" ><?= $item->title ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-lg-12 col-md-12 mt-2">
                                <div class="row justify-content-between">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-group">
                                            <button type="submit" class="btn-submit"><i class="fal fa-save"></i> UPDATE</button>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <div class="alert alert-success text-center">
                                            Do you want to changes in previous order. Please send mail to <a href="mailto:info@absolutemens.com">info@absolutemens.com</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>