<div class="pkgsteps">
	<?php include 'includes/header.php' ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12 col-md-12">
				<div class="whitebox">
					<form class="new-added-form" id="wizard-form" action="weshopforu-confirm.php">
						<section>
							<h6>We Shop For You</h6>
							<p><strong>Shipping Address</strong></p>
							<div class="row">
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<input type="text" class="form-control" placeholder="Name">
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<input type="text" class="form-control" placeholder="Email Address">
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<input type="text" class="form-control" placeholder="Phone Number">
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<input type="text" class="form-control" placeholder="Phone Number (Optional)">
									</div>
								</div>
								<div class="col-lg-8 col-md-12">
									<div class="form-group mb-3">
										<input type="text" class="form-control" placeholder="Address">
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<select class="form-control">
											<option>City</option>
										</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<select class="form-control">
											<option>State</option>
										</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<select class="form-control">
											<option>District</option>
										</select>
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<input type="text" class="form-control" placeholder="Landmark">
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<input type="text" class="form-control" placeholder="Street Name">
									</div>
								</div>
								<div class="col-lg-4 col-md-6">
									<div class="form-group mb-3">
										<input type="text" class="form-control" placeholder="Pincode">
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="form-check">
									  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
									  <label class="form-check-label" for="defaultCheck1">
									    Make this as billing address
									  </label>
									</div>
								</div>
								<div class="col-lg-12 col-md-12 mt-5">
									<div class="d-flex justify-content-between">
										<button type="button" class="btn-previous" onclick="location.href='we-shop-for-you-step-2.php'">PREVIOUS</button>
										<button type="submit" class="btn-next">CONTINUE TO CHECKOUT</button>
									</div>
								</div>
							</div>
						</section>
						
						
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="best-sell-active owl-carousel owl-theme">
					<div class="item"><a href="#"><img src="images/bannerhow-1.jpg" alt=""></a></div>
					<div class="item"><a href="#"><img src="images/bannerhow-2.jpg" alt=""></a></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php' ?>