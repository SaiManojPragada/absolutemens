<div class="cms-subpage">
    <h4><?= $info->title ?></h4>
</div>
<div class="howitbox ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <?= $info->description ?>
            </div>
            <div class="col-lg-12 col-md-12 mt-40 mt-sm-20 text-center">
                <div class="text-center">
                    <a href="javascript:void(0);" data-toggle="modal" <?= empty($this->session->userdata('user_unq_id')) ? 'data-target="#loginModal"' : 'data-target="#we-shop-for-you-step-0" ' ?> class="btn btn-primary mt-3 px-5" style="width: 30%; font-size: 14px; padding: 20px 10px">
                        <i class="fal fa-arrow-circle-right"></i>
                        <b>Order Now</b>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="we-shop-for-you-step-0" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h3>We Shop For You Budget<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                    </button></h3>
                <hr>
                <div class="form-group">
                    <label class="control-label">Your Budget</label>
                    <input type="number" id="user_budget" min="0" required class="form-control" placeholder="Your Budget" value="<?= (int) $user_budget ?>">
                    <br>
                </div>
                <div class="form-group">
                    <label class="control-label">No Of Items required</label>
                    <input type="number" id="no_of_items" min="0" required class="form-control" placeholder="No Of Items">
                    <br>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="submitWeShopForYouStep">Submit</button>
                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('#submitWeShopForYouStep').click(function () {
        var budget = $('#user_budget').val();
        var no_of_items = $('#no_of_items').val();
        $(".error").remove();
        if (no_of_items < 1) {
            $('#no_of_items').after('<span class="error" style="font-size: 13px; color: tomato">No Of Items Should be atleast 1 </span>');
            return false;
        }
        $.ajax({
            url: '<?= base_url('package_steps/we_shop_for_you_order') ?>',
            type: "post",
            data: {budget: budget, no_of_items: no_of_items},
            success: function (response) {
                console.log(response);
                var data = JSON.parse(response);
                if (!data['status'] && !data['show_swal']) {
                    $('#user_budget').after('<span class="error" style="font-size: 13px; color: tomato">' + data['message'] + '</span>');
                } else if (!data['status'] && data['show_swal']) {
                    swal({
                        title: data['message'],
                        icon: "info"
                    });
                } else {
                    $(".error").remove();
                    location.href = data['message'];
                }
            }
        });
    });
</script>