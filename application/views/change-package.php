<div class="pkgsteps">
</div>
<div class="subpagebg"></div>
<div class="banner-area ptb-50">
    <br><br><br><br>
    <div class="container">
        <div class="row justify-content-center">
            <?php foreach ($packages as $item) { ?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-10">
                    <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                        <div class="banner-img">
                            <a href="#"><img src="<?= base_url() ?>uploads/<?= $item->image ?>" alt="banner" /></a>
                        </div>
                        <div class="banner-content-3">
                            <?= $item->title ?>
                            <p><?= $item->short_description ?></p>
                            <?php if ($this->session->userdata("user_unq_id")) { ?>
                                <a href="package-steps/step-one/<?= $item->slug ?>"><i class="fal fa-chevron-circle-right"></i> Subscribe Now</a>
                            <?php } else { ?>
                                <a href="#loginModal" data-toggle="modal"><i class="fal fa-chevron-circle-right"></i> Subscribe Now</a>
                            <?php } ?>
                            <a href="package-details/<?= $item->slug ?>" class="btn-blue"><i class="fal fa-chevron-circle-right"></i> Know More</a>
                        </div>
                    </div>
                    <!-- single-banner-end -->
                </div>
            <?php } ?>



        </div>

    </div>
</div>