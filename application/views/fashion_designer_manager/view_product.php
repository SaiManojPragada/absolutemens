<div ng-controller="shopForOrderCtrl">
    <div class="card shadow-sm border-0 mixwithbg" ng-init="getProductDetails('<?= $id ?>', '<?= $inventory_id ?>')">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>My Products Preview</strong>

                    <span style="float: right; margin-right: 10px">Lasted Updated On: {{product.updated_at}}</span>
                </div>

            </div>
        </div>
        <div class="card-body p-0">
            <div class="wrapper wrapper-content row">
                <div class="col-md-4">
                    <center>
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="width: 60%; padding-top: 30px">
                            <div class="carousel-inner" style="background-color: #343A40">
                                <div class="carousel-item active" ng-if="product.images.length < 1">
                                    <center style="height: 500px; width: 100%; padding-top: 50%">
                                        <span style="color: white"> No Images</span>
                                    </center>
                                </div>
                                <div class="carousel-item {{($index==0)?'active':''}}" ng-repeat="img in product.images">
                                    <center>
                                        <img src="<?= base_url() ?>{{img.image}}" style="height: 500px;object-fit: cover;width: 100%">
                                    </center>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </center>
                </div>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css" integrity="sha512-YWzhKL2whUzgiheMoBFwW8CKV4qpHQAEuvilg9FAn5VJUDwKZZxkJNuGM4XkWuk94WCrrwslk8yWNGmY1EduTA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
                <div class="col-md-8">
                    <div style="width: 100%">
                        <p><h4>#{{product.product_id}}</h4></p>
                        <p><h5>{{product.brand}} - {{product.title}}</h5></p>
                        <p>{{product.description}}</p>
                    </div>
                    <hr>
                    <h2>Product Inventory</h2>
                    <br>
                    <table>
                        <tr>
                            <th style="width: 20%">#</th>
                            <th style="width: 20%">Size</th>
                            <th style="width: 20%">Price</th>
                            <th style="width: 20%">Quantity</th>
                        </tr>
                        <tr ng-repeat="entry in product.inventory">
                            <td>{{$index + 1}}</td>
                            <td>{{entry.size}}</td>
                            <td><i class="fa fa-inr"></i> {{entry.price}}</td>
                            <td>{{entry.quantity}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 10px 50px;
        text-align: center;
    }
</style>
<script src="<?= base_url() ?>vendor_assets/js/angular/fashion_designer/shopForOrderCtrl.js?r=<?= time() ?>"></script>