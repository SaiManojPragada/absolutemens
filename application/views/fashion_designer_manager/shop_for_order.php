<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    span{
        margin : 10px
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight" ng-controller="shopForOrderCtrl" ng-init="checkAlreadyForwardedToFashionManager(); getOrdersDetailsWithProducts('initial'); getCartList();">
    <div ng-if="pageObj.is_forwarded_to_fashion_manager">
        <div class="alert alert-primary" role="alert">
            This order has already been forwarded to fashion manager.
        </div>
        <h5>Order logs</h5>
        <hr>
        <table class="table table-striped table-bordered" ng-if="orderLogsList.length > 0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Comment</th>
                    <th>Status</th>
                    <th>Action done at</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="item in orderLogsList track by $index">
                    <td>{{$index + 1}}</td>
                    <td>{{item.log}}</td>
                    <td>{{item.order_status}}</td>
                    <td>{{item.created_at}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class='col-md-12' ng-if="!pageObj.is_forwarded_to_fashion_manager">
        <div class="rm-small">
            <div class='m-3'>
                <div class='row'>
                    <div class='col-md-6'>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>
                                    <h6><b>Package & Preferences : </b></h6>
                                    <hr/>
                                    <span> <b>Order id</b>             : #{{shopForObj.order_details.unq_id}}</span><br><br>
                                    <span><b>Order on</b>             : {{shopForObj.order_details.ordered_on}}</span><br><br>
                                    <span><b>Budget : </b> {{ shopForObj.order_details.order_budget}}</span><br><br>
                                    <span ng-if="shopForObj.order_details.is_we_shop_for_you == '0'"><b>Package name</b>         : {{shopForObj.order_details.package_details.title}} <br> <br></span> <br>
                                    <span ng-if="shopForObj.order_details.is_we_shop_for_you == '1'"><b>Package name</b>         : We Shop For you<br><br></span> 
                                    <span><b>No of items</b>          : {{shopForObj.order_details.no_of_items}}</span><br><br>
                                    <p style="word-break: break-all"><b>Preferences likes</b>    : {{shopForObj.order_details.preferences_likes}}</p><br><br>
                                    <p style="word-break: break-all"><b>Preferences dislikes</b> : {{shopForObj.order_details.preferences_dislikes}}</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class='col-md-6'>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>
                                    <h6><b>Customer details : </b></h6>
                                    <hr/>
                                    <span><b>Customer name</b>    : {{shopForObj.order_details.customer_details.name}}</span> <br><br>
                                    <span><b>Customer address</b> : {{shopForObj.order_details.customer_details.address ? shopForObj.order_details.customer_details.address : 'N/A'}}</span><br><br>
                                    <span><b>Height</b>           : {{shopForObj.order_details.height}}</span><br><br>
                                    <span><b>Weight</b>           : {{shopForObj.order_details.weight}}</span><br><br>
                                    <span>
                                        <div class="row" >
                                            <div class="col-md-3" ng-repeat="sz in shopForObj.order_details.selected_sizes track by $index">
                                                <b>{{sz.category}} size</b>       : {{sz.size}}
                                            </div>
                                        </div>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <hr/>

                <div class="alert alert-danger" role="alert" ng-if="orderLogsList.order_status == 'rejected_by_fashion_manager'">
                    <h4>Your Previous Suggestion has been rejected by fashion manager due to below reason : </h4><br>
                    <p>{{orderLogsList.reject_reason}}</p><br/>
                    <p><b>Note : </b> Please re-suggest products according to above fashion manager suggestion</p>
                </div>
                <hr/>


                <form ng-submit='getOrdersDetailsWithProducts("pagination")'>
                    <div class='row'>
                        <div class='col-md-3'>
                            <div class="form-group">
                                <label for="product_code">Search by Product code:</label>
                                <input type="text" class="form-control" id="product_code" ng-model='shopForOrderPostObj.product_code'  placeholder="Enter product code">
                            </div>
                        </div>
                        <div class='col-md-3'>
                            <div class="form-group">
                                <label for="category_id">Search by Categories:</label>
                                <select class='form-control' name='category_id' id='category_id' ng-model='shopForOrderPostObj.category_id' ng-disabled='shopForObj.categories_list.length == 0'>
                                    <option value='' selected disabled>Select category</option>
                                    <option value='{{category.category_id}}' ng-value='category.category_id' ng-repeat='category in shopForObj.order_details.selected_sizes track by $index'>{{category.category}}</option>
                                </select>
                            </div>
                        </div>
                        <div class='col-md-3'>
                            <div class="form-group">
                                <label for="vendor_id">Search by Vendors:</label>
                                <select class='form-control' name='vendor_id' id='vendor_id' ng-model='shopForOrderPostObj.vendor_id'  ng-disabled='shopForObj.vendors_list.length == 0'>
                                    <option value='' selected disabled>Select vendor</option>
                                    <option value='{{vendor.id}}' ng-value='vendor.id' ng-repeat='vendor in shopForObj.vendors_list track by $index'>{{vendor.business_name}} [{{vendor.owner_name}}]</option>
                                </select>
                            </div>
                        </div>
                        <div class='col-md-3'>
                            <button type="submit" style='margin-top: 29px;color: #fff;' class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </form>
                <hr/>
                <h6><b>Available Products : </b></h6>
                <div class="alert alert-info" role="alert">
                    {{cartList.length}} products added in cart. <a target='_blank' href='<?= base_url() ?>fashion_designer_manager/cart?order_id=<?= $unique_order_id ?>'><b>Click here to go to cart</b></a>
                </div>
                <div>
                    <div class='row'>
                        <div class='col-md-12' ng-if='shopForObj.related_products.length == 0'>
                            <div class="alert alert-danger" role="alert">
                                Oops! No products found
                            </div>
                        </div>
                        <div class='col-md-3'  ng-if='shopForObj.related_products.length > 0'  ng-repeat='product in shopForObj.related_products track by $index'>
                            <div class="card" style="width: 18rem;border-radius:5px;">
                                <img src="{{product.product_image}}" class="card-img-top" alt="{{product.title}}-{{product.size}}" style='width: 286px;height: 300px;'>
                                <div class="card-body">
                                    <h6 class="card-title">{{product.title}} </h6>
                                    <h6 class="card-title text-primary"><b>Size - {{product.size}}</b></h6>
                                    <p class="card-text">{{product.description}}</p>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">Id : #{{product.product_id}}</li>
                                    <li class="list-group-item">Price : {{product.price}} &nbsp;<br> Brand : {{product.brand}}</li>
                                </ul>
                                <div class="card-body">
                                    <a href="javascript:void(0)" ng-click='openCartCommentModalPopup(product.id, 1, product.product_inventory_id)' class="card-link">Suggest this</a>
                                    <a href="<?= base_url() ?>fashion_designer_manager/view_product/view/{{product.id}}/{{product.product_inventory_id}}" target='_blank' class="card-link">View Product</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span ng-bind-html='productsPagination.pagination'></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="cart-comment-modal-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Suggestion description</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form ng-submit="addToCart()" id="suggestionForm">
                    <div class="form-group">
                        <label>Suggestion Description</label>
                        <textarea class="form-control" type="text" name="comment" ng-model="cartObj.comment" rows="7" /></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary text-white">Save changes</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<script>
    unique_id = '<?= $unique_order_id ?>';</script>
<script src="<?= base_url() ?>vendor_assets/js/angular/fashion_designer/shopForOrderCtrl.js?r=<?= time() ?>"></script>