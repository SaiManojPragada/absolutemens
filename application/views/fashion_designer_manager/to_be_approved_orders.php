<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<div class="wrapper wrapper-content animated fadeInRight" ng-controller="myOrdersCtrl" ng-init="getToBeApprovedOrders()">
    <div class="rm-small m-4">
        <div>
            <div class='col-md-12' ng-if='toBeApprovedOrders.length == 0'>
                <div class="alert alert-danger" role="alert">
                    Oops! No data found.Please check back later
                </div>
            </div>
            <table class="table table-striped table-bordered dataTables-example dataTable" ng-if="toBeApprovedOrders.length > 0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Order id</th>
                        <th>Fashion Analyst details</th>
                        <th>Total amount</th>
                        <th>Order status</th>
                        <th>Action at</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in toBeApprovedOrders track by $index">
                        <td>{{$index + 1}}</td>
                        <td>#{{item.unique_order_id}}<br>
                            <p ng-if="item.is_exchanged === 1" style="color: tomato; font-weight: bolder; font-size: 18px">(Exchange Order)</p>
                        </td>
                        <td><b>Name</b> - {{item.fashion_analyst_details.name}} <br> <b>Phone</b> - {{item.fashion_analyst_details.phone_number}}</td>
                        <td><i class="fa fa-inr"></i>{{item.final_total}}</td>
                        <td><h6> <span class="badge badge-success p-1">{{item.order_status}}</span></h6></td>
                        <td>{{item.created_at}}</td>
                        <td>
                            <a href="javascript:void(0)" ng-click="showOrderDetailsModalPopup(item.id)"><button class="btn btn-info text-white btn-xs">View Details</button></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade " id="order-details-modal-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 98%;">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <button type="button" class="btn btn-success text-white" ng-if="orderViewObj.order_status == 'suggested_by_fashion_analyst'" ng-click="acceptAndForwardToAdmin(orderViewObj.id)">Accept and Forward To Admin</button> &nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-danger text-white"  ng-if="orderViewObj.order_status == 'suggested_by_fashion_analyst'" ng-click="openRejectModalPopup(orderViewObj.id)">Reject</button> &nbsp;  &nbsp;<span style="color:red;font-size:13px;">(If rejected all suggestions by Fashion analyst will be rejected)</span>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <h5>Order Details</h5>
                        <hr/>
                        <table class="table table-bordered">
                            <tr>
                                <td><b>Order id - </b>#{{orderViewObj.unique_order_id}}</td>
                                <td><b>Subscription name - </b>{{orderViewObj.package_title}}<br/><b>No of items = </b> {{orderViewObj.no_of_items}}</td>
                                <td><b>Analyst Name</b> - {{orderViewObj.fashion_analyst_details.name}} <br> <b>Analyst Phone</b> - {{orderViewObj.fashion_analyst_details.phone_number}}</td>
                                <td><b>Total order amount - </b><i class="fa fa-inr"></i>{{orderViewObj.final_total}}</td>
                                <td><b>Order status - </b><h6> <span class="badge badge-success p-1">{{orderViewObj.order_status}}</span></h6></td>
                            </tr>
                        </table>
                        <h5>Order Products</h5>
                        <hr/>
                        <table class="table table-bordered">
                            <tr>
                                <th>Select</th>
                                <th>Vendor details</th>
                                <th>Product details</th>
                                <th>Product Image</th>
                                <th>Unit Price</th>
                                <th>Quantity</th>
                                <th>Subtotal</th>
                                <th style="width: 411px;">Comment</th>
                                <th>Suggested at</th>
                            </tr>
                            <tr ng-repeat="op in orderViewObj.order_products track by $index">
                                <td>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" ng-model="op.approved_by_fashion_manager" ng-click="checkSubscriptionValidation(orderViewObj.subscription_transaction_id, orderViewObj.id, op.id)" id="checkbox{{op.id}}">
                                        <label class="form-check-label" for="checkbox{{op.id}}"></label>
                                    </div>
                                </td>
                                <td>
                                    <b>Business name - </b> {{op.vendor_details.business_name}}<br>
                                    <b>Name - </b> {{op.vendor_details.owner_name}}<br>
                                    <b>Email - </b> {{op.vendor_details.contact_email}}<br>
                                    <b>Phone number - </b> {{op.vendor_details.contact_number}}
                                </td>
                                <td>
                                    <b>Product name</b> - {{op.product_details.title}}<br>
                                    <b>Product id</b> - #{{op.product_details.product_id}}<br>
                                    <b>Category</b> - {{op.category_name}} <br>
                                    <b>Size</b> - {{op.size}}
                                </td>
                                <td>
                                    <img src="{{op.image}}" style="width: 150px" alt="alt"/>
                                </td>
                                <td><i class="fa fa-inr"></i>{{op.unit_price}}</td>
                                <td>{{op.quantity}}</td>
                                <td><i class="fa fa-inr"></i>{{op.subtotal}}</td>
                                <td>{{op.comment}}</td>
                                <td>{{op.created_at}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="reject-reason-modal-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Reject Reason</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form ng-submit="rejectOrder()" id="rejectReasonForm">
                        <div class="form-group">
                            <label>Reject Reason</label>
                            <textarea class="form-control" type="text" name="reject_reason" ng-model="rejectObj.reject_reason" rows="7" /></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary text-white">Save changes</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    unique_id = '<?= $this->input->get("order_id") ?>';
</script>
<script src="<?= base_url() ?>vendor_assets/js/angular/fashion_designer/myOrdersCtrl.js?r=<?= time() ?>"></script>