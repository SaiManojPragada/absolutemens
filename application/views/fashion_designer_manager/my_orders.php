<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<div class="wrapper wrapper-content animated fadeInRight" ng-controller="ordersCtrl">
    <div class="rm-small">
        <table class="table table-striped table-bordered dataTables-example dataTable" ng-init="getOrders()">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Order Details</th>
                    <th>Package Details</th>
                    <th>Customer Details</th>
                    <th ng-if="!disbled">Fashion Analyst details</th>
                    <th>Selected sizes</th>
                    <th>Height</th>
                    <th>Weight</th>
<!--                    <th>preferences_likes</th>
                    <th>preferences_dislikes</th>-->
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="item in orders">
                    <td>{{$index + 1}}</td>
                    <td>
                        <p><b>Order Id : </b>{{item.unq_id}}</p>
                        <p><b>Order On : </b>{{item.ordered_on}}</p>
                        <p ng-if="item.is_exchanged == '1'" style="color: tomato; font-weight: bolder; font-size: 18px">(Exchange Order)</p>
                    </td>
                    <td>
                        <p ng-if="!item.is_we_shop_for_you"><b>Package Name : </b>{{item.package_details.title}}</p>
                        <p><b>No Of Items : </b>{{item.no_of_items}}</p>
                        <!--<p><b>Price : </b><i class="fa fa-inr"></i> {{item.package_details.price}}</p>-->
                        <p><b>Budget Amount : </b><i class="fa fa-inr"></i> {{item.order_budget}}</p>
                    </td>
                    <td>
                        <p><b>Name : </b>{{item.customer_details.name}}</p>
                        <p><b>Phone Number : </b><a href="tel:{{item.customer_details.mobile}}">{{item.customer_details.mobile}}</a></p>
                    </td>
                    <td ng-if="!disbled">
                        <p><b>Name : </b>{{item.fashion_analyst_details.name}}</p>
                        <p><b>Email : </b>{{item.fashion_analyst_details.email}}</p>
                        <p><b>Phone Number : </b>{{item.fashion_analyst_details.phone_number}}</p>
                    </td>
                    <td>
                        <ul>
                            <li ng-repeat="s in item.selected_sizes">{{s.category}} - {{s.size}}&nbsp;</li>
                        </ul>
                    </td>
                    <td>{{item.height}}</td>
                    <td>{{item.weight}}</td>
<!--                    <td><div style="width:60px">{{item.preferences_likes}}</div></td>
                    <td><div style="width:60px">{{item.preferences_dislikes}}</div></td>-->
                    <td>
                        <?php if ($this->session->userdata('fashion_designation') == 'Fashion Analyst') { ?>
                            <a  href="<?= base_url('fashion_designer_manager/shop_for_order/products/') ?>{{item.unq_id}}" target="_blank" class="btn btn-success text-white">Shop For Order</a> <hr/>
                        <?php } ?>
                        <?php if ($this->session->userdata('fashion_designation') == 'Fashion Designer') { ?>
                            <a  href="javascript:void(0);" ng-click="openReassignModalPopup(item.id)" class="btn btn-warning text-white">Re-Assign</a> <hr/>
                        <?php } ?>
                        <a href="<?= base_url('fashion_designer_manager/view_order/view/') ?>{{item.unq_id}}"><button class="btn btn-primary text-white btn-xs">View Order</button></a>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>#</th>
                    <th>Order Details</th>
                    <th>Package Details</th>
                    <th>Customer Details</th>
                    <th ng-if="!disbled">Fashion Analyst details</th>
                    <th>Selected sizes</th>
                    <th>Height</th>
                    <th>Weight</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="modal fade" id="reassign-modal-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form ng-submit="reAssignFashionAnalyst()" id="reAssignForm">
                        <div class="form-group">
                            <label>Select Fashion Analyst</label>
                            <select name="fashion_analyst_id" class="form-control" ng-model="assignPostObj.fashion_analyst_id" ng-disabled="fashionAnalystsList.length == 0">
                                <option value="" selected disabled>Select fashion analyst id</option>
                                <option value="{{fa.id}}" ng-repeat="fa in fashionAnalystsList track by $index">{{fa.name}} ({{fa.phone_number}})</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary text-white">Save changes</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="<?= base_url() ?>vendor_assets/js/angular/fashion_designer/ordersCtrl.js?r=<?= time() ?>"></script>