<div class="wrapper wrapper-content animated fadeInLeft">
    <div class="row  border-bottom white-bg dashboard-header">
        <div class="col-md-3">
            <h4>Your Dashboard</h4>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <?php if ($this->session->userdata('fashion_designation') == "Fashion Designer") { ?>
        <div class="row rm-small">
            <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center" onclick="location.href = '<?= base_url() ?>fashion_designer_manager/my_orders'" style="cursor:pointer">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <span class="fa-stack fa-2x d-block mx-auto">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fal fa-shopping-bag fa-stack-1x fa-inverse"></i>
                        </span>
                        <small class="d-block my-2">IN PROCESS</small>
                        <h4 class="m-0 ng-binding">
                            <?php echo $in_process_orders ? $in_process_orders : 0 ?>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center" onclick="location.href = '<?= base_url() ?>fashion_designer_manager/my_orders/to_be_approved'" style="cursor:pointer">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <span class="fa-stack fa-2x d-block mx-auto">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fal fa-shopping-bag fa-stack-1x fa-inverse"></i>
                        </span>
                        <small class="d-block my-2">TO BE APPROVED</small>
                        <h4 class="m-0 ng-binding">
                            <?php echo $to_be_approved_orders ? $to_be_approved_orders : 0 ?>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center" onclick="location.href = '<?= base_url() ?>fashion_designer_manager/my_orders/forwarded_to_admin'" style="cursor:pointer">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <span class="fa-stack fa-2x d-block mx-auto">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fal fa-shopping-bag fa-stack-1x fa-inverse"></i>
                        </span>
                        <small class="d-block my-2">FORWARDED TO ADMIN</small>
                        <h4 class="m-0 ng-binding">
                            <?php echo $forwarded_to_admin_orders ? $forwarded_to_admin_orders : 0 ?>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center" onclick="location.href = '<?= base_url() ?>fashion_designer_manager/my_orders'" style="cursor:pointer">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <span class="fa-stack fa-2x d-block mx-auto">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fal fa-shopping-bag fa-stack-1x fa-inverse"></i>
                        </span>
                        <small class="d-block my-2">My Orders</small>
                        <h4 class="m-0 ng-binding">
                            <?php echo $in_process_orders ? $in_process_orders : 0 ?>
                        </h4>
                    </div>
                </div>
            </div>
        </div> 
    <?php } ?>
</div>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('Monitor every thing...', 'Welcome to <?= SITE_TITLE ?>');

        }, 1300);
    });
</script>
