<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<div class="wrapper wrapper-content animated fadeInRight" ng-controller="ordersCtrl">
    <div ng-init="getOrderDetails('<?= $order_id ?>')"></div>
    <div class="row">
        <div class="col-md-6">
            <p>Order Details (#{{singleOrder.unq_id}})<span style="float: right"><b>Max Budget Amount : </b><i class="fa fa-inr"></i> {{singleOrder.order_budget}}</span></p>
            <hr>
            <div class="row">
                <div class="col-6" ng-if="singleOrder.full_images">
                    <h5>Full Images</h5>
                    <hr>
                    <div id="fullImages" class="carousel slide" data-ride="carousel" style="padding: 0px 30px">

                        <div class="carousel-inner">
                            <div class="carousel-item active" ng-if="product.images.length < 1">
                                <center style="height: 400px; width: 100%; padding-top: 50%">
                                    <span style="color: white"> No Images</span>
                                </center>
                            </div>
                            <div class="carousel-item {{($index==0)?'active':''}}" ng-repeat="img in singleOrder.full_images">
                                <center>
                                    <img src="<?= base_url() ?>{{img.image}}" style="height: 400px;object-fit: contain;width: 100%">
                                </center>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#fullImages" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#fullImages" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-6" ng-if="singleOrder.half_images">
                    <h5>Half Images</h5>
                    <hr>
                    <div id="halfImages" class="carousel slide" data-ride="carousel" style="padding: 0px 30px">

                        <div class="carousel-inner">
                            <div class="carousel-item active" ng-if="product.images.length < 1">
                                <center style="height: 400px; width: 100%; padding-top: 50%">
                                    <span style="color: white"> No Images</span>
                                </center>
                            </div>
                            <div class="carousel-item {{($index==0)?'active':''}}" ng-repeat="img in singleOrder.half_images">
                                <center>
                                    <img src="<?= base_url() ?>{{img.image}}" style="height: 400px;object-fit: contain;width: 100%" />
                                </center>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#halfImages" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#halfImages" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6" ng-if="!singleOrder.is_we_shop_for_you">
            <h3>Package Details</h3>
            <hr>
            <br>
            <br>
            <br>
            <div class="row">
                <div class="col-3">
                    <img src="<?= base_url('uploads/') ?>{{singleOrder.package_details.image}}" alt="alt" style="max-width: 100%"/>
                </div>
                <div class="col-9">
                    <h5>{{singleOrder.package_details.title}} <span style="float: right; padding-right: 30px"><i class="fa fa-inr"></i> {{singleOrder.package_details.price}}</span></h5>
                    <p><b>{{singleOrder.package_details.no_of_items}} Items</b> For this Package</p>
                    <p ng-bind-html="thisCanBeusedInsideNgBindHtml"></p>
                </div>
            </div>
        </div>
        <div class=""></div>
        <div class="col-12" style="padding: 20px 50px">
            <br>
            <h3>Customer Related Details</h3>
            <hr>
            <div class="col-12 row">
                <table class="col-6 table table-bordered table-striped">
                    <tr>
                        <td>
                            <span> <b>Order id</b>             : #{{singleOrder.unq_id}}</span><br><br>
                            <span><b>Order on</b>             : {{singleOrder.ordered_on}}</span><br><br>
                            <span ng-if="singleOrder.is_we_shop_for_you == '0'"><b>Package name</b>         : {{singleOrder.package_details.title}}</span> <br><br>
                            <span ng-if="singleOrder.is_we_shop_for_you == '1'"><b>Package name</b>         : We Shop For you</span> <br><br>
                            <span><b>No of items</b>          : {{singleOrder.no_of_items}}</span><br><br>
                            <p style="word-break: break-all"><b>Preferences likes</b>    : {{singleOrder.preferences_likes}}</p>
                            <p style="word-break: break-all"><b>Preferences dislikes</b> : {{singleOrder.preferences_dislikes}}</p>
                        </td>
                    </tr>
                </table>

                <table class="col-6 table table-bordered table-striped">
                    <tr>
                        <td>
                            <span><b>Customer name</b>    : {{singleOrder.address_name}}</span> <br><br>
                            <span><b>Customer address</b> : {{singleOrder.address}}</span><br><br>
                            <span><b>Height</b>           : {{singleOrder.height}}</span><br><br>
                            <span><b>Weight</b>           : {{singleOrder.weight}}</span><br><br>
                            <span>
                                <div class="row" >
                                    <div class="col-md-3" ng-repeat="sz in singleOrder.order_details.selected_sizes track by $index">
                                        <b>{{sz.category}} size</b>       : {{sz.size}}
                                    </div>

                                </div>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div> 

</div>
<style>
    .carousel-control-prev-icon,
    .carousel-control-next-icon {
        background-color: grey;
        border-radius: 100%;
    }
</style>
<script src="<?= base_url() ?>vendor_assets/js/angular/fashion_designer/ordersCtrl.js?r=<?= time() ?>"></script>