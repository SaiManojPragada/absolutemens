<!-- Modal -->
<div class="modal scale" id="orderDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-full" role="document">
        <div class="modal-content">
            <div class="modal-header py-3">
                <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Order Details</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fal fa-times"></span>
                </button>
            </div>
            <div class="modal-body bg-light">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h6 class="m-0">#{{order.ref_id}}</h6>
                                <h6 class="m-0">{{order.store_name}} - {{order.store_code}}</h6>
                            </div>
                            <div class="col-auto">
                                <p class="m-0">Order  {{order.service_status}} <i class="fas fa-check-circle text-success"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list-group">
                    <div class="list-group-item" ng-repeat="item in order.cart_items">
                        <div class="row align-items-center">
                            <div class="col">
                                <span>{{item.restaurant_food_items_name}}</span>
                                <span ng-if="item.restaurant_food_options_name"> - {{item.restaurant_food_options_name}}</span>
                                <span>(Qty:{{item.qty}} * <small class="fal fa-rupee-sign fa-fw"></small> {{item.applicable_price}})</span>
                                <br>

                                <span ng-if="item.following_individual_item_tax == 'yes'">
                                    GST ({{item.tax_percentage}}%) :  {{item.applied_tax_amount}}
                                </span>
                                <br/>
                                <span ng-repeat="adItem in item.addons_json">

                                    {{adItem.item_name}} ({{item.qty}} * <small class="fal fa-rupee-sign fa-fw"></small> {{adItem.price}})

                                    <span ng-if="item.following_individual_item_tax == 'yes'">
                                        <br>
                                        GST ({{adItem.tax_percentage}}%) :  <small class="fal fa-rupee-sign fa-fw"></small>{{adItem.tax_amount}}
                                        <br/>
                                    </span>
                                </span>

                            </div>
                            <div class="col-auto"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{item.total_amount}}</strong></div>

                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row align-items-center">
                            <div class="col-6 pb-1"><span>Subtotal</span></div>
                            <div class="col-6 pb-1 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.sub_total}}</strong></div>
                            <div class="col-6 pb-1"><span>Discount <span ng-show="order.coupon_code.length > 0">Promo Code : {{order.coupon_code}}</span></span></div>
                            <div class="col-6 pb-1 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.applied_discount_amount}}</strong></div>
                            <div class="col-6 pb-1"><span>Delivery fee</span></div>
                            <div class="col-6 pb-1 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.applied_delivery_charge}}</strong></div>
                            <!-- <div class="col-6"><span>VAT (13%)</span></div>
                            <div class="col-6 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.applied_tax_amount}}</strong></div> -->
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="row align-items-center">
                            <div class="col-6"><strong>Total</strong>
                                <p class="m-0">{{order.payment_mode}}</p></div>
                            <div class="col-6 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.grand_total}}</strong></div>
                        </div>
                    </div>

                    <div class="list-group-item">
                        <div class="row align-items-center">
                            <div class="col-6"><strong>Delivery Person Name & Contact Number</strong>
                                <p class="m-0">{{order.delivery_person_name}} - {{order.delivery_person_contact_number}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="list-group-item">
                        <div class="row align-items-center">
                            <div class="col-6"><strong>Customer Name & Contact Number</strong>
                                <p class="m-0">{{order.customer_details.customer_name}} - {{order.customer_details.mobile}}</p>
                            </div>
                        </div>
                    </div>


                    <div class="row r-m-05">
                        <div class="col-md-12 c-p-05">
                            <div class="panel panel-default m-b-1">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Order Activity Log</h3>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr> 
                                                <th class="text-center">Sno</th>
                                                <th class="text-center">Activity</th>
                                                <th class="text-center">Date time</th>
                                                <th class="text-center">By</th>
                                                <th class="text-center">Who</th>
                                                <th class="text-center">Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr class="text-center" ng-repeat="pl in order.process_log">
                                                <td>{{$index + 1}}</td>
                                                <td>{{pl.action_type}}</td>
                                                <td>{{pl.action_triggered_at}}</td>
                                                <td>{{pl.person_name}}
                                                    <br/>
                                                    {{pl.contact_number}}
                                                </td> 
                                                <td>
                                                    {{pl.who  == "Restaurant" ? 'Store' : pl.who }}
                                                </td>
                                                <td>
                                                    <span ng-if="pl.retail_chain_info">
                                                        By Retail Chain 
                                                        {{pl.retail_chain_info.ref_id}}<br/>
                                                        {{pl.retail_chain_info.business_name}}<br/>
                                                        {{pl.retail_chain_info.contact_number}}<br/>
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer" style="padding-right:40%; background: white">
                <div ng-if="type == 'Pending'">
                    <button  ng-click="acceptOrder(order.ref_id)" type="button" class="btn btn-success"><i class="fal fa-check fa-fw mr-1"></i>Accept</button>
                    <button  href="#" data-toggle="modal" data-target="#orderRejectModal" type="button" class="btn btn-danger"><i class="fal fa-times fa-fw mr-1"></i>Reject</button>
                </div>
                <div ng-if="order.service_status == 'Waiting For Delivery Person'">
                    <?php if ($site_property->admin_delivery_persons == 0) { ?>
                        <button href="#" data-toggle="modal" data-target="#orderDeliverModal"  ng-click="deliverOrder(order.ref_id)" type="button" class="btn btn-success">
                            <i class="fal fa-check fa-fw mr-1"></i>Out For Delivery</button>
                        <?php
                    } else {
                        echo "Waiting for delivery person acceptance";
                    }
                    ?>
                </div>


                <div ng-if="order.service_status == 'Waiting For Pickup'">
                    <?php if ($site_property->admin_delivery_persons == 0) { ?>
                        <button href="#" data-toggle="modal" data-target="#orderDeliverModal"  ng-click="deliverOrder(order.ref_id)" type="button" class="btn btn-success">
                            <i class="fal fa-check fa-fw mr-1"></i>Out For Delivery</button>
                        <?php
                    } else {
                        echo "Waiting for Delivery person to pickup";
                    }
                    ?>
                </div>

                <?php if ($site_property->admin_delivery_persons == 0) { ?>
                    <div ng-if="order.service_status == 'Out For Delivery'">
                        <button  ng-click="markAsDelivered(order.ref_id)" type="button" class="btn btn-success"><i class="fal fa-check fa-fw mr-1"></i>Mark as Delivered</button>
                    </div>
                    <?php
                } else {
                    // echo "Waiting for delivery person delivery report";
                }
                ?>

                <button type="button" class="btn btn-info btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div> 
</div>