</div>

</div>
<script type="text/javascript" src="<?= base_url() ?>vendor_assets/js/main.js"></script>
<script>
//    document.body.style.zoom = "80%";
</script>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'excel', title: ' Orders Lists'},
                    {extend: 'pdf', title: ' Orders Lists'},

                    {extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ]

            });
        }, 2500)
    });
</script>

<script src="<?= base_url() ?>admin_assets/js/plugins/dataTables/datatables.min.js"></script>
<style>
    #DataTables_Table_0_wrapper {
        width : 100%;
    }
    .html5buttons, .dataTables_length, .DataTables_Table_0_filter, .dataTables_info{
        float: right !important;
    }
    .btn:not(:disabled):not(.disabled) {
        cursor: pointer;
        color: black;
    }
    .dataTables_paginate {
        width: 100%;
    }
    .dataTables_paginate ul{
        float: right !important;
        margin-right: 50px;
    }
    .paginate_button, .paginate_button * {
        height: auto;
        font-size: 14px;
        color: black;
    }
    .paginate_button.disabled *{
        color: grey;
    }
    .paginate_button.active{
        color: black;
    }
</style>

<script src="<?= base_url() ?>vendor_assets/js/angular/fashion_designer/commonCtrl.js?r=<?= time() ?>"></script>
</body>
</html>