<!doctype html>
<html lang="en" ng-app="vendorApp" ng-cloak ng-controller="commonCtrl">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?= $site_property->site_name ?></title>
        <meta name="robots" content="noindex">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>vendor_assets/scss/bootstrap/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>vendor_assets/fonts/f5/css/all.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>vendor_assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>vendor_assets/css/toastr.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>vendor_assets/css/jquery.datetimepicker.min.css">

        <script src="<?= base_url() ?>web_assets/js/api_url.js"></script>

        <script type="text/javascript" src="<?= base_url() ?>vendor_assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>vendor_assets/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>vendor_assets/js/additional-methods.js"></script>

        <script src="<?= base_url() ?>vendor_assets/js/angular.min.js"></script>
        <script src="<?= base_url() ?>vendor_assets/js/angular-sanitize.min.js"></script>
        <script src="<?= base_url() ?>vendor_assets/js/angular-cookies.min.js"></script>
        <script src="<?= base_url() ?>vendor_assets/js/angular/services.js?r=<?= time() ?>"></script>
        <script src="<?= base_url() ?>vendor_assets/js/angular/app.js?r=<?= time() ?>"></script>
        <script src="<?= base_url() ?>vendor_assets/js/angular/directives.js?r=<?= time() ?>"></script>



        <script type="text/javascript" src="<?= base_url() ?>vendor_assets/js/loadingoverlay.min.js" ></script>

        <script type="text/javascript" src="<?= base_url() ?>vendor_assets/js/popper.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>vendor_assets/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>vendor_assets/js/moment.min.js"></script>


        <script src="<?= base_url() ?>vendor_assets/js/jquery.datetimepicker.full.min.js"></script>

        <script type="text/javascript" src="<?= base_url() ?>vendor_assets/js/toastr.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>vendor_assets/js/angular-base64-upload.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">

        <link href="<?= base_url() ?>admin_assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

        <style>
            em.invalid{
                color:#B00020;
                font-style : inherit;
            }
        </style>
        <?php /* if ($site_property->one_signal_web_push_notifications) { ?>
          <link rel="manifest" href="<?= base_url() ?>vendor_assets/manifest.json" />
          <script src="//cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
          <script>
          var OneSignal = window.OneSignal || [];
          OneSignal.push(function() {
          OneSignal.init({
          appId: "<?= $site_property->one_signal_web_push_notifications ?>",
          autoRegister: true,
          notifyButton: {
          enable: true,
          },
          });
          });
          </script>
          <?php } */ ?>
        <style>
            ul.pagination{
                margin:25px;
            }
            ul.pagination li{
                background-color: #f2f2f2;
                color:#000;
                font-size: 24px;
                padding: 5px 10px;
                text-align: center;
                border:1px solid #fff;
            }
        </style>
        <style>
            #DataTables_Table_0_filter{
                margin-left: 25px
            }
        </style>
    </head>
    <body>
        <?php if ($this->session->userdata("fashion_id")) { ?>
            <div class="interface">
                <div class="interface_sidebar bg-dark shadow-sm">
                    <div class="py-4 p-3 text-center mb-3 has-overlay bg-cover">
                        <img src="<?= base_url() . 'uploads/' . SITE_LOGO ?>" onerror="this.onerror=null; this.src='<?= STORE_DEFAULT_IMAGE ?>'" height="70" class="overlay-holder">
                    </div>
                    <ul class="accordion list-unstyled m-0" id="res-menu">
                        <li class="active"><a href="<?= base_url() ?>fashion_designer_manager/dashboard">Dashboard</a></li>
                        <?php if ($this->session->userdata('fashion_designation') == 'Fashion Analyst') { ?>

                            <li class="active"><a href="<?= base_url() ?>fashion_designer_manager/my_orders">My Orders</a></li>

                        <?php } else if ($this->session->userdata('fashion_designation') == 'Fashion Designer') { ?>

                            <li class="hassub">
                                <a href="#" data-toggle="collapse" data-target="#cid1" aria-expanded="<?= $active_main_page == "my_orders" ? "true" : "false" ?>">Orders</a>
                                <ul id="cid1" class="collapse <?= ($active_main_page == "my_orders") ? "show" : "false" ?> list-unstyled" data-parent="#res-menu">
                                    <li class="<?= $active_sub_page == "to_be_approved_orders" ? "active" : "" ?>"><a href="<?= base_url() ?>fashion_designer_manager/my_orders/to_be_approved">To be Approved</a></li>
                                    <li class="<?= $active_sub_page == "forwarded_to_admin" ? "active" : "" ?>"><a href="<?= base_url() ?>fashion_designer_manager/my_orders/forwarded_to_admin">Forwarded to admin</a></li>
                                    <li class="<?= $active_sub_page == "my_orders" ? "active" : "" ?>"><a href="<?= base_url() ?>fashion_designer_manager/my_orders">In Process</a></li>
                                </ul>
                            </li>

                        <?php } ?>


                        <li class="hassub">
                            <a href="#" data-toggle="collapse" data-target="#cid0" aria-expanded="<?= $active_main_page == "settings" ? "true" : "false" ?>">Settings</a>
                            <ul id="cid0" class="collapse <?= $active_main_page == "settings" ? "show" : "false" ?> list-unstyled" data-parent="#res-menu">
                                <li class="<?= $active_sub_page == "change_password" ? "active" : "" ?>"><a href="<?= base_url() ?>fashion_designer_manager/change_password">Change Password</a></li>
                            </ul>
                        </li>

                        <?php
                        if ($this->session->userdata("admin_control")) {
                            ?>
                            <li><a href="<?= base_url('admin/all_restaurants') ?>">Back to Admin</a></li>
                            <?php
                        } else {
                            ?>
                            <li><a href="<?= base_url('fashion_designer_manager/logout') ?>">Logout</a></li>
                            <?php
                        }
                        ?>

                    </ul>
                </div>
                <div class="interface_header bg-white shadow-sm py-2 px-3">
                    <div class="row align-items-center justify-content-center no-gutters">
                        <div class="col-9 col-md">
                            <div class="font-weight-bold h6 m-0">
                                <span onclick="expand_side_bar()"><img src="https://img.icons8.com/material-outlined/24/000000/menu--v4.png"/></span>
                                &nbsp;&nbsp;&nbsp;<?= $restaurant_details->restaurant_name ?>
                                <span class="text-primary">Hi <?= $this->session->userdata('fashion_name') ?>, Designation : <?= $this->session->userdata('fashion_designation') ?></span>
                                <?php if ($restaurant_details->is_own_store) { ?>
                                    Own By <span class="text-info"><?= SITE_TITLE ?></span>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="col-3 text-right d-block d-md-none"><button type="button" class="btn bg-transparent shadow-none interface_sidebar_toggle"><i class="fal fa-align-right h4 m-0 text-secondary"></i></button></div>
                        <div class="col-12 text-right d-block d-md-none">
                            <hr>
                        </div>

                        <div class="col-auto" ng-controller="noficationsCtrl" style="display: none">
                            <div class="dropdown d-inline-block">
                                <button class="btn bg-transparent shadow-none" data-toggle="dropdown">
                                    <i class="fal fa-bell h4 m-0 text-secondary"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right"
                                     style="max-height:450px; overflow-y:scroll;">
                                    <a href="#" class="dropdown-item" style="border-bottom:1px solid lightgray" ng-repeat="nitem in notificationsList track by $index">
                                        {{nitem.comment}} at {{nitem.display_time}}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php
                        if ($this->session->userdata("admin_control")) {
                            ?>
                            <div class="col-auto">
                                <button class="btn bg-transparent shadow-none" onclick="location.href = '<?= base_url() ?>admin/all_restaurants'">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col"><i class="fal fa-user h4 m-0 text-secondary"></i></div>
                                        <div class="col pl-1 d-none d-md-block">
                                            Back to Admin
                                        </div>
                                    </div>
                                </button>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="col-auto">
                                <button class="btn bg-transparent shadow-none" onclick="location.href = '<?= base_url() ?>fashion_designer_manager/logout'">
                                    <div class="row align-items-center no-gutters">
                                        <div class="col"><i class="fal fa-sign-out h4 m-0 text-secondary"></i></div>
                                        <div class="col pl-1 d-none d-md-block">
                                            LOGOUT
                                        </div>
                                    </div>
                                </button>
                            </div>
                            <?php
                        }
                        ?>


                    </div>
                </div>
                <div class="interface_body p-3">
                <?php } ?>
                <script>
                    function expand_side_bar() {
                    //interface_sidebar
                    if (document.getElementsByClassName("interface_header")[0].style.width == "85%") {
                    document.getElementsByClassName("interface_header")[0].style.width = "100%";
                    document.getElementsByClassName("interface_body")[0].style.width = "100%";
                    document.getElementsByClassName("interface_sidebar")[0].style.width = "0px";
                    document.getElementsByClassName("interface_sidebar")[0].style.display = "none";
                    } else {
                    document.getElementsByClassName("interface_header")[0].style.width = "85%";
                    document.getElementsByClassName("interface_body")[0].style.width = "85%";
                    document.getElementsByClassName("interface_sidebar")[0].style.width = "15%";
                    document.getElementsByClassName("interface_sidebar")[0].style.display = "block";
                    }
                    }
                </script>
                <style>
                    .interface_header, .interface_body{
                        width: 100%;
                        float: right;
                        transition: 0.5s;
                    }
                    .interface_sidebar{
                        transition: 1s;
                    }
                </style>