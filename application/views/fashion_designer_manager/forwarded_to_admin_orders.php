<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<div class="wrapper wrapper-content animated fadeInRight" ng-controller="myOrdersCtrl" ng-init="getForwardedToAdminOrders()">
    <div class="rm-small m-4">

        <div>
            <div class='col-md-12' ng-if='forwardedToAdminOrders.length == 0'>
                <div class="alert alert-danger" role="alert">
                    Oops! No data found.Please check back later!
                </div>
            </div>
            <table class="table table-striped table-bordered dataTables-example dataTable" ng-if="forwardedToAdminOrders.length > 0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Order id</th>
                        <th>Fashion Analyst details</th>
                        <th>Total amount</th>
                        <th>Order status</th>
                        <th>Action at</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in forwardedToAdminOrders track by $index">
                        <td>{{$index + 1}}</td>
                        <td>#{{item.unique_order_id}}</td>
                        <td><b>Name</b> - {{item.fashion_analyst_details.name}} <br> <b>Phone</b> - {{item.fashion_analyst_details.phone_number}}</td>
                        <td><i class="fa fa-inr"></i>{{item.final_total}}</td>
                        <td><h6> <span class="badge badge-{{item.order_status == 'order_completed' ? 'success' : item.order_status == 'order_cancelled' ? 'danger' :   item.order_status == 'order_placed_by_admin' ? 'info' : 'primary'}} p-1">{{item.order_status}}</span></h6></td>
                        <td>{{item.created_at}}</td>
                        <td>
                            <a href="javascript:void(0)" ng-click="showLogsModalPopup(item.unique_order_id)"><button class="btn btn-info text-white btn-xs">View Details</button></a>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>

    <div class="modal fade " id="order-logs-modal-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 98%;">
            <div class="modal-content">
                <div class="modal-header">
                    <div>
                        <h5>Orders Log</h5>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered" ng-if="orderLogsList.length > 0">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Comment</th>
                                    <th>Status</th>
                                    <th>Action done at</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in orderLogsList track by $index">
                                    <td>{{$index + 1}}</td>
                                    <td>{{item.log}}</td>
                                    <td>{{item.order_status}}</td>
                                    <td>{{item.created_at}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    unique_id = '<?= $this->input->get("order_id") ?>';</script>
<script src="<?= base_url() ?>vendor_assets/js/angular/fashion_designer/myOrdersCtrl.js?r=<?= time() ?>"></script>