<div class="container" ng-controller="changePasswordCtrl">
    <center>
        <form class="form col-4" style="text-align: left" id="change_pass" ng-submit="updatePassword()">
            <div class="form-group">
                <label class="control-label">Current Password</label>
                <input type="password" name="current_password" ng-model="myObj.current_password" class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">New Password</label>
                <input type="password" id="new_password" name="new_password" ng-model="myObj.new_password" class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">Re-type New Password</label>
                <input type="password" name="re_password" ng-model="myObj.re_password" class="form-control">
            </div>
            <div class="form-group" style="width: 100%">
                <button type="submit" class="btn btn-primary pull-right" style="width: 100%">Submit</button>
            </div>
        </form>
    </center>
</div>
<script src="<?= base_url() ?>vendor_assets/js/angular/fashion_designer/changePasswordCtrl.js"></script>