<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<div class="wrapper wrapper-content animated fadeInRight" ng-if="cartObj.unique_id" ng-init="getCartList(); checkAlreadyForwardedToFashionManager();">
    <div class="rm-small m-4">
        <div ng-if="pageObj.is_forwarded_to_fashion_manager">
            <div class="alert alert-info" role="alert">
                This order has already been forwarded to fashion manager.
            </div>
            <h5>Order logs</h5>
            <hr>

            <table class="table table-striped table-bordered" ng-if="orderLogsList.length > 0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Comment</th>
                        <th>Status</th>
                        <th>Action done at</th>

                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in orderLogsList track by $index">
                        <td>{{$index + 1}}</td>
                        <td>{{item.log}}</td>
                        <td>{{item.order_status}}</td>
                        <td>{{item.created_at}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div ng-if="!pageObj.is_forwarded_to_fashion_manager">
            <div class='col-md-12' ng-if='cartList.length == 0'>
                <div class="alert alert-danger" role="alert">
                    Oops! No products found in cart
                </div>
            </div>
            <table class="table table-striped table-bordered" ng-if="cartList.length > 0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Product name</th>
                        <th>Category & size</th>
                        <th>Available stock</th>
                        <th>Unit price</th>
                        <th>Quantity</th>
                        <th>Subtotal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in cartList track by $index">
                        <td>{{$index + 1}}</td>
                        <td>{{item.product_details.title}}<br>#{{item.product_details.product_id}}</td>
                        <td>{{item.category_name}} <br> <b>Size</b> - {{item.size}}</td>
                        <td>{{item.stock}}</td>
                        <td><i class="fa fa-inr"></i>{{item.unit_price}}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="button" ng-disabled="disableButton" ng-click="updateQuantity(item.quantity, item.id, item.product_inventory_id, 'remove')"  class="btn btn-danger btn-sm text-white">-</button>
                                </div> 
                                <div class="col-md-4">
                                    <b style="font-size: 18px;">{{item.quantity}}</b>
                                </div>
                                <div class="col-md-4">
                                    <button type="button" ng-disabled="disableButton" ng-click="updateQuantity(item.quantity, item.id, item.product_inventory_id, 'add')" class="btn btn-success  btn-sm text-white">+</button>
                                </div>
                            </div>
                        </td>
                        <td><i class="fa fa-inr"></i>{{item.subtotal}}</td>
                        <td>
                            <button class="btn btn-danger text-white btn-xs" ng-click="deleteCartItem(item.id)" ng-disabled="disableButton">Delete</button>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="col-md-4 pull-right" ng-if="cartList.length > 0">
                <table class="table table-bordered table-striped" ng-controller="shopForOrderCtrl">
                    <tr>
                        <th>Sub total</th>
                        <td><i class="fa fa-inr"></i>{{sub_total}}</td>
                    </tr>
                    <tr>
                        <th>Final total</th>
                        <td><i class="fa fa-inr"></i>{{final_amount_including_taxes}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>
                            <button class="btn btn-primary text-white btn-xs" ng-disabled="disableButton"  ng-click="forwardToFashionManager()">Place order and Forward to Fashion Manager!</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    unique_id = '<?= $this->input->get("order_id") ?>';</script>
<script src="<?= base_url() ?>vendor_assets/js/angular/fashion_designer/shopForOrderCtrl.js?r=<?= time() ?>"></script>