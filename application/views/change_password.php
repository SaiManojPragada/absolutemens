<div class="cms-subpage">
</div>
<div class="container-fluid">
    <?php if (!empty($data['message'])) { ?>
        <center>
            <?= $data['message'] ?>
        </center>
    <?php } else { ?>
        <center>
            <div class="col-md-4 card" style="padding: 30px 30px">
                <form method="post" action="<?= base_url('authentication/recover/' . $key) ?>" id="changePassForm">
                    <h4>CHANGE PASSWORD</h4><br><br>
                    <div>
                        <div style="width: 100%"><span id="new_pass_err"></span></div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fal fa-key"></i></span>
                            </div>
                            <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter New Password *">
                        </div><br>
                        <div style="width: 100%"><span id="re_new_pass_err"></span></div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fal fa-sync"></i></span>
                            </div>
                            <input type="password" name="re_new_password" id="re_new_password" class="form-control" placeholder="Re Enter New Password *">
                        </div><br>
                        <div class="form-group">
                            <button type="submit" name="submit" value="submit" class="btn btn-primary" ><i class="fal fa-paper-plane"></i> Change Password</button>
                        </div>
                    </div>
                </form>
            </div>
        </center>
    <?php } ?>
    <style>
        #new_pass_err, #re_new_pass_err {
            color: tomato;
            text-align: left;
            float: left
        }
    </style>
    <script>
        $("#changePassForm").submit(function (e) {
            var pass = document.getElementById("new_password").value;
            var re_pass = document.getElementById("re_new_password").value;
            var pass_reg = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)\S{6,50}$/);
            if (pass.length < 6) {
                $("#new_pass_err").html("* Password must be atleast 6 Characters");

                e.preventDefault();
                return false;
            } else {
                $("#new_pass_err").html("");
            }


            if (!pass_reg.test(pass)) {
                $("#new_pass_err").html("* Password should contain atleast one Capital letter,Small letter and minimum of 6 characters with no spaces");
                e.preventDefault();
                return false;
            } else {
                $("#new_pass_err").html("");
            }


            if (pass != re_pass) {
                $("#re_new_pass_err").html("* Passwords does not Match");

                e.preventDefault();
                return false;
            } else {
                $("#re_new_pass_err").html("");
            }
        });
    </script>
</div>