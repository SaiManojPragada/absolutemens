</div>
<div class="subpagebg">
</div>
<div class="">
    <br><br><br>
    <div class="container">

        <div class="row justify-content-center" style="background-color: #ffffff">
            <div class="col-lg-12" style="background-color: #ffffff">

                <div class="whitebox orderconfirm" style="background-color: #F7F7F7; height: auto">
                    <div class="col-12 row">
                        <?php if ($payment_err) { ?>
                            <div class="col-6" style="height: auto; padding: 0px">
                                <img src="<?= base_url() ?>assets/images/error_2.png" alt="alt" style="width: 100%"/>
                            </div>
                            <div class="col-6" id="failuremsg">
                                <strong>Oops !</strong><br>
                                <br><br>
                                <p>Something went wrong while Processing your Transaction.</p>
                                <p style="font-size: 12px"><b>Reference Id :</b> <input onclick="copyit(this)" id="refinp" value="DA876DAS87D9ASDA79ASDFFGDF" readonly/></p>
                                <p><small>If Amount is Deducted Please Contact Us <b><a href="<?= base_url('contact-us') ?>">here</a></b></small></p>
                            </div>
                        <?php } else { ?>
                            <div class="col-12 row" id="print_this_div">
                                <center class="col-12"><h2> Payment Successful</h2></center>
                                <p class="col-12"><span style="float: right"><b>Date : </b><?= date('d M y, h:i A', $existing->created_at) ?></span><p>
                                    <?php if (!$existing->is_we_shop_for_you) { ?>
                                    <div class="col-4" style="padding: 20px">
                                        <div class="single-banner mb-lg-3 mb-md-5 mb-3">
                                            <div class="banner-img">
                                                <a href="#"><img src="<?= base_url() ?>uploads/<?= $package_details->image ?>" alt="banner" /></a>
                                            </div>
                                            <div class="banner-content-3">
                                                <?= $package_details->title ?>
                                                <h5><?= $package_details->no_of_items ?> items / <?= $package_details->duration_in_words ?></h5>
                                                <p>Subscription Start from <?= (int) $package_details->price ?>/month</p>
                                                <?= $package_details->subscription_features ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-4" style="padding: 20px">
                                        <center>
                                            <h3>We Shop For You</h3>
                                        </center>
                                    </div>
                                <?php } ?>
                                <div class="col-8" style="padding: 20px">
                                    <p class="row"><b class="col-2">Name</b><span class="col-10">: <?= $existing->address_name ?></span></p>
                                    <p class="row"><b class="col-2">Address</b><span class="col-10">: <?= $existing->address ?></span></p>
                                    <p class="row"><b class="col-2">City</b><span class="col-10">: <?= $existing->city ?></span></p>
                                    <p class="row"><b class="col-2">State</b><span class="col-10">: <?= $existing->state ?></span></p>
                                    <p class="row"><b class="col-2">District</b><span class="col-10">: <?= $existing->district ?></span></p>
                                    <hr>
                                    <div class="row">
                                        <div class="col-6 row" style="border-right: 1px solid #fff">
                                            <h3 class="col-12">Transaction Details</h3>
                                            <p class="row"><b class="col-10">Transaction Id :</b><span class="col-12" style="word-break: break-all"> <?= $existing->transaction_id ?></span></p>
                                        </div>
                                        <div class="col-6 row" id="price_div">
                                            <p class="col-12"><strong> No of Items <small>(<?= $package_details->no_of_items ?> items)</small></strong></p>
                                            <b class="col-7">Total Price :</b><span class="col-5"> <i class="fal fa-rupee-sign"></i> <?= $existing->total_price ?></span>
                                            <b class="col-7">Discount :</b><span class="col-5"> - <i class="fal fa-rupee-sign"></i> <?= ($existing->discount) ? $existing->discount : 0 ?></span>

                                            <b class="col-7">Delivery Charges    :</b><span class="col-5"> + <i class="fal fa-rupee-sign"></i> <?= $existing->delivery_charges > 0 ? $existing->delivery_charges : 0 ?></span>
                                            <b class="col-7">Coupon Discount :</b><span class="col-5"> - <i class="fal fa-rupee-sign"></i> <?= ($existing->coupon_discount) ? $existing->coupon_discount : 0 ?></span>
                                            <b class="col-7">Grand Total :</b><span class="col-5"><i class="fal fa-rupee-sign"></i> <?= $existing->grand_total ?></span>

                                            <b class="col-7">Payment Status :</b><span class="col-5"> <?php if ($existing->status) { ?>
                                                    <span style="color: green; font-weight: bold;">Paid</span>
                                                <?php } else { ?><span style="color: tomato; font-weight: bold">Unpaid</span><?php } ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <a style="float: right; color: #0857C0;" href="javascript: void(0);" onclick="print_this_div();"><i class="fal fa-print"></i> Print</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    function print_this_div() {
        window.print();
    }

</script>
<style>
    h2{
        font-weight: 100;
        color: #0857C0;
    }
    #price_div *{
        text-align: right
    }
    #refinp{
        border: 0px;
    }
    #failuremsg{
        font-family: "Snell Roundhand", cursive;
        padding: 120px 0px 0px 0px;
        letter-spacing: 1px;
    }
    #failuremsg strong{
        font-size: 40px;
        font-family: "Papyrus", "Courier New", monospace;
    }
</style>
<script>
    function copyit(copyText) {

        copyText.select();
        copyText.setSelectionRange(0, 99999);
        navigator.clipboard.writeText(copyText.value);
        alert("Copied the text: " + copyText.value);
    }
</script>