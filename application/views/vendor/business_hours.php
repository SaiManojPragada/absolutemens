<div ng-controller="businessHoursCtrl">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Business Hours</strong></div>
                <!-- <div class="col-auto">
                    <a ng-click="saveTimings()" class="btn btn-link btn-sm"><strong>Save</strong>
                      <i class="fal fa-plus fa-fw"></i></a>
                </div> -->
            </div>
        </div>
        <div class="card-body p-0">

            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Week</th>
                        <th>Working</th>
                        <th>From Time</th>
                        <th>To Time</th>
                    </tr>
                </thead>
                <tbody>
                  <form id="restaurant_business_hours_form" ng-submit="saveTimings()">
                    <tr ng-repeat ="data in dataList">
                        <td>{{$index+1}}</td>
                        <td>
                          {{data.week_name}}
                              <!-- <input type="text" required readonly class="form-control" name="name" ng-model="data.week_name"> -->
                        </td>
                        <td>
                          <div class="form-check form-check-inline">
                              <input id="is_working_day1{{$index}}" name="is_working_day{{$index}}" type="radio" ng-checked="data.is_working_day=='1'" ng-model="data.is_working_day" class="form-check-input" ng-value="'1'">
                              <label for="is_working_day1{{$index}}" class="form-check-label">Yes</label>
                          </div>
                          <div class="form-check form-check-inline">
                              <input id="is_working_day2{{$index}}" name="is_working_day{{$index}}" type="radio" ng-checked="data.is_working_day=='0'" ng-model="data.is_working_day" class="form-check-input" ng-value="'0'">
                              <label for="is_working_day2{{$index}}" class="form-check-label">No</label>
                          </div>
                        </td>
                        <td>
                              <input type="text" ng-required="data.is_working_day=='1'" class="form-control timepickerNg"    name="from_time" placeholder="HH:mm:ss" ng-model="data.from_time">
                        </td>
                        <td>
                          <input type="text"  ng-required="data.is_working_day=='1'" class="form-control timepickerNg"  name="to_time" placeholder="HH:mm:ss" ng-model="data.to_time">
                        </td>
                    </tr>
                    <tr>
                      <td colspan="5">
                        <div class="col-auto">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                      </td>
                    </tr>
                </tbody>
                <form>
            </table>
        </div>
    </div>

</div>
<script src="<?= base_url() ?>vendor_assets/js/angular/businessHoursCtrl.js"></script>
