<div ng-controller="productsImagesCtrl">
    <div class="card shadow-sm border-0 mixwithbg" ng-init="getProductImages(<?= $id ?>)">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Product Images</strong></div>
                <div class="col-auto">
                    <a ng-click="addProductImage()"  data-toggle="modal" class="btn btn-link btn-sm"><strong>Add</strong> <i class="fal fa-plus fa-fw"></i></a>
                </div>
            </div>
        </div>

        <div class="card-body p-0">
            <div class="container-fluid" ng-if="images.length == 0">
                <h4 class="text-center ">No Images found</h4>
            </div>

            <div class="list-group list-group-flush">
                <div class="list-group-item" >
                    <div class="row align-items-center">
                        <div class="col-1">#</div>
                        <div class="col-4">Product Name</div>
                        <div class="col-4">Image</div>
                        <div class="col-2">Action</div>
                    </div>
                </div>
            </div>
            <div class="list-group-item" clone="" ng-repeat="image in images">
                <div class="row align-items-center">
                    <div class="col-md-1">{{$index + 1}}</div>
                    <div class="col-4">
                        <p><b>Product Id : </b> {{image.prodcut_details.product_id}}</p>
                        <p><b>Product Name : </b> {{image.prodcut_details.title}}</p>
                    </div>
                    <div class="col-3"><img src="<?= base_url() ?>{{image.image}}" width="100"/></div>

                    <div class="col-2">
                        <center>
                            <button class="btn btn-danger btn-xs" ng-click="deleteImage(image)">delete</button>

                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="productImageModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="products_images_form" ng-submit="addUpdateProductImage()" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{item.id?"Update":"Add"}} Product Image</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-4">

                        <div class="row">
                            <div class="col-12">
                                <div class="row rm-small">
                                    <div class="col-12 cp-small">
                                        <div class="form-group">
                                            <label>Select Image <small>(Note: Image Width: 520 x 620)</small></label>
                                            <input type="file" class="form-control" name="image" accept="image/*" ng-model="block.image" base-sixty-four-input>
                                        </div>
                                    </div>

                                    <input type='hidden' name="product_id" ng-model="block.product_id" value='<?= $id ?>'>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">{{item.id?"Update":"Add"}}</button>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script src="<?= base_url() ?>vendor_assets/js/angular/productsImagesCtrl.js?r=<?= time() ?>"></script>