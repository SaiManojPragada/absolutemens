<section ng-controller="toastAlertsCtrl">
    <style>
        #snackbar {
            visibility: hidden;
            min-width: 250px;
            max-width: 250px;
            text-align: justify;
            margin-left: -125px;
            background-color: #333;
            color: #fff;
            text-align: center;
            border-radius: 2px;
            padding: 16px;
            position: fixed;
            z-index: 1;
            right: 0%;
            bottom: 30px;
            font-size: 17px;
        }

        #snackbar.show {
            visibility: visible;
            -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
            animation: fadein 0.5s, fadeout 0.5s 2.5s;
        }

        #snackbar a{
            color: lightgray;
        }
        #snackbar a:hover{
            color: white;
        }

        @-webkit-keyframes fadein {
            from {bottom: 0; opacity: 0;} 
            to {bottom: 30px; opacity: 1;}
        }

        @keyframes fadein {
            from {bottom: 0; opacity: 0;}
            to {bottom: 30px; opacity: 1;}
        }

        @-webkit-keyframes fadeout {
            from {bottom: 30px; opacity: 1;} 
            to {bottom: 0; opacity: 0;}
        }

        @keyframes fadeout {
            from {bottom: 30px; opacity: 1;}
            to {bottom: 0; opacity: 0;}
        }
    </style>

    <div id="snackbar" ng-bind-html="toastMsg"> 
    </div>

   <!-- <audio id="newOrderReceivedSound" src="<?= base_url() ?>admin_assets/new_order_received.mp3" preload="auto"></audio> -->
    <audio id="newOrderReceivedSound" src="<?= base_url() ?>vendor_assets/notification_sound.mp3" preload="auto"></audio>
</section>
<script src="<?= base_url() ?>vendor_assets/js/angular/toastAlertsCtrl.js?r=<?= time() ?>"></script>