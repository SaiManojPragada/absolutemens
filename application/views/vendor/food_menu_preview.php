<div class="card shadow-sm border-0 mixwithbg" ng-controller="restaurantFoodMenuPreviewCtrl">
    <div class="card-header"><strong>Product Menu Preview</strong></div>
    <div class="card-body p-0">
        <div class="row">
            <div class="col-md-auto">
                <div class="food-menu2">
                    <ul class="nav flex-nowrap flex-md-wrap flex-md-column p-4" role="tablist" >
                        <li  class="nav-item" ng-repeat="menu in foodMenu">
                            <a ng-click="showFoodItem(menu)" class="nav-link px-2 px-md-0" href="#m0" role="tab"  aria-selected="false">{{menu.name}}</a>
                        </li>
                    </ul>
                </div>
            </div> 
            <div class="col-md">
                <div class="tab-content border-left p-4" >
                    <div class="checkbox-toggle" style="float:right">
                        <label>
                            <input type="checkbox" ng-model="foodItem.available" value="" ng-change="UpdateRestaurantCategoryStatus(foodItem)">
                            <div class="ct-holder">
                                <div class="ct-slider">
                                    <div>Sold out<i class="fal fa-times ml-1"></i></div>
                                    <div>Available<i class="fal fa-check ml-1"></i></div>
                                </div>
                            </div>
                        </label>
                    </div>

                    <h6 class="text-primary">{{foodItem.name}}</h6>

                    <p align='center' class="text-danger" ng-if="foodItem.food_items.length == 0" >
                        No Items Added
                    </p>
                    <div ng-if="foodItem.food_items.length != 0" class="list-group list-group-flush mb-3" ng-repeat="item in foodItem.food_items">
                        <div class="list-group-item px-0">
                            <div class="row align-items-center">
                                <div class="col-auto">
                                    <img ng-src="{{item.image}}" class="rounded" width="60">
                                </div>
                                <div class="col">
                                    <strong class="d-block">{{item.item_name}}</strong>
                                    <?php if ($attachment_required != "yes") { ?>
                                        <span class="badge badge-success" ng-if="foodItem.food_type === 'Veg'">{{foodItem.food_type}}</span>
                                        <span class="badge badge-danger" ng-if="foodItem.food_type === 'Non Veg'">{{foodItem.food_type}}</span>
                                    <?php } ?>
                                    <small>{{item.description}}</small>
                                </div>
                                <div class="col-auto">

                                    <div class="checkbox-toggle" style="float:right">
                                        <label>
                                            <input type="checkbox" ng-model="item.available" value="" ng-change="UpdateRestaurantFoodItemStatus(item)">
                                            <div class="ct-holder">
                                                <div class="ct-slider">
                                                    <div>Sold out<i class="fal fa-times ml-1"></i></div>

                                                    <div>Available<i class="fal fa-check ml-1"></i></div>
                                                </div>
                                            </div>

                                        </label>

                                    </div>
                                    <br/>
                                    <span ng-if="item.item_availability == 'Sold Out'" class="text-danger">{{item.next_availability}}</span>

                                </div>
                                <div class="col-auto"><small class="fal fa-rupee-sign fa-fw"></small>{{item.actual_price}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url() ?>vendor_assets/js/angular/restaurantFoodMenuPreviewCtrl.js?r=<?= time() ?>"></script>
