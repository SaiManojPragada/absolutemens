<div class="card shadow-sm border-0 mixwithbg" ng-controller="withdrawCtrl" ng-init="getWalletBalance()">
    <div class="card-header">
        <strong>New Withdraw Request</strong>
    </div>
    <div class="card-body">
        <br>
        <div class="row rm-small">
            <div class="col-4 col-md-4 col-lg cp-small mb-3 text-center">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <span class="fa-stack fa-2x d-block mx-auto">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fal fa-wallet fa-stack-1x fa-inverse"></i>
                        </span>
                        <small class="d-block my-2">Original Wallet amount</small>
                        <h4 class="m-0">{{wallet_details.current_wallet_balance}}</h4>
                    </div>
                </div>
            </div>
            <div class="col-4 col-md-4 col-lg cp-small mb-3 text-center">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <span class="fa-stack fa-2x d-block mx-auto">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fal fa-wallet fa-stack-1x fa-inverse"></i>
                        </span>
                        <small class="d-block my-2">OnHold Wallet amount [ For Withdrawl ]</small>
                        <h4 class="m-0"><span> ( <i class="fa fa-minus" style="color:red"></i> ) </span>{{wallet_details.onhold_wallet_amount}}</h4>
                    </div>
                </div>
            </div>
            <div class="col-4 col-md-4 col-lg cp-small mb-3 text-center">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <span class="fa-stack fa-2x d-block mx-auto">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fal fa-wallet fa-stack-1x fa-inverse"></i>
                        </span>
                        <small class="d-block my-2">Current Wallet amount</small>
                        <h4 class="m-0">{{wallet_details.current_wallet_balance - wallet_details.onhold_wallet_amount}}</h4>
                    </div>
                </div>
            </div>
        </div>
        <form id="withdrawRequestForm" method="get" class="w-500 mx-auto" ng-submit="submitWithdrawlRequest()">
            <div class="row rm-small">
                <div class="col-12 cp-small">
                    <div class="form-group">
                        <label>Withdraw amount</label>
                        <input type="text" class="form-control" ng-model="withdrawObj.amount" name="amount" required>
                    </div>
                </div>
                <div class="col-12 cp-small"><button type="submit" class="btn btn-primary text-uppercase px-5"><strong>Update</strong></button></div>
            </div>
        </form>
    </div>
</div>

<script src="<?= base_url() ?>vendor_assets/js/angular/withdrawCtrl.js"></script>
