<div ng-controller="restaurantCategoriesCtrl" ng-init="getCategories()">
    <div class="card shadow-sm border-0 mixwithbg"> 
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Categories</strong></div>

                <?php if ($this->session->userdata("admin_control")) { ?>
                    <div class="col-auto">
                        <a ng-click="showModal(0)" class="btn btn-link btn-sm"><strong>Add</strong> <i class="fal fa-plus fa-fw"></i></a>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="card-body p-0"> 
            <br/>
            <div class="container-fluid">
                <form class="form" ng-submit="getCategories()"> 
                    <div class="row">
                        <div class="form-group col-md-3"> 
                            <label for="category"><strong>Category Name</strong></label>
                            <input type="text" class="form-control" name="search_key" ng-model="filterObj.search_key" placeholder="Search By Category Name">
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary" style="margin-top: 28px">Submit</button>
                            <button type="button" ng-click="filterObj.search_key = '';getCategories()" class="btn btn-danger" style="margin-top: 28px">Clear</button> 
                        </div>
                    </div>
                </form>
            </div>


            <div class="list-group list-group-flush">
                <h3 class="text-center" ng-if="categories.length == 0">No categories found</h3>
                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <strong class="text-head">Category ID</strong>
                        </div>
                        <?php if ($restaurant_details->is_own_store) { ?>
                            <div class="col">
                                <strong class="text-head">Image</strong>
                            </div>
                        <?php } ?>
                        <div class="col">
                            <strong class="text-head">Category Name</strong>
                        </div>
                        <div class="col">
                            <strong class="text-head">Admin Commission (%)</strong>
                        </div>
                        <div class="col text-center">
                            <strong class="text-head ">Timings</strong>
                        </div>
                        <div class="col-auto">
                            <strong class="text-head">Action</strong>
                        </div>
                    </div>
                </div>
                <div class="list-group-item" href="#section0" ng-repeat ="category in categories">
                    <div class="row align-items-center">
                        <div class="col">
                            <strong class="text-head">{{category.id}}</strong>
                        </div>
                        <?php if ($restaurant_details->is_own_store) { ?>
                            <div class="col">
                                <img ng-src="{{category.image}}" class="rounded" width="60">
                            </div>
                        <?php } ?>
                        <div class="col">
                            <strong class="text-head">{{category.name}} {{category.is_recommened == 1 ? "(recommended)" : ""}}</strong>
                        </div>
                        <div class="col">
                            <strong class="text-head">{{category.admin_commission_percentage}} %</strong>
                        </div>
                        <div class="col">
                            <div  class="col-12 col-md mt-2 mt-md-0 text-md-center text-left">
                                <button ng-click="manageTimings(category)" class="btn btn-primary btn-sm">Manage Timings</button>
                                <br/>
                                <span ng-if="category.has_timings == 'no'" class="text-danger">No Timings</span>
                            </div>

                        </div>
                        <div class="col-auto">
                            <?php if ($this->session->userdata("admin_control")) { ?>
                                <a ng-click="showModal(1, category)" class="btn btn-link btn-info text-secondary fal fa-pen"></a>
                                <a ng-click="deleteCategory(category)" class="btn btn-link btn-danger text-secondary fal fa-trash"></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="restaurant_category_form" ng-submit="addUpdateCategory()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{title}} Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" ng-model="category.name">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Priority</label>
                                    <input type="number" string-to-number class="form-control" name="priority" ng-model="category.priority">
                                </div>
                            </div>


                            <?php if ($restaurant_details->is_own_store) { ?>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Category Image (Width:300px; height:300px)</label>
                                        <div class="custom-file">
                                            <input type="file" name="image" file-model="category.image">
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php
                            if ($this->session->userdata("admin_control")) {
                                ?>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Commission to Admin %</label>
                                        <input type="number" string-to-number class="form-control" name="admin_commission_percentage" ng-model="category.admin_commission_percentage">
                                    </div>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Commission to Admin %</label>
                                        <br/>
                                        <span class="h5">{{category.admin_commission_percentage}}</span>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="col-12">
                                <label for=""> Is Recommended</label> &nbsp;
                                <div class="form-check form-check-inline">
                                    <input id="is_recommened1" name="is_recommened" type="radio" ng-model="category.is_recommened" class="form-check-input" value="1">
                                    <label for="is_recommened1" class="form-check-label">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input id="is_recommened2" name="is_recommened" type="radio" ng-model="category.is_recommened" class="form-check-input" value="0">
                                    <label for="is_recommened2" class="form-check-label">No</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">{{title| uppercase}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="categoryTimingsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="category_timings_form" ng-submit="addCategoryTimings()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Manage Availability timings for <strong>{{categoryItem.name}}</strong></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-4">
                        <!--<div class="row">
                            <div class="col-12">
                                <div class="row rm-small">
                                    <div class="col-5 cp-small">
                                        <div class="form-group">
                                            <label>Available From Time </label>
                                            <input type="text" autocomplete="off" required class="form-control timepickerNg" name="from_time" placeholder="HH:mm:ss" ng-model="categoryItem.from_time">
                                        </div>
                                    </div>
                                    <div class="col-5 cp-small">
                                        <div class="form-group">
                                            <label>Available To Time </label>
                                            <input type="text" autocomplete="off" required class="form-control timepickerNg" name="to_time" placeholder="HH:mm:ss" ng-model="categoryItem.to_time">
                                        </div>
                                    </div>
                                    <div class="col-2 cp-small">
                                        <button type="submit" style="margin-top: 26px" class="btn btn-primary">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div><hr/>-->

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Week Name</th>
                                        <th>Schedule</th>
                                        <th>Available From Time</th>
                                        <th>Available To Time</th>
                                      <!--  <th>Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat ="item in categoryItem.timings">
                                        <td rowspan="3" style="vertical-align: middle; text-align: center" ng-if="$index % 3 == 0">{{item.week_name}}</td>
                                        <td>{{item.schedule}}</td>
                                        <td>
                                            <input type="text" autocomplete="off" class="form-control timepickerNg" name="from_time[]" placeholder="HH:mm:ss" ng-model="item.from_time">
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" class="form-control timepickerNg" name="to_time[]" placeholder="HH:mm:ss" ng-model="item.to_time">
                                        </td>
                                      <!--  <td>
                                           <button type="button" ng-click="deleteCategoryTimings(item)"   class="btn btn-danger" >Delete</button>
                                        </td> -->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-9 cp-small">
                            <!-- Are you sure Apply same timings for the items related to categories-->
                        </div> 
                        <div class="col-3 cp-small">
                            <button type="submit" style="margin-top: 26px" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>


<script src="<?= base_url() ?>vendor_assets/js/angular/restaurantCategoriesCtrl.js?r=<?= time() ?>"></script>
