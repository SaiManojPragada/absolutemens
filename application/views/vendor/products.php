<div ng-controller="productsCtrl" ng-init="getProducts()">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Product Items</strong></div>
                <div class="col-auto">
                    <a ng-click="addProduct()"  data-toggle="modal" class="btn btn-link btn-sm"><strong>Add</strong> <i class="fal fa-plus fa-fw"></i></a>
                </div>
            </div>
        </div>
        <div class="card-body p-0">
            <br/>
            <div class="container-fluid">
                <form class="form" ng-submit="searchItems()">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="category"><strong>Enter Key</strong></label>
                            <input type="text" class="form-control" name="key" ng-model="filterObj.key" placeholder="Search By Product Name, Id, brand">
                        </div>
                        <div class="form-group col-md-2">
                            <label><strong>Product Availability</strong></label>
                            <select ng-model="filterObj.avail" ng-selected="filterObj.avail" class="form-control">
                                <option value="">All</option>
                                <option value="1">In Stock</option>
                                <option value="0">Out Of Stock</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary" style="margin-top: 28px">Submit</button>
                            <a class="btn btn-danger" href="<?= base_url('vendor/products') ?>" style="margin-top: 28px">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
            <br/>
            <div class="container-fluid" ng-if="products.length == 0">
                <h4 class="text-center ">No items found</h4>
            </div>
            <div class="list-group list-group-flush">
                <div class="list-group-item" >
                    <div class="row align-items-center">
                        <div class="col-1">#</div>
                        <div class="col-3">Product Details</div>
                        <div class="col-1">Category/Subcategory</div>
                        <div class="col-2">Product Brand</div>
                        <div class="col-1">Manage Inventory</div>
                        <div class="col-2">Product Status</div>
                        <div class="col-2">Product Action</div>
                    </div>
                </div>
                <div class="list-group-item" clone="" ng-repeat="product in products track by $index">
                    <div class="row align-items-center">
                        <div class="col-1">{{$index + 1}}</div>
                        <div class="col-3">
                            <p><b>Product Id : </b> {{product.product_id}}</p>
                            <p><b>Product Name : </b> {{product.title}}</p>
                        </div>
                        <div class="col-1">{{product.category}} <hr/> {{product.sub_category}}</div>
                        <div class="col-2">{{product.brand}}</div>
                        <div class="col-1"><button class="btn btn-warning btn-xs" style="color: white" ng-click="manageQuantity(product)">Manage Inventory</button></div>
                        <div class="col-2" ng-if="product.status === '1'"><center><b><span style="color: green">Active</span></b></center></div>
                        <div class="col-2" ng-if="product.status === '0'"><center><b><span style="color: tomato">InActive</span></b></center></div>

                        <div class="col-2" id="actions">
                            <button class="btn btn-primary btn-xs" ng-click="EditProduct(product)">Edit</button>
                            <button class="btn btn-danger btn-xs" ng-click="deleteProduct(product)">Delete</button>
                            <a href="<?= base_url('vendor/view_product/view/') ?>{{product.id}}"><button class="btn btn-secondary btn-xs">View Product</button></a>
                            <a href='<?= base_url('vendor/products_images/view/') ?>{{product.id}}'>
                                <button type="button" class="btn btn-success" >Manage Images</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #actions a, #actions button{
            margin-top: 4px;
        }
    </style>
    <div class="modal fade" id="quantityModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document" style="max-width: 94%;">
            <div class="modal-content">
                <form id="quantity_form" ng-submit="addProductQuantity()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Manage Product Quantity</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-header">
                        <p style="font-size: 18px">Add Inventory</p>
                    </div>
                    <input type="hidden" ng-model="newinv.product_id" name="product_id">
                    <input type="hidden" ng-model="newinv.category_id" name="category_id">
                    <div class="row" style="padding: 20px 40px">
                        <div class="col-3 cp-small" ng-if="!newinv.id">
                            <div class="form-group">
                                <label>Size</label>
                                <select class="custom-select chosen-select" name="sizes_id" ng-model="newinv.sizes_id">
                                    <option value="">Select Size</option>
                                    <option value="{{size.id}}" ng-repeat="size in sizes">{{size.title}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-3 cp-small">
                            <div class="form-group">
                                <label>Price</label>
                                <input type="number" class="form-control" name="price"  ng-model="newinv.price">
                            </div>
                        </div>
                        <div class="col-3 cp-small" ng-if="!newinv.id">
                            <div class="form-group">
                                <label>Quantity</label>
                                <input type="number" class="form-control" name="quantity"  ng-model="newinv.quantity">
                            </div>
                        </div>
                        <div class="col-3 cp-small">
                            <div class="form-group">
                                <label><br></label>
                                <button type="submit" class="btn btn-xs btn-primary form-control">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!--products_inventory-->
                <div class="modal-body p-4">
                    <div class="container-fluid" ng-if="inventory.length < 1">
                        <h4 class="text-center ">No Inventory found</h4>
                    </div>
                    <div class="list-group list-group-flush">
                        <div class="list-group-item" >
                            <div class="row align-items-center">
                                <div class="col-2">#</div>
                                <div class="col-2">Size</div>
                                <div class="col-2">Price</div>
                                <div class="col-2">Quantity</div>
                                <div class="col-4">Actions</div>
                            </div>
                        </div>
                    </div>
                    <div class="list-group-item" clone="" ng-repeat="inv in inventory">
                        <div class="row align-items-center">
                            <div class="col-2">{{$index + 1}}</div>
                            <div class="col-2">{{inv.size}}</div>
                            <div class="col-2">{{inv.price}}</div>
                            <div class="col-2">{{inv.quantity}}</div>
                            <div class="col-4">
                                <button class="btn btn-primary btn-xs" ng-click="editProductInventory(inv)">Edit</button>
                                <button class="btn btn-danger btn-xs" ng-click="deleteProductInventory(inv)">Delete</button>
                                <a class="btn btn-warning btn-xs text-white" href="<?= base_url() ?>vendor/stock_management/view/{{inv.id}}" target="_blank">Stock Management</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="products_form" name="products_form" ng-submit="addUpdateProduct()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{item.id?"Update":"Add"}} Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="row rm-small">
                                    <div class="col-12 col-md-12 cp-small">
                                        <h5><b>Product Code :</b> {{item.product_id}}</h5>
                                    </div>
                                    <input type="hidden" ng-model="item.product_id">
                                    <div class="col-12 col-md-6 cp-small">
                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="custom-select" name="categories_id" ng-model="item.categories_id" ng-change="getSubcategories(item.categories_id)">
                                                <option value="">Select Category</option>
                                                <option value="{{category.id}}" ng-repeat="category in categories">{{category.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 cp-small">
                                        <div class="form-group">
                                            <label>Sub Categories</label>
                                            <select class="custom-select" name="sub_categories_id" ng-model="item.sub_categories_id">
                                                <option value="">Select Sub Category</option>
                                                <option value="{{subcategory.id}}" ng-repeat="subcategory in sub_categories">{{subcategory.title}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6  cp-small">
                                        <div class="form-group">
                                            <label>Item Name</label>
                                            <input type="text" class="form-control" name="title" ng-pattern="/^[A-Za-z0-9 ]+$/" ng-model="item.title" value="{{item.title}}">
                                            <span ng-show="products_form.title.$error.pattern" style="color: #B00020">Only Alphanumeric Values are Allowed and length should be Atleast 3 Characters</span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6  cp-small">
                                        <div class="form-group">
                                            <label>Brand</label>
                                            <input type="text" class="form-control" name="brand" ng-pattern="/^[A-Za-z0-9 ]+$/" ng-model="item.brand" value="{{item.brand}}">
                                            <span ng-show="products_form.brand.$error.pattern" style="color: #B00020">Only Alphanumeric Values are Allowed and length should be Atleast 3 Characters</span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6  cp-small">
                                        <div class="form-group">
                                            <label>Brand Code <small>(Ex: LP for Louis Philip, Used for Product Code)</small></label>
                                            <input type="text" class="form-control" name="brand_code" ng-pattern="/^[a-zA-Z0-9]{3,7}$/" required ng-model="item.brand_code" ng-change="generateProductCode()" value="{{item.brand_code}}">
                                            <span ng-show="products_form.brand_code.$error.pattern" style="color: #B00020">Only Alphanumeric Values are Allowed and length should be 3 to 6 Charecters</span>
                                        </div>
                                    </div>
                                    <div class="col-12 cp-small">
                                        <div class="form-group">
                                            <label>Description </label>
                                            <textarea class="form-control" rows="5" name="description" ng-model="item.description">{{item.description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 cp-small">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <select class="custom-select" name="status" ng-model="item.status">
                                                <option value="">Select Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">InActive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">{{item.id?"Update":"Add"}}</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <script>
                setTimeout(function () {
                    document.getElementById("chosen-select").chosen();
                }, 3000);
    </script>
</div>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 40px;
        height: 24px;
    }
    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }
    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }
    .slider:before {
        position: absolute;
        content: "";
        height: 22px;
        width: 22px;
        left: 1px;
        bottom: 1px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }
    input:checked + .slider {
        background-color: #2196F3;
    }
    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }
    input:checked + .slider:before {
        -webkit-transform: translateX(16px);
        -ms-transform: translateX(16px);
        transform: translateX(16px);
    }
    .slider.round {
        border-radius: 34px;
    }
    .slider.round:before {
        border-radius: 50%;
    }
</style>
<script src="<?= base_url() ?>vendor_assets/js/angular/productsCtrl.js?r=<?= time() ?>"></script>
