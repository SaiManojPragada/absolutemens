<div ng-controller="dashboardCtrl" ng-init="getDashboardData()" style="font-size: 15px">
    <!--    <div class="alert alert-danger" ng-hide="is_online">
            <strong>Alert!</strong> Right now your in offline Mode, please swith to online mode to receive orders.
        </div>-->

    <div class="row rm-small">
        <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center" style="cursor: pointer;" onclick="location.href = '<?= base_url() ?>vendor/orders/view'">
            <div class="card shadow-sm border-0">
                <div class="card-body">
                    <span class="fa-stack fa-2x d-block mx-auto">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fal fa-shopping-bag fa-stack-1x fa-inverse"></i> 
                    </span>
                    <small class="d-block my-2">TOTAL ORDERS</small>
                    <h4 class="m-0"><?= $total_orders ?></h4>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center" style="cursor: pointer;" onclick="location.href = '<?= base_url() ?>vendor/orders/view/delivered'">
            <div class="card shadow-sm border-0">
                <div class="card-body">
                    <span class="fa-stack fa-2x d-block mx-auto">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fal fa-check fa-stack-1x fa-inverse"></i>
                    </span>
                    <small class="d-block my-2">COMPLETED ORDERS</small>
                    <h4 class="m-0"><?= $completed_orders ?></h4>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center" style="cursor: pointer;" onclick="location.href = '<?= base_url() ?>vendor/orders/view/pending'">
            <div class="card shadow-sm border-0">
                <div class="card-body">
                    <span class="fa-stack fa-2x d-block mx-auto">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fal fa-times fa-stack-1x fa-inverse"></i>
                    </span>
                    <small class="d-block my-2">PENDING ORDERS</small>
                    <h4 class="m-0"><?= $pending_orders ?></h4>
                </div>
            </div>
        </div>

        <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center" style="cursor: pointer;" onclick="location.href = '<?= base_url() ?>vendor/earnings'">
            <div class="card shadow-sm border-0">
                <div class="card-body">
                    <span class="fa-stack fa-2x d-block mx-auto">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fal fa-rupee-sign fa-stack-1x fa-inverse"></i>
                    </span>
                    <small class="d-block my-2">TOTAL EARNINGS</small>
                    <h4 class="m-0"><?= ($total_earning->amount) ? $total_earning->amount : 0 ?></h4>
                </div>
            </div>
        </div>

    </div>
    <!--    <div class="row rm-small">
            <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center">
                <div class="card shadow-sm border-0">
                    <a href="<?= base_url() ?>vendor/food_items">
                        <div class="card-body">
                            <span class="fa-stack fa-2x d-block mx-auto">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fal fa-align-left fa-stack-1x fa-inverse"></i>
                            </span>
                            <small class="d-block my-2">Total Menu Items</small>
                            <h4 class="m-0">{{dashboard.total_food_menu_items||0}}</h4>
                        </div> 
                    </a>
                </div>
            </div>
    
            <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center">
                <div class="card shadow-sm border-0">
                    <div class="card-body">
                        <span class="fa-stack fa-2x d-block mx-auto">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fal fa-align-left fa-stack-1x fa-inverse"></i>
                        </span>
                        <small class="d-block my-2">On Availability</small>
                        <h4 class="m-0">{{dashboard.total_avilable_items||0}}</h4>
                    </div> 
                </div>
            </div>
    
            <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center">
                <div class="card shadow-sm border-0">
                    <div class="card-body ">
                        <span class="fa-stack fa-2x d-block mx-auto">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fal fa-align-left fa-stack-1x fa-inverse "></i>
                        </span>
                        <small class="d-block my-2">Sold Out</small>
                        <h4 class="m-0">{{dashboard.total_sold_out_items||0}}</h4>
                    </div> 
                </div>
            </div>
    
            <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center">
                <div class="card shadow-sm border-0">
                    <a href="<?= base_url() ?>vendor/food_items?verification_status=Approved">
                        <div class="card-body ">
                            <span class="fa-stack fa-2x d-block mx-auto">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fal fa-align-left fa-stack-1x fa-inverse "></i>
                            </span>
                            <small class="d-block my-2">Total Approved Items</small>
                            <h4 class="m-0">{{dashboard.total_approved_items||0}}</h4>
                        </div> 
                    </a>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg cp-small mb-3 text-center">
                <div class="card shadow-sm border-0">
                    <a href="<?= base_url() ?>vendor/food_items?verification_status=Pending">
                        <div class="card-body ">
                            <span class="fa-stack fa-2x d-block mx-auto">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fal fa-align-left fa-stack-1x fa-inverse "></i>
                            </span>
                            <small class="d-block my-2">Approval Pending Items</small>
                            <h4 class="m-0">{{dashboard.total_pending_items||0}}</h4>
                        </div> 
                    </a>
                </div>
            </div>
        </div>-->
</div>
<script src="<?= base_url() ?>vendor_assets/js/angular/dashboardCtrl.js?r=<?= time() ?>"></script>