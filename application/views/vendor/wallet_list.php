<div ng-controller="withdrawCtrl" ng-init="getWalletBalance(); getWalletHistory();">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Wallet History List</strong></div>

            </div>
        </div>
        <div class="card-body p-0">
            <br/>
            <div class="row rm-small">
                <div class="col-4 col-md-4 col-lg cp-small mb-3 text-center">
                    <div class="card shadow-sm border-0">
                        <div class="card-body">
                            <span class="fa-stack fa-2x d-block mx-auto">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fal fa-wallet fa-stack-1x fa-inverse"></i>
                            </span>
                            <small class="d-block my-2">Original Wallet amount</small>
                            <h4 class="m-0">{{wallet_details.current_wallet_balance}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-4 col-md-4 col-lg cp-small mb-3 text-center">
                    <div class="card shadow-sm border-0">
                        <div class="card-body">
                            <span class="fa-stack fa-2x d-block mx-auto">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fal fa-wallet fa-stack-1x fa-inverse"></i>
                            </span>
                            <small class="d-block my-2">OnHold Wallet amount [ For Withdrawl ]</small>
                            <h4 class="m-0"><span> ( <i class="fa fa-minus" style="color:red"></i> ) </span>{{wallet_details.onhold_wallet_amount}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-4 col-md-4 col-lg cp-small mb-3 text-center">
                    <div class="card shadow-sm border-0">
                        <div class="card-body">
                            <span class="fa-stack fa-2x d-block mx-auto">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fal fa-wallet fa-stack-1x fa-inverse"></i>
                            </span>
                            <small class="d-block my-2">Current Wallet amount</small>
                            <h4 class="m-0">{{wallet_details.current_wallet_balance - wallet_details.onhold_wallet_amount}}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="col-md-12">
                <form ng-submit="getWalletHistory()">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Select type</label>
                                <select  class="form-control" ng-model="filter.action" >
                                    <option value="">All</option>
                                    <option value="credit">Credit</option>
                                    <option value="debit">Debit</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>From date</label>
                                <input type="date" class="form-control" ng-model="filter.from_date" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>To date</label>
                                <input type="date" class="form-control" ng-model="filter.to_date" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" style="margin-top: 29px;" class="btn btn-primary" >Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="container-fluid" ng-if="walletHistory.length == 0">
                <h4 class="text-center ">No data found</h4>
            </div>
            <div class="list-group list-group-flush" ng-if="walletHistory.length > 0">
                <div class="list-group-item" >
                    <div class="row align-items-center">
                        <div class="col-1">#</div>
                        <div class="col-2">Type</div>
                        <div class="col-2">Reason</div>
                        <div class="col-1">Amount</div>
                        <div class="col-3">Comment</div>
                        <div class="col-2">Credited / Rejected On</div>
                    </div>
                </div>
                <div class="list-group-item" clone="" ng-repeat="item in walletHistory track by $index">
                    <div class="row align-items-center">
                        <div class="col-1">{{$index + 1}}</div>
                        <div class="col-2"><h6><span class="badge badge-{{item.action == 'credit' ? 'success' : 'danger'}}">{{item.action}}</span></h6></div>
                        <div class="col-2">{{item.credit_type}}</div>
                        <div class="col-1"><i class="fa fa-rupee-sign"></i> {{item.amount}}</div>
                        <div class="col-3">{{item.comment}}</div>
                        <div class="col-2">
                            {{item.created_at}}
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <span ng-bind-html="walletPagination.pagination"></span>
        </div>
    </div>
    <script src="<?= base_url() ?>vendor_assets/js/angular/withdrawCtrl.js?r=<?= time() ?>"></script>
