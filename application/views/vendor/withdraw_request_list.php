<div ng-controller="withdrawCtrl" ng-init="getWalletBalance(); getWithDrawHistory();">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Withdraw History List</strong></div>
                <div class="col-auto">
                    <a href="<?= base_url() ?>vendor/withdraw_request/new" class="btn btn-link btn-sm"><strong>New Withdraw</strong> <i class="fal fa-plus fa-fw"></i></a>
                </div>
            </div>
        </div>
        <div class="card-body p-0">
            <br/>
            <div class="row rm-small">
                <div class="col-4 col-md-4 col-lg cp-small mb-3 text-center">
                    <div class="card shadow-sm border-0">
                        <div class="card-body">
                            <span class="fa-stack fa-2x d-block mx-auto">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fal fa-wallet fa-stack-1x fa-inverse"></i>
                            </span>
                            <small class="d-block my-2">Original Wallet amount</small>
                            <h4 class="m-0">{{wallet_details.current_wallet_balance}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-4 col-md-4 col-lg cp-small mb-3 text-center">
                    <div class="card shadow-sm border-0">
                        <div class="card-body">
                            <span class="fa-stack fa-2x d-block mx-auto">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fal fa-wallet fa-stack-1x fa-inverse"></i>
                            </span>
                            <small class="d-block my-2">OnHold Wallet amount [ For Withdrawl ]</small>
                            <h4 class="m-0"><span> ( <i class="fa fa-minus" style="color:red"></i> ) </span>{{wallet_details.onhold_wallet_amount}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-4 col-md-4 col-lg cp-small mb-3 text-center">
                    <div class="card shadow-sm border-0">
                        <div class="card-body">
                            <span class="fa-stack fa-2x d-block mx-auto">
                                <i class="fas fa-circle fa-stack-2x text-primary"></i>
                                <i class="fal fa-wallet fa-stack-1x fa-inverse"></i>
                            </span>
                            <small class="d-block my-2">Current Wallet amount</small>
                            <h4 class="m-0">{{wallet_details.current_wallet_balance - wallet_details.onhold_wallet_amount}}</h4>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div class="col-md-12">
                <form ng-submit="getWithDrawHistory(filter.page = 1)">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Select withdraw status</label>
                                <select  class="form-control" ng-model="filter.withdraw_status" >
                                    <option value="">All</option>
                                    <option value="submitted">Submitted</option>
                                    <option value="released">Released</option>
                                    <option value="rejected">Rejected</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>From date</label>
                                <input type="date" class="form-control" ng-model="filter.from_date" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>To date</label>
                                <input type="date" class="form-control" ng-model="filter.to_date" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" style="margin-top: 29px;" class="btn btn-primary" >Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="container-fluid" ng-if="withdrawHistory.length == 0">
                <h4 class="text-center ">No data found</h4>
            </div>
            <br/>
            <div class="list-group list-group-flush" ng-if="withdrawHistory.length > 0">
                <div class="list-group-item" >
                    <div class="row align-items-center">
                        <div class="col-1">#</div>
                        <div class="col-2">Withdraw Request id</div>
                        <div class="col-1">Amount</div>
                        <div class="col-3">Comment</div>
                        <div class="col-2">Withdraw status</div>
                        <div class="col-2">Comment / Txn Id / Proof</div>
                        <div class="col-1">Created at</div>
                    </div>
                </div>
                <div class="list-group-item" clone="" ng-repeat="item in withdrawHistory track by $index">
                    <div class="row align-items-center">
                        <div class="col-1">{{$index + 1}}</div>
                        <div class="col-2">#{{item.request_id}}</div>
                        <div class="col-1"><i class="fa fa-rupee-sign"></i> {{item.amount}}</div>
                        <div class="col-3">{{item.comment}}</div>
                        <div class="col-2">
                            <h6><span class="badge badge-{{item.withdraw_status == 'submitted' ? 'warning' : item.withdraw_status == 'rejected' ? 'danger' : 'success'}}">{{item.withdraw_status}}</span></h6>
                            <br>
                        </div>
                        <div class="col-2">
                            <span ng-if="item.withdraw_status == 'released'">{{item.approve_comment}}</span>
                            <span ng-if="item.withdraw_status == 'rejected'">{{item.reject_reason}}</span>
                        </div>
                        <div class="col-1">
                            {{item.created_at}}
                        </div>
                    </div>
                </div>
            </div>
            <span ng-bind-html="withdrawPagination.pagination"></span>
        </div>
    </div>
    <script src="<?= base_url() ?>vendor_assets/js/angular/withdrawCtrl.js?r=<?= time() ?>"></script>
