<section class="banner-section has-overlay text-white d-flex align-items-center py-5" style="background-image: url(images/slide2.jpg);">
   <div class="container">
      <div class="row align-items-center">
         <div class="col-auto">
            <div class="thumb rounded-circle" style="background-image: url(images/p1.jpg);"></div>
         </div>
         <div class="col">
            <h2>Dwaraka  Grand</h2>
            <p class="mb-1"><i class="fas fa-phone-square fa-fw mr-2"></i>9676969175</p>
            <p class="mb-0"><i class="fas fa-at fa-fw mr-2"></i>info@dwarakagrand.com</p>
        
         </div>
         <div class="col-auto">
           <a href="dashboard-restaurant-alerts.php" class="btn btn-circle btn-outline-light text-uppercase fal fa-bell"></a>
       
         </div>
      </div>
   </div>
</section>