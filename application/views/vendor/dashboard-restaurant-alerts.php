
<?php require_once('dashboard-restaurant-banner.php'); ?>
<section class="content-section pt-3 pb-5">
   <div class="container">
      <div class="row stick_row">
     <?php require_once('dashboard-restaurant-menu.php'); ?>
         <div class="col-9">
            <div class="pt-4">
               <div class="card whitebox">
                  <div class="card-header">
                     <div class="row align-items-center">
                        <div class="col"><strong>Notifications</strong></div>
                 
                     </div>
                  </div>
                  <div class="card-body">
                     <div class="list-group">
                        <?php foreach ($resp->menu as $key =>$item) {?>
                        <div class="list-group-item" href="#section<?php echo $key; ?>">
                           <div class="row align-items-center">
                              <div class="col-auto"><i class="fal fa-bell fa-2x"></i></div>
                              <div class="col">
                                <small>02/03/2018 2:00 PM</small>
                                <p class="m-0">Explore curated lists of top restaurants, <br/>cafes, pubs, and bars in Hyderabad, based on trends
</p>
                              </div>
                         
                           </div>
                        </div>
                        <?php }?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add/Update Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="row">
                  <div class="col-12">
               <div class="form-group">
                  <label>Priority</label>
                  <input type="text" class="form-control">
               </div>
            </div>
            <div class="col-12">
               <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control">
               </div>
            </div>
         </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Add</button>
      </div>
    </div>
  </div>
</div>
