<div ng-controller="productsCtrl">
    <div class="card shadow-sm border-0 mixwithbg" ng-init="getProductsWithImages()">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>My Products Preview</strong></div>

            </div>
        </div>

        <div class="card-body p-0">
            <div class="container-fluid" ng-if="products.length === 0">
                <h4 class="text-center ">No Images found</h4>
            </div>

            <div class="wrapper wrapper-content">
                <div class="row" style="padding-left: 20px; padding-right: 20px">
                    <div class="col-md-3" ng-repeat=" product in products" style="margin: 10px 10px;">
                        <div class="card box-prod" style="padding: 10px; width: 100%">
                            <div id="carouselExampleIndicators{{$index}}" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner" style="background-color: #343A40">
                                    <div class="carousel-item active" ng-if="product.images.length < 1">
                                        <center style="height: 400px; width: 100%; padding-top: 50%">
                                            <span style="color: white"> No Images</span>
                                        </center>
                                    </div>
                                    <div class="carousel-item {{($index==0)?'active':''}}" ng-repeat="img in product.images">
                                        <center>
                                            <img src="<?= base_url() ?>{{img.image}}" style="height: 400px;object-fit: contain;width: 100%">
                                        </center>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators{{$index}}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators{{$index}}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <br>
                            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
                            <!--<div class="price-div"><i class="fa fa-inr"></i> {{product.price}}</div>-->
                            <div style="padding-left: 20px; padding-right: 20px">
                                <h5 style="width: 100%">{{product.product_id}} <span>({{product.brand}})</span>
                                </h5>
                                <p>{{product.title}}</p>
                                <a href="<?= base_url('vendor/view_product/view/') ?>{{product.id}}"><button class="btn btn-dark col-12" style="float: right">View Product</button></a>
                            </div>
                            <div style="width: 100%; display: none">
                                <button class="btn btn-dark col-12">Add To Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .price-div{
        position: absolute;
        z-index: 1000;
        bottom: 150px;
        right: 0px;
        padding: 2.5% 8% 2.5% 5%;
        background-color: #343A40;
        margin-right: 10px;
        color: white;
    }
    .box-prod:hover{
        transition: 0.3s;
        box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
        transform: scale(1.01);
    }
</style>

<script src="<?= base_url() ?>vendor_assets/js/angular/productsCtrl.js?r=<?= time() ?>"></script>
