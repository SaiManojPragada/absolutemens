<div ng-controller="restaurantOrdersCtrl"> 
    <div class="row rm-small">
        <div class="col-12 col-md cp-small">
            <div class="card shadow-sm border-0 mixwithbg">
                <div class="card-header font-weight-bold">New Orders <span class="float-right text-muted">{{newOrders.length}}</span></div>
                <div class="card-body p-0 card-body-limited">
                    <div class="list-group list-group-flush">
                        <div ng-repeat="order in newOrders">
                            <a ng-click="showCurrentOrder(order, 'Pending')" href="#" data-toggle="modal" data-target="#orderDetailsModal" class="list-group-item list-group-item-action text-secondary">
                                <div class="row align-items-center">
                                    <div class="col">

                                        <strong class="text-primary d-block">#{{order.ref_id}}</strong>

                                        <small class="d-inline-block">{{order.service_date}}</small>
                                        <span class="d-inline-block">-</span>
                                        <span class="d-inline-block">{{order.customer_details.customer_name}}</span>

                                    </div>
                                    <div class="col-auto">

                                        <span class="d-block">{{order.cart_items.length}} Items</span>
                                        <span class="d-block"><small class="fal fa-rupee-sign"></small> <span class="font-weight-bold text-dark">{{order.grand_total}}</span></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md cp-small">
            <div class="card shadow-sm border-0 mixwithbg">
                <div class="card-header font-weight-bold">Preparing<span class="float-right text-muted">{{preparingOrders.length}}</span></div>
                <div class="card-body p-0 card-body-limited " style="min-height:250px;max-height:250px">
                    <div class="list-group list-group-flush">
                        <div ng-repeat="order in preparingOrders">
                            <a ng-click="showCurrentOrder(order, 'Preparing')" href="#" data-toggle="modal" data-target="#orderDetailsModal" class="list-group-item list-group-item-action text-secondary">
                                <div class="row align-items-center">
                                    <div class="col">

                                        <strong class="text-primary d-block">#{{order.ref_id}}</strong>

                                        <small class="d-inline-block">{{order.service_date}}</small>
                                        <span class="d-inline-block">-</span>
                                        <span class="d-inline-block">{{order.customer_details.customer_name}}</span>

                                    </div>
                                    <div class="col-auto">

                                        <span class="d-block">{{order.cart_items.length}} Items</span>
                                        <span class="d-block"><small class="fal fa-rupee-sign"></small> <span class="font-weight-bold text-dark">{{order.grand_total}}</span></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-header font-weight-bold">On Delivery<span class="float-right text-muted">{{onGoingOrders.length}}</span></div>
                <div class="card-body p-0 card-body-limited" style="min-height:250px;max-height:250px">
                    <div class="list-group list-group-flush">


                        <div ng-repeat="order in onGoingOrders">

                            <a ng-click="showCurrentOrder(order, 'On Going')" href="#" data-toggle="modal" data-target="#orderDetailsModal" class="list-group-item list-group-item-action text-secondary">
                                <div class="row align-items-center">
                                    <div class="col">

                                        <strong class="text-primary d-block">#{{order.ref_id}}</strong>

                                        <small class="d-inline-block">{{order.service_date}}</small>
                                        <span class="d-inline-block">-</span>
                                        <span class="d-inline-block">{{order.customer_details.customer_name}}</span>

                                    </div>
                                    <div class="col-auto">
                                        <span class="d-block">{{order.cart_items.length}} Items</span>
                                        <span class="d-block"><small class="fal fa-rupee-sign"></small> <span class="font-weight-bold text-dark">{{order.grand_total}}</span></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md cp-small">
            <div class="card shadow-sm border-0 mixwithbg">
                <div class="card-header font-weight-bold">Delivered<span class="float-right text-muted">{{deliveredOrders.length}}</span></div>
                <div class="card-body p-0 card-body-limited">
                    <div class="list-group list-group-flush">

                        <div ng-repeat="order in deliveredOrders">

                            <a ng-click="showCurrentOrder(order, 'Delivered')" href="#" data-toggle="modal" data-target="#orderDetailsModal" class="list-group-item list-group-item-action text-secondary">
                                <div class="row align-items-center">
                                    <div class="col">

                                        <strong class="text-primary d-block">#{{order.ref_id}}</strong>

                                        <small class="d-inline-block">{{order.service_date}}</small>
                                        <span class="d-inline-block">-</span>
                                        <span class="d-inline-block">{{order.customer_details.customer_name}}</span>

                                    </div>
                                    <div class="col-auto">

                                        <span class="d-block">{{order.cart_items.length}} Items</span>
                                        <span class="d-block"><small class="fal fa-rupee-sign"></small> <span class="font-weight-bold text-dark">{{order.grand_total}}</span></span>
                                    </div>
                                </div>
                            </a>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>



    <?php $this->load->view("vendor/includes/order_details_popup") ?>

    <div class="modal scale" id="orderRejectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="order_reject_form">
                    <div class="modal-header py-3">
                        <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Order Reject Confirmation</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="fal fa-times"></span>
                        </button>
                    </div>
                    <div class="modal-body bg-light">
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row form-group">
                                    <label for="rejected_reason">Enter Reason for Rejection</label>
                                    <textarea id="rejected_reason" class="form-control" name="rejected_reason" ng-model="reject.rejected_reason" rows="4" cols="100"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button ng-click="rejectOrder()" type="button" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal scale" id="orderDeliverModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="order_delivery_form">
                    <div class="modal-header py-3">
                        <h6 class="modal-title font-weight-bold" id="exampleModalLabel">Order Delivery Confirmation</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="fal fa-times"></span>
                        </button>
                    </div>
                    <div class="modal-body bg-light">
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row form-group">
                                    <label for="delivery_person_name">Enter Delivery Person Name</label>
                                    <input type="text" class="form-control" name="delivery_person_name" ng-model="outForDelivery.delivery_person_name" value="">
                                </div>
                                <div class="row form-group">
                                    <label for="delivery_person_contact_number">Enter Delivery Person Number</label>
                                    <input type="tel" class="form-control" name="delivery_person_contact_number" ng-model="outForDelivery.delivery_person_contact_number" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button ng-click="outForDeliveryOrder()" type="button" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<script src="<?= base_url() ?>vendor_assets/js/angular/restaurantOrdersCtrl.js?r=<?= time() ?>"></script>
