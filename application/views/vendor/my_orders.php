<div ng-controller="ordersCtrl" ng-init="getOrders('<?= $param ?>')">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong><?= ucfirst($param) ?> Orders</strong></div>
            </div>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link <?= (empty($param)) ? 'active' : '' ?>" href="<?= base_url('vendor/orders/view') ?>">All</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= ($param === 'pending') ? 'active' : '' ?>" href="<?= base_url('vendor/orders/view/pending') ?>">Pending</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= ($param === 'delivered') ? 'active' : '' ?>" href="<?= base_url('vendor/orders/view/delivered') ?>">Completed</a>
                </li>
            </ul>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-striped table-bordered dataTables-example dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Order Details</th>
                            <th>Delivery Address</th>
                            <th>Ordered Products</th>
                            <th>Order Status</th>
                            <th>Order Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="item in orders">
                            <td>{{$index + 1}}</td>
                            <td>
                                <p><b>Order Id : </b>{{item.order_id}}</p>
                                <p><b>Ordered Date : </b>{{item.order_date}}</p>
                            </td>
                            <td>
                                <p><b>Address : </b>{{item.delivery_address}}</p>
                            </td>
                            <td>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Product Id</th>
                                            <th>Product Details</th>
                                            <th>Product Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="product in item.products">
                                            <td>{{$index + 1}}</td>
                                            <td>{{product.product_details.product_id}}</td>
                                            <td><b>Name : </b>{{product.product_details.title}}<br></br><b>Brand : </b>{{product.product_details.brand}}<br><br>
                                                <span ng-repeat="is in product.inventory track by $index"><b>{{is.category}} : </b>{{is.size}},&nbsp;</span>,
                                                <b>Quantity : </b>{{product.quantity}}<br><br><b>Price : </b> Rs {{product.subtotal}}</td>
                                            <td>
                                                <span ng-if="product.order_status === 'order_placed_by_admin'" style="background-color: yellow; color: black">Order Pending</span>
                                                <span ng-if="product.order_status === 'order_dispatched_by_vendor'" style="background-color: orange; color: white">Order Dispatched</span>
                                                <span ng-if="product.order_status === 'order_delivered'" style="background-color: green; color: white">Order Delivered</span>
                                                <span ng-if="product.order_status === 'order_completed'" style="background-color: green; color: white">Order Completed</span>
                                                <span ng-if="product.order_status === 'order_cancelled_by_vendor'" style="background-color: red; color: white">Order Cancelled</span>
                                            </td>
                                            <td>
                                                <a href="<?= base_url() ?>vendor/view_product/view/{{product.product_details.id}}?inventory_id={{product.product_inventory_id}}" target="_blank"><button class="btn btn-xs btn-warning" >View Product</button></a>

                                                <select class="form-control" ng-model="product.order_status" ng-change="changeProductStatus(product.id, item.id, item.order_id, product.order_status, '<?= $param ?>')" ng-if="item.order_status !== 'order_completed'">
                                                    <option value="order_placed_by_admin">Pending Order</option>
                                                    <option value="order_dispatched_by_vendor">Order Dispatched</option>
                                                    <option value="order_delivered"  ng-if='(product.order_status === "order_dispatched_by_vendor" || product.order_status === "order_delivered")'>Order Delivered</option>
                                                    <!--<option value="order_cancelled_by_vendor"  ng-if='(product.order_status !== "order_delivered")'>Cancel Order</option>-->
                                                </select>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td id="stat">
                                <span ng-if="item.order_status === 'order_placed_by_admin'" style="background-color: yellow; color: black">Order Pending</span>
                                <span ng-if="item.order_status === 'order_dispatched_by_vendor'" style="background-color: orange; color: white">Order Dispatched</span>
                                <span ng-if="item.order_status === 'order_delivered'" style="background-color: green; color: white">Order Delivered</span>
                                <span ng-if="item.order_status === 'order_completed'" style="background-color: green; color: white">Order Completed</span>
                                <span ng-if="item.order_status === 'order_cancelled_by_vendor'" style="background-color: red; color: white">Order Cancelled</span>
                            </td>
                            <td>
                                <button class="btn btn-primary" ng-click="showLogs(item.id)">View Logs</button>
<!--                                <select class="form-control" ng-model="item.order_status" ng-change="changeStatus(item.id, item.order_id, item.order_status, '<?= $param ?>')" ng-if="item.order_status !== 'order_completed'">
                                    <option value="order_placed_by_admin">Pending Order</option>
                                    <option value="order_dispatched_by_vendor">Order Dispatched</option>
                                    <option value="order_delivered"  ng-if='(item.order_status === "order_dispatched_by_vendor" || item.order_status === "order_delivered")'>Order Delivered</option>
                                    <option value="order_cancelled_by_vendor"  ng-if='(item.order_status !== "order_delivered")'>Cancel Order</option>
                                </select>-->
<!--                                <p ng-if="item.order_status === 'order_completed'">This Order has been Completed by the Admin</p>
                                <p ng-if="item.order_status !== 'order_completed'" style="color: orange">This Order has been Assigned to you by the Admin</p>-->
                            </td>
                        </tr>
                        <tr ng-if="!orders[0]">
                            <td colspan="5">
                    <center>
                        -- No Orders Found --
                    </center>
                    </td>
                    </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Order Details</th>
                            <th>Delivery Address</th>
                            <th>Ordered Products</th>
                            <th>Order Status</th>
                            <th>Order Action</th>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>

    </div>
    <div class="modal fade" id="logsModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" style="max-width: 80%" role="document">
            <div class="modal-content">
                <div class="modal-head" style="padding: 5px 10px">
                    <h5>Order ({{logs_order_id}}) Logs</h5>
                    <button class="btn btn-default pull-right" style="position: absolute;right: 0;top: 0;" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="max-height: 900px; overflow-y: scroll">
                    <table class="table table-striped table-bordered datatable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Action Performed</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="log in logs">
                                <td>{{$index + 1}}</td>
                                <td>{{log.log}}</td>
                                <td>{{log.action_date_time}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #stat span{
        padding: 2px 10px;
        border-radius: 10px;
    }
</style>
<script src="<?= base_url() ?>vendor_assets/js/angular/ordersCtrl.js?r=<?= time() ?>"></script>