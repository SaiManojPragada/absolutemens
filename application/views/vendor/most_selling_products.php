<div ng-controller="restaurantFoodItemsCtrl" ng-init="filterObj.verification_status = '<?= $this->input->get_post("verification_status") ?>'; filterObj.most_selling = true; getFoodItems()">
    <div class="card shadow-sm border-0 mixwithbg"> 
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Most Selling Items (Based Last 30 days orders)</strong></div>
            </div>
        </div>

        <div class="card-body p-0">
            <div class="container-fluid" ng-if="foodItems.length == 0">
                <h4 class="text-center ">No items found</h4>
            </div>

            <div class="list-group list-group-flush">
                <div class="list-group-item" clone="" ng-repeat="foodItem in foodItems">
                    <div class="row align-items-center">
                        <div class="col-12 col-md">
                            <strong>{{foodItem.id}}</strong>
                            <br/>
                            <strong>SKU : {{foodItem.sku_code}}</strong>
                            <br/>
                            <strong>EAN : {{foodItem.ean_code}}</strong>
                        </div>
                        <div class="col-12 col-md-auto mb-2 mb-md-0" style="width:90px">
                            <img ng-src="{{foodItem.image}}" class="rounded" width="60">
                        </div>
                        <div class="col-12 col-md">
                            <small>{{foodItem.category_name}}</small>
                            <strong class="text-head d-block">{{foodItem.item_name}}</strong>
                            <?php if ($attachment_required != "yes") { ?>
                                <span class="badge badge-success" ng-if="foodItem.food_type === 'Veg'">{{foodItem.food_type}}</span>
                                <span class="badge badge-danger" ng-if="foodItem.food_type === 'Non Veg'">{{foodItem.food_type}}</span>
                            <?php } ?>
                            <small>{{foodItem.description}}</small>
                            <br/>
                            <small>Verification Status : <span class="{{foodItem.verification_status_css}}">{{foodItem.verification_status}}</span> </small>
                            <br/>
                            <strong>Sold Count:  <span>{{foodItem.sold_count}}</span> </strong>
                        </div>
                        <div   class="col-12 col-md mt-2 mt-md-0 text-md-center text-left">
                            <small class="fal fa-rupee-sign fa-fw"></small><strong>{{foodItem.actual_price}}</strong>
                        </div>
                        <div   class="col-12 col-md mt-2 mt-md-0 text-md-center text-left">
                            GST <strong>{{foodItem.tax_percentage}}%</strong>
                        </div>
                    </div>
                </div>
            </div>
            <span class="pull-right" ng-bind-html="foodItemsPagination.pagination"></span>

        </div>
    </div>

</div>

<script src="<?= base_url() ?>vendor_assets/js/angular/restaurantFoodItemsCtrl.js?r=<?= time() ?>"></script>