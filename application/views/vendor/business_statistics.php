
<div ng-controller="dashboardCtrl" ng-init="getDashboardData()">
<div class="card shadow-sm border-0 mixwithbg">
    <div class="card-header">
        <strong>Statistics</strong>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-md-6 mb-2 mb-md-0 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="m-0">Total Orders</h6>
                            </div>
                            <div class="col-auto"><span class="text-info h2 m-0">{{dashboard.total_orders ||0}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-12 col-md-6 mb-2 mb-md-0 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="m-0">Completed Orders</h6>
                            </div>
                            <div class="col-auto"><span class="text-success h2 m-0">{{dashboard.total_completed_orders ||0}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-12 col-md-6 mb-2 mb-md-0 ">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="m-0">Cancelled Orders</h6>
                            </div>
                            <div class="col-auto"><span class="text-danger h2 m-0">{{dashboard.total_cancelled_orders ||0}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row align-items-center">
                            <div class="col">
                                <h6 class="m-0">Total Earnings</h6>
                            </div>
                            <div class="col-auto"><span class="text-success h2 m-0"><small class="fal fa-rupee-sign fa-fw"></small> {{dashboard.total_earnings ||0}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<br/>
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="card">
					<div class="card-body">
						<div class="row align-items-center">
							<div class="col">
								<h6 class="m-0">Pending Payout Amount</h6>
							</div>
							<div class="col-auto"><span class="text-warning h2 m-0"><small class="fal fa-rupee-sign fa-fw"></small> {{dashboard.total_pending_payout ||0}}</span></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="card">
					<div class="card-body">
						<div class="row align-items-center">
							<div class="col">
								<h6 class="m-0">Completed Payout Amount</h6>
							</div>
							<div class="col-auto"><span class="text-success h2 m-0"><small class="fal fa-rupee-sign fa-fw"></small> {{dashboard.total_paid_payout ||0}}</span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
    </div>
</div>

</div>
<script src="<?=base_url()?>vendor_assets/js/angular/dashboardCtrl.js?r=<?= time() ?>"></script>
