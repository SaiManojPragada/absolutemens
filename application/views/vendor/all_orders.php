<div ng-controller="allOrdersCtrl">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>All Orders</strong></div>
            </div>
        </div>
        <div class="card-body p-10">  
            <br/>
            <form class="form" ng-submit="getOrders(filter.page = 1)">
                <div class="row">
                    <div class="form-group col-md-4">
                        <input type="text" class="form-control" name="q" ng-model="filter.q" placeholder="Search By Order Id">
                    </div>
                    <div class="form-group col-md-2">
                        <select class="form-control" name="service_status" ng-model="filter.type" title="Service Status">
                            <option value="">All</option>
                            <?php
                            $status = array('Pending', 'Waiting For Pickup', 'Waiting For Delivery Person', 'Out For Delivery', "Completed");
                            foreach ($status as $sitem) {
                                echo '<option value="' . $sitem . '">' . $sitem . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <input type="text" class="form-control datepickerFrom" ng-model="filter.from_date" autocomplete="fd" name="from_date" placeholder="From Date">
                    </div>
                    <div class="form-group col-md-2">
                        <input type="text" class="form-control datepickerTo" ng-model="filter.to_date"  autocomplete="td"  name="to_date" placeholder="To Date">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-info">Search</button> 
                        <button type="submit" name="export_btn" class="btn btn-danger">Export</button> 
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Order Id </th>
                            <th>Customer Name 
                                <br/>
                                Mobile</th>
                            <th>Date</th>
                            <th>Service Status</th>
                            <th>Payment Status</th>
                            <th>Order Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="item in ordersList track by $index">
                            <td>{{($index + ordersPagination.initial_id)}}</td>
                            <td>{{item.ref_id}}</td>
                            <td>{{item.customer_details.customer_name}}<br/>
                                {{item.customer_details.mobile}}</td>
                            <td class="text-center">
                                {{item.service_date}}
                            </td>
                            <td><span class="text-{{item.service_status_bootstrap_class}}">{{item.service_status}}</span></td>
                            <td>
                                <strong>{{item.payment_mode}}</strong><br/>
                                <span class="text-{{item.payment_status_bootstrap_class}}">{{item.payment_status}}</span>
                            </td>
                            <td>{{item.grand_total}} </td>
                            <td>
                                <button type="button" class="btn btn-info btn-sm" ng-click="ShowOrderPopup(item)">View</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <span class="pull-right" ng-bind-html="ordersPagination.pagination"></span>
        </div>
    </div>

    <?php $this->load->view("vendor/includes/order_details_popup") ?>


</div>

<script src="<?= base_url() ?>vendor_assets/js/angular/allOrdersCtrl.js?r=<?= time() ?>"></script>