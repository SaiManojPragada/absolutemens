<div class="card shadow-sm border-0 mixwithbg" ng-controller="restaurantPasswordCtrl">
   <div class="card-header">
      <strong>Change Password</strong>
   </div>
   <div class="card-body">
      <form id="restaurant_password_form" method="get" class="w-500 mx-auto" ng-submit="updateRestaturantPassword()">
         <div class="row rm-small">
            <div class="col-12 cp-small">
               <div class="form-group">
                  <label>Current Password</label>
                  <input type="password" class="form-control" ng-model="changePass.current_password" name="current_password">
               </div>
            </div>
            <div class="col-12 cp-small">
               <div class="form-group">
                  <label>New Password</label>
                  <input type="password" class="form-control" id="restaurantPassId" ng-model="changePass.password" name="password">
               </div>
            </div>
            <div class="col-12 cp-small">
               <div class="form-group">
                  <label>Confirm Password</label>
                  <input type="password" class="form-control" ng-model="changePass.password_cnf" name="password_cnf">
               </div>
            </div>

            <div class="col-12 cp-small"><button type="submit" class="btn btn-primary text-uppercase px-5"><strong>Update</strong></button></div>
         </div>
      </form>
   </div>
</div>

<script src="<?=base_url()?>vendor_assets/js/angular/restaurantPasswordCtrl.js"></script>
