<div class="card shadow-sm border-0 mixwithbg">
                  <div class="card-header">
                     <div class="row align-items-center">
                        <div class="col"><strong>Sub Categories</strong></div>
                        <div class="col-auto">
                           <a href="#modal-add" data-toggle="modal" class="btn btn-link btn-sm"><strong>Add</strong> <i class="fal fa-plus fa-fw"></i></a>
                        </div>
                     </div>
                  </div>
                  <div class="card-body p-0">
                     <div class="list-group list-group-flush">
                        <div class="list-group-item" clone="" data-items="5">
                           <div class="row align-items-center">
                              <div class="col">
                                 <small>Combos</small>
                                 <strong class="text-head d-block">Special Combos</strong>
                              </div>
                              <div class="col-auto">
                                 <a href="#modal-add" data-toggle="modal" class="btn btn-link text-secondary fal fa-pen"></a>
                                 <a href="#" class="btn btn-link text-secondary fal fa-trash"></a>
                              </div>
                           </div>
                        </div><div class="list-group-item" clone="" data-items="5">
                           <div class="row align-items-center">
                              <div class="col">
                                 <small>Combos</small>
                                 <strong class="text-head d-block">Special Combos</strong>
                              </div>
                              <div class="col-auto">
                                 <a href="#modal-add" data-toggle="modal" class="btn btn-link text-secondary fal fa-pen"></a>
                                 <a href="#" class="btn btn-link text-secondary fal fa-trash"></a>
                              </div>
                           </div>
                        </div><div class="list-group-item" clone="" data-items="5">
                           <div class="row align-items-center">
                              <div class="col">
                                 <small>Combos</small>
                                 <strong class="text-head d-block">Special Combos</strong>
                              </div>
                              <div class="col-auto">
                                 <a href="#modal-add" data-toggle="modal" class="btn btn-link text-secondary fal fa-pen"></a>
                                 <a href="#" class="btn btn-link text-secondary fal fa-trash"></a>
                              </div>
                           </div>
                        </div><div class="list-group-item" clone="" data-items="5">
                           <div class="row align-items-center">
                              <div class="col">
                                 <small>Combos</small>
                                 <strong class="text-head d-block">Special Combos</strong>
                              </div>
                              <div class="col-auto">
                                 <a href="#modal-add" data-toggle="modal" class="btn btn-link text-secondary fal fa-pen"></a>
                                 <a href="#" class="btn btn-link text-secondary fal fa-trash"></a>
                              </div>
                           </div>
                        </div><div class="list-group-item" clone="" data-items="5">
                           <div class="row align-items-center">
                              <div class="col">
                                 <small>Combos</small>
                                 <strong class="text-head d-block">Special Combos</strong>
                              </div>
                              <div class="col-auto">
                                 <a href="#modal-add" data-toggle="modal" class="btn btn-link text-secondary fal fa-pen"></a>
                                 <a href="#" class="btn btn-link text-secondary fal fa-trash"></a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add / Update Sub Category</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-12">
                  <div class="form-group">
                     <label>Category</label>
                     <select class="custom-select">
                        <option>Select</option>
                     </select>
                  </div>
               </div>
               <div class="col-12">
                  <div class="form-group">
                     <label>Priority</label>
                     <input type="text" class="form-control">
                  </div>
               </div>
               <div class="col-12">
                  <div class="form-group">
                     <label>Name</label>
                     <input type="text" class="form-control">
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Add</button>
         </div>
      </div>
   </div>
</div>