<div class="card shadow-sm border-0 mixwithbg">
    <div style="margin-top:10%">

    </div>
    <div class="card-body" ng-controller="loginCtrl">
        <form id="login_form" method="get" class="w-500 mx-auto" ng-submit="doLogin()">
            <h2 class="text-center">Store Login</h2>
            <div class="row rm-small"> 
                <div class="col-12 cp-small">
                    <div class="form-group">
                        <!--<label>Username (Registered Mobile number)</label> -->
                        <label>Username (Store Ref Code)</label>
                        <input type="tel" class="form-control" name="username" ng-model="user.username">
                    </div>
                </div>
                <div class="col-12 cp-small">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" ng-model="user.password">
                    </div>
                </div>
                <div class="col-12 cp-small">
                    <button type="submit" class="btn btn-primary text-uppercase px-5">
                        <strong>Login</strong>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="<?= base_url() ?>vendor_assets/js/angular/loginCtrl.js"></script>
