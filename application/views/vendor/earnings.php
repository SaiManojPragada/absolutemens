<div ng-controller="vendorEarningsCtrl" ng-init="getVendorEarnings()">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Vendor Earnings</strong></div>

            </div>
        </div>
        <div class="card-body p-0">

            <br/>
            <div class="col-md-12">
                <form ng-submit="getVendorEarnings(filter.page = 1)">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Select report type</label>
                                <select  class="form-control" ng-model="filter.type" >
                                    <option value="">All</option>
                                    <option value="today">Today</option>
                                    <option value="weekly">Weekly</option>
                                    <option value="monthly">Monthly</option>
                                    <option value="year">Yearly</option>
                                    <option value="custom">Custom</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3" ng-if="filter.type == 'custom'">
                            <div class="form-group">
                                <label>From date</label>
                                <input type="date" class="form-control" ng-model="filter.from_date" />
                            </div>
                        </div>
                        <div class="col-md-3" ng-if="filter.type == 'custom'">
                            <div class="form-group">
                                <label>To date</label>
                                <input type="date" class="form-control" ng-model="filter.to_date" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" style="margin-top: 29px;" class="btn btn-primary" >Search</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-12">
                <div class="alert alert-info">
                    Total Earnings :  <i class="fa fa-rupee-sign"></i> {{total_amount?total_amount:0}}
                </div>
            </div>
            <div class="container-fluid" ng-if="earningsList.length == 0">
                <h4 class="text-center ">No data found</h4>
            </div>
            <div class="list-group list-group-flush" ng-if="earningsList.length > 0">
                <div class="list-group-item" >
                    <div class="row align-items-center">
                        <div class="col-1">#</div>
                        <div class="col-2">Withdraw Req id</div>
                        <div class="col-4">Comment</div>
                        <div class="col-2">Amount</div>
                        <div class="col-2">Created at</div>
                    </div>
                </div>
                <div class="list-group-item" clone="" ng-repeat="item in earningsList track by $index">
                    <div class="row align-items-center">
                        <div class="col-1">{{$index + 1}}</div>
                        <div class="col-2">#{{item.request_id}}</div>
                        <div class="col-4">#{{item.comment}}</div>
                        <div class="col-2"><i class="fa fa-rupee-sign"></i> {{item.amount}}</div>
                        <div class="col-2">
                            {{item.created_at}}
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <span ng-bind-html="earningsPagination.pagination"></span>
        </div>
    </div>
    <script src="<?= base_url() ?>vendor_assets/js/angular/vendorEarningsCtrl.js?r=<?= time() ?>"></script>
