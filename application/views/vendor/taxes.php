<div ng-controller="taxesCtrl">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Taxes</strong></div>
                <div class="col-auto">
                    <a ng-click="addTaxModal()" class="btn btn-link btn-sm"><strong>Add</strong> <i class="fal fa-plus fa-fw"></i></a>
                </div>
            </div>
        </div>
        <div class="card-body p-0"> 
            <div class="list-group list-group-flush">
                <h3 class="text-center" ng-if="taxes.length == 0">No Taxes found</h3>
				
				<div class="table-responsive" ng-if="taxes.length>0">
					<table class="table table-hovered">
						<thead>
							<tr>
								<th>#</th>
								<th>Tax Name</th>
								<th>Tax Amount</th>
								<th>Min Purchase</th>
								<th>Tax Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="item in taxes track by $index">
								<td>{{$index+1}}</td>
								<td>{{item.tax_name}}</td>
								<td>{{item.display_tax_type}}</td>
								<td>Rs. {{item.tax_apply_above}}</td>
								<td class="{{item.tax_status_bootstrap_class}}">{{item.tax_status}}</td>
								<td>
									<a ng-click="editTaxModal(item)" class="btn btn-link text-secondary fal fa-pen"></a>
									<a ng-click="deleteTax(item)" class="btn btn-link text-secondary fal fa-trash"></a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="taxModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="restaurant_tax_form" ng-submit="addUpdateTax()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{tax.id?"Update":"Add"}} Tax</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
						<div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Tax Name</label>
                                    <input type="text" class="form-control" placeholder="Enter Tax Name" name="tax_name" ng-model="tax.tax_name">
                                </div>
                            </div>
							<div class="col-6">
                                <div class="form-group">
                                    <label>Tax Amount</label>
                                    <input type="text" class="form-control" name="tax_amount" ng-model="tax.tax_amount">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Tax Type</label>
                                    <select class="form-control" name="tax_type" ng-model="tax.tax_type">
										<option>Percentage</option>
										<option>Flat</option>
									</select>
                                </div>
                            </div>
							<div class="col-12">
                                <div class="form-group">
                                    <label>Min amount to apply Tax</label>
                                    <input type="text" class="form-control" name="tax_apply_above" ng-model="tax.tax_apply_above">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">{{tax.id? "Update" : "Add"}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url() ?>vendor_assets/js/angular/taxesCtrl.js"></script>