
<?php require_once('dashboard-restaurant-banner.php'); ?>

<section class="content-section pt-3 pb-5">
   <div class="container">
      <div class="row stick_row">
  <?php require_once('dashboard-restaurant-menu.php'); ?>
         <div class="col-9">
            <div class="pt-4">
             <div class="card whitebox">
                <div class="card-header"><strong>Orders</strong></div>
                <div class="card-body">



<div class="row">
  <div class="col-12">
    <div class="card mb-3" clone data-items="6">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col">
            <strong class="d-block text-head">#ODD-284975</strong>
            <small class="w-75 d-block">3 Items |  Penthouse, Subhodaya Colony, Vijaya Nagar Colony, Kukatpally, Hyderabad, Telangana 500072, India</small>
          </div>
          <div class="col-auto"><small class="fal fa-rupee-sign fa-fw"></small><strong>1,112.00</strong></div>
          <div class="col-auto"><i class="fas fa-check-circle text-success fa-fw"></i> Paid</div>
          <div class="col-auto"><a href="dashboard-restaurant-order-view.php" class="btn btn-outline-primary btn-sm text-uppercase"><strong>View</strong></a></div>
          
        </div>
      </div>
    </div>
  </div>
</div>





 
                </div>
             </div>
            </div>
         </div>

      </div>
   </div>
</section>
