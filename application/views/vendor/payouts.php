<div ng-controller="allOrdersCtrl">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>All Payouts</strong></div>
            </div>
        </div>
        <div class="card-body p-10"> 
			<br/>
			<form class="form" ng-submit="getOrders(filter.page=1)">
				<div class="row">
					<div class="form-group col-md-4">
						<input type="text" class="form-control" name="q" ng-model="filter.q" placeholder="Search By Order Id,Mobile,Email,ZIP Code">
					</div>
					<div class="form-group col-md-2">
						<select class="form-control" name="settlement_status" ng-model="filter.settlement_status" title="Settlement Status">
							<option value="">All</option>
							<?php
							$status = array('Completed', 'Pending');
							foreach ($status as $sitem) {
								echo '<option value="' . $sitem . '">' . $sitem . '</option>';
							}
							?>
						</select>
					</div>
					<div class="form-group col-md-2">
						<input type="text" class="form-control datepickerFrom" ng-model="filter.from_date" autocomplete="fd" name="from_date" placeholder="From Date">
					</div>
					<div class="form-group col-md-2">
						<input type="text" class="form-control datepickerTo" ng-model="filter.to_date"  autocomplete="td"  name="to_date" placeholder="To Date">
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-info">Search</button> 
						<button type="submit" name="export_btn" class="btn btn-danger">Export</button> 
					</div>
				</div>
			</form>
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>S.No</th>
							<th>Order Id </th>
							<th>Customer Name 
								<br/>
								Mobile</th>
							<th>Date</th>
							<th>Settlement Status</th>
							<th>Settlement Ref ID</th>
							<th>Order Amount</th>
							<th>Admin Commission</th>
							<th>Payable Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="item in ordersList track by $index">
							<td>{{($index + ordersPagination.initial_id)}}</td>
							<td>{{item.ref_id}}</td>
							<td>{{item.customer_details.customer_name}}<br/>
								{{item.customer_details.mobile}}</td>
							<td class="text-center">
								{{item.service_date}}
							</td>
							<td>
								<span class="text-{{item.settlement_status_bootstrap_class}}">{{item.settlement_status}}</span>
							</td>
							<td>
								{{item.settlement_ref_id}}
							</td>
							<td>{{item.grand_total}} </td>
							<td>{{item.restaurant_commission_amount}} </td>
							<td>{{item.payable_amount_to_restaurant}} </td>
						</tr>
					</tbody>
				</table>
			</div>
			<span class="pull-right" ng-bind-html="ordersPagination.pagination"></span>
        </div>
    </div>
	
	<!-- Modal -->
	<div class="modal scale" id="orderDetailsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-full" role="document">
		<div class="modal-content">
		  <div class="modal-header py-3">
			<h6 class="modal-title font-weight-bold" id="exampleModalLabel">Order Details</h6>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span class="fal fa-times"></span>
			</button>
		  </div>
		  <div class="modal-body bg-light">
				 <div class="card mb-3">
							<div class="card-body">
							   <div class="row">
								  <div class="col"><h6 class="m-0">#{{order.ref_id}}</h6></div>
								  <div class="col-auto">
									 <p class="m-0">Order  {{order.service_status}} <i class="fas fa-check-circle text-success"></i></p>
								  </div>
							   </div>
							</div>
						 </div>
					<div class="list-group">
							   <div class="list-group-item" ng-repeat="item in order.cart_items">
								  <div class="row align-items-center">
									<div class="col">
									  <span>{{item.restaurant_food_items_name}}</span>
									  <span ng-if="item.restaurant_food_options_name"> - {{item.restaurant_food_options_name}}</span>
									  <span>({{item.qty}} * Rs. {{item.applicable_price}})</span>
									  <br>
									  <span ng-if="item.restaurant_food_item_addons_names">
										{{item.restaurant_food_item_addons_names}} ({{item.qty}} * Rs. {{item.add_ons_amount}})
									  </span>

									</div>
									<div class="col-auto"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{item.total_amount}}</strong></div>

								  </div>
							   </div>

							   <div class="list-group-item">
								  <div class="row align-items-center">
									 <div class="col-6 pb-1"><span>Subtotal</span></div>
									 <div class="col-6 pb-1 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.sub_total}}</strong></div>
									 <div class="col-6 pb-1"><span>Discount <span ng-show="order.coupon_code.length>0">Promo Code : {{order.coupon_code}}</span></span></div>
									 <div class="col-6 pb-1 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.applied_discount_amount}}</strong></div>
									 <div class="col-6 pb-1"><span>Delivery fee</span></div>
									 <div class="col-6 pb-1 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.applied_delivery_charge}}</strong></div>
									 <!-- <div class="col-6"><span>VAT (13%)</span></div>
									 <div class="col-6 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.applied_tax_amount}}</strong></div> -->
								  </div>
							   </div>
							   <div class="list-group-item">
								  <div class="row align-items-center">
									 <div class="col-6"><strong>Total</strong>
										<p class="m-0">{{order.payment_mode}}</p></div>
									 <div class="col-6 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong>{{order.grand_total}}</strong></div>
								  </div>
							   </div>
							</div>
		  </div>
		  <div class="modal-footer">
			<div ng-if="type=='Pending'">
			  <button  ng-click="acceptOrder(order.ref_id)" type="button" class="btn btn-success"><i class="fal fa-check fa-fw mr-1"></i>Accept</button>
			  <button  href="#" data-toggle="modal" data-target="#orderRejectModal" type="button" class="btn btn-danger"><i class="fal fa-times fa-fw mr-1"></i>Reject</button>
			</div>
			<div ng-if="type=='Preparing'">
			  <button href="#" data-toggle="modal" data-target="#orderDeliverModal"  ng-click="deliverOrder(order.ref_id)" type="button" class="btn btn-success"><i class="fal fa-check fa-fw mr-1"></i>Out For Delivery</button>
			</div>
			<div ng-if="type=='On Going'">
			  <button  ng-click="markAsDelivered(order.ref_id)" type="button" class="btn btn-success"><i class="fal fa-check fa-fw mr-1"></i>Mark as Delivered</button>
			</div>
			<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
		  </div>
		</div>
	  </div>
	</div>
	
	
</div>

<script src="<?= base_url() ?>vendor_assets/js/angular/allOrdersCtrl.js?r=<?=time()?>"></script>