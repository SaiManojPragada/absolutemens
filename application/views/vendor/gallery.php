<div ng-controller="restaurantGalleryCtrl">
<div class="card shadow-sm border-0 mixwithbg" >
    <div class="card-header">
        <div class="row align-items-center">
          <div class="col">
            <strong>Business Gallery</strong>
          </div>
        <div class="col-auto">
            <a ng-click="addGalleryItem()"  data-toggle="modal" class="btn btn-link btn-sm"><strong>Add</strong> <i class="fal fa-plus fa-fw"></i></a>
        </div>
      </div>
    </div>
    <div class="card-body">
        <div class="row" >
        <div class="col-md-3" ng-repeat="item in gallery track by $index">
            <img style="width:200px; height:auto" ng-src="{{item.image}}" title="{{item.title}}"/>
            <br><br>
            <div align='center'>
              <button type="button" ng-click="deleteImage(item)" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="galleryFrom" ng-submit="addUpdateGallery()">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{galleryItem.id?"Update":"Add"}} Gallery</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-12">
                            <div class="row rm-small">
                                <div class="col-12 col-md-6  cp-small">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" class="form-control" name="title" ng-model="galleryItem.title">
                                    </div>
                                </div>
                                <div class="col-12 cp-small">
                                    <div class="form-group">
                                        <label>Image (Width:300px; height:300px)</label>
                                        <div class="custom-file">
                                            <input type="file" name="image" file-model="galleryItem.image">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">{{galleryItem.id?"Update":"Add"}}</button>
                </div>
        </div>
        </form>
    </div>
  </div>
</div>



<script src="<?= base_url() ?>vendor_assets/js/angular/restaurantGalleryCtrl.js"></script>
