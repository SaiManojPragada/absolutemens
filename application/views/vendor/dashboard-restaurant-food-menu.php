
<?php require_once('dashboard-restaurant-banner.php'); ?>

<section class="content-section pt-3 pb-5">
   <div class="container">
      <div class="row stick_row">
                  <div class="col-3">
            <div class="stick_col">
               <div class="pt-4">
                  <?php require_once('dashboard-restaurant-menu.php'); ?>
               </div>
            </div>
         </div>
         <div class="col-9">
            <div class="pt-4">
             <div class="card whitebox">
                <div class="card-header"><strong>Food Menu</strong></div>
                <div class="card-body">

         

                  <div class="row">
                    <div class="col-auto">
                      
<ul class="nav flex-column" role="tablist">
      <?php foreach ($resp->menu as $key =>$item) {?>
  <li class="nav-item">
    <a class="nav-link px-0 <?php if($key == 0){echo 'active';} ?>" data-toggle="tab" href="#m<?php echo $item->menu->id; ?>" role="tab"><?php echo $item->menu->name; ?></a>
  </li>
  <?php }?>
</ul>
                    </div>
                    <div class="col">
                      <div class="tab-content">
  <?php foreach ($resp->menu as $key =>$item) {?>
  <div class="tab-pane <?php if($key == 0){echo 'active';} ?>" id="m<?php echo $item->menu->id; ?>" role="tabpanel">

  <h6 class="text-primary"><?php echo $item->menu->name; ?></h6>
<div class="list-group list-group-flush mb-3">
       <?php foreach ($item->menu->categories as $item) {?>               
                        <?php foreach ($item->category->items as $item) {?>
        <div class="list-group-item px-0">
                           <div class="row align-items-center">
                          
                              <div class="col">
                                 <strong class="d-block"><?php echo $item->item->name; ?></strong>
                                 <small><?php echo $item->item->desc; ?></small>
                              </div>
                              <div class="col-auto"><small class="fal fa-rupee-sign fa-fw"></small><?php echo $item->item->min_price; ?></div>
                            
                           </div>
                        </div>

                           <?php }?>

                            <?php }?>
                          </div>
      

    </div>
    <?php }?>
</div>
                    </div>
                  </div>

                  



              


                 </div>
             </div>
            </div>
         </div>

      </div>
   </div>
</section>