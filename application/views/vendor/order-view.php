<?php require_once('dashboard-restaurant-banner.php'); ?>
<section class="content-section pt-3 pb-5">
    <div class="container">
        <div class="row stick_row">
            <?php require_once('dashboard-restaurant-menu.php'); ?>
            <div class="col-9">
                <div class="pt-4">
                    <div class="card whitebox">
                        <div class="card-header"><strong>Order Info</strong></div>
                        <div class="card-body">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <h6 class="m-0">#G4F-347076</h6>
                                        </div>
                                        <div class="col-auto">
                                            <p class="m-0">Order - Restaurant Accepted <i class="fas fa-check-circle text-success"></i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="list-group">
                                <?php foreach ($resp->cart->cart_items as $key => $item) {?>                     
                                <div class="list-group-item">
                                    <div class="row align-items-center">
                                        <div class="col"><span><?php echo $item->name; ?></span></div>
                                        <div class="col-auto"><small class="fal fa-rupee-sign fa-fw"></small><strong><?php echo $item->price; ?></strong></div>
                                    </div>
                                </div>
                                <?php }?>
                                <div class="list-group-item">
                                    <div class="row align-items-center">
                                        <div class="col-6 pb-1"><span>Subtotal</span></div>
                                        <div class="col-6 pb-1 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong><?php echo $resp->cart->subtotal; ?></strong></div>
                                        <div class="col-6 pb-1"><span>Discount</span></div>
                                        <div class="col-6 pb-1 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong><?php echo $resp->cart->discount; ?></strong></div>
                                        <div class="col-6 pb-1"><span>Delivery fee</span></div>
                                        <div class="col-6 pb-1 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong><?php echo $resp->cart->deliveryfee; ?></strong></div>
                                        <div class="col-6"><span>VAT (13%)</span></div>
                                        <div class="col-6 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong><?php echo $resp->cart->vat; ?></strong></div>
                                    </div>
                                </div>
                                <div class="list-group-item">
                                    <div class="row align-items-center">
                                        <div class="col-6">
                                            <strong>Total</strong>
                                            <p class="m-0">Paid via Credit/Debit card</p>
                                        </div>
                                        <div class="col-6 text-right"><small class="fal fa-rupee-sign fa-fw"></small><strong><?php echo $resp->cart->total; ?></strong></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>