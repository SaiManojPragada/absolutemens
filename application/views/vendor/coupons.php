<div ng-controller="restaurantCategoriesCtrl">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Coupons</strong></div>
                <div class="col-auto">
                    <a ng-click="showModal(0)" class="btn btn-link btn-sm"><strong>Add</strong> <i class="fal fa-plus fa-fw"></i></a>
                </div>
            </div>
        </div>
        <div class="card-body p-0"> 
            <div class="list-group list-group-flush">
                <h3 class="text-center" ng-if="categories.length == 0">No categories found</h3>
                <div class="list-group-item" href="#section0" ng-repeat ="category in categories">
                    <div class="row align-items-center">
                        <div class="col">
                            <strong class="text-head">{{category.name}} {{category.is_recommened == 1 ? "(recommended)" : ""}}</strong>
                        </div>
                        <div class="col-auto">
                            <a ng-click="showModal(1, category)" class="btn btn-link text-secondary fal fa-pen"></a>
                            <a ng-click="deleteCategory(category)" class="btn btn-link text-secondary fal fa-trash"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="categoryModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="restaurant_category_form" ng-submit="addUpdateCategory()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{title}} Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" ng-model="category.name">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Priority</label>
                                    <input type="number" string-to-number class="form-control" name="priority" ng-model="category.priority">
                                </div>
                            </div>
                            <div class="col-12">
                                <label for=""> Is Recommended</label> &nbsp;
                                <div class="form-check form-check-inline">
                                    <input id="is_recommened1" name="is_recommened" type="radio" ng-model="category.is_recommened" class="form-check-input" value="1">
                                    <label for="is_recommened1" class="form-check-label">Yes</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input id="is_recommened2" name="is_recommened" type="radio" ng-model="category.is_recommened" class="form-check-input" value="0">
                                    <label for="is_recommened2" class="form-check-label">No</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">{{title| uppercase}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="<?= base_url() ?>vendor_assets/js/angular/restaurantCategoriesCtrl.js"></script>
