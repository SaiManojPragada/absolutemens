<div class="card shadow-sm border-0 mixwithbg" ng-controller="restaurantProfileCtrl">
    <div class="card-header">
        <strong>Store Info</strong>
    </div>
    <div class="card-body">
        <form id="restaurant_profile_form" name="restaurant_profile_form" method="get" class="w-500 mx-auto" ng-submit="updateRestaturantProfile()">
            <div class="row rm-small">
                <div class="col-12 cp-small"> 
                    <div class="form-group">
                        <label>Shop Name</label>
                        <input type="text" class="form-control" ng-model="restaurant.business_name" ng-pattern="/^[A-Za-z0-9 ]+$/" name="business_name">
                        <span ng-show="restaurant_profile_form.business_name.$error.pattern" style="color: #B00020">Only Alphanumeric Values are Allowed and length should be Atleast 3 Characters</span>
                    </div>
                </div>
                <div class="col-12 cp-small">
                    <div class="form-group">
                        <label>Shop Image (Width:300px; height:300px)</label>
                        <div class="custom-file">
                            <input type="file" name="image" file-model="restaurant.display_image">
                        </div>
                    </div>
                </div>
                <div class="col-12 cp-small">
                    <div class="form-group">
                        <label>Prev Image : </label>
                        <img src="<?= base_url('uploads/stores/') ?>{{restaurant.display_image}}" width="70">
                    </div>
                </div>
                <div class="col-12 cp-small">
                    <div class="form-group">
                        <label>Business Description</label>
                        <textarea rows="5" class="form-control" ng-model="restaurant.description" name="description"></textarea>
                    </div>
                </div>
                <div class="col-6 cp-small">
                    <div class="form-group">
                        <label>State</label>
                        <select class="custom-select" ng-change="changeState(restaurant.states_id)" name="states_id" ng-model="restaurant.states_id">
                            <option value="{{state.id}}" ng-repeat="state in states">{{state.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-6 cp-small">
                    <div class="form-group">
                        <label>District</label>
                        <select class="custom-select" ng-change="changeDistrict(restaurant.districts_id)" name="districts_id" ng-model="restaurant.districts_id">
                            <option value="{{district.id}}" ng-repeat="district in districts">{{district.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-6 cp-small">
                    <div class="form-group">
                        <label>City</label>
                        <select class="custom-select" name="cities_id" ng-model="restaurant.cities_id">
                            <option value="{{city.id}}" ng-repeat="city in cities">{{city.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-6 cp-small">
                    <div class="form-group">
                        <label>Land Mark </label>
                        <input type="text" class="form-control" name="land_mark" ng-model="restaurant.land_mark"  ng-pattern="/^[A-Za-z0-9 ]+$/">
                        <span ng-show="restaurant_profile_form.land_mark.$error.pattern" style="color: #B00020">Only Alphanumeric Values are Allowed and length should be Atleast 3 Characters</span>

                    </div>
                </div>
                <div class="col-12 cp-small">
                    <div class="form-group">
                        <label>Address </label>
                        <input type="text" class="form-control" name="address" ng-model="restaurant.address">
                    </div>
                </div>
                <div class="col-6 cp-small">
                    <div class="form-group">
                        <label>Email Address </label>
                        <input type="email" class="form-control" name="contact_email" ng-model="restaurant.contact_email">
                    </div>
                </div>
                <div class="col-6 cp-small">
                    <div class="form-group">
                        <label>Phone Number (Read Only)</label>
                        <input disabled type="number" string-to-number class="form-control" name="mobile" ng-model="restaurant.contact_number">
                    </div>
                </div>
                <div class="col-12 cp-small">
                    <div class="form-group">
                        <label>Owner Name</label>
                        <input type="text" class="form-control" name="owner_name" ng-model="restaurant.owner_name" ng-pattern="/^[A-Za-z0-9 ]+$/">
                        <span ng-show="restaurant_profile_form.owner_name.$error.pattern" style="color: #B00020">Only Alphanumeric Values are Allowed and length should be Atleast 3 Characters</span>

                    </div>
                </div>

                <div class="col-12 cp-small"><button ng-click="updateRestaturantProfile()" class="btn btn-primary text-uppercase px-5"><strong>Save</strong></button></div>

                <?php /*
                  if (!$this->session->userdata("admin_control")) {
                  ?>
                  <div class="col-12 cp-small">
                  <br/>
                  <p><span class="text-danger">Note:</span>Please contact our support for update min order amount</p>
                  </div>
                  <?php } */ ?>
            </div>
        </form>
    </div>
</div>

<script src="<?= base_url() ?>vendor_assets/js/angular/restaurantProfileCtrl.js?r=<?= time() ?>"></script>
