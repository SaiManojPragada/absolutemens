
<?php require_once('dashboard-restaurant-banner.php'); ?>

<section class="content-section pt-3 pb-5">
   <div class="container">
      <div class="row stick_row">
     <?php require_once('dashboard-restaurant-menu.php'); ?>
         <div class="col-9">
            <div class="pt-4">
             <div class="card whitebox">
                <div class="card-header"><strong>Dashboard</strong></div>
                <div class="card-body">


               
<div class="row rm-small"><div class="col-4 cp-small mb-3 text-center">
<div class="card">
  <div class="card-body">
    <span class="fa-stack fa-2x d-block mx-auto">
      <i class="fas fa-circle fa-stack-2x text-primary"></i>
      <i class="fal fa-shopping-bag fa-stack-1x fa-inverse"></i>
    </span>
      <small class="d-block my-2">TOTAL ORDERS</small>
      <h4 class="m-0">59</h4>
  </div>
</div></div>

<div class="col-4 cp-small mb-3 text-center">
<div class="card">
  <div class="card-body">
    <span class="fa-stack fa-2x d-block mx-auto">
      <i class="fas fa-circle fa-stack-2x text-primary"></i>
      <i class="fal fa-rupee-sign fa-stack-1x fa-inverse"></i>
    </span>
      <small class="d-block my-2">TOTAL EARNINGS</small>
      <h4 class="m-0">300.90</h4>
  </div>
</div></div>

<div class="col-4 cp-small mb-3 text-center">
<div class="card">
  <div class="card-body">
    <span class="fa-stack fa-2x d-block mx-auto">
      <i class="fas fa-circle fa-stack-2x text-primary"></i>
      <i class="fal fa-check fa-stack-1x fa-inverse"></i>
    </span>
      <small class="d-block my-2">ACCEPTED ORDERS</small>
      <h4 class="m-0">32</h4>
  </div>
</div></div>

<div class="col-4 cp-small mb-3 text-center">
<div class="card">
  <div class="card-body">
    <span class="fa-stack fa-2x d-block mx-auto">
      <i class="fas fa-circle fa-stack-2x text-primary"></i>
      <i class="fal fa-times fa-stack-1x fa-inverse"></i>
    </span>
      <small class="d-block my-2">DENIED ORDERS</small>
      <h4 class="m-0">24</h4>
  </div>
</div></div>

<div class="col-4 cp-small mb-3 text-center">
<div class="card">
  <div class="card-body">
    <span class="fa-stack fa-2x d-block mx-auto">
      <i class="fas fa-circle fa-stack-2x text-primary"></i>
      <i class="fal fa-align-left fa-stack-1x fa-inverse"></i>
    </span>
    <small class="d-block my-2">TOTAL MENU ITEMS</small>
      <h4 class="m-0">13</h4>
  </div>
</div></div>
</div>





 
                </div>
             </div>
            </div>
         </div>

      </div>
   </div>
</section>