<div class="col-3">
    <div class="stick_col">
        <div class="pt-4"> 
            <div class="res-menu"><ul class="accordion list-unstyled m-0" id="res-menu">
                    <li><a href="dashboard-restaurant.php">Dashboard</a></li>
                    <li class="hassub"><a href="#" data-toggle="collapse" data-target="#cid0">Business Info</a>
                        <ul id="cid0" class="collapse list-unstyled" data-parent="#res-menu">
                            <li><a href="dashboard-restaurant-info.php">Business Info</a></li>
                            <li><a href="dashboard-restaurant-statistics.php">Business Statistics</a></li>
                            <li><a href="dashboard-restaurant-account.php">Payment Account</a></li>


                        </ul>
                    </li>

                    <li class="hassub"><a href="#" data-toggle="collapse" data-target="#cid2">Food Menu</a>
                        <ul id="cid2" class="collapse list-unstyled" data-parent="#res-menu">
                            <li><a href="dashboard-restaurant-categories.php">Categories</a></li>
                            <li><a href="dashboard-restaurant-subcategories.php">SubCategories</a></li>
                            <li><a href="dashboard-restaurant-fooditems.php">Food Items</a></li>
                            <li><a href="dashboard-restaurant-food-menu-preview.php">Food Menu Preview</a></li>

                        </ul>
                    </li>
                    <li class="hassub"><a href="#" data-toggle="collapse" data-target="#cid1">Orders</a>
                        <ul id="cid1" class="collapse list-unstyled" data-parent="#res-menu">
                            <li><a href="dashboard-restaurant-orders.php">New Orders</a></li>
                            <li><a href="dashboard-restaurant-ongoing-orders.php">Ongoing Orders</a></li>
                            <li><a href="dashboard-restaurant-rejected-orders.php">Rejected Orders</a></li>
                            <li><a href="dashboard-restaurant-completed-orders.php">Completed Orders</a></li>
                        </ul>
                    </li>

                    <li><a href="index.php">Logout</a></li>
                </ul></div>

        </div>
    </div>
</div>