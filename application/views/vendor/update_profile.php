<div class="card shadow-sm border-0 mixwithbg" ng-controller="updateProfileCtrl">
    <div class="card-header">
        <strong>Change Password</strong>
    </div>
    <div class="card-body" ng-init="getProfileInfo()">
        <form id="update_profile_form" method="post" class="w-auto mx-auto" ng-submit="updateProfile()">
            <div class="row rm-small">
                <div class="col-4 cp-small">
                    <div class="form-group">
                        <label>Vendor Name</label>
                        <input type="text" class="form-control" ng-model="profile.business_name" name="business_name">
                    </div>
                </div>
                <div class="col-4 cp-small">
                    <div class="form-group">
                        <label>Contact Email</label>
                        <input type="text" class="form-control" ng-model="profile.contact_email" name="contact_email">
                    </div>
                </div>
                <div class="col-4 cp-small">
                    <div class="form-group">
                        <label>Contact Number</label>
                        <input type="text" class="form-control" ng-model="profile.contact_number" name="contact_number">
                    </div>
                </div>
                <div class="col-4 cp-small">
                    <div class="form-group">
                        <label>Alternate Contact Number</label>
                        <input type="text" class="form-control" ng-model="profile.alternate_contact_number" name="alternate_contact_number">
                    </div>
                </div>
                <div class="col-4 cp-small">
                    <div class="form-group">
                        <label>Select City</label>
                        <select class="form-control" ng-model="profile.cities_id" name="cities_id">
                            <option value="">--select city--</option>
                            <option value="{{city.id}}" ng-repeat="city in cities">{{city.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="col-4 cp-small">
                    <div class="form-group">
                        <label>Address</label>
                        <textarea ng-model="profile.address" class="form-control" rows="4" name="address"></textarea>
                    </div>
                </div>
                <div class="col-4 cp-small">
                    <div class="form-group">
                        <label>Pincode</label>
                        <input type="text" class="form-control" ng-model="profile.pincode" name="pincode">
                    </div>
                </div>
                <div class="col-4 cp-small">
                    <div class="form-group">
                        <label>GST Number</label>
                        <input type="text" class="form-control" ng-model="profile.gst_number" name="gst_number">
                    </div>
                </div>
                <div class="col-12 form-group">
                    <button type="submit" class="btn btn-primary form-control col-4">Update Profile</button>
                </div>
            </div>
        </form>
    </div><script src="<?= base_url() ?>vendor_assets/js/angular/updateProfileCtrl.js"></script>