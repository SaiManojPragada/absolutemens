<div class="card shadow-sm border-0 mixwithbg" ng-controller="restaurantDocumentsCtrl">
    <div class="card-header">
        <strong>Business Registration Documents</strong>
    </div>
    <div class="card-body">
        <h4>If you want to update your documents please contact
            <a href="mailto:admin@indiasmartlife.com">admin@indiasmartlife.com</a></h4>

        <div ng-repeat="item in documents track by $index">
            <img style="width:200px; height:auto" ng-src="{{item.file}}" title="{{item.document_name}}"/>            
        </div>
    </div>
</div>

<script src="<?= base_url() ?>vendor_assets/js/angular/restaurantDocumentsCtrl.js"></script>
