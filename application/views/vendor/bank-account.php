<div ng-controller="restaurantBankAccountCtrl as rb" ng-init="rb.focus();">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <strong>Bank Account Details for Payout</strong>
        </div>
        <div class="card-body">
            <form ng-submit="addUpdateBankDetails()" id="bankForm" class="w-500 mx-auto">
                <input type="hidden" id="restaurants_id" name="restaurants_id" value="<?php echo $this->vendor_model->get_logged_vendor_id(); ?>" >
                <div class="row rm-small">
                    <div class="col-12 cp-small">
                        <div class="form-group"> 
                            <label>Account Number <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="account_number" name="account_number" ng-blur="getDetailsByAC()" ng-model="bank.account_number">
                        </div>
                    </div>
                    <div class="col-12 cp-small">
                        <div class="form-group">
                            <label>Account Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="account_name" name="account_name" ng-model="bank.account_name">
                        </div>
                    </div>
                    <div class="col-12 cp-small">
                        <div class="form-group">
                            <label>IFSC Code <span class="text-danger">*</span></label>
                            <input type="text" class="form-control text-uppercase" name="ifsc_code" ng-blur="getIfscDetails()" ng-model="bank.ifsc_code">
                        </div>
                    </div>
                    <div class="col-12 cp-small">
                        <div class="form-group">
                            <label>Bank Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" readonly name="bank_name" ng-model="bank.bank_name">
                        </div>
                    </div>

                    <div class="col-12 cp-small">
                        <div class="form-group">
                            <label>Branch <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" readonly name="branch" ng-model="bank.branch">
                        </div>
                    </div>

                    <div class="col-12 cp-small">
                        <button type="submit" class="btn btn-primary text-uppercase px-5"><strong>Submit</strong></button></div>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>vendor_assets/js/angular/restaurantBankAccountCtrl.js?r=<?= time() ?>"></script>