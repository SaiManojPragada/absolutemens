<div ng-controller="productsCtrl" ng-init="getStockList();">
    <div class="card shadow-sm border-0 mixwithbg">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col"><strong>Stock Management</strong></div>
                <div class="col-auto">
                    <a ng-click="ManageStock()"  data-toggle="modal" class="btn btn-link btn-sm"><strong>Manage</strong> <i class="fal fa-plus fa-fw"></i></a>
                </div>
            </div>

        </div>
        <div class="card-body p-0">
            <br/>
            <div class="col-md-12">
                <h5>Product details : </h5>
                <hr/>
                <table class="table table-bordered">
                    <tr>
                        <td> Product : {{product_details.title}}</td>
                        <td> Category : {{product_details.category}}</td>
                        <td> SubCategory : {{product_details.sub_category}}</td>
                        <td> Brand : {{product_details.brand}}</td>
                        <td> Size : {{product_details.size}}</td>
                        <td> Price : <i class="fa fa-inr"></i>{{product_details.price}}</td>
                    </tr>
                </table>
                <br/>
                <form ng-submit="getStockList()">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Select type</label>
                                <select  class="form-control" ng-model="filter.action" >
                                    <option value="">All</option>
                                    <option value="credit">Credit</option>
                                    <option value="debit">Debit</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>From date</label>
                                <input type="date" class="form-control" ng-model="filter.from_date" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>To date</label>
                                <input type="date" class="form-control" ng-model="filter.to_date" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" style="margin-top: 29px;" class="btn btn-primary" >Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <br/>
            <div class="container-fluid" ng-if="stockList.length == 0">
                <h4 class="text-center ">No data found</h4>
            </div>
            <div class="list-group list-group-flush" ng-if="stockList.length > 0">
                <div class="list-group-item" >
                    <div class="row align-items-center">
                        <div class="col-1">#</div>
                        <div class="col-2">Type</div>
                        <div class="col-2">For</div>
                        <div class="col-1">Quantity</div>
                        <div class="col-4">Comment</div>
                        <div class="col-2">Balance</div>
                    </div>
                </div>
                <div class="list-group-item" clone="" ng-repeat="item in stockList track by $index">
                    <div class="row align-items-center">
                        <div class="col-1">{{$index + 1}}</div>
                        <div class="col-2"><h6><span class="badge badge-{{item.type == 'credit' ? 'success' : 'danger'}}">{{item.type}}</span></h6></div>
                        <div class="col-2">{{item.action_for}}</div>
                        <div class="col-1"> {{item.quantity}}</div>
                        <div class="col-4">{{item.comment}}</div>
                        <div class="col-2">{{item.balance}}</div>
                    </div>
                </div>
            </div>
            <hr/>
            <span ng-bind-html="stockListPagination.pagination"></span>
        </div>
    </div>
    <div class="modal fade" id="stockModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="stock_form" ng-submit="AddOrRemoveStock()">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Manage Stock</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="row rm-small">

                                    <div class="col-12 col-md-6 cp-small">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="custom-select" name="type" ng-model="stockObj.type">
                                                <option value="">Select type</option>
                                                <option value="credit">Credit</option>
                                                <option value="debit">Debit</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 cp-small">
                                        <div class="form-group">
                                            <label>Stock</label>
                                            <input type="text" class="form-control" name="quantity" ng-model="stockObj.quantity" />
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 cp-small">
                                        <button type="submit" class="btn btn-info">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                </div>
            </div>

        </div>
    </div>
</div>
<script>
    product_inventory_id = '<?= $product_inventory_id ?>';
</script>
<script src="<?= base_url() ?>vendor_assets/js/angular/productsCtrl.js?r=<?= time() ?>"></script>
