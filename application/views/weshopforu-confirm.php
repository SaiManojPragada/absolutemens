<div class="pkgsteps">
	<?php include 'includes/header.php' ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-12">
				<div class="whitebox orderconfirm">
					<div class="row">
						<div class="col-lg-12">
							<h3 class="text-center text-black-50">We Shop For You Order Confirm</h3>
						</div>
					</div>
					<div class="row pkgitems">
						<div class="col-lg-3 col-md-4">
							<a href="package.php"><img src="images/banner-2.jpg" alt="banner" /></a>
						</div>
						<div class="col-lg-8 col-md-8 align-self-center">
							<h5>Title <small>Small title</small></h5><hr>
							<p><strong>Price : </strong><br><i class="fal fa-rupee-sign"></i> 3500/-</p>
							<p><strong>Details</strong> <br>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet, voluptatem, accusamus? Voluptates harum itaque deleniti placeat eius eaque vero, quam quasi at quaerat consequuntur officiis perferendis facere inventore exercitationem assumenda.
							</p>
							<ul>
								<li>Rent 4 items at a time </li>
								<li>10,000+ styles from 400 + desginers </li>
								<li>Wear items with retail value with up to 500 </li>
								<li>Monthly shipments worth upto 2000</li>
							</ul>
						</div>
					</div><hr>
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<a href="we-shop-for-you-step-3.php" class="btn-edit"><i class="fal fa-edit"></i> Edit</a>
						</div>
						<div class="col-lg-12">
							<p><strong>Shipping Address</strong></p>
							<div class="row">
								<div class="col-lg-6 col-md-6">
									<ul>
										<li><span>Name</span>: Kumar</li>
										<li><span>Address</span>: Bharat Towers, Flat No. 404, 4th Floor</li>
										<li><span>City</span>: Visakhapatnam</li>
										<li><span>State</span>: Andhra Pradesh</li>
										<li><span>District</span>: Visakha</li>
									</ul>
								</div>
								<div class="col-lg-6 col-md-6">
									<ul>
										<li><span>Landmark</span>: Near Tilak Show Room</li>
										<li><span>Street Name</span>: 5th Lane</li>
										<li><span>Pincode</span>: 530016</li>
										<li><span>Email</span>: infokumar@gmail.com </li>
										<li><span>Phone No.</span>: 1234567890</li>
										<li><span>Phone No.(Optional)</span>: 1234567890</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<a href="we-shop-for-you-step-1.php" class="btn-edit"><i class="fal fa-edit"></i> Edit</a>
						</div>
						<div class="col-lg-12">
							<p><strong>Budget</strong></p>
							<p><i class="fal fa-rupee-sign"></i> 5500/-</p>
							<p><strong>Occasion</strong></p>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio consectetur numquam amet ea sapiente, beatae autem animi, ullam dicta architecto saepe? Numquam, pariatur. Sed iste illo natus est impedit totam.</p>
							<p><strong>Description</strong></p>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio consectetur numquam amet ea sapiente, beatae autem animi, ullam dicta architecto saepe? Numquam, pariatur. Sed iste illo natus est impedit totam.</p>
							<p><strong>Likes</strong></p>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio consectetur numquam amet ea sapiente, beatae autem animi, ullam dicta architecto saepe? Numquam, pariatur. Sed iste illo natus est impedit totam.</p>
							<p><strong>Disikes</strong></p>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio consectetur numquam amet ea sapiente, beatae autem animi, ullam dicta architecto saepe? Numquam, pariatur. Sed iste illo natus est impedit totam.</p>
						</div>
					</div> <hr>
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<a href="we-shop-for-you-step-2.php" class="btn-edit"><i class="fal fa-edit"></i> Edit</a>
						</div>
						<div class="col-lg-4 col-md-6">
							<p><strong>Your Size Details</strong></p>
							<ul>
								<li><span>Shirt Size</span>:  XL</li>
								<li><span>T-Shirt Size</span>:  L</li>
								<li><span>Casual pants size</span>:  40</li>
								<li><span>Formals size</span>:  42</li>
								<li><span>Jeans size</span>:  42</li>
								<li><span>Shoes size</span>:  42</li>
							</ul>
						</div>
						<!-- <div class="col-lg-4 col-md-6">
							<p><strong>Your Preferences</strong></p>
							<ul>
								<li><span>Height</span>: 6.0"</li>
								<li><span>Weight</span>: 70"</li>
							</ul>
						</div> -->
					</div>
					<div class="row pt-2">
						<div class="col-lg-12">
							<p><strong>Full Sized Photos</strong></p>	
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 updphsize">
							<img src="images/upd-photo-1.jpg" alt="">
							<img src="images/upd-photo-2.jpg" alt="">
							<img src="images/upd-photo-3.jpg" alt="">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<p class="pt-3"><strong>Half Sized Photos</strong></p>	
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-lg-12 updphhfsize">
							<img src="images/upd-photo-4.jpg" alt="">
							<img src="images/upd-photo-5.jpg" alt="">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<p><strong>Special Instructions</strong></p>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio consectetur numquam amet ea sapiente, beatae autem animi, ullam dicta architecto saepe? Numquam, pariatur. Sed iste illo natus est impedit totam.</p>
							<p><strong>Special Measurements</strong></p>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio consectetur numquam amet ea sapiente, beatae autem animi, ullam dicta architecto saepe? Numquam, pariatur. Sed iste illo natus est impedit totam.</p>
						</div>
						<div class="col-lg-12 col-md-12">
							<div class="form-check">
							  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
							  <label class="form-check-label" for="defaultCheck1">
							   I Accepet the <a href="terms-and-conditions.php">Terms and conditions</a>
							  </label>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-lg-12">
							<button type="submit" class="btn-submit" onclick="location.href='place-order.php'">Order Confirm</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<?php include 'includes/footer.php' ?>