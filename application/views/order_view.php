<html>
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>uploads/<?= $site_property->favicon ?>">
</html>
<div class="pkgsteps">
    <?= $this->load->view("includes/header", $main_data); ?>
</div>
<div class="subpagebg">
</div>
<div class="greybg">
    <br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-4">
                <?php $this->load->view('menu-dashboard'); ?>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="whitebox contentdashboard">
                    <h3>#<?= $order_data->unq_id ?>
                        <a href="javascript:history.back();" style="float: right; font-size: 14px"><i class="fa fa-angle-left"></i> Back</a></h3>
                    <?php if (!$order_data->is_we_shop_for_you) { ?>
                        <div class="row mt-4">
                            <div class="col-3">
                                <img src="<?= base_url('uploads/' . $package_details->image) ?>" alt="" class="product-img mb-3">
                            </div>
                            <div class="col-9">
                                <h3>Package Details</h3>
                                <span><b><?= strip_tags($package_details->title) ?></b></span><br>
                                <span><b>Total Items  : </b><?= $order_data->no_of_items ?></span><br>
                                <span><b>Duration  : </b><?= $order_data->package_duration ?> days</span><br>
                                <span><?= $package_details->short_description ?></span>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-12">
                            <center><h3>We Shop For you Order</h3></center>
                        </div>
                    <?php } ?>
                    <h3>Order Details</h3> 
                    <div class="row mt-4">
                        <?php if ($order_data->is_we_shop_for_you) { ?>
                            <div class="col-5"><strong>No of Items </strong></div>
                            <div class="col-1"><strong> : </strong></div>
                            <div class="col-6"><?= $order_data->no_of_items ?></div>
                        <?php } ?>
                        <div class="col-5"><strong>Selected Styles </strong></div>
                        <div class="col-1"><strong> : </strong></div>
                        <div class="col-6"><?php
                            foreach ($selected_styles as $style) {
                                $comma .= $style->name . ', ';
                            }
                            echo rtrim($comma, ', ') . '.';
                            ?></div>
                        <div class="col-5"><strong>Ordered On </strong></div>
                        <div class="col-1"><strong> : </strong></div>
                        <div class="col-6"><?= empty($order_data->updated_at) ? 'N/A' : date('d M Y, h:i A', $order_data->updated_at) ?></div>
                        <div class="col-5"><strong>Expires On </strong></div>
                        <div class="col-1"><strong> : </strong></div>
                        <div class="col-6"><?= empty($order_data->expiry_date) ? 'N/A' : date('d M Y, h:i A', $order_data->expiry_date) ?></div>
                        <?php if (!empty($order_data->dispatched_date)) { ?>
                            <div class="col-5"><strong>Dispatched On </strong></div>
                            <div class="col-1"><strong> : </strong></div>
                            <div class="col-6"><?= empty($order_data->dispatched_date) ? 'N/A' : date('d M Y', $order_data->dispatched_date) ?></div>
                        <?php } ?>
                        <?php if (!empty($order_data->shipped_date)) { ?>
                            <div class="col-5"><strong>Shipped On </strong></div>
                            <div class="col-1"><strong> : </strong></div>
                            <div class="col-6"><?= empty($order_data->shipped_date) ? 'N/A' : date('d M Y', $order_data->shipped_date) ?></div>
                        <?php } ?>
                        <?php if (!empty($order_data->delivered_date)) { ?>
                            <div class="col-5"><strong>Delivered On </strong></div>
                            <div class="col-1"><strong> : </strong></div>
                            <div class="col-6"><?= empty($order_data->delivered_date) ? 'N/A' : date('d M Y', $order_data->delivered_date) ?></div>
                        <?php } ?>
                        <?php if (!empty($order_data->exchange_request_date)) { ?>
                            <div class="col-5"><strong>Last Exchange Requested On </strong></div>
                            <div class="col-1"><strong> : </strong></div>
                            <div class="col-6"><?= empty($order_data->exchange_request_date) ? 'N/A' : date('d M Y', $order_data->exchange_request_date) ?></div>
                        <?php } ?>
                        <?php if (!empty($order_data->cancelled_date)) { ?>
                            <div class="col-5"><strong>Cancelled On </strong></div>
                            <div class="col-1"><strong> : </strong></div>
                            <div class="col-6"><?= empty($order_data->cancelled_date) ? 'N/A' : date('d M Y', $order_data->cancelled_date) ?></div>
                        <?php } ?>
                        <?php if (!empty($order_data->cancellation_reason)) { ?>
                            <div class="col-5"><strong>Cancellation Reason </strong></div>
                            <div class="col-1"><strong> : </strong></div>
                            <div class="col-6"><p style="word-break: break-all"><?= empty($order_data->cancellation_reason) ? 'N/A' : $order_data->cancellation_reason ?></p></div>
                        <?php } ?>
                        <?php if (!empty($order_data->cancellation_approved_message)) { ?>
                            <div class="col-5"><strong>Cancellation Approval Message </strong></div>
                            <div class="col-1"><strong> : </strong></div>
                            <div class="col-6"><p style="word-break: break-all"><?= empty($order_data->cancellation_approved_message) ? 'N/A' : $order_data->cancellation_approved_message ?></p></div>
                        <?php } ?>
                        <div class="col-12" style="margin-top: 12px;margin-bottom: 12px; margin-left: 4px; margin-right: 4px; height: 1px; background-color: #eaeaea"></div>
                        <div class="col-5"><strong>Transaction Id </strong></div>
                        <div class="col-1"><strong> : </strong></div>
                        <div class="col-6"><?= empty($order_data->transaction_id) ? 0 : $order_data->transaction_id ?></div>
                        <div class="col-5"><strong>Total Price </strong></div>
                        <div class="col-1"><strong> : </strong></div>
                        <div class="col-6"><?= empty($order_data->total_price) ? 0 : $order_data->total_price ?></div>
                        <div class="col-5"><strong>Discount </strong></div>
                        <div class="col-1"><strong> : </strong></div>
                        <div class="col-6"><?= empty($order_data->discount) ? 0 : $order_data->discount ?></div>
                        <div class="col-5"><strong>Delivery Charge </strong></div>
                        <div class="col-1"><strong> : </strong></div>
                        <div class="col-6"><?= empty($order_data->delivery_charges) ? 0 : $order_data->delivery_charges ?></div>
                        <div class="col-5"><strong>Coupon Discount </strong></div>
                        <div class="col-1"><strong> : </strong></div>
                        <div class="col-6"><?= empty($order_data->coupon_discount) ? 0 : $order_data->coupon_discount ?></div>
                        <div class="col-5"><strong>Grand Total </strong></div>
                        <div class="col-1"><strong> : </strong></div>
                        <div class="col-6"><?= empty($order_data->grand_total) ? 0 : $order_data->grand_total ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>