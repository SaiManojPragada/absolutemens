<?php

class Vendor_wallet_model extends CI_Model {

    function create_vendor_wallet_if_not_exists($restaurants_id) {
        $this->db->where("restaurants_id", $restaurants_id);
        if ($this->db->count_all_results("store_wallet") == 0) {
            $this->db->set("restaurants_id", $restaurants_id);
            $this->db->set("total_earnings", 0);
            $this->db->set("received_amount_from_admin", 0);
            $this->db->set("pending_amount_from_admin", 0);
            $this->db->insert("store_wallet");
        }
        return true;
    }

    function add_payable_amount_to_vendor_for_order_completed($service_order_ref_id) {
        $this->db->select("payable_amount_to_restaurant, restaurants_id, coupon_promotion_by, applied_discount_amount, service_date");
        $this->db->where("service_status", "Completed");
        $this->db->where("ref_id", $service_order_ref_id);
        $this->db->where("payable_amount_added_to_vendor_wallet", 0);
        $row = $this->db->get("service_orders")->row();

        if ($row) {
            $this->db->set("payable_amount_added_to_vendor_wallet", 1);
            $this->db->where("ref_id", $service_order_ref_id);
            $this->db->update("service_orders");

            $vendor_wallet = $this->get_vendor_wallet($row->restaurants_id);

            $updated_amount = $vendor_wallet->total_earnings + $row->payable_amount_to_restaurant;
            $updated_pending_amount = $vendor_wallet->pending_amount_from_admin + $row->payable_amount_to_restaurant;


            $this->db->where("restaurants_id", $row->restaurants_id);
            $this->db->set("total_earnings", $updated_amount);
            $this->db->set("pending_amount_from_admin", $updated_pending_amount);
            $this->db->set("updated_date", THIS_DATE_TIME);
            $this->db->update("store_wallet");

            $vendor_today_wallet_row = $this->get_vendor_today_wallet_row($row->restaurants_id, $row->service_date);


            $service_order_offered_amount_by_promotion = 0;
            if ($row->coupon_promotion_by == "Restaurant") {
                $service_order_offered_amount_by_promotion = $row->applied_discount_amount;
            }


            $updated_total_orders = $vendor_today_wallet_row->total_completed_orders + 1;
            $total_earnings = $vendor_today_wallet_row->total_earnings + $row->payable_amount_to_restaurant;
            $offered_amount_by_promotion = $vendor_today_wallet_row->offered_amount_by_promotion + $service_order_offered_amount_by_promotion;
            $this->db->set("total_completed_orders", $updated_total_orders);
            $this->db->set("total_earnings", $total_earnings);
            $this->db->set("offered_amount_by_promotion", $offered_amount_by_promotion);
            $this->db->where("date", $row->service_date);
            $this->db->where("restaurants_id", $row->restaurants_id);
            $this->db->update("store_wallet_history_date_wise");
        }
    }

    function get_vendor_wallet($restaurants_id) {
        $this->create_vendor_wallet_if_not_exists($restaurants_id);
        $this->db->where("restaurants_id", $restaurants_id);
        $row = $this->db->get("store_wallet")->row();
        return $row;
    }

    function get_vendor_today_wallet_row($restaurants_id, $date = null) {
        if (!isset($date)) {
            $date = date("Y-m-d");
        }
        $this->db->where("restaurants_id", $restaurants_id);
        $this->db->where("date", $date);
        $row = $this->db->get("store_wallet_history_date_wise")->row();
        if (!$row) {
            $this->db->set("restaurants_id", $restaurants_id);
            $this->db->set("total_completed_orders", 0);
            $this->db->set("total_earnings", 0);
            $this->db->set("settled_amount_by_admin", 0);
            $this->db->set("offered_amount_by_promotion", 0);
            $this->db->set("date", $date);
            $this->db->insert("store_wallet_history_date_wise");


            $this->db->where("restaurants_id", $restaurants_id);
            $this->db->where("date", $date);
            $row = $this->db->get("store_wallet_history_date_wise")->row();
        }
        return $row;
    }

    function add_settlement_transaction_history($storeData) {
        $this->db->set($storeData);
        $this->db->set("created_at", time());
        return $this->db->insert("vendor_settlement_transaction_history");
    }

}
