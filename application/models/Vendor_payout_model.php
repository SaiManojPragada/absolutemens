<?php

class Vendor_payout_model extends CI_Model {

    function create_payout_transaction($data) {
        $payout_ref_id = $this->generate_payout_ref_id();
        $data["narration"] = "Payout to store " . $data['ref_code'] . " for the date " . $data["from_date"] . " to " . $data["to_date"] . " with payout ref id " . $payout_ref_id;
        unset($data['ref_code']);
        $this->db->set("payout_ref_id", $payout_ref_id);
        $this->db->set($data);
        $this->db->insert("vendor_settlement_transaction_history");
        $id = $this->db->insert_id();

        $this->db->where("id IN (" . $data["service_order_ids"] . ")");
        $this->db->set("vendor_settlement_transaction_history_id", $id);
        $this->db->update("service_orders");

        return ["id" => $id, "payout_ref_id" => $payout_ref_id, "narration" => $data["narration"]];
    }

    function update_wallet_history_for_payout_completed($vendor_settlement_transaction_history_id) {

        $this->db->where("id", $vendor_settlement_transaction_history_id);
        $row = $this->db->get("vendor_settlement_transaction_history")->row();

        //Update service orders table
        $this->db->where("id IN (" . $row->service_order_ids . ")");
        $this->db->set("settlement_status", "Completed");
        $this->db->set("settlement_ref_id", $row->utr_ref_no);
        $this->db->set("settled_amount", "payable_amount_to_restaurant", false);
        $this->db->set("settled_date", $row->response_date_time);
        $this->db->set("settlement_remarks", $row->narration);
        $this->db->where("settlement_status", "Pending");
        $this->db->update("service_orders");
        if ($this->db->affected_rows() > 0) {
            $vendor_today_wallet_row = $this->vendor_wallet_model->get_vendor_today_wallet_row($row->restaurants_id, date("Y-m-d", strtotime($row->response_date_time)));

            //update wallet history
            $this->db->set("settled_amount_by_admin", $vendor_today_wallet_row->settled_amount_by_admin + $row->total_amount);
            $this->db->where("id", $vendor_today_wallet_row->id);
            $this->db->update("store_wallet_history_date_wise");

            //udate wallet
            $vendor_wallet = $this->vendor_wallet_model->get_vendor_wallet($row->restaurants_id);

            $updated_amount = $vendor_wallet->received_amount_from_admin + $row->total_amount;
            $updated_pending_amount = $vendor_wallet->pending_amount_from_admin - $row->total_amount;


            $this->db->where("restaurants_id", $row->restaurants_id);
            $this->db->set("received_amount_from_admin", $updated_amount);
            $this->db->set("pending_amount_from_admin", $updated_pending_amount);
            $this->db->set("updated_date", THIS_DATE_TIME);
            $this->db->update("store_wallet");

            $mobile = $this->db->select("mobile")->where("id", $row->restaurants_id)->get("restaurants")->row()->mobile;
            //send message to vendor
            $message = "Rs " . $row->total_amount . " is paid out to your bank a/c from " . SITE_TITLE . " on " . $row->response_date_time . " For the Date of " . $row->from_date . " to " . $row->to_date . " . Payout ID is " . $row->payout_ref_id . " and UTR No." . $row->utr_ref_no;
            send_message($message, $mobile, 1407161883521300484);
        }
    }

    function mark_as_manual_payout_done($data) {
        $this->db->trans_begin();
        $response = $this->create_payout_transaction($data);
        $vendor_settlement_transaction_history_id = $response["id"];

        $payout_ref_id = $response["payout_ref_id"];
        $this->update_wallet_history_for_payout_completed($vendor_settlement_transaction_history_id);

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function mark_as_auto_payout_done($data) {
        $this->db->trans_begin();

        $store_ref_code = $data["ref_code"];
        $beneficiary_code = $data["beneficiary_code"];


        unset($data["ref_code"]);
        unset($data["beneficiary_code"]);

        $response = $this->create_payout_transaction($data);
        $vendor_settlement_transaction_history_id = $response["id"];
        $payout_ref_id = $response["payout_ref_id"];

        $wire_transfer_data = [
            "beneficiary_code" => $beneficiary_code,
            "unique_request_number" => $payout_ref_id, //this is our local database unique id
            "payment_mode" => $data["payment_type"], // [IMPS, NEFT, RTGS, UPI]
            "amount" => $data["total_amount"],
            "narration" => $response["narration"],
        ];
        $payout_response = $this->easebuzz_payout_model->initiate_transfer($wire_transfer_data);



        if (!is_array($payout_response)) {
            return $payout_response;
        } else {

            if ($payout_response["payout_status"] == "rejected" || $payout_response["payout_status"] == "failure" ||
                    $payout_response["payout_status"] == "reversed") {
                $this->db->set("transaction_status", "Failed");
            } else if ($payout_response["payout_status"] == "success") {
                $this->db->set("transaction_status", "Completed");
            }

            $this->db->set("remark", $payout_response["failure_reason"]);
            $this->db->set("payment_gateway_ref_id", $payout_response["payment_gateway_ref_id"]);
            $this->db->set("payout_status", $payout_response["payout_status"]);
            $this->db->set("utr_ref_no", $payout_response["utr_ref_no"]);
            $this->db->set("response_date_time", $payout_response["transfer_date"]);
            $this->db->set("res_data", $payout_response["res_data"]);
            $this->db->where("id", $vendor_settlement_transaction_history_id);
            $this->db->update("vendor_settlement_transaction_history");
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return [
                "response" => "negative_not_submitted",
                "message" => "Some error Occured! Please try again or check bank details"
            ];
        } else {
            $this->db->trans_commit();
            return $this->return_payout_responses($vendor_settlement_transaction_history_id);
        }
    }

    function return_payout_responses($vendor_settlement_transaction_history_id) {
        $this->db->where("id", $vendor_settlement_transaction_history_id);
        $row = $this->db->get("vendor_settlement_transaction_history")->row();

        $payout_ref_id = $row->payout_ref_id;

        if ($row->payout_status == "success") {
            $this->update_wallet_history_for_payout_completed($vendor_settlement_transaction_history_id);
            return [
                "response" => "success",
                "payout_ref_id" => $payout_ref_id,
                "message" => "Money is successfully credited to the beneficiary account"
            ];
        } else if ($row->payout_status == "accepted") {
            return [
                "response" => "positive",
                "payout_ref_id" => $payout_ref_id,
                "message" => "Transfer Request is sent to the bank"
            ];
        } else if ($row->payout_status == "in_process") {
            return [
                "response" => "in_middle",
                "payout_ref_id" => $payout_ref_id,
                "message" => "Bank is processing your transfer request"
            ];
        } else if ($row->payout_status == "failure") {
            return [
                "response" => "negative",
                "payout_ref_id" => $payout_ref_id,
                "message" => "Transfer request could not be processed due to any reason"
            ];
        } else if ($row->payout_status == "pending") {
            return [
                "response" => "in_middle",
                "payout_ref_id" => $payout_ref_id,
                "message" => "Transfer request is accepted by easebuzz but yet not sent to the bank."
            ];
        } else if ($row->payout_status == "on_hold") {
            return [
                "response" => "in_middle",
                "payout_ref_id" => $payout_ref_id,
                "message" => "Transfer request is put on hold due to any reason. Manual intervention required"
            ];
        } else if ($row->payout_status == "rejected") {
            return [
                "response" => "negative",
                "payout_ref_id" => $payout_ref_id,
                "message" => "Transfer request is rejected by the beneficiary bank. Transaction failed"
            ];
        } else if ($row->payout_status == "reversed") {
            return [
                "response" => "negative",
                "payout_ref_id" => $payout_ref_id,
                "message" => "Transfer request was successful but the beneficiary bank made a reverse entry to your account."
            ];
        }
    }

    function generate_payout_ref_id() {
        $ref_id = "payv" . generateRandomNumber(10);
        if ($this->db->get_where("vendor_settlement_transaction_history", ["payout_ref_id" => $ref_id])->num_rows() == 0) {
            return $ref_id;
        } else {
            return $this->generate_payout_ref_id();
        }
    }

    function check_and_update_status($payment_gateway_ref_id) {
        $payout_response = $this->easebuzz_payout_model->get_wire_transaction_status($payment_gateway_ref_id);

        if (!is_array($payout_response)) {
            return $payout_response;
        }

        if ($payout_response["payout_status"] == "rejected" || $payout_response["payout_status"] == "failure" ||
                $payout_response["payout_status"] == "reversed") {
            $this->db->set("transaction_status", "Failed");
        } else if ($payout_response["payout_status"] == "success") {
            $this->db->set("transaction_status", "Completed");
        }

        $this->db->set("remark", $payout_response["failure_reason"]);
        $this->db->set("payment_gateway_ref_id", $payout_response["payment_gateway_ref_id"]);
        $this->db->set("payout_status", $payout_response["payout_status"]);
        $this->db->set("utr_ref_no", $payout_response["utr_ref_no"]);
        $this->db->set("response_date_time", $payout_response["transfer_date"]);
        $this->db->set("res_data", $payout_response["res_data"]);
        $this->db->where("payment_gateway_ref_id", $payment_gateway_ref_id);
        $this->db->update("vendor_settlement_transaction_history");

        $this->db->select("id");
        $this->db->where("payment_gateway_ref_id", $payment_gateway_ref_id);
        $vendor_settlement_transaction_history_id = $this->db->get("vendor_settlement_transaction_history")->row()->id;

        return $this->return_payout_responses($vendor_settlement_transaction_history_id);
    }

}
