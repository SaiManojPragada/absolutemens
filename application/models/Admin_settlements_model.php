<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin_settlements_model extends CI_Model {

    function add_to_vendor_wallet($order_id) {
        $this->db->trans_begin();
        $order_token = $this->db->get_where('orders', ['id' => $order_id])->row()->order_id;
        $order_products = $this->get_order_products($order_id);
        foreach ($order_products as $op) {
            $amount = $op->subtotal;
            $wallet_balance = $this->get_wallet_balance($op->vendor_id);
            $updated_wallet_balance = (int) $wallet_balance + (int) $amount;
            $wallet_array = [
                "vendor_id" => $op->vendor_id,
                "vendor_settlement_id" => null,
                "action" => "credit",
                "credit_type" => "from order delivery",
                "amount" => $amount,
                "title" => "Amount credited",
                "comment" => "$amount amount credited in wallet for completing the order with ref id - $order_token ",
                "balance" => $updated_wallet_balance,
                "date" => date('Y-m-d'),
                "created_at" => time()
            ];
            $this->db->insert('vendor_wallet', $wallet_array);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get_order_products($order_id) {
        $this->db->select('id,subtotal,vendor_id');
        $this->db->from('order_products');
        $this->db->where('order_id', $order_id);
        $this->db->where('status', 1);
        $this->db->where('approved_by_fashion_manager', 'yes');
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            return $result;
        }
        return [];
    }

    function get_wallet_balance($vendor_id) {
        $balance = 0;
        $this->db->select('balance');
        $this->db->where('vendor_id', $vendor_id);
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $this->db->from('vendor_wallet');
        $balance = $this->db->get()->row()->balance;
        return $balance;
    }

}
