<?php

class Vendor_categories_model extends CI_Model {

    private $table_name = "categories";

    function get($restaurants_id, $search_key = null) {
        $this->db->where('status', 1);
        if ($search_key) {
            $this->db->like("name", $search_key, "both");
        }
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            return $data;
        } else {
            return [];
        }
    }

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        $id = $this->db->insert_id();
        return $id;
    }

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $this->db->update($this->table_name);

        $cache_key = "category_name" . $id;
        $this->cache_model->delete_cache_if_exists($cache_key);

        return $id;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        $this->vendor_food_item_timings_model->delete_timings_for_category_of_food_items($id);

        $cache_key = "category_name" . $id;
        $this->cache_model->delete_cache_if_exists($cache_key);

        return $response;
    }

    function get_restaurant_category_name($restaurant_categories_id) {
        $cache_key = "category_name" . $restaurant_categories_id;
        $cache = $this->cache_model->get_cache_if_exists($cache_key);
        if ($cache) {
            return $cache;
        }

        $column_name = "name";
        $this->db->select($column_name);
        $this->db->where("id", $restaurant_categories_id);
        $data = $this->db->get($this->table_name)->row()->{$column_name};

        $this->cache_model->save_to_cache($cache_key, $data);

        return $data;
    }

}
