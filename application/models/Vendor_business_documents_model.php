<?php 
class Vendor_business_documents_model extends CI_Model{
	
	private $table_name = "restaurant_documents";
	
	function get($restaurants_id){
		$this->db->where("restaurants_id", $restaurants_id);
		$data = $this->db->get($this->table_name)->result();
		if($data){
			foreach($data as $item){
				$item->file = FILE_UPLOAD_FOLDER_IMG_PATH.$item->file;
			}
			return $data;
		}else{
			return [];
		}
	}
	
}