<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Vendor_login_model extends CI_Model {

    function create_login_session($data) {
        $this->session->set_userdata("vendor_id", $data->id);
        $this->session->set_userdata("vendor_logged", true);
        $this->session->unset_userdata("admin_control");

        $address = $this->get_client_ip();
        $insert_data = array("ip_address" => $address,
            "vendor_id" => $data->id, "created_at" => time());
        $this->db->insert("vendor_login_logs", $insert_data);
        $vendor_details = $this->vendor_model->get_vendor_details($data->id);
        unset($vendor_details->id);
        return $vendor_details;
    }

    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
