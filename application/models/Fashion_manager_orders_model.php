<?php

class Fashion_manager_orders_model extends CI_Model {

    function get_to_be_approved_orders() {
        $this->db->select('id,subscription_transaction_id,created_at,final_total,fashion_manager_id,fashion_analyst_id,order_status,is_exchanged');
        $this->db->from('orders');
        $this->db->where('order_status', 'suggested_by_fashion_analyst');
        $this->db->where('status', 1);
        $this->db->where('fashion_manager_id', $this->session->userdata('fashion_id'));
        $result = $this->db->get()->result();

        if ($result[0]->id != "") {

            foreach ($result as $item) {
                $check_is_return_order = $this->u_model->get_data_conditon_row("orders", array("subscription_transaction_id" => $item->subscription_transaction_id,
                    "is_exchanged" => 1));
                if ($check_is_return_order) {
                    $item->is_exchanged = 1;
                }
                $item->fashion_analyst_details = $this->get_fashion_analyst_details($item->fashion_analyst_id);
                $item->unique_order_id = $this->db->get_where('subscriptions_transactions', ['id' => $item->subscription_transaction_id])->row()->unq_id;
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
            }
            return $result;
        }
        return [];
    }

    function forwarded_to_admin_orders() {
        $this->db->select('id,subscription_transaction_id,created_at,final_total,fashion_manager_id,fashion_analyst_id,order_status');
        $this->db->from('orders');
        $this->db->where('order_status !=', 'suggested_by_fashion_analyst');
        $this->db->where('order_status !=', 'rejected_by_fashion_manager');
        $this->db->where('order_status !=', '');
        $this->db->where('status', 1);
        $this->db->where('fashion_manager_id', $this->session->userdata('fashion_id'));
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $item->fashion_analyst_details = $this->get_fashion_analyst_details($item->fashion_analyst_id);
                $item->unique_order_id = $this->db->get_where('subscriptions_transactions', ['id' => $item->subscription_transaction_id])->row()->unq_id;
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
                $item->final_total = $this->get_final_total_from_selected_products($item->id);
                if ($item->order_status == 'order_placed_by_admin') {
                    $item->order_status = "order_processed_by_admin";
                }
            }
            return $result;
        }
        return [];
    }

    function get_final_total_from_selected_products($order_id) {
        $total_amount = 0;
        $this->db->select('id,subtotal');
        $this->db->from('order_products');
        $this->db->where('order_id', $order_id);
        $this->db->where('approved_by_fashion_manager', 'yes');
        $this->db->where('status', 1);
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $total_amount += $item->subtotal;
            }
        }
        return $total_amount;
    }

    function get_fashion_analyst_details($user_id) {
        $this->db->select('name,phone_number');
        $this->db->from('designers_and_analysts');
        $this->db->where('id', $user_id);
        $row = $this->db->get()->row();
        return $row;
    }

    function get_order_details($order_id) {
        $this->db->select('id,subscription_transaction_id,created_at,final_total,fashion_manager_id,fashion_analyst_id,order_status');
        $this->db->from('orders');
        $this->db->where('status', 1);
        $this->db->where('id', $order_id);
        $row = $this->db->get()->row();
        if ($row->id != "") {
            $row->fashion_analyst_details = $this->get_fashion_analyst_details($row->fashion_analyst_id);
            $row->unique_order_id = $this->db->get_where('subscriptions_transactions', ['id' => $row->subscription_transaction_id])->row()->unq_id;
            $row->created_at = date('d-m-Y H:i:s a', $row->created_at);
            $row->order_products = $this->get_orders_products($row->id);
            $subscription_details = $this->db->get_where('subscriptions_transactions', ['id' => $row->subscription_transaction_id])->row();
            $row->unique_order_id = $subscription_details->unq_id;
            $row->package_title = strip_tags($subscription_details->package_title);
            $row->no_of_items = $subscription_details->no_of_items;
            return $row;
        }
        return new stdClass();
    }

    function get_orders_products($order_id, $type = null) {
        $this->db->select('id,vendor_id,product_id,product_inventory_id,quantity,unit_price,subtotal,comment,created_at,approved_by_fashion_manager,created_at');
        $this->db->from('order_products');
        $this->db->where('order_id', $order_id);
        $this->db->where('status', 1);
        if ($type == "selected_by_fashion_manager") {
            $this->db->where('approved_by_fashion_manager', 'yes');
        }
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $item->quantity = (int) $item->quantity;
                $item->product_details = $this->db->select('title,product_id')->get_where('products', ['id' => $item->product_id])->row();
                $item->image = base_url() . $this->get_product_image($item->product_id);
                $product_inventory_row = $this->db->select('size_id')->get_where('product_inventory', ['id' => $item->product_inventory_id])->row();
                $size_data = $this->db->select('category_id,title')->get_where('sizes', ['id' => $product_inventory_row->size_id])->row();
                $item->size = $size_data->title;
                $item->category_id = $product_inventory_row->category_id;
                $item->category_name = $this->db->get_where('categories', ['id' => $size_data->category_id])->row()->name;
                $item->vendor_details = $this->get_vendor_details($item->vendor_id);
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
                if ($item->approved_by_fashion_manager == "yes") {
                    $item->approved_by_fashion_manager = true;
                } else {
                    $item->approved_by_fashion_manager = false;
                }
            }
            return $result;
        }
        return [];
    }

    function get_product_image($product_id) {
        return $this->db->get_where('products_images', ['product_id' => $product_id])->row()->image;
    }

    function get_vendor_details($vendor_id) {
        $this->db->select('business_name,owner_name,contact_email,contact_number');
        $this->db->from('vendors');
        $this->db->where('id', $vendor_id);
        $row = $this->db->get()->row();
        return $row;
    }

    function check_subscription_applicable($subscription_transaction_id, $order_id) {
        $no_of_items = $this->db->get_where('subscriptions_transactions', ['id' => $subscription_transaction_id])->row()->no_of_items;
        $current_selected_items = count($this->get_orders_products($order_id, 'selected_by_fashion_manager'));

        if ($no_of_items > $current_selected_items) {
            return "success";
        } else {
            return "error";
        }
    }

    function update_fashion_manager_selected_status($order_product_id) {
        $status = $this->db->get_where('order_products', ['id' => $order_product_id])->row()->approved_by_fashion_manager;
        if ($status == "yes") {
            $ustatus = "no";
        } else if ($status == "no") {
            $ustatus = "yes";
        }
        $this->db->set('approved_by_fashion_manager', $ustatus);
        $this->db->where('id', $order_product_id);
        $update = $this->db->update('order_products');
        if ($update) {
            return true;
        } else {
            return false;
        }
    }

    function accept_and_forward_to_admin($order_id) {
        $this->db->trans_begin();
        $current_selected_items = count($this->get_orders_products($order_id, 'selected_by_fashion_manager'));
        if ($current_selected_items == 0) {
            return "not_selected";
        } else {
            $this->db->set('order_status', 'approved_by_fashion_manager');
            $this->db->where('id', $order_id);
            $update = $this->db->update('orders');
            if ($update) {
                //add to order logs
                $order_row = $this->db->get_where('orders', ['id' => $order_id])->row();
                $fashion_id = $order_row->fashion_analyst_id;
                $fashion_manager_id = $order_row->fashion_manager_id;
                $fashion_analyst_name = $this->get_user_details($fashion_id)->name;
                $fashion_manager = $this->get_user_details($fashion_manager_id)->name;
                $fashion_analyst_mobile = $this->get_user_details($fashion_id)->phone_number;
                $fashion_manager_mobile = $this->get_user_details($fashion_manager_id)->phone_number;
                $order_log_array = [
                    "order_real_id" => $order_row->subscription_transaction_id,
                    "order_id" => $order_row->order_id,
                    "order_status" => "approved_by_fashion_manager",
                    "log" => "Fashion manager-$fashion_manager ($fashion_manager_mobile) has accepted the suggestions placed by  the Fashion analyst - $fashion_analyst_name ($fashion_analyst_mobile) and forwarded to Administrator for further processing",
                    "action_date_time" => date('Y-m-d H:i:s'),
                    "created_at" => time(),
                    "action_user_id" => $fashion_manager_id,
                    "table_name" => 'designers_and_analysts',
                    "from_table" => 'order_products'
                ];
                $this->db->insert('orders_log', $order_log_array);

                //update status in transactions
                $this->db->set('order_status', 'Selections Confirmed');
                $this->db->where('id', $order_row->subscription_transaction_id);
                $this->db->update('subscriptions_transactions');
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get_user_details($user_id) {
        $row = $this->db->select('name,phone_number')->get_where('designers_and_analysts', ['id' => $user_id])->row();
        return $row;
    }

    function get_order_logs($unique_order_id) {
        $subscription_transaction_id = $this->db->get_where('subscriptions_transactions', ['unq_id' => $unique_order_id])->row()->id;
        $this->db->select('id,order_id,log,order_status,created_at');
        $this->db->from('orders_log');
        $this->db->where('order_real_id', $subscription_transaction_id);
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
            }
            return $result;
        }
        return [];
    }

    function reject_order($order_id, $reject_reason, $is_exchanged = 0, $order_status = "rejected_by_fashion_manager") {
        $this->db->trans_begin();
        $this->db->set('order_status', $order_status);
        $this->db->set('reject_reason', $reject_reason);
        $this->db->set('is_exchanged', $is_exchanged);
        $this->db->where('id', $order_id);
        $update = $this->db->update('orders');
        if ($update) {
            //add to order logs
            $order_row = $this->db->get_where('orders', ['id' => $order_id])->row();
            $fashion_id = $order_row->fashion_analyst_id;
            $fashion_manager_id = $order_row->fashion_manager_id;
            $fashion_analyst_name = $this->get_user_details($fashion_id)->name;
            $fashion_manager = $this->get_user_details($fashion_manager_id)->name;
            $fashion_analyst_mobile = $this->get_user_details($fashion_id)->phone_number;
            $fashion_manager_mobile = $this->get_user_details($fashion_manager_id)->phone_number;
            $order_log_array = [
                "order_real_id" => $order_row->subscription_transaction_id,
                "order_id" => $order_row->order_id,
                "order_status" => "rejected_by_fashion_manager",
                "log" => "Fashion manager-$fashion_manager ($fashion_manager_mobile) has rejected the suggestions placed by  the Fashion analyst - $fashion_analyst_name ($fashion_analyst_mobile) and the order has been reverted back to Fashion analyst - $fashion_analyst_name ($fashion_analyst_mobile)",
                "action_date_time" => date('Y-m-d H:i:s'),
                "created_at" => time(),
                "action_user_id" => $fashion_manager_id,
                "table_name" => 'designers_and_analysts',
                "from_table" => 'order_products'
            ];
            if (!empty($order_status)) {
                $this->db->insert('orders_log', $order_log_array);
            }
            $log_id = $this->db->insert_id();
            if ($log_id) {
                //revert stock
                $order_products = $this->get_orders_products($order_id);
                foreach ($order_products as $op) {
                    $stock_post_arr = array(
                        "product_id" => $op->product_id,
                        "product_inventory_id" => $op->product_inventory_id,
                        "quantity" => $op->quantity,
                        "type" => 'credit',
                        "action_for" => "credit_for_order_rejection_by_fashion_manager"
                    );
                    $this->stock_management_model->add_stock($stock_post_arr);
                }
            }
        } else {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get_fashion_analysts() {
        $this->db->select('id,name,phone_number');
        $this->db->from('designers_and_analysts');
        $this->db->where('designation', 'Fashion Analyst');
        $this->db->where('status', 1);
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            return $result;
        }
        return [];
    }

    function reassign_fashion_analyst($subscription_transaction_id, $fashion_analyst_id) {
        $this->db->trans_begin();

        $this->clear_previous_fashion_analyst_suggestions($subscription_transaction_id);

        $this->db->set('fashion_analyst_id', $fashion_analyst_id);
        $this->db->where('id', $subscription_transaction_id);
        $update = $this->db->update('subscriptions_transactions');
        if ($update) {
            //insert into order row
            $subscription_row = $this->db->get_where('subscriptions_transactions', ['id' => $subscription_transaction_id])->row();
            $fashion_manager_id = $this->session->userdata('fashion_id');
            $fashion_analyst_name = $this->get_user_details($fashion_analyst_id)->name;
            $fashion_manager = $this->get_user_details($fashion_manager_id)->name;
            $fashion_analyst_mobile = $this->get_user_details($fashion_analyst_id)->phone_number;
            $fashion_manager_mobile = $this->get_user_details($fashion_manager_id)->phone_number;
            $order_log_array = [
                "order_real_id" => $subscription_transaction_id,
                "order_id" => $subscription_row->unq_id,
                "order_status" => "reassigned_to_fashion_analyst",
                "log" => "Fashion manager-$fashion_manager ($fashion_manager_mobile) has reassigned this order to Fashion analyst - $fashion_analyst_name ($fashion_analyst_mobile)",
                "action_date_time" => date('Y-m-d H:i:s'),
                "created_at" => time(),
                "action_user_id" => $fashion_manager_id,
                "table_name" => 'designers_and_analysts',
                "from_table" => 'subscription_transactions'
            ];
            $this->db->insert('orders_log', $order_log_array);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function clear_previous_fashion_analyst_suggestions($subscription_transaction_id) {

        //update cart status to inactive
        $this->db->set('cart_status', 'in_active');
        $this->db->where('subscription_transaction_id', $subscription_transaction_id);
        $update_cart = $this->db->update('fashion_designer_cart');
        if ($update_cart) {
            $this->db->set('status', 0);
            $this->db->where('subscription_transaction_id', $subscription_transaction_id);
            $update_order = $this->db->update('orders');
            if ($update_order) {
                $order_id = $this->db->get_where('orders', ['subscription_transaction_id' => $subscription_transaction_id])->row()->id;
                $this->update_order_products_status($order_id);
            }
        }
    }

    function update_order_products_status($order_id) {
        $this->db->select('id,product_id,product_inventory_id,quantity');
        $this->db->from('order_products');
        $this->db->where('order_id', $order_id);
        $this->db->where('status', 1);
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $this->db->set('status', 0);
                $this->db->where('id', $item->id);
                $update = $this->db->update('order_products');
                if ($update) {
                    //revert stock
                    $stock_post_arr = array(
                        "product_id" => $item->product_id,
                        "product_inventory_id" => $item->product_inventory_id,
                        "quantity" => $item->quantity,
                        "type" => 'credit',
                        "action_for" => "credit_for_reassigning_to_another_fashion_analyst"
                    );
                    $this->stock_management_model->add_stock($stock_post_arr);
                }
            }
        }
    }

}
