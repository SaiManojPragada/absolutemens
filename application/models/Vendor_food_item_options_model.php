<?php

class Vendor_food_item_options_model extends CI_Model {

    private $table_name = "restaurant_food_item_options";

    function get($restaurant_food_items_id, $availability_status = null) {
        //$this->db->order_by("priority");
        $this->db->where('status', 1);
        $this->db->where("restaurant_food_items_id", $restaurant_food_items_id);
        if (isset($availability_status)) {
            $this->db->where("available", $availability_status);
        }

        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                $item->offer_percentage = (float) $item->offer_percentage;
                $item->actual_price = (float) $item->actual_price;
                $item->applicable_price = $item->actual_price;
                if ($item->offer_percentage > 0) {
                    $item->applicable_price = $item->actual_price - ($item->offer_percentage / 100) * $item->actual_price;
                }

                if ($item->available) {
                    $item->available = true;
                } else {
                    $item->available = false;
                }
            }
            return $data;
        } else {
            return [];
        }
    }

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);

        $this->db->set("verification_status", "Pending");
        $this->db->where("id", $data["restaurant_food_items_id"]);
        $this->db->update("restaurant_food_items");

        return $response;
    }

    function update($data, $id) {

        $this->db->trans_begin();


        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->update($this->table_name);


        if (!$this->session->userdata("admin_control")) {
            if ($this->db->affected_rows() == 1) {
                $this->db->set("verification_status", "Pending");
                $this->db->where("id", $data["restaurant_food_items_id"]);
                $this->db->update("restaurant_food_items");
            }
        }
        $this->db->set("updated_at", time());
        $this->db->where("id", $id);
        $this->db->update($this->table_name);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
        }
        return true;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        return $response;
    }

    function update_food_option_sold_out_or_available($vendor_id, $restaurant_food_option_id, $available) {
        if ($available == "false" || $available == false) {
            $available = 0;
        } else {
            $available = 1;
        }
        $this->db->set("available", $available);
        $this->db->where("id", $restaurant_food_option_id);
        $this->db->update("restaurant_food_item_options");

        $this->cache_model->clear_restaurant_cache($vendor_id);

        return true;
    }

}
