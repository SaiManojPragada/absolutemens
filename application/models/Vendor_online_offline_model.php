<?php

class Vendor_online_offline_model extends CI_Model {

    function get_status($vendor_id) {

        /*  $is_restaurant_opened = $this->restaurants_model->check_is_restaurant_opened($vendor_id);
          if ($is_restaurant_opened == "Closed") {
          return false;
          } */

        $this->db->select("is_online");
        $this->db->where("id", $vendor_id);
        $response = $this->db->get("vendors")->row()->is_online;
        return $response == 1 ? true : false;
    }

}
