<?php

class Vendor_food_item_images_model extends CI_Model {

    private $table_name = "restaurant_food_item_images";

    function get($restaurant_food_items_id) {

        $this->db->where("restaurant_food_items_id", $restaurant_food_items_id);
        $this->db->where('status', 1);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                if ($item->image) {
                    $item->image = PRODUCT_ITEMS_IMG_PATH . $item->restaurants_id . "/" . $item->image;
                } else {
                    unset($item);
                }
                //$item->image = PRODUCT_DEFAULT_IMAGE;
            }
            return $data;
        } else {
            return [];
        }
    }

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        $id = $this->db->insert_id();
        return $id;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        return $response;
    }

}
