<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Vendor_withdraw_request_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = "vendor_withdraw_requests";
    }

    function request($data) {
        $this->db->trans_begin();
        $wallet_balance = $this->get_wallet_balance($data['vendor_id']);
        $minimum_withdraw_balance = $this->get_minimum_withdraw_amount();
        $maximum_withdraw_amount = $this->get_maximum_withdraw_amount();
        $previous_onhold_amount = $this->get_wallet_details($data['vendor_id'])['onhold_wallet_amount'];
        if ($wallet_balance >= $previous_onhold_amount) {
            $updated_wallet_balance = (int) $wallet_balance - (int) $previous_onhold_amount;
        } else if ($previous_onhold_amount >= $wallet_balance) {
            $updated_wallet_balance = (int) $previous_onhold_amount - (int) $wallet_balance;
        }
        if ($updated_wallet_balance >= $data['amount']) {
            if ($data['amount'] >= $minimum_withdraw_balance) {
                if ($data['amount'] <= $maximum_withdraw_amount) {
                    if ($wallet_balance >= $data['amount']) {
                        $is_bank_details_added = $this->check_bank_details_added($data['vendor_id']);
                        if ($is_bank_details_added) {
                            $this->db->where("vendors_id", $data['vendor_id']);
                            $bank_details = $this->db->get("vendor_bank_details")->row();
                            if (!$bank_details->status) {
                                return "bank_details_not_approved";
                                die;
                            }
                            //$is_already_submitted = $this->check_withdraw_already_submitted($data['vendor_id']);
                            // if (!$is_already_submitted) {
                            $increment = $this->get_increment();
                            $data['increment'] = $increment + 1;
                            $data['request_id'] = "WD" . str_pad($increment + 1, 5, '0', STR_PAD_LEFT);
                            $data['comment'] = "Withdraw amount of " . $data['amount'] . " on " . date('d-m-y H:i:s');
                            $data['withdraw_status'] = "submitted";
                            $data['date_time'] = date('Y-m-d H:i:s');
                            $data['created_at'] = time();
                            $this->db->insert('vendor_withdraw_requests', $data);
                            $withdraw_id = $this->db->insert_id();
                            if ($withdraw_id > 0) {
                                $previous_onhold_amount = $this->get_wallet_details($data['vendor_id'])['onhold_wallet_amount'];
                                $updated_on_hold_amount = (int) $previous_onhold_amount + (int) $data['amount'];
                                $this->db->set('onhold_wallet_amount', $updated_on_hold_amount);
                                $this->db->where('id', $data['vendor_id']);
                                $this->db->update('vendors');
                            }
                            //} else {
                            // return "already_submitted";
                            // }
                        } else {
                            return "bank_details_not_updated";
                        }
                    } else {
                        return "less_wallet_amount";
                    }
                } else {
                    return "maximum_withdraw_exceeded";
                }
            } else {
                return "minimum_withdraw_balance";
            }
        } else {
            return "less_wallet_amount";
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "error";
        } else {
            $this->db->trans_commit();
            return "success";
        }
    }

    function get_increment() {
        $this->db->select_max('id');
        $this->db->select('increment');
        $this->db->from('vendor_withdraw_requests');
        $increment = $this->db->get()->row()->id;
        if ($increment > 0) {
            return $increment;
        } else {
            return 0;
        }
    }

    function get_wallet_balance($vendor_id) {
        $this->db->select('balance');
        $this->db->from('vendor_wallet');
        $this->db->where('vendor_id', $vendor_id);
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $balance = $this->db->get()->row()->balance;
        return $balance;
    }

    function get_wallet_details($vendor_id) {
        $data['onhold_wallet_amount'] = $this->db->get_where('vendors', ['id' => $vendor_id])->row()->onhold_wallet_amount;
        $data['current_wallet_balance'] = $this->get_wallet_balance($vendor_id);
        return $data;
    }

    function get_minimum_withdraw_amount() {
        return $this->db->get_where('withdraw_settings', ['id' => 1])->row()->minimum_withdraw_amount;
    }

    function get_maximum_withdraw_amount() {
        return $this->db->get_where('withdraw_settings', ['id' => 1])->row()->maximum_withdraw_amount;
    }

    function check_bank_details_added($vendor_id) {
        $this->db->select('id');
        $this->db->from('vendor_bank_details');
        $this->db->where('vendors_id', $vendor_id);
        $rows = $this->db->count_all_results();
        if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function check_withdraw_already_submitted($vendor_id) {
        $this->db->select('id');
        $this->db->from('vendor_withdraw_requests');
        $this->db->where('vendor_id', $vendor_id);
        $this->db->where('withdraw_status', 'submitted');
        $rows = $this->db->count_all_results();
        if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    function get($filters) {
        $vendor_id = $this->session->userdata('vendor_id');
        $this->db->select('id,request_id,amount,withdraw_status,reject_reason,created_at,comment,approve_comment');
        $this->db->from('vendor_withdraw_requests');
        $this->db->where('vendor_id', $vendor_id);
        $this->db->where('status', 1);
        if ($filters['withdraw_status'] != "") {
            $this->db->where('withdraw_status', $filters['withdraw_status']);
        }
        if ($filters['from_date'] != "" && $filters['to_date'] != "") {
            $from_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['from_date']));
            $to_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['to_date']));
            $this->db->where('date_time >=', $from_date);
            $this->db->where('date_time <=', $to_date);
        }
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
            }
            return $result;
        } else {
            return [];
        }
    }

    function get_count($filters) {
        $vendor_id = $this->session->userdata('vendor_id');
        $this->db->select('id,request_id,amount,withdraw_status,reject_reason,created_at,comment,approve_comment');
        $this->db->from('vendor_withdraw_requests');
        $this->db->where('vendor_id', $vendor_id);
        $this->db->where('status', 1);
        if ($filters['withdraw_status'] != "") {
            $this->db->where('withdraw_status', $filters['withdraw_status']);
        }
        if ($filters['from_date'] != "" && $filters['to_date'] != "") {
            $from_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['from_date']));
            $to_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['to_date']));
            $this->db->where('date_time >=', $from_date);
            $this->db->where('date_time <=', $to_date);
        }
        $this->db->order_by('id', 'DESC');
        $rows = $this->db->count_all_results();
        return $rows;
    }

    function get_wallet_list($filters) {
        $vendor_id = $this->session->userdata('vendor_id');
        $this->db->select('id,action,amount,credit_type,credit_type,comment,created_at');
        $this->db->from('vendor_wallet');
        $this->db->where('vendor_id', $vendor_id);
        if ($filters['action'] != "") {
            $this->db->where('action', $filters['action']);
        }
        if ($filters['from_date'] != "" && $filters['to_date'] != "") {
            $from_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['from_date']));
            $to_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['to_date']));
            $this->db->where('date >=', $from_date);
            $this->db->where('date <=', $to_date);
        }
        $this->db->where('status', 1);
        if (isset($filters['limit']) && isset($filters['start'])) {
            $this->db->limit($filters["limit"], $filters["start"]);
        }
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
            }
            return $result;
        } else {
            return [];
        }
    }

    function get_wallet_list_count($filters) {
        $vendor_id = $this->session->userdata('vendor_id');
        $this->db->select('id,action,amount,credit_type,credit_type,comment,created_at');
        $this->db->from('vendor_wallet');
        $this->db->where('vendor_id', $vendor_id);
        if ($filters['action'] != "") {
            $this->db->where('action', $filters['action']);
        }
        if ($filters['from_date'] != "" && $filters['to_date'] != "") {
            $from_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['from_date']));
            $to_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['to_date']));
            $this->db->where('date >=', $from_date);
            $this->db->where('date <=', $to_date);
        }
        $this->db->where('status', 1);
        $rows = $this->db->count_all_results();
        return $rows;
    }

}
