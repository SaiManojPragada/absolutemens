<?php

class Vendor_gallery_model extends CI_Model {

    private $table_name = "vendors_gallery";

    function get($vendors_id) {
        $this->db->where("vendors_id", $vendors_id);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                $item->image = base_url('uploads/stores/gallery/' . $vendors_id . '/') . $item->image;
            }
            return $data;
        } else {
            return [];
        }
    }

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        return $response;
    }

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->select("image");
        $image = $this->db->get($this->table_name)->row()->image;
        $this->db->where("id", $id);
        $response = $this->db->delete($this->table_name);
        $vendor_id = $this->session->userdata("vendor_id");
        unlink('uploads/stores/gallery/' . $vendor_id . '/' . $image);
        return $response;
    }

}
