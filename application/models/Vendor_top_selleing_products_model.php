<?php

class Vendor_top_selleing_products_model extends CI_Model {

    function get_top_selling_products_list($vendor_id, $filters = []) {
        $this->db->select("id");
        $this->db->where("service_status", "Completed");
        $this->db->where("restaurants_id", $vendor_id);
        if ($filters['from_date'] && ($filters['from_date'] != '')) {
            $this->db->where('service_date >=', $filters['from_date']);
        }
        if ($filters['to_date'] && ($filters['to_date'] != '')) {
            $this->db->where('service_date <=', $filters['to_date']);
        }

        $this->db->from("service_orders");
        $service_orders_query = $this->db->get_compiled_select();

        $this->db->select("restaurant_food_items_id, sum(sub_total_without_tax) as total_product_sale_amount, restaurant_food_items_name");
        $this->db->select("ROUND((sum(sub_total_without_tax)/(SELECT sum(sub_total) FROM service_orders WHERE service_status ='Completed' AND restaurants_id = '$vendor_id' AND id IN($service_orders_query) ))*100, 2) as perct");
        $this->db->select("from_unixtime(service_order_items.created_at, '%Y-%m-%d') as date");
        $this->db->select("(SELECT sum(sub_total) FROM service_orders WHERE service_status ='Completed'  AND restaurants_id = '$vendor_id' AND id IN($service_orders_query)) as day_sale_amount");
        $this->db->where("service_order_id IN ($service_orders_query)");
        $this->db->from("service_order_items");
        $this->db->join("restaurant_food_items", "restaurant_food_items.id=service_order_items.id");
        $this->db->group_by("restaurant_food_items_id");
        $this->db->order_by("total_product_sale_amount", "desc");
        $data = $this->db->get()->result();


        //echo $this->db->last_query();
        //die;
        return $data;

        /* $this->db->select("restaurant_food_items_id,restaurant_food_items.item_name, sum(sub_total_without_tax) as total_amount");
          $this->db->select("(SELECT sum(sub_total) FROM service_orders WHERE service_status ='Completed' and service_date = from_unixtime(service_order_items.created_at, '%Y-%m-%d') AND restaurants_id = '$vendor_id' ) as day_sale_amount");
          $this->db->select("count(service_order_items.id) as no_of_times_sold");
          $this->db->select("from_unixtime(service_order_items.created_at, '%Y-%m-%d') as date");

          //$this->db->select("(day_sale_amount-total_amount / day_sale_amount*100) as percentage_sale");

          $this->db->where("service_order_id IN ($service_orders_query)");
          $this->db->from("service_order_items");
          $this->db->join("restaurant_food_items", "restaurant_food_items.id=service_order_items.id");
          $this->db->order_by("total_amount", "desc");
          $this->db->group_by("restaurant_food_items_id");
          $services_items_query = $this->db->get();

          echo $this->db->last_query();
          die; */
        //echo $services_items_query;
    }

    function get_top_sale_day($filters = [], $vendor_id) {
        $this->db->select("service_date, sum(sub_total) as total_sale");
        if ($filters['from_date'] && ($filters['from_date'] != '')) {
            $this->db->where('service_date >=', $filters['from_date']);
        }
        if ($filters['to_date'] && ($filters['to_date'] != '')) {
            $this->db->where('service_date <=', $filters['to_date']);
        }
        $this->db->limit(1);
        $this->db->where("service_status", "Completed");
        $this->db->where("restaurants_id", $vendor_id);
        $this->db->group_by("service_date");
        $this->db->order_by("total_sale", "desc");
        $data = $this->db->get("service_orders")->row();



        if ($data) {
            $data->date = date("M d, Y", strtotime($data->service_date));
            $data->week_name = date("l", strtotime($data->service_date));
        } else {
            return false;
        }

        return $data;
    }

}
