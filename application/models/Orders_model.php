<?php

class Orders_model extends CI_Model {

    function get_table_name_by_ref_id($ref_id) {
        //this is only for orders
        if ($ref_id[0] . $ref_id[1] == "PD") {
            $table_name = "pickup_and_drop_orders";
        } else {
            $table_name = "service_orders";
        }
        return $table_name;
    }

    function get_order_details($ref_id, $delivery_persons_id = null) {
        $this->db->where("ref_id", $ref_id);
        $data = $this->db->get("service_orders")->row();


        foreach ($data as $key => $item) {
            if (!isset($item)) {
                $data->{$key} = "";
            }
        }

        $data->restaurant_details = $this->vendor_model->get_vendor_details($data->restaurants_id);
        $data->restaurant_id = $data->restaurant_details->id;
        $id = $data->id;
        unset($data->restaurant_details->id);
        unset($data->restaurant_details->ref_code);
        unset($data->restaurant_details->is_exclusive);
        unset($data->restaurant_details->owner_name);
        unset($data->restaurant_details->password);
        unset($data->restaurant_details->salt);
        unset($data->restaurant_details->countries_id);
        unset($data->restaurant_details->cities_id);
        unset($data->restaurant_details->locations_id);
        unset($data->restaurant_details->access_token);

        unset($data->restaurant_details->created_at);
        unset($data->restaurant_details->updated_at);
        unset($data->restaurant_details->status);
        unset($data->restaurant_details->is_online);
        unset($data->restaurant_details->created_at);
        unset($data->restaurant_details->updated_at);
        unset($data->restaurant_details->status);
        unset($data->restaurant_details->is_recommended);
        unset($data->restaurant_details->is_most_popular);
        unset($data->restaurant_details->cuisine_types_ids);

        unset($data->restaurant_details->service_charge_type);
        unset($data->restaurant_details->service_charge_value);
        unset($data->restaurant_details->packing_charges_type);
        unset($data->restaurant_details->packing_charges_value);

        $data->customer_details = $this->customers_model->get_customer_details($data->customers_id);
        if ($data->customer_details) {
            unset($data->customer_details->id);
            unset($data->customer_details->mobile_verified);
            unset($data->customer_details->email_verified);
            unset($data->customer_details->email_verification_key);
            unset($data->customer_details->access_token);
            unset($data->customer_details->forgot_password_verfication_code);
            unset($data->customer_details->password_link_expiry_time);
            unset($data->customer_details->referral_code);
            unset($data->customer_details->reffered_by_customers_id);
            unset($data->customer_details->created_at);
            unset($data->customer_details->updated_at);
            unset($data->customer_details->status);
            unset($data->customer_details->referral_notes_obj);
            unset($data->customer_details->last_login);
            unset($data->customer_details->promotion_wallet_amount);
        } else {
            unset($data->customer_details);
        }


        $data->cart_items = $this->get_items($data->id);
        $data->taxes = $this->get_taxes($data->id);
        $data->order_prescriptions = $this->get_service_order_prescriptions($data->id);
        $this->apply_css_classes($data);


        if ($data->delivered_at) {
            $data->delivered_at = date("D, M d, h:i A", strtotime($data->delivered_at));
        }

        $data->has_live_tracking_permission = 0;
        if ($data->service_status == "Pending" ||
                $data->service_status == "Preparing" ||
                $data->service_status == "On Going" ||
                $data->service_status == "Waiting For Pickup" ||
                $data->service_status == "Out For Delivery"
        ) {
            $data->has_live_tracking_permission = 1;
        }

        $this->db->select("live_tracking");
        $this->db->where("id", 1);
        if ($this->db->get("site_settings")->row()->live_tracking == 1) {
            $data->has_live_tracking_permission = 1;
        } else {
            $data->has_live_tracking_permission = 0;
        }

        if ($data->created_at) {
            $data->service_date = date("D, M d, h:i A", $data->created_at);
        }

        //$data->distance = round(distance($data->delivery_lat, $data->delivery_lng, $data->restaurant_details->latitude, $data->restaurant_details->longitude, "K"), 2) . " Kms";
        $data->distance = round($data->distance_to_customer_from_store, 2) . " Kms";

        $data->distance_to_customer_from_store .= "Kms";

        $data->distance_to_store = -1;
        /*
          if (isset($delivery_persons_id) &&
          ($data->service_status == "Waiting For Pickup" || $data->service_status == "Waiting For Delivery Person")
          ) {
          $this->db->select("latitude, longitude");
          $delivery_person_row = $this->db->where("id", $data->delivery_persons_id)->get("delivery_persons")->row();
          $data->distance_to_store = round(distance($delivery_person_row->latitude, $delivery_person_row->longitude, $data->restaurant_details->latitude, $data->restaurant_details->longitude, "K"), 2) . " Kms";
          } */

        $data->delivery_person_lat = "";
        $data->delivery_person_lng = "";

        if ($data->service_status == "Waiting For Pickup" || $data->service_status == "Waiting For Delivery Person") {
            if ($delivery_persons_id) {
                $this->db->select("latitude, longitude");
                $delivery_person_row = $this->db->where("id", $delivery_persons_id)->get("delivery_persons")->row();
            } else {
                $this->db->select("latitude, longitude");
                $delivery_person_row = $this->db->where("id", $data->delivery_persons_id)->get("delivery_persons")->row();
            }


            /* echo "Delivery Person Lat : " . $delivery_person_row->latitude;
              echo "\n";
              echo "Delivery Person Lat : " . $delivery_person_row->longitude;
              echo "\n";
              echo "Restaurant Person Lat : " . $data->restaurant_details->latitude;
              echo "\n";
              echo "Restaurant Person Lat : " . $data->restaurant_details->longitude;
              echo "\n";


              echo $delivery_person_row->latitude, $delivery_person_row->longitude, $data->restaurant_details->latitude, $data->restaurant_details->longitude;
              die;
             */

            $data->distance_to_store = round(distance($delivery_person_row->latitude, $delivery_person_row->longitude, $data->restaurant_details->latitude, $data->restaurant_details->longitude, "K", false), 2) . " Kms";
            $data->delivery_person_lat = $delivery_person_row->latitude;
            $data->delivery_person_lng = $delivery_person_row->longitude;
        }

        $data->live_distance_to_customer = round(distance($delivery_person_row->latitude, $delivery_person_row->longitude, $data->delivery_lat, $data->delivery_lng, "K", false), 2) . " Kms";

        $this->db->where("service_request_bookings_id", $id);
        $log = $this->db->get("service_request_bookings_log")->result();

        $log_array[0] = [
            "display_lable" => "Order Placed",
            "date_time" => "",
            "time" => "",
            "action_activated" => "no",
            "image" => base_url() . "admin_assets/images/new_order.png"
        ];
        $log_array[1] = [
            "display_lable" => "Accepted",
            "date_time" => "",
            "time" => "",
            "action_activated" => "no",
            "image" => base_url() . "admin_assets/images/order_accepted.png"
        ];
        $log_array[2] = [
            "display_lable" => "Assigned to Delivery Person",
            "date_time" => "",
            "time" => "",
            "action_activated" => "no",
            "image" => base_url() . "admin_assets/images/pickup_order.png"
        ];
        $log_array[3] = [
            "display_lable" => "Out for delivery",
            "date_time" => "",
            "time" => "",
            "action_activated" => "no",
            "image" => base_url() . "admin_assets/images/out_for_delivery.png"
        ];
        $log_array[4] = [
            "display_lable" => "Order delivered",
            "date_time" => "",
            "time" => "",
            "action_activated" => "no",
            "image" => base_url() . "admin_assets/images/order_delivered.png"
        ];
        foreach ($log as $litem) {
            if ($litem->action_type == "Booked") {
                $log_array[0]["date_time"] = date("d-m-Y h:i A", $litem->action_triggered_at);
                $log_array[0]["time"] = date("h:i A", $litem->action_triggered_at);
                $log_array[0]["action_activated"] = "yes";
            }
            if ($litem->action_type == "Accepted") {
                $log_array[1]["date_time"] = date("d-m-Y h:i A", $litem->action_triggered_at);
                $log_array[1]["time"] = date("h:i A", $litem->action_triggered_at);
                $log_array[1]["action_activated"] = "yes";
            }
            if ($litem->action_type == "Delivery Person Accepted") {
                $log_array[2]["date_time"] = date("d-m-Y h:i A", $litem->action_triggered_at);
                $log_array[2]["time"] = date("h:i A", $litem->action_triggered_at);
                $log_array[2]["action_activated"] = "yes";
            }
            if ($litem->action_type == "Delivery Person Picked Up") {
                $log_array[3]["date_time"] = date("d-m-Y h:i A", $litem->action_triggered_at);
                $log_array[3]["time"] = date("h:i A", $litem->action_triggered_at);
                $log_array[3]["action_activated"] = "yes";
            }
            if ($litem->action_type == "Delivered") {
                $log_array[4]["date_time"] = date("d-m-Y h:i A", $litem->action_triggered_at);
                $log_array[4]["time"] = date("h:i A", $litem->action_triggered_at);
                $log_array[4]["action_activated"] = "yes";
            }
        }

        $data->order_status_log = $log_array;

        $this->db->where("service_orders_id", $id);
        $data->otp = $this->db->select("otp")->get("service_order_otps")->row()->otp;

        $data->delivery_person_pic = $this->db->select("profile_photo")->where("id", $data->delivery_persons_id)->get("delivery_persons")->row()->profile_photo;

        if ($data->delivery_person_pic) {
            $data->delivery_person_pic = DELIVERY_PERSONS_PATH . $data->delivery_persons_id . "/" . $data->delivery_person_pic;
        } else {
            $data->delivery_person_pic = "";
        }


        $data->refund_text = "";
        if ($data->payment_status == "Refund" || $data->service_status == "Rejected") {
            if ($data->through_wallet_amount > 0 && $data->through_wallet_amount >= $data->grand_total) {
                //echo "Case 1";
                $data->refund_text = "Rs." . $data->through_wallet_amount . "/- has been refunded to wallet";
            } else if ($data->through_wallet_amount == 0) {
                $data->refund_text = "Rs." . $data->grand_total . "/- has been refunded to your bank";
                //echo "Case 2";
            } else {
                $data->refund_text = "Rs." . $data->through_wallet_amount . "/- has been refunded to wallet and \n"
                        . "Rs." . ($data->grand_total - $data->through_wallet_amount) . "/- has been refunded to your bank";
                //echo "Case 3";
            }

            if ($data->payment_status == "Refund") {
                $data->refund_text .= " on" . date("Y-m-d h:i A", strtotime($data->refund_date_time));
            }

            if ($data->service_status == "Rejected") {
                $data->refund_text = str_replace("has been refunded", "will be refund", $data->refund_text);
            }
        }

        $this->db->select("sum(applied_tax_amount) as tax_on_items");
        $this->db->where("service_order_id", $data->id);
        $this->db->where("status", 1);
        $data->applied_on_service_tax_products = $this->db->get("service_order_items")->row()->tax_on_items;

        $data->table_name = "service_orders";
        return $data;
    }

    function get_items($service_order_id) {
        $this->db->select("restaurant_food_items_id, restaurant_food_item_options_id, restaurant_food_item_addons_id");
        $this->db->select("applicable_price, qty, restaurant_food_items_name, restaurant_food_item_addons_names");
        $this->db->select("applied_tax_amount, tax_percentage");
        $this->db->select("addons_json");
        $this->db->select("add_ons_amount, total_amount");
        $this->db->select("add_ons_amount, admin_commission_amount, admin_commission_percentage");
        $this->db->where("service_order_id", $service_order_id);
        $this->db->where("status", 1);
        $data = $this->db->get("service_order_items")->result();
        if ($data) {
            foreach ($data as $item) {
                if (!isset($item->restaurant_food_item_options_id)) {
                    $item->restaurant_food_item_options_id = "";
                }
                if (!isset($item->restaurant_food_item_addons_id)) {
                    $item->restaurant_food_item_addons_id = "";
                }
                $this->db->select("restaurants_id, food_type, restaurant_categories_id, image");
                $this->db->where("id", $item->restaurant_food_items_id);
                $rfi = $this->db->get("restaurant_food_items")->row();

                $this->db->where("id", $rfi->restaurant_categories_id);
                $item->category_name = $this->db->get("restaurant_categories")->row()->name;

                $item->food_type = $rfi->food_type;


                if ($rfi->image) {
                    $item->image = PRODUCT_ITEMS_IMG_PATH . $rfi->restaurants_id . "/" . $rfi->image;
                } else {
                    $item->image = PRODUCT_DEFAULT_IMAGE;
                    //$item->image = PRODUCT_ITEMS_IMG_PATH . $this->db->get("default_images")->row()->food_item_default_image;
                }




                if ($item->addons_json) {
                    $item->addons_json = json_decode($item->addons_json);
                }

                if ($this->input->get_post("request_from") == "mobile" && count($item->addons_json) == 0) {
                    unset($item->addons_json);
                }

                $item->following_individual_item_tax = INDIVIDUAL_TAX;

                $item->applicable_price = (float) $item->applicable_price;
            }
            return $data;
        }
        return [];
    }

    function get_taxes($service_order_id) {
        $this->db->select("tax_name, tax_type, tax_value, applied_tax_amount");
        $this->db->where("service_orders_id", $service_order_id);
        $this->db->where("status", 1);
        $taxes = $this->db->get("service_orders_taxes")->result();
        if ($taxes) {
            foreach ($taxes as $item) {
                if ($item->tax_type == "Percentage") {
                    $item->display_tax_type = $item->tax_value . " %";
                } else if ($item->tax_type == "Flat") {
                    $item->display_tax_type = "Rs. " . $item->tax_value;
                }
            }
            return $taxes;
        } else {
            return [];
        }
    }

    function get_service_order_prescriptions($service_order_id) {
        $this->db->select("service_orders_id, attachment");
        $this->db->where("service_orders_id", $service_order_id);
        $images = $this->db->get("service_order_prescriptions")->result();
        if ($images) {
            foreach ($images as $item) {
                $item->attachment = FILE_UPLOAD_FOLDER_IMG_PATH . "temp_cart/" . $item->attachment;
            }
            return $images;
        } else {
            return [];
        }
    }

    function get_vendor_orders($filters = [], $restaurants_id = null) {

        $only_cnt = false;

        if ($filters['q']) {
            $q = $filters['q'];
            $this->db->where("(ref_id = '$q' OR customers_id IN (SELECT id FROM customers WHERE mobile = '$q'))");
        }

        if ($filters['type'] == "Pending") {
            $this->db->where("service_status", $filters['type']);
        } else if ($filters['type'] == "Preparing") {
            $this->db->where("(service_status = 'Waiting For Pickup' OR service_status = 'Waiting For Delivery Person')");
        } else if ($filters['type'] == "On Going") {
            $this->db->where("(service_status = 'Waiting For Pickup' OR service_status = 'Out For Delivery')");
        } else if ($filters['type'] == "On Delivery") {
            $this->db->where("service_status", $filters['type']);
        } else if ($filters['type'] == "Rejected") {
            $this->db->where("service_status", $filters['type']);
        } else if ($filters['type'] == "Out For Delivery") {
            $this->db->where("service_status", "Out For Delivery");
        } else if ($filters['type'] == "Completed") {
            $this->db->where("service_status", "Completed");
        } else if ($filters['type'] == "Unsuccessful") {
            $this->db->where("service_status", "Unsuccessful");
        } else if ($filters['type'] == "Cancelled") {
            $this->db->where("service_status", "Cancelled");
        } else if ($filters['type'] != "" && @$filters["used_implode"] == true) {
            $t = $filters['type'];
            $this->db->where("service_status IN ($t)");
        } else if ($filters['type'] != "") {
            $this->db->where("service_status", $filters['type']);
        }


        if ($filters['payment_status'] == "Pending") {
            $this->db->where("payment_status", "Pending");
        } else if ($filters['payment_status'] == "Paid") {
            $this->db->where("payment_status", "Paid");
        } else if ($filters['payment_status'] == "Cancelled") {
            $this->db->where("payment_status", "Cancelled");
        } else if ($filters['payment_status'] == "Refund") {
            $this->db->where("payment_status", "Refund");
        } else if ($filters['payment_status'] == "Not Received") {
            $this->db->where("payment_status", "Not Received");
        }

        if (@$filters['city_id'] != "") {
            $this->db->where("city", $filters['city_id']);
        }

        if (isset($filters["request_management_app"])) {
            if ($filters["request_management_app"] == "no") {
                $this->db->where("(payment_status = 'Paid')");
                //$this->db->where("((payment_mode = 'Pay Online' AND payment_status='Paid') OR (payment_mode = 'Pay On Delivery'))");
            }
        } else {
            $this->db->where("((payment_mode = 'Pay Online' AND payment_status='Paid') OR (payment_mode = 'Pay On Delivery'))");
        }


        if ($filters['settlement_status'] == "Pending") {
            $this->db->where("settlement_status", $filters['settlement_status']);
            $this->db->where("service_status", "Completed");
            $this->db->where("payment_status", "Paid");
        } else if ($filters['settlement_status'] == "Completed") {
            $this->db->where("settlement_status", $filters['settlement_status']);
            $this->db->where("service_status", "Completed");
            $this->db->where("payment_status", "Paid");
        }

        if (!isset($filters["start"]) && !isset($filters["limit"])) {
            $only_cnt = true;
        }

        if ($filters['from_date'] && ($filters['from_date'] != '')) {
            $this->db->where('service_date >=', $filters['from_date']);
        }
        if ($filters['to_date'] && ($filters['to_date'] != '')) {
            $this->db->where('service_date <=', $filters['to_date']);
        }

        if ($filters['q']) {
            $q = $filters['q'];
            $this->db->where("(ref_id = $q)");
        }

        $this->db->where("status", 1);
        if (isset($restaurants_id)) {
            $this->db->where("restaurants_id IN ($restaurants_id)");
        }
        $this->db->from("service_orders");


        if ($only_cnt == true) {
            $total_records_found = $this->db->count_all_results();
            return $total_records_found;
        }

        $this->db->order_by("id", "asc");
        $this->db->limit($filters["limit"], $filters["start"]);

        $result = $this->db->get();


        #log_message("error", print_r($this->db->last_query(), true));

        if ($result->num_rows() == 0) {
            return [];
        }
        $data = $result->result();

        foreach ($data as $item) {

            if (isset($filters["accessing_from_retail_chain_login"])) {
                $this->db->select("restaurant_name, ref_code");
                $this->db->where("id", $item->restaurants_id);
                $r_row = $this->db->get("restaurants")->row();
                $item->store_name = $r_row->restaurant_name;
                $item->store_code = $r_row->ref_code;
            }


            $item->customer_details = $this->customers_model->get_customer_details($item->customers_id);
            $item->cart_items = $this->get_items($item->id);
            $txt = [];
            foreach ($item->cart_items as $citem) {
                $txt[] = $citem->category_name . ", " . $citem->restaurant_food_items_name . " x " . $citem->qty;
            }
            //unset($item->cart_items);
            $item->cart_items_txt = implode(", ", $txt);

            $item->service_date = date("d-M-Y", strtotime($item->service_date));
            if ($item->settled_date) {
                $item->settled_date = date("d-M-Y", strtotime($item->service_date));
            }
            $this->apply_css_classes($item);


            foreach ($item as $key => $sub_item) {
                if (!isset($sub_item)) {
                    $item->{$key} = "";
                }
            }

            $item->process_log = $this->orders_activity_log_model->get_log($item->id);
        }
        return $data;
    }

    function get_customer_orders($filters = [], $customers_id) {


        $only_cnt = false;

        if ($filters['type'] == "Upcoming") {
            //$this->db->where("service_date>=", date("Y-m-d"));
            $this->db->where("(service_status = 'Pending' "
                    . "OR service_status = 'Waiting For Pickup' "
                    . "OR service_status = 'Out For Delivery' "
                    . "OR service_status = 'Waiting For Delivery Person' )");
        } else if ($filters['type'] == "Past") {
            //$this->db->where("service_date<=", date("Y-m-d"));
            $this->db->where("(service_status != 'Pending' "
                    . "AND service_status != 'Waiting For Pickup' "
                    . "AND service_status != 'Out For Delivery' "
                    . "AND service_status != 'Waiting For Delivery Person' )");
        }

        if (!isset($filters["start"]) && !isset($filters["limit"])) {
            $only_cnt = true;
        }

        $this->db->where("service_orders.status", 1);
        $this->db->where("customers_id", $customers_id);
        $this->db->from("service_orders");

        if ($filters["own_stores"]) {
            $this->db->join("restaurants", "restaurants.id = service_orders.restaurants_id AND restaurants.is_own_store = 1");
        }

        if ($only_cnt == true) {
            $total_records_found = $this->db->count_all_results();
            return $total_records_found;
        }

        $this->db->select("service_orders.id, ref_id, restaurants_id, service_date, payment_mode, sub_total, applied_tax_amount,
		applied_delivery_charge, grand_total, payment_status, service_status, service_orders.landmark, service_orders.door_no, service_orders.address, delivered_at,
		service_orders.created_at, is_provided_feedback, delivery_person_name, delivery_person_contact_number");
        //$this->db->select("id");
        $this->db->order_by("service_orders.id", "desc");
        $this->db->limit($filters["limit"], $filters["start"]);

        $result = $this->db->get();
        if ($result->num_rows() == 0) {
            return [];
        }
        $data = $result->result();
        foreach ($data as $item) {
            //$item->restaurant_details = $this->vendor_model->get_vendor_details($item->restaurants_id);
            $item->restaurant_details = $this->temp_cart_model->get_plain_restaurant_details($item->restaurants_id);
            $item->restaurant_name = $item->restaurant_details->restaurant_name;
            $item->delivery_address = $item->landmark . " " . $item->door_no . " " . $item->address;
            $item->display_image = $item->restaurant_details->display_image;
            $item->delivery_boy_image = $item->restaurant_details->display_image;

            if ($item->service_status == "Completed") {
                if ($item->is_provided_feedback) {
                    $item->is_provided_feedback = 1;
                } else {
                    $item->is_provided_feedback = 0;
                }
            } else {
                $item->is_provided_feedback = 1;
            }


            $item->cart_items = $this->get_items($item->id);
            $txt = [];
            foreach ($item->cart_items as $citem) {
                $txt[] = $citem->restaurant_food_items_name . " x " . $citem->qty;
            }
            unset($item->cart_items);
            $item->cart_items_txt = implode(", ", $txt);

            if ($item->delivered_at) {
                $item->delivered_at = date("D, M d, h:i A", strtotime($item->delivered_at));
            }


            if ($item->created_at) {
                $item->service_date = date("D, M d, h:i A", strtotime($item->created_at));
            }
            unset($item->created_at);
            unset($item->landmark);
            unset($item->door_no);
            unset($item->address);
            unset($item->restaurants_id);
            unset($item->restaurant_details);
            //unset($item->id);
            $item->table_name = "service_orders";
            $this->apply_css_classes($item);
        }
        return $data;
    }

    function update_status_as_accepted($data, $ref_id, $retail_chain_stores_id = null) {
        $this->db->set($data);
        $this->db->where("ref_id", $ref_id);
        $this->db->set("updated_at", time());
        $this->db->update("service_orders");

        $order_data = $this->orders_model->get_order_details($ref_id);
        $message = "Your order " . $order_data->ref_id . " request has been accepted by " . $order_data->restaurant_details->restaurant_name . " and its preparing now";
        send_message($message, $order_data->customer_details->mobile, 1407161883460003105);

        $notitifcationArr = [
            "from_table_primary_id" => $order_data->id,
            "from_table_name" => "service_orders",
            "comment" => "Order Accepted #$ref_id by you",
            "to_table_primary_key" => $order_data->restaurant_id,
            "to_table_name" => "restaurants"
        ];
        $this->notifications_model->create($notitifcationArr);


        //Send Notification to Customer
        $cdata = array(
            "to" => $order_data->customer_details->push_notification_key,
            "notification" => array(
                "title" => "Order confirmed ref Id :#" . $ref_id,
                "body" => "Your Order Has been confirmed :#" . $ref_id . "\nStore Name:" . $order_data->restaurant_details->restaurant_name,
                'image' => SITE_LOGO,
                'sound' => 'notification_sound',
                'vibrate' => 1
            ),
            "data" => [
                "title" => "Order confirmed ref Id :#" . $ref_id,
                "body" => "Your Order Has been confirmed :#" . $ref_id . "\nStore Name:" . $order_data->restaurant_details->restaurant_name,
                "order_id" => $ref_id
            ]
        );

        $this->send_push_notification_model->send($cdata);


        //send push notification to available delivery boys
        $this->send_notification_to_delivery_boys($ref_id);

        $this->orders_activity_log_model->create_log($order_data->id, "Accepted", $order_data->restaurants_id, "restaurants", $retail_chain_stores_id);
        return true;
    }

    function update_status_as_ready_for_pickup($data, $ref_id, $retail_chain_stores_id = null) {
        $this->db->set($data);
        $this->db->where("ref_id", $ref_id);
        $this->db->set("updated_at", time());
        $this->db->update("service_orders");

        $order_data = $this->orders_model->get_order_details($ref_id);
        $message = "Your order " . $order_data->ref_id . " request has been accepted by " . $order_data->restaurant_details->restaurant_name . " and its preparing now";
        send_message($message, $order_data->customer_details->mobile, 1407161883460003105);

        $notitifcationArr = [
            "from_table_primary_id" => $order_data->id,
            "from_table_name" => "service_orders",
            "comment" => "Order Accepted #$ref_id by you",
            "to_table_primary_key" => $order_data->restaurant_id,
            "to_table_name" => "restaurants"
        ];
        $this->notifications_model->create($notitifcationArr);


        //Send Notification to Customer
        $cdata = array(
            "to" => $order_data->customer_details->push_notification_key,
            "notification" => array(
                "title" => "Order confirmed ref Id :#" . $ref_id,
                "body" => "Your Order Has been confirmed :#" . $ref_id . "\nStore Name:" . $order_data->restaurant_details->restaurant_name,
                'image' => SITE_LOGO,
                'sound' => 'notification_sound',
                'vibrate' => 1
            ),
            "data" => [
                "title" => "Order confirmed ref Id :#" . $ref_id,
                "body" => "Your Order Has been confirmed :#" . $ref_id . "\nStore Name:" . $order_data->restaurant_details->restaurant_name,
                "order_id" => $ref_id
            ]
        );

        $this->send_push_notification_model->send($cdata);


        //send push notification to available delivery boys
        $this->send_notification_to_delivery_boys($ref_id);

        $this->orders_activity_log_model->create_log($order_data->id, "Accepted", $order_data->restaurants_id, "restaurants", $retail_chain_stores_id);
        return true;
    }

    function send_notification_to_delivery_boys($ref_id) {

        #log_message("error", "Notification to delivery persons calling");
        #log_message("error", print_r(debug_backtrace()[1]['function'], true));


        $already_has_delivery_task_query = "SELECT delivery_persons_id FROM service_orders WHERE "
                . "(service_status = 'Waiting For Pickup' OR service_status = 'Out For Delivery') AND status = 1 "
                . " GROUP by delivery_persons_id HAVING count(delivery_persons_id)>=" . MAX_MULTIPLE_ORDERS;
        $already_has_pick_up_task_query = "SELECT delivery_persons_id FROM pickup_and_drop_orders WHERE "
                . "(delivery_status = 'Waiting For Pickup' OR delivery_status = 'Out For Delivery' OR delivery_status = 'Picked' OR delivery_status = 'Accepted') "
                . " AND status =1 GROUP by delivery_persons_id HAVING count(delivery_persons_id)>=" . MAX_MULTIPLE_ORDERS;


        $order_data = $this->orders_model->get_order_details($ref_id);
        if ($order_data->service_status != "Waiting For Delivery Person") {
            return false;
        }

        $latitude = $order_data->restaurant_lat;
        $longitude = $order_data->restaurant_lng;


        //find is there any exlusive deliveyr persons for the store
        $this->db->select("delivery_persons_id");
        //$this->db->where("(FIND_IN_SET(173, restaurants_ids))");
        $this->db->where("(FIND_IN_SET($order_data->restaurants_id, restaurants_ids))");
        $exlusive_delivery_persons = $this->db->get("exclusive_delivery_persons")->result();
        if (count($exlusive_delivery_persons)) {
            $dpids = [];
            foreach ($exlusive_delivery_persons as $edp) {
                $dpids[] = $edp->delivery_persons_id;
            }
            $this->db->where("delivery_persons.id IN (" . implode(",", $dpids) . ")");
        }

        $this->db->select("delivery_persons.*, 
			( 3959 * acos (  cos ( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($longitude) ) + sin ( radians($latitude) )  * sin( radians( latitude ) ))
	)* 1.60934 AS distance");

        $this->db->where("delivery_persons.is_online", 1);
        $this->db->where("delivery_persons.push_notification_token != ", "");
        $this->db->from("delivery_persons");
        $this->db->where("delivery_persons.status", 1);


        $this->db->where("delivery_persons.id NOT IN ($already_has_delivery_task_query)");
        $this->db->where("delivery_persons.id NOT IN ($already_has_pick_up_task_query)");

        $this->db->where("(FIND_IN_SET(" . GENERAL_ORDERS . ", delivery_types))");

        if (RADIUS_TYPE == "vendor") {
            if (isset($order_data->restaurant_details->delivery_radius)) {
                if ($order_data->restaurant_details->delivery_radius !== "") {
                    $this->db->having("(distance <= " . $order_data->restaurant_details->delivery_radius . ")");
                }
            }
        } else if (RADIUS_TYPE == "global") {
            $this->db->having("distance < " . DISTANCE_RADIUS);
        }

        $this->db->order_by("distance", "asc");
        $dps = $this->db->get()->result();

        //echo $this->db->last_query();
        //die;
        #print_r($dps);

        if ($dps) {
            $driver_found = false;
            for ($i = 1; $i <= 12; $i++) {
                if ($driver_found == true) {
                    break;
                }

                foreach ($dps as $item) {

                    $one_person_received_notification = false;

                    if (!$item->push_notification_token) {
                        continue;
                    }
                    if ($item->is_online == 0) {
                        continue;
                    }

                    $this->db->where("table_name", "delivery_persons");
                    $this->db->where("service_request_bookings_id", $order_data->id);
                    $this->db->where("action_type", "Delivery Person Skipped");
                    $this->db->where("action_by_user_id", $item->id);
                    $dp_skipped = $this->db->count_all_results("service_request_bookings_log");
                    if ($dp_skipped > 0) {
                        continue;
                    }


                    $this->db->where("service_orders_id", $order_data->id);
                    $this->db->where("delivery_persons_id", $item->id);
                    $already_sent = $this->db->count_all_results("service_order_pushes_log");
                    if ($already_sent > 0) {
                        continue;
                    }

                    $item->distance_to_store = round(distance($item->latitude, $item->longitude, $latitude, $longitude, "K", false), 2);

                    if ($item->distance_to_store < $i) {

                        //Send Notification and create log
                        $this->db->set("service_orders_id", $order_data->id);
                        $this->db->set("delivery_persons_id", $item->id);
                        $this->db->set("new_order_notification_sent_time", THIS_DATE_TIME);
                        $this->db->set("latitude", $item->latitude);
                        $this->db->set("longitude", $item->longitude);
                        $this->db->set("distance_at_the_time_of_new_order_push", $item->distance_to_store);
                        $this->db->insert("service_order_pushes_log");

                        //echo $this->db->last_query();

                        $data = array(
                            "to" => $item->push_notification_token,
                            /* "notification" => array(
                              "title" => "New Order Request",
                              "body" => "New Order ID:#" . $ref_id . "\nStore Name:" . $order_data->restaurant_details->restaurant_name,
                              'image' => SITE_LOGO,
                              'sound' => 'notification_sound',
                              "channel_id" => "fcm_smart_life",
                              'vibrate' => 1
                              ), */
                            "data" => [
                                "title" => "New Order ID: " . $ref_id,
                                "message" => "New Order ID:#" . $ref_id . "\nStore Name:" . $order_data->restaurant_details->restaurant_name,
                                "order_id" => $ref_id,
                                "payload" => [
                                    "type" => "new_order"
                                ]
                            ]
                        );

                        $this->send_push_notification_model->send($data);
                        $driver_found = true;
                        $one_person_received_notification = true;
                    }
                    if ($one_person_received_notification) {
                        break;
                    }
                }
            }
        }
        //echo "Sent notifcaiton";
    }

    function send_manual_notification_to_delivery_boys_from_admin($ref_id, $delivery_person_ids_arr) {

        $order_data = $this->orders_model->get_order_details($ref_id);

        $latitude = $order_data->restaurant_lat;
        $longitude = $order_data->restaurant_lng;

        $this->db->where("ID in (" . implode(",", $delivery_person_ids_arr) . ")");
        $dps = $this->db->get("delivery_persons")->result();

        if ($dps) {

            $driver_found = false;
            for ($i = 1; $i <= 10; $i++) {
                if ($driver_found == true) {
                    break;
                }
                foreach ($dps as $item) {
                    if ($item->distance < $i) {
                        $item->distance_to_store = round(distance($order_data->delivery_lat, $order_data->delivery_lng, $latitude, $longitude, "K", false), 2);

                        //Send Notification and create log
                        $this->db->set("service_orders_id", $order_data->id);
                        $this->db->set("delivery_persons_id", $item->id);
                        $this->db->set("new_order_notification_sent_time", THIS_DATE_TIME);
                        $this->db->set("latitude", $item->latitude);
                        $this->db->set("longitude", $item->longitude);
                        $this->db->set("distance_at_the_time_of_new_order_push", $item->distance_to_store);

                        if ($this->session->userdata("admin_user_id")) {
//                            $this->db->set("");
                        }
                        $this->db->insert("service_order_pushes_log");

                        $data = array(
                            "to" => $item->push_notification_token,
                            "notification" => array(
                                "title" => "New Order Request",
                                "body" => "New Order ID:#" . $ref_id . "\nStore Name:" . $order_data->restaurant_details->restaurant_name,
                                'image' => SITE_LOGO,
                                'sound' => 'notification_sound',
                                'vibrate' => 1
                            ),
                            "data" => [
                                "title" => "New Order ID: " . $ref_id,
                                "message" => "New Order ID:#" . $ref_id . "\nStore Name:" . $order_data->restaurant_details->restaurant_name,
                                "order_id" => $ref_id
                            ]
                        );

                        $this->send_push_notification_model->send($data);
                        $driver_found = true;
                    }
                }
            }
        }
        //echo "Sent notifcaiton";
    }

    function update_status_as_rejected($data, $ref_id, $retail_chain_stores_id = null) {
        $this->db->set($data);
        $this->db->where("ref_id", $ref_id);
        $this->db->set("updated_at", time());
        $this->db->update("service_orders");

        $order_data = $this->orders_model->get_order_details($ref_id);
        $message = "Your order " . $order_data->ref_id . " request has been rejected by " . $order_data->restaurant_details->restaurant_name . " Reason :" . $order_data->rejected_reason;
        send_message($message, $order_data->customer_details->mobile, 1407161883464379397);

        //refund referral wallet_amount 

        /*
          $paid_amount_from_wallet = $order_data->through_wallet_amount;

          $message = "Refund due to store rejected your order #" . $ref_id;
          $od = [
          "amount" => $paid_amount_from_wallet,
          "remark" => $message,
          "customers_id" => $order_data->customers_id,
          ];

          $this->customer_wallet_credits_model->add_credits($od); */

        //Send Notification to Customer
        $cdata = array(
            "to" => $order_data->customer_details->push_notification_key,
            "notification" => array(
                "title" => "Order Rejected ref Id :#" . $ref_id,
                "body" => "Your Order Has been rejected :#" . $ref_id . "\nBy Store Name:" . $order_data->restaurant_details->restaurant_name,
                'image' => SITE_LOGO,
                'sound' => 'notification_sound',
                'vibrate' => 1
            ),
            "data" => [
                "title" => "Order Rejected ref Id :#" . $ref_id,
                "body" => "Your Order Has been rejected :#" . $ref_id . "\nBy Store Name:" . $order_data->restaurant_details->restaurant_name,
                "order_id" => $ref_id
            ]
        );

        $this->send_push_notification_model->send($cdata);


        $notitifcationArr = [
            "from_table_primary_id" => $order_data->id,
            "from_table_name" => "service_orders",
            "comment" => "Order rejected #$ref_id by you",
            "to_table_primary_key" => $order_data->restaurants_id,
            "to_table_name" => "restaurants"
        ];
        $this->notifications_model->create($notitifcationArr);

        $this->orders_activity_log_model->create_log($order_data->id, "Rejected", $order_data->restaurants_id, "restaurants", $retail_chain_stores_id);

        return true;
    }

    function update_status_as_out_for_delivery($data, $ref_id) {
        $this->db->set($data);
        $this->db->where("ref_id", $ref_id);
        $this->db->set("updated_at", time());
        $this->db->update("service_orders");

        $order_data = $this->orders_model->get_order_details($ref_id);
        $message = "Your order " . $order_data->ref_id . " has been packed and out for delivery from " . $order_data->restaurant_details->restaurant_name . "\nContact Name :" . $order_data->delivery_person_name . "\nMobile:" . $order_data->delivery_person_contact_number;
        send_message($message, $order_data->customer_details->mobile, 1407161883470956513);


        //Send Notification to Customer
        $cdata = array(
            "to" => $order_data->customer_details->push_notification_key,
            "notification" => array(
                "title" => "Order Out for delivery ref Id :#" . $ref_id,
                "body" => $message,
                'image' => SITE_LOGO,
                'sound' => 'notification_sound',
                'vibrate' => 1
            ),
            "data" => [
                "title" => "Order Out for delivery ref Id :#" . $ref_id,
                "body" => $message,
                "order_id" => $ref_id
            ]
        );

        $this->send_push_notification_model->send($cdata);


        $this->orders_activity_log_model->create_log($order_data->id, "Out for Delivery", $order_data->restaurants_id, "restaurants");

        $notitifcationArr = [
            "from_table_primary_id" => $order_data->id,
            "from_table_name" => "service_orders",
            "comment" => "Order #$ref_id marked as out for delivery by you",
            "to_table_primary_key" => $data["restaurants_id"],
            "to_table_name" => "restaurants"
        ];
        $this->notifications_model->create($notitifcationArr);
        return true;
    }

    function update_status_as_delivered($data, $ref_id) {
        $this->db->set($data);
        $this->db->where("ref_id", $ref_id);
        $this->db->set("updated_at", time());
        $this->db->set("payment_status", "Paid");
        $this->db->update("service_orders");


        $order_data = $this->orders_model->get_order_details($ref_id);

        //Check and add referral amount
        $this->referral_system_model->credit_referral_amount_on_first_order_delivered($order_data->customers_id);


        $message = "Your order " . $order_data->ref_id . " has been delivered from " . $order_data->restaurant_details->restaurant_name . "\nThank you.";
        send_message($message, $order_data->customer_details->mobile, 1407161883402470928);


        //Send Notification to Customer
        $cdata = array(
            "to" => $order_data->customer_details->push_notification_key,
            "notification" => array(
                "title" => "Order delivered ref Id :#" . $ref_id,
                "body" => $message,
                'image' => SITE_LOGO,
                'sound' => 'notification_sound',
                'vibrate' => 1
            ),
            "data" => [
                "title" => "Order delivered ref Id :#" . $ref_id,
                "body" => $message,
                "order_id" => $ref_id
            ]
        );

        $this->send_push_notification_model->send($cdata);

        $this->orders_activity_log_model->create_log($order_data->id, "Delivered", $order_data->restaurants_id, "restaurants");

        //Credit amount to store wallet
        $this->vendor_wallet_model->add_payable_amount_to_vendor_for_order_completed($ref_id);

        $notitifcationArr = [
            "from_table_primary_id" => $order_data->id,
            "from_table_name" => "service_orders",
            "comment" => "Order #$ref_id marked as delivered by you",
            "to_table_primary_key" => $data["restaurants_id"],
            "to_table_name" => "restaurants"
        ];
        $this->notifications_model->create($notitifcationArr);

        return true;
    }

    function _common_filter($restaurants_id, $filter = []) {
        if ($filter["from_date"]) {
            $this->db->where("service_date >= ", $filter["from_date"]);
        }
        if ($filter["to_date"]) {
            $this->db->where("service_date <= ", $filter["to_date"]);
        }
        $this->db->where("status", 1);
        $this->db->where("restaurants_id IN ($restaurants_id)");
    }

    function get_vendor_total_orders($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->where("service_status", "Completed");
        $this->db->where("payment_status", "Paid");
        return $this->db->count_all_results("service_orders");
    }

    function get_vendor_pending_orders($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->where("service_status", "Pending");
        $this->db->where("((payment_mode = 'Pay Online' AND payment_status='Paid') OR (payment_mode = 'Pay On Delivery'))");
        return $this->db->count_all_results("service_orders");
    }

    function get_vendor_accepted_orders($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->where("service_status", "Waiting For Delivery Person");
        return $this->db->count_all_results("service_orders");
    }

    function get_vendor_waiting_for_pickup($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->where("service_status", "Waiting For Pickup");
        return $this->db->count_all_results("service_orders");
    }

    function get_vendor_out_for_delivery($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->where("service_status", "Out For Delivery");
        return $this->db->count_all_results("service_orders");
    }

    function get_vendor_total_completed_orders($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->where("service_status", "Completed");
        return $this->db->count_all_results("service_orders");
    }

    function get_vendor_total_rejected($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->where("service_status", "Rejected");
        return $this->db->count_all_results("service_orders");
    }

    function get_vendor_on_delivery_orders($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->where("(service_status='Out For Delivery' OR service_status='Waiting For Pickup')");
        $this->db->where("restaurants_id IN ($restaurants_id)");
        return $this->db->count_all_results("service_orders");
    }

    function get_vendor_rejected_orders($restaurants_id, $filter = []) {
        $this->db->where("status", 1);
        $this->db->where("service_status", "Rejected");
        $this->db->where("restaurants_id", $restaurants_id);
        return $this->db->count_all_results("service_orders");
    }

    function get_vendor_total_earnings($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->select("sum(payable_amount_to_restaurant) as earnings");
        $this->db->where("status", 1);
        $this->db->select("payment_status", "Paid");
        $this->db->where("service_status", "Completed");
        $cnt = $this->db->get("service_orders")->row()->earnings;
        if ($cnt) {
            return $cnt;
        }
        return 0;
    }

    function get_vendor_earnings_from_orders($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->select("sum(payable_amount_to_restaurant) as earnings");
        $this->db->where("status", 1);
        $this->db->select("payment_status", "Paid");
        $this->db->where("service_status", "Completed");
        $cnt = $this->db->get("service_orders")->row()->earnings;
        if ($cnt) {
            return (float) $cnt;
        }
        return 0;
    }

    function get_vendor_sale_amount_from_orders($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->select("sum(sub_total) as earnings");
        $this->db->where("status", 1);
        $this->db->select("payment_status", "Paid");
        $this->db->where("service_status", "Completed");
        $cnt = $this->db->get("service_orders")->row()->earnings;
        if ($cnt) {
            return (float) $cnt;
        }
        return 0;
    }

    function get_vendor_pending_payout_from_orders($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->select("sum(payable_amount_to_restaurant) as earnings");
        $this->db->where("status", 1);
        $this->db->select("payment_status", "Paid");
        $this->db->where("service_status", "Completed");
        $this->db->where("settlement_status", "Pending");
        $cnt = $this->db->get("service_orders")->row()->earnings;
        if ($cnt) {
            return $cnt;
        }
        return 0;
    }

    function get_vendor_paid_payout_from_orders($restaurants_id, $filter = []) {
        $this->_common_filter($restaurants_id, $filter);
        $this->db->select("sum(payable_amount_to_restaurant) as earnings");
        $this->db->where("status", 1);
        $this->db->select("payment_status", "Paid");
        $this->db->where("service_status", "Completed");
        $this->db->where("settlement_status", "Paid");
        $cnt = $this->db->get("service_orders")->row()->earnings;
        if ($cnt) {
            return $cnt;
        }
        return 0;
    }

    function apply_css_classes($item) {
        if ($item->service_status == "Pending") {
            $item->service_status_bootstrap_class = "warning";
        } else if ($item->service_status == "Completed") {
            $item->service_status_bootstrap_class = "success";
        } else if ($item->service_status == "On Going") {
            $item->service_status_bootstrap_class = "info";
        } else if ($item->service_status == "Preparing") {
            $item->service_status_bootstrap_class = "info";
        } else if ($item->service_status == "Rejected") {
            $item->service_status_bootstrap_class = "danger";
        } else if ($item->service_status == "Unsuccessful") {
            $item->service_status_bootstrap_class = "danger";
        }

        if ($item->payment_status == "Pending") {
            $item->payment_status_bootstrap_class = "warning";
        } else if ($item->payment_status == "Paid") {
            $item->payment_status_bootstrap_class = "success";
        } else if ($item->payment_status == "Cancelled") {
            $item->payment_status_bootstrap_class = "danger";
        } else if ($item->payment_status == "Not Received") {
            $item->payment_status_bootstrap_class = "danger";
        } else if ($item->payment_status == "Refund") {
            $item->payment_status_bootstrap_class = "danger";
        }

        if ($item->settlement_status == "Pending") {
            $item->settlement_status_bootstrap_class = "warning";
        } else if ($item->settlement_status == "Completed") {
            $item->settlement_status_bootstrap_class = "success";
        } else if ($item->settlement_status == "Cancelled") {
            $item->settlement_status_bootstrap_class = "danger";
        }
    }

    function get_vendor_completed_orders($restaurants_id) {
        $this->db->select("count(id) as orders_count");
        $this->db->where("status", 1);
        $this->db->select("payment_status", "Paid");
        $this->db->where("service_status", "Completed");
        $this->db->where("restaurants_id", $restaurants_id);
        $cnt = $this->db->get("service_orders")->row()->orders_count;
        if ($cnt) {
            return $cnt;
        }
        return 0;
    }

    function get_vendor_cancelled_orders($restaurants_id) {
        $this->db->select("count(id) as orders_count");
        $this->db->where("status", 1);
        $this->db->where("service_status", "Cancelled");
        $this->db->where("restaurants_id", $restaurants_id);
        $cnt = $this->db->get("service_orders")->row()->orders_count;
        if ($cnt) {
            return $cnt;
        }
        return 0;
    }

    function check_for_any_in_progress_for_customer($customers_id) {
        $this->db->select("ref_id, restaurants_id, service_status, delivery_person_name, delivery_person_contact_number");
        $this->db->where("status", 1);
        $this->db->where("customers_id", $customers_id);

        $this->db->where("service_date", date("Y-m-d"));
        $this->db->where("(service_status = 'Preparing' 
			OR service_status = 'On Going'
			OR service_status = 'Pending'
			OR service_status = 'Waiting For Pickup'
			OR service_status = 'Out For Delivery'
			OR service_status = 'Waiting For Delivery Person'
			)");
        $this->db->where("((payment_mode = 'Pay Online' AND payment_status='Paid') OR (payment_mode = 'Pay On Delivery'))");
        $data = $this->db->get("service_orders")->row();
        if ($data) {
            $restaurant_details = $this->vendor_model->get_vendor_details($data->restaurants_id);
            $data->restaurant_name = $restaurant_details->restaurant_name;
            $data->is_own_store = $restaurant_details->is_own_store;
            return $data;
        }
        return [];
    }

    function delivery_persons_who_are_near_to_store($store_id, $service_order_id, $store_distance_check = true) {

        $data = (object) [
                    "push_notification_sent_delivery_persons" => [],
                    "other_delivery_persons" => []
        ];

        $this->db->select('latitude, longitude, delivery_radius');
        $this->db->where('id', $store_id);
        $store = $this->db->get("restaurants")->row();

        if ($store->latitude == "" || $store->longitude == "") {
            return [];
        }
        //push notification sent delivery_persons
        $this->db->select("service_order_pushes_log.new_order_notification_sent_time");
        $this->db->select("delivery_persons.person_name");
        $this->db->select("delivery_persons.mobile_number");
        $this->db->select("service_order_pushes_log.delivery_persons_id");
        $this->db->select("service_order_pushes_log.delivery_persons_id as id");
        $this->db->select("delivery_persons.is_online");
        $this->db->select("delivery_persons.latitude");
        $this->db->select("delivery_persons.longitude");

        $this->db->where("service_orders_id", $service_order_id);
        $this->db->from("service_order_pushes_log");
        $this->db->join("delivery_persons", "service_order_pushes_log.delivery_persons_id = delivery_persons.id");
        $this->db->group_by("service_order_pushes_log.delivery_persons_id");
        //$this->db->order_by("service_order_pushes_log.id", "desc");

        $this->db->select(" ( 3959 * acos (  cos ( radians($store->latitude) ) * "
                . "cos( radians( delivery_persons.latitude ) ) * cos( radians( delivery_persons.longitude ) - radians($store->longitude) ) + "
                . "sin ( radians($store->latitude) )  * sin( radians( delivery_persons.latitude ) )) )* 1.60934 AS distance");
        $this->db->order_by("distance", "asc");
        $data->push_notification_sent_delivery_persons = $this->db->get()->result();

        //----------------------------------------------------------------------------//

        $ids = [];
        foreach ($data->push_notification_sent_delivery_persons as $dp_item) {
            $ids[] = $dp_item->delivery_persons_id;
            $dp_item->distance = round($dp_item->distance, 2) . " Km";
        }

        if (count($ids)) {
            $this->db->where("delivery_persons.id NOT IN (" . implode(",", $ids) . ")");
        }
        $this->db->select("id, person_name, mobile_number, is_online, latitude, longitude");
        $this->db->select(" ( 3959 * acos (  cos ( radians($store->latitude) ) * "
                . "cos( radians( latitude ) ) * cos( radians( longitude ) - radians($store->longitude) ) + "
                . "sin ( radians($store->latitude) )  * sin( radians( latitude ) )) )* 1.60934 AS distance");

        $this->db->where("status", 1);
        if ($store_distance_check) {
            $this->db->having("distance < '$store->delivery_radius'");
        }
        $this->db->order_by("distance", "asc");

        $this->db->where("(FIND_IN_SET(" . GENERAL_ORDERS . ", delivery_types))");

        $data->other_delivery_persons = $this->db->get("delivery_persons")->result();
        //$this->db->where("distance <=", $store->delivery_radius);

        foreach ($data->other_delivery_persons as $odp) {
            $odp->distance = round($odp->distance, 2) . " Km";
        }

        // print_r($data->other_delivery_persons);
        //die;
        return $data;
    }

}
