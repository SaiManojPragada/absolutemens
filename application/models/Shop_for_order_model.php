<?php

class Shop_for_order_model extends CI_Model {

    function get_order_details($order_id) {

        $data = $this->u_model->get_nostatcheck_row("subscriptions_transactions", array("unq_id" => $order_id));
        $data->ordered_on = date('d M Y, h:i A', $data->updated_at);
        $data->package_details = $this->u_model->get_nostatcheck_row("packages", array("id" => $data->packages_id));
        $data->package_details->title = strip_tags($data->package_details->title);
        $data->package_details->price = (int) $data->package_details->price;
        $data->package_details->price = number_format($data->package_details->price);
        $this->db->where('id', $data->customers_id);
        $data->customer_details = $this->db->get('customers')->row();
        $this->db->where('subscriptions_transactions_id', $data->unq_id);
        $data->images = $this->db->get("subscription_images")->result();
        $data->selected_sizes = $this->get_selected_sizes($data->selected_sizes);
        return $data;
    }

    function get_selected_sizes($selected_sizes) {
        $selected_sizes_arr = json_decode($selected_sizes);
        $arr = [];
        foreach ($selected_sizes_arr as $key => $value) {
            $obj = new stdClass();
            $obj->category = $this->db->get_where('categories', ['id' => $key])->row()->name;
            $obj->size = $this->db->get_where('sizes', ['id' => $value])->row()->title;
            $obj->category_id = $key;
            $arr[] = $obj;
        }
        return $arr;
    }

    function get($filters) {
        $subscription_row = $this->db->select('selected_sizes')->get_where('subscriptions_transactions', ['unq_id' => $filters['unique_order_id']])->row();
        $selected_sizes_arr = json_decode($subscription_row->selected_sizes);
        $this->db->select('p.categories_id,p.id,p.product_id,p.title,p.description,p.brand,p.categories_id,pi.id as product_inventory_id,pi.size_id,c.name,s.title as size');
        $this->db->from('product_inventory pi');
        $this->db->join('products p', 'p.id = pi.product_id', 'inner');
        $this->db->join('categories c', 'c.id = p.categories_id', 'inner');
        $this->db->join('sizes s', 's.id = pi.size_id', 'inner');
        if ($filters['category_id'] != "") {
            $this->db->where('p.categories_id', $filters['category_id']);
        }
        if ($filters['product_code'] != "") {
            $this->db->where('p.product_id', $filters['product_code']);
        }
        if ($filters['vendor_id'] != "") {
            $this->db->where('p.vendors_id', $filters['vendor_id']);
        }
//        if (isset($filters['limit']) && isset($filters['start'])) {
//            $this->db->limit($filters["limit"], $filters["start"]);
//        }
        $data = $this->db->get()->result();

        $main_arr = [];
        if ($data[0]->id != "") {
            foreach ($data as $item) {
                $item->price = $this->u_model->get_data_conditon_row("product_inventory", array("product_id" => $item->id, "size_id" => $item->size_id))->price;
                $item->product_image = base_url() . $this->get_product_image($item->id);
                foreach ($selected_sizes_arr as $key => $value) {
                    if ($item->categories_id == $key && $item->size_id == $value) {
                        $main_arr[] = $item;
                    }
                }
            }
            return $main_arr;
        } else {
            return [];
        }
    }

    function get_category_size_prefix($category_id) {
        return $this->db->get_where('categories', ['id' => $category_id])->row()->data_table_prefix;
    }

    function get_product_image($product_id) {
        return $this->db->get_where('products_images', ['product_id' => $product_id])->row()->image;
    }

    function get_product_sizes($product_id, $size_prefix) {
        $this->db->select('pi.id,pi.size');
        $this->db->from('products_inventory pi');
        $this->db->join("$size_prefix" . "_sizes ps", 'ps.id = pi.sizes_id', 'inner');
        $this->db->where('pi.status', 1);
        $this->db->where('pi.product_id', $product_id);
        $result = $this->db->get()->result();
        if ($result[0]->id > 0) {
            return $result;
        }
        return [];
    }

    function get_products_count($filters) {
        $subscription_row = $this->db->select('selected_sizes')->get_where('subscriptions_transactions', ['unq_id' => $filters['unique_order_id']])->row();
        $selected_sizes_arr = json_decode($subscription_row->selected_sizes);
        $this->db->select('p.categories_id,p.id,p.product_id,p.title,p.description,p.brand,p.categories_id,pi.id as product_inventory_id,pi.size_id,c.name,s.title as size');
        $this->db->from('product_inventory pi');
        $this->db->join('products p', 'p.id = pi.product_id', 'inner');
        $this->db->join('categories c', 'c.id = p.categories_id', 'inner');
        $this->db->join('sizes s', 's.id = pi.size_id', 'inner');
        if ($filters['category_id'] != "") {
            $this->db->where('p.categories_id', $filters['category_id']);
        }
        if ($filters['product_code'] != "") {
            $this->db->where('p.product_id', $filters['product_code']);
        }
        if ($filters['vendor_id'] != "") {
            $this->db->where('p.vendors_id', $filters['vendor_id']);
        }

        $data = $this->db->get()->result();
        $main_arr = [];
        if ($data[0]->id != "") {
            foreach ($data as $item) {
                foreach ($selected_sizes_arr as $key => $value) {
                    if ($item->size_id == $value) {
                        $main_arr[] = $item;
                    }
                }
            }
            return count($main_arr);
        } else {
            return 0;
        }
    }

    function get_product_details($product_id, $inventory_id = null) {
        $res = $this->u_model->get_data_conditon_row("products", array("id" => $product_id));
        $this->db->where("id", $res->categories_id);
        $res->category = $this->db->get("categories")->row();
        $this->db->where("id", $res->sizes_id);
        $res->size = $this->db->get($res->category->data_table_prefix . "_sizes")->row()->size;
        $res->category = $res->category->name;
        $this->db->where("product_id", $res->id);
        $res->images = $this->db->get("products_images")->result();
        $this->db->where("product_id", $res->id);
        if ($inventory_id) {
            $res->inventory = $this->db->get_where("product_inventory", ['id' => $inventory_id])->result();
        } else {
            $res->inventory = $this->db->get("product_inventory")->result();
        }

        foreach ($res->inventory as $i) {
            $i->size = $this->db->get_where('sizes', ['id' => $i->size_id])->row()->title;
            $i->price = (int) $i->price;
        }
        $res->updated_at = date('d M Y, h:i A', $res->updated_at);
        return $res;
    }

    function get_categories() {
        $this->db->select('id,name');
        $this->db->from('categories');
        $this->db->where('status', 1);
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            return $result;
        }
        return [];
    }

    function get_vendors() {
        $this->db->select('id,owner_name,business_name');
        $this->db->from('vendors');
        $this->db->where('status', 1);
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            return $result;
        }
        return [];
    }

    function forward_order_to_fashion_manager() {
        $this->db->trans_begin();

        $cart_data = $this->fashion_designer_cart_model->get_cart();
        $cart_list = $cart_data['result'];
        $subscription_transaction_id = $cart_list[0]->subscription_transaction_id;
        $fashion_manager_id = $this->db->get_where('subscriptions_transactions', ['id' => $subscription_transaction_id])->row()->fashion_manager_id;
        $fashion_id = $this->db->get_where('subscriptions_transactions', ['id' => $subscription_transaction_id])->row()->fashion_analyst_id;
        $subscription_transaction_unq_id = $this->u_model->get_nostatcheck_row('subscriptions_transactions', array('id' => $subscription_transaction_id))->unq_id;
//        $order_id = $this->generate_order_id();
        $order_id = $subscription_transaction_unq_id;

        //update previous orders status to 0;
        $this->update_previous_order_status($subscription_transaction_unq_id);

        $fashion_designer_name = $this->get_user_details($fashion_id)->name;
        $fashion_manager = $this->get_user_details($fashion_manager_id)->name;

        $fashion_designer_mobile = $this->get_user_details($fashion_id)->phone_number;
        $fashion_manager_mobile = $this->get_user_details($fashion_manager_id)->phone_number;

        if (count($cart_list) > 0) {
            //insert into main orders
            $order_array = [
                "order_id" => $order_id,
                "subscription_transaction_id" => $subscription_transaction_id,
                "fashion_manager_id" => $fashion_manager_id,
                "fashion_analyst_id" => $fashion_id,
                "total" => $cart_data['sub_total'],
                "final_total" => $cart_data['final_amount_including_taxes'],
                "order_date" => date('Y-m-d'),
                "order_status" => "suggested_by_fashion_analyst",
                "created_at" => time()
            ];
            $this->db->insert('orders', $order_array);
            $insert_order_id = $this->db->insert_id();
            if ($insert_order_id) {
                //add to order products
                foreach ($cart_list as $cl) {
                    $vendor_id = $this->db->select('vendors_id')->from('products')->where('id', $cl->product_id)->get()->row()->vendors_id;
                    $order_products_array = [
                        "order_id" => $insert_order_id,
                        "vendor_id" => $vendor_id,
                        "product_id" => $cl->product_id,
                        "product_inventory_id" => $cl->product_inventory_id,
                        "unit_price" => $cl->unit_price,
                        "quantity" => $cl->quantity,
                        "subtotal" => $cl->subtotal,
                        "comment" => $cl->comment,
                        "created_at" => time(),
                        "approved_by_fashion_manager" => "no"
                    ];
                    $this->db->insert('order_products', $order_products_array);
                    $inserted_product_id = $this->db->insert_id();
                    //add to order logs
                }

                $order_log_array = [
                    "order_real_id" => $subscription_transaction_id,
                    "order_id" => $order_array['order_id'],
                    "order_status" => "suggested_by_fashion_analyst",
                    "log" => "Fashion analyst-$fashion_designer_name ($fashion_designer_mobile) has forwared the fashion items containing Order id - " . $order_array['order_id'] . " to Fashion manager-$fashion_manager ($fashion_manager_mobile).Fashion manager-$fashion_manager($fashion_manager_mobile) yet to be processed to forward to the admin",
                    "action_date_time" => date('Y-m-d H:i:s'),
                    "created_at" => time(),
                    "action_user_id" => $fashion_id,
                    "table_name" => 'designers_and_analysts',
                    "from_table" => 'order_products'
                ];
                $this->db->insert('orders_log', $order_log_array);

                //update cart status to inactive
                $this->db->set('cart_status', 'in_active');
                $this->db->where('subscription_transaction_id', $subscription_transaction_id);
                $this->db->update('fashion_designer_cart');

                //update status in transactions
                $this->db->set('order_status', 'Selected Items Under Review');
                $this->db->where('id', $subscription_transaction_id);
                $this->db->update('subscriptions_transactions');
            }
        } else {
            return ['status' => "no_items"];
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return ['status' => "error"];
        } else {
            $this->db->trans_commit();
            return ['status' => "success", "name" => $fashion_manager . "($fashion_manager_mobile)"];
        }
    }

    function update_previous_order_status($unq_id) {
        $result = $this->db->select('id')->get_where('orders', ['order_id' => $unq_id, 'status' => 1])->result();
        foreach ($result as $r) {
            $this->db->set('status', 0);
            $this->db->where('id', $r->id);
            $this->db->update('orders');
        }
    }

    function generate_order_id() {
        $order_id = "ABM" . rand(11111111, 99999999);
        $rows = $this->db->select('id')->from('orders')->where('order_id', $order_id)->count_all_results();
        if ($rows == 0) {
            return $order_id;
        } else {
            $this->generate_order_id();
        }
    }

    function get_user_details($user_id) {
        $row = $this->db->select('name,phone_number')->get_where('designers_and_analysts', ['id' => $user_id])->row();
        return $row;
    }

    function check_order_already_forwarded_to_fashion_manager($unique_order_id) {
        $subscription_transaction_id = $this->db->get_where('subscriptions_transactions', ['unq_id' => $unique_order_id])->row()->id;
        $this->db->select('id');
        $this->db->from('orders');
        $this->db->where('subscription_transaction_id', $subscription_transaction_id);
        $this->db->where('order_status !=', 'rejected_by_fashion_manager');
        $this->db->where('is_exchanged', 0);
        $this->db->where('status', 1);
        $rows = $this->db->count_all_results();
        if ($rows > 0) {
            return ['status' => "forwarded", "data" => null];
        } else {
            $order_row = $this->get_order_status($subscription_transaction_id);
            return ['status' => "not_forwarded", "data" => $order_row];
        }
    }

    function get_order_status($subscription_transaction_id) {
        $this->db->select('order_status,reject_reason');
        $this->db->from('orders');
        $this->db->where('subscription_transaction_id', $subscription_transaction_id);
        $this->db->where('status', 1);
        $this->db->where('is_exchanged', 0);
        $this->db->order_by('id', 'DESC');
        $row = $this->db->get()->row();
        return $row;
    }

    function get_order_logs($unique_order_id) {
        $subscription_transaction_id = $this->db->get_where('subscriptions_transactions', ['unq_id' => $unique_order_id])->row()->id;
        $this->db->select('id,order_id,log,order_status,created_at');
        $this->db->from('orders_log');
        $this->db->where('order_real_id', $subscription_transaction_id);
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
            }
            return $result;
        }
        return [];
    }

}
