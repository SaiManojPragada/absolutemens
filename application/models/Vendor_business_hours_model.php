<?php

class Vendor_business_hours_model extends CI_Model {

    private $table_name = "restaurant_work_hours";

    function get($restaurants_id) {


        $cache_key = "vendor_business_hours_" . $restaurants_id;
        $cache = $this->cache_model->get_cache_if_exists($cache_key);
        if ($cache) {
            return $cache;
        }


        $array = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $data = [];
        foreach ($array as $week) {
            $this->db->where("week_name", $week);
            $this->db->where("status", 1);
            $this->db->where("restaurants_id", $restaurants_id);
            $row = $this->db->get($this->table_name)->row();
            if ($row) {
                $row->display_week_name = $row->week_name == date("l") ? "Today" : $row->week_name;
                $row->display_timings = date("h:i A", strtotime(date("Y-m-d " . $row->from_time))) . " - " . date("h:i A", strtotime(date("Y-m-d " . $row->to_time)));
                unset($row->created_at);
                unset($row->updated_at);
                unset($row->status);
                $data[] = $row;
            } else {
                $data[] = array(
                    "restaurants_id" => $restaurants_id,
                    "is_working_day" => "1",
                    "week_name" => $week,
                    "from_time" => "10:00:00",
                    "to_time" => "21:00:00"
                );
            }
        }
        $this->cache_model->save_to_cache($cache_key, $data);
        return $data;
    }

    function get_working_days_count($restaurants_id, $week = NULL) {
        if ($week != NULL && $week != '') {
            $this->db->where("week_name", $week);
        }
        $this->db->where("status", 1);
        $this->db->where("is_working_day", 1);
        $this->db->where("restaurants_id", $restaurants_id);
        $rows = $this->db->get($this->table_name)->num_rows();
        return $rows;
    }

    function updated_business_hours($data) {
        $restaurants_id = $data["restaurants_id"];
        $business_hours = $data["business_hours"];
        foreach ($business_hours as $item) {
            $this->db->where("week_name", $item["week_name"]);
            $this->db->where("restaurants_id", $restaurants_id);
            $row = $this->db->get($this->table_name)->row();

            $this->db->set("week_name", $item["week_name"]);
            $this->db->set("from_time", $item["from_time"]);
            $this->db->set("is_working_day", $item["is_working_day"]);
            $this->db->set("to_time", $item["to_time"]);
            $this->db->set("status", 1);
            if ($row) {
                $this->db->set("updated_at", time());
                $this->db->where("id", $row->id);
                $this->db->update($this->table_name);
            } else {
                $this->db->set("restaurants_id", $restaurants_id);
                $this->db->set("created_at", time());
                $this->db->insert($this->table_name);
            }
        }

        $cache_key = "vendor_business_hours_" . $restaurants_id;
        $this->cache_model->delete_cache_if_exists($cache_key);

        return true;
    }

}
