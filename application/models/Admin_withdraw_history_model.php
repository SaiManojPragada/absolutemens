<?php

class Admin_withdraw_history_model extends CI_model {

    function get($filters) {
        $this->db->select('wr.released_at,wr.rejected_at,wr.reject_reason,wr.approve_comment,wr.id,wr.request_id,wr.comment,wr.amount,wr.withdraw_status,wr.created_at,vb.account_name,vb.account_number,vb.bank_name,vb.branch,vb.ifsc_code,v.business_name,v.owner_name,v.contact_email,v.contact_number');
        $this->db->from('vendor_withdraw_requests wr');
        $this->db->join('vendor_bank_details vb', 'vb.vendors_id = wr.vendor_id');
        $this->db->join('vendors v', 'v.id = wr.vendor_id');
        $this->db->where('wr.status', 1);
        if ($filters['owner_name'] != "") {
            $this->db->like('v.owner_name', $filters['owner_name']);
        }
        if ($filters['owner_email'] != "") {
            $this->db->like('v.contact_email', $filters['owner_email']);
        }
        if ($filters['owner_contact_number'] != "") {
            $this->db->where('v.contact_number', $filters['owner_contact_number']);
        }
        if ($filters['request_id'] != "") {
            $this->db->where('wr.request_id', $filters['request_id']);
        }
        if ($filters['withdraw_status'] != "") {
            $this->db->where('wr.withdraw_status', $filters['withdraw_status']);
        }
        if ($filters['from_date'] != "" && $filters['to_date'] != "") {
            $this->db->where('wr.date_time >=', date('Y-m-d', strtotime($filters['from_date'])));
            $this->db->where('wr.date_time <=', date('Y-m-d', strtotime($filters['to_date'])));
        }
        $this->db->order_by('wr.id', 'DESC');
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
                if ($item->withdraw_status == "released") {
                    $item->approved_at = date('d-m-Y H:i:s a', strtotime($item->released_at));
                }
                if ($item->withdraw_status == "rejected") {
                    $item->rejected_at = date('d-m-Y H:i:s a', strtotime($item->rejected_at));
                }
            }
            return $result;
        }
        return [];
    }

    function approve_request($approve_comment, $withdraw_request_id) {
        $this->db->trans_begin();
        $this->db->set('approve_comment', $approve_comment);
        $this->db->set('released_at', date('Y-m-d H:i:s'));
        $this->db->set('withdraw_status', 'released');
        $this->db->where('id', $withdraw_request_id);
        $update = $this->db->update('vendor_withdraw_requests');
        if ($update) {

            $this->debit_from_wallet_add_to_vendor_settlements($withdraw_request_id);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function debit_from_wallet_add_to_vendor_settlements($withdraw_request_id) {

        $vendor_id = $this->db->get_where('vendor_withdraw_requests', ['id' => $withdraw_request_id])->row()->vendor_id;
        $amount = $this->db->get_where('vendor_withdraw_requests', ['id' => $withdraw_request_id])->row()->amount;
        $request_id = $this->db->get_where('vendor_withdraw_requests', ['id' => $withdraw_request_id])->row()->request_id;
        $debit_from_wallet = $this->debit_amount_from_wallet($vendor_id, $amount, $request_id);
        if ($debit_from_wallet) {
            //add to vendor settlements
            $settlement_arr = [
                "vendor_id" => $vendor_id,
                "withdraw_request_id" => $withdraw_request_id,
                "amount" => $amount,
                "date" => date('Y-m-d'),
                "settled_status" => "completed",
                "created_at" => time()
            ];
            $this->db->insert('vendor_settlements', $settlement_arr);
            $settlement_id = $this->db->insert_id();
            if ($settlement_id > 0) {
                $this->clear_hold_wallet_amount_for_vendor($vendor_id, $amount);
            }
        }
    }

    function clear_hold_wallet_amount_for_vendor($vendor_id, $amount) {
        $previous_on_hold_amount = $this->db->get_where('vendors', ['id' => $vendor_id])->row()->onhold_wallet_amount;
        $updated_on_hold_amount = $previous_on_hold_amount - $amount;
        $this->db->set('onhold_wallet_amount', $updated_on_hold_amount);
        $this->db->where('id', $vendor_id);
        $this->db->update('vendors');
    }

    function debit_amount_from_wallet($vendor_id, $amount, $request_id) {

        $wallet_balance = $this->get_wallet_balance($vendor_id);
        $updated_wallet_balance = (int) $wallet_balance - (int) $amount;
        $wallet_array = [
            "vendor_id" => $vendor_id,
            "vendor_settlement_id" => null,
            "action" => "debit",
            "credit_type" => "from withdraw request",
            "amount" => $amount,
            "title" => "Amount Debited",
            "comment" => "$amount amount debited for withdraw request id - $request_id ",
            "balance" => $updated_wallet_balance,
            "date" => date('Y-m-d'),
            "created_at" => time()
        ];
        $this->db->insert('vendor_wallet', $wallet_array);
        $wallet_id = $this->db->insert_id();
        if ($wallet_id > 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_wallet_balance($vendor_id) {
        $this->db->select('balance');
        $this->db->from('vendor_wallet');
        $this->db->where('vendor_id', $vendor_id);
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $balance = $this->db->get()->row()->balance;
        return $balance;
    }

    function reject_request($reject_reason, $withdraw_request_id) {
        $this->db->trans_begin();
        $this->db->set('reject_reason', $reject_reason);
        $this->db->set('rejected_at', date('Y-m-d H:i:s'));
        $this->db->set('withdraw_status', 'rejected');
        $this->db->where('id', $withdraw_request_id);
        $update = $this->db->update('vendor_withdraw_requests');
        if ($update) {
            $vendor_id = $this->db->get_where('vendor_withdraw_requests', ['id' => $withdraw_request_id])->row()->vendor_id;
            $amount = $this->db->get_where('vendor_withdraw_requests', ['id' => $withdraw_request_id])->row()->amount;
            $this->clear_hold_wallet_amount_for_vendor($vendor_id, $amount);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

}
