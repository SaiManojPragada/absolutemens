<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Vendor_earnings_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = "vendor_settlements";
    }

    function get($filters) {
        $vendor_id = $this->session->userdata('vendor_id');
        $this->db->select('id,withdraw_request_id,amount,created_at');
        $this->db->from('vendor_settlements');
        $this->db->where('vendor_id', $vendor_id);
        if ($filters['type'] == "today") {
            $this->db->where('Date(date)= CURDATE()');
        } else if ($filters['type'] == "weekly") {
            $this->db->where('YEARWEEK(date)= YEARWEEK(CURDATE())');
        } else if ($filters['type'] == "monthly") {
            $this->db->where('Month(date)= Month(CURDATE())');
        } else if ($filters['type'] == "year") {
            $this->db->where('Year(date)=Year(CURDATE())');
        } else if ($filters['type'] == "custom") {
            $post_from_date = convert_js_timer_to_php_timer($filters['from_date']);
            $post_to_date = convert_js_timer_to_php_timer($filters['to_date']);
            $from_date = date('Y-m-d', $post_from_date);
            $to_date = date('Y-m-d', $post_to_date);
            $this->db->where('date >=', $from_date);
            $this->db->where('date <=', $to_date);
        }
        $this->db->where('status', 1);
        if (isset($filters['limit']) && isset($filters['start'])) {
            $this->db->limit($filters["limit"], $filters["start"]);
        }
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get()->result();

        $total_amout = 0;
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $withdraw_row = $this->db->get_where('vendor_withdraw_requests', ['id' => $item->withdraw_request_id])->row();
                $item->request_id = $withdraw_row->request_id;
                $item->comment = $withdraw_row->approve_comment;
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
                $total_amout += $item->amount;
            }
            return ["data" => $result, "total_amount" => $total_amout];
        } else {
            return [];
        }
    }

    function get_count($filters) {
        $vendor_id = $this->session->userdata('vendor_id');
        $this->db->select('id,withdraw_request_id,amount,created_at');
        $this->db->from('vendor_settlements');
        $this->db->where('vendor_id', $vendor_id);

        if ($filters['type'] == "today") {
            $this->db->where('Date(date)= CURDATE()');
        } else if ($filters['type'] == "weekly") {
            $this->db->where('YEARWEEK(date)= YEARWEEK(CURDATE())');
        } else if ($filters['type'] == "monthly") {
            $this->db->where('Month(date)= Month(CURDATE())');
        } else if ($filters['type'] == "year") {
            $this->db->where('Year(date)=Year(CURDATE())');
        } else if ($filters['type'] == "custom") {
            $post_from_date = convert_js_timer_to_php_timer($filters['from_date']);
            $post_to_date = convert_js_timer_to_php_timer($filters['to_date']);
            $from_date = date('Y-m-d', $post_from_date);
            $to_date = date('Y-m-d', $post_to_date);
            $this->db->where('date >=', $from_date);
            $this->db->where('date <=', $to_date);
        }
        $this->db->where('status', 1);
        $rows = $this->db->count_all_results();
        return $rows;
    }

}
