<?php

class Fashion_designer_model extends CI_model {

    function get_details($id) {
        $details = $this->db->get_where("designers_and_analysts", ['id' => $id])->row();
        if (!$details) {
            return false;
        }
        return $details;
    }

    function create_login_session($data) {
        $this->session->set_userdata("fashion_id", $data->id);
        $this->session->set_userdata("fashion_name", $data->name);
        $this->session->set_userdata("fashion_ref_id", $data->ref_id);
        $this->session->set_userdata("fashion_designation", $data->designation);
        $this->session->set_userdata("fashion_logged", true);

        $address = $this->get_client_ip();
        $insert_data = array("ip_address" => $address,
            "designers_and_analysts_id" => $data->id, "created_at" => time());

        return $this->db->insert("fashion_desg_login_logs", $insert_data);
    }

    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
