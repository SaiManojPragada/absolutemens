<?php
class Vendor_coupons_model extends CI_Model{

	private $table_name = "restaurant_coupon_requests";

	function get($restaurants_id){
		//$this->db->order_by("priority");
		$this->db->where('status',1);
		$this->db->where("restaurants_id", $restaurants_id);
		$data = $this->db->get($this->table_name)->result();
		if($data){
			return $data;
		}else{
			return [];
		}
	}

	function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        return $response;
    }

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        return $response;
    }

}
