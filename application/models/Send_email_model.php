<?php

class Send_email_model extends CI_Model {

    function send_mail($email, $subject, $message, $from = 'support@absolutemens.com') {
        $site_data = $this->db->get("site_settings")->row();
        $this->load->library('email');
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = $site_data->smtp_type . '://' . $site_data->smtp_host;
        $config['smtp_user'] = $site_data->smtp_user_name;
        $config['smtp_pass'] = $site_data->smtp_password;
        $config['smtp_port'] = 465;
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->load->library('email');
        $this->email->from($from);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);
        $send = $this->email->send();
        if (!$send) {
            echo $this->email->print_debugger();
            die();
        }
        return true;
    }

}
