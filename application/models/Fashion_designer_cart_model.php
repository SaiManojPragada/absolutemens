<?php

class Fashion_designer_cart_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->fashion_id = $this->session->userdata('fashion_id');
    }

    function add($data, $unique_id) {
        $subscription_transaction_id = $this->db->get_where('subscriptions_transactions', ['unq_id' => $unique_id])->row()->id;

        $data['designers_and_analysts_id'] = $this->fashion_id;
        $previous_cart_row = $this->check_product_already_added_to_cart($data['product_id'], $data['product_inventory_id'], $data['designers_and_analysts_id']);
        if (!$previous_cart_row) {
            $unit_price = $this->get_product_price($data['product_id'], $data['product_inventory_id']);
            $data['unit_price'] = $unit_price;
            $data['subtotal'] = $unit_price * $data['quantity'];
            $data['subscription_transaction_id'] = $subscription_transaction_id;
            $data['cart_status'] = "active";
            $data['created_at'] = time();
            $this->db->insert('fashion_designer_cart', $data);
            $insert_id = $this->db->insert_id();
            if ($insert_id) {
                $stock_post_arr = array(
                    "product_id" => $data['product_id'],
                    "product_inventory_id" => $data['product_inventory_id'],
                    "quantity" => 1,
                    "type" => 'debit',
                    "action_for" => "debit_from_increasing_quantity_in_cart"
                );
                $this->stock_management_model->add_stock($stock_post_arr);
                return "added";
            } else {
                return "error";
            }
        } else if ($previous_cart_row->id != "") {
            $this->db->set('comment', $data['comment']);
            $this->db->set('updated_at', time());
            $this->db->where('product_id', $data['product_id']);
            $this->db->where('product_inventory_id', $data['product_inventory_id']);
            $this->db->where('designers_and_analysts_id', $data['designers_and_analysts_id']);
            $update = $this->db->update('fashion_designer_cart');
            if ($update) {
                return "updated";
            } else {
                return "error";
            }
            return "already_added";
        } else {
            return "fail";
        }
    }

    function check_product_already_added_to_cart($product_id, $product_inventory_id, $fashion_designer_id) {
        $this->db->select('id,quantity,unit_price');
        $this->db->from('fashion_designer_cart');
        $this->db->where('product_id', $product_id);
        $this->db->where('product_inventory_id', $product_inventory_id);
        $this->db->where('designers_and_analysts_id', $fashion_designer_id);
        $this->db->where('cart_status', 'active');
        $this->db->where('status', 1);
        $row = $this->db->get()->row();
        if ($row->id != "") {
            return $row;
        } else {
            return false;
        }
    }

    function check_cart_products_stock_exceeded_for_product($product_inventory_id, $quantity) {
        $current_stock = $this->get_current_product_stock($product_inventory_id);
        $updated_balance = $quantity;
        if ($current_stock <= 0 || $current_stock < $updated_balance) {
            return "out_of_stock";
        }
    }

    function get_current_product_stock($product_inventory_id) {
        $this->db->select('id,quantity');
        $this->db->from('product_inventory');
        $this->db->where('id', $product_inventory_id);
        $this->db->where('status', 1);
        $stock = $this->db->get()->row()->quantity;
        if ($stock > 0) {
            return $stock;
        }
        return 0;
    }

    function get_product_price($product_id, $inventory_id) {
        $price = $this->db->select('price')->get_where('product_inventory', ['id' => $inventory_id,])->row()->price;
        return $price;
    }

    function get_cart() {
        $this->db->select('id,product_id,product_inventory_id,unit_price,quantity,subtotal,subscription_transaction_id,comment');
        $this->db->from('fashion_designer_cart');
        $this->db->where('designers_and_analysts_id', $this->fashion_id);
        $this->db->where('cart_status', 'active');
        $this->db->where('status', 1);
        $result = $this->db->get()->result();
        $total_amount = 0;
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $item->quantity = (int) $item->quantity;
                $item->product_details = $this->db->select('title,product_id,categories_id')->get_where('products', ['id' => $item->product_id])->row();
                $item->image = base_url() . $this->get_product_image($item->product_id);
                $total_amount += $item->subtotal;
                $item->stock = $this->get_current_product_stock($item->product_inventory_id);
                $product_inventory_row = $this->db->select('size_id')->get_where('product_inventory', ['id' => $item->product_inventory_id])->row();
                $item->size = $this->db->get_where('sizes', ['id' => $product_inventory_row->size_id])->row()->title;
                $item->category_name = $this->db->get_where('categories', ['id' => $item->product_details->categories_id])->row()->name;
            }
            $final_amount = $total_amount;
            return [
                "result" => $result,
                "sub_total" => $total_amount,
                "final_amount_including_taxes" => round($final_amount, 2)
            ];
        }
        return [
            "result" => [],
            "sub_total" => 0,
            "final_amount_including_taxes" => 0
        ];
    }

    function get_product_image($product_id) {
        return $this->db->get_where('products_images', ['product_id' => $product_id])->row()->image;
    }

    function delete_cart_item($id) {
        $cart_row = $this->db->get_where('fashion_designer_cart', ['id' => $id])->row();
        $this->db->where('id', $id);
        $delete = $this->db->delete('fashion_designer_cart');
        if ($delete) {
            $stock_post_arr = array(
                "product_id" => $cart_row->product_id,
                "product_inventory_id" => $cart_row->product_inventory_id,
                "quantity" => $cart_row->quantity,
                "type" => 'credit',
                "action_for" => "credit_from_decreasing_quantity_in_cart"
            );
            $this->stock_management_model->add_stock($stock_post_arr);
            return true;
        } else {
            return false;
        }
    }

    function update_cart_quantity($quantity, $cart_id, $type) {
        $product_id = $this->db->get_where('fashion_designer_cart', ['id' => $cart_id])->row()->product_id;
        $product_inventory_id = $this->db->get_where('fashion_designer_cart', ['id' => $cart_id])->row()->product_inventory_id;
        $unit_price = $this->get_product_price($product_id, $product_inventory_id);
        $sub_total = $unit_price * $quantity;
        $this->db->set('subtotal', $sub_total);
        $this->db->set('quantity', $quantity);
        $this->db->where('id', $cart_id);
        $update = $this->db->update('fashion_designer_cart');

        if ($update) {
            if ($type == "add") {
                $updated_type = "debit";
                $action_for = "debit_from_increasing_quantity_in_cart";
            } else if ($type == "remove") {
                $updated_type = "credit";
                $action_for = "credit_from_reducing_quantity_in_cart";
            }
            $stock_post_arr = array(
                "product_id" => $product_id,
                "product_inventory_id" => $product_inventory_id,
                "quantity" => 1,
                "type" => $updated_type,
                "action_for" => $action_for
            );
            $response = $this->stock_management_model->add_stock($stock_post_arr);
            if ($response) {
                return true;
            }
        } else {
            return false;
        }
    }

    function get_cart_comment($product_id, $product_inventory_id, $unique_id) {
        $subscription_transaction_id = $this->db->get_where('subscriptions_transactions', ['unq_id' => $unique_id])->row()->id;
        $this->db->select('comment');
        $this->db->from('fashion_designer_cart');
        $this->db->where('product_id', $product_id);
        $this->db->where('product_inventory_id', $product_inventory_id);
        $this->db->where('subscription_transaction_id', $subscription_transaction_id);
        $this->db->where('cart_status', 'active');
        $comment = $this->db->get()->row()->comment;
        return $comment;
    }

}
