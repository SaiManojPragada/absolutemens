<?php

class Vendor_category_timings_model extends CI_Model {

    private $table_name = "restaurant_categories_time_slots";

    function get($restaurant_categories_id) {
        $array = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $data = [];
        foreach ($array as $week) {
            $i = 1;
            while ($i <= 3) {
                $this->db->where("week_name", $week);
                $this->db->where("status", 1);
                $this->db->where("restaurant_categories_id", $restaurant_categories_id);
                $this->db->where("schedule", $i);
                $row = $this->db->get($this->table_name)->row();
                if ($row) {
                    $row->display_week_name = $row->week_name == date("l") ? "Today" : $row->week_name;
                    $row->display_timings = date("h:i A", strtotime(date("Y-m-d " . $row->from_time))) . " - " . date("h:i A", strtotime(date("Y-m-d " . $row->to_time)));
                    unset($row->created_at);
                    unset($row->updated_at);
                    unset($row->status);
                    $data[] = $row;
                } else {
                    $data[] = (object) array(
                                "restaurant_categories_id" => $restaurant_categories_id,
                                "week_name" => $week,
                                "schedule" => $i,
                                //"from_time" => "10:00:00",
                                "from_time" => "",
                                //"to_time" => "21:00:00"
                                "to_time" => ""
                    );
                }
                $i++;
            }
        }
        return $data;

        /*

          $this->db->where('status', 1);
          $this->db->where("restaurant_categories_id", $restaurant_categories_id);
          $data = $this->db->get($this->table_name)->result();
          if ($data) {
          return $data;
          } else {
          return [];
          } */
    }

    function add($data = []) {

        $this->db->where("restaurant_categories_id", $data[0]["restaurant_categories_id"]);
        $this->db->delete($this->table_name);

        for ($i = 0; $i < count($data); $i++) {
            $this->db->set($data[$i]);
            $response = $this->db->insert($this->table_name);
        }

        if ($response) {
            $id = $this->db->insert_id();
            $this->db->where("id", $id);
            $row = $this->db->get($this->table_name)->row();
            $this->vendor_food_item_timings_model->update_products_timing_related_to_category($row->restaurant_categories_id);
        }
        return $response;
    }

    function delete($id) {
        $this->db->select("from_time, to_time, restaurant_categories_id");
        $this->db->where("id", $id);
        $row = $this->db->get($this->table_name)->row();

        //delete perticular timing for all products 
        $this->db->select("id");
        $this->db->where("restaurant_categories_id", $row->restaurant_categories_id);
        $data = $this->db->get("restaurant_food_items")->result();

        foreach ($data as $item) {
            $this->db->where("restaurant_food_items_id", $item->id);
            $this->db->where("from_time", $row->from_time);
            $this->db->where("to_time", $row->to_time);
            $this->db->delete("restaurant_food_items_timings");
        }

        $this->db->where("id", $id);
        $response = $this->db->delete($this->table_name);
        return $response;
        /*
          $this->db->where("id", $id);
          $this->db->set("status", 0);
          $this->db->set("updated_at", THIS_DATE_TIME);
          $response = $this->db->update($this->table_name);
          return $response; */
    }

}
