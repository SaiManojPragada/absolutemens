<?php

class Vendor_food_item_addons_model extends CI_Model {

    private $table_name = "restaurant_food_item_addons";

    function get($restaurant_food_items_id, $availability_status = null) {
        //$this->db->order_by("priority");
        $this->db->where('status', 1);
        $this->db->where("restaurant_food_items_id", $restaurant_food_items_id);
        if (isset($availability_status)) {
            $this->db->where("available", $availability_status);
        }
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            foreach ($data as $item) {
                $item->is_mandatory = $item->is_mandatory ? $item->is_mandatory : '0';
                if ($item->is_mandatory) {
                    $item->is_mandatory_text = "Yes";
                    $item->is_mandatory_class = "text-success";
                } else {
                    $item->is_mandatory_text = "No";
                    $item->is_mandatory_class = "text-danger";
                }

                if ($item->available) {
                    $item->available = true;
                } else {
                    $item->available = false;
                }
//              $item->image = FILE_UPLOAD_FOLDER_IMG_PATH . $item->image;
//              $item->category_name = $this->vendor_categories_model->get_restaurant_category_name($item->restaurant_categories_id);
            }
            return $data;
        } else {
            return [];
        }
    }

    function add($data, $vendor_id) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);

        $this->db->set("verification_status", "Pending");
        $this->db->where("id", $data["restaurant_food_items_id"]);
        $this->db->update("restaurant_food_items");

        $this->cache_model->clear_restaurant_cache($vendor_id);

        return $response;
    }

    function update($data, $id, $vendor_id) {
        $this->db->trans_begin();

        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->update($this->table_name);

        if (!$this->session->userdata("admin_control")) {
            if ($this->db->affected_rows() == 1) {
                $this->db->set("verification_status", "Pending");
                $this->db->where("id", $data["restaurant_food_items_id"]);
                $this->db->update("restaurant_food_items");
            }
        }
        $this->db->set("updated_at", time());
        $this->db->where("id", $id);
        $this->db->update($this->table_name);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $this->cache_model->clear_restaurant_cache($vendor_id);
        }
        return true;
    }

    function delete($id, $restaurants_id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        $this->cache_model->clear_restaurant_cache($restaurants_id);
        return $response;
    }

    function update_food_addon_sold_out_or_available($vendor_id, $restaurant_food_addon_id, $available) {
        if ($available == "false" || $available == false) {
            $available = 0;
        } else {
            $available = 1;
        }
        $this->db->set("available", $available);

        $this->db->where("id", $restaurant_food_addon_id);
        $this->db->update("restaurant_food_item_addons");

        $this->cache_model->clear_restaurant_cache($vendor_id);
        return true;
    }

}
