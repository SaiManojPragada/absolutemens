<?php

class Vendor_model extends CI_model {

    function update_profile($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        if ($this->db->update("vendors")) {
            return true;
        } else {
            return false;
        }
    }

    function is_username_exists($username) {
        $data = $this->db->get_where("restaurants", ['mobile' => $username]);
        if ($data->num_rows() == 0) {
            return false;
        }
        $data = $data->row();
        return $data->id;
    }

    function check_for_current_password_ok($vendor_id, $password) {
        $ud = $this->get_vendor_details($vendor_id);
        if ($ud->password == md5($password . $ud->salt)) {
            return true;
        } else {
            return false;
        }
    }

    function get_vendor_details($vendor_id) {
        $vendor_details = $this->db->get_where("vendors", ['id' => $vendor_id])->row();
        if (!$vendor_details) {
            return false;
        }

//        foreach ($vendor_details as $key => $item) {
//            if (!isset($item)) {
//                $vendor_details->{$key} = "";
//            }
//        }
//        $vendor_details->location_name = $this->locations_model->get_location_name($vendor_details->locations_id);
//        $vendor_details->city_name = $this->locations_model->get_city_name($vendor_details->cities_id);
//        $vendor_details->country_name = $this->locations_model->get_country_name($vendor_details->countries_id);
//
//        $vendor_details->display_address = $vendor_details->land_mark . "," . $vendor_details->address . ", " . $vendor_details->location_name . ", " . $vendor_details->city_name . "," . $vendor_details->country_name . ", " . $vendor_details->zipcode;
//
//        if ($vendor_details->display_image) {
//            $vendor_details->display_image = STORE_IMG_PATH . $vendor_details->id . "/" . $vendor_details->display_image;
//        } else {
//            $vendor_details->display_image = STORE_DEFAULT_IMAGE;
//            //$item->display_image = FILE_UPLOAD_FOLDER_IMG_PATH . $this->db->get("default_images")->row()->restaurant_default_image;
//        }
//
//
//
//        $vendor_details->minimum_order_amount = (int) $vendor_details->minimum_order_amount;
//        $vendor_details->delivery_radius = (int) $vendor_details->delivery_radius;
//
//        $this->db->select("id");
//        $this->db->where("is_pharmacy", 1);
//        $cids = $this->db->get("cuisine_types")->result();
//
//        $this->db->select("cuisine_types_ids");
//        $this->db->where("id", $vendor_id);
//        $rcids = $this->db->get("restaurants")->row()->cuisine_types_ids;
//
//        $rcids = explode(",", $rcids);
//        $vendor_details->attachment_required = 0;
//
//        foreach ($cids as $item) {
//            if (in_array($item->id, $rcids)) {
//                $vendor_details->attachment_required = 1;
//            }
//        }
//
//        $vendor_details->rating = (float) $this->restaurants_model->get_restaurant_rating($vendor_details->id);
//        $vendor_details->num_of_ratings = (int) $this->restaurants_model->get_restaurant_reviews_count($vendor_details->id);
//
//        $vendor_details->marketing_executive_name = SITE_TITLE;
//        $vendor_details->marketing_executive_mobile = "04435530084";
//
//        $this->db->where("id", $vendor_details->marketing_executives_id);
//        $marketing_person_row = $this->db->get("marketing_executives")->row();
//
//        if ($marketing_person_row) {
//            $vendor_details->marketing_executive_name = $marketing_person_row->name;
//            $vendor_details->marketing_executive_mobile = $marketing_person_row->mobile;
//        }

        return $vendor_details;
    }

    function check_for_user_logged() {

        if (@$_GET["vendor_id"] && $_GET["mode"] == "admin_control") {

            $data = $this->db->get_where("vendors", ["id" => $_GET["vendor_id"]])->row();

            if ($data->access_token == $_GET["access_token"]) {

                $this->session->set_userdata("vendor_id", $_GET["vendor_id"]);
                $this->session->set_userdata("vendor_logged", true);
                $this->session->set_userdata("admin_control", true);

                return true;
            }
        } else if (@$_GET["vendor_id"] && $_GET["mode"] == "retail_chain_control") {
            $data = $this->db->get_where("vendors", ["id" => $_GET["vendor_id"]])->row();

            if ($data->access_token == $_GET["access_token"]) {

                $this->session->set_userdata("vendor_id", $_GET["vendor_id"]);
                $this->session->set_userdata("vendor_logged", true);
                $this->session->set_userdata("retail_chain_control", true);

                return true;
            }
        }

        $vendor_id = $this->get_logged_vendor_id();
        if ($this->input->get_post("access_token")) {
            $access_token = $this->input->get_post("access_token");
            $this->db->select("id");
            $this->db->where("access_token", $access_token);
            $this->db->where("status", 1);
            $vendor = $this->db->get("vendors")->row();
            if ($vendor) {
                $this->session->set_userdata("vendor_id", $vendor->id);
                return $vendor->id;
            }
        }
        if ($this->session->userdata("vendor_logged")) {
            if ($this->get_vendor_details($vendor_id)->status == 0) {
                $this->session->unset_userdata("vendor_id");
                $this->session->unset_userdata("vendor_logged");
                return false;
            }
            return true;
        } else {
            $this->session->unset_userdata("vendor_id");
            $this->session->unset_userdata("vendor_logged");
            $this->session->set_flashdata("timeout");
            return false;
        }
    }

    function get_logged_vendor_id() {
//        if ($this->input->get_post("access_token") || $this->input->get_post("store_access_token")) {
//            if ($this->input->get_post("store_access_token")) {
//                $access_token = $this->input->get_post("store_access_token");
//            } else {
//                $access_token = $this->input->get_post("access_token");
//            }
//            $this->db->select("id");
//            $this->db->where("access_token", $access_token);
//            $this->db->where("status", 1);
//            $vendor = $this->db->get("vendors")->row();
//            if ($vendor->id) {
//                $this->session->set_userdata("vendor_id", $vendor->id);
//                return $vendor->id;
//            } else {
//                show_forbidden();
//            }
//        }
        return $this->session->userdata("vendor_id");
    }

    // get city of the vendor
    function get_city_by_vendor_id($vendor_id) {
        $this->db->select("cities_id");
        $this->db->where("id", $vendor_id);
        $vendor = $this->db->get("vendors")->row();
        return $vendor->cities_id;
    }

    function do_logout() {
        $this->session->unset_userdata("vendor_id");
        $this->session->unset_userdata("vendor_logged");
        $this->session->set_flashdata("timeout");
        $this->session->unset_userdata("admin_control");
        delete_cookie("token");
        unset($_COOKIE["token"]);
        unset($_COOKIE["last_login"]);
        return true;
    }

    function update_password($vendor_id, $password) {
        $salt = rand(12551512, 9946546999);
        $enc_pwd = md5($password . $salt);
        $this->db->set("password", $enc_pwd);
        $this->db->set("salt", $salt);
        $this->db->where("id", $vendor_id);
        $res = $this->db->update("vendors");

        if (!$res) {
            return false;
        }
        $customer_details = $this->get_vendor_details($vendor_id);
        send_message("Your password has been updated for " . SITE_TITLE, $customer_details->mobile, 1407161883375533827);
        //send_message
        return true;
    }

    function get_minimum_order_value($vendor_id) {
        $this->db->select("minimum_order_amount");
        $this->db->where("id", $vendor_id);
        $minimum_order_amount = $this->db->get("restaurants")->row()->minimum_order_amount;
        return $minimum_order_amount;
    }

}
