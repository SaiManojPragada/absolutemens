<?php
class Vendor_taxes_model extends CI_Model{

	private $table_name = "restaurants_tax_slabs";

	function get($restaurants_id){
		$this->db->where('status',1);
		$this->db->where("restaurants_id", $restaurants_id);
		$data = $this->db->get($this->table_name)->result();
		if($data){
			foreach($data as $item){
				if($item->tax_type=="Percentage"){
					$item->display_tax_type = $item->tax_amount." %";
				}else if($item->tax_type=="Flat"){
					$item->display_tax_type = "Rs. " .$item->tax_amount;
				}
				
				if($item->tax_status="Active"){
					$item->tax_status_bootstrap_class = "text-success";
				}else{
					$item->tax_status_bootstrap_class = "text-danger";
				}
			}
			return $data;
		}else{
			return [];
		}
	}

	function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
        return $response;
    }

    function update($data, $id) {
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        return $response;
    }
}
