<?php

class Vendor_products_model extends CI_Model {

    private $table_name = "products";

    function get_stock_list($filters) {
        $this->db->select('id,type,quantity,comment,created_at,balance,action_for');
        $this->db->from('stock');
        $this->db->where('product_inventory_id', $filters['product_inventory_id']);
        $this->db->where('status', 1);
        if ($filters['type'] != "") {
            $this->db->where('type', $filters['type']);
        }
        if ($filters['from_date'] != "" && $filters['to_date'] != "") {
            $from_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['from_date']));
            $to_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['to_date']));
            $this->db->where('date >=', $from_date);
            $this->db->where('date <=', $to_date);
        }
        if (isset($filters['limit']) && isset($filters['start'])) {
            $this->db->limit($filters["limit"], $filters["start"]);
        }
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $item) {
                $item->created_at = date('d-m-Y H:i:s a', $item->created_at);
            }
            return $result;
        } else {
            return [];
        }
    }

    function get_stock_list_count($filters) {
        $this->db->select('id');
        $this->db->from('stock');
        $this->db->where('product_inventory_id', $filters['product_inventory_id']);
        $this->db->where('status', 1);
        if ($filters['type'] != "") {
            $this->db->where('type', $filters['type']);
        }
        if ($filters['from_date'] != "" && $filters['to_date'] != "") {
            $from_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['from_date']));
            $to_date = date('Y-m-d', convert_js_timer_to_php_timer($filters['to_date']));
            $this->db->where('date >=', $from_date);
            $this->db->where('date <=', $to_date);
        }
        $rows = $this->db->count_all_results();
        return $rows;
    }

    function get_product_details($product_inventory_id) {
        $product_inventory_row = $this->db->select('size_id,price,product_id')->get_where('product_inventory', ['id' => $product_inventory_id])->row();
        $product_details = $this->db->select('title,categories_id,sub_categories_id,brand')->get_where('products', ['id' => $product_inventory_row->product_id])->row();
        $product_details->category = $this->db->get_where('categories', ['id' => $product_details->categories_id])->row()->name;
        $product_details->sub_category = $this->db->get_where('sub_categories', ['id' => $product_details->sub_categories_id])->row()->title;
        $product_details->size = $this->db->select('title')->get_where('sizes', ['id' => $product_inventory_row->size_id])->row()->title;
        $product_details->price = $product_inventory_row->price;
        return $product_details;
    }

}
