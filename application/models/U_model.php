<?php

class U_model extends CI_Model {

    function select_styles($styles) {
        $this->db->where("id IN (" . rtrim($styles, ',') . ")");
        $result = $this->db->get('styles')->result();
        return $result;
    }

    function get_orders($user_id) {
        $this->db->order_by("id", "desc");
        $this->db->where("status", 1);
        $this->db->where("customers_id", $user_id);
        $data = $this->db->get("subscriptions_transactions")->result();
        return $data;
    }

    function get_data($table, $where = null, $limit = "", $order = "", $order_type = "") {
        if (!empty($limit)) {
            $this->db->limit($limit);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        if (empty($order)) {
            $order = "id";
        }
        if (empty($order_type)) {
            $order_type = "desc";
        }
        $this->db->order_by($order, $order_type);
        $this->db->where("status", true);
        $data = $this->db->get($table)->result();
        return $data;
    }

    function get_nostatcheck_data($table, $where = null, $limit = "") {
        if (!empty($limit)) {
            $this->db->limit($limit);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by("id", "desc");
        $data = $this->db->get($table)->result();
        return $data;
    }

    function get_nostatcheck_row($table, $where = null, $limit = "") {
        if (!empty($limit)) {
            $this->db->limit($limit);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by("id", "desc");
        $data = $this->db->get($table)->row();
        return $data;
    }

    function get_data_row($table) {
        $this->db->order_by("id", "desc");
        $data = $this->db->get($table)->row();
        return $data;
    }

    function get_data_conditon_row($table, $where = null, $limit = "") {
        if (!empty($limit)) {
            $this->db->limit($limit);
        }
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by("id", "desc");
        $this->db->where("status", true);
        $data = $this->db->get($table)->row();
        return $data;
    }

    function post_data($table, $data) {
        $res = $this->db->insert($table, $data);
        return $res;
    }

    function update_data($table, $data, $where) {
        $this->db->set($data);
        $this->db->where($where);
        $res = $this->db->update($table);
        return true;
    }

    function delete_data($table, $where) {
        $this->db->where($where);
        $res = $this->db->delete($table);
        return $res;
    }

    function send_email($to, $subject, $message) {
        $this->db->limit(1);
        $site_data = $this->db->get("site_settings")->row();
        $this->load->library('email');
        $config = array();
        $config['protocol'] = "smtp";
        $config['smtp_host'] = $site_data->smtp_host;
        $config['smtp_user'] = $site_data->smtp_user_name;
        $config['smtp_pass'] = $site_data->smtp_password;
        $config['smtp_port'] = $site_data->smtp_port;
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = true;
        $this->email->initialize($config);

        $from = $site_data->smtp_user_name;

        $this->email->from($from, $site_data->site_name . ' Support');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $send = $this->email->send();
        if (!$send) {
            echo $this->email->print_debugger();
            die();
        }
        return true;
    }

    function build_contact_message_template($data, $site_data) {
        $ref_data = array(
            "site_data" => $site_data,
            "title" => "<b>" . $data['name'] . "</b> has Contacted You.",
            "message" => "<h5> Message : </h5><br>"
            . "<div style='margin-left: 4%'><p style='overflow-wrap: normal;
overflow-wrap: break-word;
overflow-wrap: anywhere;'>" . $data['message'] . "</p></div><br><br>"
            . "<h5>Contact Details : </h5><br>"
            . "<div style='margin-left: 4%'><strong>E-Mail : </strong>" . $data['email'] . "<br>"
            . "<strong>Phone Number : </strong>" . $data['phone'] . "<br>"
            . "<strong>Request Type : </strong>" . $data['request_type'] . "</div><br><br>"
        );
        $message = $this->load->view("templates/email_template", $ref_data, true);
        return $message;
    }

    function get_categories_with_sizes($preferences = NULL) {

        $this->db->select('id,name');
        $this->db->from('categories');
        $this->db->where('status', 1);
        $result = $this->db->get()->result();
        if ($result[0]->id != "") {
            foreach ($result as $key => $item) {
                $item->sizes = $this->db->select('id,title')->get_where('sizes', ['category_id' => $item->id])->result();
                foreach ($item->sizes as $s) {
                    $category_id = $item->id;

                    if ($preferences->$category_id == $s->id) {
                        $s->is_selected = "yes";
                    } else {
                        $s->is_selected = "no";
                    }
                }
            }
            return $result;
        }
        return [];
    }

    function get_total($table, $where, $column) {
        $this->db->select("SUM($column) as $column");
        if (!empty($where)) {
            $this->db->where($where);
        }
        $res = $this->db->get("$table")->row();
        return $res;
    }

}
