<?php

class Vendor_food_item_timings_model extends CI_Model {

    private $table_name = "restaurant_food_items_timings";

    function get($restaurant_food_items_id) {

        $array = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $data = [];
        foreach ($array as $week) {
            $i = 1;
            while ($i <= 3) {
                $this->db->where("week_name", $week);
                $this->db->where("status", 1);
                $this->db->where("restaurant_food_items_id", $restaurant_food_items_id);
                $this->db->where("schedule", $i);
                $row = $this->db->get($this->table_name)->row();
                if ($row) {
                    $row->display_week_name = $row->week_name == date("l") ? "Today" : $row->week_name;
                    $row->display_timings = date("h:i A", strtotime(date("Y-m-d " . $row->from_time))) . " - " . date("h:i A", strtotime(date("Y-m-d " . $row->to_time)));
                    unset($row->created_at);
                    unset($row->updated_at);
                    unset($row->status);
                    $data[] = $row;
                } else {
                    $data[] = (object) array(
                                "restaurant_food_items_id" => $restaurant_food_items_id,
                                "week_name" => $week,
                                "schedule" => $i,
                                "from_time" => "",
                                "to_time" => ""
                    );
                }
                $i++;
            }
        }
        return $data;

        /* $this->db->where('status', 1);
          $this->db->where("restaurant_food_items_id", $restaurant_food_items_id);
          $data = $this->db->get($this->table_name)->result();
          if ($data) {
          return $data;
          } else {
          return [];
          } */
    }

    function add($data, $restaurants_id = null) {
        for ($i = 0; $i < count($data); $i++) {

            $this->db->where("week_name", $data[$i]["week_name"]);
            $this->db->where("schedule", $data[$i]["schedule"]);
            $this->db->where("restaurant_food_items_id", $data[$i]["restaurant_food_items_id"]);
            $cnt = $this->db->get("restaurant_food_items_timings")->row();
            if ($cnt) {
                $this->db->set($data[$i]);
                $this->db->where("week_name", $data[$i]["week_name"]);
                $this->db->where("restaurant_food_items_id", $data[$i]["restaurant_food_items_id"]);
                $this->db->where("schedule", $data[$i]["schedule"]);
                $this->db->update($this->table_name);
            } else {
                $this->db->set($data[$i]);
                $this->db->insert($this->table_name);
            }
        }

        if ($restaurants_id) {
            $this->cache_model->clear_restaurant_cache($restaurants_id);
        }

        return true;

        $this->db->set($data);
        $this->db->set("created_at", THIS_DATE_TIME);
        $this->db->insert($this->table_name);
        $id = $this->db->insert_id();




        return $id;
    }

    function delete($id, $restaurants_id) {
        $this->db->where("id", $id);
        $response = $this->db->delete($this->table_name);
        $this->cache_model->clear_restaurant_cache($restaurants_id);
        return $response;
        /*
          $this->db->where("id", $id);
          $this->db->set("status", 0);
          $this->db->set("updated_at", THIS_DATE_TIME);
          $response = $this->db->update($this->table_name);
          return $response; */
    }

    function update_products_timing_related_to_category($restaurant_categories_id) {

        $category_timings = $this->vendor_category_timings_model->get($restaurant_categories_id);

        $this->db->select("id as restaurant_food_items_id");
        $this->db->where("restaurant_categories_id", $restaurant_categories_id);
        $data = $this->db->get("restaurant_food_items")->result();
        foreach ($data as $item) {
            $item->restaurant_food_items_id;
            $timings_arr = [];
            foreach ($category_timings as $cit) {
                $timings_arr[] = [
                    "restaurant_food_items_id" => $item->restaurant_food_items_id,
                    "week_name" => $cit->week_name,
                    "schedule" => $cit->schedule,
                    "from_time" => $cit->from_time ? $cit->from_time : null,
                    "to_time" => $cit->to_time ? $cit->to_time : null,
                    "created_at" => THIS_DATE_TIME
                ];
            }
            $this->add($timings_arr);
        }


        $this->db->select("restaurants_id");
        $this->db->where("id", $restaurant_categories_id);
        $this->db->limit(1);
        $restaurants_id = $this->db->get("restaurant_categories")->row()->restaurants_id;

        $this->cache_model->clear_restaurant_cache($restaurants_id);
        return true;
    }

    function delete_timings_for_category_of_food_items($restaurant_categories_id) {
        $this->db->select("id as restaurant_food_items_id");
        $this->db->where("restaurant_categories_id", $restaurant_categories_id);
        $data = $this->db->get("restaurant_food_items")->result();
        foreach ($data as $item) {
            $this->db->where("restaurant_food_items_id", $item->restaurant_food_items_id);
            $this->db->delete("restaurant_food_items_timings");
        }


        $this->db->select("restaurants_id");
        $this->db->where("id", $restaurant_categories_id);
        $this->db->limit(1);
        $restaurants_id = $this->db->get("restaurant_categories")->row()->restaurants_id;

        $this->cache_model->clear_restaurant_cache($restaurants_id);

        return true;
    }

    function copy_timings_from_category($restaurant_food_items_id, $restaurant_categories_id) {
        $this->db->select("from_time, to_time");
        $this->db->where("restaurant_categories_id", $restaurant_categories_id);
        $data = $this->db->get("restaurant_categories_time_slots")->result();
        foreach ($data as $item) {
            $this->db->set("from_time", $item->from_time);
            $this->db->set("to_time", $item->to_time);
            $this->db->set("restaurant_food_items_id", $restaurant_food_items_id);
            $this->db->insert("restaurant_food_items_timings");
        }

        $this->db->select("restaurants_id");
        $this->db->where("id", $restaurant_categories_id);
        $this->db->limit(1);
        $restaurants_id = $this->db->get("restaurant_categories")->row()->restaurants_id;
        $this->cache_model->clear_restaurant_cache($restaurants_id);
    }

}
