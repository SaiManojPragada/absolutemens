<?php

class Stock_management_model extends CI_Model {

    function add_stock($data) {
        $this->db->trans_begin();
        $product_stock = $this->get_current_product_stock($data['product_inventory_id']);
        if ($data['type'] == "credit") {
            $updated_product_stock = (int) $product_stock + (int) $data['quantity'];
        } else if ($data['type'] == "debit") {
            $updated_product_stock = (int) $product_stock - (int) $data['quantity'];
        }
        $this->db->set('quantity', $updated_product_stock);
        $this->db->where('id', $data['product_inventory_id']);
        $update_inventory = $this->db->update('product_inventory');
        if ($update_inventory) {
            $stock_arr = [
                "product_id" => $data['product_id'],
                "product_inventory_id" => $data['product_inventory_id'],
                "quantity" => $data['quantity'],
                "type" => $data['type'],
                "comment" => $data['quantity'] . " stock " . $data['type'] . "ed on " . date('d-m-Y'),
                "date" => date('Y-m-d'),
                "balance" => $updated_product_stock,
                "action_for" => $data['action_for'],
                "created_at" => time()
            ];
            $this->db->insert('stock', $stock_arr);
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function get_current_product_stock($product_inventory_id) {
        $this->db->select('id,quantity');
        $this->db->from('product_inventory');
        $this->db->where('id', $product_inventory_id);
        $this->db->where('status', 1);
        $stock = $this->db->get()->row()->quantity;
        if ($stock > 0) {
            return $stock;
        }
        return 0;
    }

}
