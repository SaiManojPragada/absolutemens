<?php

class Vendor_bank_details_model extends CI_Model {

    private $table_name = "vendor_bank_details";
    private $colums_names = "id,vendors_id,account_name,account_number,bank_name,branch,ifsc_code,contact_id,created_at,updated_at,status";

    function get($restaurants_id) {
        $this->db->select($this->colums_names);
        $this->db->where('status', 1);
        $this->db->where("vendors_id", $restaurants_id);
        $data = $this->db->get($this->table_name)->result();
        if ($data) {
            return $data;
        } else {
            return [];
        }
    }

    function add($data) {
        $this->db->set($data);
        $this->db->set("created_at", time());
        $response = $this->db->insert($this->table_name);
//        if ($response) {
//            //once bank account added then update contact id and benificiary id
//            $restaurans_id = $data["vendors_id"];
////            $this->add_or_update_contact($restaurans_id, $this->db->insert_id());
//        }
        return $response;
    }

    function add_or_update_contact($restaurans_id, $bank_details_id) {
        $this->db->where("id", $bank_details_id);
        $bank_details = $this->db->get("vendor_bank_details")->row();
        //if (!$bank_details->contact_id && !$bank_details->beneficiaries_id) {
        $this->db->select("business_name, owner_name, contact_number, contact_email");
        $this->db->where("id", $restaurans_id);
        $row = $this->db->get("vendors")->row();

        if (!$row) {
            return;
        }

        $data = [
            "name" => $row->restaurant_name,
            "phone" => $row->mobile,
            "email" => $row->contact_email,
        ];

        $contact_id = $this->easebuzz_payout_model->add_contact($data);

        $this->db->set($data);
        $this->db->set("contact_id", $contact_id);
        $this->db->set("table_primary_key_id", $restaurans_id);
        $this->db->set("table_name", "restaurants");
        $this->db->insert("easebuzz_contacts");

        //Update contact id
        $this->db->set("contact_id", $contact_id);
        $this->db->where("id", $bank_details_id);
        $this->db->update("restaurant_bank_details");

        //Now update benificiary id
        $benificary_arr = [
            "contact_id" => $contact_id,
            "beneficiary_name" => $bank_details->account_name,
            "account_number" => $bank_details->account_number,
            "ifsc" => $bank_details->ifsc_code
        ];

        $response = $this->easebuzz_payout_model->add_beneficiaries($benificary_arr);

        if (!is_array($response)) {

            if ($response == "Invalid IFSC." || $response == "Only alphanumeric values are allowed for bank account number.") {
                $this->db->where("id", $bank_details_id);
                $this->db->delete("restaurant_bank_details");
                return;
            }

            $arr = array('err_code' => "invalid",
                "title" => $response,
                "message" => $response,
                "data" => $benificary_arr
            );
            echo json_encode($arr);
            die;
        }

        if (isset($response["beneficiary_id"]) && isset($response["contact_id"])) {
            $this->db->set("contact_id", $response["contact_id"]);
            $this->db->set("beneficiaries_id", $response["beneficiary_id"]);
            $this->db->where("id", $bank_details_id);
            $this->db->update("restaurant_bank_details");
        }
        // }
    }

    function update($data, $id) {
        $data['status'] = false;
        $this->db->set($data);
        $this->db->where("id", $id);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
//        $restaurans_id = $data["restaurants_id"];
//        $this->add_or_update_contact($restaurans_id, $id);
        return $response;
    }

    function delete($id) {
        $this->db->where("id", $id);
        $this->db->set("status", 0);
        $this->db->set("updated_at", time());
        $response = $this->db->update($this->table_name);
        return $response;
    }

    function get_bank_details_by_account($account_number, $currentID = NULL) {
        $this->db->select($this->colums_names);
        $this->db->from("restaurant_bank_details");
        $this->db->where("account_number", $account_number);
        if ($currentID) {
            $this->db->where("id !=", $currentID);
        }
        $this->db->order_by("id", "DESC");
        $this->db->limit(1);
        return $this->db->get()->row();
    }

    function get_bank_details_by_restaurant_id($restaurantID) {
        $this->db->select($this->colums_names);
        $this->db->from("restaurant_bank_details");
        $this->db->where("restaurants_id", $restaurantID);
        $this->db->where("status", 1);
        return $this->db->get()->row();
    }

    function get_bank_details_by_id($bankID) {
        $this->db->select($this->colums_names);
        $this->db->from("restaurant_bank_details");
        $this->db->where("id", $bankID);
        return $this->db->get()->row();
    }

}
