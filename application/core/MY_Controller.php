<?php

header("access-control-allow-origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class MY_Controller extends CI_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->data = array();
        /* $config['protocol'] = 'smtp';
          $config['charset'] = 'utf-8';
          $config['smtp_host'] = 'smtp.sendgrid.net';
          $config['smtp_user'] = 'shearcircle';
          $config['smtp_pass'] = 'DIGITEKitinc12';
          $config['smtp_port'] = 587; */
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->load->library('email');

        $this->data["site_property"] = $this->site_model->get_site_properties();
        $this->data["social_media_link"] = $this->site_model->get_social_media_links();
        $this->data["footer_info"] = $this->db->get("footer_info")->row();
        $this->data['policies'] = $this->db->where("status", true)->get("cms_pages")->result();
//        $this->data['services'] = $this->db->get_where("services", ["status" => 1])->result();
//        foreach ($this->data['services'] as $item) {
//            $item->image = base_url() . "uploads/" . $item->image;
//        }
        $url = $_SERVER['REQUEST_URI'];

        if (strpos($url, 'dashboard')) {
            $logged = $this->session->userdata("logged");
            if ($logged) {
                $this->session->unset_userdata("login_err");
                $this->session->unset_userdata("user_unq_id");
                $this->session->unset_userdata("user_id");
                $this->session->unset_userdata("user_email");
                $this->session->unset_userdata("user_mobile");
                $this->session->set_userdata("is_user_logged_in", false);
                $this->session->unset_userdata("is_user_logged_in");
                $this->session->set_userdata("logout_success_admin", true);
                redirect(base_url());
            }
        }
    }

    function generate_random_key($table, $column) {
        $random = "ABM" . rand(000, 9999999);
        $chec = $this->db->where($column, $random)->get($table)->result();
        if ($chec) {
            $this->generate_random_key($table, $column);
            return false;
        } else {
            return $random;
        }
    }

    function generate_random_shop_id($table, $column) {
        $random = "SHP" . rand(000000, 9999999);
        $chec = $this->db->where($column, $random)->get($table)->result();
        if ($chec) {
            $this->generate_random_shop_id($table, $column);
            return false;
        } else {
            return $random;
        }
    }

    function generateDesgId($table, $column, $length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $randomString = "AMFD" . $randomString;
        $chec = $this->db->where($column, $randomString)->get($table)->result();
        if ($chec) {
            $this->generateDesgId($table, $column, $length);
            return false;
        } else {
            return $randomString;
        }
        return $randomString;
    }

    function admin_view($design = null) {
        $this->load->view("admin/includes/header", $this->data);
        //$this->load->view("admin/");        
        $this->load->view("admin/includes/footer", $this->data);
    }

    function front_view($design = null) {
        $this->load->view("includes/header", $this->data);
        $this->load->view($design);
        $this->load->view("includes/footer", $this->data);
    }

    function front_iframe_view($design = null) {
        $this->load->view("includes/header", $this->data);
        $this->load->view("booking_frame/" . $design);
        $this->data["hide_footer"] = "yes";
        $this->load->view("includes/footer", $this->data);
    }

    function profession_view($design = null) {
        $this->load->view("includes/dash_header", $this->data);
        $this->load->view("professional/" . $design);
        $this->load->view("includes/dash_footer", $this->data);
    }

    function check_login_status($user_id) {
        if ($this->user_model->check_user_status($user_id) == false) {
            $this->session->set_flashdata("type", "inactive");
            redirect("login");
        }
        if ($this->user_model->check_email_verified($user_id) == false) {
            $this->session->set_flashdata("type", "email_inactive");
            redirect("login");
        }
        return true;
    }

    function get_user_id($token) {
        $user_id = $this->user_model->get_user_id_by_token($token);
        return $user_id;
    }

    function is_user_logged() {
        if (!get_cookie("token")) {
            redirect("login");
            return false;
        } else {
            $user_id = $this->user_model->get_user_id_by_token(get_cookie("token"));
            return $this->check_login_status($user_id);
        }
    }

    function routing_process() {
        $user_id = $this->user_model->get_user_id_by_token(get_cookie("token"));
        $role = $this->user_model->get_user_role_id($user_id);
        switch ($role) {
            case 1:
                redirect("admin/login");
            case 5:
                redirect("franchise/dashboard");
            case 4:
                redirect("vendor/dashboard");
            case 3:
                redirect("professional/dashboard");
            case 2:
                redirect("customer/dashboard");
        }
    }

    function is_token_required() {
        if (!$this->input->get_post('token')) {
            $arr = array('err_code' => "invalid", "error_type" => "token_required", "message" => "Unauthorized access..");
            echo json_encode($arr);
            die;
        }
    }

    function check_about_subscription($user_id) {
        return true;
        $response = $this->customer_model->check_subscription_plan_is_expired($user_id);
        if ($response == "SUBSCRITPION_IS_REQUIRED") {
            redirect("customer/subscription_plans?err_type=SUBSCRITPION_IS_REQUIRED");
        } else if ($response == "SUBSCRITPION_IS_EXPIRED") {
            redirect("customer/subscription_plans?err_type=SUBSCRITPION_IS_EXPIRED");
        } else {
            return true;
        }
    }

    function vendor_view($design = null, $data = null) {

        if ($this->vendor_model->check_for_user_logged() == true) {
            $vendor_id = $this->session->userdata("vendor_id");
            $this->data["restaurant_details"] = $this->vendor_model->get_vendor_details($vendor_id);
        }
        $this->load->view("vendor/includes/header", $this->data);
        $this->load->view("vendor/" . $design, $data);
        $this->load->view("vendor/includes/footer", $this->data);
    }

    function fashion_view($design = null, $data = null) {

        if ($this->session->userdata("fashion_logged") == true) {
            $fashion_designer_id = $this->session->userdata("fashion_id");
            $this->data["desg_details"] = $this->fashion_designer_model->get_details($fashion_designer_id);
        }
        $this->load->view("fashion_designer_manager/includes/header", $this->data);
        $this->load->view("fashion_designer_manager/" . $design, $data);
        $this->load->view("fashion_designer_manager/includes/footer", $this->data);
    }

    function retail_chain_view($design = null) {

        if ($this->retail_chain_model->check_for_user_logged() == true) {
            $retail_chain_id = $this->session->userdata("retail_chain_id");
            $this->data["retail_chain_details"] = $this->retail_chain_model->get_retail_chain_details($retail_chain_id);
        }

        $this->load->view("retail_chain/includes/header", $this->data);
        $this->load->view("retail_chain/" . $design);
        $this->load->view("retail_chain/includes/footer", $this->data);
    }

}
