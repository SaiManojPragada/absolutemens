<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['committe/(:any)'] = 'committe/index/$1';
$route['Committe/(:any)'] = 'committe/index/$1';

$route['facilities/(:any)'] = 'facilities/index/$1';
$route['Facilities/(:any)'] = 'facilities/index/$1';

$route['events/(:any)'] = 'events/index/$1';
$route['Events/(:any)'] = 'events/index/$1';

$route['events/images/(:any)'] = 'events/images/$1';
$route['Events/images/(:any)'] = 'events/images/$1';

$route['policies/(:any)'] = 'policies/index/$1';
$route['Policies/(:any)'] = 'policies/index/$1';

$route['blog-view/(:any)'] = 'blog_view/index/$1';
$route['Blog-view/(:any)'] = 'blog_view/index/$1';

$route['contact-us'] = 'contact_us/index';
$route['Contact-us'] = 'contact_us/index';

$route['how-it-works'] = 'how_it_works/index';
$route['How-it-works'] = 'how_it_works/index';

$route['package-details/(:any)'] = 'package_details/index/$1';
$route['Package-details/(:any)'] = 'package_details/index/$1';

$route['dashboard/my-orders'] = 'my_orders/index';
$route['dashboard/My-orders'] = 'my_orders/index';

$route['dashboard/my-orders/(:any)'] = 'my_orders/view/$1';
$route['dashboard/My-orders/(:any)'] = 'my_orders/view/$1';

$route['dashboard/sizes'] = 'sizes/index';
$route['dashboard/Sizes'] = 'sizes/index';

$route['dashboard/report'] = 'report/index';
$route['dashboard/Report'] = 'report/index';

$route['dashboard/change-password'] = 'change_password/index';
$route['dashboard/Change-password'] = 'change_password/index';

$route['dashboard/we-shop-for-you'] = 'we_shop_for_you/index';
$route['dashboard/We-shop-for-you'] = 'we_shop_for_you/index';

$route['weshop-details'] = 'weshop_details/index';
$route['Weshop-details'] = 'weshop_details/index';

$route['dashboard/subscription-preferances'] = 'preferances/index';
$route['dashboard/Subscription-preferances'] = 'preferances/index';

$route['package-steps'] = 'package_steps/index';
$route['package-steps/step-one'] = 'package_steps/step_one';
$route['package-steps/step-one/(:any)'] = 'package_steps/step_one/$1';
$route['Package-steps/step-one/(:any)'] = 'package_steps/step_one/$1';
$route['package-steps/step-one/(:any)/(:any)'] = 'package_steps/step_one/$1/$2';
$route['Package-steps/step-one/(:any)/(:any)'] = 'package_steps/step_one/$1/$2';

$route['package-steps/step-two'] = 'package_steps/step_two';
$route['package-steps/step-two/(:any)/(:any)'] = 'package_steps/step_two/$1/$2';
$route['Package-steps/step-two/(:any)/(:any)'] = 'package_steps/step_two/$1/$2';

$route['package-steps/step-three'] = 'package_steps/step_three';
$route['package-steps/step-three/(:any)/(:any)'] = 'package_steps/step_three/$1/$2';
$route['Package-steps/step-three/(:any)/(:any)'] = 'package_steps/step_three/$1/$2';

$route['package-steps/step-four'] = 'package_steps/step_four';
$route['package-steps/step-four/(:any)/(:any)'] = 'package_steps/step_four/$1/$2';
$route['Package-steps/step-four/(:any)/(:any)'] = 'package_steps/step_four/$1/$2';

$route['package-steps/checkout'] = 'package_steps/checkout';
$route['package-steps/checkout/(:any)/(:any)'] = 'package_steps/checkout/$1/$2';
$route['Package-steps/checkout/(:any)/(:any)'] = 'package_steps/checkout/$1/$2';

$route['place-order'] = 'place_order/index';
$route['place-order/(:any)'] = 'place_order/order/$1';
$route['Place-order/(:any)'] = 'place_order/order/$1';

$route['payment/(:any)'] = 'payment/pay/$1';
$route['Payment/(:any)'] = 'payment/pay/$1';

//Error Page

$route['error-404'] = 'error_404/index';

//how_it_works

// $route['search/(:any)'] = 'search/index/$1';
// $route['Search/(:any)'] = 'search/index/$1';