<?php

class Sizes extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }
        $this->data['user_page'] = "sizes";
    }

    function index() {

        $this->data['user_preferences'] = $this->u_model->get_data_conditon_row("customers",
                        array("id" => $this->session->userdata("user_id")))->preferences;
        if ($this->data['user_preferences'] != "") {
            $user_preferences = json_decode($this->data['user_preferences']);
        } else {
            $user_preferences = null;
        }

        $this->data['categories'] = $this->u_model->get_categories_with_sizes($user_preferences);

        $this->data['main_data'] = $this->data;
        $this->load->view("sizes", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function update_sizes() {
        $sizes = json_encode($_POST);
        if ($sizes != "") {
            $update_sizes = $this->u_model->update_data("customers", ['preferences' => $sizes], array("id" => $this->session->userdata("user_id")));
            if ($update_sizes) {
//                $this->session->set_userdata("sizes_update_success", "<span style='color: green'>Sizes Updated.</span>");
                $this->session->set_userdata("feedback_submission_success", "<span style='color: green'>Sizes Updated.</span>");
            } else {
//                $this->session->set_userdata("sizes_update_error", "<span style='color: tomato'>Failed to Update Sizes, Try Again.</span>");
                $this->session->set_userdata("feedback_submission_failed", "<span style='color: tomato'>Failed to Update Sizes, Try Again.</span>");
            }
        } else {
//            $this->session->set_userdata("sizes_update_error", "<span style='color: tomato'>Select your sizes and try again</span>");
            $this->session->set_userdata("feedback_submission_failed", "<span style='color: tomato'>Failed to Update Sizes, Try Again.</span>");
        }
        redirect($this->agent->referrer());
    }

}
