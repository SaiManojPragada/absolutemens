<?php

class Policies extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model("u_model");
    }

    function index($key = null) {
        if (empty($key)) {
            redirect($this->agent->referrer());
        }
        $where = array("title" => urldecode($key));
        $this->data['data'] = $this->u_model->get_data_conditon_row("cms_pages", $where);
        $this->data['site_property']->site_name = $this->data['site_property']->site_name . ' - ' . $this->data['data']->seo_title;
        $this->data['site_property']->seo_title = $this->data['data']->seo_title;
        $this->data['site_property']->seo_keywords = $this->data['data']->seo_keywords;
        $this->data['site_property']->seo_description = $this->data['data']->seo_description;
        if ($this->data['data']) {
            $this->load->view("includes/header", $this->data);
            $this->load->view("policies", $this->data);
            $this->load->view("includes/footer", $this->data);
        } else {
            redirect($this->agent->referrer());
        }
    }

}
