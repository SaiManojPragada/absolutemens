<?php

class How_it_works extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
    }

    function index() {
        $seo = $this->u_model->get_data_conditon_row("seo", array("page_name" => "how_it_works"));
        $this->data['site_property']->site_name = $this->data['site_property']->site_name . ' - ' . $seo->seo_title;
        $this->data['site_property']->seo_title = $seo->seo_title;
        $this->data['site_property']->seo_keywords = $seo->seo_keywords;
        $this->data['site_property']->seo_description = $seo->seo_description;
        $this->data['how_it_works_points'] = $this->u_model->get_data("how_it_works_points");
        $this->data['how_it_works_images'] = $this->u_model->get_data_row("how_it_works_images");
        $this->load->view("includes/header", $this->data);
        $this->load->view("how-it-works", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
