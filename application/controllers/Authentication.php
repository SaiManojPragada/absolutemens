<?php

class Authentication extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model("u_model");
        $this->load->model('send_email_model');
    }

    function register() {
        $password = $this->input->post("password");
        $check = $this->u_model->get_data_conditon_row("customers", "email='" .
                $this->input->post("email") . "' OR mobile='" . $this->input->post("mobile") . "'");
        if ($check) {
            $this->session->set_userdata("register_err", "Account related to this Email or Phone Number Already Exists!");
            redirect($this->agent->referrer());
            return false;
        }
        $salt = $this->curd_model->generate_random_string_code("customers", 'salt');
        $data = array(
            "name" => $this->input->post("name"),
            "email" => $this->input->post("email"),
            "mobile" => $this->input->post("mobile"),
            "password" => md5($password . $salt),
            "salt" => $salt,
            "created_at" => time()
        );
        $register = $this->u_model->post_data("customers", $data);
        if ($register) {

            $this->data['message'] = "Hi " . $this->input->post("name") . ",Thank You For Registration.<br><br><br>"
                    . "<center>"
                    . "<p><b>Email</b></p>"
                    . "<p>" . $this->input->post("email") . "</p>"
                    . "<p><b>password</b></p>"
                    . "<p>" . $password . "</p>"
                    . "</center>";
            $message = $this->load->view('email_templates/orders_message_template', $this->data, true);
            $this->send_email_model->send_mail($this->input->post("email"), 'Registration Success', $message);
            $this->session->set_userdata("register_success", "Succesfully Registered, Please Login to Continue");
        } else {
            $this->session->set_userdata("register_err", "Passwords Does not Match");
        }

        redirect($this->agent->referrer());

        return true;
    }

    function login() {
        $this->session->unset_userdata("login_err");
        $this->session->unset_userdata("user_unq_id");
        $this->session->unset_userdata("user_id");
        $this->session->unset_userdata("user_email");
        $this->session->unset_userdata("user_mobile");
        $this->session->unset_userdata("is_user_logged_in");
        $email_or_phone = $this->input->post("email_or_phone");
        $password = $this->input->post("password");
        $check_user_existed = $this->u_model->get_data_conditon_row("customers", "email='" .
                $email_or_phone . "' OR mobile='" . $email_or_phone . "'");
        if ($check_user_existed) {
            $salt = $check_user_existed->salt;
            $check_credentials = $this->u_model->get_data_conditon_row("customers",
                    "(email='" .
                    $email_or_phone . "' OR mobile='" . $email_or_phone . "') AND "
                    . "password = '" . md5($password . $salt) . "'");
            if ($check_credentials) {
                $this->u_model->post_data("customers_login_logs", array("customers_id" => $check_credentials->id,
                    "created_at" => time(), "ip_address" => $_SERVER['REMOTE_ADDR']));
                $this->session->set_userdata("user_unq_id", md5($check_credentials->mobile));
                $this->session->set_userdata("user_id", $check_credentials->id);
                $this->session->set_userdata("user_name", $check_credentials->name);
                $this->session->set_userdata("user_email", $check_credentials->email);
                $this->session->set_userdata("is_user_logged_in", true);
                $this->session->set_userdata("login_success", true);
            } else {
                $this->session->set_userdata("login_err", "Invalid Email/Phone or Password");
            }
        } else {
            $this->session->set_userdata("login_err", "User Does not Exists");
        }
        redirect($this->agent->referrer());

        return true;
    }

    function logout() {
        $this->session->unset_userdata("login_err");
        $this->session->unset_userdata("user_unq_id");
        $this->session->unset_userdata("user_id");
        $this->session->unset_userdata("user_email");
        $this->session->unset_userdata("user_mobile");
        $this->session->set_userdata("is_user_logged_in", false);
        $this->session->unset_userdata("is_user_logged_in");
        $this->session->set_userdata("logout_success", true);
        redirect(base_url());
    }

    function forgot_password() {
        $email = $this->input->post("email");
        $check_email = $this->u_model->get_data_conditon_row("customers", array("email" => $email));
        if (empty($check_email)) {
            $arr = array(
                "status" => "invalid",
                "message" => "This Email does not Exists"
            );
            echo json_encode($arr);
            die;
        } else {
            $random = time() . generateRandomString(10);
            $message = "<center>"
                    . "<h4>Reset Your Password</h4>"
                    . "click here to Reset " . base_url('authentication/recover/') . $random
                    . "</center>";
            $forgot_data = array(
                "forgot_password_token" => $random,
                "forgot_password_timeout" => strtotime(date('H:i')) + (60 * 60) * 2
            );
            $update = $this->u_model->update_data("customers", $forgot_data, array("email" => $email));
            if ($update) {
                $this->send_email_model->send_mail($email, "Forgot Password", $message);
                $arr = array(
                    "status" => "valid",
                    "message" => "Email Sent Please Check your Inbox to reset"
                );
                echo json_encode($arr);
                die;
            } else {
                $arr = array(
                    "status" => "invalid",
                    "message" => "Something went Wrong Please Try Again"
                );
                echo json_encode($arr);
                die;
            }
        }
    }

    function recover($key) {
        if (!empty($_POST)) {
            $password = $this->input->post("new_password");
            $salt = $this->curd_model->generate_random_string_code("customers", 'salt');
            $update_data = array(
                "salt" => $salt,
                "password" => md5($password . $salt),
                "forgot_password_token" => null,
                "forgot_password_timeout" => null
            );
            unset($_POST);
            $update = $this->u_model->update_data("customers", $update_data, array("forgot_password_token" => $key));
            if ($update) {
                $this->session->set_userdata('custom_message', "Password updated, Please Login to Continue");
                redirect(base_url());
                die;
            } else {
                $this->session->set_userdata('custom_message', "Password updated, Please Login to Continue");
            }
            die;
        }

        $this->data['key'] = $key;
        $this->db->where("forgot_password_timeout <" . time());
        $check = $this->u_model->get_data_conditon_row("customers", array("forgot_password_token" => $key));
        if (!empty($check)) {
            $this->$data['message'] = "";
        } else {
            $this->$data['message'] = "Invalid Request / Link Expired";
        }
        $this->load->view("includes/header", $this->data);
        $this->load->view("change_password", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
