<?php

class We_shop_for_you extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }
        $this->data['user_page'] = "dash-we-shop-for-you";
    }

    function index() {

        $this->data['user_preferences'] = $this->u_model->get_data_conditon_row("user_preferences", array("customers_id" => $this->session->userdata("user_id")));

        $main_user_prefrences = $this->data['user_preferences']->preferences;

        $custom_user_preferences = $this->u_model->get_data_conditon_row("customers", array("id" => $this->session->userdata("user_id")))->preferences;
        if ($main_user_prefrences != "") {
            $user_preferences = json_decode($main_user_prefrences);
        } else if ($custom_user_preferences != "") {
            $user_preferences = json_decode($custom_user_preferences);
        } else {
            $user_preferences = null;
        }

        $this->data['categories'] = $this->u_model->get_categories_with_sizes($user_preferences);

        //SHOES SIZES DATA

        $this->data['shoes_sizes'] = $this->u_model->get_data("shoes_sizes", null, null, "size", "asc");

        //HEIGHTS AND WEIGHTS

        $this->data['heights'] = $this->u_model->get_data("heights");
        $this->data['weights'] = $this->u_model->get_data("weights");

        $this->data['main_data'] = $this->data;
        $this->load->view("dash-we-shop-for-you", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function update_preferences() {



        $input_data = array(
            "budget" => $this->input->post("budget"),
            "occassion" => $this->input->post("occassion"),
            "description" => $this->input->post("description"),
            "liked_preferences" => $this->input->post("liked_preferences"),
            "disliked_preferences" => $this->input->post("disliked_preferences"),
            "special_preferences" => $this->input->post("special_preferences"),
            "special_measurements" => $this->input->post("special_measurements"),
            "height" => $this->input->post("height"),
            "weight" => $this->input->post("weight")
        );
        unset($_POST['budget']);
        unset($_POST['occassion']);
        unset($_POST['description']);
        unset($_POST['liked_preferences']);
        unset($_POST['disliked_preferences']);
        unset($_POST['special_preferences']);
        unset($_POST['special_measurements']);
        unset($_POST['height']);
        unset($_POST['weight']);
        $user_id = $this->session->userdata("user_id");
        $categories_wise_sizes = json_encode($_POST);
        $input_data['preferences'] = $categories_wise_sizes;
        $is_pref_exists = $this->check_pref_exists($user_id);
        if ($is_pref_exists) {
            $update_sizes = $this->u_model->update_data("user_preferences", $input_data,
                    array("customers_id" => $this->session->userdata("user_id")));
            if ($update_sizes) {
//                $this->session->set_userdata("prefs_update_success", "<span style='color: green'>Sizes Updated.</span>");
                $this->session->set_userdata("feedback_submission_success", "<span style='color: green'>Succesfully Updated.</span>");
            } else {
//                $this->session->set_userdata("prefs_update_error", "<span style='color: tomato'>Failed to Update Sizes, Try Again.</span>");
                $this->session->set_userdata("feedback_submission_failed", "<span style='color: tomato'>Failed to Update, Try Again.</span>");
            }
            redirect($this->agent->referrer());
        } else {
            $input_data['customers_id'] = $user_id;
            $update_sizes = $this->db->insert('user_preferences', $input_data);
            if ($update_sizes) {
//                $this->session->set_userdata("prefs_update_success", "<span style='color: green'>Sizes Updated.</span>");
                $this->session->set_userdata("feedback_submission_success", "<span style='color: green'>Succesfully Updated.</span>");
            } else {
//                $this->session->set_userdata("prefs_update_error", "<span style='color: tomato'>Failed to Update Sizes, Try Again.</span>");
                $this->session->set_userdata("feedback_submission_failed", "<span style='color: tomato'>Failed to Update, Try Again.</span>");
            }
            redirect($this->agent->referrer());
        }
    }

    function check_pref_exists($user_id) {
        $this->db->select('id');
        $this->db->from('user_preferences');
        $this->db->where('customers_id', $user_id);
        $rows = $this->db->count_all_results();
        if ($rows > 0) {
            return true;
        } else {
            return false;
        }
    }

}
