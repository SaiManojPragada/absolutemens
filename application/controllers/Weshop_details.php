<?php

class Weshop_details extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model("home_model");
        $this->load->model("u_model");
    }

    function index() {
        $this->data['user_budget'] = $this->u_model->get_data_row("user_preferences")->budget;
        $this->data['we_shop_details_min_amount'] = $this->u_model->get_data_row("we_shop_for_you_values")->minimum_amount;
        $this->data['info'] = $this->u_model->get_data_row("we_shop_for_you_info");
        $this->load->view("includes/header", $this->data);
        $this->load->view("weshop-details", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
