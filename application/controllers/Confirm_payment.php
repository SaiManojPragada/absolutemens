<?php

class Confirm_payment extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
        $this->load->model('send_email_model');
    }

    function success_up() {
        $unq_id = $this->input->post('ref_id');
        $this->data['order_details'] = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                array("unq_id" => $unq_id));
        $user_id = $this->data['order_details']->customers_id;
        $customer_data = $this->u_model->get_nostatcheck_row("customers", array("id" => $user_id));
        $package_details = $this->u_model->get_nostatcheck_row("packages",
                array("id" => $this->data['order_details']->packages_id));
        unset($_POST['ref_id']);
        $data = $this->input->post();

        $data["estimated_delivery_date"] = strtotime(date('Y-m-d H:i:s', strtotime('+ 10 days')));
        $data['transaction_id'] = $this->generate_random_key('subscriptions_transactions', 'transaction_id');
        $data['expiry_date'] = strtotime('+' . $package_details->duration . ' days');
        if ($this->data['order_details']->is_we_shop_for_you == 1) {
            $we_shop_data = $this->u_model->get_data_row("we_shop_for_you_values");
            $data['expiry_date'] = strtotime('+' . $we_shop_data->expiry_duration . ' days');
        }
        $update_success = $this->u_model->update_data("subscriptions_transactions", $data, array("unq_id" => $unq_id));
        if ($update_success) {
            $this->data['order_details'] = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                    array("unq_id" => $unq_id));
            $this->data['customer_data'] = $customer_data;
            $this->data['package_details'] = $package_details;
            $message = $this->load->view('email_templates/order_placed_template', $this->data, true);
            $this->send_email_model->send_mail($customer_data->email, "Order Placed", $message);
            echo $message;
        } else {
            echo false;
        }
        die;
    }

    function remove_coupon() {
        $unq_id = $this->input->post("unq_id");
        $trans_info = $this->u_model->get_nostatcheck_row("subscriptions_transactions", array("unq_id" => $unq_id));
        $grand_total = $trans_info->grand_total + $trans_info->coupon_discount;
        $up_data = array(
            "grand_total" => $grand_total,
            "coupon_code" => "",
            "coupon_discount" => 0
        );
        $update = $this->u_model->update_data("subscriptions_transactions", $up_data, array("unq_id" => $unq_id));
        if ($update) {
            $order_details = $this->u_model->get_nostatcheck_row("subscriptions_transactions", array("unq_id" => $unq_id));
            $package_details = $this->u_model->get_nostatcheck_row("packages", array("id" => $order_details->packages_id));
            $data = array(
                "grand_total" => (float) $grand_total,
                "coupon_discount" => "<span style='color: tomato'>Apply Coupon</span>",
                "coupon_code" => ""
            );
            $arr = [
                "err_code" => "valid",
                "message" => "Coupon Code Removed",
                "data" => $data
            ];
        } else {
            $arr = [
                "err_code" => "invalid",
                "message" => "Invalid Coupon Code"
            ];
        }
        echo json_encode($arr);
    }

    function apply_coupon() {
        $unq_id = $this->input->post("unq_id");
        $coupon_code = $this->input->post("coupon_code");
        $check_coupon = $this->u_model->get_data_conditon_row("coupons", "coupon_code LIKE binary '$coupon_code'");
        sleep(1);
        if ($check_coupon) {
            $coupon_discount = 0;
            $order_details = $this->u_model->get_nostatcheck_row("subscriptions_transactions", array("unq_id" => $unq_id));
            $package_details = $this->u_model->get_nostatcheck_row("packages", array("id" => $order_details->packages_id));
            if ($check_coupon->discount_type === "Percentage") {

                $coupon_discount = ($order_details->total_price / 100) * $check_coupon->discount;
                if ($check_coupon->discount_max_amount < $coupon_discount) {
                    $coupon_discount = $check_coupon->discount_max_amount;
                }
            }
            if ($check_coupon->discount_type === "Flat") {
                $coupon_discount = $check_coupon->discount;
            }
            $grand_total = $order_details->grand_total - $coupon_discount;
            $data = array(
                "grand_total" => (float) $grand_total,
                "coupon_discount" => (float) $coupon_discount,
                "coupon_code" => $coupon_code
            );
            $this->u_model->update_data("subscriptions_transactions", $data, array("unq_id" => $unq_id));
            $arr = [
                "err_code" => "valid",
                "message" => "Yay you've saved Rs. " . $coupon_discount,
                "data" => $data
            ];
        } else {
            $arr = [
                "err_code" => "invalid",
                "message" => "Invalid Coupon Code"
            ];
        }
        echo json_encode($arr);
    }

}
