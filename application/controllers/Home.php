<?php

class Home extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model("home_model");
        $this->load->model("u_model");
    }

    function index() {

        $this->data['sliders'] = $this->u_model->get_data("homepage_sliders");
        $this->data['packages'] = $this->u_model->get_data("packages", "", 3);
        $this->db->order_by("view_order", "asc");
        $this->data['banners'] = $this->u_model->get_data("banners");
        $this->data['how_it_works_images'] = $this->u_model->get_data_row("how_it_works_images");
        $this->data['how_it_works_points'] = $this->u_model->get_data("how_it_works_points");
        $this->data['about_us'] = $this->u_model->get_data_row("aboutus");
        $this->data['testimonials'] = $this->u_model->get_data("testimonials");
        $this->data['blog_info'] = $this->u_model->get_data_row("blog_info");
        $this->data['blogs'] = $this->u_model->get_data("blogs", "", 9);
        $this->load->view("includes/header", $this->data);
        $this->load->view("index", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
