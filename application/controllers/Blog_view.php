<?php

class Blog_view extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model("u_model");
    }

    function index($key) {
        $this->data['blog'] = $this->u_model->get_data_conditon_row("blogs", array("slug" => $key));
        $this->data['similar_blogs'] = $this->u_model->get_data("blogs", "id != " . $this->data['blog']->id, 5);
        $this->load->view("includes/header", $this->data);
        $this->load->view("blog-view", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
