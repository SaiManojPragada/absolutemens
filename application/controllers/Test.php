<?php

class Test extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model("admin_settlements_model");
    }

    function index() {
        $this->admin_settlements_model->add_to_vendor_wallet(25);
    }

}
