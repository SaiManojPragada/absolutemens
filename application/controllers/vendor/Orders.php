<?php

class Orders extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function view($param = "") {
        $this->data["active_main_page"] = "My Orders";
        $this->data["active_sub_page"] = "All Orders";
        $this->data["param"] = $param;
        $this->vendor_view("my_orders", $this->data);
    }

}
