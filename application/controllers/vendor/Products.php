<?php

class Products extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }

        if (isset($_GET["mode"])) {
            if ($_GET["mode"] == "admin_control") {
                unset($_GET["mode"]);
                unset($_GET["access_token"]);
                unset($_GET["vendor_id"]);
                redirect("vendor/products?" . http_build_query($_GET));
            }
        }
    }

    function index() {
        $vendor_id = $this->session->userdata("vendor_id");
//        die;
//        $vendor_id = $this->vendor_model->get_logged_vendor_id();

        $this->db->where("vendors_id", $vendor_id);
        $products = $this->db->get("products")->result();

        $this->data["active_main_page"] = "Products";
        $this->data["active_sub_page"] = "products";
        $this->vendor_view("products");
    }

}
