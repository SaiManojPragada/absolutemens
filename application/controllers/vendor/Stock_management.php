<?php

class Stock_management extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->data = [];
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function view($product_inventory_id) {
        $this->data['product_inventory_id'] = $product_inventory_id;
        $this->data["active_main_page"] = "Stock Management";
        $this->data["active_sub_page"] = "Stock Management";
        $this->vendor_view("stock_management");
    }

}
