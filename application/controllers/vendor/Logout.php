<?php

class Logout extends MY_Controller {

    function index() {

//        $vendor_id = $this->vendor_model->get_logged_vendor_id();
//        $this->db->set("is_online", 0);
//        $this->db->where("id", $vendor_id);
//        $this->db->update("restaurants");
        //$this->session->unset_userdata("user_id");
        foreach ($this->session->all_userdata() as $key => $item) {
            /* if ($key == "w_user_id") {
              continue;
              } */
            $this->session->unset_userdata($key);
        }
        $this->session->set_flashdata("msg", "You Have Successfully Logged Out.");
        redirect("vendor/login");
    }

}
