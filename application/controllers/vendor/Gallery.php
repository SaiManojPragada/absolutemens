<?php

class Gallery extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function index() {
        $this->data["active_main_page"] = "Business Info";
        $this->data["active_sub_page"] = "gallery";
        $this->vendor_view("gallery");
    }

}
