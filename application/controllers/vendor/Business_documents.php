<?php

class Business_documents extends MY_Controller{

	function __construct(){
		parent::__construct();
		if ($this->vendor_model->check_for_user_logged() == false) {
			redirect("vendor/login");
		}

	}
	function index(){
		$this->vendor_view("business_documents");
	}
}
