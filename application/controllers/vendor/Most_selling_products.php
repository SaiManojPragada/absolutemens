<?php

class Most_selling_products extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function index() {

        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $this->db->where("status", 1);

        $this->db->select("id");
        $this->db->where("is_pharmacy", 1);
        $cids = $this->db->get("cuisine_types")->result();

        $this->db->select("cuisine_types_ids");
        $this->db->where("id", $vendor_id);
        $rcids = $this->db->get("restaurants")->row()->cuisine_types_ids;


        $rcids = explode(",", $rcids);
        $this->data["attachment_required"] = "no";

        foreach ($cids as $item) {
            if (in_array($item->id, $rcids)) {
                $this->data["attachment_required"] = "yes";
            }
        }

        $this->data["active_main_page"] = "Food Menu";
        $this->data["active_sub_page"] = "most_selling_products";
        $this->vendor_view("most_selling_products");
    }

}
