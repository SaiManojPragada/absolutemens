<?php

class Withdraw_request extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function new() {
        $this->data["active_main_page"] = "Withdraw Request";
        $this->data["active_sub_page"] = "new_withdraw_request";
        $this->vendor_view("new_withdraw_request");
    }

    function list() {
        $this->data["active_main_page"] = "Withdraw Request";
        $this->data["active_sub_page"] = "withdraw_request_list";
        $this->vendor_view("withdraw_request_list");
    }

}
