<?php

class Change_password extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function index() {
        $this->data["active_main_page"] = "change_password";
        $this->vendor_view("change-password");
    }

}
