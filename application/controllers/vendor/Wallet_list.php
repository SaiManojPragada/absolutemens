<?php

class Wallet_list extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function index() {
        $this->data["active_main_page"] = "Wallet List";
        $this->data["active_sub_page"] = "wallet_list";
        $this->vendor_view("wallet_list");
    }

}
