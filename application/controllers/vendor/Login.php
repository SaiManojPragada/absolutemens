<?php

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->data = [];
        $this->load->model("vendor_model");
        if ($this->vendor_model->check_for_user_logged() == true) {
            redirect("vendor/dashboard");
        }
    }

    function index() {
        $this->vendor_view("login");
    }

}
