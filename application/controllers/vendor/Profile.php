<?php

class Profile extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->data = [];
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function index() {
        $this->data["active_main_page"] = "Business Info";
        $this->data["active_sub_page"] = "profile";
        $this->vendor_view("profile");
    }

}
