<?php

class Products_preview extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->data["active_main_page"] = "Products Preview";
        $this->data["active_sub_page"] = "products_preview";
        $this->vendor_view("products_preview");
    }

}
