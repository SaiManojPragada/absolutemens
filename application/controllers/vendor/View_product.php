<?php

class View_product extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function view($id) {
        $this->data["active_main_page"] = "Products Preview";
        $this->data["active_sub_page"] = "products_preview";
        $data['id'] = $id;
        $this->vendor_view("view_product", $data);
    }

}
