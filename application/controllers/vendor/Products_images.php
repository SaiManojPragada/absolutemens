<?php

class Products_images extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function view($id) {
        $this->data['id'] = $id;
        $this->data["active_main_page"] = "Products";
        $this->data["active_sub_page"] = "products";
        $this->vendor_view("products_images");
    }

}
