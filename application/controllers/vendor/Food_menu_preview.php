<?php

class Food_menu_preview extends MY_Controller{

	function __construct(){
		parent::__construct();
    if ($this->vendor_model->check_for_user_logged() == false) {
        redirect("vendor/login");
    }

	}
	function index(){
		$this->data["active_main_page"] = "Food Menu";
        $this->data["active_sub_page"] = "food_menu_preview";
		$this->vendor_view("food_menu_preview");
	}
}
