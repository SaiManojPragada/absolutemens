<?php

class Dashboard extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
        if (isset($_GET["mode"])) {
            if ($_GET["mode"] == "admin_control") {
                unset($_GET["mode"]);
                unset($_GET["access_token"]);
                unset($_GET["vendor_id"]);
                redirect("vendor/food_items?" . http_build_query($_GET));
            }
        }
    }

    function index() {
        $completed = 0;
        $this->data["active_main_page"] = "dashboard";
        $vendor_id = $this->session->userdata("vendor_id");
        $this->data['total_earning'] = $this->u_model->get_total("vendor_settlements", array("vendor_id" => $vendor_id), "amount");

        $this->db->group_by("order_id");
        $this->data['total_orders'] = sizeof($this->u_model->get_data("order_products", array("vendor_id" => $vendor_id)));

        $this->db->group_by("order_id");
        $delivered_orders = $this->u_model->get_data("order_products", array("vendor_id" => $vendor_id, "order_status" => "order_delivered"));
        foreach ($delivered_orders as $orders) {
            $order = $this->u_model->get_data_row("orders", array("id" => $orders->order_id, "order_status" => "order_completed"));
            if (!empty($order)) {
                $completed = $completed + 1;
            }
        }

        $this->db->where("order_status != 'order_delivered'");
        $this->db->group_by("order_id");
        $this->data['pending_orders'] = sizeof($this->u_model->get_data("order_products", array("vendor_id" => $vendor_id)));
        $this->data['completed_orders'] = $completed;
        $this->vendor_view("dashboard", $this->data);
    }

}
