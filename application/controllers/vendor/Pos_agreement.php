<?php

class Pos_agreement extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("agent_store_documents_model");
    }

    function index() {
        $this->data["store_data"] = $this->get_details();

        if ($this->data["store_data"]->agreement_status == "Accepted") {
            $ref_code = $this->data["store_data"]->ref_code;
            $link = "<a href='" . base_url() . "vendor/pos_agreement/mpdf_view?ref_code=" . $ref_code . "'>Click here to download agreement form</a>";
            die("Thank you! Your already accpeted this link " . $link);
        }

        $this->load->view("pos_agreement/v2", $this->data);
    }

    function accepted() {
        $this->load->model("agent_store_documents_model");
        $ref_code = $this->input->get_post("ref_code");

        $data = $this->db->where("ref_code", $ref_code)->get("restaurants")->row();

        if ($data->agreement_status != "Accepted") {
            $this->db->set("agreement_status", "Accepted");
            $this->db->set("agreement_accepted_date", THIS_DATE_TIME);
            $this->db->where("id", $data->id);
            $this->db->update("restaurants");

            $this->email->from(NO_REPLAY_MAIL, SITE_TITLE);
            $this->email->to($data->contact_email);
            if (BCC_EMAIL) {
                $this->email->bcc(BCC_EMAIL);
            }
            $this->email->subject("Congratulations! POS Created " . $data->ref_code . "(" . $data->restaurant_name . ") Agreement Confirmation mail");

            $email_data = array(
                'restaurant_name' => $data->restaurant_name,
                'ref_code' => $data->ref_code,
            );

            $message = $this->load->view('email_templates/pos_accepted_agreement_mail', $email_data, TRUE);

            $this->email->attach(base_url() . 'vendor/pos_agreement/mpdf_view?ref_code=' . $data->ref_code, 'attachment', $data->ref_code . '_agreement.pdf');
            $this->email->message($message);
            $this->email->send();

            redirect("vendor/pos_agreement/mpdf_view?ref_code=" . $data->ref_code);
        }
    }

    function get_details() {
        $ref_code = $this->input->get_post("ref_code");
        $data = $this->db->where("ref_code", $ref_code)->get("restaurants")->row();

        $data->country_name = $this->locations_model->get_country_name($data->countries_id);
        $data->state_name = $this->locations_model->get_state_name($data->states_id);
        $data->city_name = $this->locations_model->get_city_name($data->cities_id);
        $data->location_name = $this->locations_model->get_location_name($data->locations_id);
        $data->business_hours = $this->vendor_business_hours_model->get($data->id);
        $data->documents = $this->agent_store_documents_model->get_documents($data->id);
        $data->categories = $this->vendor_categories_model->get($data->id);
        $data->registered_date = date("d-M-Y", $data->created_at);

        $data->display_address = $data->land_mark . "," . $data->address . ", " . $data->location_name . ", " . $data->city_name . "," . $data->country_name . ", " . $data->zipcode;
        $cuisine_types_ids = explode(",", $data->cuisine_types_ids);
        $data->cuisine_types = [];

        for ($i = 0, $l = count($cuisine_types_ids); $i < $l; $i++) {
            $data->cuisine_types[] = $this->restaurants_model->get_cuisine_type_name($cuisine_types_ids[$i]);
        }
        $data->cuisine_types = implode(",", $data->cuisine_types);

        if ($data->display_image) {
            $data->display_image = STORE_IMG_PATH . $data->id . "/" . $data->display_image;
        }

        return $data;
    }

    function mpdf_view() {
        $base_path = str_replace("system", "vendor", BASEPATH);
        require_once $base_path . '/autoload.php';


        $this->data["store_data"] = $this->get_details();
        if ($this->input->get_post("type") == "agent_app") {
            
        } else if ($this->data["store_data"]->agreement_status == "Pending") {
            redirect("vendor/pos_agreement?ref_code=" . $this->data["store_data"]->ref_code);
        }

        $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'margin_left' => 20,
            'margin_right' => 20,
            'margin_top' => 50,
            'margin_bottom' => 10
        ]);

        $this->data["mpdf"] = $mpdf;

        $mpdf->SetWatermarkImage(
                base_url() . "web_assets/pos/watermark.png", 0.4, ''
        );
        $mpdf->showWatermarkImage = true;
        $mpdf->use_kwt = true;
        $mpdf->autoPageBreak = true;

        //$html = file_get_contents($_GET['url']);
        //$html = $this->load->view("pos_agreement/index", $this->data, true);
        $html = $this->load->view("pos_agreement/v2", $this->data, true);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }

}
