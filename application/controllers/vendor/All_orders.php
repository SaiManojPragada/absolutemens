<?php

class All_orders extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function index() {
        $this->data["active_main_page"] = "All Orders";
        $this->data["active_sub_page"] = "all_orders";
        $this->vendor_view("all_orders");
    }

}
