<?php

class Coupons extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function index() {
        $this->data["active_main_page"] = "Offers and Coupons";
        $this->data["active_sub_page"] = "coupons";
        $this->vendor_view("coupons");
    }

}
