<?php

class Update_profile extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            redirect("vendor/login");
        }
    }

    function index() {
        $this->data["active_main_page"] = "Update Profile";
        $this->data["active_sub_page"] = "Update Shop Profile";
        $this->vendor_view("update_profile");
    }

}
