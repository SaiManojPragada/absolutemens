<?php

class Place_order extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }
        $this->load->model("u_model");
    }

    function index() {
        redirect(base_url('error-404'));
    }

    function order($unq_id = null) {
        if (empty($unq_id)) {
            redirect(base_url('error-404'));
            die;
        }

        $this->session->set_userdata('can_pay', 'YES');
        $this->data['order_details'] = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                array("unq_id" => $unq_id));
        $this->data['package_details'] = $this->u_model->get_nostatcheck_row("packages",
                array("id" => $this->data['order_details']->packages_id));

        $this->load->view("includes/header", $this->data);
        $this->load->view("place-order", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
