<?php

class Packages extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model("u_model");
    }

    function index() {
        $this->data['packages'] = $this->u_model->get_data("packages");
        $this->load->view("includes/header", $this->data);
        $this->load->view("change-package", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
