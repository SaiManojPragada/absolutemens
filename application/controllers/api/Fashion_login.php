<?php

header('Content-type: application/json');

class Fashion_login extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $username = $this->input->get_post('username');
        $password = $this->input->get_post('password');
        $data = $this->db->get_where("designers_and_analysts", ["email" => $username])->row();
        if ($data) {
            if ($data->status == 0) {
                $arr = array('err_code' => "invalid", "error_type" => "account_not_existed", "message" => "Sorry, your account is suspended, please contact admin");
            } else if (md5($password . $data->salt) !== $data->password) {
                $arr = array('err_code' => "invalid", "data" => $this->data["user"], "message" => "Invalid Current Password");
            } else {
                $this->fashion_designer_model->create_login_session($data);
//                unset($data->password);
                unset($data->salt);
                unset($data->id);
                unset($data->status);

                $arr = array('err_code' => "valid", "error_type" => "account_existed", "message" => "Login Success", "data" => md5($password . $data->salt) . ", " . $data->password);
            }
        } else {
            $arr = array('err_code' => "invalid", "error_type" => "account_not_existed", "message" => "Sorry, this account does not exist");
        }
        echo json_encode($arr);
    }

}
