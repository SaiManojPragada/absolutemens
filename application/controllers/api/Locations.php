<?php

header('Content-type: application/json');

class Locations extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('u_model');
    }

    function index() {
        $data = $this->u_model->get_nostatcheck_data("states");
        $arr = [
            "err_code" => "valid",
            "message" => "States List",
            "data" => $data
        ];
        echo json_encode($arr);
    }

    function districts() {
        $data = $this->u_model->get_nostatcheck_data("districts", array("states_id" => $this->input->get_post('states_id')));
        $arr = [
            "err_code" => "valid",
            "message" => "Districts List",
            "data" => $data
        ];
        echo json_encode($arr);
    }

    function cities() {
        $data = $this->u_model->get_nostatcheck_data("cities", array(
            "districts_id" => $this->input->get_post('districts_id')));
        $arr = [
            "err_code" => "valid",
            "message" => "Cities List",
            "data" => $data
        ];
        echo json_encode($arr);
    }

}
