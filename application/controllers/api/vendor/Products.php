<?php

class Products extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
        $this->load->model('stock_management_model');
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $result['results'] = $this->u_model->get_nostatcheck_data("products", array("vendors_id" => $vendor_id));
        foreach ($result['results'] as $res) {
            $this->db->where("id", $res->categories_id);
            $res->category = $this->db->get_where('categories', ['id' => $res->categories_id])->row()->name;
            $res->sub_category = $this->db->get_where('sub_categories', ['id' => $res->sub_categories_id])->row()->title;
        }
        if ($result['results']) {

            $arr = array(
                'err_code' => "valid",
                "title" => "",
                "message" => $this->title . " list",
                "data" => $result
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                'title' => "Sorry",
                "message" => "No products found",
                "data" => $result
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function get_products() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $key = $this->input->get_post('key');
        $avail = $this->input->get_post('avail');
        if (!empty($key)) {

            $this->db->where("(title LIKE '%" . $key . "%' OR product_id LIKE '%" . $key . "%' OR brand LIKE '%" . $key . "%')");
        }
        if ($avail === 0 || $avail === '0') {
            $this->db->where("in_stock", false);
        }
        if (!empty($avail)) {
            $this->db->where("in_stock", $avail);
        }
        $result['results'] = $this->u_model->get_data("products", array("vendors_id" => $vendor_id));
        foreach ($result['results'] as $res) {
            $this->db->where("id", $res->categories_id);
            $res->category = $this->db->get("categories")->row();
            $res->category = $res->category->name;
        }
        if ($result['results']) {

            $arr = array(
                'err_code' => "valid",
                "title" => "",
                "message" => "Products Found",
                "data" => $result
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                'title' => "Sorry",
                "message" => "No products found",
                "data" => $result
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function availability($product_id) {
        $res = $this->u_model->get_data_conditon_row("products", array("id" => $product_id));
        if ($res->in_stock) {
            $this->db->set(array("in_stock" => false));
        } else {
            $this->db->set(array("in_stock" => true));
        }
        $this->db->where("id", $product_id);
        $update = $this->db->update("products");
        if ($update) {
            $arr = array(
                'err_code' => "valid",
                "message" => "Product Status Updated"
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => "Unable to update product Status"
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function single($product_id) {
        $inventory_id = $this->input->get('inventory_id');
        $res = $this->u_model->get_data_conditon_row("products", array("id" => $product_id));
        if ($res) {
            $res->category = $this->db->get_where('categories', ['id' => $res->categories_id])->row()->name;
            $res->sub_category = $this->db->get_where('sub_categories', ['id' => $res->sub_categories_id])->row()->title;

            $this->db->where("product_id", $res->id);
            $res->images = $this->db->get("products_images")->result();
            if ($inventory_id) {
                $this->db->where('id', $inventory_id);
            }
            $this->db->where("product_id", $res->id);
            $res->inventory = $this->db->get("product_inventory")->result();

            foreach ($res->inventory as $i) {
                $i->size = $this->db->get_where('sizes', ['id' => $i->size_id])->row()->title;
                $i->price = (int) $i->price;
            }

            $res->updated_at = date('d M Y, h:i A', $res->updated_at);

            $arr = array(
                'err_code' => "valid",
                "title" => "",
                "message" => "Product Data",
                "data" => $res
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                'title' => "Sorry",
                "message" => "No products found"
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function images($id = null) {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        if (!empty($id)) {
            $vendor_id = $id;
        }
        $result['results'] = $this->u_model->get_data("products", array("vendors_id" => $vendor_id));
        foreach ($result['results'] as $res) {
            $this->db->where("id", $res->categories_id);
            $res->category = $this->db->get("categories")->row();
            $this->db->where("id", $res->sizes_id);
            $res->size = $this->db->get($res->category->data_table_prefix . "_sizes")->row()->size;
            $res->category = $res->category->name;
            $this->db->where("product_id", $res->id);
            $res->images = $this->db->get("products_images")->result();
        }
        if ($result['results']) {

            $arr = array(
                'err_code' => "valid",
                "title" => "",
                "message" => $this->title . " list",
                "data" => $result
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                'title' => "Sorry",
                "message" => "No products found",
                "data" => $result
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function inventory($id) {
        $this->db->where("product_id", $id);
        $inventory = $this->db->get("product_inventory")->result();
        foreach ($inventory as $i) {
            $i->size = $this->db->get_where('sizes', ['id' => $i->size_id])->row()->title;
            $i->price = (int) $i->price;
        }
        if ($inventory) {
            $arr = array(
                'err_code' => "valid",
                "message" => "Product Inventory",
                'data' => $inventory
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => "No Product Inventory Found"
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function add_to_inventory() {

        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $data = [
            "product_id" => $this->input->post('product_id'),
            "size_id" => $this->input->post('sizes_id'),
            "price" => $this->input->post('price'),
            "quantity" => $this->input->post('quantity'),
            "vendor_id" => $vendor_id
        ];
        if ($this->input->post('id')) {
            $this->db->where('id !=', $this->input->post('id'));
        }
        $inventory_rows = $this->db->select('id')->get_where('product_inventory', ['product_id' => $data['product_id'], 'size_id' => $data['size_id']])->num_rows();
        if ($inventory_rows > 0) {
            $arr = array(
                'err_code' => "invalid",
                "message" => "This inventory already exists"
            );
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }

        if ($this->input->post('id')) {
            $this->update_inventory($data);
        }
        unset($data['quantity']);
        $insert = $this->u_model->post_data('product_inventory', $data);
        if ($insert) {
            $product_inventory_id = $this->db->insert_id();
            $stock_post_arr = array(
                "product_id" => $this->input->post('product_id'),
                "product_inventory_id" => $product_inventory_id,
                "quantity" => $this->input->post('quantity'),
                "type" => 'credit',
                "action_for" => "initial_stock"
            );
            $this->stock_management_model->add_stock($stock_post_arr);
            $arr = array(
                'err_code' => "valid",
                "message" => "Added to Product Inventory"
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => "Unable to Add to Product Inventory"
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function update_inventory($data) {
        $id = $this->input->post('id');
        unset($data['quantity']);
        unset($data['id']);
        unset($data['vendor_id']);
        unset($data['product_id']);
        unset($data['size_id']);
        $update = $this->u_model->update_data("product_inventory", $data, array("id" => $id));

        if ($update) {
            $arr = array(
                'err_code' => "valid",
                "message" => "Product Inventory Updated"
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => "Unable to Update Product Inventory"
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function delete_from_inventory() {
        $id = $this->input->post("id");
        $delete = $this->u_model->delete_data("product_inventory", array("id" => $id));
        if ($delete) {
            $arr = array(
                'err_code' => "valid",
                "message" => "Entry Deleted from Inventory"
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => "Unable to Delete Entry from Inventory"
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function add() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $_POST['updated_at'] = time();
        unset($_POST['price']);
        unset($_POST['sub_category']);
        if (!empty($_POST['id'])) {
            $this->update($_POST);
        } else {
            $_POST['created_at'] = time();
            $_POST['vendors_id'] = $vendor_id;
            $insert = $this->u_model->post_data('products', $_POST);
            if ($insert) {
                $arr = array(
                    'err_code' => "valid",
                    "message" => "Product Added"
                );
            } else {
                $arr = array(
                    'err_code' => "invalid",
                    "message" => "Unable to Add Product"
                );
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
        }
    }

    function update($data) {
        $id = $data['id'];
        unset($data['id']);
        unset($data['created_at']);
        unset($data['category']);
        unset($data['size']);
        $update = $this->u_model->update_data("products", $data, array("id" => $id));
        if ($update) {
            $arr = array(
                'err_code' => "valid",
                "message" => "Product Updated"
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => "Unable to Update Product"
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function delete() {

        $id = $this->input->post('id');
        $delete = $this->u_model->delete_data("products", array("id" => $id));
        if ($delete) {
            $arr = array(
                'err_code' => "valid",
                "message" => "Product Deleted"
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => "Unable to Delete Product"
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function get_sub_categories() {
        $category_id = $this->input->post('category_id');
        $response = $this->db->get_where('sub_categories', ['category_id' => $category_id])->result();
        if ($response[0]->id != "") {
            $arr = array(
                'err_code' => "valid",
                "data" => $response
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "data" => []
            );
        }
        echo json_encode($arr);
        die;
    }

    function stock_list() {

        $filters = array();
        $filters['type'] = $this->input->post('action');
        $filters['from_date'] = $this->input->post('from_date');
        $filters['to_date'] = $this->input->post('to_date');
        $filters['product_inventory_id'] = $this->input->post('product_inventory_id');

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : 50;
        $_GET['start'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $total_results = $this->vendor_products_model->get_stock_list_count($filters);

        $filters["limit"] = $limit;
        $filters["start"] = $start;
        $pagination = my_pagination("products/stock_list", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
        $data = $this->vendor_products_model->get_stock_list($filters);
        $arr = [
            'status' => "valid",
            "pagination" => $pagination,
            "data" => [
                "data" => $data,
                "product_details" => $this->vendor_products_model->get_product_details($filters['product_inventory_id'])
            ]
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

}
