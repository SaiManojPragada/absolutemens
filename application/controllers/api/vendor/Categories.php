<?php

header('Content-type: application/json');

class Categories extends MY_Controller {

    private $title = "Categories";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $search_key = $this->input->get_post("search_key");
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $categories = $this->vendor_categories_model->get($vendor_id, $search_key);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $categories
        );
        echo json_encode($arr);
    }

}
