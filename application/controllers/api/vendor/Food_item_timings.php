<?php

header('Content-type: application/json');

class Food_item_timings extends MY_Controller {

    private $model_name = "vendor_food_item_timings_model";
    private $title = "Product Timings";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $category_id = $this->input->get_post("restaurant_food_items_id");
        $timings = $this->{$this->model_name}->get($category_id);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $timings
        );
        echo json_encode($arr);
    }

    function add() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        // $this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("restaurant_food_items_id", "Category Id", "required");
        //$this->form_validation->set_rules("from_time", "From Time", "required");
        //$this->form_validation->set_rules("to_time", "To Time", "required");


        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }


        if ($this->input->get_post("request_from") == "mobile") {
            $timings = json_decode($this->input->get_post('timings'), true);
        } else {
            $timings = $this->input->get_post('timings');
        }

        $restaurant_food_items_id = $this->input->get_post('restaurant_food_items_id');


        $timings_arr = [];
        for ($i = 0; $i < count($timings); $i++) {
            $timings_arr[] = [
                "restaurant_food_items_id" => $restaurant_food_items_id,
                "week_name" => $timings[$i]["week_name"],
                "schedule" => $timings[$i]["schedule"],
                "from_time" => $timings[$i]["from_time"] ? $timings[$i]["from_time"] : null,
                "to_time" => $timings[$i]["to_time"] ? $timings[$i]["to_time"] : null,
                "created_at" => THIS_DATE_TIME
            ];
        }

        $data = $timings_arr;


        if ($this->{$this->model_name}->add($data, $vendor_id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " added successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => singular($this->title) . " not added, please try again"
            );
            echo json_encode($arr);
        }
    }

    function delete() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $id = (int) $this->input->get_post("id");
        $response = $this->{$this->model_name}->delete($id, $vendor_id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        echo json_encode($arr);
    }

}
