<?php

header('Content-type: application/json');

class Sizes extends MY_Controller {

    private $title = "Sizes";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $category_id = $this->input->post('cat');
        $sizes = $this->db->get_where('sizes', ['category_id' => $category_id, 'status' => 1])->result();
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $sizes
        );
        echo json_encode($arr);
    }

}
