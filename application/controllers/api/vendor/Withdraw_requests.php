<?php

class Withdraw_requests extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('vendor_withdraw_request_model');
    }

    function index() {

        $this->form_validation->set_rules("amount", "Amount", "required", array(
            'required' => 'Amount is required',
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "amount" => form_error("amount")
            ];
            $single_line_message = "";

            if (form_error("amount")) {
                $single_line_message = form_error("amount");
            }
            $arr = [
                "status" => "invalid_form",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr);
        } else {
            $data = array(
                "vendor_id" => $this->session->userdata('vendor_id'),
                "amount" => $this->input->post("amount")
            );
            $response = $this->vendor_withdraw_request_model->request($data);
            if ($response == "bank_details_not_approved") {
                $arr = [
                    "status" => 'invalid',
                    "message" => "Your bank Details not Approved"
                ];
            } else if ($response == "minimum_withdraw_balance") {
                $minimum_amount = $this->vendor_withdraw_request_model->get_minimum_withdraw_amount();
                $arr = [
                    "status" => 'invalid',
                    "message" => "Minimum withdraw amount should be $minimum_amount",
                ];
            } else if ($response == "maximum_withdraw_exceeded") {
                $maximum_withdraw_amount = $this->vendor_withdraw_request_model->get_maximum_withdraw_amount();
                $arr = [
                    "status" => 'invalid',
                    "message" => "Maximum withdraw amount exceeded.It should be less than $maximum_withdraw_amount",
                ];
            } else if ($response == "less_wallet_amount") {
                $arr = [
                    "status" => 'invalid',
                    "message" => "Insufficent funds in wallet"
                ];
            } else if ($response == "bank_details_not_updated") {
                $arr = [
                    "status" => 'invalid',
                    "message" => "Please update your bank details"
                ];
            } else if ($response == "already_submitted") {
                $arr = [
                    "status" => 'invalid',
                    "message" => "Please wait for your previous request to be completed"
                ];
            } else if ($response == "error") {
                $arr = [
                    "status" => 'invalid',
                    "message" => "Problem with server.Please try again"
                ];
            } else if ($response == "success") {
                $arr = [
                    "status" => 'valid',
                    "message" => "Withdraw amount of " . $data['amount'] . " has been requested successfully"
                ];
            }
            echo json_encode($arr);
            die;
        }
    }

//    function list() {
//        $vendor_id = $this->session->userdata('vendor_id');
//        $response = $this->vendor_withdraw_request_model->get($vendor_id);
//        $arr = [
//            "status" => 'valid',
//            "message" => "Withdraw Request List",
//            "data" => $response,
//        ];
//        echo json_encode($arr);
//        die;
//    }

    function wallet_balance() {
        $vendor_id = $this->session->userdata('vendor_id');
        $response = $this->vendor_withdraw_request_model->get_wallet_details($vendor_id);
        $arr = [
            "status" => 'valid',
            "message" => "Wallet Balance",
            "data" => $response
        ];
        echo json_encode($arr);
        die;
    }

//    function wallet_list() {
//        $vendor_id = $this->session->userdata('vendor_id');
//        $response = $this->vendor_withdraw_request_model->get_wallet_list($vendor_id);
//        $arr = [
//            "status" => 'valid',
//            "message" => "Wallet Balance",
//            "data" => $response
//        ];
//        echo json_encode($arr);
//        die;
//    }

    function wallet_list() {

        $filters = array();
        $filters['action'] = $this->input->post('action');
        $filters['from_date'] = $this->input->post('from_date');
        $filters['to_date'] = $this->input->post('to_date');

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : 100;
        $_GET['start'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $total_results = $this->vendor_withdraw_request_model->get_wallet_list_count($filters);

        $filters["limit"] = $limit;
        $filters["start"] = $start;
        $pagination = my_pagination("wallet_history", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
        $data = $this->vendor_withdraw_request_model->get_wallet_list($filters);
        $arr = [
            'status' => "valid",
            "pagination" => $pagination,
            "data" => $data
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function list() {

        $filters = array();
        $filters['withdraw_status'] = $this->input->post('withdraw_status');
        $filters['from_date'] = $this->input->post('from_date');
        $filters['to_date'] = $this->input->post('to_date');

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : 100;
        $_GET['start'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $total_results = $this->vendor_withdraw_request_model->get_count($filters);

        $filters["limit"] = $limit;
        $filters["start"] = $start;
        $pagination = my_pagination("withdraw_history", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
        $data = $this->vendor_withdraw_request_model->get($filters);
        $arr = [
            'status' => "valid",
            "pagination" => $pagination,
            "data" => $data
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

}
