<?php

header('Content-type: application/json');

class Profile extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->session->userdata("vendor_id");
        $vendor_details = $this->vendor_model->get_vendor_details($vendor_id);
        unset($vendor_details->password);
        unset($vendor_details->salt);

        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => "Vendor details",
            "vendor_details" => $vendor_details
        );
        echo json_encode($arr);
    }

    function update() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        // echo json_encode($this->vendor_model->get_logged_vendor_id()); die;
        extract($_POST);
        $data = [
            "business_name" => $business_name,
            "owner_name" => $owner_name,
            "contact_email" => $contact_email,
            "states_id" => $states_id,
            "districts_id" => $districts_id,
            "cities_id" => $cities_id,
            "land_mark" => $land_mark,
            "address" => $address,
            "description" => $description
        ];

        if (!$this->session->userdata("admin_control") && !$this->session->userdata("admin_user_id")) {
            unset($data["minimum_order_amount"]);
        }

        // echo json_encode($data); die;

        if (@$_FILES["display_image"]["name"]) {
            $config['upload_path'] = 'uploads/stores/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1000;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('display_image')) {
                $this->data['error'] = array('error' => $this->upload->display_errors());
                $arr = array(
                    'err_code' => "invalid",
                    "message" => strip_tags($this->data['error']["error"]),
                    "title" => "Invalid",
                    "error" => strip_tags($this->data['error'])
                );
                echo json_encode($arr);
                die;
            } else {
                $uploaded_data = array('upload_data' => $this->upload->data());
                $file_name = $uploaded_data["upload_data"]["file_name"];
                $data["display_image"] = $file_name;

                $file_name = $data["display_image"];
//                if (FILE_STORAGE_AREA == "aws") {
//                    $this->load->library('upload');
//                    $destination = "uploads/stores/" . $vendor_id . "/";
//                    $this->upload->do_upload_manually(LOCAL_FILE_UPLOAD_SOURCE, $file_name, $destination);
//                    $source = "uploads/";
//                    unlink($source . $file_name);
//                }
            }
        }

        if (@$m_display_image != "") {
            $data["display_image"] = $m_display_image;

            $file_name = $data["display_image"];
            if (FILE_STORAGE_AREA == "aws") {
                $this->load->library('upload');
                $destination = DESTINATION_FILE_UPLOAD . "stores/" . $vendor_id . "/";
                $this->upload->do_upload_manually(LOCAL_FILE_UPLOAD_SOURCE, $file_name, $destination);
                $source = "uploads/";
                unlink($source . $file_name);
            }
        }


        if ($this->vendor_model->update_profile($data, $vendor_id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => "Profile details has been updated successfully"
            );
            echo json_encode($arr);

//            $cache_key = "restaurant_" . $vendor_id;
//            $this->cache_model->delete_cache_if_exists($cache_key);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => "Error occured, information not updated"
            );
            echo json_encode($arr);
        }
    }

}
