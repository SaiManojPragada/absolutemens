<?php

class Upload_image extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        if (@$_FILES["image"]["name"]) {
            $config['upload_path'] = FILE_UPLOAD_FOLDER;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1000;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $this->data['error'] = array('error' => $this->upload->display_errors());
//                print_r($this->data['error']);
//                die;
                $arr = array(
                    'err_code' => "invalid",
                    "message" => strip_tags($this->data['error']["error"]),
                    "title" => "Invalid",
                    "error" => strip_tags($this->data['error'])
                );
                echo json_encode($arr);
                die;
            } else {
                $uploaded_data = array('upload_data' => $this->upload->data());
                $file_name = $uploaded_data["upload_data"]["file_name"];
                $data["image"] = $file_name;
            }
        }

        $data["url"] = FILE_UPLOAD_FOLDER_IMG_PATH . $data["image"];

        $arr = array(
            'err_code' => "valid",
            "message" => "Image uploaded",
            "title" => "Uploaded",
            "data" => $data
        );

        echo json_encode($arr);
    }

}
