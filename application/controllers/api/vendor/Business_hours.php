<?php

header('Content-type: application/json');

class Business_hours extends MY_Controller {

    private $model_name = "vendor_business_hours_model";
    private $title = "Business Hours";

    function __construct() {
        parent::__construct();
        /* if ($this->vendor_model->check_for_user_logged() == false) {
          show_forbidden();
          } */
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $business_hours = $this->{$this->model_name}->get($vendor_id);

        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $business_hours
        );
        echo json_encode($arr);
    }

    function add() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        extract($_POST);

        $my_data = [
            "restaurants_id" => $restaurants_id
        ];
        if ($this->input->get_post("request_from") == "mobile") {
            if (isset($_POST["data"])) {
                $business_hours = json_decode($_POST["data"]);
                $arr = [];
                foreach ($business_hours as $item) {
                    $item = (array) $item;
                    $arr[] = [
                        "is_working_day" => $item["is_working_day"],
                        "week_name" => $item["week_name"],
                        "from_time" => $item["from_time"],
                        "to_time" => $item["to_time"]
                    ];
                }
                $my_data["business_hours"] = $arr;
            }
        } else {
            $arr = [];
            foreach ($business_hours as $item) {
                $arr[] = [
                    "is_working_day" => $item["is_working_day"],
                    "week_name" => $item["week_name"],
                    "from_time" => $item["from_time"],
                    "to_time" => $item["to_time"]
                ];
            }
            $my_data["business_hours"] = $arr;
        }


        if ($this->{$this->model_name}->updated_business_hours($my_data)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " saved successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

}
