<?php

header('Content-type: application/json');

class Update_push_notification_token extends MY_Controller {

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $push_notification_key = $this->input->get_post("push_notification_key");

        if (!$push_notification_key) {
            $arr = array('err_code' => "invalid",
                "message" => "Push notification key is required",
                "data" => []
            );
        } else {
            $this->db->set("push_notification_token", $push_notification_key);
            $this->db->where("id", $vendor_id);
            $this->db->update("restaurants");
            $arr = array('err_code' => "valid", "message" => "Push notification device id updated");
        }
        echo json_encode($arr);
    }

}
