<?php

header('Content-type: application/json');

class Update_order_status extends MY_Controller {

    private $title = "Order";

    function __construct() {
        parent::__construct();
        /* if ($this->vendor_model->check_for_user_logged() == false) {
          show_forbidden();
          } */
    }

    // function to get restatunt food items details
    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        if (!$this->input->get_post("restaurant_food_items_id")) {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => "Please select food category"
            );
        }
        $restaurant_food_items_id = $this->input->get_post("restaurant_food_items_id");
        $addon_items = $this->{$this->model_name}->get($restaurant_food_items_id);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $addon_items
        );
        echo json_encode($arr);
    }

    // function when vendor accepts the order
    function accepted() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("ref_id", "Order id", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }
        // $city_id = $this->vendor_model->get_city_by_vendor_id($vendor_id);

        extract($_POST);
        $ref_id = $this->input->get_post("ref_id");


        $order = $this->_get_order_status($ref_id);
        if ($order->service_status != "Pending" && $order->payment_status != "Cancelled" &&
                $order->payment_status != "Not Received") {
            $arr = array('err_code' => "invalid",
                "title" => "Sorry!",
                "message" => "This action not allowed here "
            );
            echo json_encode($arr);
            die;
        }


        $data = [
            "restaurant_order_accepted_at" => THIS_DATE_TIME,
            "service_status" => "Waiting For Delivery Person",
                // "city" => $city_id
        ];

        if ($this->orders_model->update_status_as_accepted($data, $ref_id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " accepted successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    function out_for_delivery() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("ref_id", "Order id", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }

        extract($_POST);
        $data = [
            "service_status" => "On Going",
            "out_for_delivery_at" => THIS_DATE_TIME,
            "delivery_person_name" => $delivery_person_name,
            "delivery_person_contact_number" => $delivery_person_contact_number
        ];

        if ($this->orders_model->update_status_as_out_for_delivery($data, $this->input->get_post("ref_id"))) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " updated successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    function rejected() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("ref_id", "Order id", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }

        $rejected_reason = $this->input->get_post("rejected_reason");
        extract($_POST);
        $ref_id = $this->input->get_post("ref_id");
        $order = $this->_get_order_status($ref_id);

        if ($order->service_status != "Pending") {
            $arr = array('err_code' => "invalid",
                "title" => "Sorry!",
                "message" => "This order already accepted, and now cannot be rejected"
            );
            echo json_encode($arr);
            die;
        }


        $data = [
            "restaurant_order_rejected_at" => THIS_DATE_TIME,
            "service_status" => "Rejected",
            "rejected_reason" => $rejected_reason
        ];


        if ($this->orders_model->update_status_as_rejected($data, $ref_id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => "This order has been " . singular($this->title) . " rejected"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    function mark_as_delivered() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("ref_id", "Order id", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }

        extract($_POST);
        $data = [
            "service_status" => "Completed",
            "delivered_at" => THIS_DATE_TIME,
        ];

        if ($this->orders_model->update_status_as_delivered($data, $this->input->get_post("ref_id"))) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " accepted successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    // function when order is ready 
    function order_is_ready() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("ref_id", "Order id", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }
        // $city_id = $this->vendor_model->get_city_by_vendor_id($vendor_id);

        extract($_POST);
        $ref_id = $this->input->get_post("ref_id");


        $order = $this->_get_order_status($ref_id);
        if ($order->service_status != "Pending" && $order->payment_status != "Cancelled" &&
                $order->payment_status != "Not Received") {
            $arr = array('err_code' => "invalid",
                "title" => "Sorry!",
                "message" => "This action not allowed here "
            );
            echo json_encode($arr);
            die;
        }


        $data = [
            "restaurant_order_accepted_at" => THIS_DATE_TIME,
            "service_status" => "Order Ready",
                // "city" => $city_id
        ];

        if ($this->orders_model->update_status_as_ready_for_pickup($data, $ref_id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " accepted successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    function _get_order_status($ref_id) {
        $this->db->select("service_status, payment_status");
        $this->db->where("ref_id", $ref_id);
        $data = $this->db->get("service_orders")->row();
        return $data;
    }

}
