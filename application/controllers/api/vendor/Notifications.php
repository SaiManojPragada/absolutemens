<?php

header('Content-type: application/json');

class Notifications extends MY_Controller {

    private $title = "Notifications";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
//        extract($_POST);
//
//		$filters = array();
//		
//		$filters["from_date"] = date("Y-m-d");
//		$filters["to_date"] = date("Y-m-d");
//		
//        $total_results = $this->notifications_model->get_vendor_notifications($filters, $vendor_id);
//        
//		$limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
//        $_GET['page'] = $this->input->get_post("start");
//        $start = $this->input->get_post("start") > 1 ? ($this->input->get_post("start") - 1) * $limit : 0;
//
//        $filters["start"] = $start;
//		$filters["limit"] = $limit;
//		
//        $data = $this->notifications_model->get_vendor_notifications($filters, $vendor_id);

        $data = array();

        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $data
        );
        echo json_encode($arr);
    }

}
