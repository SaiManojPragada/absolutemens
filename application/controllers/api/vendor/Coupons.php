<?php

header('Content-type: application/json');

class Coupons extends MY_Controller {

    private $model_name = "vendor_coupons_model";
    private $title = "Vendor Coupons";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }
	
	function global_coupons(){
		
	}

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $categories = $this->{$this->model_name}->get($vendor_id);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $categories
        );
        echo json_encode($arr);
    }

    function add() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        // $this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("name", "Category Name", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }

        extract($_POST);
        $data = [
            "restaurants_id" => $restaurants_id,
            "coupon_code" => $coupon_code,
            "cities_ids" => $cities_ids,
            "start_date" => $start_date,
            "end_date" => $end_date,
            "discount_type" => $discount_type,
            "discount_value" => $discount_value,
            "minimum_to_apply_discount" => $minimum_to_apply_discount,
            "coupon_usage_attempts" => $coupon_usage_attempts,
            "applicable_on" => $applicable_on
        ];

        if ($this->input->get_post("id")) {
            return $this->update($data);
        }

        if ($this->{$this->model_name}->add($data)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " added successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not added, please try again"
            );
            echo json_encode($arr);
        }
    }

    function update($data) {
        $id = $this->input->get_post("id");

        if ($this->{$this->model_name}->update($data, $id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " updated successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    function delete() {
        $id = (int) $this->input->get_post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        echo json_encode($arr);
    }

}
