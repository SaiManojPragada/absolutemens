<?php

header('Content-type: application/json');

class Update_password extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {

        $vendor_id = $this->session->userdata("vendor_id");

        $this->form_validation->set_rules("current_password", "Current Password", "required");
        $this->form_validation->set_rules("password", "Password", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "Invalid details",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }

        $current_password = $this->input->get_post("current_password");
        $password = $this->input->get_post("password");

        if ($this->vendor_model->check_for_current_password_ok($vendor_id, $current_password) == false) {
            $arr = array('err_code' => "invalid", "message" => "Invalid Current Password");
        } else {
            $res = $this->vendor_model->update_password($vendor_id, $password);
            if ($res) {
                $arr = array('err_code' => "valid", "message" => "Password Updated successfully");
            } else {
                $arr = array('err_code' => "invalid", "error_type" => "account_not_existed", "message" => "Sorry, this account does not existed");
            }
        }
        echo json_encode($arr);
    }

}
