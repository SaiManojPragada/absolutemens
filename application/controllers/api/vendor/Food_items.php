<?php

header('Content-type: application/json');

class Food_items extends MY_Controller {

    private $model_name = "vendor_food_items_model";
    private $title = "Products";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $filters = [];
        $filters["verification_status"] = $this->input->get_post("verification_status");
        $filters["restaurant_categories_id"] = $this->input->get_post("restaurant_categories_id");
        $filters["search_key"] = $this->input->get_post("search_key");
        $filters["most_selling"] = $this->input->get_post("most_selling");
        $filters["food_type"] = $this->input->get_post("food_type");

        $total_results = $this->{$this->model_name}->get($vendor_id, $filters);

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $filters["limit"] = $limit;
        $filters["start"] = $start;

        $result["total_results_found"] = $total_results;
        $result["total_pages"] = ceil($total_results / ORDERS_PER_PAGE);

        $result["results"] = $this->{$this->model_name}->get($vendor_id, $filters);

        $pagination = my_pagination("vendor/all_orders", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;

        if ($result["results"]) {

            foreach ($result["results"] as $item) {
                $item->available = boolval($item->available);
            }

            $arr = array(
                'err_code' => "valid",
                "title" => "",
                "message" => $this->title . " list",
                "data" => $result,
                "pagination" => $pagination
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                'title' => "Sorry",
                "message" => $filters["search_key"] ? "No product matched with " . $filters["search_key"] : "No products found",
                "data" => $result,
                "pagination" => $pagination
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function add() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("restaurant_categories_id", "Category", "required");
        $this->form_validation->set_rules("item_name", "Food Item Name", "required");
        $this->form_validation->set_rules("actual_price", "Item Price", "required");

        if ($this->input->get_post("request_from") != "mobile") {
            $this->form_validation->set_rules("admin_commission_percentage", "Admin Comission", "required");
            if ($this->input->get_post("admin_commission_percentage") <= 0) {
                $arr = [
                    'err_code' => "invalid_form",
                    'title' => "",
                    "message" => "Admin Commission(%) Required and it must be greater than Zero"
                ];
            }
        }
        if ($this->session->userdata("admin_control") && $this->session->userdata("admin_user_id")) {
            $this->form_validation->set_rules("admin_commission_percentage", "Admin Comission", "required");
            if ($this->input->get_post("admin_commission_percentage") <= 0) {
                $arr = [
                    'err_code' => "invalid_form",
                    'title' => "",
                    "message" => "Admin Commission(%) Required and it must be greater than Zero"
                ];
            }
        }

        $this->form_validation->set_rules("weight", "Weight", "required");
        #$this->form_validation->set_rules("tax_percentage", "Tax Percentage", "required");
        //$this->form_validation->set_rules("offer_percentage", "Offer Percentage", "greater_than[100]|less_than[0]");
        // $this->form_validation->set_rules("available_from_time", "Available From time", "required");
        // $this->form_validation->set_rules("available_to_time", "Available To time", "required");
        //$this->form_validation->set_rules("food_type", "Food type", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => strip_tags(validation_errors())
            ];
            echo json_encode($arr);
            die;
        }

        extract($_POST);

        $available_from_time = "00:00:00";
        $available_to_time = "23:59:59";

        $data = [
            "restaurants_id" => $restaurants_id,
            "restaurant_categories_id" => $restaurant_categories_id,
            "item_name" => $item_name,
            "actual_price" => $actual_price,
            "offer_percentage" => $offer_percentage,
            "description" => $description,
            "available_from_time" => $available_from_time,
            "available_to_time" => $available_to_time,
            "available_to_time" => $available_to_time,
            "is_recommened" => $is_recommened,
            "weight" => isset($weight) ? $weight : 0,
            "tax_percentage" => isset($tax_percentage) ? $tax_percentage : 0,
            "admin_commission_percentage" => isset($admin_commission_percentage) ? $admin_commission_percentage : 0,
            "verification_status" => isset($verification_status) ? $verification_status : "Pending",
            "attachment_required" => isset($attachment_required) ? $attachment_required : 0,
            "food_type" => isset($food_type) ? $food_type : "",
            "sku_code" => isset($sku_code) ? $sku_code : "",
            "ean_code" => isset($ean_code) ? $ean_code : "",
        ];

        if (!$this->session->userdata("admin_control")) {
            unset($data["verification_status"]);
        }

        if (@$_FILES["image"]["name"]) {
            $config['upload_path'] = FILE_UPLOAD_FOLDER;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1000;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $this->data['error'] = array('error' => $this->upload->display_errors());
//                print_r($this->data['error']);
//                die;
                $arr = array(
                    'err_code' => "invalid",
                    "message" => strip_tags($this->data['error']["error"]),
                    "title" => "Invalid",
                    "error" => strip_tags($this->data['error']["error"])
                );
                echo json_encode($arr);
                die;
            } else {
                $uploaded_data = array('upload_data' => $this->upload->data());
                $file_name = $uploaded_data["upload_data"]["file_name"];
                $data["image"] = $file_name;
                if (FILE_STORAGE_AREA == "aws") {
                    $this->load->library('upload');
                    $destination = DESTINATION_FILE_UPLOAD . "product_items/" . $restaurants_id . "/";
                    $this->upload->do_upload_manually(LOCAL_FILE_UPLOAD_SOURCE, $file_name, $destination);
                    $source = "uploads/";
                    unlink($source . $file_name);
                }
            }
        }

        if (@$m_image != "") {
            $data["image"] = $m_image;
            $file_name = $data["image"];
            if (FILE_STORAGE_AREA == "aws") {
                $this->load->library('upload');
                $destination = DESTINATION_FILE_UPLOAD . "product_items/" . $restaurants_id . "/";
                $this->upload->do_upload_manually(LOCAL_FILE_UPLOAD_SOURCE, $file_name, $destination);
                $source = "uploads/";
                unlink($source . $file_name);
            }
        }

        if ($this->input->get_post("id")) {
            return $this->update($data);
        }

        $response = $this->{$this->model_name}->add($data);
        if ($response) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " added successfully",
                "data" => $response
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => singular($this->title) . " not added, please try again"
            );
            echo json_encode($arr);
        }
    }

    function update($data) {
        $id = $this->input->get_post("id");

        if ($this->{$this->model_name}->update($data, $id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " updated successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    function delete() {
        $id = (int) $this->input->get_post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        echo json_encode($arr);
    }

    function preview() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $items = $this->{$this->model_name}->get_food_menu($vendor_id);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $items
        );
        echo json_encode($arr);
    }

    function by_category_id() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $category_id = $this->input->get_post("category_id");

        $filters = [];
        $filters["verification_status"] = $this->input->get_post("verification_status");

        if ($this->input->get_post("verification_status") == "All") {
            $filters["verification_status"] = "";
        }
        $filters["restaurant_categories_id"] = $category_id;

        $total_results = $this->{$this->model_name}->get_food_items_for_category($category_id, $filters);

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $filters["limit"] = $limit;
        $filters["start"] = $start;

        $result["total_results_found"] = $total_results;
        $result["total_pages"] = ceil($total_results / ORDERS_PER_PAGE);

        // $filters["start"] = 0;
        // $filters["limit"] = 100000; 

        $result["results"] = $this->{$this->model_name}->get_food_items_for_category($category_id, $filters);

        $pagination = my_pagination("vendor/all_orders", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;

        if ($result) {
            $arr = array(
                'err_code' => "valid",
                "title" => "",
                "message" => $this->title . " list",
                "data" => $result,
                //"data" => $result["results"],
                "pagination" => $pagination
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "data" => [],
                "message" => "No products found"
            );
        }
        $arr = json_encode($arr);
        $response_arry = json_decode(str_replace('null', '""', $arr));
        if (json_last_error_msg() == "Syntax error") {
            $response_arry = json_decode(str_replace('"null"', '""', $arr));
        }

        echo json_encode($response_arry);
    }

    function mark_category_as_sold_out_or_available() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $category_id = $this->input->get_post("category_id");
        $available = $this->input->get_post("available");
        $items = $this->{$this->model_name}->update_category_sold_out_or_available($vendor_id, $category_id, $available);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => "Status updated"
        );
        echo json_encode($arr);
    }

    function mark_food_item_as_sold_out_or_available() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurant_food_items_id = $this->input->get_post("id");
        $available = $this->input->get_post("available");
        $items = $this->{$this->model_name}->update_food_item_sold_out_or_available($vendor_id, $restaurant_food_items_id, $available);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => "Status updated"
        );
        echo json_encode($arr);
    }

}
