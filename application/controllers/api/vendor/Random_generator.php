<?php

class Random_generator extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $table = $this->input->post('table');
        $column_name = $this->input->post('column');
        $random = $this->random($table, $column_name);
        $arr = array(
            'err_code' => "valid",
            "message" => "Random Number",
            "data" => $random
        );
        echo json_encode($arr);
    }

    function random($table, $column_name) {
        $random = rand(0000000, 9999999);
        $this->db->where($column_name . " LIKE '%" . $random . "%'");
        $check = $this->db->get($table)->row();
        if ($check) {
            $this->random($table, $column_name);
        } else {
            return $random;
        }
    }

}
