<?php

header('Content-type: application/json');

class Bank_details extends MY_Controller {

    private $model_name = "vendor_bank_details_model";
    private $title = "Bank Details";

    function __construct() {
        parent::__construct();
        /* if ($this->vendor_model->check_for_user_logged() == false) {
          show_forbidden();
          } */
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $bank_details = $this->{$this->model_name}->get($vendor_id);

        if ($bank_details) {
            $arr = array('err_code' => "valid",
                "title" => "",
                "message" => $this->title,
                "data" => $bank_details
            );
        } else {
            $arr = array('err_code' => "invalid",
                "title" => "",
                "message" => "Bank details not found"
            );
        }
        echo json_encode($arr);
    }

    function add() {
        $vendor_id = $this->session->userdata("vendor_id");
        $restaurants_id = $vendor_id;
        // $this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("account_name", "Account Name", "required");
        $this->form_validation->set_rules("bank_name", "Bank Name", "required");
        $this->form_validation->set_rules("branch", "Branch Name", "required");
        $this->form_validation->set_rules("ifsc_code", "IFSC Code", "required");
        $this->form_validation->set_rules("account_number", "Account Number", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            $this->response($arr);
            die;
        }
        extract($_POST);
        $data = [
            "vendors_id" => $restaurants_id,
            "account_name" => trim($account_name),
            "account_number" => trim($account_number),
            "bank_name" => trim($bank_name),
            "branch" => trim($branch),
            "ifsc_code" => trim(strtoupper($ifsc_code)),
            "status" => false
        ];
        if ($this->input->get_post("id")) {
            return $this->update($data);
        }
        if ($this->{$this->model_name}->add($data)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " added successfully"
            );
        } else {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => singular($this->title) . " not added, please try again"
            );
        }
        echo json_encode($arr);
    }

    function update($data) {
//        if ($this->session->userdata("admin_control") && $this->session->userdata("admin_user_id")) {
//            $id = $this->input->get_post("id");
//            if ($this->{$this->model_name}->update($data, $id)) {
//                $arr = array(
//                    'err_code' => "valid",
//                    "title" => "Success",
//                    "message" => singular($this->title) . " updated successfully"
//                );
//            } else {
//                $arr = array('err_code' => "invalid",
//                    "title" => "Error",
//                    "message" => singular($this->title) . " not updated, please try again"
//                );
//            }
//        } else {
//            $arr = array('err_code' => "invalid",
//                "title" => "Sorry!",
//                "message" => "For Updating your bank details please contact Support"
//            );
//        }
        $id = $this->input->get_post("id");
        if ($this->{$this->model_name}->update($data, $id)) {
            $arr = array(
                'err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " updated successfully"
            );
        } else {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
        }
        echo json_encode($arr);
    }

    function ifsc_details() {

        $ifsc_code = $this->input->get_post("ifsc_code");

        if (strlen(trim($ifsc_code)) != 11) {
            $arr = [
                "err_code" => "invalid",
                "title" => "IFSC details not found",
                "message" => "IFSC details not found",
                "data" => [
                    "ifsc_detais" => []
                ]
            ];
            echo json_encode($arr);
            die;
        }


        $URL = "https://ifsc.razorpay.com/" . $ifsc_code;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $URL);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code == 404) {
            $arr = [
                "err_code" => "invalid",
                "title" => "IFSC details not found",
                "message" => "IFSC details not found",
                "data" => [
                    "ifsc_detais" => []
                ]
            ];
        } else if ($code == 200) {
            if ($response != "") {
                $arr = [
                    "err_code" => "valid",
                    "title" => "IFSC details",
                    "message" => "IFSC details",
                    "data" => [
                        "ifsc_detais" => json_decode($response)
                    ]
                ];
            }
        } else {
            $arr = [
                "err_code" => "invalid",
                "title" => "IFSC details not found",
                "message" => "IFSC details not found",
                "data" => [
                    "ifsc_detais" => []
                ]
            ];
        }
        echo json_encode($arr);
    }

}
