<?php

class Products_images extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $id = $this->input->get_post('id');
        $this->db->order_by("id", "desc");
        $this->db->where('product_id', $id);
        $res = $this->db->get("products_images")->result();
        if ($res) {
            foreach ($res as $item) {
                $this->db->where("id", $item->product_id);
                $item->prodcut_details = $this->db->get("products")->row();
            }
            $arr = array(
                'err_code' => "valid",
                "message" => " Images Found",
                "data" => $res
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => " No Images Found"
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function post_image() {
        if (!empty($this->input->get_post('image'))) {
            $converted_image = common_base64image($this->input->get_post('image'), 'products');
            $_POST['image'] = $converted_image;
        } else {
            unset($_POST['image']);
        }
        $this->db->set('created_at', time());
        $this->db->set('updated_at', time());
        $this->db->set($this->input->post());
        if ($this->db->insert("products_images")) {
            // success
            $arr = array('err_code' => 'valid', 'message' => "Product Image Added Successfully");
        } else {
            //failed
            $arr = array('err_code' => 'invalid', 'message' => "Inserted Failed");
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

    function delete() {
        $id = $this->input->get_post('id');
        $image = $this->input->get_post('image');
        if (unlink($image)) {
            $this->db->where("id", $id);
            if ($this->db->delete("products_images")) {
                $arr = array('err_code' => 'valid', 'message' => "Image Has Been Deleted");
            } else {
                $arr = array('err_code' => 'invalid', 'message' => "Unable to Delete image");
            }
        } else {
            $arr = array('err_code' => 'invalid', 'message' => "Unable to Delete image");
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

}
