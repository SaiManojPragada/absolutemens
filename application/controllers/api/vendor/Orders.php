<?php

header('Content-type: application/json');

class Orders extends MY_Controller {

    private $title = "Orders";

    function __construct() {
        parent::__construct();
    }

    function index() {
        $vendor_id = $this->session->userdata("vendor_id");
        extract($_POST);

        $filters = array();

        $filters["from_date"] = date("Y-m-d", strtotime("-2 day"));
        $filters["to_date"] = date("Y-m-d");

        if ($this->input->get_post('request_from') == "mobile") {
            $filters["from_date"] = $this->input->get_post("from_date");
            $filters["to_date"] = $this->input->get_post("to_date");
        }

        $filters['type'] = $this->input->get_post('type');
        $total_results = $this->orders_model->get_vendor_orders($filters, $vendor_id);
        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("start");
        $start = $this->input->get_post("start") > 1 ? ($this->input->get_post("start") - 1) * $limit : 0;

        $filters["start"] = $start;
        $filters["limit"] = $limit;
        $filters["limit"] = 50;

        $pagination = my_pagination("search_result.php", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);

        $data = $this->orders_model->get_vendor_orders($filters, $vendor_id);

        if ($this->input->get_post("request_from") == "mobile") {
            $mobile_results = [];

            foreach ($data as $item) {
                $mobile_results[] = (object) [
                            "ref_id" => $item->ref_id,
                            "cart_items" => count($item->cart_items),
                            "cart_items_list" => $item->cart_items,
                            "grand_total" => $item->grand_total,
                            "service_date" => $item->service_date,
                            "cart_items_txt" => $item->cart_items_txt,
                            "service_status" => $item->service_status
                ];
            }
            $data = $mobile_results;
        }

        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => count($data) > 0 ? "Orders List" : "No orders found",
            "data" => $data
        );
        echo json_encode($arr);
    }

}
