<?php

class Earnings extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('vendor_earnings_model');
    }

    function index() {

        $filters = array();
        $filters['type'] = $this->input->post('type');
        $filters['from_date'] = $this->input->post('from_date');
        $filters['to_date'] = $this->input->post('to_date');

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : 100;
        $_GET['start'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
        $total_results = $this->vendor_earnings_model->get_count($filters);

        $filters["limit"] = $limit;
        $filters["start"] = $start;
        $pagination = my_pagination("vendor_earnings", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
        $data = $this->vendor_earnings_model->get($filters);
        $arr = [
            'status' => "valid",
            "pagination" => $pagination,
            "data" => $data
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

}
