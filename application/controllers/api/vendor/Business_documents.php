<?php

header('Content-type: application/json');

class Business_documents extends MY_Controller {

    private $model_name = "vendor_business_documents_model";
    private $title = "Documents";

    function __construct() {
        parent::__construct();
        /*if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }*/
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $categories = $this->{$this->model_name}->get($vendor_id);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $categories
        );
        echo json_encode($arr);
    }

}
