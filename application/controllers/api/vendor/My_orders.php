<?php

class My_orders extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $param = $this->input->post("param");
        $where = "order_status='order_placed_by_admin' OR order_status='order_dispatched_by_vendor' OR  order_status='order_delivered' OR order_status='order_cancelled_by_vendor' OR order_status='order_completed'";
        if ($param == "pending") {
            $where = "order_status='order_placed_by_admin'";
        }
        if ($param == "dispatched") {
            $where = "order_status='order_dispatched_by_vendor'";
        }
        if ($param == "delivered") {
            $where = "order_status='order_delivered' OR order_status='order_completed'";
        }
        $this->db->order_by('id', 'desc');
        $this->db->where($where);
        $data = $this->db->get("orders")->result();
        $out = [];
        foreach ($data as $item) {
            $item->order_date = date('d-M-Y', strtotime($item->order_date));
            $user_order = $this->u_model->get_nostatcheck_row("subscriptions_transactions", array("id" => $item->subscription_transaction_id));
            $item->user_order = $user_order;
            $item->delivery_address = strip_tags($this->u_model->get_nostatcheck_row("admin_addresses", array("id" => $user_order->admin_address_id))->address);
            $items = $this->u_model->get_data("order_products", array("order_id" => $item->id, "vendor_id" => $this->session->userdata("vendor_id"), 'approved_by_fashion_manager' => 'yes'));
            if ($items) {
                foreach ($items as $prd) {
                    $prd->product_details = $this->u_model->get_nostatcheck_row("products", array("id" => $prd->product_id));
                    $prd->product_images = $this->u_model->get_data("products_images", array("product_id" => $prd->product_id));
                    $prd->inventory = $this->get_product_inventory_sizes($item->order_id);
                }
                $item->products = $items;
                array_push($out, $item);
            } else {
                unset($item);
            }
        }
        if ($data) {
            $arr = array(
                "err_code" => "valid",
                "data" => $out
            );
        } else {
            $arr = array(
                "err_code" => "invalid",
                "message" => "No Orders Found"
            );
        }
        echo json_encode($arr);
    }

    function get_product_inventory_sizes($order_id) {
        $selected_sizes = $this->db->get_where('subscriptions_transactions', ['unq_id' => $order_id])->row()->selected_sizes;
        $selected_sizes_arr = json_decode($selected_sizes);
        $arr = [];
        foreach ($selected_sizes_arr as $key => $value) {
            $obj = new stdClass();
            $obj->category = $this->db->get_where('categories', ['id' => $key])->row()->name;
            $obj->size = $this->db->get_where('sizes', ['id' => $value])->row()->title;
            $obj->category_id = $key;
            $arr[] = $obj;
        }
        return $arr;
    }

    function update_order_status() {
        $id = $this->input->post("id");
        $order_id = $this->input->post("order_id");
        $user_id = $this->session->userdata("vendor_id");
        $user_data = $this->u_model->get_nostatcheck_row("vendors", array("id" => $user_id));
        unset($_POST['id']);
        unset($_POST['order_id']);
        $update = $this->u_model->update_data("orders", $this->input->post(), array("id" => $id));
        if ($update) {
            $dd = array(
                "order_real_id" => $id,
                "order_id" => $order_id,
                "log" => "Order '" . $this->input->post("order_status") . "' action performed by Vendor " . $user_data->owner_name . "(" . $user_data->ref_code . "), $user_data->contact_email, $user_data->contact_number",
                "action_user_id" => $user_id,
                "table_name" => "vendors",
                "order_status" => $this->input->post("order_status"),
                "created_at" => time(),
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->u_model->post_data("orders_log", $dd);
            $arr = array(
                "err_code" => "valid",
                "message" => "Order Status Updated."
            );
        } else {
            $arr = array(
                "err_code" => "invalid",
                "message" => "Unable to Perform the Action Please Try again."
            );
        }
        echo json_encode($arr);
    }

    function update_order_product_status() {
        $id = $this->input->post("id");
        $real_id = $this->input->post("real_id");
        $order_id = $this->input->post("order_id");
        $user_id = $this->session->userdata("vendor_id");
        $user_data = $this->u_model->get_nostatcheck_row("vendors", array("id" => $user_id));
        unset($_POST['id']);
        unset($_POST['order_id']);
        unset($_POST['real_id']);
        $update = $this->u_model->update_data("order_products", array("order_status" => $this->input->post('order_status')), array("id" => $id));
        if ($update) {
            $product_id = $this->db->get_where('order_products', ['id' => $id])->row()->product_id;
            $product_name = $this->db->get_where('products', ['id' => $product_id])->row()->title;
            $product_code = $this->db->get_where('products', ['id' => $product_id])->row()->product_id;
            $dd = array(
                "order_real_id" => $real_id,
                "order_product_id" => $id,
                "order_id" => $order_id,
                "log" => "Order '" . $this->input->post("order_status") . "' action performed by Vendor for product $product_name ($product_code) by" . $user_data->owner_name . "(" . $user_data->ref_code . "), $user_data->contact_email, $user_data->contact_number",
                "action_user_id" => $user_id,
                "table_name" => "vendors",
                "order_status" => $this->input->post("order_status"),
                "created_at" => time(),
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->u_model->post_data("orders_log", $dd);
            $arr = array(
                "err_code" => "valid",
                "message" => "Order Product Status Updated."
            );
        } else {
            $arr = array(
                "err_code" => "invalid",
                "message" => "Unable to Perform the Action Please Try again."
            );
        }
        echo json_encode($arr);
    }

    function get_logs() {
        $this->db->where($this->input->post());
        $log = $this->db->get("orders_log")->result();
        if ($log) {
            $arr = array(
                "err_code" => "valid",
                "data" => array(
                    "order_id" => $log[0]->order_id,
                    "logs" => $log
                )
            );
        } else {
            $arr = array(
                "err_code" => "invalid",
                "message" => "No Logs Found for this Order"
            );
        }
        echo json_encode($arr);
    }

}
