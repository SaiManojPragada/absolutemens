<?php

header('Content-type: application/json');

class All_orders extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {

        extract($_REQUEST);
        $restaurants_id = $this->vendor_model->get_logged_vendor_id();

        $filters["q"] = $q;
        $filters["sort_by"] = $sort_by;
        $filters["from_date"] = @$from_date != "" ? $from_date : date("Y-m-d");
        $filters["to_date"] = @$to_date != "" ? $to_date : date("Y-m-d");
        $filters["type"] = @$type != "" ? $type : ""; //service _status
        $filters["settlement_status"] = @$settlement_status != "" ? $settlement_status : "";
        $filters["restaurants_id"] = $restaurants_id;

        $total_results = $this->orders_model->get_vendor_orders($filters, $restaurants_id);

        $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : ORDERS_PER_PAGE;
        $_GET['page'] = $this->input->get_post("page");
        $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;

        $filters["limit"] = $limit;
        $filters["start"] = $start;


        $data = $this->orders_model->get_vendor_orders($filters, $restaurants_id);
        if ($this->input->get_post("request_from") == "mobile") {
            $mobile_results = [];

            foreach ($data as $item) {
                $mobile_results[] = (object) [
                            "ref_id" => $item->ref_id,
                            "cart_items" => count($item->cart_items),
                            "grand_total" => $item->grand_total,
                            "service_date" => $item->service_date,
                            "cart_items_txt" => $item->cart_items_txt,
                            "service_status" => $item->service_status,
                            "settlement_ref_id" => $item->settlement_ref_id,
                            "settlement_remarks" => $item->settlement_remarks,
                            "settlement_status" => $item->settlement_status,
                            "settled_amount" => $item->payable_amount_to_restaurant,
                            "settled_date" => $item->settled_date,
                ];
            }
            $data = $mobile_results;
        }


        $pagination = my_pagination("vendor/all_orders", $limit, $total_results, true);
        $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
        $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
        $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;


        $result["total_results_found"] = $total_results;

        $result["total_pages"] = ceil($total_results / ORDERS_PER_PAGE);

        $result["results"] = $data;

        if ($data) {
            $arr = array('err_code' => "valid",
                "title" => "",
                "message" => "Orders list",
                "data" => $result,
                "pagination" => $pagination
            );
        } else {
            $arr = array('err_code' => "invalid",
                "title" => "",
                "message" => "No orders data found",
                    // "data" => [],
                    //"pagination" => array("pagination" => [])
            );
        }
        echo json_encode($arr);
    }

}
