<?php

header('Content-type: application/json');

class Food_item_images extends MY_Controller {

    private $model_name = "vendor_food_item_images_model";
    private $title = "Item Images";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        if (!$this->input->get_post("restaurant_food_items_id")) {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => "Please select item"
            );
        }
        $restaurant_food_items_id = $this->input->get_post("restaurant_food_items_id");
        $images = $this->{$this->model_name}->get($restaurant_food_items_id);
        $arr = array(
            'err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $images
        );
        echo json_encode($arr);
    }

    function add() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("restaurant_food_items_id", "Food item", "required");
        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }

        $data = [
            "restaurant_food_items_id" => $this->input->get_post('restaurant_food_items_id'),
            "restaurants_id" => $vendor_id,
        ];

        if (@$_FILES["image"]["name"]) {
            $config['upload_path'] = FILE_UPLOAD_FOLDER;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1000;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $this->data['error'] = array('error' => $this->upload->display_errors());
//                print_r($this->data['error']);
//                die;
                $arr = array(
                    'err_code' => "invalid",
                    "message" => strip_tags($this->data['error']["error"]),
                    "title" => "Invalid",
                    "error" => strip_tags($this->data['error']["error"])
                );
                echo json_encode($arr);
                die;
            } else {
                $uploaded_data = array('upload_data' => $this->upload->data());
                $file_name = $uploaded_data["upload_data"]["file_name"];
                $data["image"] = $file_name;
                if (FILE_STORAGE_AREA == "aws") {
                    $this->load->library('upload');
                    $destination = DESTINATION_FILE_UPLOAD . "product_items/" . $restaurants_id . "/";
                    $this->upload->do_upload_manually(LOCAL_FILE_UPLOAD_SOURCE, $file_name, $destination);
                    $source = "uploads/";
                    unlink($source . $file_name);
                }
            }
        }


        $response_id = $this->{$this->model_name}->add($data);
        if ($response_id) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " added successfully",
                "data" => [
                    "id" => $response_id,
                    "image" => PRODUCT_ITEMS_IMG_PATH . $restaurants_id . "/" . $file_name,
                ]
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not added, please try again"
            );
            echo json_encode($arr);
        }
    }

    function delete() {
        $id = (int) $this->input->get_post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        echo json_encode($arr);
    }

}
