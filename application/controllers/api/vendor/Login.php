<?php

header('Content-type: application/json');

class Login extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        //Inputs: username, password  
        //Table: restaurants
        $username = $this->input->get_post('username');
        $this->db->select("id, status");
        //$data = $this->db->get_where("restaurants", ["mobile" => $username])->row();
        $data = $this->db->get_where("vendors", ["contact_email" => $username])->row();
        if ($data) {

//            $this->db->select("id");
//            $this->db->where("is_pharmacy", 1);
//            $cids = $this->db->get("cuisine_types")->result();
//
//            $this->db->select("cuisine_types_ids");
//            $this->db->where("id", $data->id);
//            $rcids = $this->db->get("restaurants")->row()->cuisine_types_ids;
//
//            $rcids = explode(",", $rcids);
//            $data->attachment_required = 0;
//
//            foreach ($cids as $item) {
//                if (in_array($item->id, $rcids)) {
//                    $data->attachment_required = 1;
//                }
//            }
//
//
//            $_REQUEST['current_password'] = $this->input->get_post('password');

            if ($data->status == 0) {
                $arr = array('err_code' => "invalid", "error_type" => "account_not_existed", "message" => "Sorry, your account is suspended, please contact admin");
            } else if ($this->vendor_model->check_for_current_password_ok($data->id, $this->input->get_post('password')) == false) {
                $arr = array('err_code' => "invalid", "data" => $this->data["user"], "message" => "Invalid Current Password");
            } else {
                $this->vendor_login_model->create_login_session($data);
                unset($data->password);
                unset($data->salt);
                unset($data->id);
                unset($data->status);

                $arr = array('err_code' => "valid", "error_type" => "account_existed", "message" => "Login Success", "data" => $data);
            }
        } else {
            $arr = array('err_code' => "invalid", "error_type" => "account_not_existed", "message" => "Sorry, this account does not exist");
        }
        echo json_encode($arr);
    }

}
