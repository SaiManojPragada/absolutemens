<?php

class Online_or_offline_status extends MY_Controller {

    function index() {
        $vendor_id = $this->session->userdata("vendor_id");
        $response = $this->vendor_online_offline_model->get_status($vendor_id);
        $arr = array('err_code' => "valid",
            "title" => "Restaurant Status",
            "message" => "Status updated",
            "data" => $response
        );
        echo json_encode($arr);
    }

    function update() {
        $vendor_id = $this->session->userdata("vendor_id");
        extract($_POST);
        if ($is_online == "false" || $is_online == false) {
            $is_online = 0;
        } else {
            $is_online = 1;
        }

        $this->db->set("is_online", $is_online);
        $this->db->where("id", $vendor_id);
        $this->db->update("vendors");

//        $cache_key = "restaurant_" . $vendor_id;
//        $this->cache_model->delete_cache_if_exists($cache_key);

        $this->index();
    }

}
