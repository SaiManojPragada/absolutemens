<?php

class Check_version extends REST_Controller {

    function index_post() {

        if ($this->input->get_post("source") == "ios_app") {
            return $this->ios_post();
        }
        $current_version = $this->input->get_post("current_version");
        if (ENVIRONMENT == "production") {
            $latest_version_code = 1;
        } else {
            $latest_version_code = 1;
        }

        if ($current_version < $latest_version_code) {
            $arr = [
                'err_code' => "valid",
                'title' => "Success",
                "message" => "Latest Version Available, please update your app"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "Success",
                "message" => "Your using latest version"
            ];
        }
        $this->response($arr);
    }

    function ios_post() {
        $current_version = $this->input->get_post("current_version");
        if (ENVIRONMENT == "production") {
            $latest_version_code = 1;
        } else {
            $latest_version_code = 1;
        }
        if ($current_version < $latest_version_code) {
            $arr = [
                'err_code' => "valid",
                'title' => "Success",
                "message" => "Latest Version Available, please update your app"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "Success",
                "message" => "Your using latest version"
            ];
        }
        $this->response($arr);
    }

}
