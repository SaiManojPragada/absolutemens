<?php

header('Content-type: application/json');

class Food_item_addons extends MY_Controller {

    private $model_name = "vendor_food_item_addons_model";
    private $title = "Food Items";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        if (!$this->input->get_post("restaurant_food_items_id")) {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => "Please select food category"
            );
        }
        $restaurant_food_items_id = $this->input->get_post("restaurant_food_items_id");
        $addon_items = $this->{$this->model_name}->get($restaurant_food_items_id);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $addon_items
        );
        echo json_encode($arr);
    }

    function add() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("restaurant_food_items_id", "Food item", "required");
        $this->form_validation->set_rules("name", "Add on Name", "required");
        $this->form_validation->set_rules("price", "Price", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }

        extract($_POST);
        $data = [
            "restaurant_food_items_id" => $restaurant_food_items_id,
            "name" => $name,
            "price" => $price,
            "tax_percentage" => $tax_percentage,
            "is_mandatory" => $is_mandatory
        ];



        if ($this->input->get_post("id")) {
            return $this->update($data);
        }

        if ($this->{$this->model_name}->add($data, $vendor_id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " added successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not added, please try again"
            );
            echo json_encode($arr);
        }
    }

    function update($data) {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $id = $this->input->get_post("id");

        if ($this->{$this->model_name}->update($data, $id, $vendor_id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " updated successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    function delete() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        $id = (int) $this->input->get_post("id");
        $response = $this->{$this->model_name}->delete($id, $restaurants_id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        echo json_encode($arr);
    }

    function mark_food_addon_as_sold_out_or_available() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurant_food_addon_id = $this->input->get_post("id");
        $available = $this->input->get_post("available");
        $this->{$this->model_name}->update_food_addon_sold_out_or_available($vendor_id, $restaurant_food_addon_id, $available);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => "Status updated"
        );
        echo json_encode($arr);
    }

}
