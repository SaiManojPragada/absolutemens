<?php

header('Content-type: application/json');

class Update_firebase_token extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        extract($_POST);
        $vendor_id = $this->vendor_model->get_logged_vendor_id();

        if (!$this->input->get_post("firebase_token")) {
            $arr = array('err_code' => "invalid", "error_type" => "firebase_token_required", "message" => "Please provide firebase token");
            echo json_encode($arr);
            die;
        }

        $this->db->where("id", $vendor_id);
        $this->db->set("push_notification_token", $firebase_token);
        $response = $this->db->update("restaurants");

        if ($response) {
            $arr = array('err_code' => "valid", "message" => "Token updated");
            echo json_encode($arr);
            die;
        } else {
            $arr = array('err_code' => "invalid", "message" => "Token not updated");
            echo json_encode($arr);
            die;
        }
    }

}
