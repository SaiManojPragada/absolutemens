<?php

header('Content-type: application/json');

class Gallery extends MY_Controller {

    private $model_name = "vendor_gallery_model";
    private $title = "Gallery item";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->session->userdata("vendor_id");
        $gallery = $this->{$this->model_name}->get($vendor_id);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $gallery
        );
        echo json_encode($arr);
    }

    function add() {
        $vendor_id = $this->session->userdata("vendor_id");
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("title", "Image title", "required");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }

        extract($_POST);
        $data = [
            "vendors_id" => $vendor_id,
            "title" => $title
        ];
        $up_path = 'uploads/stores/gallery/' . $vendor_id . '/';
        if (@$_FILES["image"]["name"]) {
            if (!file_exists($up_path)) {
                mkdir($up_path);
            }
            $config['upload_path'] = $up_path;
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 1000;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
                $this->data['error'] = array('error' => $this->upload->display_errors());
//                print_r($this->data['error']);
//                die;
                $arr = array(
                    'err_code' => "invalid",
                    "message" => strip_tags($this->data['error']["error"]),
                    "title" => "Invalid",
                    "error" => strip_tags($this->data['error'])
                );
                echo json_encode($arr);
                die;
            } else {
                $uploaded_data = array('upload_data' => $this->upload->data());
                $file_name = $uploaded_data["upload_data"]["file_name"];
                $data["image"] = $file_name;
            }
        }

        if ($this->input->get_post("id")) {
            return $this->update($data);
        }

        if ($this->{$this->model_name}->add($data)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " added successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not added, please try again"
            );
            echo json_encode($arr);
        }
    }

    function update($data) {
        $id = $this->input->get_post("id");

        if ($this->{$this->model_name}->update($data, $id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " updated successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    function delete() {
        $id = (int) $this->input->get_post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        echo json_encode($arr);
    }

}
