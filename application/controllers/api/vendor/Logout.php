<?php

header('Content-type: application/json');

class Logout extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();

        $this->db->set("push_notification_token", "");
        $this->db->where("id", $vendor_id);
        $this->db->update("restaurants");

        $arr = array(
            'err_code' => "valid",
            "title" => "",
            "message" => "Logout successfully"
        );
        echo json_encode($arr);
    }

}
