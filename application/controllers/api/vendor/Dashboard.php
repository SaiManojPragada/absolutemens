<?php

header('Content-type: application/json');

class Dashboard extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();

//        $stats["total_orders"] = $this->orders_model->get_vendor_total_orders($vendor_id);
//        $stats["accepted_orders"] = $this->orders_model->get_vendor_accepted_orders($vendor_id);
//        $stats["rejected_orders"] = $this->orders_model->get_vendor_rejected_orders($vendor_id);
//
//        $stats["total_earnings"] = $this->orders_model->get_vendor_earnings_from_orders($vendor_id);
//        $stats["total_pending_payout"] = $this->orders_model->get_vendor_pending_payout_from_orders($vendor_id);
//        $stats["total_paid_payout"] = $this->orders_model->get_vendor_paid_payout_from_orders($vendor_id);
//        $stats["total_completed_orders"] = $this->orders_model->get_vendor_completed_orders($vendor_id);
//        $stats["total_cancelled_orders"] = $this->orders_model->get_vendor_cancelled_orders($vendor_id);
//
//        $stats["total_food_menu_items"] = $this->vendor_food_items_model->get_food_menu_items_count($vendor_id);
//        $stats["total_avilable_items"] = $this->vendor_food_items_model->get_total_available_items_count($vendor_id);
//        $stats["total_sold_out_items"] = $this->vendor_food_items_model->get_total_sold_out_items_count($vendor_id);
//
//        $stats["total_approved_items"] = $this->vendor_food_items_model->get_total_approved_items_count($vendor_id);
//        $stats["total_pending_items"] = $this->vendor_food_items_model->get_total_pending_items_count($vendor_id);
//
//        $stats["business_hours"] = $this->vendor_business_hours_model->get_working_days_count($vendor_id, null);
//        $stats["profile"] = $this->vendor_model->get_vendor_details($vendor_id);
//
//        $filters = [];
//        $filters["from_date"] = date("Y-m-d");
//        $filters["to_date"] = date("Y-m-d");
//
//        $week_name = date('l');
//        $waiting_for_dp = $this->orders_model->get_vendor_accepted_orders($vendor_id, $filters);
//        $waiting_for_pickup = $this->orders_model->get_vendor_waiting_for_pickup($vendor_id, $filters);
//
//        $stats["today_date"] = date("Y-m-d");
//        $stats["today_business_hours"] = $this->vendor_business_hours_model->get_working_days_count($vendor_id, $week_name);
//        $stats["today_new_orders"] = $this->orders_model->get_vendor_pending_orders($vendor_id, $filters);
//        $stats["today_preparing_orders"] = $waiting_for_dp + $waiting_for_pickup;
//        $stats["today_out_for_delivery_orders"] = $this->orders_model->get_vendor_out_for_delivery($vendor_id, $filters);
//        $stats["today_completed_orders"] = $this->orders_model->get_vendor_total_completed_orders($vendor_id, $filters);
//        $stats["today_rejected_orders"] = $this->orders_model->get_vendor_total_rejected($vendor_id, $filters);
//        $stats["today_earnings"] = $this->orders_model->get_vendor_earnings_from_orders($vendor_id, $filters);
//        $stats["today_sale_amount"] = $this->orders_model->get_vendor_sale_amount_from_orders($vendor_id, $filters);
//
//        $stats["yesterday_date"] = date("Y-m-d", strtotime("-1 day"));
//        $filters["from_date"] = $stats["yesterday_date"];
//        $filters["to_date"] = $stats["yesterday_date"];
//
//        $stats["yesterday_business_hours"] = $this->vendor_business_hours_model->get_working_days_count($vendor_id, $week_name);
//        $stats["yesterday_new_orders"] = $this->orders_model->get_vendor_pending_orders($vendor_id, $filters);
//        $stats["yesterday_preparing_orders"] = $waiting_for_dp + $waiting_for_pickup;
//        $stats["yesterday_out_for_delivery_orders"] = $this->orders_model->get_vendor_out_for_delivery($vendor_id, $filters);
//        $stats["yesterday_completed_orders"] = $this->orders_model->get_vendor_total_completed_orders($vendor_id, $filters);
//        $stats["yesterday_rejected_orders"] = $this->orders_model->get_vendor_total_rejected($vendor_id, $filters);
//        $stats["yesterday_earnings"] = $this->orders_model->get_vendor_earnings_from_orders($vendor_id, $filters);
//        $stats["yesterday_sale_amount"] = $this->orders_model->get_vendor_sale_amount_from_orders($vendor_id, $filters);
//
//        $filters["from_date"] = date("Y-m-d", strtotime("this week monday"));
//        $filters["to_date"] = date("Y-m-d", strtotime("this week sunday"));
//
//        $stats["weekly_new_orders"] = $this->orders_model->get_vendor_pending_orders($vendor_id, $filters);
//        $stats["weekly_preparing_orders"] = $waiting_for_dp + $waiting_for_pickup;
//        $stats["weekly_out_for_delivery_orders"] = $this->orders_model->get_vendor_out_for_delivery($vendor_id, $filters);
//        $stats["weekly_completed_orders"] = $this->orders_model->get_vendor_total_completed_orders($vendor_id, $filters);
//        $stats["weekly_rejected_orders"] = $this->orders_model->get_vendor_total_rejected($vendor_id, $filters);
//        $stats["weekly_earnings"] = $this->orders_model->get_vendor_earnings_from_orders($vendor_id, $filters);
//        $stats["weekly_sale_amount"] = $this->orders_model->get_vendor_sale_amount_from_orders($vendor_id, $filters);
//
//        $filters["from_date"] = date("Y-m-01", strtotime("first day of this month"));
//        $filters["to_date"] = date("Y-m-d", strtotime("last day of this month"));
//        $stats["monthly_new_orders"] = $this->orders_model->get_vendor_pending_orders($vendor_id, $filters);
//        $stats["monthly_preparing_orders"] = $waiting_for_dp + $waiting_for_pickup;
//        $stats["monthly_out_for_delivery_orders"] = $this->orders_model->get_vendor_out_for_delivery($vendor_id, $filters);
//        $stats["monthly_completed_orders"] = $this->orders_model->get_vendor_total_completed_orders($vendor_id, $filters);
//        $stats["monthly_rejected_orders"] = $this->orders_model->get_vendor_total_rejected($vendor_id, $filters);
//        $stats["monthly_earnings"] = $this->orders_model->get_vendor_earnings_from_orders($vendor_id, $filters);
//        $stats["monthly_sale_amount"] = $this->orders_model->get_vendor_sale_amount_from_orders($vendor_id, $filters);
//
//        $top_day_the_month = $this->vendor_top_selleing_products_model->get_top_sale_day($filters, $vendor_id);
//        if ($top_day_the_month) {
//            $stats["top_day_of_the_month"] = $top_day_the_month;
//        }
//
//
//
//        $stats["today_stats"] = [
//            "date_text" => date("d-M-y"),
//            "sale_amount" => $stats["today_sale_amount"],
//            "earning_amount" => $stats["today_earnings"],
//            "rejected_orders" => $stats["today_rejected_orders"],
//            "completed_orders" => $stats["today_completed_orders"],
//            "from_date" => date("Y-m-d"),
//            "to_date" => date("Y-m-d")
//        ];
//        $stats["weekly_stats"] = [
//            "date_text" => date("d-M-y", strtotime("this week monday")) . " - " . date("d-M-y", strtotime("this week sunday")),
//            "sale_amount" => $stats["weekly_sale_amount"],
//            "earning_amount" => $stats["weekly_earnings"],
//            "rejected_orders" => $stats["weekly_rejected_orders"],
//            "completed_orders" => $stats["weekly_completed_orders"],
//            "from_date" => date("Y-m-d", strtotime("this week monday")),
//            "to_date" => date("Y-m-d", strtotime("this week sunday"))
//        ];
//        $stats["monthly_stats"] = [
//            "date_text" => date("d-M-y", strtotime("first day of this month")) . " - " . date("d-M-y", strtotime("last day of this month")),
//            "sale_amount" => $stats["monthly_sale_amount"],
//            "earning_amount" => $stats["monthly_earnings"],
//            "rejected_orders" => $stats["monthly_rejected_orders"],
//            "completed_orders" => $stats["monthly_completed_orders"],
//            "from_date" => date("Y-m-d", strtotime("first day of this month")),
//            "to_date" => date("Y-m-d", strtotime("last day of this month"))
//        ];
//
//        $filters["type"] = implode(",", ["'Waiting For Pickup'", "'Waiting For Delivery Person'", "'Out For Delivery'"]);
//
//        $filters["used_implode"] = true;
//
//        $filters["start"] = 0;
//        $filters["limit"] = 50;
//        $data = $this->orders_model->get_vendor_orders($filters, $vendor_id);
//
//        $mobile_results = [];
//
//        foreach ($data as $item) {
//            $mobile_results[] = (object) [
//                        "ref_id" => $item->ref_id,
//                        "cart_items" => count($item->cart_items),
//                        "cart_items_list" => $item->cart_items,
//                        "grand_total" => $item->grand_total,
//                        "service_date" => $item->service_date,
//                        "cart_items_txt" => $item->cart_items_txt,
//                        "service_status" => $item->service_status
//            ];
//        }
//        $data = $mobile_results;
//        $stats["orders_list"] = $data;
        $stats = array();
        $arr = array(
            'err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $stats,
        );
        echo json_encode($arr);
    }

}
