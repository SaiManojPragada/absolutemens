<?php

header('Content-type: application/json');

class Districts_cities extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("u_model");
    }

    function get_cities() {
        $cities = $this->u_model->get_nostatcheck_data("cities");
        if ($cities) {
            $arr = [
                "err_code" => "valid",
                "message" => "Cities Data",
                "data" => $cities
            ];
        } else {
            $arr = [
                "err_code" => "invalid",
                "message" => "Unable to get Cities Data"
            ];
        }
        echo json_encode($arr);
    }

}
