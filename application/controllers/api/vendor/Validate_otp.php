<?php

header('Content-type: application/json');

class Validate_otp extends MY_Controller {

    function index() {

        $vendor_id = $this->vendor_model->get_logged_vendor_id();

        if (!$this->input->get_post("ref_id")) {
            $arr = array('err_code' => "invalid", "error_type" => "ref_id_required", "message" => "Please select order");
            echo json_encode($arr);
            die;
        }
        if (!$this->input->get_post("otp")) {
            $arr = array('err_code' => "invalid", "error_type" => "otp_required", "message" => "Please Enter OTP");
            echo json_encode($arr);
            die;
        }

        $ref_id = $this->input->get_post("ref_id");
        $otp = $this->input->get_post("otp");

        $this->db->select("id, vendor_otp");
        $this->db->where("ref_id", $ref_id);
        $vendor_otp = $this->db->get("service_orders")->row()->vendor_otp;


        if ($otp == $vendor_otp) {

            $this->db->where("ref_id", $ref_id);
            $this->db->set("vendor_otp_verified", 1);
            $this->db->update("service_orders");

            $arr = array(
                'err_code' => "valid",
                "message" => "OTP Verified"
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => "Invalid OTP"
            );
        }
        echo json_encode($arr);
    }

}
