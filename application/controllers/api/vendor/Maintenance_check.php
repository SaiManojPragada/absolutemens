<?php

header('Content-type: application/json');

class Maintenance_check extends MY_Controller {

    function index() {
        $cache_key = "vendor_app_maintainance_check";
        $this->cache_model->show_cache_if_exists($cache_key);

        $maintenance_mode_enabled = $this->maintenance_model->vendor_app_maintenance_status();
        $arr = array(
            'err_code' => "valid",
            "message" => "Maintenance",
            "data" => [
                "maintenance_mode_enabled" => $maintenance_mode_enabled,
                "title" => "testing",
                "message" => "testing",
                "image" => "https://mumstores.co.in/uploads/c6ebe2d78478c7441b88ca29def136af.png"
            ]
        );
        $this->cache_model->save_to_cache($cache_key, $arr);
        echo json_encode($arr);
    }

}
