<?php

header('Content-type: application/json');

class Order_view extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        if (!$this->input->get_post("ref_id")) {
            $arr = array('err_code' => "invalid", "error_type" => "ref_id_required", "message" => "Please select order");
            echo json_encode($arr);
            die;
        }

        $ref_id = $this->input->get_post("ref_id");
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $data = $this->orders_model->get_order_details($ref_id);

        if ($data) {

            /* if ($data->payment_mode == "Pay On Delivery") {
              $data->grand_total = $data->grand_total - $data->through_wallet_amount;
              } */

            unset($data->created_at);
            unset($data->updated_at);
            unset($data->id);
            unset($data->customers_id);
            unset($data->restaurants_id);
            unset($data->creation_source);
            unset($data->version_code);
            unset($data->status);
            unset($data->is_skipped_feedback);
            unset($data->is_provided_feedback);
            unset($data->settlement_remarks);
            unset($data->settlement_attachment);
            unset($data->settlement_status);
            unset($data->settlement_ref_id);
            unset($data->settled_amount);
            unset($data->settled_date);
            $data->is_out_for_delivery_option_enabled = $this->data["site_property"]->admin_delivery_persons;
            //unset($data->restaurant_details); 
            //unset($data->coupon_promotion_by);

            $arr = array('err_code' => "valid", "title" => "Order Details", "data" => $data);
        } else {
            $arr = array('err_code' => "invalid", "data" => [], "message" => "Sorry this order not exists");
        }

        $arr = json_encode($arr);
        $response_arry = json_decode(str_replace('null', '""', $arr));
        if (json_last_error_msg() == "Syntax error") {
            $response_arry = json_decode(str_replace('"null"', '""', $arr));
        }
        //$this->response($response_arry);

        echo json_encode($response_arry);
    }

}
