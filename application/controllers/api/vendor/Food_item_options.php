<?php

header('Content-type: application/json');

class Food_item_options extends MY_Controller {

    private $model_name = "vendor_food_item_options_model";
    private $title = "Food Items";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        if (!$this->input->get_post("restaurant_food_items_id")) {
            $arr = array('err_code' => "invalid",
                "title" => "Error",
                "message" => "Please select food category"
            );
        }
        $restaurant_food_items_id = $this->input->get_post("restaurant_food_items_id");
        $addon_items = $this->{$this->model_name}->get($restaurant_food_items_id);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => $this->title . " list",
            "data" => $addon_items
        );
        echo json_encode($arr);
    }

    function add() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurants_id = $vendor_id;
        //$this->form_validation->set_rules("restaurants_id", "Restaurant id", "required");
        $this->form_validation->set_rules("restaurant_food_items_id", "Food item", "required");
        $this->form_validation->set_rules("name", "Qty Name", "required");
        $this->form_validation->set_rules("actual_price", "Actual Price", "required");
        //$this->form_validation->set_rules("offer_percentage", "Offer Percentage");
        //$this->form_validation->set_rules("admin_commission_percentage", "Admin Commission Percentage", "is_natural");

        if ($this->form_validation->run() == FALSE) {
            $arr = [
                'err_code' => "invalid_form",
                'title' => "",
                "message" => validation_errors()
            ];
            echo json_encode($arr);
            die;
        }

        extract($_POST);
        $data = [
            "restaurant_food_items_id" => $restaurant_food_items_id,
            "name" => $name,
            "actual_price" => $actual_price,
            "offer_percentage" => $offer_percentage,
            "weight" => isset($weight) ? $weight : 0,
            "admin_commission_percentage" => isset($admin_commission_percentage) ? $admin_commission_percentage : 0,
        ];



        if ($this->input->get_post("id")) {
            return $this->update($data);
        }

        if ($this->{$this->model_name}->add($data)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " added successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not added, please try again"
            );
            echo json_encode($arr);
        }
    }

    function update($data) {
        $id = $this->input->get_post("id");

        if ($this->{$this->model_name}->update($data, $id)) {
            $arr = array('err_code' => "valid",
                "title" => "Success",
                "message" => singular($this->title) . " updated successfully"
            );
            echo json_encode($arr);
        } else {
            $arr = array('err_code' => "valid",
                "title" => "Error",
                "message" => singular($this->title) . " not updated, please try again"
            );
            echo json_encode($arr);
        }
    }

    function delete() {
        $id = (int) $this->input->get_post("id");
        $response = $this->{$this->model_name}->delete($id);
        if ($response) {
            $arr = [
                'err_code' => "valid",
                'title' => "",
                "message" => singular($this->title) . " deleted successfully"
            ];
        } else {
            $arr = [
                'err_code' => "invalid",
                'title' => "",
                "message" => singular($this->title) . " not deleted, please try again"
            ];
        }
        echo json_encode($arr);
    }

    function mark_food_option_as_sold_out_or_available() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $restaurant_food_items_id = $this->input->get_post("id");
        $available = $this->input->get_post("available");
        $this->{$this->model_name}->update_food_option_sold_out_or_available($vendor_id, $restaurant_food_items_id, $available);
        $arr = array('err_code' => "valid",
            "title" => "",
            "message" => "Status updated"
        );
        echo json_encode($arr);
    }

}
