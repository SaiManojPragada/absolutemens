<?php

class Stock_management extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('stock_management_model');
    }

    function add_or_update_stock() {


        $this->form_validation->set_rules("product_inventory_id", "Product inventory id", "required", array(
            'required' => 'Product inventory id cannot be empty',
        ));
        $this->form_validation->set_rules("quantity", "Quantity", "required", array(
            'required' => 'Quantity cannot be empty',
        ));
        $this->form_validation->set_rules("type", "Type", "required", array(
            'required' => 'Type cannot be empty',
        ));
        $this->form_validation->set_rules("action_for", "Action for", "required", array(
            'required' => 'Action for cannot be empty',
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "product_id" => form_error("product_id"),
                "product_inventory_id" => form_error("product_inventory_id"),
                "quantity" => form_error("quantity"),
                "type" => form_error("type"),
                "action_for" => form_error("action_for")
            ];
            $single_line_message = "";

            if (form_error("product_id")) {
                $single_line_message = form_error("product_id");
            }

            if (form_error("product_inventory_id")) {
                $single_line_message = form_error("product_inventory_id");
            }

            if (form_error("quantity")) {
                $single_line_message = form_error("quantity");
            }

            if (form_error("type")) {
                $single_line_message = form_error("type");
            }
            if (form_error("action_for")) {
                $single_line_message = form_error("action_for");
            }


            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {

            $product_inventory_id = $this->input->post('product_inventory_id');
            $quantity = $this->input->post('quantity');
            $product_id = $this->db->get_where('product_inventory', ['id' => $product_inventory_id])->row()->product_id;
            $type = $this->input->post('type');
            $action_for = $this->input->post('action_for');

            $data = array(
                "product_id" => $product_id,
                "product_inventory_id" => $product_inventory_id,
                "quantity" => $quantity,
                "type" => $type,
                "action_for" => $action_for
            );
            $response = $this->stock_management_model->add_stock($data);
            if ($response) {
                $arr = [
                    "status" => 'valid',
                    "message" => "Stock updated successfully",
                ];
            } else {
                $arr = [
                    "status" => 'invalid',
                    "message" => "Stock not updated.Please try again later",
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

}
