<?php

header('Content-type: application/json');

class App_banner_image extends MY_Controller {

    function __construct() {
        parent::__construct();
        /* if ($this->vendor_model->check_for_user_logged() == false) {
          show_forbidden();
          } */
    }

    function index() {
        $this->db->select("image");
        $row = $this->db->get("vendor_app_image")->row();
        if ($row) {
            $row->image = S3FILES_PATH . "uploads/store_app_promotional_banner/" . $row->image;
            $arr = array('err_code' => "valid",
                "title" => "Banner image",
                "message" => "Banner image",
                "data" => $row->image
            );
        } else {
            $arr = array('err_code' => "invalid",
                "title" => "",
                "message" => "Banner image not found"
            );
        }
        echo json_encode($arr);
    }

}
