<?php

header('Content-type: application/json');

class Update_profile extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
    }

    function index() {
        if (empty($this->session->userdata("vendor_id"))) {
            $arr = array(
                'err_code' => "invalid",
                "message" => "Please Login to Continue"
            );
            echo json_encode($arr);
            die;
        }
        $res = $this->u_model->update_data("vendors", $_POST, array("id" => $this->session->userdata("vendor_id")));
        if ($res) {
            $arr = array(
                'err_code' => "valid",
                "message" => "Profile Updated"
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                "message" => "Unable to Update Profile"
            );
        }
        echo json_encode($arr);
    }

}
