<?php

class Time_slots extends MY_Controller{
    
    function index(){
        $slots = time_slots("12:00 AM", "11:59 PM", 30);
        
        
        
        $slots_arr = [];
        for($i=0; $i<count($slots); $i++){
            
            $key = date("H:i:00", strtotime(date("Y-m-d").$slots[$i]));
            
            $slots_arr[] = array("key"=>$key, "value"=>$slots[$i]);
        }
        $arr = [
            "err_code"=>"valid",
            "data"=>$slots_arr
        ];
        echo json_encode($arr);
    }
}