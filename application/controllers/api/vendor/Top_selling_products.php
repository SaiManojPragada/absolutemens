<?php

header('Content-type: application/json');

class Top_selling_products extends MY_Controller {

    private $model_name = "vendor_top_selleing_products_model";
    private $title = "Top Selling Products";

    function __construct() {
        parent::__construct();
        if ($this->vendor_model->check_for_user_logged() == false) {
            show_forbidden();
        }
    }

    function index() {
        $vendor_id = $this->vendor_model->get_logged_vendor_id();
        $filters = [];
        $filters["from_date"] = $this->input->get_post("from_date");
        $filters["to_date"] = $this->input->get_post("to_date");
        $data = $this->{$this->model_name}->get_top_selling_products_list($vendor_id, $filters);

        if ($data) {


            $arr = array(
                'err_code' => "valid",
                "title" => "",
                "message" => $this->title . " list",
                "data" => $data,
            );
        } else {
            $arr = array(
                'err_code' => "invalid",
                'title' => "No data found",
                "message" => "No data found",
            );
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
    }

}
