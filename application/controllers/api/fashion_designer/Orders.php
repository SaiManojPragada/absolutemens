<?php

class Orders extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
        $this->load->model('fashion_manager_orders_model');
        $this->load->model('stock_management_model');
    }

    function to_be_approved() {
        $response = $this->fashion_manager_orders_model->get_to_be_approved_orders();
        $arr = [
            "status" => 'valid',
            "message" => "To be Approved Orders",
            "data" => $response
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function forwarded_to_admin() {

        $response = $this->fashion_manager_orders_model->forwarded_to_admin_orders();
        $arr = [
            "status" => 'valid',
            "message" => "Forwarded to Admin Orders",
            "data" => $response
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function details() {
        $order_id = $this->input->post('order_id');
        $response = $this->fashion_manager_orders_model->get_order_details($order_id);
        $arr = [
            "status" => 'valid',
            "message" => "Orders details",
            "data" => $response
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function check_subscription_validation() {
        $this->form_validation->set_rules("subscription_transaction_id", "Subscription transaction id", "required", array(
            'required' => 'Subscription transaction id cannot be empty',
        ));
        $this->form_validation->set_rules("order_id", "Order id", "required", array(
            'required' => 'Order id cannot be empty',
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "subscription_transaction_id" => form_error("subscription_transaction_id"),
                "order_id" => form_error("order_id")
            ];
            $single_line_message = "";

            if (form_error("subscription_transaction_id")) {
                $single_line_message = form_error("subscription_transaction_id");
            }

            if (form_error("order_id")) {
                $single_line_message = form_error("order_id");
            }

            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $subscription_transaction_id = $this->input->post('subscription_transaction_id');
            $order_id = $this->input->post('order_id');
            $response = $this->fashion_manager_orders_model->check_subscription_applicable($subscription_transaction_id, $order_id);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "message" => "Subscription applicable"
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "message" => "No of items for a subscription limit exceeded"
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

    function update_fashion_manager_selected_status() {

        $this->form_validation->set_rules("order_product_id", "Order product id", "required", array(
            'required' => 'Order product id cannot be empty',
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "order_product_id" => form_error("order_product_id")
            ];
            $single_line_message = "";

            if (form_error("order_product_id")) {
                $single_line_message = form_error("order_product_id");
            }
            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $order_product_id = $this->input->post('order_product_id');
            $response = $this->fashion_manager_orders_model->update_fashion_manager_selected_status($order_product_id);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "message" => "Product selected successfully"
                ];
            } else {
                $arr = [
                    "status" => "valid",
                    "message" => "Product not selected.Please try again later"
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

    function orders_log() {

        $this->form_validation->set_rules("unique_order_id", "Unique Order id", "required", array(
            'required' => 'Unique Order id cannot be empty',
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "unique_order_id" => form_error("unique_order_id")
            ];
            $single_line_message = "";

            if (form_error("unique_order_id")) {
                $single_line_message = form_error("unique_order_id");
            }
            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $unique_order_id = $this->input->post('unique_order_id');
            $response = $this->fashion_manager_orders_model->get_order_logs($unique_order_id);
            $arr = [
                "status" => "valid",
                "message" => "Orders log",
                "data" => $response
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

    function accept_and_forward_to_admin() {

        $this->form_validation->set_rules("order_id", "Order id", "required", array(
            'required' => 'Order id cannot be empty',
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "order_id" => form_error("order_id")
            ];
            $single_line_message = "";

            if (form_error("order_id")) {
                $single_line_message = form_error("order_id");
            }
            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $order_id = $this->input->post('order_id');
            $response = $this->fashion_manager_orders_model->accept_and_forward_to_admin($order_id);
            if ($response == "success") {
                $arr = [
                    "status" => "valid",
                    "message" => "Status updated successfully"
                ];
            } else if ($response == "error") {
                $arr = [
                    "status" => "invalid",
                    "message" => "Status not updated.Please try again later"
                ];
            } else if ($response == "not_selected") {
                $arr = [
                    "status" => "invalid",
                    "message" => "Please select products as per purchased subscription"
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

    function reject_order() {
        $this->form_validation->set_rules("order_id", "Order id", "required", array(
            'required' => 'Order id cannot be empty',
        ));
        $this->form_validation->set_rules("reject_reason", "Reject Reason", "required", array(
            'required' => 'Reject Reason cannot be empty',
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "order_id" => form_error("order_id"),
                "reject_reason" => form_error("reject_reason")
            ];
            $single_line_message = "";

            if (form_error("order_id")) {
                $single_line_message = form_error("order_id");
            }
            if (form_error("reject_reason")) {
                $single_line_message = form_error("reject_reason");
            }
            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $order_id = $this->input->post('order_id');
            $reject_reason = $this->input->post('reject_reason');
            $response = $this->fashion_manager_orders_model->reject_order($order_id, $reject_reason);
            if ($response == "success") {
                $order_data = $this->u_model->get_nostatcheck_row("orders", array("id" => $order_id));

                $fashion_analyst_data = $this->u_model->get_nostatcheck_row("designers_and_analysts", array("id" => $order_data->fashion_analyst_id));
                $subs_data = $this->u_model->get_nostatcheck_row("subscriptions_transactions", array("id" => $order_data->subscription_transaction_id));
                $this->data['message'] = "<center>Hi, your suggestions has been Rejected. Order No #" . $subs_data->unq_id . ".<br><br><br>"
                        . "<hr>"
                        . "Click here to Login to your Panel " . base_url() . 'fashion_designer_manager/login'
                        . "</center>";
                $message = $this->load->view('email_templates/orders_message_template', $this->data, true);
                $this->send_email_model->send_mail($fashion_analyst_data->email, 'Order Rejected', $message);

                $arr = [
                    "status" => "valid",
                    "message" => "Status updated successfully"
                ];
            } else if ($response == "error") {
                $arr = [
                    "status" => "invalid",
                    "message" => "Status not updated.Please try again later"
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

    function get_fashion_analysts() {
        $response = $this->fashion_manager_orders_model->get_fashion_analysts();
        $arr = [
            "status" => "valid",
            "message" => "Fashion Analysts",
            "data" => $response
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function reassign_fashion_analyst() {
        $this->form_validation->set_rules("subscription_transaction_id", "Subscription Transaction id", "required", array(
            'required' => 'Subscription Transaction id cannot be empty',
        ));
        $this->form_validation->set_rules("fashion_analyst_id", "Fashion analyst id", "required", array(
            'required' => 'Fashion analyst id cannot be empty',
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "subscription_transaction_id" => form_error("subscription_transaction_id"),
                "fashion_analyst_id" => form_error("fashion_analyst_id")
            ];
            $single_line_message = "";

            if (form_error("subscription_transaction_id")) {
                $single_line_message = form_error("subscription_transaction_id");
            }
            if (form_error("fashion_analyst_id")) {
                $single_line_message = form_error("fashion_analyst_id");
            }
            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $subscription_transaction_id = $this->input->post('subscription_transaction_id');
            $fashion_analyst_id = $this->input->post('fashion_analyst_id');
            $response = $this->fashion_manager_orders_model->reassign_fashion_analyst($subscription_transaction_id, $fashion_analyst_id);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "message" => "Re-assigned successfully",
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "message" => "Re-assign failed.Please try again later"
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

}
