<?php

class Shop_for_order extends MY_Controller {

    private $model_name = "Shop_for_order_model";
    private $title = "Shop for orders";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model_name);
        $this->load->model('fashion_designer_cart_model');
        $this->load->helper('my_pagination_helper');
    }

    function index() {
        $unique_order_id = $this->input->post('unique_order_id');
        $filters = array();
        $filters['category_id'] = $this->input->post('category_id');
        $filters['vendor_id'] = $this->input->post('vendor_id');
        $filters['product_code'] = $this->input->post('product_code');
        $filters['unique_order_id'] = $this->input->post('unique_order_id');

        $check_order_already_forwarded_to_fashion_manager = $this->{$this->model_name}->check_order_already_forwarded_to_fashion_manager($unique_order_id);
        if ($check_order_already_forwarded_to_fashion_manager['status'] == 'not_forwarded') {
            $type = $this->input->post('type');
//            $limit = $this->input->get_post("limit") ? $this->input->get_post("limit") : 12;
//            $_GET['start'] = $this->input->get_post("page");
//            $start = $this->input->get_post("page") > 1 ? ($this->input->get_post("page") - 1) * $limit : 0;
//            $total_results = $this->{$this->model_name}->get_products_count($filters);
//            $filters["limit"] = $limit;
//            $filters["start"] = $start;
//
//            $pagination = my_pagination("shop_for_order", $limit, $total_results, true);
//            $pagination['pagination'] = str_replace("&amp;", "&", $pagination['pagination']);
//            $cur_page = ($pagination['pagination_helper']->cur_page != 0) ? ($pagination['pagination_helper']->cur_page - 1) : $pagination['pagination_helper']->cur_page;
//            $pagination['initial_id'] = (($cur_page) * $pagination['pagination_helper']->per_page) + 1;
            $data = $this->{$this->model_name}->get($filters);
            $arr = [
                'status' => "valid",
//                "pagination" => $pagination,
                "data" => [
                    "related_products" => $data,
                    "order_details" => $type == 'initial' ? $this->{$this->model_name}->get_order_details($unique_order_id) : null,
                    "categories_list" => $this->{$this->model_name}->get_categories(),
                    "vendors_list" => $this->{$this->model_name}->get_vendors()
                ]
            ];
        } else {
            $arr = [
                'status' => "invalid",
                "message" => "Already forwarded to fashion manager",
                "pagination" => "",
                "data" => [
                    "related_products" => [],
                    "order_details" => (object) [],
                    "categories_list" => [],
                    "vendors_list" => []
                ]
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function product_details() {
        $product_id = $this->input->post('product_id');
        $inventory_id = $this->input->post('inventory_id');
        $response = $this->{$this->model_name}->get_product_details($product_id, $inventory_id);
        $arr = [
            'status' => "valid",
            "data" => $response
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function forward_to_fashion_manager() {
        $response = $this->{$this->model_name}->forward_order_to_fashion_manager();
        if ($response['status'] == "no_items") {
            $arr = [
                'status' => "invalid",
                "message" => "No items available in cart"
            ];
        } else if ($response['status'] == "error") {
            $arr = [
                'status' => "invalid",
                "message" => "Order not forwarded to fashion manager.Please try again later"
            ];
        } else if ($response['status'] == "success") {
            $arr = [
                'status' => "valid",
                "message" => "Successfully forwarded to Fashion manager - " . $response['name']
            ];
        }
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function check_order_already_forwarded_to_fashion_manager() {
        $this->form_validation->set_rules("unique_id", "Unique id", "required", array(
            'required' => 'Unique id  cannot be empty',
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "unique_id" => form_error("unique_id")
            ];
            $single_line_message = "";

            if (form_error("unique_id")) {
                $single_line_message = form_error("unique_id");
            }


            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $unique_id = $this->input->post('unique_id');
            $response = $this->{$this->model_name}->check_order_already_forwarded_to_fashion_manager($unique_id);
            if ($response['status'] == 'not_forwarded') {
                $arr = [
                    "status" => "valid",
                    "message" => "This order not yet forwarded to fashion manager",
                    "data" => $response['data']
                ];
            } else if ($response['status'] == 'forwarded') {
                $arr = [
                    "status" => "invalid",
                    "message" => "This order has already forwarded to fashion manager",
                    "data" => $this->{$this->model_name}->get_order_logs($unique_id)
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

}
