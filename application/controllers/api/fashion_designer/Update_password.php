<?php

header('Content-type: application/json');

class Update_password extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
    }

    function index() {

        $vendor_id = $this->session->userdata("fashion_id");
        $current_password = $this->input->get_post("current_password");
        $new_password = $this->input->get_post("new_password");
        $re_password = $this->input->get_post("re_password");

        if (empty($current_password)) {
            $arr = array('err_code' => "invalid", "error_type" => "passwords_does_not_match", "message" => "Current Password is Required");
            echo json_encode($arr);
            die;
        }
        if (empty($new_password)) {
            $arr = array('err_code' => "invalid", "error_type" => "passwords_does_not_match", "message" => "New Password is Required");
            echo json_encode($arr);
            die;
        }
        $id = $this->session->userdata("fashion_id");
        if ($new_password !== $re_password) {
            $arr = array('err_code' => "invalid", "error_type" => "passwords_does_not_match", "message" => "Passwords Does Match");
            echo json_encode($arr);
            die;
        }
        $userdata = $this->u_model->get_nostatcheck_row("designers_and_analysts", array('id' => $vendor_id));
        if (md5($current_password . $userdata->salt) === $userdata->password) {
            $salt = generateRandomString();
            $enc_pwd = md5($new_password . $salt);
            $this->db->set("password", $enc_pwd);
            $this->db->set("salt", $salt);
            $this->db->where("id", $vendor_id);
            $res = $this->db->update("designers_and_analysts");
            if ($res) {
                $arr = array('err_code' => "valid", "message" => "Password Updated successfully");
            } else {
                $arr = array('err_code' => "invalid", "error_type" => "account_not_existed", "message" => "Sorry, this account does not existed");
            }
        } else {
            $arr = array('err_code' => "invalid", "error_type" => "Incorrect_password", "message" => "Incorrect Current Password");
        }

        echo json_encode($arr);
    }

}
