<?php

class My_orders extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Shop_for_order_model');
        $this->load->model('fashion_designer_cart_model');
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
    }

    function index() {

        $this->db->select('*');
        $this->db->order_by('is_exchanged', 'desc');
        $this->db->order_by('id', 'desc');
        $this->db->from('subscriptions_transactions st');
        // $this->db->where("'NOT EXISTS(SELECT id FROM orders o WHERE o.subscription_transaction_id = st.id)'");
        $this->db->group_start();
        $this->db->where("st.order_status", 'Assigned to Fashion Analyst');
        $this->db->or_where('st.order_status', 'Selected Items Under Review');
        $this->db->group_end();

        $this->db->where("st.fashion_manager_id='" . $this->session->userdata('fashion_id') . "' OR st.fashion_analyst_id='" . $this->session->userdata('fashion_id') . "'");
        $data = $this->db->get()->result();
        if ($data[0]->fashion_analyst_id == $this->session->userdata('fashion_id')) {
            $disable_details = true;
        } else {
            $disable_details = false;
        }
        foreach ($data as $item) {
            $item->fashion_analyst_details = $this->u_model->get_data_conditon_row("designers_and_analysts", array("id" => $item->fashion_analyst_id));
            $item->ordered_on = date('d M Y, h:i A', $item->updated_at);
            $item->package_details = $this->u_model->get_nostatcheck_row("packages", array("id" => $item->packages_id));
            $item->package_details->title = strip_tags($item->package_details->title);
            $item->package_details->price = (int) $item->package_details->price;
            $item->package_details->price = number_format($item->package_details->price);
            $this->db->where('id', $item->customers_id);
            $item->customer_details = $this->db->get('customers')->row();
            $this->db->where('subscriptions_transactions_id', $item->unq_id);
            $item->images = $this->db->get("subscription_images")->result();
            $item->selected_sizes = $this->get_selected_sizes($item->selected_sizes);
        }
        if ($data) {
            $arr = array(
                "err_code" => "valid",
                "message" => "Your Orders",
                "data" => $data,
                "disable_details" => $disable_details
            );
        } else {
            $arr = array(
                "err_code" => "invalid",
                "message" => "No Orders Found"
            );
        }
        echo json_encode($arr);
    }

    function get_selected_sizes($selected_sizes) {
        $selected_sizes_arr = json_decode($selected_sizes);
        $arr = [];
        foreach ($selected_sizes_arr as $key => $value) {
            $obj = new stdClass();
            $obj->category = $this->db->get_where('categories', ['id' => $key])->row()->name;
            $obj->size = $this->db->get_where('sizes', ['id' => $value])->row()->title;
            $obj->category_id = $key;
            $arr[] = $obj;
        }
        return $arr;
    }

    function get_single_order() {
        $order_id = $this->input->get_post('id');
        $data = $this->u_model->get_nostatcheck_row("subscriptions_transactions", array("unq_id" => $order_id));
        $data->ordered_on = date('d M Y, h:i A', $data->updated_at);
        $data->package_details = $this->u_model->get_nostatcheck_row("packages", array("id" => $data->packages_id));
        $data->package_details->title = strip_tags($data->package_details->title);
        $data->package_details->price = (int) $data->package_details->price;
        $data->package_details->price = number_format($data->package_details->price);
        $this->db->where('id', $data->customers_id);
        $data->customer_details = $this->db->get('customers')->row();
        $this->db->where('subscriptions_transactions_id', $data->unq_id);
        $this->db->where("type", "Half");
        $data->half_images = $this->db->get("subscription_images")->result();
        $this->db->where('subscriptions_transactions_id', $data->unq_id);
        $this->db->where("type", "Full");
        $data->full_images = $this->db->get("subscription_images")->result();
        $data->order_details = $this->Shop_for_order_model->get_order_details($order_id);
        $data->order_id = $order_id;
        if ($data) {
            $arr = array(
                "err_code" => "valid",
                "message" => "Your Orders",
                "data" => $data
            );
        } else {
            $arr = array(
                "err_code" => "invalid",
                "message" => "No Orders Found"
            );
        }
        echo json_encode($arr);
    }

}
