<?php

class Cart extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
        $this->load->model('fashion_designer_cart_model');
        $this->load->model('stock_management_model');
    }

    function add_or_update_cart() {

        $this->form_validation->set_rules("product_id", "Product id", "required", array(
            'required' => 'Product id cannot be empty',
        ));
        $this->form_validation->set_rules("product_inventory_id", "Product inventory id", "required", array(
            'required' => 'Product inventory id cannot be empty',
        ));
        $this->form_validation->set_rules("quantity", "Quantity", "required", array(
            'required' => 'Quantity cannot be empty',
        ));
        $this->form_validation->set_rules("unique_id", "Unique_id", "required", array(
            'required' => 'Unique id cannot be empty',
        ));
        $this->form_validation->set_rules("comment", "Comment", "required", array(
            'required' => 'Comment cannot be empty',
        ));

        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "product_id" => form_error("product_id"),
                "product_inventory_id" => form_error("product_inventory_id"),
                "quantity" => form_error("quantity"),
                "unique_id" => form_error("unique_id"),
                "comment" => form_error("comment")
            ];
            $single_line_message = "";

            if (form_error("product_id")) {
                $single_line_message = form_error("product_id");
            }

            if (form_error("quantity")) {
                $single_line_message = form_error("quantity");
            }

            if (form_error("unique_id")) {
                $single_line_message = form_error("unique_id");
            }

            if (form_error("comment")) {
                $single_line_message = form_error("comment");
            }

            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $product_inventory_id = $this->input->post('product_inventory_id');
            $quantity = $this->input->post('quantity');
            $product_id = $this->input->post('product_id');
            $unique_id = $this->input->post('unique_id');
            $comment = $this->input->post('comment');
            $check_cart_product_stock_eceeded = $this->fashion_designer_cart_model->check_cart_products_stock_exceeded_for_product($product_inventory_id, $quantity);
            if ($check_cart_product_stock_eceeded == "out_of_stock") {
                $arr = [
                    'status' => 'invalid',
                    "message" => $quantity . "  stock not available for this product"
                ];
                echo json_encode($arr, JSON_PRETTY_PRINT);
                die;
            }
            $data = array(
                "product_id" => $product_id,
                "product_inventory_id" => $product_inventory_id,
                "quantity" => $this->input->post('quantity'),
                "comment" => $comment
            );
            $response = $this->fashion_designer_cart_model->add($data, $unique_id);
            if ($response == "added") {
                $arr = [
                    "status" => 'valid',
                    "message" => "Product has been successfully suggested",
                ];
            } else if ($response == "updated") {
                $arr = [
                    "status" => 'valid',
                    "message" => "Cart item updated successfully",
                ];
            } else if ($response == "error") {
                $arr = [
                    "status" => 'invalid',
                    "message" => "Product not added to cart.Please try again later",
                ];
            } else {
                $arr = [
                    "status" => 'invalid',
                    "message" => "There might be problem with server.Please try again later",
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

    function cart_list() {
        $response = $this->fashion_designer_cart_model->get_cart();
        $arr = [
            "status" => 'valid',
            "message" => "Cart list",
            "data" => $response
        ];
        echo json_encode($arr, JSON_PRETTY_PRINT);
        die;
    }

    function delete_cart_item() {
        $this->form_validation->set_rules("id", "id", "required", array(
            'required' => 'Id cannot be empty',
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "id" => form_error("id"),
            ];
            $single_line_message = "";
            if (form_error("id")) {
                $single_line_message = form_error("id");
            }
            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $id = $this->input->post('id');
            $response = $this->fashion_designer_cart_model->delete_cart_item($id);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "message" => "Cart item deleted successfully"
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "message" => "Cart item not deleted.Please try later"
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

    function get_cart_comment() {
        $this->form_validation->set_rules("product_id", "Product id", "required", array(
            'required' => 'Product id cannot be empty',
        ));
        $this->form_validation->set_rules("product_inventory_id", "Product inventory id", "required", array(
            'required' => 'Product id cannot be empty',
        ));
        $this->form_validation->set_rules("unique_id", "Unique order id", "required", array(
            'required' => 'Unique order id cannot be empty',
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "product_inventory_id" => form_error("product_inventory_id"),
                "product_id" => form_error("product_id"),
                "unique_id" => form_error("unique_id")
            ];
            $single_line_message = "";
            if (form_error("product_inventory_id")) {
                $single_line_message = form_error("product_inventory_id");
            }
            if (form_error("product_id")) {
                $single_line_message = form_error("product_id");
            }
            if (form_error("unique_id")) {
                $single_line_message = form_error("unique_id");
            }
            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $product_id = $this->input->post('product_id');
            $product_inventory_id = $this->input->post('product_inventory_id');
            $unique_id = $this->input->post('unique_id');
            $response = $this->fashion_designer_cart_model->get_cart_comment($product_id, $product_inventory_id, $unique_id);
            $arr = [
                "status" => "valid",
                "message" => "Cart comment",
                "data" => $response
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

    function update_cart_quantity() {

        $this->form_validation->set_rules("quantity", "Quantity", "required", array(
            'required' => 'Quantity cannot be empty',
        ));
        $this->form_validation->set_rules("cart_id", "Cart id", "required", array(
            'required' => 'Cart id cannot be empty',
        ));
        $this->form_validation->set_rules("product_inventory_id", "Product inventory id", "required", array(
            'required' => 'Product inventory id cannot be empty',
        ));
        $this->form_validation->set_rules("type", "Type", "required", array(
            'required' => 'Type cannot be empty',
        ));
        if ($this->form_validation->run() == FALSE) {
            $errors = [
                "quantity" => form_error("quantity"),
                "cart_id" => form_error("cart_id"),
                "product_inventory_id" => form_error("product_inventory_id"),
                "type" => form_error("type")
            ];
            $single_line_message = "";
            if (form_error("quantity")) {
                $single_line_message = form_error("quantity");
            }
            if (form_error("cart_id")) {
                $single_line_message = form_error("cart_id");
            }
            if (form_error("product_inventory_id")) {
                $single_line_message = form_error("product_inventory_id");
            }
            if (form_error("type")) {
                $single_line_message = form_error("type");
            }
            $arr = [
                "status" => "invalid",
                "message" => strip_tags($single_line_message),
                "data" => $errors
            ];
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        } else {
            $quantity = $this->input->post('quantity');
            $cart_id = $this->input->post('cart_id');
            $product_inventory_id = $this->input->post('product_inventory_id');
            $type = $this->input->post('type');
            if ($type == "add") {
                $check_cart_product_stock_eceeded = $this->fashion_designer_cart_model->check_cart_products_stock_exceeded_for_product($product_inventory_id, $quantity);
                if ($check_cart_product_stock_eceeded == "out_of_stock") {
                    $arr = [
                        'status' => 'invalid',
                        "message" => $quantity . "  stock not available for this product"
                    ];
                    echo json_encode($arr, JSON_PRETTY_PRINT);
                    die;
                }
            }
            if ($type == "add") {
                $updated_quantity = $quantity + 1;
            } else if ($type == "remove") {
                $updated_quantity = $quantity - 1;
            }
            if ($updated_quantity == 0) {
                $arr = [
                    'status' => 'invalid',
                    "message" => "Qunatity should be greater than zero"
                ];
                echo json_encode($arr, JSON_PRETTY_PRINT);
                die;
            }
            $response = $this->fashion_designer_cart_model->update_cart_quantity($updated_quantity, $cart_id, $type);
            if ($response) {
                $arr = [
                    "status" => "valid",
                    "message" => "Quantity updated successfully"
                ];
            } else {
                $arr = [
                    "status" => "invalid",
                    "message" => "Quantity not updated.Please try later"
                ];
            }
            echo json_encode($arr, JSON_PRETTY_PRINT);
            die;
        }
    }

}
