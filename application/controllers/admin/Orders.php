<?php

class Orders extends CI_Controller {

    private $data;

    function __construct() {
        parent::__construct();
        if ($this->site_model->check_for_user_logged() == false) {
            redirect("admin/login");
        }

        $this->load->model('send_email_model');
        $this->load->model('stock_management_model');

        $this->data['add_subject'] = "Orders List";
        $this->data['subject'] = plural($this->data['add_subject']);
        $this->data['primary_table_name'] = "subscriptions_transactions";
        $this->data['current_page_link'] = base_url() . "admin/" . strtolower(__CLASS__);

        $this->data['primary_uri_segment_name'] = $this->data['primary_table_name'];

        $this->data['column_name_display_as'] = array('time' => 'Date and Time');
        $this->data['hide_columns_in_list_view'] = array("updated_at", "password", "salt", "selected_styles", 'preferences_likes',
            'preferences_dislikes', 'city', 'state', 'district', 'landmark', 'street_name', 'is_billing_address_checked', 'is_terms_and_conditions_checked',
            'shirt_size', 't_shirt_size', 'casual_pant_size', 'formal_pant_size', 'jeans_size', 'shoes_size', 'height', 'weight', 'packages_id', 'address_email', 'alternative_phone_number', 'total_price',
            'discount', 'coupon_code', 'delivery_charges', 'payment_type', 'razorpay_payment_id', 'razorpay_signature', 'expiry_date', 'status', 'coupon_discount', 'package_title', 'amount_paid', 'cancelled_date', 'dispatched_date',
            'shipped_date', 'estimated_delivery_date', 'delivered_date', 'no_of_items', 'admin_address_id', 'admin_address', 'selected_sizes', 'is_exchanged', 'exchange_request_date', 'package_amount', 'package_duration'
        );

        $this->data['hide_columns_in_edit_view'] = array("updated_at", 'unique_code');
        $this->data['hide_columns_in_add_view'] = array("unique_code", "status");
        $this->data['image_fields'] = array('image');
        $this->data['email_fields'] = array('username', 'email');

        $this->data['action_buttons'] = array(
            "delete_action" => false,
            "edit_action" => false,
            "add_action" => false,
            "view_action" => false
        );

        $this->data['button_size'] = "xs";

        $this->data['unset_all_actions'] = false;

        $this->data['editor_type'] = "standard";
        $this->data['password_fields'] = array("password");
        $this->data['readonly_fields'] = array();

        $this->data['relation_fields'] = array(
            'user_id' => array('table_name' => 'users', 'display_column_name' => 'id, username', "condition" => array("")),
            'states_id' => array('table_name' => 'states', 'display_column_name' => 'name', "condition" => array("")),
            'districts_id' => array('table_name' => 'districts', 'display_column_name' => 'name', "condition" => array("")),
            'cities_id' => array('table_name' => 'cities', 'display_column_name' => 'name', "condition" => array("")),
            'packages_id' => array('table_name' => 'packages', 'display_column_name' => 'title', "condition" => array("")),
            'customers_id' => array('table_name' => 'customers', 'display_column_name' => 'name', "condition" => array("")),
            'admin_address_id' => array('table_name' => 'admin_addresses', 'display_column_name' => 'address_title', "condition" => array(""))
        );
        $this->data['multiple_selection_of_options'] = array("role_ids");

        $this->data['column_name_display_as'] = array(
            'user_id' => 'User', 'time' => 'Date and Time', "states_id" => "State", "cities_id" => "City", "districts_id" => "District",
            'unq_id' => 'Order Id', 'packages_id' => 'Package Name',
            'customers_id' => 'Customer Name', 'created_at' => 'Ordered At', 'expiry_date' => "Package Expiry Date",
            "fashion_manager_id" => "Assigned Fashion Manager", "fashion_analyst_id" => "Assigned Fashion Analyst",
            'admin_address_id' => 'Admin Address Title', "address_name" => "Customer Name at Address");

        $this->data['unique_fileds'] = array();

        $this->data['signle_file_browse_fileds'] = array();

        $this->data["image_columns_properties"] = array(
            "image" => array("max_size" => "10000", //in kb format only
                "accepted_file_formats" => array("png", "jpeg", "jpg", "gif"))
        );

        $this->data['date_fields'] = array(
            'type_date'
        );

        $this->data['add_action_buttons'] = array();

        $this->data["generate_random_key_fields"] = array('salt');

        if ($this->input->is_ajax_request()) {
            if (isset($_GET['existed_value'])) {
                $keys = array_keys($_POST);
                if ($this->db->get_where($this->data['primary_table_name'], [$keys[0] => $_REQUEST[$keys[0]], $keys[0] . "!=" => $_REQUEST['existed_value']])->num_rows()) {
                    echo "false";
                } else {
                    echo "true";
                }
            } else {
                $keys = array_keys($_POST);
                if ($this->db->get_where($this->data['primary_table_name'], [$keys[0] => $_REQUEST[$keys[0]]])->num_rows()) {
                    echo "false";
                } else {
                    echo "true";
                }
            }
            exit;
        }

        $this->data["fileds_info"] = $this->curd_model->get_fields($this->data['primary_table_name']);

        if (!empty($_POST)) {


            if (isset($_POST['submit'])) {

                foreach ($this->data['date_fields'] as $key_item) {
                    if ($this->db->field_exists($key_item, $this->data['primary_table_name'])) {
                        $_POST[$key_item] = get_database_date_format_from_us($_POST[$key_item]);
                    }
                }
                if ($_POST['submit'] == "insert") {
                    unset($_POST['submit']);
                    //print_r($_POST);

                    foreach ($this->data['generate_random_key_fields'] as $key_item) {
                        if ($this->db->field_exists($key_item, $this->data['primary_table_name'])) {
                            $_POST[$key_item] = $this->curd_model->generate_random_string_code($this->data['primary_table_name'], $key_item);
                        }
                    }
                    $_POST['password'] = md5($_POST['password'] . $_POST['salt']);

                    $keys = array_keys($_POST);

                    $obj = new Requests();
                    $obj->is_password_update_required = FALSE;
                    $obj->tableName = $this->data['primary_table_name'];
                    $obj->fillable = $keys;

                    foreach ($this->data['image_fields'] as $item) {
                        if (isset($_FILES[$item . '__i']['name'])) {
                            if ($_FILES[$item . '__i']['name'] != '') {
                                array_push($obj->fillable, $item . '__i');
                            }
                        }
                    }
                    if ($obj->posted_values_dynamic_save()) {
                        redirect($this->data['current_page_link'] . "?msg=add");
                        //any extra code such as email, sms functionality code
                        //js_redirect($back_button_nav."?msg=add");
                    }
                } else if ($_POST['submit'] == "update") {

                    unset($_POST['submit']);

                    $obj = new Requests();

                    $id = $_POST['id'];
                    if (isset($_POST['password'])) {
                        if ($_POST['password'] != '') {
                            foreach ($this->data['generate_random_key_fields'] as $key_item) {
                                echo $_POST[$key_item] = $this->curd_model->generate_random_string_code($this->data['primary_table_name'], $key_item);
                            }
                            $_POST['password'] = md5($_POST['password'] . $_POST['salt']);
                            $password = $_POST['password'];
                            unset($_POST['password']);
                            $obj->is_password_update_required = FALSE;
                        }
                    }

                    $keys = array_keys($_POST);

                    $obj->tableName = $this->data['primary_table_name'];
                    $obj->whereCnd = array("id" => $id);
                    $obj->fillable = $keys;
                    foreach ($this->data['image_fields'] as $item) {
                        if (isset($_FILES[$item . '__i']['name'])) {
                            if ($_FILES[$item . '__i']['name'] != '') {
                                array_push($obj->fillable, $item . '__i');
                            }
                        }
                    }
                    if (isset($password)) {
                        $_POST['password'] = $password;
                        array_push($obj->fillable, 'password');
                    }

                    if ($obj->posted_values_dynamic_update()) {
                        //any extra code such as email, sms functionality code
                        redirect($this->data['current_page_link'] . "?msg=update");
                    }
                }
            }
        }
    }

    function admin_view($design = null) {
        $this->load->view("admin/includes/header", $this->data);
        $this->load->view("admin/curd_files/" . $design);
        $this->load->view("admin/includes/footer", $this->data);
    }

    function assign($tt = false, $expected_date = "") {
        $ins_data = array(
            "fashion_manager_id" => $this->input->post('fashion_manger_id'),
            "fashion_analyst_id" => $this->input->post('fashion_analyst_id'),
            "order_status" => 'Assigned to Fashion Analyst',
            "admin_address_id" => $this->input->post('admin_address_id'),
            "order_budget" => $this->input->post('order_budget'),
            "admin_address" => $this->u_model->get_data_conditon_row("admin_addresses", array("id" => $this->input->post('admin_address_id')))->address
        );
        if (!empty($expected_date)) {
            $ins_data['estimated_delivery_date'] = $expected_date;
        }
        $this->db->set($ins_data);
        $this->db->where("id", $this->input->post('order_id'));
        $update = $this->db->update('subscriptions_transactions');
        if ($update) {
            $fashion_manager_data = $this->u_model->get_nostatcheck_row("designers_and_analysts", array("id" => $this->input->post('fashion_manger_id')));
            $fashion_analyst_data = $this->u_model->get_nostatcheck_row("designers_and_analysts", array("id" => $this->input->post('fashion_analyst_id')));
            $this->data['message'] = "<center>Hi,you've been assigned to an Order, Order No #" . $this->input->post('order_real_id') . ".<br><br><br>"
                    . "<hr>"
                    . "Click here to Login to your Panel " . base_url() . 'fashion_designer_manager/login'
                    . "</center>";
            $message = $this->load->view('email_templates/orders_message_template', $this->data, true);
            $this->send_email_model->send_mail($fashion_manager_data->email, 'Order Assigned', $message);
            $this->send_email_model->send_mail($fashion_analyst_data->email, 'Order Assigned', $message);
            $fm = $this->u_model->get_data_conditon_row("designers_and_analysts", array("id" => $this->input->post('fashion_manger_id')));
            $fa = $this->u_model->get_data_conditon_row("designers_and_analysts", array("id" => $this->input->post('fashion_analyst_id')));
            $log = array(
                "order_real_id" => $this->input->post('order_id'),
                "order_id" => $this->input->post('order_real_id'),
                "action_user_id" => $this->session->userdata("user_id"),
                "table_name" => 'users',
                "log" => "Admin Has Assigned / Updated the " . $fm->name . " (" . $fm->ref_id . ") Fashion Manger and " . $fa->name . " (" . $fa->ref_id . ") Fashion Analyst to the Order",
                "created_at" => time(),
                "order_status" => "admin_assigned_to_fashion",
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->db->insert("orders_log", $log);
            if ($tt) {
                return true;
            } else {
                redirect($this->data['current_page_link'] . "?msg=update");
            }
        } else {
            redirect($this->data['current_page_link'] . "?msg=error");
        }
    }

    function update_previous_order_status($unq_id) {
        $result = $this->db->select('id')->get_where('orders', ['order_id' => $unq_id, 'status' => 1])->result();
        foreach ($result as $r) {
            $this->db->set('status', 0);
            $this->db->where('id', $r->id);
            $this->db->update('orders');
        }
    }

    function assign_exchange() {
        $expected_date = strtotime($this->input->post("estimated_delivery_date"));
        $this->assign(true, $expected_date);
        $this->load->model("fashion_manager_orders_model");
        $product_order_id = $this->u_model->get_data_conditon_row("orders", array("subscription_transaction_id" => $this->input->post('order_id')))->id;
        $subs_trans = $this->u_model->get_data_conditon_row("subscriptions_transactions", array("id" => $this->input->post('order_id')));
        $reason = "This Order is Reassigned due to Exchange";
        $this->update_previous_order_status($subs_trans->unq_id);
        $reassign = $this->fashion_manager_orders_model->reject_order($product_order_id, $reason, 1, "");
        if ($reassign) {
            $order_log_array = [
                "order_real_id" => $this->input->post('order_id'),
                "order_id" => $subs_trans->unq_id,
                "order_status" => "exchange_order",
                "log" => $reason,
                "action_date_time" => date('Y-m-d H:i:s'),
                "created_at" => time(),
                "action_user_id" => $this->session->userdata("user_id"),
                "table_name" => 'users',
                "from_table" => 'subscription_transactions'
            ];
            $this->db->insert("orders_log", $order_log_array);
            $this->session->set_userdata("s_scss", "Exchange Order Assigned Succesfully");
            redirect($this->data['current_page_link']);
        } else {
            $this->session->set_userdata("s_error", "Something went Wrong, please try again");
            redirect($this->data['current_page_link']);
        }
    }

    function index() {
        $where = array(
            "status" => 1,
            "designation" => "Fashion Designer"
        );
        $this->data['fashion_managers'] = $this->u_model->get_data("designers_and_analysts", $where);
        $where = array(
            "status" => 1,
            "designation" => "Fashion Analyst"
        );
        $this->data['fashion_analysts'] = $this->u_model->get_data("designers_and_analysts", $where);
        $this->data['admin_addresses'] = $this->u_model->get_data("admin_addresses", array("status" => true));

        // Tabs Filter Starts Here

        if (!empty($this->input->get('type'))) {
            $type = $this->input->get('type');
            if ($type == 'inprogress') {
                $this->db->where("(order_status = 'Order Placed' OR order_status = 'Assigned to Fashion Analyst')");
            }
            if ($type == 'dispatched') {
                $this->db->where("order_status = 'Dispatched'");
            }
            if ($type == 'shipped') {
                $this->db->where("order_status = 'Shipped'");
            }
            if ($type == 'delivered') {
                $this->db->where("order_status = 'Delivered'");
            }
            if ($type == 'exchange') {
                $this->db->where("order_status = 'Exchange Requested'");
            }
            if ($type == 'cancellation_pending') {
                $this->db->where("order_status = 'Cancellation Requested'");
            }
            if ($type == 'cancelled') {
                $this->db->where("order_status = 'Cancelled'");
            }
        }
        $this->db->order_by("is_exchanged", "desc");
        $this->data["list_items"] = $this->u_model->get_data($this->data['primary_table_name'], "", "", "id", "desc");
        foreach ($this->data["list_items"] as $item) {

            $item->cancellation_requested_date = empty($item->cancellation_requested_date) ? $item->cancellation_requested_date : date("d M Y, h:i A", $item->cancellation_requested_date);
            $feedback = $this->u_model->get_data_conditon_row("orders_feedbacks", array("order_id" => $item->id));

            $item->add_action_buttons[] = array(
                "title" => "Download Invoice",
                "target" => "",
                "onclick" => "generateInvoice('$item->id','$item->unq_id')",
                "btn_class" => "primary",
                "data-toggle" => "",
                "data-target" => "",
                "btn_size" => "xs"
            );

            if (!empty($feedback)) {
                $item->add_action_buttons[] = array(
                    "title" => "View Feedback",
                    "onclick" => "showOrderFeedback('" . $feedback->rating . "','" . $feedback->feedback_message . "','" . $feedback->order_unq_id . "')",
                    "btn_class" => "success",
                    "data-toggle" => "modal",
                    "data-target" => "#viewFeedbackModal",
                    "btn_size" => "xs"
                );
            }

            if ($item->order_status == 'Cancellation Requested') {
                $item->add_action_buttons[] = array(
                    "title" => "Approve Cancellation",
                    "target" => "",
                    "onclick" => "approveCancellation('$item->id','$item->unq_id')",
                    "btn_class" => "primary",
                    "data-toggle" => "modal",
                    "data-target" => "#approve_cancellation",
                    "btn_size" => "xs"
                );
            }


            $item->add_action_buttons[] = array(
                "title" => "View Order",
                "link" => base_url() . "admin/orders/edit/" . $item->id,
                "target" => "",
                "btn_class" => "warning",
                "btn_size" => "xs"
            );
            if ($item->order_status == 'Dispatched') {
                $item->add_action_buttons[] = array(
                    "title" => "Mark as Shipped",
                    "link" => "javascript: void(0);",
                    "target" => "",
                    "btn_class" => "primary",
                    "btn_size" => "xs",
                    "onclick" => "orderMarkAsShipped('" . $item->id . "')"
                );
            }
            if ($item->order_status == 'Shipped') {
                $item->add_action_buttons[] = array(
                    "title" => "Mark as Delivered",
                    "link" => "javascript: void(0);",
                    "target" => "",
                    "btn_class" => "primary",
                    "btn_size" => "xs",
                    "onclick" => "orderMarkAsDelivered('" . $item->id . "')"
                );
            }


            if ($item->order_status == 'Order Placed' || $item->order_status == 'Assigned to Fashion Analyst') {
                $item->add_action_buttons[] = array(
                    "title" => (!$item->fashion_manager_id) ? "Assign Fashion Manager / Fashion Analyst" : "Re-assign Fashion Manager / Fashion Analyst",
                    "target" => "",
                    "onclick" => "assignOrderId('" . $item->id . "','" . $item->unq_id . "','" . $item->fashion_manager_id . "','" . $item->fashion_analyst_id . "','" . $item->order_budget . "','" . $item->admin_address_id . "');",
                    "btn_class" => "success",
                    "data-toggle" => "modal",
                    "data-target" => "#assignFashionManagerModal",
                    "btn_size" => "xs"
                );
            }

            if ($item->order_status == 'Exchange Requested') {
                $item->add_action_buttons[] = array(
                    "title" => "Assign and Process",
                    "target" => "",
                    "onclick" => "exchangeRequest('$item->id','$item->unq_id','$item->fashion_manager_id','$item->fashion_analyst_id','$item->order_budget','$item->admin_address_id ');",
                    "btn_class" => "primary",
                    "data-toggle" => "modal",
                    "data-target" => "#reassignExchangeOrder",
                    "btn_size" => "xs"
                );
            }
            $item->add_action_buttons[] = array(
                "title" => "View Logs",
                "target" => "",
                "onclick" => "viewLogs('$item->unq_id');",
                "btn_class" => "info",
                "data-toggle" => "modal",
                "data-target" => "#logsModal",
                "btn_size" => "xs"
            );

            if ($item->is_exchanged == 1) {
                $item->unq_id = $item->unq_id . '<br><b style="color: tomato"> ( Exchange Order ) </b>';
            }

            if ($item->is_we_shop_for_you == 1) {
                $item->unq_id = $item->unq_id . '<br><b style="color: green"> ( We Shop For You Order ) </b>';
            }
            if (!empty($item->fashion_manager_id)) {
                $temp = $this->u_model->get_data_conditon_row("designers_and_analysts", array("id" => $item->fashion_manager_id));
                $item->fashion_manager_id = '<b>Id : </b>' . $temp->ref_id . ',<br><b>Name : </b>' . $temp->name;
            }
            if (!empty($item->fashion_analyst_id)) {
                $temp = $this->u_model->get_data_conditon_row("designers_and_analysts", array("id" => $item->fashion_analyst_id));
                $item->fashion_analyst_id = '<b>Id : </b>' . $temp->ref_id . ',<br><b>Name : </b>' . $temp->name;
            }
        }
        $this->admin_view("orders_list_view");
    }

    function add() {
        if ($this->data["action_buttons"]['add_action'] == false) {
            die("Access Deined for add operation");
        }
        $this->admin_view("add_view");
    }

    function edit($item_primary_key) {

        $this->data['edit_item_row'] = $this->curd_model->get_row_from_pk($this->data['primary_table_name'], $item_primary_key);
        $this->db->where("id", $this->data['edit_item_row']->packages_id);
        $res = $this->db->get('packages')->row();
        $this->data['edit_item_row']->package_details = $res;
        $this->data['selected_sizes'] = $this->get_selected_sizes($this->data['edit_item_row']->selected_sizes);

        $this->admin_view("view_edit_orders");
    }

    function delete($item_primary_key) {
        if ($this->data["action_buttons"]['delete_action'] == true) {
            $this->db->where("id", $item_primary_key);
            $this->db->delete($this->data['primary_table_name']);
            redirect($this->data['current_page_link'] . "?msg=delete");
        } else {
            redirect($this->data['current_page_link']);
        }
    }

    function get_selected_sizes($selected_sizes) {
        $selected_sizes_arr = json_decode($selected_sizes);
        $arr = [];
        foreach ($selected_sizes_arr as $key => $value) {
            $obj = new stdClass();
            $obj->category = $this->db->get_where('categories', ['id' => $key])->row()->name;
            $obj->size = $this->db->get_where('sizes', ['id' => $value])->row()->title;
            $arr[] = $obj;
        }
        return $arr;
    }

    function toggle_status($item_primary_key) {
        $current_status = $this->curd_model->get_status($this->data['primary_table_name'], $item_primary_key);
        if ($current_status != FALSE) {
            $this->curd_model->toggle_status($this->data['primary_table_name'], $item_primary_key, $current_status);
        }
        redirect($this->data['current_page_link'] . "?msg=update");
    }

    function mark_as_shipped($id) {
        $update = $this->u_model->update_data($this->data['primary_table_name'], array('order_status' => "Shipped", 'shipped_date' => time()), array("id" => $id));
        $order_data = $this->u_model->get_nostatcheck_row($this->data['primary_table_name'], array("id" => $id));
        $customer_id = $order_data->customers_id;
        $customer_data = $this->u_model->get_nostatcheck_row('customers', array("id" => $customer_id));
        $this->data['message'] = "Hi " . $customer_data->name . ",Your Order with Id #" . $order_data->unq_id . " has been Shipped";
        $message = $this->load->view('email_templates/orders_message_template', $this->data, true);

        if ($update) {
            $this->send_email_model->send_mail($customer_data->email, "Order Shipped", $message);
            $log = array(
                "order_real_id" => $order_data->id,
                "order_id" => $order_data->unq_id,
                "action_user_id" => $this->session->userdata("user_id"),
                "table_name" => 'users',
                "log" => "Admin Has marked this order $order_data->unq_id as Shipped",
                "created_at" => time(),
                "order_status" => "order_shipped",
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->db->insert("orders_log", $log);
            redirect($this->data['current_page_link'] . "?type=dispatched&msg=update");
        } else {
            redirect($this->data['current_page_link'] . "?type=dispatched&msg=error");
        }
    }

    function mark_as_delivered($id) {
        $update = $this->u_model->update_data($this->data['primary_table_name'], array('order_status' => "Delivered", 'delivered_date' => time()), array("id" => $id));
        $order_data = $this->u_model->get_nostatcheck_row($this->data['primary_table_name'], array("id" => $id));
        $customer_id = $order_data->customers_id;
        $customer_data = $this->u_model->get_nostatcheck_row('customers', array("id" => $customer_id));
        $this->data['message'] = "Hi " . $customer_data->name . ",Your Order with Id #" . $order_data->unq_id . " has been Delivered";
        $message = $this->load->view('email_templates/orders_message_template', $this->data, true);
        if ($update) {
            $this->send_email_model->send_mail($customer_data->email, "Order Delivered", $message);
            $log = array(
                "order_real_id" => $order_data->id,
                "order_id" => $order_data->unq_id,
                "action_user_id" => $this->session->userdata("user_id"),
                "table_name" => 'users',
                "log" => "Admin Has marked this order $order_data->unq_id as Delivered",
                "created_at" => time(),
                "order_status" => "order_delivered",
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->db->insert("orders_log", $log);
            redirect($this->data['current_page_link'] . "?type=shipped&msg=update");
        } else {
            redirect($this->data['current_page_link'] . "?type=shipped&msg=error");
        }
    }

    function approve_cancellation() {
        $id = $this->input->post("order_id");
        $message = $this->input->post("cancellation_approved_message");
        $update = $this->u_model->update_data($this->data['primary_table_name'], array('order_status' => "Cancelled", 'cancellation_approved_message' => $message, 'cancelled_date' => time()), array("id" => $id));
        $order_data = $this->u_model->get_nostatcheck_row($this->data['primary_table_name'], array("id" => $id));
        $customer_id = $order_data->customers_id;
        $customer_data = $this->u_model->get_nostatcheck_row('customers', array("id" => $customer_id));
        $this->data['message'] = "Hi " . $customer_data->name . ",Your Order with Id #" . $order_data->unq_id . " has been Cancelled, the amount paid will be credited back into your account.";
        $message = $this->load->view('email_templates/orders_message_template', $this->data, true);
        if ($update) {
            $this->u_model->update_data('orders', array('order_status' => "order_cancelled", 'updated_at' => time()), array("subscription_transaction_id" => $id));
            $this->send_email_model->send_mail($customer_data->email, "Order Cancelled", $message);
            $log = array(
                "order_real_id" => $order_data->id,
                "order_id" => $order_data->unq_id,
                "action_user_id" => $this->session->userdata("user_id"),
                "table_name" => 'users',
                "log" => "Admin Has marked this order $order_data->unq_id as Cancelled",
                "created_at" => time(),
                "order_status" => "order_cancelled",
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->db->insert("orders_log", $log);
            redirect($this->data['current_page_link'] . "?type=cancellation_pending&msg=update");
        } else {
            redirect($this->data['current_page_link'] . "?type=cancellation_pending&msg=error");
        }
    }

}
