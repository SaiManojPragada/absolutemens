<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Withdraw_history extends CI_Controller {

    private $data;

    function __construct() {
        parent::__construct();
        if ($this->site_model->check_for_user_logged() == false) {
            redirect("admin/login");
        }
        $this->load->model('admin_withdraw_history_model');
    }

    function admin_view($design = null) {
        $this->load->view("admin/includes/header", $this->data);
        $this->load->view("admin/" . $design);
        $this->load->view("admin/includes/footer", $this->data);
    }

    function index() {
        $filters['owner_name'] = $this->input->get('owner_name');
        $filters['owner_email'] = $this->input->get('owner_email');
        $filters['owner_contact_number'] = $this->input->get('owner_contact_number');
        $filters['request_id'] = $this->input->get('request_id');
        $filters['withdraw_status'] = $this->input->get('withdraw_status');
        $filters['request_id'] = $this->input->get('request_id');
        $filters['from_date'] = $this->input->get('from_date');
        $filters['to_date'] = $this->input->get('to_date');
        $this->data["data"] = $this->admin_withdraw_history_model->get($filters);
        $this->admin_view("withdraw_history");
    }

    function approve() {
        $approve_comment = $this->input->post('approve_comment');
        $withdraw_request_id = $this->input->post('withdraw_request_id');
        if ($approve_comment == "") {
            $this->session->set_flashdata('error', "Comment is required");
            redirect(base_url() . 'admin/withdraw_history', 'refresh');
        }
        if ($withdraw_request_id == "") {
            $this->session->set_flashdata('error', "Request id is required");
            redirect(base_url() . 'admin/withdraw_history', 'refresh');
        }
        $response = $this->admin_withdraw_history_model->approve_request($approve_comment, $withdraw_request_id);
        if ($response) {
            $this->session->set_flashdata('success', "Request approved successfully");
            redirect(base_url() . 'admin/withdraw_history', 'refresh');
        } else {
            $this->session->set_flashdata('error', "Request not approved.Please try again later");
            redirect(base_url() . 'admin/withdraw_history', 'refresh');
        }
    }

    function reject() {
        $reject_reason = $this->input->post('reject_reason');
        $withdraw_request_id = $this->input->post('withdraw_request_id');
        if ($reject_reason == "") {
            $this->session->set_flashdata('error', "Reject reason is required");
            redirect(base_url() . 'admin/withdraw_history', 'refresh');
        }
        if ($withdraw_request_id == "") {
            $this->session->set_flashdata('error', "Request id is required");
            redirect(base_url() . 'admin/withdraw_history', 'refresh');
        }
        $response = $this->admin_withdraw_history_model->reject_request($reject_reason, $withdraw_request_id);
        if ($response) {
            $this->session->set_flashdata('success', "Request rejected successfully");
            redirect(base_url() . 'admin/withdraw_history', 'refresh');
        } else {
            $this->session->set_flashdata('error', "Request not rejected.Please try again later");
            redirect(base_url() . 'admin/withdraw_history', 'refresh');
        }
    }

}
