<?php

class Dashboard extends CI_Controller {

    private $data;

    function __construct() {
        parent::__construct();
        if ($this->site_model->check_for_user_logged() == false) {
            redirect("admin/login");
        }
    }

    function admin_view($design = null, $data = null) {
        $this->load->view("admin/includes/header", $this->data);
        $this->load->view("admin/" . $design, $data);
        $this->load->view("admin/includes/footer", $this->data);
    }

    function index() {
        $data['registered_users'] = sizeof($this->u_model->get_nostatcheck_data("customers"));
        $data['fashion_managers'] = sizeof($this->u_model->get_nostatcheck_data("designers_and_analysts", array('designation' => 'Fashion Designer')));
        $data['fashion_analysts'] = sizeof($this->u_model->get_nostatcheck_data("designers_and_analysts", array('designation' => 'Fashion Analyst')));
        $data['vendors'] = sizeof($this->u_model->get_nostatcheck_data("vendors"));
        $data['total_orders'] = sizeof($this->u_model->get_nostatcheck_data("subscriptions_transactions", array("status" => 1)));
        $data['completed_orders'] = sizeof($this->u_model->get_nostatcheck_data("subscriptions_transactions", array("order_status" => "Delivered")));
        $data['cancelled_orders'] = sizeof($this->u_model->get_nostatcheck_data("subscriptions_transactions", array("order_status" => "Cancelled")));
        $data['pending_orders'] = sizeof($this->u_model->get_nostatcheck_data("subscriptions_transactions", "order_status != 'Cancelled' AND order_status != 'Delivered'"));
        $data['completed_orders_amount'] = $this->u_model->get_total("subscriptions_transactions", array("order_status" => 'Delivered'), 'amount_paid');
        $this->db->where("status", 1);
        $data['pending_orders_amount'] = $this->u_model->get_total("subscriptions_transactions", "order_status != 'Cancelled' AND order_status != 'Delivered'", 'amount_paid');
        $now = time();
        $today_start = strtotime(date('y-m-d') . ' 00:00');
        $data['todays_orders'] = $this->u_model->get_nostatcheck_data("subscriptions_transactions", "updated_at < $now AND updated_at >= $today_start");
        foreach ($data['todays_orders'] as $item) {
            $item->customer_details = $this->u_model->get_nostatcheck_row("customers", array("id" => $item->customers_id));
            $item->package_details = $this->u_model->get_nostatcheck_row("packages", array("id" => $item->packages_id));
        }
        $this->admin_view("dashboard", $data);
    }

}
