<?php

class Product_orders extends CI_Controller {

    private $data;

    function __construct() {
        parent::__construct();
    }

    function index() {
        if ($this->site_model->check_for_user_logged() == false) {
            redirect("admin/login");
        }
        $this->data['subject'] = "Manage Product Orders";
        $this->db->where("order_status != 'rejected_by_fashion_manager'");

        if (!empty($this->input->get('type'))) {
            $type = $this->input->get('type');
            if ($type === "inprogress") {
                $this->db->where("order_status != 'order_completed' AND order_status != 'order_cancelled'");
            }

            if ($type === "completed") {
                $this->db->where("order_status = 'order_completed'");
            }

            if ($type === "cancelled") {
                $this->db->where("order_status = 'order_cancelled'");
            }
        }
        $this->data['list_items'] = $this->u_model->get_data("orders");
        foreach ($this->data['list_items'] as $k => $item) {
            $item->order_pending = 0;
            $item->order_dispatched_by_vendor = 0;
            $item->order_delivered = 0;
            $item->order_cancelled_by_vendor = 0;
            $where = array("order_id" => $item->id, 'approved_by_fashion_manager' => 'yes');
            $item->order_products = $this->u_model->get_data("order_products", $where);

            $where = array("id" => $item->fashion_manager_id);
            $item->fashion_manager = $this->u_model->get_nostatcheck_row("designers_and_analysts", $where);

            $where = array("id" => $item->fashion_analyst_id);
            $item->fashion_analyst = $this->u_model->get_nostatcheck_row("designers_and_analysts", $where);

            foreach ($item->order_products as $chec) {
                if ($chec->order_status == "order_placed_by_admin") {
                    $item->order_pending = $item->order_pending + 1;
                }
                if ($chec->order_status == "order_dispatched_by_vendor") {
                    $item->order_dispatched_by_vendor = $item->order_dispatched_by_vendor + 1;
                }
                if ($chec->order_status == "order_delivered") {
                    $item->order_delivered = $item->order_delivered + 1;
                }
                if ($chec->order_status == "order_cancelled_by_vendor") {
                    $item->order_cancelled_by_vendor = $item->order_cancelled_by_vendor + 1;
                }
            }
            if ($this->input->get('inprogress') == "tobeordered") {
                if ($item->order_status !== 'approved_by_fashion_manager') {
                    unset($this->data['list_items'][$k]);
                }
            }
            if ($this->input->get('inprogress') == "ordered") {
                if ($item->order_status !== 'order_placed_by_admin' || $item->order_delivered > $item->order_pending + $item->order_dispatched_by_vendor) {
                    unset($this->data['list_items'][$k]);
                }
            }
            if ($this->input->get('inprogress') == "delivered") {
                if ($item->order_pending != 0 || $item->order_dispatched_by_vendor != 0) {
                    unset($this->data['list_items'][$k]);
                }
            }
        }
        $this->load->view("admin/includes/header", $this->data);
        $this->load->view("admin/curd_files/products_orders_list", $this->data);
        $this->load->view("admin/includes/footer", $this->data);
    }

    function get_order_items() {
        $id = $this->input->post('id');
        $where = array("order_id" => $id, "approved_by_fashion_manager" => "yes");
        $data = $this->u_model->get_data("order_products", $where);
        foreach ($data as $item) {
            $item->product_details = $this->u_model->get_nostatcheck_row("products", array("id" => $item->product_id));
            $item->vendor_details = $this->u_model->get_nostatcheck_row("vendors", array("id" => $item->vendor_id));
            $item->inventory = $this->u_model->get_nostatcheck_row("product_inventory", array("id" => $item->product_inventory_id));
            $item->size = $this->u_model->get_nostatcheck_row("sizes", array("id" => $item->inventory->size_id));
            $item->product_image = base_url() . $this->u_model->get_data_conditon_row("products_images", array("product_id" => $item->product_id))->image;
            if ($item->order_status == 'order_dispatched_by_vendor' || $item->order_status == "order_placed_by_admin") {
                $color = "orange";
            }
            if ($item->order_status == 'order_delivered' || $item->order_status == "order_completed") {
                $color = "green";
            }
            if ($item->order_status == 'order_cancelled_by_vendor') {
                $color = "red";
            }

            $item->dsp_status = "<span style='background-color: " . $color . "; padding: 3px 10px; color: white; border-radius: 10px'>" . ucfirst(str_replace("_", " ", $item->order_status)) . "</span>";
        }
        if ($data) {
            echo json_encode($data);
        } else {
            echo false;
        }
        die;
    }

    function place_order($id, $order_id) {
        $set = array("order_status" => "order_placed_by_admin");
        $where = array("id" => $id);
        $user_id = $this->session->userdata("user_id");
        $update = $this->u_model->update_data("orders", $set, $where);
//        $update = true;
        if ($update) {
            $order_data = $this->u_model->get_nostatcheck_row("orders", array("id" => $id));
            $this->db->group_by("vendor_id");
            $get_vendors_ids = $this->u_model->get_nostatcheck_data("order_products", array("order_id" => $id, "approved_by_fashion_manager" => "yes"));
            foreach ($get_vendors_ids as $ids) {
                $vendor_data = $this->u_model->get_nostatcheck_row("vendors", array("id" => $ids->vendor_id));
                $this->data['message'] = "<center>Hi,you have a new Order, Order No #" . $order_data->order_id . ".<br><br><br>"
                        . "<hr>"
                        . "Click here to Login to your Panel " . base_url() . 'vendor/login'
                        . "</center>";
                $message = $this->load->view('email_templates/orders_message_template', $this->data, true);
//                print_r($message);
                $this->send_email_model->send_mail($vendor_data->contact_email, 'New Order', $message);
            }
            $order_products = $this->db->select('id,product_id')->get_where('order_products', ['order_id' => $id, 'approved_by_fashion_manager' => 'yes'])->result();
            foreach ($order_products as $op) {
                $product_name = $this->db->get_where('products', ['id' => $op->product_id])->row()->title;
                $product_id = $this->db->get_where('products', ['id' => $op->product_id])->row()->product_id;
                $dd = array(
                    "order_real_id" => $id,
                    "order_product_id" => $op->id,
                    "order_id" => $order_id,
                    "log" => "Create Order for $product_name  ($product_id) and Send it to the Vendor Action Performed By the Admin",
                    "action_user_id" => $user_id,
                    "table_name" => "users",
                    "order_status" => "order_placed_by_admin",
                    "created_at" => time(),
                    "action_date_time" => date('Y-m-d H:i:s')
                );
                $this->u_model->post_data("orders_log", $dd);
            }
            redirect($this->agent->referrer() . '?msg=update');
        } else {
            redirect($this->agent->referrer() . '?msg=error');
        }
    }

    function complete_order($id, $order_id) {
        $this->load->model('admin_settlements_model');
        $set = array("order_status" => "order_completed");
        $where = array("id" => $id);
        $user_id = $this->session->userdata("user_id");
        $update = $this->u_model->update_data("orders", $set, $where);
        if ($update) {
            $dd = array(
                "order_real_id" => $id,
                "order_id" => $order_id,
                "log" => "Order Completed Action Performed By the Admin",
                "action_user_id" => $user_id,
                "table_name" => "users",
                "order_status" => "order_completed",
                "created_at" => time(),
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->admin_settlements_model->add_to_vendor_wallet($id);
            $this->u_model->post_data("orders_log", $dd);
            $subscription_trans_id = $this->u_model->get_nostatcheck_row("orders", array("id" => $id))->subscription_transaction_id;
            $order_data = $this->u_model->get_nostatcheck_row('subscriptions_transactions', array("id" => $subscription_trans_id));
            $customer_id = $order_data->customers_id;
            $customer_data = $this->u_model->get_nostatcheck_row('customers', array("id" => $customer_id));
            $this->data['message'] = "Hi " . $customer_data->name . ", Your Order with Id #" . $order_data->unq_id . " has been Dispatched";
            $message = $this->load->view('email_templates/orders_message_template', $this->data, true);
            $this->send_email_model->send_mail($customer_data->email, "Order Dispatched", $message);
            $this->u_model->update_data("subscriptions_transactions", array("dispatched_date" => time(), "order_status" => "dispatched"), array("id" => $subscription_trans_id));
            redirect($this->agent->referrer() . '?msg=update');
        } else {
            redirect($this->agent->referrer() . '?msg=error');
        }
    }

    function cancel_order($id, $order_id) {
        $set = array("order_status" => "order_cancelled");
        $where = array("id" => $id);
        $user_id = $this->session->userdata("user_id");
        $update = $this->u_model->update_data("orders", $set, $where);
        if ($update) {
            $dd = array(
                "order_real_id" => $id,
                "order_id" => $order_id,
                "log" => "Order Cancellation Action Performed By the Admin",
                "action_user_id" => $user_id,
                "table_name" => "users",
                "order_status" => "order_completed",
                "created_at" => time(),
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->u_model->post_data("orders_log", $dd);
            redirect($this->agent->referrer() . '?msg=update');
        } else {
            redirect($this->agent->referrer() . '?msg=error');
        }
    }

}
