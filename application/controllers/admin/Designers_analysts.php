<?php

class Designers_analysts extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if ($this->site_model->check_for_user_logged() == false) {
            redirect("admin/login");
        }
        $this->load->model('send_email_model');

        $this->data['add_subject'] = "Designers Or Analysts";
        $this->data['subject'] = plural($this->data['add_subject']);
        $this->data['primary_table_name'] = "designers_and_analysts";
        $this->data['current_page_link'] = base_url() . "admin/" . strtolower(__CLASS__);

        $this->data['primary_uri_segment_name'] = $this->data['primary_table_name'];

        $this->data['column_name_display_as'] = array('city_name' => 'City Name',
            'role_id' => 'Role',
            'time' => 'Date and Time');
        $this->data['hide_columns_in_list_view'] = array("updated_at", 'password', 'salt', 'password',
            'salt',
            'forgot_password_verfication_code',
            'activation_key',
            'token', 'email_verified');

        $this->data['hide_columns_in_edit_view'] = array("updated_at", 'ref_id');
        $this->data['hide_columns_in_add_view'] = array("status", 'ref_id');
        $this->data['image_fields'] = array('image');
        $this->data['email_fields'] = array('username', 'email');
        $this->data['editor_type'] = "full";

        $this->data['action_buttons'] = array(
            "delete_action" => true,
            "edit_action" => true,
            "add_action" => true,
            "view_action" => false
        );

        $this->data['button_size'] = "xs";

        $this->data['unset_all_actions'] = false;

        $this->data['password_fields'] = array("password");
        $this->data['readonly_fields'] = array();

        $this->data['relation_fields'] = array(
            'role_id' => array('table_name' => 'roles', 'display_column_name' => 'name', "condition" => array("")),
            'states_id' => array('table_name' => 'states', 'display_column_name' => 'name', "condition" => array(""))
        );
        $this->data['multiple_selection_of_options'] = array("role_ids");

        $this->data['column_name_display_as'] = array('role_id' => 'User Role', 'time' => 'Date and Time');

        $this->data['unique_fileds'] = array();

        $this->data['signle_file_browse_fileds'] = array();

        $this->data["image_columns_properties"] = array(
            "image" => array("max_size" => "10000", //in kb format only
                "accepted_file_formats" => array("png", "jpeg", "jpg", "gif"))
        );

        $this->data["extra_jquery_validation"] = array(
            "name" => array(
                array("rule" => "lettersOnly", "rule_value" => "true")
            )
        );

        $this->data['add_action_buttons'] = array();

        $this->data["generate_random_key_fields"] = array("salt");

        if ($this->input->is_ajax_request()) {
            if (isset($_GET['existed_value'])) {
                $keys = array_keys($_POST);
                if ($this->db->get_where($this->data['primary_table_name'], [$keys[0] => $_REQUEST[$keys[0]], $keys[0] . "!=" => $_REQUEST['existed_value']])->num_rows()) {
                    echo "false";
                } else {
                    echo "true";
                }
            } else {
                $keys = array_keys($_POST);
                if ($this->db->get_where($this->data['primary_table_name'], [$keys[0] => $_REQUEST[$keys[0]]])->num_rows()) {
                    echo "false";
                } else {
                    echo "true";
                }
            }
            exit;
        }

        $this->data["fileds_info"] = $this->curd_model->get_fields($this->data['primary_table_name']);

        if (!empty($_POST)) {
            $pass = $_POST['password'];
            if (isset($_POST['submit'])) {
                if ($_POST['submit'] == "insert") {
                    $check = $this->u_model->get_nostatcheck_row($this->data['primary_table_name'], array("email" => $this->input->post('email')));
                    if (!empty($check)) {
                        $this->session->set_userdata('s_error', "User with this E-mail Already exists");
                        unset($_POST);
                        redirect($this->data['current_page_link'] . "?msg=s_error");
                        die;
                    }

                    $check = $this->u_model->get_nostatcheck_row($this->data['primary_table_name'], array("phone_number" => $this->input->post('phone_number')));
                    if (!empty($check)) {
                        $this->session->set_userdata('s_error', "User with this Phone number Already exists");
                        unset($_POST);
                        redirect($this->data['current_page_link'] . "?msg=s_error");
                        die;
                    }


                    $_POST['ref_id'] = $this->generateDesgId($this->data['primary_table_name'], 'ref_id', 6);
                    unset($_POST['submit']);
//                    unset($_POST['password']);
                    //print_r($_POST);
                    foreach ($this->data['generate_random_key_fields'] as $key_item) {
                        echo $_POST[$key_item] = $this->curd_model->generate_random_string_code($this->data['primary_table_name'], $key_item);
                    }

                    $_POST['password'] = md5($_POST['password'] . $_POST['salt']);
                    $keys = array_keys($_POST);

                    $obj = new Requests();
                    $obj->is_password_update_required = false;
                    $obj->tableName = $this->data['primary_table_name'];
                    $obj->fillable = $keys;

                    foreach ($this->data['image_fields'] as $item) {
                        if (isset($_FILES[$item . '__i']['name'])) {
                            if ($_FILES[$item . '__i']['name'] != '') {
                                array_push($obj->fillable, $item . '__i');
                            }
                        }
                    }
                    if ($obj->posted_values_dynamic_save()) {
                        $name = $_POST['name'];
                        $mobile = $_POST['mobile'];
                        $email = $_POST['email'];
                        $playstore_url = $this->db->get_where('site_settings')->row()->playstore_url;
                        $msg = "Dear $name, your registration successful for " . SITE_TITLE . "
Username : $mobile
Download mobile app $playstore_url
";
                        send_message($msg, $mobile);

                        $this->data['message'] = "Hi " . $this->input->post("name") . ",your registration successful as <b>" . $_POST['designation'] . "</b> for " . SITE_TITLE . ".<br><br><br>"
                                . "<center>"
                                . "<hr>"
                                . "<p><b>Email</b></p>"
                                . "<p>" . $this->input->post("email") . "</p>"
                                . "<p><b>password</b></p>"
                                . "<p>" . $pass . "</p>"
                                . "<hr>"
                                . "Click here to Login " . base_url() . 'fashion_designer_manager/login'
                                . "</center>";
                        $message = $this->load->view('email_templates/orders_message_template', $this->data, true);
                        $this->send_email_model->send_mail($this->input->post("email"), 'Registration Success', $message);

                        redirect($this->data['current_page_link'] . "?msg=add");
                        //any extra code such as email, sms functionality code
                        //js_redirect($back_button_nav."?msg=add");
                    }
                } else if ($_POST['submit'] == "update") {
                    unset($_POST['submit']);

                    $obj = new Requests();
                    $id = $_POST['id'];
                    if (isset($_POST['password'])) {
                        if (!empty($_POST['password'])) {
                            foreach ($this->data['generate_random_key_fields'] as $key_item) {
                                echo $_POST[$key_item] = $this->curd_model->generate_random_string_code($this->data['primary_table_name'], $key_item);
                            }
                            $_POST['password'] = md5($_POST['password'] . $_POST['salt']);
                            $password = $_POST['password'];
                            unset($_POST['password']);
                            $obj->is_password_update_required = false;
                        } else {
                            unset($_POST['password']);
                            unset($_POST['salt']);
                        }
                    }

                    $keys = array_keys($_POST);

                    $obj->tableName = $this->data['primary_table_name'];
                    $obj->whereCnd = array("id" => $id);
                    $obj->fillable = $keys;
                    foreach ($this->data['image_fields'] as $item) {
                        if (isset($_FILES[$item . '__i']['name'])) {
                            if ($_FILES[$item . '__i']['name'] != '') {
                                array_push($obj->fillable, $item . '__i');
                            }
                        }
                    }
                    if (isset($password)) {
                        $_POST['password'] = $password;
                        array_push($obj->fillable, 'password');
                    }

                    if ($obj->posted_values_dynamic_update()) {
                        //any extra code such as email, sms functionality code
                        redirect($this->data['current_page_link'] . "?msg=update");
                    }
                }
            }
        }
    }

    function admin_view($design = null) {
        $this->load->view("admin/includes/header", $this->data);
        $this->load->view("admin/curd_files/" . $design);
        $this->load->view("admin/includes/footer", $this->data);
    }

    function index() {
        $this->data["list_items"] = $this->curd_model->get_all_data_from_table($this->data['primary_table_name']);
        $this->admin_view("list_view");
    }

    function add() {
        if ($this->data["action_buttons"]['add_action'] == false) {
            die("Access Deined for add operation");
        }
        $this->admin_view("add_view");
    }

    function edit($item_primary_key) {
        if ($this->data["action_buttons"]['edit_action'] == false) {
            die("Access Deined for edit operation");
        }
        $this->data['edit_item_row'] = $this->curd_model->get_row_from_pk($this->data['primary_table_name'], $item_primary_key);
        $this->admin_view("edit_view");
    }

    function delete($item_primary_key) {
        if ($this->data["action_buttons"]['delete_action'] == true) {
            $this->db->where("id", $item_primary_key);
            $this->db->delete($this->data['primary_table_name']);
            redirect($this->data['current_page_link'] . "?msg=delete");
        } else {
            redirect($this->data['current_page_link']);
        }
    }

    function toggle_status($item_primary_key) {
        $current_status = $this->curd_model->get_status($this->data['primary_table_name'], $item_primary_key);
        if ($current_status != FALSE) {
            $this->curd_model->toggle_status($this->data['primary_table_name'], $item_primary_key, $current_status);
        }
        redirect($this->data['current_page_link'] . "?msg=update");
    }

}
