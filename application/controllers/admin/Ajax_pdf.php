<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Ajax_pdf extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function generate_pdf() {
        $id = $this->input->post('id');
        $data['order_data'] = $this->u_model->get_nostatcheck_row('subscriptions_transactions', array("id" => $id));
        $data['customer_details'] = $this->u_model->get_nostatcheck_row('customers', array("id" => $data['order_data']->customers_id));
        $data['package_details'] = $this->u_model->get_nostatcheck_row('packages', array("id" => $data['order_data']->packages_id));
        $template = $this->load->view('email_templates/invoice_template', $data, true);
        echo $template;
        die;
    }

}
