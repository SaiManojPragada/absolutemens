<?php

class Report extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }
        $this->data['user_page'] = "report";
    }

    function index() {
        $this->data['main_data'] = $this->data;
        $this->load->view("report-issue", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function post_report() {
        $inp_data = array(
            "customers_id" => $this->session->userdata("user_id"),
            "order_id" => $this->input->post("order_id"),
            "issue_type" => $this->input->post("issue_type"),
            "message" => $this->input->post("message"),
            "created_at" => time()
        );
        $insert = $this->u_model->post_data("customer_reports", $inp_data);
        if ($insert) {
            $this->session->set_userdata("report_success", "<span style='color: green'>Thankyou for contacting us, We will get back to you Shortly.</span>");
        } else {
            $this->session->set_userdata("report_error", "<span style='color: tomato'>Failed to send Report, Try Again later.</span>");
        }
        redirect($this->agent->referrer());
    }

}
