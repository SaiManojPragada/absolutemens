<?php

class Blogs extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model("u_model");
    }

    function index() {
        $this->data['blogs'] = $this->u_model->get_data("blogs");
        $this->load->view("includes/header", $this->data);
        $this->load->view("blog", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
