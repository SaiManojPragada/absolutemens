<?php

class My_orders extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }
        $this->load->model('u_model');
        $this->data['user_page'] = "myorders";
    }

    function index() {
        $this->data['orders'] = $this->u_model->get_orders($this->session->userdata("user_id"));

        $previous_feedbacks = $this->u_model->get_data("orders_feedbacks", array("customers_id" => $this->session->userdata("user_id")));
        foreach ($this->data['orders'] as $item) {
            $package = $this->u_model->get_nostatcheck_row("packages", array("id" => $item->packages_id));
            $item->package = $package;
            $item->feedback = array();
            foreach ($previous_feedbacks as $pfs) {
                if ($item->id == $pfs->order_id) {
                    $item->feedback = $pfs;
                }
            }
        }

        $this->data['main_data'] = $this->data;
        $this->load->view("myorders", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function view($unq_id) {
        $user_id = $this->session->userdata("user_id");
        $order_id = $unq_id;
        $this->data['order_data'] = $this->u_model->get_nostatcheck_row("subscriptions_transactions", array("unq_id" => $order_id));
        if (empty($this->data['order_data'])) {
            redirect('dashboard/my-orders');
        }
        $this->data['selected_styles'] = $this->u_model->get_data("styles", "id IN (" . rtrim($this->data['order_data']->selected_styles, ',') . ")");
        $this->data['package_details'] = $this->u_model->get_nostatcheck_row("packages", array("id" => $this->data['order_data']->packages_id));
        $this->data['main_data'] = $this->data;
        $this->load->view("order_view", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function submit_feedback() {
        $user_id = $this->session->userdata("user_id");
        $order_id = $this->input->post("order_id");
        $order_unq_id = $this->input->post("order_unq_id");
        $data = array(
            "order_id" => $order_id,
            "order_unq_id" => $order_unq_id,
            "customers_id" => $user_id,
            "rating" => $this->input->post("rating"),
            "feedback_message" => $this->input->post("feedback_message"),
            "created_at" => time(),
            "status" => 1
        );
        $check = $this->u_model->get_data_conditon_row("orders_feedbacks", array("customers_id" => $user_id, "order_id" => $order_id));
        if (!empty($check)) {
            $update = $this->u_model->update_data("orders_feedbacks", $data, array("id" => $check->id));
            if ($update) {
                $this->session->set_userdata("feedback_submission_success", "Thanks for your Feedback");
            } else {
                $this->session->set_userdata("feedback_submission_failed", "Unable to submit your Feedback");
            }
        } else {
            $ins = $this->u_model->post_data("orders_feedbacks", $data);
            if ($ins) {
                $this->session->set_userdata("feedback_submission_success", "Thanks for your Feedback");
            } else {
                $this->session->set_userdata("feedback_submission_failed", "Unable to submit your Feedback");
            }
        }
        redirect(base_url('dashboard/my-orders'));
    }

    function Request_exchange() {
        $user_id = $this->session->userdata("user_id");
        $subs_id = $this->input->post("exchange_subscription_transaction_id");
        $order_unq_id = $this->input->post("exchange_subscription_transaction_unq_id");
        $upd_arr = array(
            "order_status" => "Exchange Requested",
            "exchange_remarks" => $this->input->post("exchange_reason"),
            "exchange_request_date" => time(),
            "is_exchanged" => 1,
            "dispatched_date" => null,
            "shipped_date" => null,
            "estimated_delivery_date" => null,
            "delivered_date" => null
        );
        $upd = $this->u_model->update_data("subscriptions_transactions", $upd_arr, array("id" => $subs_id, "customers_id" => $user_id));
        $customer_data = $this->u_model->get_nostatcheck_row("customers", array("id" => $user_id));

        if ($upd) {
            $log = array(
                "order_real_id" => $subs_id,
                "order_id" => $order_unq_id,
                "action_user_id" => $user_id,
                "table_name" => 'customers',
                "log" => "Exchange Order Has Been placed By the User",
                "created_at" => time(),
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->db->insert("orders_log", $log);
            $this->data['message'] = "Hi " . $customer_data->name . ",Your Request for the Order with Id #" . $order_unq_id . " has been Successful."
                    . " You'll be hearing from us regarding the Next steps.";
            $message = $this->load->view('email_templates/orders_message_template', $this->data, true);
            $this->send_email_model->send_mail($customer_data->email, "Order Exchange Requested", $message);
            $this->session->set_userdata("feedback_submission_success", "Exchange Request for this Order has been Placed");
        } else {
            $this->session->set_userdata("feedback_submission_failed", "Unable to place Exchange Request for this Order has been Placed");
        }
        redirect(base_url('dashboard/my-orders'));
    }

    function cancel_order() {
        sleep(2);
        $user_id = $this->session->userdata("user_id");
        $subs_trans_id = $this->input->post('id');
        $reason = $this->input->post('reason');
        $update = $this->u_model->update_data("subscriptions_transactions",
                array('order_status' => 'Cancellation Requested', 'cancellation_reason' => $reason, 'cancellation_requested_date' => time()),
                array("customers_id" => $user_id, "id" => $subs_trans_id));
        if ($update) {
            $order_data = $this->u_model->get_nostatcheck_row('subscriptions_transactions', array("id" => $subs_trans_id));
            $log = array(
                "order_real_id" => $order_data->id,
                "order_id" => $order_data->unq_id,
                "action_user_id" => $this->session->userdata("user_id"),
                "table_name" => 'customers',
                "log" => "User Has Requested for the Cancellation for the Order " . $order_data->unq_id,
                "created_at" => time(),
                "order_status" => "cancellation_requested",
                "action_date_time" => date('Y-m-d H:i:s')
            );
            $this->db->insert("orders_log", $log);
            echo "Cancellation for this order has been Requested.";
        } else {
            echo "Unable to perform Cancellation request right now. Please try Again.";
        }
    }

}
