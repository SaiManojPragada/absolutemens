<?php

class Dashboard extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }
        $this->data['user_page'] = "dashboard";
    }

    function index() {
        $this->data['user_data'] = $this->u_model->get_data_conditon_row("customers", array("id" => $this->session->userdata("user_id")));
        $this->data['states'] = $this->u_model->get_nostatcheck_data("states");
        $this->data['districts'] = $this->u_model->get_nostatcheck_data("districts");
        $this->data['cities'] = $this->u_model->get_nostatcheck_data("cities");
        $this->data['main_data'] = $this->data;
        $this->load->view("dashboard", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function get_districts() {
        $state_id = $this->input->post("state_id");
        $districts = $this->u_model->get_nostatcheck_data("districts", array("states_id" => $state_id));
        $out = '<option value="">--select district--</option>';
        if ($districts) {
            foreach ($districts as $item) {
                $out .= '<option value="' . $item->id . '">' . $item->name . '</option>';
            }
        } else {
            $out = '<option value="">Cannot Deliver to this Area</option>';
        }
        echo $out;
    }

    function get_cities() {
        $districts_id = $this->input->post("districts_id");
        $cities = $this->u_model->get_nostatcheck_data("cities", array("districts_id" => $districts_id));
        $out = '<option value="">--select city--</option>';
        if ($cities) {
            foreach ($cities as $item) {
                $out .= '<option value="' . $item->id . '">' . $item->name . '</option>';
            }
        } else {
            $out = '<option value="">Cannot Deliver to this Area</option>';
        }
        echo $out;
    }

    function update_profile() {
        $data = array(
            "name" => $this->input->post("name"),
            "mobile" => $this->input->post("mobile"),
            "address" => $this->input->post("address"),
            "states_id" => $this->input->post("states_id"),
            "districts_id" => $this->input->post("districts_id"),
            "cities_id" => $this->input->post("cities_id"),
            "landmark" => $this->input->post("landmark"),
            "street_name" => $this->input->post("street_name"),
            "pincode" => $this->input->post("pincode"),
            "updated_at" => time()
        );
        $user_id = $this->session->userdata("user_id");
        $this->db->set($data);
        $this->db->where("id", $user_id);
        $update = $this->db->update("customers");
        if ($update) {
//            $this->session->set_userdata("update_profile_success", "Profile updated Succesfully.");
            $this->session->set_userdata("feedback_submission_success", "<span style='color: green'>Profile updated Succesfully.</span>");
        } else {
//            $this->session->set_userdata("update_profile_error", "Unable to Update Profile.");
            $this->session->set_userdata("feedback_submission_failed", "<span style='color: tomato'>Unable to Update Profile, Try Again.</span>");
        }
        redirect(base_url('dashboard'));
    }

}
