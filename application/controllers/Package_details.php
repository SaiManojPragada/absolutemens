<?php

class Package_details extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
    }

    function index($key) {
        $this->data['pack_details'] = $this->u_model->get_data_conditon_row("packages", array("slug" => $key));
        $this->load->view("includes/header", $this->data);
        $this->load->view("package-details", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
