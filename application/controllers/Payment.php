<?php

class Payment extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }

        $this->load->model("u_model");
        $this->load->model("razorpay_model");
        $this->load->model('send_email_model');
    }

    function index() {
        redirect(base_url('error-404'));
    }

    function pay($unq_id = null) {
        if (empty($unq_id)) {
            redirect(base_url('error-404'));
            die;
        }
        if (empty($this->session->userdata('can_pay'))) {
            redirect(base_url() . 'dashboard/my-orders');
            die;
        }

        $this->session->unset_userdata('can_pay');
        $this->data['refId'] = $unq_id;
        $this->data['order_details'] = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                array("unq_id" => $unq_id));
        $this->data['package_details'] = $this->u_model->get_nostatcheck_row("packages",
                array("id" => $this->data['order_details']->packages_id));
        $grand_total = ((float) $this->data['order_details']->total_price - (float) $this->data['order_details']->discount - (float) $this->data['order_details']->coupon_discount) + ((float) $this->data['order_details']->delivery_charges);
        $this->db->where("unq_id", $unq_id)->update("subscriptions_transactions", array("grand_total" => $grand_total));
        $this->data['grand_total'] = $grand_total;

        $this->data['order_details'] = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                array("unq_id" => $unq_id));

        $this->data['order'] = $this->razorpay_model->create_order($unq_id, $grand_total);
        $this->data['user_data'] = $this->u_model->get_data_conditon_row("customers", array("id" => $this->session->userdata('user_id')));
        $this->load->view("includes/header", $this->data);
        $this->load->view("payment", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function okPayment($unq_id = null) {
        if (empty($unq_id)) {
            redirect(base_url('error-404'));
            die;
        }
        $this->data['payment_err'] = false;

        $this->data['existing'] = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                array("unq_id" => $unq_id));

        $this->data['package_details'] = $this->u_model->get_nostatcheck_row("packages",
                array("id" => $this->data['existing']->packages_id));

        $log = array(
            "order_real_id" => $this->data['existing']->id,
            "order_id" => $unq_id,
            "action_user_id" => $this->session->userdata("user_id"),
            "table_name" => 'customers',
            "log" => "Order Has Been Created By the User and Payment is Successful",
            "created_at" => time(),
            "action_date_time" => date('Y-m-d H:i:s')
        );
        $this->db->insert("orders_log", $log);
        $this->load->view("includes/header", $this->data);
        $this->load->view("payment_message", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function noPayment($unq_id = null) {
        if (empty($unq_id)) {
            redirect(base_url('error-404'));
            die;
        }

        $log = array(
            "order_real_id" => $this->data['existing']->id,
            "order_id" => $unq_id,
            "action_user_id" => $this->session->userdata("user_id"),
            "table_name" => 'customers',
            "log" => "Order Has Been Created By the User and Payment is Failed",
            "created_at" => time(),
            "action_date_time" => date('Y-m-d H:i:s')
        );
        $this->db->insert("orders_log", $log);

        $this->data['payment_err'] = true;
        $this->load->view("includes/header", $this->data);
        $this->load->view("payment_message", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
