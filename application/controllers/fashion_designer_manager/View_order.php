<?php

class View_order extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
    }

    function view($order_id) {
        $data['order_id'] = $order_id;
        $this->fashion_view("view_order", $data);
    }

}
