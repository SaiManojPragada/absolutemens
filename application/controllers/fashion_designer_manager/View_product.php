<?php

class View_product extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
    }

    function view($id, $inventory_id) {
        $this->data["active_main_page"] = "Products Preview";
        $data['id'] = $id;
        $data['inventory_id'] = $inventory_id;
        $this->fashion_view("view_product", $data);
    }

}
