<?php

class Login extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if (!empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/dashboard');
        }
    }

    function index() {
        print_r($this->session->userdata("fashion_id"));
        $this->fashion_view("login");
    }

}
