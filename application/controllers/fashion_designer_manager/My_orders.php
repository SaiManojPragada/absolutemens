<?php

class My_orders extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
    }

    function index() {
        $data['active_main_page'] = 'my_orders';
        $data['active_sub_page'] = 'my_orders';
        $this->fashion_view("my_orders", $data);
    }

    function to_be_approved() {
        $data['active_main_page'] = 'my_orders';
        $data['active_sub_page'] = 'to_be_approved_orders';
        $this->fashion_view("to_be_approved_orders", $data);
    }

    function forwarded_to_admin() {
        $data['active_main_page'] = 'my_orders';
        $data['active_sub_page'] = 'forwarded_to_admin_orders';
        $this->fashion_view("forwarded_to_admin_orders", $data);
    }

    function in_process() {
        $data['active_main_page'] = 'my_orders';
        $data['active_sub_page'] = 'in_process_orders';
        $this->fashion_view("in_process_orders", $data);
    }

}
