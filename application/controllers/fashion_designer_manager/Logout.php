<?php

class Logout extends MY_Controller {

    function index() {



        //$this->session->unset_userdata("user_id");
        foreach ($this->session->all_userdata() as $key => $item) {
            /* if ($key == "w_user_id") {
              continue;
              } */
            $this->session->unset_userdata($key);
        }
        $this->session->set_flashdata("msg", "You Have Successfully Logged Out.");
        redirect("fashion_designer_manager/login");
    }

}
