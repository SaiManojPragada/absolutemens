<?php

class Cart extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
    }

    function index() {
        $data = [];
        $this->fashion_view("cart", $data);
    }

}
