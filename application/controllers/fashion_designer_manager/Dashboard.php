<?php

class Dashboard extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
        $this->load->model('fashion_manager_orders_model');
    }

    function index() {
        $data['to_be_approved_orders'] = count($this->fashion_manager_orders_model->get_to_be_approved_orders());
        $data['forwarded_to_admin_orders'] = count($this->fashion_manager_orders_model->forwarded_to_admin_orders());
        $data['in_process_orders'] = count($this->in_process_orders());
        $this->fashion_view("dashboard", $data);
    }

    function in_process_orders() {
        $this->db->select('*');
        $this->db->from('subscriptions_transactions st');
        // $this->db->where("'NOT EXISTS(SELECT id FROM orders o WHERE o.subscription_transaction_id = st.id)'");
        $this->db->group_start();
        $this->db->where("st.order_status", 'Assigned to Fashion Analyst');
        $this->db->or_where('st.order_status', 'Selected Items Under Review');
        $this->db->group_end();
        $this->db->where("st.fashion_manager_id='" . $this->session->userdata('fashion_id') . "' OR st.fashion_analyst_id='" . $this->session->userdata('fashion_id') . "'");
        $data = $this->db->get()->result();
        foreach ($data as $item) {
            $item->ordered_on = date('d M Y, h:i A', $item->updated_at);
            $item->package_details = $this->u_model->get_nostatcheck_row("packages", array("id" => $item->packages_id));
            $item->package_details->title = strip_tags($item->package_details->title);
            $item->package_details->price = (int) $item->package_details->price;
            $item->package_details->price = number_format($item->package_details->price);
            $this->db->where('id', $item->customers_id);
            $item->customer_details = $this->db->get('customers')->row();
            $this->db->where('subscriptions_transactions_id', $item->unq_id);
            $item->images = $this->db->get("subscription_images")->result();
        }
        return $data;
    }

}
