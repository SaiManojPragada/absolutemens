<?php

class Shop_for_order extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
    }

    function products($unique_id = null) {

        if ($unique_id) {
            $data['unique_order_id'] = $unique_id;
            $this->fashion_view("shop_for_order", $data);
        } else {
            $this->session->set_flashdata('error', 'This link is broken');
            redirect(base_url() . 'fashion_designer_manager/my_orders', 'refresh');
        }
    }

}
