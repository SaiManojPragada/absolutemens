<?php

class Change_password extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("fashion_id"))) {
            redirect('fashion_designer_manager/login');
        }
    }

    function index() {
        $this->fashion_view("change_password");
    }

}
