<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Preferances
 *
 * @author Admin
 */
class Preferances extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }
        $this->data['user_page'] = "preferances";
    }

    function index() {
        $this->data['user_data'] = $this->u_model->get_data_conditon_row("customers", array("id" => $this->session->userdata("user_id")));
        $this->data['states'] = $this->u_model->get_nostatcheck_data("states");
        $this->data['districts'] = $this->u_model->get_nostatcheck_data("districts");
        $this->data['cities'] = $this->u_model->get_nostatcheck_data("cities");
        $this->load->view("includes/header", $this->data);
        $this->load->view("subscription-preferances", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
