<?php

header('Content-type: application/json');

class Update_package_expiry extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->db->select('id,expiry_date');
        $this->db->from('subscriptions_transactions');
        $this->db->where('expiry_date <=', time());
        $result = $this->db->get()->result();
        foreach ($result as $item) {
            $this->db->set('status', 0);
            $this->db->where('id', $item->id);
            $this->db->update('subscriptions_transactions');
        }
    }

}
