<?php

class Contact_us extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model("u_model");
        $this->load->model("send_email_model");
    }

    function index() {

        if ($this->input->post("submit")) {
            $data = array(
                "name" => $this->input->post("name"),
                "email" => $this->input->post("email"),
                "phone" => $this->input->post("phone"),
                "request_type" => $this->input->post("request_type"),
                "message" => $this->input->post("message"),
                "created_at" => time()
            );
            $insert = $this->u_model->post_data("contact_enquiries", $data);
            if ($insert) {
                $site_data = $this->db->get("site_settings")->row();
                $message = $this->u_model->build_contact_message_template($data, $site_data);
                $site_data = $this->db->get("site_settings")->row();
                $send_email = $this->send_email_model->send_mail($site_data->contact_email, "Contact Enquiry", $message);
                if ($send_email) {
                    $this->session->set_userdata("contact_success", "Message sent, Thankyou for Contacting Us.");
                } else {
                    $this->session->set_userdata("contact_error", "Unable to Send your Message, Please Try Again.");
                }
            } else {
                $this->session->set_userdata("contact_error", "Unable to Send your Message, Please Try Again.");
            }

            unset($_POST);
            redirect($this->agent->referrer());
        }

        $seo = $this->u_model->get_data_conditon_row("seo", array("page_name" => "contact_us"));
        $this->data['site_property']->site_name = $this->data['site_property']->site_name . ' - ' . $seo->seo_title;
        $this->data['site_property']->seo_title = $seo->seo_title;
        $this->data['site_property']->seo_keywords = $seo->seo_keywords;
        $this->data['site_property']->seo_description = $seo->seo_description;

        $this->load->view("includes/header", $this->data);
        $this->load->view("contact-us", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function subscribe_email() {
        $data = array(
            "email" => $this->input->post("email"),
            "created_at" => time()
        );
        $check = $this->u_model->get_nostatcheck_data("email_subscriptions", array("email" => $this->input->post("email")));
        if (!$check) {
            $res = $this->u_model->post_data("email_subscriptions", $data);
            if ($res) {
                echo "<p style='color: #47ff78; font-weight: bold; padding 20px;'>Thankyou For Subscribing.</p>";
            } else {
                echo "<br>Unable to Subscribe, Try Again Later.";
            }
        } else {
            echo "<br>User Already Subscribed.";
        }
    }

}
