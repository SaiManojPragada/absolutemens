<?php

class Package_steps extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }
        $this->load->model("u_model");
        $this->data['package_banners'] = $this->u_model->get_data("package_page_banners");
    }

    function index() {
        redirect(base_url('error-404'));
    }

    function upload_submitted_images($key, $type) {

//        print_r($_FILES);
//        die;
        $countfiles = count($_FILES['files']['name']);
        $files = $_FILES;
        $real_path = realpath(APPPATH . '../uploads/user/');
        $config['upload_path'] = $real_path;
        $config['allowed_types'] = 'jpg|png';
        $config['encrypt_name'] = true;
        $config['overwrite'] = FALSE;
        $this->load->library('upload', $config);
        for ($i = 0; $i < $countfiles; $i++) {
            $_FILES['files']['name'] = $files['files']['name'][$i];
            $_FILES['files']['type'] = $files['files']['type'][$i];
            $_FILES['files']['tmp_name'] = $files['files']['tmp_name'][$i];
            $_FILES['files']['size'] = $files['files']['size'][$i];
            $this->upload->do_upload('files');
            $img_data = $this->upload->data();
            $dataInfo = array(
                "subscriptions_transactions_id" => $key,
                "customers_id" => $this->session->userdata("user_id"),
                "image" => 'uploads/user/' . $img_data['file_name'],
                "type" => $type,
                "created_at" => time()
            );
            $insert = $this->u_model->post_data("subscription_images", $dataInfo);
        }
        echo $this->prepdisplay_info($key, $type);
        die;
    }

    function prepdisplay_info($key, $type) {
        $get_images = $this->u_model->get_data("subscription_images",
                array("subscriptions_transactions_id" => $key, "type" => $type));
        $out = "";
        foreach ($get_images as $img) {
            $out .= '<div class="col-3">
                        <a onclick="deleteImage(`' . $img->id . '`,`' . $img->type . '`,`' . $img->subscriptions_transactions_id . '`)">
                            <center>
                                <i class="fal fa-trash-alt"></i>
                            </center>
                        </a>
                        <img src="' . base_url($img->image) . '" alt="alt"/>
                    </div>';
        }
        return $out;
    }

    function delete_image() {
        $this->u_model->delete_data("subscription_images", array("id" => $this->input->post("id")));
        echo $this->prepdisplay_info($this->input->post("unq_id"), $this->input->post("type"));
    }

    function submit_form($key = null, $route_key = null) {
        if (empty($key)) {
            redirect(base_url('error-404'));
            die;
        }

        if (!$route_key) {
            $route = "step-two";
        } else {
            $route = $route_key;
        }

        if ($route_key == 'step-three') {

            $unq_id = $_POST['unq_id'];
            unset($_POST['unq_id']);
            $data['selected_sizes'] = json_encode($_POST);
            $data['unq_id'] = $unq_id;

            $insert = $this->do_update($data);
            if ($insert) {
                redirect(base_url('package-steps/' . $route . '/' . $key . '/' . $unq_id));
            } else {
                redirect(base_url('package-steps/step-one/' . $key));
            }
        }

        if ($_POST["is_billing_address_checked"] && $_POST["is_billing_address_checked"] == 'on') {
            $_POST["is_billing_address_checked"] = 1;
        } else {
            $_POST["is_billing_address_checked"] = 0;
        }
        $check_first = $this->u_model->get_nostatcheck_row("subscriptions_transactions", array("unq_id" => $unq_id));

        $data = $this->input->post();
        $package_details = (object) array();
        if ($key == "we-shop-for-you") {
            $package_details->id = 0;
            $package_details->no_of_items = $check_first->no_of_items;
            $package_details->price = 0;
            $package_details->title = "";
            $package_details->duration = $check_first->package_duration;
            $package_details->delivery_charges = $check_first->delivery_charges;
            $package_details->discount_amount = 0;
        } else {
            $package_details = $this->u_model->get_nostatcheck_row("packages", array("slug" => $key));
        }
        $data['customers_id'] = $this->session->userdata("user_id");
        $data['packages_id'] = $package_details->id;
        if (empty($check_first)) {
            $data['package_amount'] = $package_details->price;
            $data['package_duration'] = $package_details->duration;
        }
        if ($key != "we-shop-for-you") {
            $data['package_amount'] = $package_details->price;
//        $this->db->select('no_of_items, title');
//        $this->db->where("id", $package_details->id);
//        $package_no_of_items = $this->db->get("packages")->row();
            $data['no_of_items'] = $package_details->no_of_items;
            $data['package_title '] = strip_tags($package_details->title);
            $data['expiry_date'] = strtotime('+ ' . $package_details->duration . " days");
        }
        $check = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                array("unq_id" => $data['unq_id']));
        $data['grand_total'] = ($package_details->price - $package_details->discount_amount) + $package_details->delivery_charges;
        if ($key == "we-shop-for-you") {
            $data['grand_total'] = $check->grand_total;
        }
        if (!$check) {
            $package_details = $this->u_model->get_nostatcheck_row("packages", array("slug" => $key));
            if (empty($package_details)) {
                redirect(base_url());
            }
            $data['total_price'] = $package_details->price;
            $data['discount'] = $package_details->discount_amount;
            $data['delivery_charges'] = $package_details->delivery_charges;
            $data['created_at'] = time();
            $insert = $this->u_model->post_data("subscriptions_transactions", $data);
        } else {
            if (!empty($data['state'])) {
                $data['state'] = $this->u_model->get_nostatcheck_row("states", array("id" => $this->input->post('state')))->name;
                $data['district'] = $this->u_model->get_nostatcheck_row("districts", array("id" => $this->input->post('district')))->name;
                $data['city'] = $this->u_model->get_nostatcheck_row("cities", array("id" => $this->input->post('city')))->name;
            }
            $insert = $this->do_update($data);
        }

        if ($insert) {
            redirect(base_url('package-steps/' . $route . '/' . $key . '/' . $this->input->post('unq_id')));
        } else {
            redirect(base_url('package-steps/step-one/' . $key));
        }
    }

    function do_update($data = null) {
        if (empty($data)) {
            $data = $this->input->post();
            echo $this->u_model->update_data("subscriptions_transactions", $data, array("unq_id" => $data['unq_id']));
            die;
        }
        return $this->u_model->update_data("subscriptions_transactions", $data, array("unq_id" => $data['unq_id']));
    }

    function step_one($key = null, $unq = null) {
        if (empty($key)) {
            redirect(base_url('error-404'));
            die;
        }
        $this->data['random_unq_key'] = $this->generate_random_key("subscriptions_transactions", "unq_id");
        if (!empty($unq)) {
            $existing = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                    array("unq_id" => $unq));
            $this->data['random_unq_key'] = $unq;
            $this->data['selected_styles'] = $existing->selected_styles;
        }
        $this->data['current_package'] = $key;
        $this->data['package_details'] = $this->u_model->get_data_conditon_row("packages",
                array("slug" => $key));
        $this->data['styles'] = $this->u_model->get_data("styles");
        $this->load->view("includes/header", $this->data);
        $this->load->view("package-step-1", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function step_two($key = null, $unq = null) {
        if (empty($key) || empty($unq)) {
            redirect(base_url('error-404'));
            die;
        }

        $we_shop_for_u_pref = $this->u_model->get_data_conditon_row("user_preferences", array("customers_id" => $this->session->userdata("user_id")))->preferences;

        $this->data['user_preferences'] = $this->u_model->get_data_conditon_row("customers", array("id" => $this->session->userdata("user_id")))->preferences;

        $previously_selected_pref = $this->db->get_where("subscriptions_transactions", array("unq_id" => $unq))->row()->selected_sizes;

        if ($we_shop_for_u_pref != "") {
            $user_preferences = json_decode($we_shop_for_u_pref);
        } else if ($this->data['user_preferences']) {
            $user_preferences = json_decode($this->data['user_preferences']);
        } else {
            $user_preferences = null;
        }

        if ($previously_selected_pref != 'null') {
            $user_preferences = json_decode($previously_selected_pref);
        }

        $this->data['categories'] = $this->u_model->get_categories_with_sizes($user_preferences);

        $this->data['package_details'] = $this->u_model->get_data_conditon_row("packages", array("slug" => $key));
        $this->data['current_package'] = $key;
        $this->data['random_unq_key'] = $unq;

        $this->load->view("includes/header", $this->data);
        $this->load->view("package-step-2", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    public function step_three($key = null, $unq = null) {
        if (empty($key) || empty($unq)) {
            redirect(base_url('error-404'));
            die;
        }
        $this->data['user_preferences'] = $this->u_model->get_data_conditon_row("user_preferences",
                array("customers_id" => $this->session->userdata("user_id")));

        $existing = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                array("unq_id" => $unq));
//            print_r($existing);
        if (!empty($existing->height)) {
            $this->data['user_preferences']->height = $existing->height;
            $this->data['user_preferences']->weight = $existing->weight;
            $this->data['user_preferences']->preferences_likes = $existing->preferences_likes;
            $this->data['user_preferences']->preferences_dislikes = $existing->preferences_dislikes;
        }


        //HEIGHTS AND WEIGHTS

        $this->data['heights'] = $this->u_model->get_data("heights");
        $this->data['weights'] = $this->u_model->get_data("weights");

        $this->data['package_details'] = $this->u_model->get_data_conditon_row("packages",
                array("slug" => $key));

        $this->data['previous_full_images'] = $this->u_model->get_data("subscription_images",
                array("subscriptions_transactions_id" => $unq, "customers_id" => $this->session->userdata("user_id"), "type" => "Full"));

        $this->data['previous_half_images'] = $this->u_model->get_data("subscription_images",
                array("subscriptions_transactions_id" => $unq, "customers_id" => $this->session->userdata("user_id"), "type" => "Half"));
        $this->data['current_package'] = $key;
        $this->data['random_unq_key'] = $unq;

        $this->load->view("includes/header", $this->data);
        $this->load->view("package-step-3", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    public function step_four($key = null, $unq = null) {
        if (empty($key) || empty($unq)) {
            redirect(base_url('error-404'));
            die;
        }
        $this->data['package_details'] = $this->u_model->get_data_conditon_row("packages",
                array("slug" => $key));
        $existing = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                array("unq_id" => $unq));

        $this->data['user_data'] = $this->u_model->get_data_conditon_row("customers", array("id" => $this->session->userdata('user_id')));
        $this->data['user_data']->is_billing_address_checked = $existing->is_billing_address_checked;

        $selected_state_id = $this->u_model->get_nostatcheck_row("states", array("name" => $existing->state))->id;
        $selected_district_id = $this->u_model->get_nostatcheck_row("districts", array("name" => $existing->district))->id;
        $selected_city_id = $this->u_model->get_nostatcheck_row("cities", array("name" => $existing->city))->id;

        if ($existing->address_name) {
            $this->data['user_data']->name = $existing->address_name;
            $this->data['user_data']->email = $existing->address_email;
            $this->data['user_data']->mobile = $existing->phone_number;
            $this->data['user_data']->alternative_phone_number = $existing->alternative_phone_number;
            $this->data['user_data']->address = $existing->address;
            $this->data['user_data']->landmark = $existing->landmark;
            $this->data['user_data']->street_name = $existing->street_name;
            $this->data['user_data']->pincode = $existing->pincode;
            $this->data['user_data']->cities_id = $selected_city_id;
            $this->data['user_data']->states_id = $selected_state_id;
            $this->data['user_data']->districts_id = $selected_district_id;
        }
        $selected_state_id = $this->data['user_data']->states_id;
        $selected_district_id = $this->data['user_data']->districts_id;
        $selected_city_id = $this->data['user_data']->cities_id;
        $this->data['states'] = $this->u_model->get_nostatcheck_data("states");
        $this->data['districts'] = $this->u_model->get_nostatcheck_data("districts", array("states_id" => $selected_state_id));
        $this->data['cities'] = $this->u_model->get_nostatcheck_data("cities", array("districts_id" => $selected_district_id));

        $this->data['current_package'] = $key;
        $this->data['random_unq_key'] = $unq;
        $this->load->view("includes/header", $this->data);
        $this->load->view("package-step-4", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function checkout($key = null, $unq = null) {
        if (empty($key) || empty($unq)) {
            redirect(base_url('error-404'));
            die;
        }

        $this->data['package_details'] = $this->u_model->get_data_conditon_row("packages",
                array("slug" => $key));
        $this->data['existing'] = $this->u_model->get_nostatcheck_row("subscriptions_transactions",
                array("unq_id" => $unq));

        $this->data['selected_images'] = $this->u_model->get_data("subscription_images",
                array("subscriptions_transactions_id" => $unq, "customers_id" => $this->session->userdata("user_id")));

        $this->data['selected_styles'] = $this->u_model->select_styles($this->data['existing']->selected_styles);

        $this->data['current_package'] = $key;
        $this->data['random_unq_key'] = $unq;

        $this->data['user_preferences'] = $this->u_model->get_data_conditon_row("customers", array("id" => $this->session->userdata("user_id")))->preferences;

        $previously_selected_pref = $this->db->get_where("subscriptions_transactions", array("unq_id" => $unq))->row()->selected_sizes;

        if ($this->data['user_preferences']) {

            $user_preferences = json_decode($this->data['user_preferences']);
        } else {
            $user_preferences = null;
        }



        if ($previously_selected_pref != 'null') {
            $user_preferences = json_decode($previously_selected_pref);
        }

        $this->data['categories'] = $this->u_model->get_categories_with_sizes($user_preferences);

        $this->load->view("includes/header", $this->data);
        $this->load->view("package-confirm", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function we_shop_for_you_order() {
        $budget = (float) $this->input->post('budget');
        $no_of_items = (float) $this->input->post('no_of_items');
        $we_shop_data = $this->u_model->get_data_row("we_shop_for_you_values");
        $we_shop_min_budget = (float) $we_shop_data->minimum_amount;
        if ($budget < $we_shop_min_budget) {
            $resp = array(
                "status" => false,
                "show_swal" => false,
                "message" => "Minimum Amount Should be Atleast ₹" . number_format($we_shop_min_budget)
            );
            echo json_encode($resp);
            die;
        } else {
            $unq_id = $this->generate_random_key("subscriptions_transactions", "unq_id");
            $data['unq_id'] = $unq_id;
            $data['is_we_shop_for_you'] = true;
            $data['grand_total'] = $budget + $we_shop_data->delivery_charges;
            $data['total_price'] = $budget;
            $data['discount'] = 0;
            $data['no_of_items'] = $no_of_items;
            $data['delivery_charges'] = $we_shop_data->delivery_charges;
            $data['package_duration'] = $we_shop_data->expiry_duration;
            $data['expiry_date'] = strtotime('+ ' . $we_shop_data->expiry_duration . " days");
            $data['customers_id'] = $this->session->userdata("user_id");
            $data['created_at'] = time();
            $insert = $this->u_model->post_data("subscriptions_transactions", $data);
            if ($insert) {
                $resp = array(
                    "status" => true,
                    "show_swal" => false,
                    "message" => base_url() . "package-steps/step-one/we-shop-for-you/" . $unq_id
                );
            } else {
                $resp = array(
                    "status" => false,
                    "show_swal" => false,
                    "message" => "Unable perform this action. Please Try Again."
                );
            }
            echo json_encode($resp);
            die;
        }
    }

}
