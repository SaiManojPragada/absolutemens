<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Change_password
 *
 * @author Admin
 */
class Change_password extends MY_Controller {

    //put your code here

    function __construct() {
        parent::__construct();
        if (empty($this->session->userdata("user_unq_id"))) {
            $this->session->set_userdata("login_err", "Please Login to Continue.");
            redirect(base_url());
        }
        $this->load->model('u_model');
        $this->data['user_page'] = "changepassword";
    }

    function index() {
        $this->data['main_data'] = $this->data;
        $this->load->view("change_password_user", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

    function change() {
        if (!empty($this->input->post())) {
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');
            $re_new_password = $this->input->post('re_new_password');
            if ($new_password !== $re_new_password) {
                $this->session->set_userdata("change_password_e", "Passwords Does not Match");
            }
            $user_id = $this->session->userdata('user_id');
            $user_data = $this->u_model->get_nostatcheck_row('customers', array("id" => $user_id));
            if (md5($old_password . $user_data->salt) === $user_data->password) {
                $salt = generateRandomString(12);
                $update_data = array(
                    "password" => md5($new_password . $salt),
                    "salt" => $salt
                );
                $update = $this->u_model->update_data("customers", $update_data, array("id" => $user_id));
                if ($update) {
                    $this->session->set_userdata("change_password_s", "Password updated");
                } else {
                    $this->session->set_userdata("change_password_e", "Passwords Does not Match");
                }
            } else {
                $this->session->set_userdata("change_password_e", "Invalid Old Password");
            }
        }
        redirect(base_url('change_password'));
    }

}
