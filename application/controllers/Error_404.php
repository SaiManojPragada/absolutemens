<?php

class Error_404 extends MY_Controller {

    public $data;

    function __construct() {
        parent::__construct();
        $this->load->model('u_model');
    }

    function index() {

        $this->load->view("includes/header", $this->data);
        $this->load->view("error", $this->data);
        $this->load->view("includes/footer", $this->data);
    }

}
