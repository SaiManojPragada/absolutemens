<?php

function make_slug_name($title) {
    return strtolower(str_replace(' ', '-', trim(preg_replace('/\s\s+/', ' ', $title))));
}
