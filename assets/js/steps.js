
(function($) {

    var form = $("#wizard-form");
    form.steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        labels: {
            previous: 'Previous',
            next: 'Next',
            finish: 'Continue to Checkout',
            current: ''
        },
        titleTemplate: '<div class="title"><span class="number">#index#</span>#title#</div>',
        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            if(form.valid()){
                setTimeout(function(){
                    $("html, body").animate({ scrollTop: 0 }, 1000); 
                }, 500);
            }
            return form.valid();
        },
        onFinishing: function(event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            console.log(currentIndex);
            return form.valid();
        },
        onFinished: function(event, currentIndex) {
            // alert('Sumited');
            document.getElementById("wizard-form").submit();
        },
    });


    // Custom Button Jquery Steps
    $('.forward').click(function() {
        $("#wizard").steps('next');
    })
    $('.backward').click(function() {
        $("#wizard").steps('previous');
    })
    // jQuery.extend(jQuery.validator.messages, {
    //     required: "Required Field",
    //     remote: "",
    //     url: "",
    //     date: "",
    //     dateISO: "",
    //     number: "",
    //     digits: "",
    //     creditcard: "",
    //     equalTo: ""
    // });
})(jQuery);


if (currentIndex === 4) { //if last step
    //remove default #finish button
    $('#wizard-form').find('a[href="#finish"]').remove();
    //append a submit type button
    $('#wizard-form .actions li:last-child').append('<button type="submit" id="submit" class="btn-large"><span class="fa fa-chevron-right"></span></button>');
}